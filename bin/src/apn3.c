#include "stdio.h"

int main(int argc, char *argv[])
   {
   /* Gives the absolute path name of a file */
   char pwd[1024];
   char ext[1024];
  
   /* Find the pwd and file extension arguments */
   int i;
   char *p;
   char *c = pwd;
   for (i=1; i<argc; i++)
      {
      /* Transfer to the first argument */
      for (p=argv[i]; *p; p++) *(c++) = *p;
      if (*(p-1) == ':') 
	      {
	      *(c-1) = '\0';
	      c = ext;
	      }
      else
	      {
	      *(c++) = ' ';
	      }
      }
 
   /* printf ("pwd = \"%s\"\n", pwd+1);
   printf ("ext = \"%s\"\n", ext+1); */

   /* First, check for absolute path name */
   if (ext[1] == '/' || ext[1] == '\\')
      printf ("%s", ext+1);
   else
      printf ("%s/%s", pwd+1, ext+1);
   }
   
