#include <stdio.h>

int main (int argc, char *argv[])
   {
   /* Copies a file to output */
   FILE *infile;
   int skipping = 0;

   /* Check the arguments */
   if (argc != 2)
      {
      fprintf (stderr, "usage : fix_nl <filename>\n");
      return (0);
      }

   /* Now open the file */
   infile = fopen (argv[1], "r");
   if (infile == NULL)
      {
      fprintf (stderr, "fix_nl : Can not open file \"%s\" for input\n", 
	argv[1]);
      return (0);
      }

   /* Now copy input to output */
   while (1)
      {
      // Read a character
      int n = getc (infile);
      
      // If EOF, then done
      if (n == EOF) break;

      // If the character is ^M, then put nl
      if (n == 13) putchar('\n');
      else putchar (n);
      } 

   /* Close the file */
   fclose (infile);

   /* Return success */
   return 0;
   }
