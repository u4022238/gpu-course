#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
   {
   int n;
   FILE *infile;
   int lastchar;

   /* Get the name of the file */
   if (argc > 1)
      {
      /* File name is first argument */
      char *infilename = argv[1];
 
      /* Open the file */
      infile = fopen (infilename, "r");
      if (infile == (FILE *)0)
	      {
	      fprintf (stderr, "Unable to open file \"%s\" for input\n", infilename);
	      exit (1);
         } 
      }
   else 
      infile = stdin;
   
   /* Get the first character */
   lastchar = getc (infile);
   
   /* Go into a loop translating the standard input */
   while (1)
      {
      /* Get the next character */
      n = getc (infile);
      
      /* Break on end of file */
      if (n == EOF) break;

      /* Write out the last character, unless it is an end of line */
      if (lastchar != '\015' || n != '\012') putchar (lastchar);

      /* Put the last character to the present one */
      lastchar = n;
      }

   /* Output the last character */
   if (lastchar != EOF) putchar (lastchar);
   }
  
