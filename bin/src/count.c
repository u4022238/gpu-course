#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
   {
   int i;
   if (argc == 1)
      return 0;

   if (argc == 2)
      {
      int n = atoi(argv[1]);
      int i;
      for (i=1; i<=n; i++)
         printf ("%d ", i);
      }

   else
      {
      int start = atoi(argv[1]);
      int end = atoi(argv[2]);
      int incr = 1;

      if (argc >3)
         incr = atoi(argv[3]);

      for (i=start; i<=end; i+=incr)
         printf ("%d ", i);
      }
   }
