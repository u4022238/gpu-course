#include <stdio.h>

//
//  The purpose is to remove line feeds so it can be used as Microsoft
//  word input.
//

#define CR 13
#define LF 10

int main (int argc, char *argv[])
   {
   /* Copies a file to output */
   FILE *infile;
   int skipping = 0;

   /* Check the arguments */
   if (argc != 2)
      {
      fprintf (stderr, "usage : fix_nl <filename>\n");
      return (0);
      }

   /* Now open the file */
   infile = fopen (argv[1], "r");
   if (infile == NULL)
      {
      fprintf (stderr, "fix_nl : Can not open file \"%s\" for input\n", 
	argv[1]);
      return (0);
      }

   /* Now copy input to output */
   int pending_state = 0; // 0 = no LF seen.  1 = LF seen, one pending. 
                          // 2 = LF seen, none pending
   while (1)
      {
      // Read a character
      int n = getc (infile);
      
      // If EOF, then done
      if (n == EOF) break;

      // If the character is ^M, then put nl
      if (n == CR) 
         {
         // Ignore CR entirely
         }
      else if (n == LF)
         {
         if (pending_state == 1)
            {
            putchar ('\n');
            putchar ('\n');
            pending_state = 2;
            }
         else if (pending_state == 2)
            {
            putchar ('\n');
            }
         else
            {
            putchar (' ');
            pending_state = 1;
            }
         }
      else
         {
         putchar(n);
         pending_state = 0;
         }
      } 

   /* Close the file */
   fclose (infile);

   /* Return success */
   return 0;
   }
