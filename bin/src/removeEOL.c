#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
   {
   int n;
   FILE *infile;
   int lfcount;

   /* Get the name of the file */
   if (argc > 1)
      {
      /* File name is first argument */
      char *infilename = argv[1];
 
      /* Open the file */
      infile = fopen (infilename, "r");
      if (infile == (FILE *)0)
         {
         fprintf (stderr, "Unable to open file \"%s\" for input\n", infilename);
         exit (1);
         } 
      }
   else 
      infile = stdin;
   
   /* Get the first character */
   lfcount = 0;
   
   /* Go into a loop translating the standard input */
   while (1)
      {
      /* Get the next character */
      n = getc (infile);
      
      /* Break on end of file */
      if (n == EOF) break;

      /* Skip '\015' altogether */
      if (n == '\015') continue;

      /* If the character is not CR or LF, then output it */
      if (n != '\012') 
         {
         /* If a blank is pending, then output it */
         if (lfcount == 1) 
            putchar (' ');

         /* Output the character */
         putchar(n);

         /* Reset lfcount */
         lfcount = 0;

         /* Done for this character */
         continue;
         }

      /* At this point, the current character must be a '012' */
      if (lfcount >= 1)
         {
         /* Put a lf */
         putchar ('\n');
         }

      /* Update the LF count */
      lfcount++;
      }
   }
  
