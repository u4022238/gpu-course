#include "stdio.h"

int main(int argc, char *argv[])
   {
   /* Gives the absolute path name of a file */
   char *pwd = argv[1];
   char *extn = argv[2];

   /* First, check for absolute path name */
   if (extn[0] == '/' || extn[0] == '\\')
      {
      printf ("%s", extn);
      }
   else
      {
      // Check out pwd.  Sometimes it has a slash too many
      if (pwd[0] == '/' && pwd[1] == '/' && pwd[3] != '/' && pwd[3] != '\0')
	      pwd = &(pwd[1]);

      // Special case where pwd is /
      if (pwd[0] == '/' && pwd[1] == '\0')
	      printf ("/%s", extn);

      // Normal case
      else
         printf ("%s/%s", pwd, extn);
      }
   }
   
