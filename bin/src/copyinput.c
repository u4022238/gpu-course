#include <stdio.h>

int main (int argc, char *argv[])
   {
   /* Copies a file to output */
   FILE *infile;
   int skipping = 0;

   /* Check the arguments */
   if (argc != 2)
      {
      fprintf (stderr, "copyinput : usage : copyinput <filename>\n");
      return (0);
      }

   /* Now open the file */
   infile = fopen (argv[1], "r");
   if (infile == NULL)
      {
      fprintf (stderr, "copyinput : Can not open file \"%s\" for input\n", 
	argv[1]);
      return (0);
      }

   /* Now copy input to output */
   while (1)
      {
      // Read a character
      int n = getc (infile);
      
      // If EOF, then done
      if (n == EOF) break;

      // If the character is #, then start skipping
      if (n == '#') skipping = 1;

      // Stop skipping at end of line
      if (n == '\n') skipping = 0;

      // Otherwise, put to output
      if (! skipping ) putchar (n);
      } 

   /* Close the file */
   fclose (infile);

   /* Return success */
   return 0;
   }
