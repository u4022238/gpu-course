/* @(#)apn.c	1.1 9/21/87 */
/* Outputs the absolute path name of a file */
#define MAXPATHLEN 512
// #include <winnt.h>
// #include <windef.h>
// #include <winbase.h>
#include "stdio.h"
char str[MAXPATHLEN];

int main(int argc, char *argv[])
   {
   /* Gives the absolute path name of a file */
   register int i;
   for (i=1; i<argc; i++)
      {
      char buffer[1024];
      if (*(argv[i]) == '/') printf ("%s\n",argv[i]);
      else 
         {
	      int n = GetCurrentDirectory (1024, buffer);
         printf ("%s/%s\n",buffer,argv[i]);
         }
      }
   }
   
