#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Characters to cut the line before
static char cutchars[] = "  &\\{,.+-";

typedef enum {CRLF, CR, LF} eol_format;

int read_line (FILE *infile, char *buf)
   {
   // Read a line from input to a buffer
   int charcount = 0;

   char *p = buf;
   while (1)
      {
      // Get a character from the file
      int n = getc(infile);
      if (n != EOF) charcount++;

      // If this is end of line, or end of file, then replace by 0 and break
      if (n == '\n' || n == EOF)
         {
         *p = '\0';
         return charcount;
         }

      // Otherwise, put the character in the buffer
      *(p++) = n;
      }
   }

int find_comment_start (char *buf)
   {
   // Finds the position of a comment in a line -- line must end with '\0'
   for (int i=0; ; i++)
      {
      // Return position if it is a comment character
      if (buf[i] == '%' && buf[i-1] != '\\') return i;

      // Return -1 if it is end of line
      if (buf[i] == '\0') return -1;
      }
   }

int main (int argc, char *argv[])
   {
   int MaxLineLen = 80;
   eol_format format = CRLF;

   // Get the parameters
   // Skip over the program name
   char *program_name = argv[0];
   argv++; argc--;
 
   // Read the parameters
   while (argc > 0)
      {  
      if (argv[0][0] != '-') break;
 
      // parse the option
      switch (argv[0][1])
         {
         case 'L' :
            {
            // Specifies the method to be used
  	         MaxLineLen = atoi(argv[0]+2);
            break;
            }
         default :
            {
            fprintf (stderr, "%s : Unknown option \"%s\"",
                program_name, argv[0]);
            exit (1);
            break;
            }
         }

      // Skip to the next argument
      argv++; argc--;
      }  

   // Check the input line
   if (argc != 1)
      {
      fprintf (stderr, "Usage: latex_fix [-Lnn] infilename\n");
      exit (1);
      }
   
   // Open the file
   char *infilename = argv[0];
   FILE *infile = fopen (infilename, "r");
   if (!infile)
      {
      fprintf (stderr, "Can not open file \"%s\" for read\n", infilename);
      exit (1);
      }

   // Now, read the input and translate to output
   char line[256000];    // Assume no line bigger than 256000
   while (1)
      {
      // Read an input line - if no more characters, then exit loop
      int nread = read_line (infile, line);
      if (nread == 0)
         break;

      // Get the length of the line
      int linelen = strlen(line);

      // Check for a comment
      int comment_start = find_comment_start (line);

      // Now, write line out in MaxLineLen character chunks
      int lastcut = 0;
      while (1)
         {
         // Look for a good place to cut (i.e. a space)
         int minlen = 40;
         int maxlen = MaxLineLen;

         // See if the line has remaining length less than MaxLineLen
         int cuthere = -1;
         if (linelen - lastcut < MaxLineLen)
             cuthere = linelen;

         else
            {
            // First try finding a character to cut at
            for (char *ch=cutchars; ch++; *ch)
               {
               for (int i=maxlen; i>=minlen; i--)
                  if (line[lastcut + i] == *ch) 
                     {
                     cuthere = lastcut + i;
                     break;
                     }

               // Break further out
               if (cuthere != -1) break;
               }

            // Next, try something else
            if (cuthere == -1) cuthere = lastcut + maxlen;
            }

         // Write out the line now

         // Put a comment marker before it if necessary
         if (comment_start != -1 && comment_start < lastcut)
            printf ("%% ");  // This really means just one %

         // Now write out the rest of the line, followed by end of line
         for (int i=lastcut; i<cuthere; i++) 
            if (line[i] != '\r') putchar(line[i]);

         // According to format, put end of line
         switch (format)
            {
            case CRLF : putchar ('\r'); putchar ('\n'); break;
            case   LF : putchar ('\n'); break;
            case   CR : putchar ('\r'); break;
            default   : putchar ('\n'); break;
            }

         // Set this as next line start - if it is a blank, then skip it.
         lastcut = cuthere;
         if (line[cuthere] == ' ') lastcut++;

         // Now, we break if the line is finished
         if (lastcut == linelen) break;
         }
      }
   
   return 0;
   }
