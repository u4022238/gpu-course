#include <ncurses.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CR 10
#define LF 13
#define CTRL_D 4
#define UP_KEY     'k'
#define DOWN_KEY   'j'
#define ABORT_KEY  'x'
#define SEARCH_KEY  '/'
#define ABORT_KEY2  '.'

typedef enum {SEARCH_MODE, STANDARD_MODE} Mode;

Mode mode = STANDARD_MODE;
static int highlighted_line = 0;

int get_file_size(char *fname)
   {
   /* First, open the file */
   int fd = open (fname, O_RDONLY);
   if (fd == 0)
      {
      fprintf (stderr, "Can not open file \"%s\" for read\n", fname);
      exit (1);
      }

   /* Next, do an lseek to find the size of the file */
   int size = lseek (fd, 0L, SEEK_END);

   /* Close the file */
   close (fd);
   
   return size;
   }

void read_input_file (char *fname, char **fbuf, int *fsize)
   {
   /* Reads in the input file and puts it in memory */

   /* First, get the file size */
   int size = get_file_size (fname);
   *fsize = size;

   /* Make a buffer of the right size */
   char *buf = (char *)malloc (size+2);
   if (buf == 0)
      {
      fprintf (stderr, "Can not allocate %d bytes for buffer\n", size+1);
      exit(1);
      }
   *fbuf = buf;

   /* Open the file again */
   int fd = open (fname, O_RDONLY);
   if (fd == 0)
      {
      fprintf (stderr, "Can not open file \"%s\" for read\n", fname);
      exit (1);
      }

   /* Read it in */
   read (fd, buf, size);

   /* Put a null character on the end */
   buf[size] = '\0';
   buf[size+1] = '\0';

   /* Close the file */
   close (fd);
   }

void set_pointers (
	char *buf, 
	int bufsize, 
	char ***pline, 
	int *pnlines
	)
   {
   /* Sets up pointers to the two parts of each line */
   int i;

   /* First, count the lines */
   int nlines = 0;
   for (i=0; i<bufsize; i++)
      if (buf[i] == '\n') nlines++;
   *pnlines = nlines;

   /* Next, allocate the buffers */
   char **line = (char **)malloc(nlines * sizeof(char *));
   if (line == 0)
      {
      fprintf (stderr, "Can not allocate space for tables\n");
      exit(1);
      }
   *pline = line;

   /* Now, go through and fill them */
      {
      char *cursor = buf;	/* Position in the buffer */
      for (i=0; i<nlines; i++)
	 {
	 /* This is the position of the cursor */
	 line[i] = cursor;

         /* Now, go on to find the next LF or CR */
  	 while (*cursor != LF && *cursor != CR && *cursor != '\0') cursor++;

	 /* Change it to a null */
	 *cursor = '\0';

         /* Also any other CR or LF */
	 cursor += 1;
	 while (*cursor == LF || *cursor == CR) 
	   {
	   *cursor = '\0';
	   cursor++;
	   }

	 /* Now, this is the start of the next line, so continue */
	 }
      }
   }

bool match (char *str1, char *str2)
   {
   /* True of str2 matches a substring of str1 */
   int i;
   
   /* First sum str2 */
   int str2len = strlen(str2);
   int str2sum = 0;
   for (i=0; i<str2len; i++)
      str2sum += str2[i];

   /* Sum the start of string1 */
   int str1len = strlen(str1);
   if (str1len < str2len) return false;

   int str1sum = 0;
   for (i=0; i<str2len; i++)
      str1sum += str1[i];

   /* Now, continue to consider all the characters of str1 */
   for (i=0; i<=str1len-str2len; i++)
      {
      /* Check the sum */
      if (str1sum == str2sum)
	 {
	 /* Strings are potentially the same check */
 	 int j;
	 bool true_match = true;
         for (j=0; j<str2len; j++)
	   if (str1[i+j] != str2[j]) 
	      {
	      true_match = false;
	      break;
	      }

	 if (true_match)
	    return true;
	 }

      /* Otherwise, strings are different, so go on to the next character */
      str1sum += str1[i+str2len] - str1[i];
      }

   /* Have not found them to be the same */
   return false;
   }

void print_lines(char **line, int highlight, int startline, int endline)
{
   int i;   
   int x = 0;
   int y = 0;

   for(i = startline; i<=endline; i++)
   {  
      if (highlight == i) /* Highlight the present choice */
      {  
	 /* Set referse video */
	 attron(A_REVERSE); 

	 /* Print the line */
	 mvprintw(y, x, "%s", line[i]);
         clrtoeol();

	 /* Turn off reverse video */
         attroff(A_REVERSE);

	 /* Store this as the highlighted line */
	 highlighted_line = i;
      }
      else
	 {
	 mvprintw(y, x, "%s", line[i]);
	 clrtoeol();
	 }

      ++y;
   }

   /* Draw it */
   refresh();
}

void print_search_lines(
	char **line, 
	int highlight, 
	char *search_string, 
	int nlines, int nshown)
{
   int i;   
   int x = 0;
   int y = 0;

   // Print the search string
   mvprintw(y, x, "Search String: %s", search_string);
   clrtoeol();
   ++y; 
   mvprintw(y, x, "");
   clrtoeol();
   ++y;

   int printed_line = -1;
   for(i = 0; i<nlines; i++)
      {  
      /* Skip over lines that do not match */
      if (!match (line[i], search_string)) continue;

      /* Update the printed line */
      printed_line++;

      if (highlight == printed_line) /* Highlight the present choice */
         {  
	 /* Set referse video */
	 attron(A_REVERSE); 

	 /* Print the line */
	 mvprintw(y, x, "%s", line[i]);
         clrtoeol();

	 /* Turn off reverse video */
         attroff(A_REVERSE);

	 /* Save this as the highlighted line */
	 highlighted_line = i;
         }
      
      else
	 {
         mvprintw(y, x, "%s", line[i]);
         clrtoeol();
	 }

      ++y;
      if (y >= nshown) break;
      }

   // Delete the rest
   while (y < nshown) 
      {
      /* Print the line */
      mvprintw(y, x, "");
      clrtoeol();

      ++y;
      }

   /* Draw it */
   refresh();
}

/*=========================================================================*/

int main (int argc, char *argv[])
   {
   /* Set up curses stuff */

   char *fname = argv[1];
   FILE *commandfile = fopen(argv[2], "w");
   FILE *outfile = stdout;

   /*-----------------------------------------*/
   /* Read data into a file */
   char *buf;
   int bufsize;
   read_input_file (fname, &buf, &bufsize);

   /* Now, go through and make pointers */
   int nlines;
   char **line;
   set_pointers (buf, bufsize, &line, &nlines);

   /* Reverse the pointers */
   int i;
   for (i=0; i<nlines/2; i++)
      {
      char *temp = line[i];
      line[i] = line[nlines-i-1];
      line[nlines-i-1] = temp;
      }

   /*-----------------------------------------*/
   /* Initialize the curses stuff */

   initscr();
   clear();
   noecho();
   cbreak();
   keypad(stdscr, TRUE);

   /*-----------------------------------------*/

   int choice = -1;
   int highlight = 0;
   int startline = 0;
   int nshown = 20;
   if (nshown > nlines) nshown = nlines;
   int endline = startline + nshown -1;

   /* Buffer for searching */
   char search_string[256];
   char *search_ptr = search_string;
   *search_ptr = '\0';

   /* Print out the first set of lines from the file */
   print_lines (line, highlight, startline, endline);

   while(1)
   {  
      int c = getch();

      if (mode == STANDARD_MODE)
         switch(c)
            {  
            case KEY_UP: case UP_KEY:
               if(highlight >= 1)
                  --highlight;
               break;

            case KEY_DOWN: case DOWN_KEY:
               if(highlight < nlines-1)
                  ++highlight;
               break;
   
	    case KEY_ENTER:
            case 10: 
               choice = highlight;
               break;
   
            case ABORT_KEY: case ABORT_KEY2:
               choice = -ABORT_KEY;
               break;
   
            case SEARCH_KEY: 
	       {
               mode = SEARCH_MODE;
               search_ptr = search_string;
               *search_ptr = '\0';
	       highlight = 0;
   
               break;
	       }
   
            default:
	       {
               break;
	       }
            }
   
      /* Search Mode */
      else
         switch(c)
            {  
            case KEY_UP: 
               if(highlight >= 1)
                  --highlight;
               break;

            case KEY_DOWN: 
               if(highlight < nlines-1)
                  ++highlight;
               break;

	    case KEY_ENTER:
            case 10:
               choice = highlight;
               break;

            case KEY_END:
	       {
               mode = STANDARD_MODE;
   
               break;
	       }

	    case KEY_DC:
	    case KEY_LEFT:
	    case KEY_BACKSPACE:
	       {
	       if (search_ptr > search_string)
	          *(--search_ptr) = '\0';
	       break;
	       }
   
            default:
	       {
               *search_ptr = c;
               *(++search_ptr) = '\0';
   
               break;
	       }
            }
   
      /* Exit if choice is to exit */
      if (choice == highlight) break;
      if (choice == -ABORT_KEY) break;

	 {
         /* If we go out of range, then change the range */
         if (highlight > endline)
            {
            startline = startline + 3*nshown/4;
            endline = startline + (nshown - 1);

            /* See we have not gone too far */
            if (endline >= nlines)
	       {
               endline = nlines-1;
	       startline = endline - (nshown - 1);
	       }
            }

         if (highlight < startline)
            {
            startline = startline - 3*nshown/4;
            endline = startline + (nshown - 1);
   
            /* See we have not gone too far */
            if (startline < 0)
	       {
               startline = 0;
	       endline = nshown-1;
	       }
            }

         /* Now, redraw the line */
         if (mode == SEARCH_MODE)
	    print_search_lines (line, highlight, search_string, nlines, nshown);

	 else
            print_lines (line, highlight, startline, endline);
         }
   }

   /* Finished with curses */
   endwin();

   /* Print out the line */
   if (choice == -ABORT_KEY)
      fprintf (commandfile, ".\n");
   else
      fprintf (commandfile, "%s\n", line[highlighted_line]);

   fclose (commandfile);

   return 0;
   }
