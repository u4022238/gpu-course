/* %W% %G% */
/* 
 * This gives a simple editor for command selection
 * 
 * To compile :
 *    cc edit_commands.c -lcurses -o edit_commands
 */

/* Comments
   8/26/99 : RIH changed fopen(..., "r") to fopen (..., "rb").
             because fseek does not work properly with files opened in text
	     mode
*/

// #define DEBUG
#define _CURSES_
#define _CBREAK_
#define CHTYPE int
#include <stdio.h>
#ifdef _CURSES_
#   include <ncurses.h>
#else
    typedef int WINDOW;
    WINDOW *stdscr;
#   define TRUE 1
#   define FALSE 0
#endif

#define CR 10
#define LF 13
#define CTRL_D 4
#define UP_KEY     'j'
#define UP_KEY2    's'
#define DOWN_KEY   'k'
#define DOWN_KEY2  'd'
#define ACCEPT_KEY 'a'
#define ABORT_KEY  'x'
/* #define UP_KEY   '\016' */
/* #define DOWN_KEY '\020' */

// Forward declarations
void find_next_line_start (FILE *infile);
void find_prev_line_start (FILE *infile);

FILE *outfile, *infile, *commandfile;

int current_line_start = 0;
int last_record = 0;

#ifdef _CURSES2_
wputstring (WINDOW *w, char *str)
   {
   char *p;
   for (p=str; *p; p++)
      waddch (w, *p);
   wrefresh (w);
   }

void print_line (FILE *infile)
   {
   int n;

   /* Go to a new line and clear it */
   addch(LF);
   clrtoeol();

   while (1)
      {
      /* Get the character */
      n = getc(infile);
      if (n == EOF || n == CR)
	      break;
      else addch (n);
      }

   /* Draw it */
   refresh();
   }
#else

void print_line (FILE *infile)
   {
   int n;

#ifdef DEBUG
   fprintf (outfile, "LINE:");
#endif

   /* Go to a new line and clear it */
   while (1)
      {
      /* Get the character */
      n = getc(infile);
      if (n == EOF || n == CR || n == LF)
	      break;
      else putc (n, outfile);
      }

#ifdef DEBUG
   putc (LF, outfile);
#endif
   //fprintf (outfile, " : ");
   fflush (outfile);

   // Return to the current position
   // printf ("Seeking to %ld\n", (long) current_line_start);
   fseek (infile, (long)current_line_start, SEEK_SET);
   }

#endif

void print_current_line (FILE *infile, FILE *outfile)
   {
   /* Transfers a line from one file to another */

#ifdef DEBUG
   fprintf (outfile, "Printing current line (offset %d):\n", 
   	current_line_start);
   fflush (outfile);
#endif

   /* Find the start of the file */
   // printf ("Seeking to %ld\n", (long) current_line_start);
   fseek (infile, (long)current_line_start, SEEK_SET);

   /* Transfer one line */
   /* Look for the next line feed */
   while (1)
      {
      /* Get the next character */
      int n = getc (infile);

      /* If we reach EOF, then we do not move current_line_start */
      if (n == EOF || n == CR || n == LF) break;

      /* Output the character */
      putc (n, outfile);
      }

   /* End with a line feed */
   putc ('\n', outfile);
   }

void find_last_line_start (FILE *infile)
   {
   int n;
   int CRpending = 1;

   /* Go to the start of the file */
   // printf ("Seeking to %ld\n", 0);
   fseek (infile, (long)0, SEEK_SET);
   last_record = 0;

   /* Look for the next line feed */
   while (1)
      {
      /* Get the next character */
      n = getc (infile);

      /* If we reach EOF, then we do not move current_line_start */
      if (n == EOF) break;

      /* If we have just passed a carriage return, then this is the start 
       * of the line */
      if (CRpending)
	      {
	      int position = ftell (infile);
	      last_record = position-1;
	      CRpending = 0;
	      }

      /* If we find a carriage-return, then go one character past it */
      if (n == CR)
	      {
	      int position = ftell (infile);
  	      CRpending = 1;
	      }
      }

   /* Set last line */
   current_line_start = last_record;

   /* Since this does not seem to work, we go to previous and then
    * next line */
   find_prev_line_start(infile);
   find_next_line_start(infile);

   /* Seek to the start of the next line */
   /* fseek (infile, (long)current_line_start, SEEK_SET); */
   }

void find_next_line_start (FILE *infile)
   {
   int n;

   /* Go to the start of the current line */
   // printf ("Seeking to %ld\n", (long) current_line_start);
   fseek (infile, (long)current_line_start, SEEK_SET);

   /* Look for the next line feed */
   while (1)
      {
      /* Get the next character */
      n = getc (infile);

      /* If we reach EOF, then we do not move current_line_start */
      if (n == EOF) break;

      /* If we find a carriage-return, then go one character past it */
      if (n == CR)
	      {
	      int position = ftell (infile);
	      if (position <= last_record)
	         current_line_start = position;
	      break;
	      }
      }

   /* Seek to the start of the next line */
   // printf ("Seeking to %ld\n", (long) current_line_start);
   fseek (infile, (long)current_line_start, SEEK_SET);
   }

void find_prev_line_start (FILE *infile)
   {
   int n;
   int count;
   int lastCR = -1;

   /* Go to the start of the current line */
   // printf ("Seeking to %ld\n", 0);
   fseek (infile, (long)0, SEEK_SET);

   /* Read to the present position, looking for a carriage return */
   for (count = 0; count < current_line_start-1; count++)
      {
      /* Get the next character */
      n = getc (infile);

      /* We actually should not find an EOF */
      if (n == EOF) break;

      /* If we find a CR, then record it */
      if (n == CR) 
	      {
	      lastCR = ftell (infile) -1;
	      }
      }

   /* Set to one character past CR */
   current_line_start = lastCR + 1;

   /* Seek to the start of the next line */
   // printf ("Seeking to %ld\n", (long) current_line_start);
   fseek (infile, (long)current_line_start, SEEK_SET);
   }

void print_prev_line (FILE *infile)
   {
#ifdef DEBUG
   fprintf (outfile, "Printing previous line (offset %d):\n", 
   	current_line_start);
   fflush (outfile);
#endif
   find_prev_line_start (infile);
   print_line (infile);
   }

void print_next_line (FILE *infile)
   {
#ifdef DEBUG
   fprintf (outfile, "Printing next line (offset %d):\n", 
   	current_line_start);
   fflush (outfile);
#endif
   find_next_line_start (infile);
   print_line (infile);
   }

void print_last_line (FILE *infile)
   {
#ifdef DEBUG
   fprintf (outfile, "Printing last line (offset %d):\n", 
    	current_line_start);
   fflush (outfile);
#endif
   find_last_line_start (infile); 
   print_line (infile);
   }

int main (int argc, char *argv[])
   {
   /* Set up curses stuff */
#ifdef _CURSES_
   WINDOW *awin, *thewin;

   filter ();
   awin = initscr();
   thewin = stdscr;
   /* thewin = newwin(0, 0, LINES-2, 0); */ /* Is this right -- Nov 2015 RIH */ 
#endif

   infile = fopen (argv[1], "rb");
   commandfile = fopen (argv[2], "w");
   outfile = stdout;

#ifdef _CURSES_
   /* Set cbreak mode */
   cbreak ();
   noecho (); 
   scrollok(thewin, TRUE);
   clearok (thewin, FALSE);
   clearok (stdscr, FALSE);
   nl();

   /* Clear the window */
   wrefresh (thewin);
#endif

   /* Find the last character in the file */
   /* fseek (infile, (long)0, SEEK_END); */
   /* end_of_input = ftell(infile); */

   /* Find the last line */
#ifdef DEBUG
   fprintf (outfile, "About to print line\n");
#endif
   print_last_line (infile);
#ifdef DEBUG
   fprintf (outfile, "Printed line\n"); 
   fflush (outfile);
#endif

   while (1)
      {
      /* Read characters from the input and output them */
      int n = getchar();

#ifdef DEBUG
      fprintf (outfile, "Got character %d = \"%c\"\n", n, (unsigned char)n);
      fflush (outfile);
#endif

      if (n == ABORT_KEY) 
	      break;
      else if (n == ACCEPT_KEY)
	      {
	      print_current_line (infile, commandfile);
	      break;
	      }
      else if (n == UP_KEY || n == UP_KEY2) find_next_line_start (infile);
      else if (n == DOWN_KEY || n == DOWN_KEY2) find_prev_line_start (infile);
      else if (n == CR || n == LF) 
	      {
	      /* Alternative accept */
	      print_current_line (infile, commandfile);
	      break;
	      }

      putc (LF, outfile);

#ifdef _CBREAK_
      // putc (n, outfile);
      putc (CR, outfile);
      putc (LF, outfile);
#endif
      print_line(infile);

      /* if (n == LF) wechochar(thewin, CR); */
      /* else wechochar (thewin, n); */

      }

#ifdef _CURSES_
   /* Get rid of the last line */
   addch (LF);
   clrtoeol();
   refresh();

   /* Finished with curses */
   endwin();
#endif

   return 0;
   }
