#include <ncurses.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int wstartow = 25;
int wstartcol = 10;
int statusy = 30;
int statusx = 10;

char LF = '\012';
char CR = '\015';

static int CONTINUE_TRUE = 1;
static int CONTINUE_FALSE = 2;

static char *FAILFILE = "fail-file.txt";
static char *PASSFILE = "pass-file.txt";

int get_file_size(char *fname)
   {
   /* First, open the file */
   int fd = open (fname, O_RDONLY);
   if (fd == 0)
      {
      fprintf (stderr, "Can not open file \"%s\" for read\n", fname);
      exit (1);
      }

   /* Next, do an lseek to find the size of the file */
   int size = lseek (fd, 0L, SEEK_END);

   /* Close the file */
   close (fd);
   
   return size;
   }

void read_input_file (char *fname, char **fbuf, int *fsize)
   {
   /* Reads in the input file and puts it in memory */

   /* First, get the file size */
   int size = get_file_size (fname);
   *fsize = size;

   /* Make a buffer of the right size */
   char *buf = (char *)malloc (size+2);
   if (buf == 0)
      {
      fprintf (stderr, "Can not allocate %d bytes for buffer\n", size+1);
      exit(1);
      }
   *fbuf = buf;

   fprintf (stderr, "File is of size %d\n", size);

   /* Open the file again */
   int fd = open (fname, O_RDONLY);
   if (fd == 0)
      {
      fprintf (stderr, "Can not open file \"%s\" for read\n", fname);
      exit (1);
      }

   fprintf (stderr, "About to read %d bytes into buffer \n", size);

   /* Read it in */
   read (fd, buf, size);

   fprintf (stderr, "Successfully read\n");

   /* Put a null character on the end */
   buf[size] = '\0';
   buf[size+1] = '\0';

   fprintf (stderr, "Initialized buffer\n");
   /* fprintf (stderr, "%s\n", buf); */

   /* Close the file */
   close (fd);
   }

void set_pointers (
	char *buf, 
	int bufsize, 
	char ***plang1, 
	char ***plang2, 
	int *pnlines
	)
   {
   /* Sets up pointers to the two parts of each line */
   int i;

   /* First, count the lines */
   int nlines = 0;
   for (i=0; i<bufsize; i++)
      if (buf[i] == '\n') nlines++;
   *pnlines = nlines;

   fprintf (stderr, "File has %d lines\n", nlines);

   /* Next, allocate the buffers */
   char **lang1 = (char **)malloc(nlines * sizeof(char *));
   char **lang2 = (char **)malloc(nlines * sizeof(char *));
   if (lang1 == 0 || lang2 == 0)
      {
      fprintf (stderr, "Can not allocate space for tables\n");
      exit(1);
      }
   *plang1 = lang1;
   *plang2 = lang2;

   /* Now, go through and fill them */
      {
      char *cursor = buf;	/* Position in the buffer */
      for (i=0; i<nlines; i++)
	 {
	 /* This is the position of the cursor */
	 lang1[i] = cursor;

         /* Now, go on to find the next LF or CR */
  	 while (*cursor != LF && *cursor != CR && *cursor != '\0') cursor++;

	 /* Change it to a null */
	 *cursor = '\0';

         /* Also any other CR or LF */
	 cursor += 1;
	 while (*cursor == LF || *cursor == CR) 
	   {
	   *cursor = '\0';
	   cursor++;
	   }

	 /* Now, this is the start of the next line, so continue */
	 }

      /* Next, we want to set the colons to nulls and store them */
      for (i=0; i<nlines; i++)
         {
         /* Set cursor to start of line */
	 char *cursor = lang1[i];

         /* Seek for a colon */
	 lang2[i] = lang1[i];
	 while (*cursor != '\0')
	    {
	    /* Store the position of character after cursor */
	    if (*cursor == ':')
	       {
	       *cursor = '\0';
	       lang2[i] = cursor+1;
	       break;
	       }

  	    /* Go to the next character */
 	    cursor++;
	    }

	 /* Signal an error if no colon */
	 if (lang2[i] == lang1[i])
	    fprintf (stderr, "No colon in line %d\n", i);
         }
      }
   }

#include <time.h>
void permute_words (char **lang1, char **lang2, int nlines)
   {
   /* Permute the words */

   /* Get a random seed for the number generator */
   long seed = time(0);
   srandom(seed);

   int i;
   for (i=0; i<nlines; i++)
      {
      /* Get a random number between 0 and nlines-i */
      int randn = random() % (nlines-i);

      /* Swap to the i + randn-th entry into the i-th position */
      char *temp = lang1[i];
      lang1[i] = lang1[i+randn];
      lang1[i+randn] = temp;

      temp = lang2[i];
      lang2[i] = lang2[i+randn];
      lang2[i+randn] = temp;
      }
   }


void show_status (int numwrong, int numright, int numleft)
   {
   int statusrow = LINES-2;
   int right = COLS - 12;
   int middle = (COLS - 12)/2;

   mvprintw (statusrow, 0,      "# wrong : %d", numwrong);
   clrtoeol();
   mvprintw (statusrow, right,  "# known : %d", numright);
   mvprintw (statusrow, middle, "# left : %d",  numleft);

   /* Do not refresh */
   }


int run_tests (char *fname)
   {
   /* Statistics */
   int numright = 0;
   int numwrong = 0;
   int nlines;

   /* Read data into a file */
   char *buf;
   int bufsize;
   read_input_file (fname, &buf, &bufsize);

   /* Now, go through and make pointers */
   char **lang1, **lang2;
   set_pointers (buf, bufsize, &lang1, &lang2, &nlines);

   /* Now, permute the words */
   permute_words (lang1, lang2, nlines);

   /* Open some files for failure and success */
   FILE *failfile = fopen (FAILFILE, "w");
   FILE *passfile = fopen (PASSFILE, "a");
   if (failfile == 0 || passfile == 0)
      {
      fprintf (stderr, "Unable to open output files\n");
      exit(1);
      }

   /* Initialize */
   initscr();
   cbreak();
   keypad(stdscr, TRUE);

   /* Print message in top corner */
   printw("Press F1 to exit");
   clrtoeol();
   refresh();

   /* Main loop for handling events */
   int wordcount = 0;

   /* Print a word */
   mvprintw (wstartow, wstartcol, "%s", lang1[wordcount]);
   clrtoeol();
   show_status (numwrong, numright, nlines-wordcount-1);
   refresh();
      
   /* Get a response */
   int ch;
   while (1)
   {
      /* Assume we will show a new word */
      bool show_new_word = false;

      /* Get the character */
      int ch = getch();

      /* Handle the stop condition */
      if (ch == KEY_F(1))
	 {
	 /* Print out all the rest of the words as if failed */
	 int i;
	 for (i=wordcount; i<nlines; i++)
   	    fprintf (failfile, "%s:%s\n", lang1[i], lang2[i]);

	 /* Exit */
	 endwin();
	 fclose (failfile);
	 fclose (passfile);
	 return (CONTINUE_FALSE);
	 }

      /* Otherwise, handle the other conditions */
      switch(ch)
      {  
        case KEY_LEFT:
   	   fprintf (failfile, "%s:%s\n", lang1[wordcount], lang2[wordcount]);
	   show_status (++numwrong, numright, nlines-wordcount-1);
	   show_new_word = true;
           break;

         case KEY_RIGHT:
   	   fprintf (passfile, "%s:%s\n", lang1[wordcount], lang2[wordcount]);
	   show_status (numwrong, ++numright, nlines-wordcount-1);
	   show_new_word = true;
           break;

         case KEY_DOWN:
           mvprintw (wstartow, wstartcol, "%s : %s", 
		lang1[wordcount], lang2[wordcount]);
           clrtoeol();
	   refresh();
           break;   
      }

      /* If we are not showing the new word, then we can continue */
      if (! show_new_word) continue;

      /* Count the words */
      wordcount++;

      /* Have we finished all the words */
      if (wordcount >= nlines)
         {
	 /* Are we completely finished */
         if (numwrong == 0)
	    {
	    /* Put a message and wait for confirmation */
            mvprintw (wstartow, wstartcol, 
		"FINISHED: Congratulations (press any key to exit)");
	    clrtoeol();
	    refresh();
	    getch();

	    /* Clean up and return */
            endwin ();
            fclose (failfile);
            fclose (passfile);
            return CONTINUE_FALSE;
	    }

	 /* Put up finishing message */
         mvprintw (wstartow, wstartcol, "FINISHED: Continue? <y/n>");
         clrtoeol();
         refresh();
	    
	 /* Get y or n answer */
	 while (1)
 	    {
	    /* Get the character and see if continue or not */
            int ch = getch();
	    if (ch == 'y')
	       {
	       endwin ();
	       fclose (failfile);
	       fclose (passfile);
	       return CONTINUE_TRUE;
	       }
	    else if (ch == 'n')
	       {
	       endwin ();
	       fclose (failfile);
	       fclose (passfile);
	       return CONTINUE_FALSE;
	       }
	    }
         }

      /* Otherwise, show the next word */
      else
         {
         mvprintw (wstartow, wstartcol, "%s", lang1[wordcount]);
         clrtoeol();
	 refresh();
         }
   }
}

int main(int argc, char *argv[])
   {
   int i;

   /* Get the file name */
   char *fname = argv[1];

   /* Go into a loop of running tests */
   while (1)
      {
      /* Run the program and get the return value */
      int returnval = run_tests (fname);

      /* Either exit or repeat */
      if (returnval == CONTINUE_FALSE)
	 return 0;

      else
	 fname = FAILFILE;
      }
   }
