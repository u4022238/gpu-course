/* Sorts a file of words, retaining just the last occurrence of each word */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

static int initial_buffersize = 2;
#define TRUE 1
#define FALSE 0

/* Order of sorting */
int inverse_order = 0;

void *resize_buffer (void *ptr, int *buffersize)
   {
   /* Reallocates the buffer, checking for success, and exiting on failure */
   void *newptr;

   /* Double the size of the buffer */
   *buffersize *= 2;

   /* Reallocate memory */
   newptr = realloc(ptr, *buffersize);

   /* Check success */
   if (newptr == NULL)
      {
      fprintf (stderr, "realloc : Out of memory\n");
      exit (1);
      }

   /* Return the new buffer */
   return newptr;
   }

int wordcmp (const void *pw1, const void *pw2)
   {
   char *w1 = *(char **)pw1, *w2 = *(char **)pw2;
   int n = strcmp (w1, w2);
   if (n != 0) return n;
   else if (inverse_order)
      return w2-w1;
   else return w1 - w2;
   }

int main (int argc, char *argv[])
   {
   /* Sort the file of words */
   
   int i;
   int nwords;
   char *buffer = NULL;
   char *ptr;
   int buffersize = initial_buffersize;
   char **words;	/* An array of words */
   char **owords;	/* Sorted array of words */
   int skipping;

   /* Check the arguments */
   /* Skip over the program name */
   argv++; argc--;

   while (argc > 0)
      {
      if (argv[0][0] != '-') break;

      /* parse the option */
      switch (argv[0][1])
         {
         case 'i' :
            {
            /* Normalize the points */
 	    inverse_order = TRUE;
            break;
            }
         default :
            {
            fprintf (stderr, "comb : Unknown option \"%s\"", argv[0]);
            exit (1);
            }
         }

      /* Skip to the next argument */
      argv++; argc--;
      }

   /* Initially size the buffer */
   buffer = resize_buffer (buffer, &buffersize);

   /* Now read in the file */
      {
      int nread = 0;  /* Number of characters transferred to buffer */
      ptr = buffer;
      while (1)
         {
         /* Read a character */
         register int n = getchar ();

         /* Stop on EOF */
	 if (n == EOF) break;

         /* Test to see if there is still room in the buffer */
	 /* Always leave room for one more character */
	 if (nread >= buffersize-1)
	    {
 	    /* Reallocate the buffer, and reposition the insertion point */
	    int pos = ptr-buffer;
	    buffer = resize_buffer (buffer, &buffersize);
	    ptr = buffer + pos;
	    }
	
	 /* Put the character in the buffer */
	 *ptr = n;
	 ptr++;
	 nread ++;
         }

      /* Put a null character at EOF */
      *ptr = '\0';
      }

   /* Now, set up an array of pointers to the words */
   
   /* First count the words */
   nwords = 0;
   skipping = TRUE;
   for (ptr = buffer; *ptr; ptr++)
      {
      if (skipping && ! isspace (*ptr))
         {
         nwords ++;
         skipping = FALSE;
         }
      else if (isspace (*ptr)) 
         skipping = TRUE;
      }

   /* Now allocate space for the words */
   words = (char **) malloc (nwords * sizeof (char *));
   if (words == NULL)
      {
      fprintf (stderr, "realloc : Out of memory\n");
      exit (1);
      }

   /* Put the words in the array */
   nwords = 0;
   skipping = TRUE;
   for (ptr = buffer; *ptr; ptr++)
      {
      if (skipping && ! isspace (*ptr))
         {
         words[nwords++] = ptr;
         skipping = FALSE;
         }
      else if (isspace (*ptr)) 
	 {
	 *ptr = '\0';
         skipping = TRUE;
	 }
      }

   /* Now allocate space for a second set of pointers */
   owords = (char **) malloc (nwords * sizeof (char *));
   if (owords == NULL)
      {
      fprintf (stderr, "realloc : Out of memory\n");
      exit (1);
      }

   /* Make them point at the words */
   for (i=0; i<nwords; i++)
      owords[i] = words[i];

   /* Sort the index */
   qsort (owords, nwords, sizeof(char *), wordcmp);

   /* Now, go through and get rid of duplicates */
      {
      char *lastword = owords[0];
      for (i=1; i<nwords; i++)
	 if (strcmp(owords[i], lastword) == 0)
	    *(owords[i]) = '\0';
	 else
	    lastword = owords[i];
      }

   /* Write out the file */
   for (i=0; i<nwords; i++)
      {
      if (*words[i] != '\0')
	 {
         for (ptr=words[i]; *ptr; ptr++)
	    putchar(*ptr);
         putchar ('\n');
	 }
      }

   /* Return success */
   return 0;
   }
