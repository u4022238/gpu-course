#ifndef _pngreadfile_cc_dcl_
#define _pngreadfile_cc_dcl_
#include "Utilities/cvar.h"


FUNCTION_DECL (int read_png_image, (
         FILE *fp, 
         Unsigned_char_matrix &im,
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type  = NULL , 
         int *p_interlace_type  = NULL 
         ) );

FUNCTION_DECL (int read_png_header, (
         FILE *fp, 
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type  = NULL , 
         int *p_interlace_type  = NULL 
         ) );

FUNCTION_DECL (int read_png_header, (
         const char *fname,
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type  = NULL , 
         int *p_interlace_type  = NULL 
         ) );

#endif
