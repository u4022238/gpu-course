#ifndef _pngwritefile_cc_dcl_
#define _pngwritefile_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include "Utilities_CC/utilities_CC.h"
#include <PNG/png.h>
/* */

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   unsigned char **R,
	   unsigned char **G,
	   unsigned char **B,
	   png_uint_32 height,
	   png_uint_32 width
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   unsigned char **R,
	   png_uint_32 height,
	   png_uint_32 width
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   Unsigned_char_matrix &im
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   Unsigned_char_matrix &R,
      Unsigned_char_matrix &G,
      Unsigned_char_matrix &B
	   ));

#endif
