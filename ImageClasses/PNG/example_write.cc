
#include <Utilities_CC/utilities_CC.h>
// #include <Tjinterface/imageio_dcl.h>
// #include <Tjinterface/imageio_color_dcl.h>
#include "PNG/png.h"

#define PNG_SUCCESS				0
#define ERROR_CAN_NOT_OPEN_FILE			1
#define ERROR_CAN_NOT_CREATE_STRUCTURE 		2
#define ERROR_CAN_NOT_SET_JUMP 			3

#if 0
void my_error_fn(png_structp png_ptr,
        png_const_charp error_msg)
   {
   printf ("Error\n");
   }

void my_warning_fn(png_structp png_ptr,
        png_const_charp warning_msg)
   {
   printf ("Warning\n");
   }
#endif

/* write a png file */
int my_write_png(
	char *file_name, 
	Unsigned_char_matrix R,
	Unsigned_char_matrix G,
	Unsigned_char_matrix B
	)
{
   int i, j, k;
   const png_uint_32 height = R.idim();
   const png_uint_32 width = R.jdim();
   const png_uint_32 bit_depth = 8;
   const int bytes_per_pixel = 3;
   const int nbytes = height*width*bytes_per_pixel;
   int nrows = height;
   int ncols = width;

   /* To hold the image */
   png_byte  *image        = new png_byte[nbytes];
   png_bytep *row_pointers = new png_bytep [height];

   // Fill out the image
   int count = 0;
   for (i=0; i<nrows; i++)
      for (j=0; j<ncols; j++)
	 {
	 // Get the values of the pixels
	 png_byte r = R[i][j];
	 png_byte g = G[i][j];
	 png_byte b = B[i][j];
	
	 // Assign them
	 image[count++] = r;
	 image[count++] = g;
	 image[count++] = b;
	 }

   /* Write a random png image */
   FILE *fp;
   png_structp png_ptr;
   png_infop info_ptr;

   /* open the file */
   fp = fopen(file_name, "wb");
   if (fp == NULL)
      return (ERROR_CAN_NOT_OPEN_FILE);

   /*------------------------------------------- */
   /* Set up the png_ptr and info_ptr structures */
   // png_voidp my_error_ptr = png_get_error_ptr(png_ptr);
   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
      NULL, NULL, NULL);
      //(png_voidp) my_error_ptr, my_error_fn, my_warning_fn);

   if (png_ptr == NULL)
      {
      fclose(fp);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /* Allocate/initialize the image information data.  REQUIRED */
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
      {
      fclose(fp);
      png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /*---------------------------------------------------------------*/
   /* Set error handling.   */
   if (setjmp(png_jmpbuf(png_ptr)))
      {
      /* If we get here, we had a problem reading the file */
      fclose(fp);
      png_destroy_write_struct(&png_ptr, &info_ptr);
      delete [] image;
      delete [] row_pointers;
      return (ERROR_CAN_NOT_SET_JUMP);
      }

   /* set up the output control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   /* Set the image header */
   png_set_IHDR(png_ptr, info_ptr, 
	width, 
	height, 
	bit_depth, 
	PNG_COLOR_TYPE_RGB,
      	PNG_INTERLACE_NONE, 
	PNG_COMPRESSION_TYPE_DEFAULT, 
	PNG_FILTER_TYPE_DEFAULT);

   /* Write the file header information */
   png_write_info(png_ptr, info_ptr);

   /* Get the image data */
   for (k=0; k<height; k++)
      row_pointers[k] = image + k*width*bytes_per_pixel;

   /* Write the image */
   png_write_image(png_ptr, row_pointers);

   /* clean up after the write, and free any memory allocated */
   png_destroy_write_struct(&png_ptr, &info_ptr);

   /* close the file */
   fclose(fp);

   /* Delete the temporaries */
   delete [] image;
   delete [] row_pointers;

   /* that's it */
   return (PNG_SUCCESS);
}

int main (int argc, char *argv[])
   {
   // Get the image input and output names
   char *inputname = argv[1];
   char *outputname = argv[2];

   Unsigned_char_matrix R, G, B;

   if (is_color_image (inputname))
      {
      // Read in the three images
      read_pix_band_to_array (1, inputname, R);
      read_pix_band_to_array (2, inputname, G);
      read_pix_band_to_array (3, inputname, B);
      }
   else
      {
      read_pix_to_array (inputname, R);
      read_pix_to_array (inputname, G);
      read_pix_to_array (inputname, B);
      }

   // Write the image to the output
   int result = my_write_png(outputname, R, G, B);
   printf ("my_write_png returned %d\n", result);
   return 0;
   }
