
/* SFILE_BEGIN */
#include "Utilities_CC/utilities_CC.h"
#include <PNG/png.h>
/* SFILE_END */

#define PNG_SUCCESS				         0
#define ERROR_CAN_NOT_OPEN_FILE			1
#define ERROR_CAN_NOT_CREATE_STRUCTURE	2
#define ERROR_CAN_NOT_SET_JUMP 			3

#if 0
void my_error_fn(png_structp png_ptr,
        png_const_charp error_msg)
   {
   printf ("Error\n");
   }

void my_warning_fn(png_structp png_ptr,
        png_const_charp warning_msg)
   {
   printf ("Warning\n");
   }
#endif

/* write a png file */
FUNCTION_DEF (int write_png_image, (
	   const char *file_name, 
	   unsigned char **R,
	   unsigned char **G,
	   unsigned char **B,
	   png_uint_32 height,
	   png_uint_32 width
	   ))
   {
   // RGB image - it writes it with the pixels from the RGB bands interleaved
   const png_uint_32 bit_depth = 8;
   const int bytes_per_pixel = 3;
   const int nbytes = height*width*bytes_per_pixel;
   int nrows = height;
   int ncols = width;

   /* To hold the image */
   png_byte  *image        = new png_byte[nbytes];
   png_bytep *row_pointers = new png_bytep [height];

   // Fill out the image
   int count = 0;
   for (unsigned int i=0; i<nrows; i++)
      for (unsigned int j=0; j<ncols; j++)
	      {
	      // Get the values of the pixels
	      png_byte r = R[i][j];
	      png_byte g = G[i][j];
	      png_byte b = B[i][j];
	
	      // Assign them
	      image[count++] = r;
	      image[count++] = g;
	      image[count++] = b;
	      }

   /* Write a random png image */
   FILE *fp;
   png_structp png_ptr;
   png_infop info_ptr;

   /* open the file */
   fp = fopen(file_name, "wb");
   if (fp == NULL)
      return (ERROR_CAN_NOT_OPEN_FILE);

   /*------------------------------------------- */
   /* Set up the png_ptr and info_ptr structures */
   // png_voidp my_error_ptr = png_get_error_ptr(png_ptr);
   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
      NULL, NULL, NULL);
      //(png_voidp) my_error_ptr, my_error_fn, my_warning_fn);

   if (png_ptr == NULL)
      {
      fclose(fp);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /* Allocate/initialize the image information data.  REQUIRED */
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
      {
      fclose(fp);
      png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /*---------------------------------------------------------------*/
   /* Set error handling.   */
   if (setjmp(png_jmpbuf(png_ptr)))
      {
      /* If we get here, we had a problem reading the file */
      fclose(fp);
      png_destroy_write_struct(&png_ptr, &info_ptr);
      delete [] image;
      delete [] row_pointers;
      return (ERROR_CAN_NOT_SET_JUMP);
      }

   /* set up the output control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   /* Set the image header */
   png_set_IHDR(png_ptr, info_ptr, 
	width, 
	height, 
	bit_depth, 
	PNG_COLOR_TYPE_RGB,
   PNG_INTERLACE_NONE, 
	PNG_COMPRESSION_TYPE_DEFAULT, 
	PNG_FILTER_TYPE_DEFAULT);

   /* Write the file header information */
   png_write_info(png_ptr, info_ptr);

   /* Get the image data */
   for (unsigned int k=0; k<height; k++)
      row_pointers[k] = image + k*width*bytes_per_pixel;

   /* Write the image */
   png_write_image(png_ptr, row_pointers);

   /* clean up after the write, and free any memory allocated */
   png_destroy_write_struct(&png_ptr, &info_ptr);

   /* close the file */
   fclose(fp);

   /* Delete the temporaries */
   delete [] image;
   delete [] row_pointers;

   /* that's it */
   return (PNG_SUCCESS);
   }

FUNCTION_DEF (int write_png_image, (
	   const char *file_name, 
	   unsigned char **R,
	   png_uint_32 height,
	   png_uint_32 width
	   ))
   {
   // Grey scale image
   const png_uint_32 bit_depth = 8;
   const int bytes_per_pixel = 1;
   const int nbytes = height*width*bytes_per_pixel;
   int nrows = height;
   int ncols = width;

   /* To hold the image */
   png_byte  *image        = new png_byte[nbytes];
   png_bytep *row_pointers = new png_bytep [height];

   // Fill out the image
   int count = 0;
   for (unsigned int i=0; i<nrows; i++)
      for (unsigned int j=0; j<ncols; j++)
	      image[count++] = R[i][j];

   /* Create and open the file */
   FILE *fp;
   png_structp png_ptr;
   png_infop info_ptr;

   /* open the file */
   fp = fopen(file_name, "wb");
   if (fp == NULL)
      return (ERROR_CAN_NOT_OPEN_FILE);

   /*------------------------------------------- */
   /* Set up the png_ptr and info_ptr structures */
   // png_voidp my_error_ptr = png_get_error_ptr(png_ptr);
   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
      NULL, NULL, NULL);
      //(png_voidp) my_error_ptr, my_error_fn, my_warning_fn);

   if (png_ptr == NULL)
      {
      fclose(fp);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /* Allocate/initialize the image information data.  REQUIRED */
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
      {
      fclose(fp);
      png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
      return (ERROR_CAN_NOT_CREATE_STRUCTURE);
      }

   /*---------------------------------------------------------------*/
   /* Set error handling.   */
   if (setjmp(png_jmpbuf(png_ptr)))
      {
      /* If we get here, we had a problem reading the file */
      fclose(fp);
      png_destroy_write_struct(&png_ptr, &info_ptr);
      delete [] image;
      delete [] row_pointers;
      return (ERROR_CAN_NOT_SET_JUMP);
      }

   /* set up the output control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   /* Set the image header */
   png_set_IHDR(png_ptr, info_ptr, 
	width, 
	height, 
	bit_depth, 
	PNG_COLOR_TYPE_GRAY,
   PNG_INTERLACE_NONE, 
	PNG_COMPRESSION_TYPE_DEFAULT, 
	PNG_FILTER_TYPE_DEFAULT);

   /* Write the file header information */
   png_write_info(png_ptr, info_ptr);

   /* Get the image data */
   for (unsigned int k=0; k<height; k++)
      row_pointers[k] = image + k*width*bytes_per_pixel;

   /* Write the image */
   png_write_image(png_ptr, row_pointers);

   /* clean up after the write, and free any memory allocated */
   png_destroy_write_struct(&png_ptr, &info_ptr);

   /* close the file */
   fclose(fp);

   /* Delete the temporaries */
   delete [] image;
   delete [] row_pointers;

   /* that's it */
   return (PNG_SUCCESS);
   }

FUNCTION_DEF (int write_png_image, (
	   const char *file_name, 
	   Unsigned_char_matrix &im
	   ))
   {
   return write_png_image (file_name, im, im.idim(), im.jdim());
   }

FUNCTION_DEF (int write_png_image, (
	   const char *file_name, 
	   Unsigned_char_matrix &R,
      Unsigned_char_matrix &G,
      Unsigned_char_matrix &B
	   ))
   {
   return write_png_image (file_name, R, G, B, R.idim(), R.jdim());
   }



