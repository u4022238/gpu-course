#include <png.h>

    void write_row_callback(png_ptr, png_uint_32 row,
       int pass);
    {
      /* put your code here */
    }

png_write_image (char *file_name)
    {

    //--------------------------------------------
    // Open a file for write
    FILE *fp = fopen(file_name, "wb");
    if (!fp)
    {
       return (ERROR);
    }

    //----------------------------------------------
    // Define the png_structp and png_infop pointers

    png_structp png_ptr = png_create_write_struct
       (PNG_LIBPNG_VER_STRING, (png_voidp)user_error_ptr,
        user_error_fn, user_warning_fn);
    if (!png_ptr)
       return (ERROR);

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
       png_destroy_write_struct(&png_ptr,
         (png_infopp)NULL);
       return (ERROR);
    }

    //--------------
    // Set longjump
 
    if (setjmp(png_jmpbuf(png_ptr)))
    {
       png_destroy_write_struct(&png_ptr, &info_ptr);
       fclose(fp);
       return (ERROR);
    }

    //--------------------------
    // Set up the file for write
    png_init_io(png_ptr, fp);


    // Set up writing callback
    png_set_write_status_fn(png_ptr, write_row_callback);

You now need to fill in the png_info structure with all the data you
wish to write before the actual image.  Note that the only thing you
are allowed to write after the image is the text chunks and the time
chunk (as of PNG Specification 1.2, anyway).  See png_write_end() and
the latest PNG specification for more information on that.  If you
wish to write them before the image, fill them in now, and flag that
data as being valid.  If you want to wait until after the data, don't
fill them until png_write_end().  For all the fields in png_info and
their data types, see png.h.  For explanations of what the fields
contain, see the PNG specification.

Some of the more important parts of the png_info are:

    png_set_IHDR(png_ptr, info_ptr, width, height,
       bit_depth, color_type, interlace_type,
       compression_type, filter_method)

/* 
    width          - holds the width of the image
                     in pixels (up to 2^31).
    height         - holds the height of the image
                     in pixels (up to 2^31).
    bit_depth      - holds the bit depth of one of the
                     image channels.
                     (valid values are 1, 2, 4, 8, 16
                     and depend also on the
                     color_type.  See also significant
                     bits (sBIT) below).
    color_type     - describes which color/alpha
                     channels are present.
                     PNG_COLOR_TYPE_GRAY
                        (bit depths 1, 2, 4, 8, 16)
                     PNG_COLOR_TYPE_GRAY_ALPHA
                        (bit depths 8, 16)
                     PNG_COLOR_TYPE_PALETTE
                        (bit depths 1, 2, 4, 8)
                     PNG_COLOR_TYPE_RGB
                        (bit_depths 8, 16)
                     PNG_COLOR_TYPE_RGB_ALPHA
                        (bit_depths 8, 16)

                     PNG_COLOR_MASK_PALETTE
                     PNG_COLOR_MASK_COLOR
                     PNG_COLOR_MASK_ALPHA

    interlace_type - PNG_INTERLACE_NONE or
                     PNG_INTERLACE_ADAM7
    compression_type - (must be
                     PNG_COMPRESSION_TYPE_DEFAULT)
    filter_method  - (must be PNG_FILTER_TYPE_DEFAULT
                     or, if you are writing a PNG to
                     be embedded in a MNG datastream,
                     can also be
                     PNG_INTRAPIXEL_DIFFERENCING)
    */

    png_set_PLTE(png_ptr, info_ptr, palette,
       num_palette);

    /*
    palette        - the palette for the file
                     (array of png_color)
    num_palette    - number of entries in the palette
    */

    png_set_gAMA(png_ptr, info_ptr, gamma);

    /*
    gamma          - the gamma the image was created
                     at (PNG_INFO_gAMA)
    */

    png_set_sRGB(png_ptr, info_ptr, srgb_intent);

    /* 
    srgb_intent    - the rendering intent
                     (PNG_INFO_sRGB) The presence of
                     the sRGB chunk means that the pixel
                     data is in the sRGB color space.
                     This chunk also implies specific
                     values of gAMA and cHRM.  Rendering
                     intent is the CSS-1 property that
                     has been defined by the International
                     Color Consortium
                     (http://www.color.org).
                     It can be one of
                     PNG_sRGB_INTENT_SATURATION,
                     PNG_sRGB_INTENT_PERCEPTUAL,
                     PNG_sRGB_INTENT_ABSOLUTE, or
                     PNG_sRGB_INTENT_RELATIVE.

    */


    png_set_sRGB_gAMA_and_cHRM(png_ptr, info_ptr,
       srgb_intent);

    /*
    srgb_intent    - the rendering intent
                     (PNG_INFO_sRGB) The presence of the
                     sRGB chunk means that the pixel
                     data is in the sRGB color space.
                     This function also causes gAMA and
                     cHRM chunks with the specific values
                     that are consistent with sRGB to be
                     written.
    */

    png_set_iCCP(png_ptr, info_ptr, name, compression_type,
                      profile, proflen);

    /*
    name            - The profile name.
    compression     - The compression type; always
                      PNG_COMPRESSION_TYPE_BASE for PNG 1.0.
                      You may give NULL to this argument to
                      ignore it.
    profile         - International Color Consortium color
                      profile data. May contain NULs.
    proflen         - length of profile data in bytes.

    */

    png_set_sBIT(png_ptr, info_ptr, sig_bit);

    /*
    sig_bit        - the number of significant bits for
                     (PNG_INFO_sBIT) each of the gray, red,
                     green, and blue channels, whichever are
                     appropriate for the given color type
                     (png_color_16)
    */

    png_set_tRNS(png_ptr, info_ptr, trans, num_trans,
       trans_values);

    /*
    trans          - array of transparent entries for
                     palette (PNG_INFO_tRNS)
    trans_values   - graylevel or color sample values of
                     the single transparent color for
                     non-paletted images (PNG_INFO_tRNS)
    num_trans      - number of transparent entries
                     (PNG_INFO_tRNS)
    */

    png_set_hIST(png_ptr, info_ptr, hist);

    /*
                    (PNG_INFO_hIST)
    hist           - histogram of palette (array of
                     png_uint_16)
    */

    png_set_tIME(png_ptr, info_ptr, mod_time);

    /*
    mod_time       - time image was last modified
                     (PNG_VALID_tIME)
    */

    png_set_bKGD(png_ptr, info_ptr, background);

    /*
    background     - background color (PNG_VALID_bKGD)
    */

    png_set_text(png_ptr, info_ptr, text_ptr, num_text);

    /*
    text_ptr       - array of png_text holding image
                     comments
    text_ptr[i].compression - type of compression used
                 on "text" PNG_TEXT_COMPRESSION_NONE
                           PNG_TEXT_COMPRESSION_zTXt
                           PNG_ITXT_COMPRESSION_NONE
                           PNG_ITXT_COMPRESSION_zTXt
    text_ptr[i].key   - keyword for comment.  Must contain
                 1-79 characters.
    text_ptr[i].text  - text comments for current
                         keyword.  Can be NULL or empty.
    text_ptr[i].text_length - length of text string,
                 after decompression, 0 for iTXt
    text_ptr[i].itxt_length - length of itxt string,
                 after decompression, 0 for tEXt/zTXt
    text_ptr[i].lang  - language of comment (NULL or
                         empty for unknown).
    text_ptr[i].translated_keyword  - keyword in UTF-8 (NULL
                         or empty for unknown).
    num_text       - number of comments
    */

    png_set_sPLT(png_ptr, info_ptr, &palette_ptr,
       num_spalettes);

    /*
    palette_ptr    - array of png_sPLT_struct structures
                     to be added to the list of palettes
                     in the info structure.
    num_spalettes  - number of palette structures to be
                     added.
    */

    png_set_oFFs(png_ptr, info_ptr, offset_x, offset_y,
        unit_type);

    /*
    offset_x  - positive offset from the left
                     edge of the screen
    offset_y  - positive offset from the top
                     edge of the screen
    unit_type - PNG_OFFSET_PIXEL, PNG_OFFSET_MICROMETER
    */

    png_set_pHYs(png_ptr, info_ptr, res_x, res_y,
        unit_type);

    /*
    res_x       - pixels/unit physical resolution
                  in x direction
    res_y       - pixels/unit physical resolution
                  in y direction
    unit_type   - PNG_RESOLUTION_UNKNOWN,
                  PNG_RESOLUTION_METER
    */

    png_set_sCAL(png_ptr, info_ptr, unit, width, height)

    /*
    unit        - physical scale units (an integer)
    width       - width of a pixel in physical scale units
    height      - height of a pixel in physical scale units
                  (width and height are doubles)
    */

    png_set_sCAL_s(png_ptr, info_ptr, unit, width, height)

    /*
    unit        - physical scale units (an integer)
    width       - width of a pixel in physical scale units
    height      - height of a pixel in physical scale units
                 (width and height are strings like "2.54")
    */

    png_set_unknown_chunks(png_ptr, info_ptr, &unknowns,
       num_unknowns)

    /*
    unknowns          - array of png_unknown_chunk
                        structures holding unknown chunks
    unknowns[i].name  - name of unknown chunk
    unknowns[i].data  - data of unknown chunk
    unknowns[i].size  - size of unknown chunk's data
    unknowns[i].location - position to write chunk in file
                           0: do not write chunk
                           PNG_HAVE_IHDR: before PLTE
                           PNG_HAVE_PLTE: before IDAT
                           PNG_AFTER_IDAT: after IDAT
    */

    // Time conversion functions
    // png_convert_from_time_t()
    // png_convert_from_struct_tm()

    // PNG_TRANSFORM_IDENTITY      No transformation
    // PNG_TRANSFORM_PACKING       Pack 1, 2 and 4-bit samples
    // PNG_TRANSFORM_PACKSWAP      Change order of packed pixels to LSB first
    // PNG_TRANSFORM_INVERT_MONO   Invert monochrome images
    // PNG_TRANSFORM_SHIFT         Normalize pixels to the sBIT depth
    // PNG_TRANSFORM_BGR           Flip RGB to BGR, RGBA to BGRA
    // PNG_TRANSFORM_SWAP_ALPHA    Flip RGBA to ARGB or GA to AG
    // PNG_TRANSFORM_INVERT_ALPHA  Change alpha from opacity to transparency
    // PNG_TRANSFORM_SWAP_ENDIAN   Byte-swap 16-bit samples
    // PNG_TRANSFORM_STRIP_FILLER  Strip out filler bytes.

    // png_set_rows()

    png_write_png(png_ptr, info_ptr, png_transforms, NULL)
