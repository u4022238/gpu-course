#include <stdlib.h>
#include "./png.h"
#include "Utilities_CC/utilities_CC.h"

FUNCTION_DEF (int read_png_image, (
         FILE *fp, 
         Unsigned_char_matrix &im,
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type /*- = NULL -*/, 
         int *p_interlace_type /*- = NULL -*/
         ) )
   {
   // Read from a file to an image -- just returns a string of bytes
   
   /* Create and initialize the png_struct with the desired error handler
    * functions.  If you want to use the default stderr and longjump method,
    * you can supply NULL for the last three parameters.  We also supply the
    * the compiler header file version, so that we know if the application
    * was compiled with a compatible version of the library.  REQUIRED
    */
   png_structp png_ptr = 
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
   if (png_ptr == NULL) return 0;

   /* Allocate the memory for image information. */
   png_infop info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
      {
      png_destroy_read_struct(&png_ptr, png_infopp_NULL, png_infopp_NULL);
      error_message("read_png_image: png_create_info_struct failed");
      return 0;
      }

   /* Set error handling if you are using the setjmp/longjmp method (this is
    * the normal method of doing things with libpng).  REQUIRED unless you
    * set up your own error handlers in the png_create_read_struct() earlier.
    */

   if (setjmp(png_jmpbuf(png_ptr)))
      {
      /* Free all of the memory associated with the png_ptr and info_ptr */
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);

      /* If we get here, we had a problem reading the file */
      error_message("read_png_image: returned to longjmp");
      return 0;
      }

   /* Set up the input control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   // What we want to extract from the file
   png_uint_32 width, height;
   int bit_depth, colour_type, interlace_type;

   /* Read the header info */
   png_read_info(png_ptr, info_ptr);

   /* Read the header */
   png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &colour_type,
       &interlace_type, int_p_NULL, int_p_NULL);

   /* Initialize the image */
   int bytes_per_row = png_get_rowbytes (png_ptr, info_ptr);
   im.Init (0, height-1, 0, bytes_per_row-1);

   // Now read row by row
   for (unsigned int row=0; row<height; row++)
      png_read_row (png_ptr, &(im[row][0]), png_bytep_NULL);

#if 0
   /* read rest of file, and get additional chunks in info_ptr - REQUIRED */
   png_read_end(png_ptr, info_ptr);

   /* At this point you have read the entire image */

   /* clean up after the read, and free any memory allocated - REQUIRED */
   png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
#endif

   /* Return the values */
   if (p_height)   
      *p_height = height;
   if (p_width)  
      *p_width = width;
   if (p_bit_depth)  
      *p_bit_depth = bit_depth;
   if (p_colour_type)  
      *p_colour_type = colour_type;
   if (p_interlace_type)  
      *p_interlace_type = interlace_type;

   /* that's it */
   return 1;
   }

FUNCTION_DEF (int read_png_header, (
         FILE *fp, 
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type /*- = NULL -*/, 
         int *p_interlace_type /*- = NULL -*/
         ) )
   {
   // Read the header of png image file
   // File should already be open and at the beginning of the file
   // It returns the fp pointing after the header.
   
   png_structp png_ptr;
   png_infop info_ptr;

   /* Create and initialize the png_struct with the desired error handler
    * functions.  If you want to use the default stderr and longjump method,
    * you can supply NULL for the last three parameters.  We also supply the
    * the compiler header file version, so that we know if the application
    * was compiled with a compatible version of the library.  REQUIRED
    */
   png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
   if (png_ptr == NULL) return 0;

   /* Allocate the memory for image information. */
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL)
      {
      png_destroy_read_struct(&png_ptr, png_infopp_NULL, png_infopp_NULL);
      error_message("read_png_image: png_create_info_struct failed");
      return 0;
      }

   /* Set error handling if you are using the setjmp/longjmp method (this is
    * the normal method of doing things with libpng).  REQUIRED unless you
    * set up your own error handlers in the png_create_read_struct() earlier.
    */

   if (setjmp(png_jmpbuf(png_ptr)))
      {
      /* Free all of the memory associated with the png_ptr and info_ptr */
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);

      /* If we get here, we had a problem reading the file */
      error_message("read_png_header: returned to longjmp");
      return 0;
      }

   /* Set up the input control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   // What we want to extract from the file
   png_uint_32 width, height;
   int bit_depth, colour_type, interlace_type;

   /* Read the header info */
   png_read_info(png_ptr, info_ptr);

   /* Read the header */
   png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &colour_type,
       &interlace_type, int_p_NULL, int_p_NULL);

   /* clean up after the read, and free any memory allocated - REQUIRED */
   png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);

   /* Return the values */
   if (p_height)   
      *p_height = height;
   if (p_width)  
      *p_width = width;
   if (p_bit_depth)  
      *p_bit_depth = bit_depth;
   if (p_colour_type)  
      *p_colour_type = colour_type;
   if (p_interlace_type)  
      *p_interlace_type = interlace_type;

   /* that's it */
   return 1;
   }

FUNCTION_DEF (int read_png_header, (
         const char *fname,
         int *p_height,
         int *p_width,
         int *p_bit_depth,
         int *p_colour_type /*- = NULL -*/, 
         int *p_interlace_type /*- = NULL -*/
         ) )
   {
   // Open a file
   FILE *infile = fopen (fname, "rb");
   if (! infile)
      {
      error_message("read_png_header: cannot open file \"%s\" for read", fname);
      return 0;
      }

   // Now call the other routine
   int result = read_png_header (infile, p_height, p_width, p_bit_depth,
         p_colour_type, p_interlace_type);

   // Close the file and return
   fclose (infile);
   return result;
   }


