#ifndef _pngwritefile_16bit_cc_dcl_
#define _pngwritefile_16bit_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include "Utilities_CC/utilities_CC.h"
#include <PNG/png.h>
/* */

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   unsigned short **R,
	   unsigned short **G,
	   unsigned short **B,
	   png_uint_32 height,
	   png_uint_32 width
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   unsigned short **R,
	   png_uint_32 height,
	   png_uint_32 width
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   Unsigned_short_matrix &im
	   ));

FUNCTION_DECL (int write_png_image, (
	   const char *file_name, 
	   Unsigned_short_matrix &R,
      Unsigned_short_matrix &G,
      Unsigned_short_matrix &B
	   ));

#endif
