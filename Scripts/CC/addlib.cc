
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define Bool short
#define FALSE 0
#define TRUE 1

#define TRUE 1

// String constants for file locations
char *IUPACKAGEHOME, *SYS_IUPACKAGEHOME;
char *HOMENAME = "IUPACKAGEHOME";
char *SYS_HOMENAME = "SYS_IUPACKAGES";
const char *IU_LIBRARY_TABLE = "iu_library_table";

// Flag for debugging information
Bool verbose = FALSE;
Bool silent = FALSE;

int main (int argc, char *argv[])
   {
   // Flags
   int from_stdin = 0;  // Take the library names from stdin

   // Get the parameters
   char *program_name = argv[0];

   // Skip over the program name
   argv++; argc--;

   while (argc > 0)
      {
      if (argv[0][0] != '-') break;

      // parse the option
      switch (argv[0][1])
         {
         case 'i' :
            {
            // Input from standard input
            from_stdin = 1;
            break;
            }
         case 'v' :
            {
            // Verbose flag (to stderr)
            verbose = TRUE;
            break;
            }
          case 's' :
            {
            // Sets the standard output silent
            silent = TRUE;
            break;
            }
         default :
            {
            fprintf (stderr, "%s : Unknown option \"%s\"\n",
                program_name, argv[0]);
            exit (1);
            break;
            }
         }

      // Skip to the next argument
      argv++; argc--;
      }

   // Test the parameters
   if ((from_stdin && argc != 0) || (! from_stdin && argc == 0))
      {
      fprintf (stderr,
         "Usage : libdependencies -i OR libdependencies libnames ...\n");
      exit (1);
      }

   // Get the environment variable
   IUPACKAGEHOME = getenv (HOMENAME);
   if (IUPACKAGEHOME == (char *)0)
      {
      fprintf (stderr,
                "Error : Environement variable IUPACKAGEHOME must be set\n");
      exit (1);
      }

   // Get the environment variable
   SYS_IUPACKAGEHOME = getenv (SYS_HOMENAME);
   if (SYS_IUPACKAGEHOME == (char *)0)
      {
      fprintf (stderr,
                "Error : Environement variable SYS_IUPACKAGES must be set\n");
      exit (1);

      }

   // Find the library name and its directory
   
