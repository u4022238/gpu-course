#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define Bool short
#define FALSE 0
#define TRUE 1

// String constants for file locations
char *IUPACKAGEHOME            = (char *)0;
char *SYS_IUPACKAGEHOME        = (char *)0;
const char *HOMENAME           = "IUPACKAGEHOME";
const char *SYS_HOMENAME       = "SYS_IUPACKAGES";
const char *IU_LIBRARY_TABLE   = "iu_library_table";
const char *IU_LibDependencies = "IU_LibDependencies";

// Output options
int arglist_output = FALSE;
int plain_output   = FALSE;
int debug_log      = FALSE;

// Name for querying
char *queryname = (char *)0;

char *debug_logfile = (char *)0;

// Flag for debugging information
Bool verbose = FALSE;
Bool silent = FALSE;

class LibraryInfo
   {
   public :
   char library_name[256]; // The name of this library
   char lib_directory[256];   // The directory that this library is made in
   char uses_name[256];    // The USES name for this library

   int numafter;     // Number of libraries that must come after
   int *after;       // List of libraries that must come after
   int numbefore;    // Number of libraries that must come before
   int *before;         // List of libraries that must come before

   int count_after;     // Counts unplaced libraries in the after list
   int count_before;    // Counts unplaced libraries in the before list

   Bool used;        // Whether the library is used or not
   Bool placed;         // Whether order has been found for this library
   Bool is_user;     // Whether in IUPACKAGEHOME or NOT

   LibraryInfo () :
   numafter (0),
   after ((int *)0),
   numbefore (0),
   before ((int *)0),
   used (FALSE),
   placed(FALSE)
   {}
   
   ~LibraryInfo ()
      {
      delete [] after;
      delete [] before;
      }
   };

// The main data structure, kept global
static LibraryInfo *library;
static int numlibs;

// Also keep a list of the package names that have been used
static char **uses_names;
static int numuses;

// Keep the lists of used libraries and another array order_list to 
// give their order
static int *used_library;
static int *order_list;
static int numused;
static int num_not_placed;

static char *stringcopy (char *name)
   {
   int length = strlen(name);
   char *copy = new char[length+1];
   strcpy (copy, name);
   return copy;
   }

int find_library (const char *libname)
   {
   // fprintf (stderr, "\nSearching for library \"%s\"\n", libname);
   int returnval = -1;
   for (int i=0; i<numlibs; i++)
      {
      // fprintf (stderr, "\tCompare \"%s\"\n", library[i].library_name);
      
      // find the name
      if (strcmp(libname, library[i].library_name) == 0)
         {
         returnval = i;
         // fprintf (stderr, "\t\t\tFound\n");
         break;
         }
      }

   // Return the library number
   return returnval;
   }

//-------------------------------------------------------------------
// Extra library names
//
// List of extra library names.  To avoid having to use dynamic storage
static const int MAX_EXTRA_LIB_NAMES = 100;
static char *extra_library_names[MAX_EXTRA_LIB_NAMES];
static int num_extra_libraries = 0;

void add_extra_library (char *libname)
   {

   // See if the name is there already
   for (int i=0; i<num_extra_libraries; i++)
      if (strcmp (extra_library_names[i], libname) == 0)
    return;

   // Debug output
   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         // fprintf(fp,"add_extra_library_name: [%d] %s\n",numuses,libname);
         fclose(fp);
         }
      }

   // Take a copy of this, and store it
   if (num_extra_libraries >= MAX_EXTRA_LIB_NAMES)
      {
      // fprintf (stderr, "Too many extra libraries.  Can not add \"%s\"\n",
      //   libname);
      return;
      }
   else
      extra_library_names[num_extra_libraries++] = stringcopy(libname);
   }

void print_extra_library_names ()
   {
   // See if the name is there already
   for (int i=0; i<num_extra_libraries; i++)
      {
      char *libname = extra_library_names[i];
      if (arglist_output) 
         printf("-l%s ",libname);
      else if (plain_output) 
         printf("MD_LIBRARY %s\n",libname);
      else
         printf ("LDLIBS += -l%s\n", libname);
      }
   }

//-------------------------------------------------------------------

void add_library_name (char *libname)
   {
   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         fprintf(fp,"add_library_name: [%d] %s\n",numuses,libname);
         fclose(fp);
         }
       }

   uses_names[numuses++] = libname;
   }

int find_used_name (const char *uname)
   {
   int returnval = -1;
   for (int i=0; i<numuses; i++)
      {
      // find the name
      if (strcmp(uname, uses_names[i]) == 0)
         {
         returnval = i;
         break;
         }
      }

   // Return the library number
   return returnval;
   }

void get_uses_name (const char *libdir_name, char *uses_name)
   {
   // Given the name of the library directory, gets the uses name from it
   // We want the stuff up to the last / EXCEPT in the case where there
   // is no / (example Smallg++).
   const char *p;

   // First of all, go over the initial ./
   p = libdir_name+2;

   // Next find the last / in the name

   // Get it pointing at the final null character
   const char *end = libdir_name + strlen(libdir_name);

   // Move back from there.  We must at least stop at the initial ./
   while (*end != '/') end--;
   if (end < p) end = libdir_name + strlen(libdir_name);

   // Now end is pointing at the last /.  Do the copy
   char *out = uses_name;
   while (p != end)
      *(out++) = *(p++);

   // Final null
   *out = '\0';
   }

void print_uses_names ()
   {
   // Prints out all the USES statements for the used libraries
   for (int i=0; i<numused; i++)
      {
      // Get the library
      LibraryInfo &lib = library[used_library[i]];

      // Find the USES name and print it
      if (find_used_name (lib.uses_name) < 0)
         {
         if (! silent) 
            {
            if (arglist_output) 
               printf("-u%s ",lib.uses_name);
            else if (plain_output) 
               printf("MD_INCLUDES %s\n",lib.uses_name);
            else
               printf ("USES += %s\n", lib.uses_name);
            }

         // Also store the name, so we do not print it again
         add_library_name (lib.uses_name);
         }
      }
   }

int count_dependents (const char *dependencies_filename)
   {

   // Counts the number of lines in the dependency file.
   // Returns -1 if it can not open the file

   FILE *dependencies_file = fopen (dependencies_filename, "r");
   if (! dependencies_file) return -1;

   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         fprintf(fp,"count_dependents: %s\n",dependencies_filename);
         fclose(fp);
         }
      }

   // Now, read the dependencies
   int count = 0;
   while (1)
      {
      // Read the next library string from the dependencies file
      char tlibstring[256];
      int n = fscanf (dependencies_file, "%s", tlibstring);

      // Done at end of file
      if (n == EOF) break;

      if (debug_log) 
         {
         FILE *fp = fopen(debug_logfile,"a");
         if (fp) 
            {
            fprintf(fp,"count_dependents: %s\n",tlibstring);
            fclose(fp);
            }
         }

      // Update the count
      count ++;
      }

   // Close the file
   fclose (dependencies_file);

   // Return the count -- number of dependent libraries mentioned here
   return count;
   }

void read_library (char *libname, char *parent)
   {
   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         fprintf(fp,"read_library: %s  [parent=%s]\n",libname,parent);
         fclose(fp);
         }
      }

   // Reads the library and its dependents
   int libindex = find_library (libname);

   // If the library is not found, then return
   if (libindex < 0)
      {
      fprintf (stderr, 
         "Library \"%s\" not in iu_library_table - needed by \"%s\"\n",
         libname, parent);

      // Add to extra library names
      add_extra_library (libname);

      return;
      }

   // Indicate if the library is one that is being queried
   if (queryname && strcmp(libname, queryname) == 0)
      fprintf (stderr, "Library \"%s\" is needed by \"%s\"\n", libname, parent);

   // Now, read the sublibrary file
   const char *directory_name = library[libindex].lib_directory;

   // fprintf (stderr, "read_library \"%s\", directory name = \"%s\"\n",
   //      libname, directory_name);

   // Skip the libraries that have directory name "-"
   if (strcmp(directory_name, "-") == 0) return;

   // If the library has already been marked, then return
   if (library[libindex].used == 1)
      return;

   // Otherwise, we need to mark it
   library[libindex].used = 1;

   // Open the dependencies file
   char dependencies_filename [256];
   FILE *dependencies_file = NULL;
   int numdependents;
   
   // Get the dependency file name, open the file and get the dependents
   do
      {
      // First of all, try the IUPACKAGEHOME

      // Make a name for the dependencies file
      sprintf (dependencies_filename, "%s/%s/%s", 
         IUPACKAGEHOME, directory_name, IU_LibDependencies);

      // fprintf (stderr, "makedepends:  dependencies_filename = \"%s\"\n",
      //      dependencies_filename);
      // fprintf (stderr, "makedepends:  IUPACKAGEHOME = \"%s\"\n",
      //      IUPACKAGEHOME);
      // fprintf (stderr, "makedepends:  directory_name = \"%s\"\n",
      //      directory_name);
      // fprintf (stderr, "makedepends:  IU_LibDependencies = \"%s\"\n",
      //      IU_LibDependencies);

      // Count the dependents, and allocate space for the after list
      numdependents = count_dependents (dependencies_filename);

      // Skip the rest of this block if succeeded
      if (numdependents >= 0) break;

      // If this did not succeed, then try the SYS_IUPACKAGEHOME
      // Make a name for the dependencies file
      sprintf (dependencies_filename, "%s/%s/%s", 
         SYS_IUPACKAGEHOME, directory_name, IU_LibDependencies);

      // Count the dependents, and allocate space for the after list
      numdependents = count_dependents (dependencies_filename);

      // If file could not be opened, then break
      if (numdependents < 0)
         {
         fprintf (stderr, 
           "Could not find dependencies file in directory \"%s\"\n",
           directory_name);
         return;
         }
      } while (0);
   
   // If there are no dependents, then skip out
   if (numdependents == 0)
      {
      if (dependencies_file != NULL) 
         {
         fclose(dependencies_file);
         dependencies_file = NULL;
         }
      return;
      }
   
   // Make space for new dependents
   library[libindex].after = new int [numdependents];

   // Now, to through the file again, this time filling in the lists
   dependencies_file = fopen (dependencies_filename, "r");
   if (! dependencies_file)
      {
      fprintf (stderr, "Can not read dependencies file \"%s\"\n",
         dependencies_filename);
      return;
      }

   // Now, read the dependencies
   while (1)
      {
      // Read the next library string from the dependencies file
      char tlibstring[256];
      int n = fscanf (dependencies_file, "%s", tlibstring);

      // Done at end of file
      if (n == EOF) break;

      // We need to ignore the initial -l in the string
      char *sublibname = tlibstring;
      if (strncmp (tlibstring, "-l", 2) == 0)
         sublibname = sublibname + 2;

      // Lop off a final ".lib"
         {
         char *p;
         if ((p = strstr (tlibstring, ".lib")) != NULL)
            *p = '\0';
         }

      // Find this library
      int subindex = find_library(sublibname);

      // Do not make a library dependent on itself
      if (subindex == libindex) continue;

      // Store the subindex in the after list 
      if (subindex >= 0)
    library[libindex].after[library[libindex].numafter++] = subindex;
      
#ifdef MARK_MISSING_LIBRARIES
      // Message if not there
      if (subindex < 0)
         fprintf (stderr, "Library \"%s\" depends on unknown library \"%s\"\n",
            libname, sublibname);
#endif

      // Recursively descend 
      read_library (sublibname, libname);
      }

   // Close the dependencies file
   fclose (dependencies_file);
   dependencies_file = NULL;
   }

void fill_before_lists ()
   {
   // We now have to fill out the before lists for the libraries
   int i, j;

   // First of all see how many are before each library
   for (i=0; i<numlibs; i++)
      library[i].numbefore = 0;

   // Get the counts
   for (i=0; i<numlibs; i++)
      for (j=0; j<library[i].numafter; j++)
         {
         int index = library[i].after[j];
         library[index].numbefore++;
         }

   // Allocate
   for (i=0; i<numlibs; i++)
      library[i].before = new int [library[i].numbefore];

   // Reset the counts
   for (i=0; i<numlibs; i++)
      library[i].numbefore = 0;

   // Now, fill in the before lists
   for (i=0; i<numlibs; i++)
      for (j=0; j<library[i].numafter; j++)
         {
         int index = library[i].after[j];
         library[index].before[library[index].numbefore++] = i;
         }
   }

void print_remaining_libraries ()
   {
   // Prints out all the libraries not yet placed
   fprintf (stderr, "\nRemaining libraries\n");

   for (int i=0; i<numused; i++)
      {
      // If this library is place already, then skip
      int index = used_library[i];
      if (library[index].placed) continue;

      // Get an alias
      LibraryInfo &lib = library[index];

      // Print out
      fprintf (stderr, "\t %-25s   A : %2d  B : %2d  CA : %2d  CB : %2d\n",
         lib.library_name, lib.numafter, lib.numbefore, 
         lib.count_after, lib.count_before);
      }

   fprintf (stderr, "\n");
   }

void place_library_front (int index, int *low)
   {
   // Places a library at the front of the order list
   int i;
   order_list[(*low)++] = index;

   if (verbose)
      fprintf (stderr, "Front Placing library %-25s in position %d\n", 
   library[index].library_name, (*low)-1);

   // We also need to mark it as placed
   library[index].placed = TRUE;

   // We also need to decrease the counts on all the other dependent libraries
   for (i=0; i<library[index].numbefore; i++)
      {
      int ind_before = library[index].before[i];
      library[ind_before].count_after--;
      }

   for (i=0; i<library[index].numafter; i++)
      {
      int ind_after = library[index].after[i];
      library[ind_after].count_before--;
      }

   // Decrease the number not placed
   num_not_placed--;

   // Give useful debugging information and loop information
   if (verbose) print_remaining_libraries();
   }

void place_library_back (int index, int *high)
   {
   // Places a library at the front of the order list
   int i;
   order_list[(*high)--] = index;

   if (verbose)
      fprintf (stderr, "Back  Placing library %-25s in position %d\n", 
   library[index].library_name, (*high)+1);

   // We also need to mark it as placed
   library[index].placed = TRUE;

   // We also need to decrease the counts on all the other dependent libraries
   for (i=0; i<library[index].numbefore; i++)
      {
      int ind_before = library[index].before[i];
      library[ind_before].count_after--;
      }

   for (i=0; i<library[index].numafter; i++)
      {
      int ind_after = library[index].after[i];
      library[ind_after].count_before--;
      }

   // Decrease the number not placed
   num_not_placed--;

   // Give useful debugging information and loop information
   if (verbose) print_remaining_libraries();
   }

void order_libraries ()
   {
   // Places the libraries in order of dependence.
   // The output is through the global array order_list which is allocated here
   int i;
   
   // First of all, find how many of the libraries are used
   numused = 0;
   for (i=0; i<numlibs; i++)
      if (library[i].used) numused++;

   // Now, make a used list - keeps the index of the used libraries
   used_library = new int [numused];

   // Put the used libraries in the list
   numused = 0;
   for (i=0; i<numlibs; i++)
      if (library[i].used) 
    used_library[numused++] = i;

   // Now we should print out the USES list
   print_uses_names();

   // Also make an order list
   order_list = new int [numused];

   // Next high and low places for putting libraries
   int low = 0;
   int high = numused-1;

   // Fill out the count of the number of libraries before and after
   for (i=0; i<numlibs; i++)
      {
      library[i].count_after = library[i].numafter;
      library[i].count_before = library[i].numbefore;
      }

   // Set the number not placed
   num_not_placed = numused;

   // Give useful debugging information and loop information
   if (verbose) print_remaining_libraries();

   // We stay in this loop until all the libraries are placed
   while (1)
      {

      // Now, go ahead and start filling the order list
      // We alternate filling from the top and the bottom
   
      while (1)
         {
         // This loop continues until there is nothing, either top of bottom that one
         // can place.

         // This is the flag to stop
         Bool progress2 = FALSE;
         
         // Try placing at the top
         while (1)
            {
            // Try peeling off libraries from the bottom
            Bool progress = FALSE;
   
            for (i=0; i<numused; i++)
               {
               // If this library is place already, then skip
               int index = used_library[i];
               if (library[index].placed) continue;
   
               if (library[index].count_before == 0)
                  {
                  place_library_front(index, &low);
                  progress = TRUE;
                  progress2 = TRUE;
                  }
               }
   
            // If progress is still false, then we have found no library with 0 count
            if (progress == FALSE) break;
            }
   
         // Now try placing at the bottom
         while (1)
            {
            // Try peeling off libraries from the bottom
            Bool progress = FALSE;
   
            for (i=0; i<numused; i++)
               {
               // If this library is place already, then skip
               int index = used_library[i];
               if (library[index].placed) continue;
   
               if (library[index].count_after == 0)
                  {
                  place_library_back(index, &high);
                  progress = TRUE;
                  progress2 = TRUE;
                  }
               }
   
            // If progress is still false, then we have found no library with 0 count
            if (progress == FALSE) break;
            }
   
         // If we are making no more progress here, then stop
         if (progress2 == FALSE) break;
         }
   
      // At this point there must be loops in the graph, so we need to break the loop
      // However first check that all are not placed
      if (num_not_placed == 0) break;
      
      // The results of the choice of library made below
      int bestindex;
      Bool front_placement = TRUE;

      // Which strategy to use
      const int strategy = 2;
      if (strategy == 1)
         {
         // The strategy is to place the one that has the fewest things before it
         bestindex = -1;
         for (i=0; i<numused; i++)
            {
            int index = used_library[i];
            if (library[index].placed) continue;
   
            int count = library[index].count_before;
            if (bestindex == -1 || count < library[bestindex].count_before)
               bestindex = index;
            }

         // We place in front
         front_placement = TRUE;
         }

      else if (strategy == 2)
         {
         // The strategy is to place the one that has the greatest difference
         // between before and after
         bestindex = -1;
         double bestcost;
         for (i=0; i<numused; i++)
            {
            int index = used_library[i];
            if (library[index].placed) continue;
   
            // Compute the cost
            double cost = library[index].count_before - library[index].count_after;
            if (cost < 0) cost = -cost;

            if (bestindex == -1 || cost > bestcost)
               {
               bestindex = index;
               bestcost = cost;
               }

            if (library[bestindex].count_before < library[bestindex].count_after)
               front_placement = TRUE;
            else
               front_placement = FALSE;
            }
         }
   
      // Give a warning before placing the library
      // fprintf (stderr, 
      //    "Warning : Loops found -- library \"%s\" chosen to break loop\n", 
      //    library[bestindex].library_name);

      // This is the one that we will place
      if (front_placement)
         place_library_front (bestindex, &low);
      else
         place_library_back (bestindex, &high);
      }
   }

void print_libraries_in_order ()
   {
   if (! silent)
      for (int i=0; i<numused; i++)
         {
         int index = order_list[i];

         // Print out the library
         if (arglist_output) 
            printf("-l%s ",library[index].library_name);
         else if (plain_output) 
            printf("MD_LIBRARY %s\n",library[index].library_name);
         else
            printf ("LDLIBS += -l%s\n", library[index].library_name);

    // Debug information
         if (debug_log) 
       {
            FILE *fp = fopen(debug_logfile,"a");
            if (fp) 
          {
               fprintf(fp,"print_libraries_in_order: %s\n",
                       library[index].library_name);
               fclose(fp);
               }
            }
         }
   }

int countlines (const char *iu_or_sys)
   {
   // Counts the number of library entries in a file
   int loc_numlibs = 0;

   fprintf (stderr, "countlines:  iu_or_sys = \"%s\"\n", iu_or_sys);

   // Construct the name for the library file
   char libfname[256];
   sprintf (libfname, "%s/%s", iu_or_sys, IU_LIBRARY_TABLE);

   // Open the file
   fprintf (stderr, "countlines:  opening = \"%s\"\n", libfname);
   FILE *libfile = fopen (libfname, "r");
   if (libfile == NULL)
      {
      fprintf (stderr, "Can not open iu_library_table file \"%s\"\n", libfname);
      return 0;
      }

   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         fprintf(fp,"countlines: Reading library table '%s'\n",libfname);
         fclose(fp);
         }
      }

   // Read the file in a line at a time, just to see how many lines
   while (1)
      {
      // Read a line of the file
      char libname[80];
      char directory[256];
      int n = fscanf (libfile, "%s : %s", libname, directory);

      // Stop on EOF
      if (n == EOF) break;

      if (n != 2)
         {
         fprintf (stderr, "Error at line %d of file \"%s\"\n", 
            loc_numlibs+1, libfname);
         exit(1);
         }

      loc_numlibs++;
      }

   // Close the file
   fclose (libfile);

   if (debug_log) 
      {
      FILE *fp = fopen(debug_logfile,"a");
      if (fp) 
         {
         fprintf(fp,"countlines: found %d libraries\n", loc_numlibs);
         fclose(fp);
         }
      }

   return loc_numlibs;
   }

int readin_libraries (const char *iu_or_sys, int *loc_numlibs, Bool is_user)
   {
   // Actually reads in the libraries into the table

   // Construct the name for the library file
   char libfname[256];
   sprintf (libfname, "%s/%s", iu_or_sys, IU_LIBRARY_TABLE);

   // Open the file
   FILE *libfile = fopen (libfname, "r");
   if (libfile == NULL)
      {
      fprintf (stderr, "Can not open iu_library_table file \"%s\"\n",
    libfname);
      return 0;
      }

   // Read the file in a line at a time, just to see how many lines
   while (1)
      {
      // Read a line of the file
      char libname[80];
      char directory[256];
      int n = fscanf (libfile, "%s : %s", libname, directory);

      // Stop on EOF
      if (n == EOF) break;
      if (n != 2)
         {
         fprintf (stderr, "Error at line %d of file \"%s\"\n", 
            *loc_numlibs, libfname);
         exit(1);
         }

      // Transfer 
      strcpy (library[*loc_numlibs].library_name, libname);
      strcpy (library[*loc_numlibs].lib_directory, directory);

      // Set user flag
      library[*loc_numlibs].is_user = is_user;

      // Also set the uses_name
      if (strcmp (directory, "-") != 0)
         get_uses_name (directory, library[*loc_numlibs].uses_name);

      (*loc_numlibs)++;
      }

   // Return success
   return 1;
   }


int main (int argc, char *argv[])
   {
   // Flags
   int from_stdin = 0;  // Take the library names from stdin

   // Get the parameters 
   char *program_name = argv[0];

   // Skip over the program name 
   argv++; argc--;

   while (argc > 0)
      {  
      if (argv[0][0] != '-') break;
       
      // parse the option 
      switch (argv[0][1])
         {
         case 'i' :
            {
            // Input from standard input
            from_stdin = 1;
            break;
            }
         case 'v' :
            {
            // Verbose flag (to stderr)
            verbose = TRUE;
            break;
            }
         case 's' :
            {
            // Sets the standard output silent
            silent = TRUE;
            break;
            }
         case 'd' :
            {
            debug_log = TRUE;
            debug_logfile = argv[0]+2;
            FILE *fp = fopen(debug_logfile,"w");
            if (fp)
               fclose(fp);
            break;
            }
         case 'a' :
            {
            arglist_output = TRUE;
            break;
            }
         case 'p' :
            {
            plain_output = TRUE;
            break;
            }
         case 't' :
            {
            if (IUPACKAGEHOME == (char*)0)
               IUPACKAGEHOME = argv[0]+2;
            else
               SYS_IUPACKAGEHOME = argv[0]+2;
            break;
            }
         case 'q' :
            {
            queryname = argv[0]+2;
            break;
            }
         default :
            {
            fprintf (stderr, "%s : Unknown option \"%s\"\n",
                      program_name, argv[0]);
            exit (1);
            break;
            }
         }
 
      // Skip to the next argument 
      argv++; argc--;
      }  

   
   if (verbose) fprintf (stderr, "Running makedependencies\n");

   // Test the parameters
   if ((from_stdin && argc != 0) || (! from_stdin && argc == 0))
      {
      fprintf (stderr, 
         "Usage : libdependencies -i OR libdependencies libnames ...\n");
      exit (1);
      }

   // When IUPACKAGEHOME is not explicitly specfied on the command line, use
   // the value of the environment variable.
   if (IUPACKAGEHOME == (char *)0) 
      {
      IUPACKAGEHOME = getenv (HOMENAME);
      fprintf (stderr, "Environment: \"%s\" -> \"%s\"\n",
            HOMENAME, IUPACKAGEHOME);
      if (IUPACKAGEHOME == (char *)0)
         {
         fprintf (stderr, 
                  "Error : Environement variable IUPACKAGEHOME must be set\n");
         exit (1);
         }
      }

   // When SYS_IUPACKAGEHOME is not explicitly specfied on the command line, use
   // the value of the environment variable.
   if (SYS_IUPACKAGEHOME == (char *)0) 
      {
      SYS_IUPACKAGEHOME = getenv (SYS_HOMENAME);
      if (SYS_IUPACKAGEHOME == (char *)0)
         {
         fprintf (stderr, 
                  "Error : Environement variable SYS_IUPACKAGES must be set\n");
         exit (1);
         }
      }

   // Now, determine the number of lines in the library files
   numlibs = countlines (IUPACKAGEHOME);
   if (strcmp(IUPACKAGEHOME, SYS_IUPACKAGEHOME) != 0)
      numlibs += countlines (SYS_IUPACKAGEHOME);
 
   // Make a structure to hold the libraries
   library = new LibraryInfo [numlibs];

   // Also the used names
   uses_names = new char * [numlibs];
   numused = 0;

   // Now, read again, transferring data
   numlibs = 0;
   readin_libraries (IUPACKAGEHOME, &numlibs, TRUE);
   if (strcmp(IUPACKAGEHOME, SYS_IUPACKAGEHOME) != 0)
      readin_libraries (SYS_IUPACKAGEHOME, &numlibs, FALSE);

   //----------------------------------------------------------------
   // At this point we have the library translation table read

   // Next try to get the libraries
   if (from_stdin)
      while (1)
         {
         char libname[128];
         int n = scanf ("%s", libname);
         if (n != 1) break;
         read_library (libname, "Top level");
         }
   else
      for (int i=0; i<argc; i++)
         {
         // Mark and report the libraries needed here
         char *libname = argv[i];
         read_library (libname, "Top level");
         }

   // Now, fill out the before lists
   fill_before_lists();

   // Now order the libraries
   order_libraries();

   // Print out the order
   print_libraries_in_order ();

   // Print out any extra library names
   print_extra_library_names ();

   // Clean up (might as well)
   delete[] library;
   delete[] uses_names;

   return 0;
   }
