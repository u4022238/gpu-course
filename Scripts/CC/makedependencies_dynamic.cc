#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// String constants for file locations
char *IUPACKAGEHOME, *SYS_IUPACKAGES;
const char *HOMENAME = "IUPACKAGEHOME";
const char *SYS_HOMENAME = "SYS_IUPACKAGES";
int arglist_output = 0;
const char *IU_LIBRARY_TABLE = "iu_library_table";
const char *IU_LibDependencies = "IU_LibDependencies";

class LibraryInfo
   {
   public :
   char library_name[80];  // The name of this library
   char lib_directory[256];   // The directory that this library is made in
   char uses_name[256];    // The USES name for this library

   int numsublibs;      // Number of sublibraies in the sublibrary list
   LibraryInfo *sublibs;   // List of libraries this one calls

   int numdependents;      // Number of libraries calling this one

   int used;         // Whether the library is used or not
   int is_user;         // 0 means from IUPACKAGEHOME
            // 1 means from SYS_IUPACKAGES

   LibraryInfo () :
   numsublibs (0),
   sublibs ((LibraryInfo *)0),
   numdependents (0),
   used (0)
   {}
   
   ~LibraryInfo ()
   {
   delete [] sublibs;
   }
   };

// The main data structure, kept global
static LibraryInfo *library;
static int numlibs;

// Also keep a list of the library names that have been used
static char **uses_names;
static int numused;

int find_library (const char *libname)
   {
   int returnval = -1;
   for (int i=0; i<numlibs; i++)
      {
      // find the name
      if (strcmp(libname, library[i].library_name) == 0)
    {
    returnval = i;
    break;
    }
      }

   // Return the library number
   return returnval;
   }

void add_library_name (char *libname)
   {
   uses_names[numused++] = libname;
   }

int find_used_name (const char *uname)
   {
   int returnval = -1;
   for (int i=0; i<numused; i++)
      {
      // find the name
      if (strcmp(uname, uses_names[i]) == 0)
    {
    returnval = i;
    break;
    }
      }

   // Return the library number
   return returnval;
   }

void get_uses_name (const char *libdir_name, char *uses_name)
   {
   // Given the name of the library directory, gets the uses name from it
   // We want the stuff up to the last / EXCEPT in the case where there
   // is no / (example Smallg++).
   const char *p;

   // First of all, go over the initial ./
   p = libdir_name+2;

   // Next find the last / in the name

   // Get it pointing at the final null character
   const char *end = libdir_name + strlen(libdir_name);

   // Move back from there.  We must at least stop at the initial ./
   while (*end != '/') end--;
   if (end < p) end = libdir_name + strlen(libdir_name);

   // Now end is pointing at the last /.  Do the copy
   char *out = uses_name;
   while (p != end)
      *(out++) = *(p++);

   // Final null
   *out = '\0';
   }

void mark_libraries (char *libname)
   {
   // Marks the libraries that are needed
   int libindex = find_library (libname);

   // If the library is not found, then return
   if (libindex < 0)
      return;

   // If the library has already been marked, then return
   if (library[libindex].used == 1)
      return;

   // Output the USES name
   if (find_used_name (library[libindex].uses_name) < 0)
      {
        if (arglist_output)
          fprintf(stdout, "-u%s ",library[libindex].uses_name);
        else
          fprintf (stdout, "USES += %s\n", library[libindex].uses_name);

      // Also store the name
      add_library_name (library[libindex].uses_name);
      }

   // Output the name of the library
   if (arglist_output)
     fprintf(stdout,"-l%s ",libname);
   else
     fprintf (stdout, "LDLIBS += -l%s\n", libname);

   // Otherwise, we need to mark it
   library[libindex].used = 1;

   // Now, read the sublibrary file
   const char *directory_name = library[libindex].lib_directory;

   //fprintf(stderr,"lib %s\n",directory_name);

   // Open the dependencies file
   char dependencies_filename [256];
   FILE *dependencies_file;
   
   // Get the dependency file name, open the file and get the dependents
   do
      {
      // First of all, try the IUPACKAGEHOME

      // Make a name for the dependencies file
      sprintf (dependencies_filename, "%s/%s/%s", 
   IUPACKAGEHOME, directory_name, IU_LibDependencies);

      // Try to read the dependencies file
      dependencies_file = fopen (dependencies_filename, "r");
      if (dependencies_file) break;

      // If this did not succeed, then try the SYS_IUPACKAGES
      // Make a name for the dependencies file
      sprintf (dependencies_filename, "%s/%s/%s", 
      SYS_IUPACKAGES, directory_name, IU_LibDependencies);

      // Try to read the dependencies file
      dependencies_file = fopen (dependencies_filename, "r");
      if (! dependencies_file)
    {
    fprintf (stderr, 
      "Cannot find dependencies file in directory \"%s\"\n",
      directory_name);
    return;
    }
      } while (0);
   
   // Now, read the dependencies
   while (1)
      {
      // Read the next library string from the dependencies file
      char tlibstring[80];
      int n = fscanf (dependencies_file, "%s", tlibstring);

      //      fprintf(stderr,"\tneeds %s\n",tlibstring);

      // Done at end of file
      if (n == EOF) break;

      // We need to ignore the initial -l in the string
      char *sublibname = tlibstring;
      if (strncmp (tlibstring, "-l", 2) == 0)
    sublibname = sublibname + 2;

      // Lop off a final ".lib"
    {
    char *p;
         if ((p = strstr (tlibstring, ".lib")) != NULL)
       *p = '\0';
    }

      // Find this library
      int subindex = find_library(sublibname);
      
#ifdef MARK_MISSING_LIBRARIES
      // Message if not there
      if (subindex < 0)
    fprintf (stdout, "Library \"%s\" depends on unknown library \"%s\"\n",
       libname, sublibname);
#endif

      // Recursively descend 
      mark_libraries (sublibname);
      }

   // Close the dependencies file
   fclose (dependencies_file);
   }

int countlines (const char *iu_or_sys)
   {
   // Counts the number of library entries in a file
   int loc_numlibs = 0;

   // Construct the name for the library file
   char libfname[256];
   sprintf (libfname, "%s/%s", iu_or_sys, IU_LIBRARY_TABLE);

   // Open the file
   FILE *libfile = fopen (libfname, "r");
   if (libfile == NULL)
      {
      fprintf (stderr, "Can not open iu_library_table file \"%s\"\n",
    libfname);
      return 0;
      }

   // Read the file in a line at a time, just to see how many lines
   while (1)
      {
      // Read a line of the file
      char libname[80];
      char directory[256];
      int n = fscanf (libfile, "%s : %s", libname, directory);

      // Stop on EOF
      if (n == EOF) break;

      if (n != 2)
    {
    fprintf (stderr, "Error at line %d of file \"%s\"\n", 
      loc_numlibs+1, libname);
    exit(1);
    }

      loc_numlibs++;
      }

   // Close the file
   fclose (libfile);

   // Return the count
   return loc_numlibs;
   }

int readin_libraries (const char *iu_or_sys, int *loc_numlibs, int is_user)
   {
   // Actually reads in the libraries into the table

   // Construct the name for the library file
   char libfname[256];
   sprintf (libfname, "%s/%s", iu_or_sys, IU_LIBRARY_TABLE);

   // Open the file
   FILE *libfile = fopen (libfname, "r");
   if (libfile == NULL)
      {
      fprintf (stderr, "Can not open iu_library_table file \"%s\"\n",
    libfname);
      return 0;
      }

   // Read the file in a line at a time, just to see how many lines
   while (1)
      {
      // Read a line of the file
      char libname[80];
      char directory[256];
      int n = fscanf (libfile, "%s : %s", libname, directory);

      // Stop on EOF
      if (n == EOF) break;
      if (n != 2)
    {
    fprintf (stderr, "Error at line %d of file \"%s\"\n", 
      *loc_numlibs, libname);
    exit(1);
    }

      // Transfer 
      strcpy (library[*loc_numlibs].library_name, libname);
      strcpy (library[*loc_numlibs].lib_directory, directory);

      // Set user flag
      library[*loc_numlibs].is_user = is_user;

      // Also set the uses_name
      if (strcmp (directory, "-") != 0)
         get_uses_name (directory, library[*loc_numlibs].uses_name);

      (*loc_numlibs)++;
      }

   // Return success
   return 1;
   }

int main (int argc, char *argv[])
   {
   // Flags
   int from_stdin = 0;  // Take the library names from stdin

   // Get the parameters 
   char *program_name = argv[0];

   // Skip over the program name 
   argv++; argc--;

   while (argc > 0)
      {  
      if (argv[0][0] != '-') break;

      // parse the option 
      switch (argv[0][1])
         {
         case 'i' :
            {
              // Input from standard input
              from_stdin = 1;
              break;
            }
         case 'a' :
           arglist_output = 1;
           break;
         default :
            {
              fprintf (stderr, "%s : Unknown option \"%s\"\n",
                       program_name, argv[0]);
              exit (1);
              break;
            }
         }
 
      // Skip to the next argument 
      argv++; argc--;
      }  

   // Test the parameters
   if ((from_stdin && argc != 0) || (! from_stdin && argc == 0))
      {
      fprintf (stderr, 
    "Usage : libdependencies -i OR libdependencies libnames ...\n");
      exit (1);
      }

   // Get the environment variable
   IUPACKAGEHOME = getenv (HOMENAME);
   if (IUPACKAGEHOME == (char *)0)
      {
      fprintf (stderr, 
      "Error : Environment variable IUPACKAGEHOME must be set\n");
      exit (1);
      }

   // Get the environment variable
   SYS_IUPACKAGES = getenv (SYS_HOMENAME);
   if (SYS_IUPACKAGES == (char *)0)
      {
      fprintf (stderr, 
      "Error : Environment variable SYS_IUPACKAGES must be set\n");
      exit (1);
      }

   // Now, determine the number of lines in the library files
   numlibs = countlines (IUPACKAGEHOME);
   if (strcmp(IUPACKAGEHOME, SYS_IUPACKAGES) != 0)
      numlibs += countlines (SYS_IUPACKAGES);

   // Make a structure to hold the libraries
   library = new LibraryInfo [numlibs];

   // Also the used names
   uses_names = new char * [numlibs];
   numused = 0;

   // Now, read again, transferring data
   numlibs = 0;
   readin_libraries (IUPACKAGEHOME, &numlibs, 1);
   if (strcmp(IUPACKAGEHOME, SYS_IUPACKAGES) != 0)
      readin_libraries (SYS_IUPACKAGES, &numlibs, 0);

   //----------------------------------------------------------------
   // At this point we have the library translation table read

   // Next try to get the libraries
   if (from_stdin)
      while (1)
         {
         char libname[128];
    int n = scanf ("%s", libname);
    if (n != 1) break;
         mark_libraries (libname);
         }
   else
      for (int i=0; i<argc; i++)
         {
         // Mark and report the libraries needed here
         char *libname = argv[i];
         mark_libraries (libname);
         }

   // Clean up (might as well)
   delete[] library;
   delete[] uses_names;

   return 0;
   }
