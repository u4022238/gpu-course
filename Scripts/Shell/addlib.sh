#!/bin/bash

# Get the two parameters
library=$1
tjwddirectory=$2
comment=$3

# Make a temporary file name
tempfile=/tmp/adlib$$

# Strip everything up to IUPackages and replace by .
newdir=`echo ./$tjwddirectory`

# Create the new line for the library file
newline="$library : $newdir"


function create_lock ( )
{
   TEMPFILE="$1.$$"
   LOCKFILE="$1.lock"
   echo $$ >& $TEMPFILE 2> /dev/null || {
      echo "You do not have permission to access `dirname $TEMPFILE`"
      return 1
   }
   ln $TEMPFILE $LOCKFILE 2> /dev/null && {
      rm -f $TEMPFILE
      return 0
   }
   STALE_PID=`< $LOCKFILE`
   test "$STALE_PID" -gt "0" > /dev/null || {
      return 1
   }
   kill -O $STALE_PID 2> /dev/null && {
      rm -f $TEMPFILE
      return 1
   }
#   set -x
   echo "Stale lock file !!!!!!!!!!!!!!!!!!!!"
#   rm $LOCKFILE 2> /dev/null && {
#      echo "Removing stale lock file of process $STALE_PID"
#   }
#   ln $TEMPFILE $LOCKFILE 2> /dev/null && {
#      rm -f $TEMPFILE
#      return 0
#   }
   rm -f $TEMPFILE
   return 1
}

# Looking for a lockfile
lockfile=$IUPACKAGEHOME/iu_library_table
until create_lock $lockfile ; do
#   set x `ls -l "$lockfile".lock`
#   echo "Waiting for user $4 (working since $7 $8 $9) ... "
   echo "Waiting for lockfile ... "
   sleep 2
done

# Locate the iu_library_table file
libfile=$IUPACKAGEHOME/iu_library_table
if [ ! -f $libfile ] ; then
   echo "$library : $newdir" > $libfile

   # Remove the lockfile
   rm -f $lockfile.lock

   exit 0
fi

# Say what we are doing
libline=`egrep "^$library : " $libfile`
echo "   Previous : $libline"
echo "   New      : $newline"

# Remove the library and replace by the new one
echo "$library : $newdir" > $tempfile
# echo library = $library, tjwddir = $tjwddirectory, comment = $comment >> $tempfile
grep -v "^$library : " $libfile >> $tempfile

# Move to new location
cp $libfile $libfile.bak
mv $tempfile $libfile
#mv $tempfile $libfile.new

# Remove the lockfile
rm -f $lockfile.lock

exit 0
