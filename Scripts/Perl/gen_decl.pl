# this script generates .h files from .c files 
# 
#  Creates two files, a .file_decl.h and a file_decl.h, the
#  .file is created every time the script is run.  The other file
#  is only created when the the .file is different from the file.
#  if the -create_only flag is given, this script will create the file
#  only if it does not exist

# To run to stdout: perl gen_decl.pl -c -N eigenvalues.c -o - eigenvalues.c

# set up global variables

$SKIPPING = 1; # starts in skipping mode
$ECHOING = 0;
$INCLUDING = 0;
$FILENAME = "";
$CVARDIR = "Utilities";

# set the paren level to be 0 initially
$level = 0;
$incomment = 0;

# set up the default macro name
$MACRO = "FUNCTION_DECL";
$OUTPUT_FILE = "-";
$create_only = 0;

# extract the command line arguments
while ($ARGV[0] =~ /^-(\S+)/)
{
    # -N gives the name of the file
    if ($1 eq "N")
    {
	   shift;
	   $FILENAME = $ARGV[0];
	   # replace . with _
	   $FILENAME =~ s/\./_/g;
	   # replace / with _
	   $FILENAME =~ s/\//_/g;
    }

    elsif ($1 eq "X")
    {
      shift;
      $CVARDIR = $ARGV[0];
    }

    elsif ($1 eq "o")
    {
	   shift;
	   $OUTPUT_FILE = $ARGV[0];
    }

    # -c switches to c mode
    elsif ($1 eq "c")
    {
	   $MACRO = "EXTERN_C_FUNCTION";
    }

    elsif ($1 eq "create_only")
    {
	   $create_only = 1;
    }
    shift;
}


if($create_only == 1)
{
    if ( -e $OUTPUT_FILE )
    {
	exit;
    }
}

if( $create_only == 1)
{
    $DOT_OUTFILE = "." . $OUTPUT_FILE;
}

else
{
    $DOT_OUTFILE = $OUTPUT_FILE;
    $OUTPUT_FILE =~ s/^\.//;
}

open(TMP_OUT, ">" . $DOT_OUTFILE) || die "can't open $OUTPUT_FILE\n";
if ( (! -e $OUTPUT_FILE) || ($create_only == 1) )
{
    print STDERR "\n-- Creating file $OUTPUT_FILE\n";
} 

# put program in SKIPPING mode
sub BEGIN_SKIPPING
{
    $SKIPPING = 1;
    $ECHOING = 0;
    $INCLUDING = 0;
}

# put program in ECHOING mode
sub BEGIN_ECHOING
{
    $SKIPPING = 0;
    $ECHOING = 1;
    $INCLUDING = 0;
} 

# put program in INCLUDING mode
sub BEGIN_INCLUDING
{
    $SKIPPING = 0;
    $ECHOING = 0;
    $INCLUDING = 1;
} 

# print the beginning of an sfile
#    print TMP_OUT "#include \"Utilities/cvar.h\"\n";
sub begin_sfile
{
    print TMP_OUT "#ifndef _" .  $FILENAME . "_dcl_\n"; 
    print TMP_OUT "#define _" .  $FILENAME . "_dcl_\n";
    print TMP_OUT "#include \"" . $CVARDIR . "/cvar.h\"\n";
    print TMP_OUT "\n\n";
}

# print the end of the sfile
sub end_sfile
{
    print TMP_OUT "#endif\n"; 
}

##----------------------------------
##
## Real work starts here
##
##----------------------------------

# starts in SKIPPING mode
&BEGIN_SKIPPING;

# print out the start of the sfile
&begin_sfile;

# loop over input file 
while (<>)
{

    # getting a bit messy now!
    # we have to remove the comments from the code to count the parens

    #    make a copy of the input line
    $no_comment_line = $_;

    # remove /* anything */ from the line
    $no_comment_line =~ s|\/\*.*\*\/||g;

    # remove lines starting with // if we are not in a multi line comment
    if($incomment == 0)	
    {
	$no_comment_line =~ s|\/\/.*$||;
    }

    # if not in a two line comment then remove characters between the /* */ 
    if($incomment == 0)
    {
	# check for "/* " on a line by itself and set the mode to incomment
	if ( $no_comment_line =~ s|\/\*.*$|| ) 
	{
	    $incomment = 1;
	}
    }

    # we are in a multiline comment started with a /*
    else
    {

        # if the */ is on this line remove everything upto and including the */
	if ( $no_comment_line =~ s|^.*\*\/|| ) 
	{
	    $incomment = 0;
	}
	else
	{

            # inside a comment remove the entire line
	    $no_comment_line = "";
	}
    }

    # this keeps track of the parens
    # add one to level for each ( in the current line
    $level += ($no_comment_line =~s/\(/\(/g);  

    # subtract one from level for each ) in the current line
    $level -= ($no_comment_line =~ s/\)/\)/g);  

    #---------------
    # SKIPPING mode 
    #---------------
    if ($SKIPPING)
    {
	# replace FUNCTION_DEF with $MACRO on the line
	if( s/FUNCTION_DEF/$MACRO/g )
	{
	    # if it is a static function then go back to skipping
	    if(/static/)
	    {
		&BEGIN_SKIPPING;
	    }
	    else
	    {
		# print the current line , note this will have the
		# replacement of FUNCTION_DEF with $MACRO
		if($level == 0)
		{
		    # if level is 0 then we are done with this and 
		    # do not need to go into echoing mode, 
		    # we just print out the line with a ; at the end.

		    chop; # remove the newline from the end of the input
		    s/{.*$//;
		    print TMP_OUT $_;
		    print TMP_OUT ";\n\n";
		}
		else
		{	
                    # if level is not 0 then print the current line 
		    # and go into echo mode
		    print TMP_OUT $_;
		    &BEGIN_ECHOING;
		}
	    }
	}

	# for SFILE START BEGIN and INCLUDE
	if ( /SFILE_(START|BEGIN|INCLUDE)/ )
	{
	    print TMP_OUT "/* */\n";
	    &BEGIN_INCLUDING;
	}

	# for SFILE TOGGLE 
	if ( /SFILE_TOGGLE/ )
	{
	    &BEGIN_INCLUDING;
	}
    }


    #---------------
    # INCLUDING MODE
    #---------------
    elsif ($INCLUDING)
    {
        # look for reasons to quit including
	if ( /SFILE_(FINISH|END)/ )
	{
	    print TMP_OUT "/* */\n\n";
	    &BEGIN_SKIPPING;
	}
	elsif ( /SFILE_INCLUDE/ )
	{
	    &BEGIN_SKIPPING;
	}
	elsif ( /SFILE_TOGGLE/ )
	{
	    &BEGIN_SKIPPING;
	}

        # if including just print the current line to TMP_OUT
	else
	{
	    print TMP_OUT;
	}
    }

    #---------------
    # ECHOING mode
    #---------------
    elsif ($ECHOING)
    {

	# Edit out strings of the form /*- and -*/
        s|\/\*-||;
        s|-\*\/||;

	if ($level == 0)
	{
	    chop;
	    print TMP_OUT $_;
	    print TMP_OUT ";\n\n";
	    &BEGIN_SKIPPING;
	}
	else
	{
	    {
		print TMP_OUT $_;
	    }
	}

    }
}

&end_sfile;
close TMP_OUT;


# if we are going to stdout then just be done with it now.
if ($OUTPUT_FILE eq "-")
{
    exit;
}

if (!&cmp_files($OUTPUT_FILE, $DOT_OUTFILE) == 0)
{
# we have a change
    unlink($OUTPUT_FILE);  # remove the no dot file
    print "Create $OUTPUT_FILE\n";
    &copy_file ($DOT_OUTFILE, $OUTPUT_FILE);  # copy the decl_file to the no_dot version
    
}

sub copy_file
{
    local($name1,$name2) = @_;
    open(IFILE,"<$name1")  || die "can't open $name1 for read";
    open(OFILE,">$name2") || die "can't open $name2 for write";
    $buf = "";
    $block = 65536;
    while (($nread = sysread(IFILE,$buf,$block)) != 0)
    {
	syswrite(OFILE,$buf,$nread);
    }
    close(IFILE);
    close(OFILE);
}


# ------------------------------------------------------------
# Subroutine for comparing two files.  Return true if different.
sub cmp_files {
    # Arguments are two file names.
    local($name1,$name2) = @_;
    local(@stats1);
    local(@stats2);

    # Compare sizes.
    @stats1 = stat($name1);
    @stats2 = stat($name2);

    if ($stats1[7] != $stats2[7]) 
    {
	return 1;
    }
    
    return &cmp_file_content($name1, $name2);
}

# ------------------------------------------------------------
# Subroutine for comparing content of two files. Return true if different.
sub cmp_file_content {
    # Arguments are two file names.
    local($name1,$name2) = @_;
    local($ret,$bytes1,$bytes2,$buf1,$buf2);

    # Open the two files.
    return (1) unless open(FILE1,$name1);
    return (1) unless open(FILE2,$name2);

    $ret = 0;
    while (!eof(FILE1))
    {
	if (eof(FILE2)) 
	{
	    $ret = 1;
	    last;
	}
	$buf1 = "";
	$buf2 = "";
	$bytes1 = read(FILE1,$buf1,16384);
	$bytes2 = read(FILE2,$buf2,16384);
	$buf1 =~ s/\s+/ /g;
	$buf2 =~ s/\s+/ /g;
	if (length($buf1) != length($buf2))
	{
	    $ret = 1;
	    last;
	}
	if ($buf1 ne $buf2)
	{
	    $ret = 1;
	    last;
	}
    }
    close(FILE1);
    close(FILE2);
    return $ret;
}


