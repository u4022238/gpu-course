/* @(#)fileopen.c       1.9 01 Oct 1995 */
/* Routines for passing directories for file opening via the parameter list,
 * and for opening files using this list */

#include <stdlib.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/rhlist.h>
#include <Utilities_cuda/table.h>
#define IFLAG "-I"
#define F_OK 0          /* File exists access mode */

static LIST *dirlist = 0;
static TABLE *filetab = 0;

/* External declaration */
EXTERN_C_FUNCTION (int access, (char *filename, int status));

/* Record type to be located in the file name record */
/* Include in .s file : SFILE_BEGIN */
typedef struct
   {
   char *fname;
   char *expanded_name;
   } fn_record;

/* SFILE_END */

/* Couple of small routines for finding table entries */
FUNCTION_DEF (static int fn_find, (DATA aname, DATA arec))
   {
   return (strcmp ((char *)aname, ((fn_record *)arec)->fname));
   }

FUNCTION_DEF ( static int fn_cmp, (DATA arec1, DATA arec2))
   {
   return (strcmp (((fn_record *)arec1)->fname, ((fn_record *)arec2)->fname));
   }

FUNCTION_DEF ( char *findfile, (
   char *filename               /* The name of the file to find */
   ))
   {
   /* Finds a readable file in the given directory list */
   char *foundname = NULL;      /* Name of the file that we found */

   /* First try to find the file in the file table */
      {
      fn_record *arecord = 
          (fn_record *) tbl_find ((DATA)filename, &filetab, fn_find);
      if (arecord)
         return (arecord->expanded_name);
      }
 
   /* First try to open the unexpanded file name */
   if (access(filename, F_OK) == 0)
      foundname = COPY(filename);
   else
      {
      /* Attempt to find the new file in one of the alternative directories */
      if (filename[0] != '/')
         {
         if (dirlist)
            lst_forall (char *, dname, dirlist)
               {
               char *fname = CONCAT (dname, "/", filename, 0);
               if (access (fname, F_OK) == 0)
                  {
                  foundname = fname;
                  break;
                  }
               free (fname);
               } lst_endall;
         }
      }

   /* Set up a new fn_record */
      {
      fn_record *newrecord = NEW(fn_record);
      newrecord->fname = COPY (filename);
      newrecord->expanded_name = foundname;
      tbl_insert ((DATA)newrecord, &(filetab), fn_cmp);

      /* Return the name of the new record */
      return (newrecord->expanded_name);
      }
   }

#include "Utilities_cuda/bashfopen_dcl.h"

FUNCTION_DEF ( FILE *openinput, (
   char *newfilename,
   char **fullname    /* Returned full name of file with directory prefix */
   ))
   {
   /* This will attempt to open newfilename for read.
    * If newfilename is a relative path name , then the directories
    * in dirlist will be searched in turn to find the first fit.
    * If the file is successfully opened, then the full name of the
    * file is returned in the string fullname.
    */
   FILE *newfile;

   /* Find the file */
   char *fname = findfile (newfilename);

   /* If file is not found, then return NULL */
   if (fname == NULL)
      {
      if (fullname) *fullname = NULL;
      return (NULL);
      }

   /* Attempt to open it */
   newfile = (FILE *) rhfopen (fname, "r");
 
   /* return the appropriate data */
   if (newfile == NULL)
      {
#ifndef __CUDA_ARCH__
      fprintf (stderr, "Unable to read file \"%s\"\n", fname);
#else
      printf ("Unable to read file \"%s\"\n", fname);
#endif

      if (fullname) *fullname = NULL;
      }
   else
      if (fullname) *fullname = fname;

   /* Return the file that has been opened (or NULL) */
   return (newfile);
   }

FUNCTION_DEF ( int getdirlist, (
        int *argc,
        char *argv[]))
   {
   int i;
   int count = 1;

   /* Initialise the directory list */
   dirlist = lst_init();

   /* Now insert directories in it */
   for (i=1; i<*argc; i++)
      {  
      if (strncmp (IFLAG, argv[i], 2) == 0)
         lst_enqueue (COPY(argv[i]+2), dirlist);
      else argv[count++] = argv[i];
      }  

   /* Now update argc and argv */
   argv[count] = (char *)NULL;
   *argc = count;

   /* Return success */
   return 1;
   }   
