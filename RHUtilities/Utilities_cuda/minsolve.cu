/* @(#)minsolve.c       1.7 20 Dec 1994 */
/* Routines for computing the projections that make the joint
 * epipolar projection. 
 */

#include <math.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/determinant_dcl.h>
#include <Utilities_cuda/svd_dcl.h>

FUNCTION_DEF ( void check_min_solve_base1, (
   double **A,                  /* Coefficient matrix */
   double *x,                   /* The solution vector */
   int m, int n                 /* The size of the system */
   ))
   {
   /* Checks the solution, writing out the result */
   int i;
   double error = 0.0;
   double *b = VECTOR (1, m, double);
   matrix_vector_prod_base1 (A, x, m, n, b);
   
   /* Write out the product */
   for (i=1; i<=m; i++)
      {
      fprintf (stdout, "%.6e\n", b[i]);
      error += b[i]*b[i];
      }

   /* Write out the length of the error */
#ifndef __CUDA_ARCH__
   fprintf (stderr, "RMS error = %.6e\n", sqrt (error / m));
#else
   printf ("RMS error = %.6e\n", sqrt (error / m));
#endif

   /* Free the temporary */
   FREE (b, 1);
   }

FUNCTION_DEF ( void min_solve_base1, (
   double **A,          /* Coefficient matrix */
   double *x,           /* The solution vector */
   int m, int n         /* The size of the system */
   ))
   {
   /* Find the x of norm 1 that minimizes || Ax || */
   /* It is assumed that A is of rank n */
   int i;
   double **U = NULL;
   double **V = MATRIX (1, n, 1, n, double);
   double *D  = VECTOR (1, n, double);

   /* Do a SVD */
   sorted_svd_base1 (A, m, n, U, D, V);

   /* Now take the last column of V */
   for (i=1; i<=n; i++)
      x[i] = V[i][n];

   /* Free the matrices */
   FREE (V, 1);
   FREE (D, 1);
   }

FUNCTION_DEF ( void min_solve_direct_base1, (
   double **X,                  /* The system to be solved */
   double *x,                   /* The solution */
   int n                        /* The dimensions of the system */
   ))
   {
   /* Solves an n-1 by n system using determinants */
   int i, j, k;
   double sign;
   double **D = MATRIX (1, n-1, 1, n-1, double);

   /* We solve this by determinants */
   sign = 1.0;
   for (j=1; j<=n; j++)
      {
      /* Fill out D with the j-th cofactor matrix */
      /* Columns before the j-th column */
      for (k=1; k<j; k++)
         for (i=1; i<n; i++)
            D[i][k] = X[i][k];

      /* Columns after the j-th column */
      for (k=j+1; k<=n; k++)
         for (i=1; i<n; i++)
            D[i][k-1] = X[i][k];

      /* Now take the determinant */
      x[j] = sign * determinant_base1 (D, n-1);

      /* Change the sign */
      sign = -sign;
      }

   /* Free the matrix */
   FREE (D, 1);
   }

