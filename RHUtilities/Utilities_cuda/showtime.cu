/* @(#)showtime.c       1.1 16 Jan 1996 */

/* SFILE_BEGIN */
#include <stdio.h>
/* SFILE_END */

#include <time.h>
#include <Utilities_cuda/cvar.h>
#include <string.h>

/*-----------------------------------------------------------------------*/

FUNCTION_DEF (const char *timestring, ())

#ifndef WIN32

   {
   static char timestr[20];
   long n = time(0);
   char *timestring = ctime(&n);
   timestring[19] = '\0';
   strcpy (timestr, timestring);
   return timestr;
   }

#else
   {
   static timestr[20];

   __time64_t ltime;
   _time64(&ltime);

      {
      char *timestring = _ctime64(&ltime);
      timestring[19] = '\0';
      strcpy (timestr, timestring);
      return timestr;
      }
   }
#endif


/*-----------------------------------------------------------------------*/

FUNCTION_DEF (void timed_message, (FILE *afile, const char *string))

/*-----------------------------------------------------------------------*/
#ifndef WIN32

   {
   /* extern char *ctime();
   extern long time(); */
   long n = time(0);
   char *timestring = ctime(&n);
   timestring[19] = '\0';
   fprintf (afile, "%s,   %s", timestring+11, string);
   /* free (timestring);*/
   }


#else

   // Windows version

   {
   //long ltime = time(0);
   __time64_t ltime;
   _time64(&ltime);

      {
      char *timestring = _ctime64(&ltime);
      timestring[19] = '\0';
      fprintf (afile, "%s,   %s", timestring+11, string);
      }
   }

#endif

/*-----------------------------------------------------------------------*/

