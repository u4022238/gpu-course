/* @(#)fibonaci_min.c   1.4 01 Oct 1995 */
/* Routine for finding the minimum of a function by Fibonnacci search */

#include <Utilities_cuda/cvar.h>
#include <math.h>

FUNCTION_DEF ( double fibonnacci_min, (
   double (*f) (double), /* The function to be minimized */
   double low, double high,     /* The bounds in which the minimum lies */
   int (*time_to_stop)  /* The maximum acceptable error */
           (double, double, double, double, double, double)
   ))
   {
   /* Finds the minimum of a function of one variable between two bounds.
    * It assumes a single local minimum.
    * The method used is Fibonnacci search.
    */

   /* First, set the golden ratio */
   double golden = 0.5 * (sqrt(5.0) - 1);

   /* Evaluate the function at reference points */
   double mid = low + golden * (high - low);
   double lowval  = (*f)(low);
   double highval = (*f)(high);
   double midval  = (*f)(mid);

   /* Find the minimum by recursion */
   while (1)
      {
      /* Recursive search for the minimum */
      double newmid, newmidval; /* New division point at value at that point */

      /* See if we have reached convergence */
      if (time_to_stop (low, lowval, mid, midval, high, highval)) 
         {
         if (lowval < mid) return low;
         else if (highval < mid) return high;
         else return mid;
         }
      if (high <= mid || mid <= low) return mid;
      if (midval >= highval && midval >= lowval) return mid;

      /* Otherwise do fibbonnacci division */

      /* There are two cases */
      if(2*mid > high + low)
         {
         /* mid point past half way */
         newmid = low + golden * (mid - low);
         newmidval = (*f)(newmid);

         /* Now choose the new three points depending on result */
         if(newmidval > midval)
            {
            low = newmid; lowval = newmidval;
            }
         else
            {
            high = mid;   highval = midval;
            mid = newmid; midval = newmidval;
            }
         }

      else
         {
         /* mid point before half way */
         newmid = mid + (1.0-golden) * (high - mid);
         newmidval = (*f)(newmid);

         /* Now choose the new three points depending on result */
         if(newmidval > midval)
            {
            high = newmid; highval = newmidval;
            }
         else
            {
            low = mid;    lowval = midval;
            mid = newmid; midval = newmidval;
            }
         }
      }
   }

#ifdef TEST
double parabola (double x)
   {
   /* The minimum of this function is at -1.5 */
   return (x*x);
   }

main ()
   {
   double minimum;
   minimum = fibonnacci_min (parabola, -10.0, 5.0, stop_function);
   printf ("Minimum value at %f = %f\n", minimum, parabola(minimum));
   }
#endif

