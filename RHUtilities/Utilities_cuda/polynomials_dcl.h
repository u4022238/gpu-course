#ifndef _polynomials_c_dcl_
#define _polynomials_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( double evaluate_poly, (
   double *p,		/* The polynomial */
   int n,		/* Its degree */
   double x)		/* The value to evaluate at */
   );

EXTERN_C_FUNCTION ( void poly_product, (
   double *p,		/* The first polynomial */
   int n,		/* Its degree */
   double *q,		/* The second polynomial */
   int m,		/* Its degree */
   double *pq)		/* The product */
   );

EXTERN_C_FUNCTION ( void poly_division, (
   double *a,		/* The dividend */
   int n,		/* Its degree */
   double *b,		/* The divisor */
   int m,		/* Its degree */
   double *q,		/* The quotient */
   double *r)		/* The remainder */
   );

EXTERN_C_FUNCTION ( void cofactor_base1, (
   double **M,			/* The matrix to take a cofactor of */
   int ci, int cj,		/* The row and column to remove */
   int n,			/* The dimension */
   double **cof)		/* The cofactor matrix */
   );

EXTERN_C_FUNCTION ( void polydet_base1, (
   double **Y1,		/* Matrix of constant terms */
   double **Yk,		/* Matrix of first degree terms */
   int n,		/* Size of the determinant */
   double *det)		/* The determinant as a polynomial */
   );

EXTERN_C_FUNCTION ( void characteristic_polynomial_base1, (
   double **A, 
   int n, 
   double *det));

#endif
