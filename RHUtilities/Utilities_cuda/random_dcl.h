#ifndef _random_c_dcl_
#define _random_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION (long rh_random, ());

EXTERN_C_FUNCTION (void rh_srandom, ( unsigned int n ));

EXTERN_C_FUNCTION ( double rh_irandom, ());

EXTERN_C_FUNCTION (long rh_random, ());

EXTERN_C_FUNCTION (void rh_srandom, ( unsigned int n ));

EXTERN_C_FUNCTION ( double rh_irandom, ());

EXTERN_C_FUNCTION (double urandom, (double mean, double radius));

EXTERN_C_FUNCTION ( double grandom, (
        double mean,
        double dev));

EXTERN_C_FUNCTION (int random_choice, (double frac));

EXTERN_C_FUNCTION ( void circular_random, (double *x, double *y));

#endif
