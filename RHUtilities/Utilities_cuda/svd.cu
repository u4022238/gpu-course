/* @(#)svd.c    1.9 29 Nov 1995 */
/* Routines for computing the projections that make the joint
 * epipolar projection. 
 */

#include <math.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/qsort_cmp_dcl.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/svalue3_dcl.h>

#if 0
FUNCTION_DEF ( static int ptrcmp, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two double pointers based on the value they point to */
   double val = **((double **)ptr2) - **((double **)ptr1);
   if (val > 0) return 1;
   else if (val < 0) return -1;
   else return (0);
   }

FUNCTION_DEF ( static int ptrcmp_double, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two double pointers based on the value they point to */
   double val = **((double **)ptr2) - **((double **)ptr1);
   if (val > 0) return 1;
   else if (val < 0) return -1;
   else return (0);
   }

FUNCTION_DEF ( static int ptrcmp_float, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two float pointers based on the value they point to */
   float val = **((float **)ptr2) - **((float **)ptr1);
   if (val > 0) return 1;
   else if (val < 0) return -1;
   else return (0);
   }
#endif

FUNCTION_DEF ( void sort_svd_base1, (
   double **U, double *D, double **V,   /* Components of a SVD */
   int m, int n                         /* Sizes of the U and V matrices */
   ))
   {
   /* Rearranges the singular value decomposition so that the
    * singular values are in descending order */
   int i;
   double **perm = VECTOR (1, n, double *);
   int *index    = VECTOR (1, n, int);

   /* Set up perm pointing into the D array */
   for (i=1; i<=n; i++)
      perm[i] = &D[i]; 

   /* Now sort the array perm */
   qsort ((char *)(perm+1), n, sizeof (double *), double_ptrcmp);

   /* Now from this get the index array */
   for (i=1; i<=n; i++)
      index[i] = perm[i] - D;

   /* Now rearrange the elements of D */
      {
      int i;
      double *Dcopy = VECTOR (1, n, double);
      for (i=1; i<=n; i++) Dcopy[i] = D[i];
      for (i=1; i<=n; i++) D[i] = Dcopy[index[i]];
      FREE (Dcopy, 1);
      }

   /* Rearrange the columns of U */
   if (U)
      {
      int i, j;
      double **Ucopy = MATRIX (1, m, 1, n, double);
      for (i=1; i<=m; i++) for (j=1; j<=n; j++) 
         Ucopy[i][j] = U[i][j];
      for (i=1; i<=m; i++) for (j=1; j<=n; j++)
         U[i][j] = Ucopy[i][index[j]];
      FREE (Ucopy, 1);
      }
   
   /* Rearrange the columns of V */
   if (V)
      {
      int i, j;
      double **Vcopy = MATRIX (1, n, 1, n, double);
      for (i=1; i<=n; i++) for (j=1; j<=n; j++) 
         Vcopy[i][j] = V[i][j];
      for (i=1; i<=n; i++) for (j=1; j<=n; j++)
         V[i][j] = Vcopy[i][index[j]];
      FREE (Vcopy, 1);
      }

   /* Free the temporaries */
   FREE (perm, 1);
   FREE (index,1);
   }
   
FUNCTION_DEF ( int sorted_svd_base1, (
   double **A,                          /* The matrix to be decomposed */
   int m, int n,                        /* Size of the matrix */
   double **U, double *D, double **V    /* Components of the SVD */
   ))
   {
   /* Does a SVD.  The Singular Values will be sorted.
    *
    * This works for any size of input matrices
    * The output matrices will be of the following sizes :
    *           U : m x n
    *           D : n
    *           V : n x n
    *
    * Thus, the output matrix U has the same dimension as A.
    * If desired, them may be the same matrix.
    */

   double **Atemp;
   int nrows;
   int i, j;

   /* If A has fewer rows than columns, then we must add zero rows */
   if (m < n)
      {
      Atemp = MATRIX (1, n, 1, n, double);
      nrows = n;
      }
   else if (U == NULL)
      {
      Atemp = MATRIX (1, m, 1, n, double);
      nrows = m;
      }
   else 
      {
      Atemp = U;
      nrows = m;
      }

   /* Copy A to U */
   for (i=1; i<=m; i++)
      for (j=1; j<=n; j++)
         Atemp[i][j] = A[i][j];
   
   /* Fill with zeros the last rows */
   for (i=m+1; i<=nrows; i++)
      for (j=1; j<=n; j++)
         Atemp[i][j] = 0.0;

   /* Now do the SVD -- reqires u or v as needed by parent */
   if (! svdcmp_base1 (Atemp, nrows, n, D, V, (U!=NULL))) return 0;

   /* If necessary, we must copy Atemp back to U */
   if (U && (Atemp != U))
      {
      /* Copy */
      for (i=1; i<=m; i++)
         for (j=1; j<=n; j++)
            U[i][j] = Atemp[i][j];
      }

   /* Free Atemp, unless it is the same as U */
   /* Free the temporary */
   if (Atemp != U)
      FREE (Atemp, 1);

   /* Sort it */
   sort_svd_base1 (U, D, V, m, n);

   return (1);
   }

