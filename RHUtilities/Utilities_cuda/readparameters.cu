/* @(#)readparameters.c 1.6 01 Oct 1995 */
/*>>
Routine for initializing the parameters.
The parameters are read from a file which is passed to the routine.
<<*/


#include <Utilities_cuda/cvar.h>
#include <io.h>
#include <ctype.h>

/* Forward definitions */
static rhBOOLEAN test_init_variables();
static void uninit_variables();

static rhBOOLEAN isinteger (string)
   char *string;
   {
   char *p = string;
   
   /* Skip a possible sign */
   if (*p == '-') p++;

   /* Now test that all characters are digits */
   while (*p)
      if (! isdigit(*(p++))) return (FALSE);

   /* If we get this far, it is a digit */
   return (TRUE);
   }

static rhBOOLEAN isfloat (string)
   char *string;
   {
   char *p = string;
   rhBOOLEAN dot = FALSE;

   /* Skip a possible sign */
   if (*p == '-') p++;

   /* Now test that all characters are digits with one decimal point */
   while (*p)
     {   
      if (! isdigit(*p) && (dot || (*p != '.'))) return (FALSE);
      if (*p == '.') dot = TRUE;
      p++;
     }    

   /* If we get this far, it is a float */
   return (TRUE);
   }

static char *removequotes (string)
   char *string;
   {
   /* Removes the quotes from the string. Returns NULL if not a good string */
   char *p = string;
   char *str;                   /* To be returned */

   /* Test that the string starts with a quote */
   if (p == NULL || *p != '"') return NULL;

   /* Copy the string */
   p = str = COPY(string+1);

   /* Look for the final quote in the string */
   while (*p != '"' && *p != '\0') p++;
   
   /* If there is no quote, then return NULL */
   if (*p == '\0')
      {
      free (str);
      return (NULL);
      }

   /* Otherwise, remove the quote */
   *p = '\0';
   return (str);
   }
   
init_parameters( filename )
   char *filename;
   {
   /* The main control routine for reading pad order from a file */
   rhBOOLEAN founderrors = FALSE;
   FILE *parameterfile; /* File to read from */

   /* First open the file */
   parameterfile = FOPEN (filename, "r");

   /* Set the variables to uninitialized values */
   uninit_variables ();

   /* Init the io routines */
   _io_init (parameterfile, filename);

   /* Read the file */
   while ( _nextline(_infile) != EOF)
      {
      if (NF == 0) continue;
      if (NF == 1)
         {
#ifndef __CUDA_ARCH__
         fprintf (stderr, "ERROR : %s : \n", filename);
#else
         printf ("ERROR : %s : \n", filename);
#endif
         _reporterror();
         founderrors = TRUE;
         }
      else 
         {
         /* Otherwise try the various variables */
         founderrors |= ! tryvariables (); 
         } 
      }

   /* If there were errors, then exit */
   if (founderrors)
      {
      bail_out(1);
      }

   /* Test that all the variables were initialized */
   if (test_init_variables())
      bail_out (1);

   /* Clean up the io routines */
   fclose (parameterfile);
   _io_cleanup();
   }

static int dosetchar (var, name)
   char **var;
   char *name;
   {
   /* Attempts to set a double precision variable */
   if (strcmp(_[1], name) == 0)         
      {                                 
      char *temp = removequotes (_[2]); 
      if (temp == NULL)                 
         {                                      
#ifndef __CUDA_ARCH__
         fprintf (stderr, "ERROR : Bad string parameter.\n");   
#else
         printf ("ERROR : Bad string parameter.\n");    
#endif
         _reporterror();                        
         return (1);                    
         }                                      
      *var = temp;                              
      return (2);                       
      }
   return (0);
   }

static int dosetint (var, name)
   int *var;
   char *name;
   {
   /* Attempts to set an integer parameter */
   if (strcmp(_[1], name) == 0)         
      {                                 
      if (! isinteger(_[2]))            
         {                                      
#ifndef __CUDA_ARCH__
         fprintf (stderr, "ERROR : Bad integer parameter.\n");  
#else
         printf ("ERROR : Bad integer parameter.\n");   
#endif
         _reporterror();                        
         return (1);                    
         }                                      
      *var = atoi(_[2]);                        
      return (2);                       
      }
   return (0);
   }

static int dosetdouble (var, name)
   double *var;
   char *name;
   {                                       
   double atof();                          
   if (strcmp(_[1], name) == 0)          
      {                                    
      if (! isfloat(_[2]))                 
         {                                 
#ifndef __CUDA_ARCH__
         fprintf (stderr, "ERROR : Bad real parameter.\n");        
#else
         printf ("ERROR : Bad real parameter.\n");        
#endif
         _reporterror();                   
         return (1);                   
         }                                 
      *var = (double) atof(_[2]);          
      return (2);                       
      }                                    
   return (0);
   }

/*>>
Declarations of the variables 
<<*/
#undef tryint
#undef trychar
#undef trydouble
#undef setint
#undef setchar
#undef setdouble

#define tryint(var) extern int var
#define trychar(var) extern char *var
#define trydouble(var) extern double var
#define setint(var) extern int var
#define setchar(var) extern char *var
#define setdouble(var) extern double var

#include <Utilities_cuda/globvar_macros.h>

/*>>
Here the macros must be redefined to initialize variables.
<<*/
#undef tryint
#undef trychar
#undef trydouble
#undef setint
#undef setchar
#undef setdouble

/* Define uninitialized values */
#define INTXX 0xefefefef
#define CHARXX (char *)NULL
#define DOUBLEXX 0xefefefefefefefef

#define tryint(var)
#define trychar(var)
#define trydouble(var)
#define setint(var)  var = INTXX;
#define setchar(var) var = CHARXX;
#define setdouble(var) var = DOUBLEXX;
 
static void uninit_variables ()
   {
   /* sets all the variables to uninitialized values */
   /* Format of elevation file */
#include <Utilities_cuda/globvar_macros.h>
   }

/*>>
Here we define some macros for setting variable values.
<<*/
#undef tryint
#undef trychar
#undef trydouble
#undef setint
#undef setchar
#undef setdouble

/* First some macro definitions to make the following code easy */
#define trychar(name)                           \
        if (n = dosetchar (&name, "name")) return (n-1);
#define tryint(name)                            \
        if (n = dosetint (&name, "name")) return (n-1);
#define trydouble(name)                          \
        if (n = dosetdouble (&name, "name")) return (n-1);
#define setint(var)     tryint(var)
#define setchar(var)    trychar(var)
#define setdouble(var)  trydouble(var)

static int tryvariables ()
   {
   /* Run through the various parameters testing if this is the one */
   /* Returns FALSE if there are errors, otherwise TRUE */
   int n;

/* Look for the variable */
#include <Utilities_cuda/globvar_macros.h>

   /* Otherwise, the variable is not recognized */
#ifndef __CUDA_ARCH__
   fprintf (stderr, "ERROR : Parameter name not recognized.\n");
#else
   printf ("ERROR : Parameter name not recognized.\n");
#endif
   _reporterror();
   return (FALSE);
   }

/*>> 
Redefine the macros yet again to test if the variables have been initialized 
<<*/
#undef tryint
#undef trychar
#undef trydouble
#undef setint
#undef setchar
#undef setdouble

#define tryint(var)
#define trychar(var)
#define trydouble(var)
#define setint(var)                                                     \
        if ((var) == INTXX)                                             \
           {                                                            \
#ifndef __CUDA_ARCH__
           fprintf (stderr, "Variable \"var\" not initialized\n");      \
#else
           printf ("Variable \"var\" not initialized\n");       \
#endif
           founderrors = TRUE;                                          \
           }                                                            \

#define setchar(var)                                                    \
        if ((var) == CHARXX)                                            \
           {                                                            \
#ifndef __CUDA_ARCH__
           fprintf (stderr, "Variable \"var\" not initialized\n");      \
#else
           printf ("Variable \"var\" not initialized\n");       \
#endif
           founderrors = TRUE;                                          \
           }                                                            \

#define setdouble(var)                                                  \
        if ((var) == DOUBLEXX)                                          \
           {                                                            \
#ifndef __CUDA_ARCH__
           fprintf (stderr, "Variable \"var\" not initialized\n");      \
#else
           printf ("Variable \"var\" not initialized\n");       \
#endif
           founderrors = TRUE;                                          \
           }                                                            \

static rhBOOLEAN test_init_variables ()
   {
   /* sets all the variables to uninitialized values */
   /* Format of elevation file */
   rhBOOLEAN founderrors = FALSE;
#include <Utilities_cuda/globvar_macros.h>
   return (founderrors);
   }

