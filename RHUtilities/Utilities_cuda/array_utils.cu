/* @(#)array_utils.c    1.18 11/26/96 */

/*>>
Utility routines for the Numerical recipes cookbook programs 
<<*/

#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <math.h>
#include <Utilities_cuda/gaussj_dcl.h>
#include <Utilities_cuda/symmetric_dcl.h>

FUNCTION_DEF ( char **alloca_matrix, (
        int nrl,
        int nrh,
        int ncl,
        int nch,
        int entry_size,
        char **m))
   {
   /* Allocates a matrix with the range [nrl .. nrh][ncl .. nch] */
   int i;
   char *start;                 /* Address of the [0][0] entry */

   int rdim = nrh - nrl + 1;    /* # entries in a row */
   int cdim = nch - ncl + 1;    /* # entries in a column */

   /* Adjust the start of arrays, and adjust m */
   start = (char *) (m + rdim); /* Count forward rdim (MTYPE *) */
   start -= (ncl + nrl * cdim) * entry_size; /* Count back units of entry_size*/
   m -= nrl;

   /* Fill out the addresses */
   for (i=nrl; i<=nrh; i++)
      m[i] = &(start[i*cdim*entry_size]);

   /* Return pointer to the pointer array */
   return (m);
   }

FUNCTION_DEF ( char **malloc_matrix, (
        int nrl,
        int nrh,
        int ncl,
        int nch,
        int entry_size))
   {
   /* Allocates a matrix with the range [nrl .. nrh][ncl .. nch] */
   int i;
   char **m;
   char *start;                 /* Address of the [0][0] entry */

   int rdim = nrh - nrl + 1;    /* # entries in a row */
   int cdim = nch - ncl + 1;    /* # entries in a column */
   int nptrs = rdim;
   int size;

   /*
    * We must make sure that double words are allocated on double word
    * boundaries
    */
   /* We must make sure that we have an even number of pointers */
   if (entry_size > sizeof(char *) && nptrs % 2 != 0)
      nptrs += 1;
      
   /* Now, compute the size of space to be allocated */
   size = nptrs*sizeof(char *) + rdim*cdim*entry_size;

   /* Allocate m */
   m = (char **) MALLOC(size);
   start = (char *) (m + nptrs);             /* Count forward nptrs (MTYPE *) */
   start -= (ncl + nrl * cdim) * entry_size; /* Count back units of entry_size*/
   m -= nrl;

   /* Fill out the addresses */
   for (i=nrl; i<=nrh; i++)
      m[i] = &(start[i*cdim*entry_size]);

   /* Return pointer to the pointer array */
   return (m);
   }

FUNCTION_DEF ( void *malloc_array3D, (
        int ilow, int ihigh,
        int jlow, int jhigh,
        int klow, int khigh,
        int entry_size))
   {
   /* Allocates an array with the range 
        [ilow--ihigh][jlow--jhigh][klow--khigh] */

   int i, j;

   int idim = ihigh - ilow + 1; /* # entries in a row */
   int jdim = jhigh - jlow + 1; /* # entries in a column */
   int kdim = khigh - klow + 1; /* # entries in a column */
   int niptrs = idim;
   int njptrs = idim * jdim;
   int nptrs = niptrs + njptrs;

   /*
    * We must make sure that double words are allocated on double word
    * boundaries
    */
   /* We must make sure that we have an even number of pointers */
   if (entry_size > sizeof(char *) && nptrs % 2 != 0)
      nptrs += 1;
      
   /* Now, compute the size of space to be allocated */
      {
      int size = nptrs*sizeof(char *) + idim*jdim*kdim*entry_size;

      /* Allocate m */
      char ***m = (char ***) MALLOC(size);

      /* Get the start of the pointers, etc */
      char ***start_iptrs = m;
      char **start_jptrs = (char **) (m + niptrs);
      char *start_data   = (char *)  (m +  nptrs);

      /* Get the nominal positions for the 0-th iptr, jptr, data */
      char ***iptr0 = start_iptrs - ilow;
      char **jptr0  = start_jptrs - (ilow*jdim + jlow);
      char *data0 = start_data  -(ilow*jdim*kdim + jlow*kdim + klow)*entry_size;

      /* Now, fill in the i-ptrs */
      char ***iptrs = iptr0;
      for (i=ilow; i<=ihigh; i++)
         iptrs[i] = jptr0 + i*jdim;

      /* Now, fill out all the j-ptrs */
      for (i=ilow; i<=ihigh; i++)
         for (j=jlow; j<=jhigh; j++)
             iptrs[i][j] = data0 + (i*jdim*kdim + j*kdim) * entry_size;

      /* Return pointer to the pointer array */
      return ((void *)iptr0);
      }
   }

FUNCTION_DEF ( void fprint_intmatrix, (
        char *format,
        FILE *afile,
        int **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of int values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_shortmatrix, (
        char *format,
        FILE *afile,
        short **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of short values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_floatmatrix, (
        char *format,
        FILE *afile,
        float **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of float values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_doublematrix, (
        char *format,
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of double values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_matrix, (
        const char *format,
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of double values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_matrix, (
        const char *format,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of double values to stdout */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         printf (format, a[i][j]);
      printf ("\n");
      }
   printf ("\n");
   }

FUNCTION_DEF ( void print_matrix, (
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of double values to stdout */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      int count = 0;
      for (j=ncl; j<=nch; j++)
         {
         if (++count == 8)
            {
            printf ("\n\t");
            count = 1;
            }
         printf ("12.6f ", a[i][j]);
         }
      printf ("\n");
      }
   printf ("\n");
   }

FUNCTION_DEF ( void print_matrix, (
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch))
   {
   /* Print out a matrix of double values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      int count = 0;
      for (j=ncl; j<=nch; j++)
         {
         if (++count == 8)
            {
            fprintf (afile, "\n\t");
            count = 1;
            }
         fprintf (afile,"%12.6f ", a[i][j]);
         }
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( int p_matrix_base1, ( double **a, int m, int n))
   {
   /* Print out a matrix of double values */
   /* Returns a value, so we can display at break points at break points */
   fprint_matrix ("%12.5e ", a, 1, m, 1, n);
   return 1;
   }

FUNCTION_DEF ( void fprint_vector, (
        const char *format,
        FILE *afile,
        double *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      fprintf (afile, format, v[i]);
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_vector, (
        const char *format,
        double *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      printf (format, v[i]);
   printf ("\n");
   }

FUNCTION_DEF ( void fprint_intvector, (
        char *format,
        FILE *afile,
        int *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      fprintf (afile, format, v[i]);
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void fprint_floatvector, (
        char *format,
        FILE *afile,
        float *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      fprintf (afile, format, v[i]);
   fprintf (afile, "\n");
   }

FUNCTION_DEF ( void print_vector, (
        FILE *afile,
        double *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         fprintf (afile, "\n\t");
         count = 1;
         }
      fprintf (afile, "%9.6f ", v[i]);
      }
   fprintf (afile, "\n\n");
   }

FUNCTION_DEF ( void print_floatvector, (
        FILE *afile,
        float *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         fprintf (afile, "\n\t");
         count = 1;
         }
      fprintf (afile, "%9.6f ", v[i]);
      }
   fprintf (afile, "\n\n");
   }

FUNCTION_DEF ( void print_intvector, (
        FILE *afile,
        int *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         fprintf (afile, "\n\t");
         count = 1;
         }
      fprintf (afile, "%9d ", v[i]);
      }
   fprintf (afile, "\n\n");
   }

FUNCTION_DEF ( int p_vector_base1, ( double *a, int n))
   {
   /* Print out a vector of double values */
   /* Returns a value, so we can display at break points at break points */
   fprint_vector ("%12.5e ", a, 1, n);
   return 1;
   }

FUNCTION_DEF ( void matrix_prod_base1, (
        double **A,
        double **B,
        int l,
        int m,
        int n,
        double **X))
   {
   /* Multiplies a k x m matrix (A) by an m x n matrix (B) to get matrix X */
   /* All indices start at 1 */
   /* The goal matrix may be the same as one of the operands */
   int i, j, k;
   double **T = MATRIX (1, l, 1, n, double);     /* Temporary product */

   /* Clear the matrix */
   for (i=1; i<=l; i++)
      for (j=1; j<=n; j++)
         T[i][j] = 0.0;

   /* Do the multiplication */
   for (i=1; i<=l; i++)
      for (j=1; j<=n; j++)
         for (k=1; k<=m; k++)
            T[i][j] += A[i][k] * B[k][j];

   /* Clear the matrix */
   for (i=1; i<=l; i++)
      for (j=1; j<=n; j++)
         X[i][j] = T[i][j];

   /* Free T */
   FREE (T, 1);
   }


FUNCTION_DEF ( void vector_matrix_prod_base1, (
        double *b,
        double **A,
        int l,
        int m,
        double *x))
   {
   /* Multiplies a vector (b) by an  l x m matrix (A) to get vector x */
   /* All indices start at 1 */
   /* The goal vector may be the same as b */
   int i, k;
   double *T = VECTOR (1, m, double);     /* Temporary product */

   /* Clear the vector */
   for (i=1; i<=m; i++)
      T[i] = 0.0;

   /* Do the multiplication */
   for (i=1; i<=m; i++)
      for (k=1; k<=l; k++)
         T[i] += b[k] * A[k][i];

   /* Transfer the vector */
   for (i=1; i<=m; i++)
      x[i] = T[i];

   /* Free T */
   FREE (T, 1);
   }

FUNCTION_DEF ( void matrix_vector_prod_base1, (
        double **A,
        double *b,
        int l,
        int m,
        double *x))
   {
   /* Multiplies a l x m matrix (A) by an m vector (b) to get vector x */
   /* All indices start at 1 */
   /* The goal vector may be the same as b */
   int i, k;
   double *T = VECTOR (1, l, double);     /* Temporary product */

   /* Clear the vector */
   for (i=1; i<=l; i++)
      T[i] = 0.0;

   /* Do the multiplication */
   for (i=1; i<=l; i++)
      for (k=1; k<=m; k++)
         T[i] += A[i][k] * b[k];

   /* Transfer the vector */
   for (i=1; i<=l; i++)
      x[i] = T[i];

   /* Free T */
   FREE (T, 1);
   }


FUNCTION_DEF ( double inner_product_base1, (
        double *A,
        double *B,
        int n))
   {
   /* Returns the inner product of two vectors */
   int i;
   double prod = 0.0;

   /* Do the multiplication */
   for (i=1; i<=n; i++)
      prod += A[i] * B[i];

   /* Return the product */
   return (prod);
   }

FUNCTION_DEF ( double vector_length_base1, (
        double *A,
        int n))
   {
   /* Returns the length of the vector */
   return (sqrt (inner_product_base1 (A, A, n)));
   }

FUNCTION_DEF ( void transpose, (
   double **A,          /* The matrix to be transposed */
   double **At,         /* Its transpose (returned) */
   int nrl, int nrh,    /* X dimension */
   int ncl, int nch     /* Y dimension */
   ))
   {
   /* Does the transpose of a matrix.           */
   /* Goal and origin may be the same.          */
   int i, j;
   double **temp;
   int del = 0;
   
   /* First does the diagonal entries */
   if (A == At)
      {
      temp = MATRIX (nrl, nrh, ncl, nch, double);
      del = 1;
      for (i=nrl; i<=nrh; i++) for (j=ncl; j<=nch; j++)
          temp[i][j] = A[i][j];
      }
   else temp = A;

   /* Now swaps the off-diagonal entries */
   for (i=nrl; i<=nrh; i++) for (j=ncl; j<=nch; j++)
      At[j][i] = temp[i][j];

   /* Free the matrix if required */
   if(del) FREE(temp, nrl);
   }


FUNCTION_DEF ( int matrix_inverse_base1, (
   double **A,          /* The matrix to be inverted */
   double **Ainv,       /* The inverse computed */
   int n                /* Dimension of the matrix */
   ))
   {
   /* Inverts the matrix */
   /* Returns 0 if matrix is not invertible */
   int i, j;
   
   /* First copy the matrix to Ainv */
   for (i=1; i<=n; i++) for (j=1; j<=n; j++)
      Ainv[i][j] = A[i][j];

   /* Now invert it on the spot */
   return gaussj2_check_base1 (Ainv, n, (double **)0, 0);
   }

FUNCTION_DEF ( char **sub_matrix, (
   char **A,                    /* Base array */
   int nrl, int ncl,            /* Origin of the new array */
   int rdim,                    /* Number of rows of the sub-array */
   int orgi, int orgj,          /* Starting index for new array */
   int entry_size               /* Size of an entry */
   ))
   {
   /* Allocates a sub-matrix of A with the given range */
   int i;
   char **m;
   int offset;

   int size = rdim*sizeof(char *);

   /* Allocate m */
   m = (char **) MALLOC(size);
   m -= nrl ;

   /* Fill out the addresses */
   offset = (ncl-orgj) * (entry_size/sizeof(char));
   for (i=nrl; i<nrl+rdim; i++)
      m[i] = A[i] + offset;

   /* Return pointer to the pointer array */
   return (m+nrl-orgi);
   }

FUNCTION_DEF ( double det3x3_base1, (
        double **A))
   {
   /* Takes a 3 by 3 determinant */
   return A[1][1]*A[2][2]*A[3][3] + A[1][2]*A[2][3]*A[3][1] + 
          A[1][3]*A[2][1]*A[3][2] - A[1][1]*A[2][3]*A[3][2] - 
          A[1][2]*A[2][1]*A[3][3] - A[1][3]*A[2][2]*A[3][1];
   }

FUNCTION_DEF ( double det2x2_base1, (
        double **A))
   {
   /* Takes a 2 by 2 determinant */
   return A[1][1]*A[2][2] - A[1][2]*A[2][1];
   }

FUNCTION_DEF ( void cofactor_matrix_3x3_base1, (
        double **A,
        double **Aadj))
   {
   /* Takes the cofactor matrix of a given 3x3 matrix */
   int i, j;
   
   /* First, take a copy of A */
   double **A2 = MATRIX (1, 3, 1, 3, double);
   for (i=1; i<=3; i++) for (j=1; j<=3; j++)
      A2[i][j] = A[i][j];

   /* Now take the cofactors */
   Aadj[1][1] = A2[2][2]*A2[3][3] - A2[2][3]*A2[3][2];
   Aadj[1][2] = A2[2][3]*A2[3][1] - A2[2][1]*A2[3][3];
   Aadj[1][3] = A2[2][1]*A2[3][2] - A2[2][2]*A2[3][1];

   Aadj[2][1] = A2[3][2]*A2[1][3] - A2[3][3]*A2[1][2];
   Aadj[2][2] = A2[3][3]*A2[1][1] - A2[3][1]*A2[1][3];
   Aadj[2][3] = A2[3][1]*A2[1][2] - A2[3][2]*A2[1][1];

   Aadj[3][1] = A2[1][2]*A2[2][3] - A2[1][3]*A2[2][2];
   Aadj[3][2] = A2[1][3]*A2[2][1] - A2[1][1]*A2[2][3];
   Aadj[3][3] = A2[1][1]*A2[2][2] - A2[1][2]*A2[2][1];

   /* Free the temporary matrix */
   FREE (A2, 1);
   }

FUNCTION_DEF ( void matrix_copy, (
   double **A, double **B,      /* The matrices to copy from and to */
   int i0, int i1,              /* Row bounds */
   int j0, int j1               /* Column bounds */
   ))
   {
   /* Copies a matrix */
   int i, j;
   for (i=i0; i<=i1; i++)  
      for (j=j0; j<=j1; j++)
         B[i][j] = A[i][j];
   }
   
FUNCTION_DEF ( void cross_product_base1, (
   double *A, double *B,        /* Two vectors 1..3 */
   double *C                    /* Their cross product */
   ))
   {
   C[1] = A[2]*B[3] - A[3]*B[2];
   C[2] = A[3]*B[1] - A[1]*B[3];
   C[3] = A[1]*B[2] - A[2]*B[1];
   }

FUNCTION_DEF ( int lin_solve_base1, (
   double **A,          /* The matrix of coefficients */
   double *x,           /* The vector of unknowns */
   double *b,           /* The goal vector */
   int n                /* Size of the system */
   ))
   {
   /* Solves a square linear system of equations */
   int i, j;
   int return_val;
   double **A2 = MATRIX (1, n, 1, n, double);
   double *b2  = VECTOR (1, n, double);

   /* Copy the values */
   for (i=1; i<=n; i++) for (j=1; j<=n; j++)
      A2[i][j] = A[i][j];
   for (i=1; i<=n; i++)
      b2[i] = b[i];

   /* Now solve using gaussj */
   return_val = gaussj_check_base1 (A2, n, b2);

   /* Now copy the values */
   if (return_val)
      for (i=1; i<=n; i++)
         x[i] = b2[i];

   /* Free the temporary matrices */
   FREE (A2, 1);
   FREE (b2, 1);

   /* Return the appropriate value */
   return return_val;
   }
   
FUNCTION_DEF ( int lin_solve_symmetric_base1, (
   double **A,          /* The matrix of coefficients */
   double *x,           /* The vector of unknowns */
   double *b,           /* The goal vector */
   int n                /* Size of the system */
   ))
   {
   /* Solves a square linear system of equations */
   int i, j;
   int return_val;
   double **A2 = MATRIX (1, n, 1, n, double);
   double *b2  = VECTOR (1, n, double);

   /* Copy the values */
   for (i=1; i<=n; i++) for (j=1; j<=n; j++)
      A2[i][j] = A[i][j];
   for (i=1; i<=n; i++)
      b2[i] = b[i];

   /* Now solve using gaussj */
   return_val = solve_symmetric_base1 (A2, n, b2);

   /* Now copy the values */
   if (return_val)
      for (i=1; i<=n; i++)
         x[i] = b2[i];

   /* Free the temporary matrices */
   FREE (A2, 1);
   FREE (b2, 1);

   /* Return the appropriate value */
   return return_val;
   }
   
FUNCTION_DEF ( void identity_matrix_base1, (
        double **A,
        int dim))
   { 
   /* Initializes the matrix A to be the identity */
   int i, j;
   for (i=1; i<=dim; i++) 
      { 
      A[i][i] = 1.0; 
      for (j=i+1; j<=dim; j++)
         A[i][j] = A[j][i] = 0.0;
      } 
   } 

FUNCTION_DEF ( void swap_columns, (
   double **M,          /* The matrix whose columns are to be swapped */
   int rowl, int rowh,  /* Bounds of row indices */
   int col1, int col2   /* Columns to be swapped */
   ))
   {
   /* Swaps two columns of a matrix */
   double temp;
   int i;
   for (i=rowl; i<=rowh; i++)
      {
      temp = M[i][col1];
      M[i][col1] = M[i][col2];
      M[i][col2] = temp;
      }
   }

//---------------------------

FUNCTION_DEF ( void fprint_intvector, (
        char *format,
        int *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      printf (format, v[i]);
   printf ("\n");
   }

FUNCTION_DEF ( void fprint_floatvector, (
        char *format,
        float *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      printf (format, v[i]);
   printf ("\n");
   }

FUNCTION_DEF ( void print_vector, (
        double *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         printf ("\n\t");
         count = 1;
         }
      printf ("%9.6f ", v[i]);
      }
   printf ("\n\n");
   }

FUNCTION_DEF ( void print_floatvector, (
        float *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         printf ("\n\t");
         count = 1;
         }
      printf ("%9.6f ", v[i]);
      }
   printf ("\n\n");
   }

FUNCTION_DEF ( void print_intvector, (
        int *v,
        int nrl,
        int nrh))
   {
   /* Print out a vector of double values */
   int i;
   int count = 0;
   for (i=nrl; i<=nrh; i++)
      {
      if (++count == 8)
         {
         printf ("\n\t");
         count = 1;
         }
      printf ("%9d ", v[i]);
      }
   printf ("\n\n");
   }


