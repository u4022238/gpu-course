/*
 * Module Purpose: Contains the standard error routine used by the parser.
 *
 * Creation Date:  87/10/23
 *
 * Compiler Version:
 *
 * Hardware Used:  SUN 3
 *
 * Operating Systems:  UNIX -- SUN Release 3.4
 *
 *
 * Class 3: General Electric Proprietary Data
 * Copyright (C) 1987 General Electric Company
 *
 */

#include <Utilities_cuda/cvar.h>
#include <stdio.h>
/* SFILE_BEGIN */
#include <Utilities_cuda/rh_util.h>
/* SFILE_END */

#ifndef CPARSE_RIH
#   include <stdarg.h>
#endif

// Host
#ifndef __CUDA_ARCH__

static rhBOOLEAN output_informative_messages = FALSE;
static rhBOOLEAN output_warning_messages = TRUE;
static rhBOOLEAN output_error_messages = TRUE;
static rhBOOLEAN output_debugging_messages = FALSE;
static FILE *error_file = (FILE *)0;
static FILE *warning_file = (FILE *)0;
static FILE *debugging_file = (FILE *)0;
static FILE *info_file = (FILE *)0;

/* Routines for setting the error enable flags */

__host__ void set_error_output_file (FILE *afile)
   {
   error_file = afile;
   }

__host__ FILE *error_output_file ()
   {
   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   return error_file;
   }

__host__ void set_info_output_file (FILE *afile)
   {
   info_file = afile;
   }

__host__ FILE *info_output_file ()
   {
   /* Set default for info_file */
   if (info_file == (FILE *) 0) info_file = stdout;

   return info_file;
   }

__host__ void set_warning_output_file (FILE *afile)
   {
   warning_file = afile;
   }

__host__ FILE *warning_output_file ()
   {
   /* Set default for warning_file */
   if (warning_file == (FILE *) 0) warning_file = stderr;

   return warning_file;
   }

__host__ void set_debugging_output_file (FILE *afile)
   {
   debugging_file = afile;
   }

__host__ FILE *debugging_output_file ()
   {
   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   return debugging_file;
   }

__host__ rhBOOLEAN enable_debugging_messages ()
   {
   rhBOOLEAN oldvalue = output_debugging_messages;

   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   output_debugging_messages = TRUE;
   // fprintf (debugging_file, "enable_debugging_messages\n");
   return oldvalue;
   }

__host__ rhBOOLEAN enable_informative_messages ()
   { 
   rhBOOLEAN oldvalue = output_informative_messages;
   output_informative_messages = TRUE; 
   return oldvalue;
   }

__host__ rhBOOLEAN enable_warning_messages ()
   { 
   rhBOOLEAN oldvalue = output_warning_messages;
   output_warning_messages = TRUE; 
   return oldvalue;
   }

__host__ rhBOOLEAN enable_error_messages ()
   { 
   rhBOOLEAN oldvalue = output_error_messages;
   output_error_messages = TRUE; 
   return oldvalue;
   }

__host__ rhBOOLEAN disable_informative_messages ()
   { 
   rhBOOLEAN oldvalue = output_informative_messages;
   output_informative_messages = FALSE; 
   return oldvalue;
   }

__host__ rhBOOLEAN disable_warning_messages ()
   { 
   rhBOOLEAN oldvalue = output_warning_messages;
   output_warning_messages = FALSE; 
   return oldvalue;
   }

__host__ rhBOOLEAN disable_error_messages ()
   { 
   rhBOOLEAN oldvalue = output_error_messages;
   output_error_messages = FALSE; 
   return oldvalue;
   }

__host__ rhBOOLEAN disable_debugging_messages ()
   {
   rhBOOLEAN oldvalue = output_debugging_messages;
   output_debugging_messages = FALSE;
   return oldvalue;
   }

__host__ rhBOOLEAN warning_messages_enabled ()
   { return output_warning_messages; }

__host__  rhBOOLEAN informative_messages_enabled ()
   { return output_informative_messages; }

__host__ rhBOOLEAN error_messages_enabled ()
   { return output_error_messages; }

__host__  rhBOOLEAN debugging_messages_enabled ()
   { return output_debugging_messages; }

/*>>
Routines for outputting the messages.
<<*/

__host__  void debugging_message (const char* fmt, ... /* DOTDOTDOT*/)
   {
   /* Get the message and print the specific error message */

   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   /* Check to see whether we should output the message */
   if (! output_debugging_messages) return;

   /* Output the message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (debugging_file, fmt, args);

      /* Print a line feed */
      fprintf (debugging_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (debugging_file);
   }

__host__  void informative_message (const char* fmt, DOTDOTDOT)
   {
   /* Get the message and print the specific error message */

   /* Set default for info_file */
   if (info_file == (FILE *) 0) info_file = stdout;

   /* Check to see whether we should output the message */
   if (! output_informative_messages) return;

   /* Output the message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (info_file, fmt, args);

      /* Print a line feed */
      fprintf (info_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (info_file);
   }

__host__ void warning_message (const char* fmt, DOTDOTDOT)
   {
   /* Print the general message header */

   /* Set default for warning_file */
   if (warning_file == (FILE *) 0) warning_file = stderr;

   /* Check to see whether we should output the message */
   if (! output_warning_messages) return;

   fprintf (warning_file, "Warning : ");

   /* Get the message and print the specific error message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (warning_file, fmt, args);

      /* Print a line feed */
      fprintf (warning_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (warning_file);
   }

__host__ void error_message (const char* fmt, DOTDOTDOT)
   {
   /* Print the general message header */

   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   /* Check to see whether we should output the message */
   if (! output_error_messages) return;

   fprintf (error_file,"Error : ");

   /* Get the message and print the specific error message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (error_file, fmt, args);

      /* Print a line feed */
      fprintf (error_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (error_file);
   }

__host__  void fatal_error_message (const char* fmt, DOTDOTDOT)
   {

   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   /* Print the general message header */
   fprintf (error_file,"Fatal error ...\n");

   /* Get the message and print the specific error message */
      {  
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (error_file, fmt, args);

      /* Print a line feed */
      fprintf (error_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }  

   /* Exit the program */
   fprintf (error_file, "\t... Exiting ...\n");
   fflush (error_file);
   bail_out (2);
   }

// End of host versions
#endif

//=======================================================================
//                Now, device versions
//=======================================================================

#ifdef __CUDA_ARCH__
namespace DeviceErrorMessage
{
   __device__ rhBOOLEAN output_error_messages = TRUE;
};

__device__ void set_error_output_file (FILE *afile)
   { 
   // Void 
   }

__device__ FILE *error_output_file ()
   { return NULL; }

__device__ void set_info_output_file (FILE *afile)
   { 
   // Void 
   }

__device__ FILE *info_output_file ()
   { return NULL; }

__device__ void set_warning_output_file (FILE *afile)
   { 
   // Void 
   }

__device__ FILE *warning_output_file ()
   { return NULL; }

__device__ void set_debugging_output_file (FILE *afile)
   { 
   // Void 
   }

__device__ FILE *debugging_output_file ()
   { return NULL; }

__device__ rhBOOLEAN enable_debugging_messages ()
   { 
   DeviceErrorMessage::output_error_messages = TRUE;
   return TRUE; 
   }

__device__ rhBOOLEAN enable_informative_messages ()
   { return TRUE; }

__device__ rhBOOLEAN enable_warning_messages ()
   { return TRUE; }

__device__ rhBOOLEAN enable_error_messages ()
   { return TRUE; }

__device__ rhBOOLEAN disable_informative_messages ()
   { return TRUE; }

__device__ rhBOOLEAN disable_warning_messages ()
   { return TRUE; }

__device__ rhBOOLEAN disable_error_messages ()
   { return TRUE; }

__device__ rhBOOLEAN disable_debugging_messages ()
   { return TRUE; }

__device__ rhBOOLEAN warning_messages_enabled ()
   { return TRUE; }

__device__ rhBOOLEAN informative_messages_enabled ()
   { return TRUE; }

__device__ rhBOOLEAN error_messages_enabled ()
   { return TRUE; }

__device__ rhBOOLEAN debugging_messages_enabled ()
   { return TRUE; }

__device__  void error_message (const char* fmt)
   { printf (fmt); }

__device__  void error_message (const char* fmt, int d)
   { printf (fmt, d); }

#endif
