#include <Utilities_cuda/complex_dcl.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/polynomials_dcl.h>
#include <Utilities_cuda/zroots_dcl.h>

/*-------------------------------------------------------------------
//     Takes eigenvalues of a 3x3 matrix
//------------------------------------------------------------------- */
 
FUNCTION_DEF ( static void swap, ( fcomplex *x, fcomplex *y))
   {
   /* Swaps two complex numbers */
   fcomplex temp;
   temp = *x; *x = *y; *y = temp;
   }
 
FUNCTION_DEF ( void eigenvalues_3x3_base1, (
        double **M,
        fcomplex *ev
        ))
   {
   /* Computes the eigenvalues of a 3x3 real matrix */
   /* They will be sorted according to imaginary part (descending order) */
   int j;

   double   *poly  = VECTOR (0, 3, double);     /* Index starts at 0 !! */
   fcomplex *cpoly = VECTOR (0, 3, fcomplex);   /* Index starts at 0 !! */
 
   /* Compute characteristic polynomial of the transform matrix */
   characteristic_polynomial_base1 (M, 3, poly);
 
   /* Transform it to complex */
   for (j=0; j<=3; j++)
      {  
      cpoly[j].r = poly[j];
      cpoly[j].i = 0.0;
      }  
 
   /* Compute its roots */
   complex_roots_base1 (cpoly, 3, ev, 1);
 
   /* Sort the eigenvalues according to imaginary part (descending) */
   if (ev[1].i < ev[2].i) swap (&(ev[1]), &(ev[2]));
   if (ev[2].i < ev[3].i) swap (&(ev[2]), &(ev[3]));
   if (ev[1].i < ev[2].i) swap (&(ev[1]), &(ev[2]));
 
   /* Free temporary arrays */
   FREE (poly, 0);
   FREE (cpoly, 0);
   }

