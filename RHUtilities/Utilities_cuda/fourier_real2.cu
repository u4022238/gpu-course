#include <math.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_cuda/fourier2D_dcl.h>

#define SWAP(a,b) {double tempr=(a);(a)=(b);(b)=tempr;}

#ifdef rhDEBUG
static void print_data_array ( double *data, int n)
   {
   /* Prints the data */
   int i;
   for (i=1; i<=n; i++)
      printf ("\t%3d : %12.4f\n", i, data[i]);
   }
#endif

FUNCTION_DEF ( void fft_unpack2, (
        double *data,           /* Packed real fft */
        int i0,                 /* Start index for data array */
        int n,                  /* Dimension of data array */
        double *real,
        double *imag            /* Real and imaginary parts of unpacked */
        ))
   {
   /*
    * Unpacks the output of the real FFT into real and imaginary parts.
    */

   int i1, i2;

   /* Define a quarter block */
   int imid = i0 + n/2;

   /* The real term first */
   real[i0]   = data[i0];   imag[i0]   = 0.0;
   real[imid] = data[imid]; imag[imid] = 0.0;

   for (i1=i0+1, i2=n+i0-1; i1<imid; i1++, i2--)
      {
      /* Write out the real and imaginary parts */
      real[i1] = real[i2] = data[i1];
      imag[i1] = data[i2];
      imag[i2] = -data[i2];
      }
   }

FUNCTION_DEF ( void fft_2D_unpack2, (
        double **data,          /* Packed real fft */
        double i0, int j0,      /* Starting indices for the array */
        int idim, int jdim, 
        double **real,
        double **imag           /* Real and imaginary parts of unpacked */
        ))
   {
   /*
    * Unpacks the output of the real FFT into real and imaginary parts.
    *
    * Either of real or imag may be the same as the input array, data.
    */

   int i1, j1;

   /* Define a quarter block */
   int imid = i0 + idim/2;
   int jmid = j0 + jdim/2;

   for (i1=i0; i1<=imid; i1+=idim/2)
      {
      /* i1 takes values if i0 and imid only */

      for (j1=j0+1; j1<jmid; j1++)
         {
         /* Get the two coordinates positions */
         int j2 = jdim - j1 + j0;

         /* Get the pixels */
         double a = data[i1][j1];
         double b = data[i1][j2];

         /* Write out the real and imaginary parts */
         real[i1][j1] = a;
         real[i1][j2] = a;
         imag[i1][j1] = c;
         imag[i1][j2] = -imag[i2][j1];
         }
      }

   for (i1=i0+1; i1<imid; i1++)
      {
      /* Get the two coordinate positions */
      int i2 = idim - i1 + i0;

      for (j1=j0+1; j1<jmid; j1++)
         {
         /* Get the two coordinates positions */
         int j2 = jdim - j1 + j0;

         /* Get the pixels */
         double a = data[i1][j1];
         double b = data[i2][j1];
         double c = data[i1][j2];
         double d = data[i2][j2];

         /* Write out the real and imaginary parts */
         real[i1][j1] = real[i2][j2] = a-d;
         real[i1][j2] = real[i2][j1] = a+d;
         imag[i1][j1] = b+c;
         imag[i2][j1] = b-c;
         imag[i1][j2] = -imag[i2][j1];
         imag[i2][j2] = -imag[i1][j1];
         }
      }
   }

FUNCTION_DEF ( void fft_real2, ( double *data, int n))
   {
   /* Does a Fourier transform of a real array of size n. */
   /* Actually, this is just for test : We take the complex
    * fft and then reorder the data.
    */
   }

FUNCTION_DEF ( int fourier2D_real2_base1, (
   double **re,                 /* The image */
   int nrows, int ncols         /* The dimensions of the image */
   ))
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (1, nrows) x (1, ncols) */
   int i, j;
  
   /* Declare an array to hold the columns */
   double *acol;

   /* Check that the dimensions of the image are powers of 2 */
   if (! is_2_power (nrows) || ! is_2_power (ncols))
      {
      error_message ("fourier2D: Image size not power of 2");
      return 0;
      }

   /* Now, take the transform one row at a time */
   for (i=1; i<=nrows; i++)
      {
      /* The row is the i-th row of the array.  Start indexing at 1 */
      double *arow = re[i];

      /* Take the transform */
      fft_real2 (arow, ncols);
      }

   /* Declare an array to hold columns */
   acol = VECTOR (1, nrows, double);

   /* Now take the transforms in the opposite direction */
   for (j=1; j<=ncols; j++)
      {
      /* Take the transform of the columns */

      /* First, build a column */
      for (i=1; i<=nrows; i++)
         acol[i] = re[i][j];

      /* Take the transform */
      fft_real2 (acol, nrows);
        
      /* Put it back in the arrays */
      for (i=1; i<=nrows; i++)
         re[i][j] = acol[i];
      }

   /* Now, free the data */
   FREE (acol, 1);

   /* Return success */
   return 1;
   }

#undef SWAP

