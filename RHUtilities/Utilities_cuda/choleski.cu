/* @(#)choleski.c       1.3 07 Nov 1994 */

/*
 * Choleski factorization
 * Factors a symmetric positive-definite matrix into C C^T
 */

#include <Utilities_cuda/utilities_c.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/QRdecomp_dcl.h>
#include <Utilities_cuda/jacobi_dcl.h>

FUNCTION_DEF (int choleski_RRT_base1, (
   double **A,
   int n,
   double **K
   ))
   {
   /* Does a Cholesk decomposition of A  = K * K^T, where K
    * is upper triangular
    */
   int i, j, k;
   double sum;

   /* Clear out the lower part of K */
   for (i=2; i<=n; i++) 
      for (j=1; j<i; j++) 
         K[i][j] = 0.0;
   
   for (j=n; j>=1; j--)
      for (i=j; i>=1; i--)
         {
         double sum = A[i][j];
         for (k=j+1; k<=n; k++)
            sum -= K[i][k] * K[j][k];
         if (i == j)
            {
            if (sum < 0.0) return 0;    /* Not pos def */
            K[i][i] = sqrt(sum);
            }
         else
            K[i][j] = sum / K[j][j];
         }
   
   /* Success */
   return 1;
   }

FUNCTION_DEF ( int choleski_3x3_base1, (
   double **Ain,        /* Input symmetric positive definite matrix */
   double **C           /* Output such that Ain = C*C^T */
   ))
   {
   /*
    * Ain is a symmetric matrix, assumed positive definite.
    * This routine finds C such that Ain = C*C^T
    * If it turns out the Ain is not positive definite, then return 0
    * If it is, then C is computed and return 1
    * Ain may be the same as C.
    */

   int i, j;
   
   /* Need some matrices to get the eigenvalue decomposition */
   double **A = MATRIX (1, 3, 1, 3, double);  /* Copy of the input */
   double **V = MATRIX (1, 3, 1, 3, double);  /* Matrix of eigenvectors */
   double *D  = VECTOR (1, 3, double);        /* Vector of eigenvalues */

   /* Copy Ain to A */
   matrix_copy (Ain, A, 1, 3, 1, 3);

   /* First, find the eigenvalues and eigenvectors of A */
   jacobi_base1 (A, 3, D, V);

   /* Now, multiply the columns of V by the square roots of the eigenvalues */
   for (i=1; i<=3; i++)
      {
      double d;   /* Square root of the eigenvalue */
     
      /* Check that the eigenvalue is positive */
      if (D[i] < 0.0) 
         {
         FREE (V, 1);
         FREE (D, 1);
         return 0;
         }
      else d = sqrt (D[i]);

      /* Carry out the multiplication of the i-th column */
      for (j=1; j<=3; j++)
         V[j][i] *= d;
      }

   /* Now a QR factorization to get an upper triangular C */
   QRdecomposition_3x3_base1 (V, C, V);

   /* Ensure that the diagonal entries of C are all positive */
   for (i=1; i<=3; i++)
      if (C[i][i] < 0.0)
         for (j=1; j<=i; j++)
            C[j][i] = -C[j][i];

   /* Set the below-diagonal entries to exactly 0 */ 
   C[2][1] = C[3][1] = C[3][2] = 0.0;

   /* Free the temporaries */
   FREE (A, 1);
   FREE (V, 1);
   FREE (D, 1);

   /* Return success code */
   return 1;
   }
   

   

