/* @(#)jacobi.c   1.4 17 Jun 1994 */
#include <math.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/error_message.h>
#include <assert.h>

#define MAX_ITER 50

FUNCTION_DEF ( static void sort_eigenvalues_base1, (
        double *d,
        double **v,
        int n
        ))
   {
   /* Sort the eigenvalues and eigenvectors into decreasing order */
   int i, j;

   for (i=1; i<n; i++) 
      {
      /* k contains the position of the maximum eigenvalue */
      int k = i;

      /* Find the largest remaining eigenvalue */
      for (j=i+1; j<=n; j++)
         if (d[j] >= d[k])
            k = j;

      /* If the maximum eigenvalue is not already in position */
      if (k != i) 
         {
         /* Swap */
         double maxval = d[k];
         d[k] = d[i];
         d[i] = maxval;

         /* Also swap the eigenvectors */
         for (j=1; j<=n; j++) 
            {
            double temp = v[j][i];
            v[j][i] = v[j][k];
            v[j][k] = temp;
            }
         }
      }
   }

#define ROTATE(a,i,j,k,l,tau)           \
        {                               \
        double g = a[i][j];             \
        double h = a[k][l];             \
        a[i][j] = g - s*(h+g*tau);      \
        a[k][l] = h + s*(g-h*tau);      \
        }

FUNCTION_DEF ( void jacobi_base1, (
        double **a_in,
        int n,
        double *d,
        double **v))
   {
   int i, j;

   /* Take a copy of the matrix */
   double **a = MATRIX (1, n, 1, n, double);
   for (i=1; i<=n; i++)
      for (j=1; j<=n; j++)
          a[i][j] = a_in[i][j];

   /* Set v to the identity matrix to start with */
   for (i=1; i<=n; i++) 
      {
      for (j=1; j<=n; j++) 
         v[i][j] = 0.0;
      v[i][i] = 1.0;
      }

   /* d is the diagonals of the matrix a */
   for (i=1; i<=n; i++) 
      d[i] = a[i][i];

   for (i=1; i<=MAX_ITER; i++) 
      {
      int ip, iq;
      double sm = 0.0;
      double thresh = 0.0;

      /* Find the maximum value off diagonal */
      for (ip=1; ip<=n-1; ip++) 
         {
         for (iq=ip+1; iq<=n; iq++)
            sm += fabs(a[ip][iq]);
         }

      /* Return if zero off-diagonal */
      if (sm == 0.0) 
         {
         /* Normal return */

         /* First sort the eigenvalues */
         sort_eigenvalues_base1 (d, v, n);

         /* Free the temporaries */
         FREE(a, 1);
         return;
         }

      /* Set nonn-zero threshold for first iterations */
      if (i < 4)
         thresh = 0.2*sm/(n*n);

      /* Now sweep */
      for (ip=1; ip<=n-1; ip++) 
         {
         for (iq=ip+1; iq<=n; iq++) 
            {
            double theta, tau, t, s, c;
            double g = 100.0*fabs(a[ip][iq]);

            if (i > 4 && fabs(d[ip])+g == fabs(d[ip])
               && fabs(d[iq])+g == fabs(d[iq]))
               a[ip][iq] = 0.0;

            else if (fabs(a[ip][iq]) > thresh) 
               {
               double h = d[iq]-d[ip];

               if (fabs(h)+g == fabs(h))
                  t = (a[ip][iq])/h;
               else 
                  {
                  theta = 0.5*h/(a[ip][iq]);
                  t = 1.0/(fabs(theta)+sqrt(1.0+theta*theta));
                  if (theta < 0.0) t = -t;
                  }

               c = 1.0/sqrt(1+t*t);
               s = t*c;
               tau = s/(1.0+c);
               h = t*a[ip][iq];
               d[ip] -= h;
               d[iq] += h;
               a[ip][iq] = 0.0;

               for (j=1; j<=ip-1; j++) 
                  {
                  ROTATE(a, j, ip, j, iq, tau);
                  }

               for (j=ip+1; j<=iq-1; j++) 
                  {
                  ROTATE(a, ip, j, j, iq, tau);
                  }

               for (j=iq+1; j<=n; j++) 
                  {
                  ROTATE(a, ip, j, iq, j, tau);
                  }

               for (j=1; j<=n; j++) 
                  {
                  ROTATE(v, j, ip, j, iq, tau);
                  }
               }
            }
         }
      }

   fatal_error_message("Too many iterations in routine JACOBI");
   }

#undef ROTATE

