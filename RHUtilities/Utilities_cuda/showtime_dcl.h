#ifndef _showtime_c_dcl_
#define _showtime_c_dcl_
#include "Utilities_cuda/cvar.h"


/* */
#include <stdio.h>
/* */

EXTERN_C_FUNCTION (const char *timestring, ());

EXTERN_C_FUNCTION (void timed_message, (FILE *afile, const char *string));

#endif
