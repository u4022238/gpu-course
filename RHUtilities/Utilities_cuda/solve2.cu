/* @(#)solve2.c 1.7 28 Nov 1995 */
/*>>
 Singular value decomposition routines taken from the book
" Numerical Recipes in C " (Cambridge University Press 1988)
<<*/

#include <Utilities_cuda/cvar.h>
#include <math.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/svalue3_dcl.h>

/*>> 
Singular value decomposition routines.
<<*/


FUNCTION_DEF ( int solve_2_base1, (
        double **a,
        double *x,
        int nrow,
        int ncol))
   {
   /* Find the x of norm 1 which minimizes the value of a*x.
    * The inputs are 
    *    a[1..nrow][1..ncol]
    * The output is 
    *    x[1..ncol]
    *
    * The routine returns 1 on success, 0 on failure.  It fails if the SVD
    * fails.
    */
   int i, j;
   double *w, **v, **u;
   double wmin;
   int index;
   int returnval = 1;
   
   /* Allocate matrices */
   u = MATRIX (1, nrow, 1, ncol, double);
   w = VECTOR (1, ncol, double);
   v = MATRIX (1, ncol, 1, ncol, double);

   /* Copy a to u */
   for (i=1; i<=nrow; i++)
      for (j=1; j<=ncol; j++)
          u[i][j] = a[i][j];

   /* Now do the singular value decomposition */
      {
      const int no = 0;
      if (! svdcmp_base1(u, nrow, ncol, w, v, no))      /* Need v only */
         {
         returnval = 0;
         goto cleanup;
         }
      }

   /* Find the minimum singular value */
   wmin = w[1];
   index = 1;
   for (i=2; i<=ncol; i++)
      {
      double abswi = rhAbs (w[i]);
      if (abswi < wmin) 
         {
         wmin = abswi;
         index = i;
         }
      }

   /* Copy the indexth column of u to w */
   for (i=1; i<=ncol; i++)
      x[i] = v[i][index];

cleanup:
   /* Free the temporaries */
   FREE (u, 1); 
   FREE (w, 1);
   FREE (v, 1);

   /* return success */
   return (returnval);
   }

