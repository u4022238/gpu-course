#ifndef _error_message_c_dcl_
#define _error_message_c_dcl_
#include "Utilities_cuda/cvar.h"

/* */
#include <Utilities_cuda/rh_util.h>
/* */

#if __CUDA_ARCH__

// Defined as device functions 

__device__ void error_message (const char[]);
__device__ void error_message (const char*, int);
__device__ void error_message (const char*, int, int);
__device__ void error_message (const char*, int, int, int);
__device__ void error_message (const char*, int, double);
__device__ void error_message (const char*, int, int, double);
__device__ void error_message (const char*, void*);
__device__ void error_message (const char*, void*, void*);

__device__ void warning_message (const char*);
__device__ void warning_message (const char*, int);
__device__ void warning_message (const char*, int, int);
__device__ void warning_message (const char*, int, int, int);
__device__ void warning_message (const char*, int, double);
__device__ void warning_message (const char*, int, int, double);
__device__ void warning_message (const char*, void*);
__device__ void warning_message (const char*, void*, void*);

__device__ void informative_message (const char*);
__device__ void informative_message (const char*, int);
__device__ void informative_message (const char*, int, int);
__device__ void informative_message (const char*, int, int, int);
__device__ void informative_message (const char*, int, double);
__device__ void informative_message (const char*, int, int, double);
__device__ void informative_message (const char*, void*);
__device__ void informative_message (const char*, void*, void*);

__device__ void debugging_message (const char* fmt);
__device__ void debugging_message (const char* fmt, int);
__device__ void debugging_message (const char* fmt, int, int);
__device__ void debugging_message (const char* fmt, int, int, int);
__device__ void debugging_message (const char* fmt, int, double);
__device__ void debugging_message (const char* fmt, int, int, double);
__device__ void debugging_message (const char*, void*);
__device__ void debugging_message (const char*, void*, void*);

__device__ void fatal_error_message (const char* fmt);
__device__ void fatal_error_message (const char* fmt, int);
__device__ void fatal_error_message (const char* fmt, int, int);
__device__ void fatal_error_message (const char* fmt, int, int, int);
__device__ void fatal_error_message (const char* fmt, int, double);
__device__ void fatal_error_message (const char* fmt, int, int, double);
__device__ void fatal_error_message (const char*, void*);
__device__ void fatal_error_message (const char*, void*, void*);

__device__ void set_error_output_file (FILE *afile);
__device__ FILE *error_output_file ();
__device__ void set_info_output_file (FILE *afile);
__device__ FILE *info_output_file ();
__device__ void set_warning_output_file (FILE *afile);
__device__ FILE *warning_output_file ();
__device__ void set_debugging_output_file (FILE *afile);
__device__ FILE *debugging_output_file ();
__device__ rhBOOLEAN enable_debugging_messages ();
__device__ rhBOOLEAN enable_informative_messages ();
__device__ rhBOOLEAN enable_warning_messages ();
__device__ rhBOOLEAN enable_error_messages ();
__device__ rhBOOLEAN disable_informative_messages ();
__device__ rhBOOLEAN disable_warning_messages ();
__device__ rhBOOLEAN disable_error_messages ();
__device__ rhBOOLEAN disable_debugging_messages ();
__device__ rhBOOLEAN warning_messages_enabled ();
__device__ rhBOOLEAN informative_messages_enabled ();
__device__ rhBOOLEAN error_messages_enabled ();
__device__ rhBOOLEAN debugging_messages_enabled ();

#else

// Compiling as host functions

__host__ void set_error_output_file (FILE *afile);

__host__ FILE *error_output_file ();

__host__ void set_info_output_file (FILE *afile);

__host__ FILE *info_output_file ();

__host__ void set_warning_output_file (FILE *afile);

__host__ FILE *warning_output_file ();

__host__ void set_debugging_output_file (FILE *afile);

__host__ FILE *debugging_output_file ();

__host__ rhBOOLEAN enable_debugging_messages ();

__host__ rhBOOLEAN enable_informative_messages ();

__host__ rhBOOLEAN enable_warning_messages ();

__host__ rhBOOLEAN enable_error_messages ();

__host__ rhBOOLEAN disable_informative_messages ();

__host__ rhBOOLEAN disable_warning_messages ();

__host__ rhBOOLEAN disable_error_messages ();

__host__ rhBOOLEAN disable_debugging_messages ();

__host__ rhBOOLEAN warning_messages_enabled ();

__host__ rhBOOLEAN informative_messages_enabled ();

__host__ rhBOOLEAN error_messages_enabled ();

__host__ rhBOOLEAN debugging_messages_enabled ();

__host__ void debugging_message (const char* fmt, DOTDOTDOT);

__host__ void informative_message (const char* fmt, DOTDOTDOT);

__host__ void warning_message (const char* fmt, DOTDOTDOT);

__host__ void error_message (const char* fmt, DOTDOTDOT);

__host__ void fatal_error_message (const char* fmt, DOTDOTDOT);
#endif

#endif
