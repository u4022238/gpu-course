#ifndef _quaternion_c_dcl_
#define _quaternion_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION (double sinc, (double x));

EXTERN_C_FUNCTION ( double **quaternion_to_matrix, (
   double *q,		/* The quaternion */
   double **R)		/* The rotation matrix */
   );

EXTERN_C_FUNCTION ( double **VecToMatrix, (
   double *V,	/* Input vector, representing a rotation */
   double **R	/* Output rotation matrix */
   ));

EXTERN_C_FUNCTION ( int getinput, (
        double *V));

EXTERN_C_FUNCTION ( int quaternion_main, (
        int argc,
        char *argv[]));

#endif
