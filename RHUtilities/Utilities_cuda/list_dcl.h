#ifndef _list_c_dcl_
#define _list_c_dcl_
#include "Utilities_cuda/cvar.h"


/* */
#include <Utilities_cuda/list_defs.h>
/* */

EXTERN_C_FUNCTION (LISTENTRY *lst_newentry, ());

EXTERN_C_FUNCTION ( LISTENTRY *lst_insertafter, (
   /* Inserts a piece of data after the indicated position */
   DATA dat,		/* The data to insert */
   LISTENTRY *aposition/* Position to insert the list */
   ));

EXTERN_C_FUNCTION ( LIST *lst_init, ());

EXTERN_C_FUNCTION ( void lst_joinafter, (
   LIST *lst,		/* The list to be grafted in */
   LISTENTRY *position	/* Where it should be placed */
   ));

EXTERN_C_FUNCTION ( LIST *lst_join, (
        LIST *l1,
        LIST *l2));

EXTERN_C_FUNCTION ( void lst_deleteentry, (
        LISTENTRY *entry));

EXTERN_C_FUNCTION ( DATA lst_delete, (
        DATA dat,
        LIST *lst));

EXTERN_C_FUNCTION ( DATA lst_pop, (
        LIST *que));

EXTERN_C_FUNCTION ( DATA lst_dequeue, (
        LIST *que));

EXTERN_C_FUNCTION ( void lst_free, (
        LIST *alist));

EXTERN_C_FUNCTION ( int lst_length, (
        LIST *alist));

EXTERN_C_FUNCTION ( void lst_bubblesort, (
   LIST *alist,                 	/* A list to be sorted */
   int (*cmp) (DATA, DATA)/* Function for comparing elements */
   ));

EXTERN_C_FUNCTION ( void lst_check, (
        LIST *alist));

EXTERN_C_FUNCTION (int lst_usage_summary, ());

EXTERN_C_FUNCTION (int lst_usage, ());

#endif
