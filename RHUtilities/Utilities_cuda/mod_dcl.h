#ifndef _mod_c_dcl_
#define _mod_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( int mod, (
        int m,
        int n));

#endif
