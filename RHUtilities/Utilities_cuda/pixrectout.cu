/* @(#)pixout.cc        1.12 11/22/96 */
/* Routines for outputting arrays as pixrects, and vice versa */

#define NEWPIXMEM
#include <pixrect/pixrect_hs.h>
#undef NEWPIXMEM

/* #include <pixrect/pr_io.h> */
#include <Utilities_cuda/utilities_c.h>

/* Stuff for the color map */
static const int PIX_DEPTH  = 8;
static colormap_t greyscale_colormap;
#define CMAPSIZE 256
unsigned char red[CMAPSIZE], green[CMAPSIZE], blue[CMAPSIZE];

FUNCTION_DEF (static void init_greyscale_colormap, ())
   {
   /* Initializes the colormap if it has not already been done */
   int i;
   if (greyscale_colormap.length != NULL) return;

   for (i = 0; i<CMAPSIZE; i++)
      red[i] = green[i] = blue[i] = (i * 256) / CMAPSIZE;

   greyscale_colormap.length = CMAPSIZE;
   greyscale_colormap.type   = RMT_EQUAL_RGB;
   greyscale_colormap.map[0] = red;
   greyscale_colormap.map[1] = green;
   greyscale_colormap.map[2] = blue;
   }

FUNCTION_DEF (static Pixrect *load_ras, (char *filename))
   {
   /* Loads a raster image, ignoring the color map  */
   FILE *fp;
   Pixrect *pr;
   colormap_t cmap;
   
   /* Set maps to null */
   cmap.length = 0;
   cmap.type = RMT_EQUAL_RGB;
   cmap.map[0] = cmap.map[1] = cmap.map[2] = NULL;

   /* Open the file  */
   if ((fp = rhfopen (filename, "r")) == NULL)
      {
      error_message ("Could not open file \"%s\" for reading", filename);
      return(NULL);
      }

   /* read pr  */
   if ((pr = pr_load(fp, &cmap)) == NULL)
      error_message ("ERROR : pr_load failed for file \"%s\"", filename);
   else   
      informative_message ("File \"%s\" loaded. Size = %d x %d",
         filename, pr->pr_size.y, pr->pr_size.x);

   /* Free the colormap storage.  Only free map[0], since the other two */
   /* maps are part of the same allocation block. */
   if (cmap.map[0]) free ((char *)cmap.map[0]);

   /* Close file and return the new pixrect  */
   fclose(fp);
   return(pr);
   }

FUNCTION_DEF (static struct pixrect *convert_char_array_to_pix, (
   unsigned char **val, 
   int IDIM, 
   int JDIM
   ))
   {
   /* Output a radlet image as a pixrect. */
   int i, j;

   /* Open a pixrect to hold the value  */
   struct pixrect *pr = mem_create (JDIM, IDIM, PIX_DEPTH);

   /* Test that this worked  */
   if (pr == NULL)
      {
      error_message ("Could not create memory pixrect\n");
      return NULL;
      }

   /* Now put the values  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         pr_put (pr, j, i, val[i][j]);

   /* Else, return success. */
   return (pr);
   }

FUNCTION_DEF (static struct pixrect *convert_float_array_to_pix, (
   float **val, 
   int IDIM, 
   int JDIM
   ))
   {
   /* Output a radlet image as a pixrect. */
   int i, j;

   /* Open a pixrect to hold the value  */
   struct pixrect *pr = mem_create (JDIM, IDIM, PIX_DEPTH);

   /* Test that this worked  */
   if (pr == NULL)
      {
      error_message ("Could not create memory pixrect\n");
      return NULL;
      }

   /* Now put the values  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         pr_put (pr, j, i, nint(val[i][j]));

   /* Else, return success. */
   return (pr);
   }

FUNCTION_DEF (static struct pixrect *convert_int_array_to_pix, (
   int **val, 
   int IDIM, 
   int JDIM
   ))
   {
   /* Output a radlet image as a pixrect. */
   int i, j;

   /* Open a pixrect to hold the value  */
   struct pixrect *pr = mem_create (JDIM, IDIM, PIX_DEPTH);

   /* Test that this worked  */
   if (pr == NULL)
      {
      error_message ("Could not create memory pixrect\n");
      return NULL;
      }

   /* Now put the values  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         pr_put (pr, j, i, val[i][j]);

   /* Else, return success. */
   return (pr);
   }

FUNCTION_DEF (static int output_and_destroy_pixrect, (
   struct pixrect *pr, char *fname))
   {  
   int type = RT_STANDARD, copyflag = FALSE;

   /* Open the output pixrect file  */
   FILE *pixfile = FOPEN (fname, "w");

   /* Initialize the colormap */
   init_greyscale_colormap ();

   /* Dump the image */
   if (pr_dump(pr, pixfile, &greyscale_colormap, type, copyflag) == PIX_ERR)
      {
      error_message ("ERROR : pr_dump failed.\n");
      fclose (pixfile);
      pr_destroy(pr);
      return (0);
      }

   /* Close the pix file again  */
   fclose (pixfile);

   /* Destroy the pixrect */
   pr_destroy(pr);

   /* Else, return success. */
   return (1);
   }

FUNCTION_DEF (int put_char_array_to_pix, (
   char *pixfilename, 
   unsigned char **val, 
   int IDIM, 
   int JDIM))
   {
   /* Output a radlet image as a pixrect. */
   struct pixrect *pr;          /* The pixrect to be created  */

   /* Open a pixrect to hold the value  */
   if ((pr = convert_char_array_to_pix (val, IDIM, JDIM)) == 0)
      return (0);

   /* Having read the values, now output the pixrect to a file  */
   return output_and_destroy_pixrect (pr, pixfilename);
   }

FUNCTION_DEF (int put_float_array_to_pix, (
   char *pixfilename, 
   float **val, 
   int IDIM, 
   int JDIM))
   {
   /* Output a radlet image as a pixrect. */

   struct pixrect *pr;          /* The pixrect to be created  */

   /* Open a pixrect to hold the value  */
   if ((pr = convert_float_array_to_pix (val, IDIM, JDIM)) == 0)
      return (0);

   /* Having read the values, now output the pixrect to a file  */
   return output_and_destroy_pixrect (pr, pixfilename);
   }

FUNCTION_DEF (int put_int_array_to_pix, (
   char *pixfilename, 
   int **val, 
   int IDIM, 
   int JDIM))
   {
   /* Output a radlet image as a pixrect. */

   struct pixrect *pr;          /* The pixrect to be created  */

   /* Open a pixrect to hold the value  */
   if ((pr = convert_int_array_to_pix (val, IDIM, JDIM)) == 0)
      return (0);

   /* Having read the values, now output the pixrect to a file  */
   return output_and_destroy_pixrect (pr, pixfilename);
   }

FUNCTION_DEF (int read_pix_to_new_char_array, (
   char *infilename,            /* Name of the input file */
   unsigned char ***val,        /* Array to be filled */
   int *IDIM, int *JDIM         /* Size of the array. */
   ))
   {
   /* Read the pix image. */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Get the dimension. */
   *IDIM = pr->pr_size.y;
   *JDIM = pr->pr_size.x;

   /* Allocate space for the arrays  */
   *val = MATRIX (0, *IDIM, 0, *JDIM, unsigned char);

   /* Now read the file  */
   for (i=0; i<*IDIM; i++)
      for (j=0; j<*JDIM; j++)
         (*val)[i][j] = (unsigned char) pr_get (pr, j, i);

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_new_float_array, (
   char *infilename,            /* Name of the input file */
   float ***val,        /* Array to be filled */
   int *IDIM, int *JDIM         /* Size of the array. */
   ))
   {
   /* Read the pix image. */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Get the dimension. */
   *IDIM = pr->pr_size.y;
   *JDIM = pr->pr_size.x;

   /* Allocate space for the arrays  */
   *val = MATRIX (0, *IDIM, 0, *JDIM, float);

   /* Now read the file  */
   for (i=0; i<*IDIM; i++)
      for (j=0; j<*JDIM; j++)
         (*val)[i][j] = (float) pr_get (pr, j, i);

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_new_int_array, (
   char *infilename,            /* Name of the input file */
   int ***val,                  /* Array to be filled */
   int *IDIM, int *JDIM         /* Size of the array. */
   ))
   {
   /* Read the pix image. */
   int i, j;
   Pixrect *pr = load_ras( infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Get the dimension. */
   *IDIM = pr->pr_size.y;
   *JDIM = pr->pr_size.x;

   /* Allocate space for the arrays  */
   *val = MATRIX (0, *IDIM, 0, *JDIM, int);

   /* Now read the file  */
   for (i=0; i<*IDIM; i++)
      for (j=0; j<*JDIM; j++)
         (*val)[i][j] = (int) pr_get (pr, j, i);

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_new_short_array, (
   char *infilename,            /* Name of the input file */
   unsigned short ***val,       /* Array to be filled */
   int *IDIM, int *JDIM         /* Size of the array. */
   ))
   {
   /* Read the pix image. */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Get the dimension. */
   *IDIM = pr->pr_size.y;
   *JDIM = pr->pr_size.x;

   /* Allocate space for the arrays  */
   *val = MATRIX (0, *IDIM, 0, *JDIM, unsigned short);

   /* Now read the file  */
   for (i=0; i<*IDIM; i++)
      for (j=0; j<*JDIM; j++)
         (*val)[i][j] = (unsigned short) pr_get (pr, j, i);

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_new_double_array, (
   char *infilename,            /* Name of the input file */
   double ***val,               /* Array to be filled */
   int *IDIM, int *JDIM))       /* Size of the array. */
   {
   /* Read the pix image. */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Get the dimension. */
   *IDIM = pr->pr_size.y;
   *JDIM = pr->pr_size.x;

   /* Allocate space for the arrays  */
   *val = MATRIX (0, *IDIM, 0, *JDIM, double);

   /* Now read the file  */
   for (i=0; i<*IDIM; i++)
      for (j=0; j<*JDIM; j++)
         (*val)[i][j] = (double) pr_get (pr, j, i);

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

/* */
/* Now a bunch of routines for reading into already allocated arrays */
/* */

FUNCTION_DEF (int read_pix_to_float_array, (
   char *infilename,            /* Name of the input file */
   float **val,                 /* Array to be filled */
   int IDIM, int JDIM,          /* Size of the array. */
   double fill
   ))
   {
   /* Read the pix image.  Array must already be allocated */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Now read the file  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         {
         if (i < pr->pr_size.y && j < pr->pr_size.x)
            val[i][j] = (float) pr_get (pr, j, i);
         else 
            val[i][j] = fill;
         }

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_int_array, (
   char *infilename,            /* Name of the input file */
   int **val,                   /* Array to be filled */
   int IDIM, int JDIM,          /* Size of the array. */
   int fill
   ))
   {
   /* Read the pix image.  Array must already be allocated */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Now read the file  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         {
         if (i < pr->pr_size.y && j < pr->pr_size.x)
            val[i][j] = (int) pr_get (pr, j, i);
         else 
            val[i][j] = fill;
         }

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_char_array, (
   char *infilename,            /* Name of the input file */
   unsigned char **val,         /* Array to be filled */
   int IDIM, int JDIM,          /* Size of the array. */
   int fill
   ))
   {
   /* Read the pix image.  Array must already be allocated */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Now read the file  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         {
         if (i < pr->pr_size.y && j < pr->pr_size.x)
            val[i][j] = (unsigned char) pr_get (pr, j, i);
         else 
            val[i][j] = fill;
         }

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_double_array, (
   char *infilename,            /* Name of the input file */
   double **val,                /* Array to be filled */
   int IDIM, int JDIM,          /* Size of the array. */
   double fill
   ))
   {
   /* Read the pix image.  Array must already be allocated */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Now read the file  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         {
         if (i < pr->pr_size.y && j < pr->pr_size.x)
            val[i][j] = (double) pr_get (pr, j, i);
         else 
            val[i][j] = fill;
         }

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

FUNCTION_DEF (int read_pix_to_short_array, (
   char *infilename,            /* Name of the input file */
   unsigned short **val,        /* Array to be filled */
   int IDIM, int JDIM,          /* Size of the array. */
   int fill
   ))
   {
   /* Read the pix image.  Array must already be allocated */
   int i, j;
   Pixrect *pr = load_ras(infilename);

   /* Exit on failure */
   if (pr == NULL) return 0;

   /* Now read the file  */
   for (i=0; i<IDIM; i++)
      for (j=0; j<JDIM; j++)
         {
         if (i < pr->pr_size.y && j < pr->pr_size.x)
            val[i][j] = (unsigned short) pr_get (pr, j, i);
         else 
            val[i][j] = fill;
         }

   /* Free the pixrect */
   pr_destroy (pr);

   /* Return the success */
   return (1);
   }

/* */
/* Routine for getting the size of an image */
/* */

FUNCTION_DEF (int get_raster_image_size, (
   char *infilename,            /* Name of the raster file */
   int *idim,                   /* x dimension to be returned */
   int *jdim                    /* y dimension to be returned */
   ))
   {
   /* Gets the size of a raster image by reading the header */
   FILE *infile;
   struct rasterfile rh;

   /* Open the file */
   infile = rhfopen (infilename, "r");
   if (infile == NULL)
      {  
      error_message ("Can not open file \"%s\" for read", infilename);
      return (0);
      }  

   /* Read the header */
      {  
      int retval = pr_load_header (infile, &rh);
      if (retval != 0)
         {
         error_message ("Error reading raster file header");
         fclose (infile);
         return (0);
         }
      }   

   /* Get the dimensions */
   *idim = rh.ras_height;
   *jdim = rh.ras_width;

   /* Close the file */
   fclose (infile);

   /* Return success */
   return (1);
   }

