#ifndef _bail_out_c_dcl_
#define _bail_out_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( int bail_out, (
        int n));

#endif
