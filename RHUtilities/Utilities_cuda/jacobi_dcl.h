#ifndef _jacobi_c_dcl_
#define _jacobi_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( void jacobi_base1, (
        double **a_in,
        int n,
        double *d,
        double **v));

#endif
