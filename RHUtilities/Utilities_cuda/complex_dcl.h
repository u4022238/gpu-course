#ifndef _complex_c_dcl_
#define _complex_c_dcl_
#include "Utilities_cuda/cvar.h"


/* */
#include <Utilities_cuda/complex.h>
/* */

EXTERN_C_FUNCTION ( fcomplex Cadd, (
        fcomplex a,
        fcomplex b));

EXTERN_C_FUNCTION ( fcomplex Csub, (
        fcomplex a,
        fcomplex b));

EXTERN_C_FUNCTION ( fcomplex Cmul, (
        fcomplex a,
        fcomplex b));

EXTERN_C_FUNCTION ( fcomplex Complex, (
        double re,
        double im));

EXTERN_C_FUNCTION ( fcomplex Conjg, (
        fcomplex z));

EXTERN_C_FUNCTION ( fcomplex Cdiv, (
        fcomplex a,
        fcomplex b));

EXTERN_C_FUNCTION ( double Cabs_sq, (
        fcomplex z));

EXTERN_C_FUNCTION ( double Cabs, (
        fcomplex z));

EXTERN_C_FUNCTION ( fcomplex Csqrt, (
        fcomplex z));

EXTERN_C_FUNCTION ( fcomplex RCmul, (
        double x,
        fcomplex a));

EXTERN_C_FUNCTION ( fcomplex Crecip, (
        fcomplex z));

#endif
