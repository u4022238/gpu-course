/*
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
*/

/* @(#)cvar.h	1.5 4/28/93 */
/* Defines macros for the external definitions */
#ifndef CVAR_H
#define CVAR_H

/* LINTLIBRARY */

/*	@(#)c_varieties.h 1.4 89/09/18 SMI	*/

/*
 *	Copyright (c) 1988,1989,1990 by Sun Microsystems
 */

/*
 *	This file defines some macros that are used to make code
 *	portable between the major C dialects currently in use at
 *	Sun.  As of 12/90, these include Sun C (a lot like K&R C),
 *	ANSI C, and C++.
 *
 * external functions:
 *	To declare an external function, invoke the EXTERN_C_FUNCTION
 *	macro; the macro's first parameter should be the function's
 *	return type and function name, and the second macro parameter
 *	should be the parenthesized list of function arguments (or an
 *	ellipsis - DOTDOTDOT macro should be used to indicate the 
 *      ellipsis as explained later in this file - if the arguments are 
 *      unspecified or of varying number or type, or the uppercase word 
 *      "_VOID_" if the function takes no arguments).  Some examples:
 *
 *	    EXTERN_C_FUNCTION( void printf, (char *, DOTDOTDOT) );
 *	    EXTERN_C_FUNCTION( int fread, (char*, int, int, FILE*) );
 *	    EXTERN_C_FUNCTION( int getpid, (_VOID_) );
 *
 *	Note that to be ANSI-C conformant, one should put "," at the end
 *	first argument of printf() declaration.
 *
 * structure tags:
 *	In order to handle cases where a structure tag has the same name
 *	as a type, the STRUCT_TAG macro makes the tag disappear in C++.
 *	An example (from <sys/types.h>):
 *
 *	    typedef struct STRUCT_TAG(fd_set) { ... } fd_set;
 *
 * enum bitfields:
 *	In K&R C as interpreted at UCB, bitfields may be declared to
 *	be of an enumerated type.  Neither ANSI C nor C++ permit this,
 *	so the ENUM_BITFIELD macro replaces the enum declaration with
 *	"unsigned".   An example (from C++'s <sunwindow/attr.h>):
 *
 *	    struct {
 *		ENUM_BITFIELD( Attr_pkg )	pkg		: 8;
 *		unsigned			ordinal		: 8;
 *		ENUM_BITFIELD( Attr_list_type )	list_type	: 8;
 *		...
 *	    };
 *
 * enum type specifier:
 *	In K&R C, it is OK to use "enum xyz" as return type but in C++,
 * 	one should use "xyz".  ENUM_TYPE macro is used to handle this.
 *
 *		ENUM_TYPE(enum, xyz) (*func) (char *, DOTDOTDOT);
 *
 * Pointers to functions declared as struct members or function arguments:
 *	Ideally, all pointers to function types should be declared with
 *	the exact function argument types for C++ and ANSI-C, as in
 *
 *	    struct xp_ops {
 *	    #if defined(__cplusplus) || defined(__STDC__)
 *           	bool_t      (*xp_recv)(SVCXPRT *, struct rpc_msg *);
 *			....
 *          #else
 *		bool_t      (*xp_recv)();
 *			....
 *	    };
 *
 *	    EXTERN_C_FUNCTION(int foo, (char*, int (*func) (int, char)));
 *
 *	In the event when the argument types of the function pointer may
 * 	vary depending on the situation, one can use DOTDOTDOT to tell
 *	C++ not to be so uptight.  Using this method may require pointer
 *	casting when actual assignments or function calls take place,
 *	because ellipses "..." in function pointers does not work as
 *	a wildcard.  It is meant to match only those pointers to functions
 *	that are defined specifically to take variable arguments.
 *	Note that for ANSI-C, at least one argument has to be provided 
 *	before the ellipses.
 *
 *	    struct foo {
 *		    int	(*f)( int, DOTDOTDOT );
 *		    . . .
 *	    };
 *
 */

typedef void (*_PFV_)();
typedef int (*_PFI_)();

/* Which type of C/C++ compiler are we using? */

#if defined(__cplusplus)
    /*
     * Definitions for C++ 2.0 and later require extern "C" { decl; }
     */
#   define EXTERN_C_FUNCTION( rtn, args ) extern "C" { CUDA rtn args; }
#   define EXTERN_C  extern "C"
#   define FUNCTION_DECL( rtn, args )  CUDA rtn args
#   define FUNCTION_DEF( rtn, args ) CUDA rtn args
#   define FUNCTION_PARAMETER( rtn, args ) rtn args
#   define STRUCT_TAG( tag_name ) /* the tag disappears */
#   define ENUM_BITFIELD( enum_type ) unsigned
#   define ENUM_TYPE( enum_sp, enum_ty ) enum_ty
#   define DOTDOTDOT ...
#   define _VOID_ void

#elif defined(__STDC__)
    /*
     * Definitions for ANSI C
     */
#   define EXTERN_C_FUNCTION( rtn, args ) CUDA rtn args
#   define EXTERN_C  extern
#   define FUNCTION_DECL( rtn, args ) CUDA rtn args
#   define FUNCTION_DEF( rtn, args ) CUDA rtn args
#   define FUNCTION_PARAMETER( rtn, args ) rtn args
#   define STRUCT_TAG( tag_name ) tag_name
#   define ENUM_BITFIELD( enum_type ) unsigned
#   define ENUM_TYPE( enum_sp, enum_ty ) enum_sp enum_ty
#   define DOTDOTDOT ...
#   define _VOID_ void

#else
    /*
     * Definitions for Sun/K&R C -- ignore function prototypes,
     * but preserve tag names and enum bitfield declarations.
     */
#   define EXTERN_C_FUNCTION( rtn, args ) rtn()
#   define EXTERN_C  extern
#   define FUNCTION_DECL( rtn, args ) rtn()
#   define FUNCTION_DEF( rtn, args ) rtn args /* This will probably not work */
#   define STRUCT_TAG( tag_name ) tag_name
#   define ENUM_BITFIELD( enum_type ) enum_type
#   define ENUM_TYPE( enum_sp, enum_ty ) enum_sp enum_ty
#   define DOTDOTDOT ...
#   define _VOID_
    /* VOID is only used where it disappears anyway */

#endif /* Which type of C/C++ compiler are we using? */

#ifdef WIN32
#   define EXTERN_DLL extern _declspec(dllimport)
#else
#   define EXTERN_DLL extern
#endif

#endif
