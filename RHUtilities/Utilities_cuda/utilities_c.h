/* @(#)utilities_c.h	1.11 28 Nov 1995 */
#ifndef utilities_c_h
#define utilities_c_h

#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/rhlist.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/marquardt.h>
#include <Utilities_cuda/table.h>

#include <Utilities_cuda/mod_dcl.h>
#include <Utilities_cuda/random_dcl.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/array_utils_0_dcl.h>
#include <Utilities_cuda/gaussj_dcl.h>
#include <Utilities_cuda/solve2_dcl.h>
#include <Utilities_cuda/svalue_dcl.h>
#include <Utilities_cuda/svalue3_dcl.h>
#include <Utilities_cuda/pseudo_dcl.h>
#include <Utilities_cuda/symmetric_dcl.h>
#include <Utilities_cuda/fileopen_dcl.h>
#include <Utilities_cuda/ffileopen_dcl.h>
#include <Utilities_cuda/bail_out_dcl.h>
#include <Utilities_cuda/fibonaci_min_dcl.h>
#include <Utilities_cuda/error_message.h>
#include <Utilities_cuda/io_dcl.h>
/* #include <Utilities_cuda/concat_dcl.h> */
#include <Utilities_cuda/QRdecomp_dcl.h>
#include <Utilities_cuda/choleski_dcl.h>
#include <Utilities_cuda/determinant_dcl.h>
#include <Utilities_cuda/fourier_dcl.h>
#include <Utilities_cuda/minsolve_dcl.h>
#include <Utilities_cuda/svd_dcl.h>
#include <Utilities_cuda/svd3x3_dcl.h>
#include <Utilities_cuda/fourier2D_dcl.h>
#include <Utilities_cuda/fourier_real_dcl.h>
#include <Utilities_cuda/jacobi_dcl.h>
#include <Utilities_cuda/zroots_dcl.h>
#include <Utilities_cuda/eigenvalues_dcl.h>
#include <Utilities_cuda/showtime_dcl.h>
#include <Utilities_cuda/base1_interface_dcl.h>
#include <Utilities_cuda/try_crash_dcl.h>
#include <Utilities_cuda/polynomials_dcl.h>
#include <Utilities_cuda/qsort_cmp_dcl.h>
#endif
