#ifndef _fibonaci_min_c_dcl_
#define _fibonaci_min_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( double fibonnacci_min, (
   double (*f) (double), /* The function to be minimized */
   double low, double high,	/* The bounds in which the minimum lies */
   int (*time_to_stop) 	/* The maximum acceptable error */
	   (double, double, double, double, double, double)
   ));

#endif
