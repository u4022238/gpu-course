#ifndef _determinant_c_dcl_
#define _determinant_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( double determinant_base1, (
        double **A,
        int n));

#endif
