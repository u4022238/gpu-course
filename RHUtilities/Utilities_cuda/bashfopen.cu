
#include <Utilities_cuda/utilities_c.h>

/* fopen on a file which may be a symbolic link in the style of Cygwin */
#define bashMaxFnameLength 1024
#define bashMaxLinks 20

FUNCTION_DEF (static rhBOOLEAN is_absolute_pathname, (const char *pathname))
   {
   if (pathname[0] == '/') return TRUE;
   if (pathname[1] == ':' && (pathname[2] == '/' || pathname[2] == '\\')) 
      return TRUE;
   return FALSE;
   }

FUNCTION_DEF (static void create_composite_name, (
        char *currentname,
        char *linkname
        ))
   {
   /* Makes a composite name from the current and link names */
   if (is_absolute_pathname (linkname))
      {
      strcpy (currentname, linkname);
      return;
      }

   /* replace the last part of the name with the linkname */
   
   /* Find the last slash in the name */
      {
      /* Set ptr to point to the null at the end of currentname */
      char *ptr = currentname + strlen(currentname);

      /* Go backwards until we find a slash or hit the start of the string */
      while (ptr != currentname-1 && *ptr != '/') ptr--;

      /* Now copy at this point */
      strcpy (ptr+1, linkname);
      }
   }

FUNCTION_DEF (char *rhCyglinkName, (const char *fnamein))
   {
   /* Determines the Cygwin link name of a file */
   int count;

   /* File to read the link information from */
   FILE *tfile = NULL;

   /* Buffer for the current name of the file */
   static char currentname[bashMaxFnameLength];

   /* Buffer for the name read from the link file */
   char linkname[bashMaxFnameLength];

   /* Initial name is the input name */
   if (fnamein != currentname)
      strcpy (currentname, fnamein);

   for (count=1; ; count++)
      {
      /* If there have been too many links, then stop */
      if (count >= bashMaxLinks)
         {
         error_message ("CyglinkName : Link count exceeded");
         break;
         }

      /* Try opening for read */
      tfile = fopen (currentname, "r");

      /* If file can not be opened, then return NULL */
      if (tfile == NULL) break;
   
      /* Now, see if this is a symlink */
         {
         /* Read the start of the file */
         const static char *symlink_header = "!<symlink>";
         int hsize = strlen (symlink_header);
         char header [32];              /* Needs to be bigger than hsize */
         int n = fread (header, 1, hsize, tfile);

         /* If this the wrong number of bytes is read, then this can not
            be a symbolic link, so return the file itself */
         if (n != hsize) break;

         /* Otherwise, compare the header with symbolic file */
         if (strncmp (header, symlink_header, hsize) != 0)
            {
            /* This is not a symbolic link */
            break;
            }
         }

      /* OK, this file is a symbolic link. Open it */

      /* First step, find what it is a symbolic link to */
         {
         char *lname = linkname;
   
         /* Read the file name */
         int n = fread (linkname, 1, bashMaxFnameLength, tfile);
   
         /* If we have really read n characters, then this is too many */
         if (n == bashMaxFnameLength)
            break;
   
         /* Translate //g or similar things to g: */
         if (linkname[0] == '/' && linkname[1] == '/')
            {
            linkname[1] = linkname[2];
            linkname[2] = ':';
            lname = linkname + 1;
            }
   
         /* Close the file, since we are done with it */
         fclose (tfile);

         /* Copy the string name and try again */
         create_composite_name (currentname, lname);
         }
      }

   /* At this point, the input file is known the same as the input */
   if (tfile != NULL) fclose (tfile);
   return currentname;
   }

FUNCTION_DEF (FILE *rhfopen, (const char *fname, const char *mode))
   {
   /* Opens a file for read or write */
   return fopen (rhCyglinkName (fname), mode);
   }
