/* @(#)interrupt.c      1.5 01 Oct 1995 */
/* Routines for handling an interrupt to tell us to stop after next iteration */

#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/rh_util.h>
#include <signal.h>
#define MAX_NUM_SIGNALS 64

static int sig_interrupt_pending[MAX_NUM_SIGNALS];

FUNCTION_DECL ( static void interrupt, (int errno));
FUNCTION_DECL ( int interrupt_pending, ());
FUNCTION_DECL ( void set_interrupt, ());
FUNCTION_DECL ( void cancel_interrupt, ());

static void interrupt (int_level)
   int int_level;
   {
   sig_interrupt_pending[int_level] = 1;
#ifndef __CUDA_ARCH__
   fprintf (stderr, "** INTERRUPT : signal[%d] set to 1\n", int_level);
#else
   printf ("** INTERRUPT : signal[%d] set to 1\n", int_level);
#endif
   }

int interrupt_pending (int_level)
   int int_level;
   {
#ifndef __CUDA_ARCH__
   fprintf (stderr, "Query : level %d, returning value %d\n", 
#else
   printf ("Query : level %d, returning value %d\n", 
#endif
        int_level, sig_interrupt_pending[int_level]);
   return sig_interrupt_pending[int_level];
   }

void set_interrupt (int_level)
   int int_level;
   {
   /* Set up the interrupt_pending value */
#ifndef __CUDA_ARCH__
   fprintf (stderr, "Setting interrupt level %d\n", int_level);
#else
   printf ("Setting interrupt level %d\n", int_level);
#endif
   sig_interrupt_pending[int_level] = 0;
   signal (int_level, interrupt);
   }

void cancel_interrupt (int_level)
   int int_level;
   {
   sig_interrupt_pending[int_level] = 0;
   signal (int_level, SIG_DFL);
   }

