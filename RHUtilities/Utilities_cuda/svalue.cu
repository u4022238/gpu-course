/* @(#)svalue.c 1.13 28 Nov 1995 */
/*>>
 Singular value decomposition routines modelled on 
  "Numerical Recipes in C " (Cambridge University Press 1988)
  but rewritten.
<<*/

#include <Utilities_cuda/cvar.h>
#include <math.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/error_message.h>
#include <Utilities_cuda/svalue3_dcl.h>

/*>> 
Solution of linear equations using SVD.
<<*/

FUNCTION_DEF ( void svbksb_base1, (
        double **u,
        double w[],
        double **v,
        int m,
        int n,
        double b[],
        double x[]))

   /* Solves A.X = B for a vector X, where A is specified by the arrays 
      u[1..m][1..n], w[1..n], v[1..n][1..n] as returned by svdcmp.
      m and n are dimensions of A, and will be equal for square matrices.
      b[1..m] is the input right-hand side.
      x[1..n] is the output solution vector.
      No input quantities are destroyed, so the routine may be called 
      sequentially with different b's.
   */

   {
   int i, j;

   /* We must take care if x = bin -- must make a temporary */
   double *xtemp = VECTOR (1, n, double);

   /* Compute the product W^(-1) U^T . B */
   for (j=1; j<=n; j++)
      {
      double val = 0.0;
      if (w[j])         /* Nonzero result only if w[j] is non-zero. */
         {
         for (i=1; i<=m; i++)
            val += u[i][j] * b[i];
         val /= w[j];
         }
      xtemp[j] = val;
      }

   /* Matrix multiply by V to get answer */
   matrix_vector_prod_base1 (v, xtemp, n, n, x);

   /* Free temporary */
   FREE (xtemp, 1);
   }

FUNCTION_DEF ( int solve_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol))
   {
   /* Find the least-squares solution to an over-constrained linear system.
    * The inputs are 
    *    a[1..nrow][1..ncol]
    *    b[1..nrow]
    * The output is 
    *    x[1..ncol]
    *
    * The routine returns 1 on success, 0 on failure.  It fails if the SVD
    * fails.
    */
   int i, j;
   double *w, **v, **u, *w2;
   double besterror = HUGE_VAL;
   double bestthreshold = 0.0;
   int returnval = 1;
   
   /* Allocate matrices */
   u = MATRIX (1, nrow, 1, ncol, double);
   w = VECTOR (1, ncol, double);
   w2 = VECTOR (1, ncol, double);
   v = MATRIX (1, ncol, 1, ncol, double);

   /* Copy a to u */
   for (i=1; i<=nrow; i++)
      for (j=1; j<=ncol; j++)
          u[i][j] = a[i][j];

   /* Now do the singular value decomposition */
      {
      int yes = 1;  /* Accumulate u */
      if (! svdcmp_base1(u, nrow, ncol, w, v, yes))     /* Needs both u and v */
         {
         returnval = 0;
         goto cleanup;
         }
      }

   /* Go into a loop of back-substitution, to find the best */
   for (j=1; j<=ncol; j++)
      {
      /* Threshold on the i-th singular value */
      double wmin = rhAbs(w[j]);

      /* Set near-zero values to zero */
      for (i=1; i<=ncol; i++)
         {
         if ((rhAbs(w[i]) < wmin)) w2[i] = 0.0;
         else w2[i] = w[i];
         }

      /* Back substitute */
      svbksb_base1 (u, w2, v, nrow, ncol, b, x); 

      /* Calculate the difference */
         {
         int i, k;
         double error = 0.0;
         for (i=1; i<=nrow; i++)
            {
            double bt = 0.0;
            for (k=1; k<=ncol; k++) bt += a[i][k] * x[k];
            error += (bt-b[i]) * (bt-b[i]);
            }

         /* If this is better than the best error, then record */
         if (error <= besterror)
            {
            besterror = error;
            bestthreshold = wmin;
            }
         }
      }

   /* Repeat the calculation with the best values */
      {
      double wmin = bestthreshold;
   
      /* Set near-zero values to zero */
      for (i=1; i<=ncol; i++)
         {
         if ((rhAbs(w[i]) < wmin)) w2[i] = 0.0;
         else w2[i] = w[i];
         }

      /* Back substitute */
      svbksb_base1 (u, w2, v, nrow, ncol, b, x); 
      }

   /* Free the temporaries */
cleanup: 
   FREE (u, 1);
   FREE (w, 1);
   FREE (w2, 1);
   FREE (v, 1);

   /* return success */
   return (returnval);
   }

#define NEAR_ZERO 1.0e-12

FUNCTION_DEF ( int solve_simple_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol))
   {
   /* Find the least-squares solution to an over-constrained linear system.
    * The inputs are 
    *    a[1..nrow][1..ncol]
    *    b[1..nrow]
    * The output is 
    *    x[1..ncol]
    * It is allowable for x and b to be the same vector.
    *
    * The routine returns 1 on success, 0 on failure.  It fails if the SVD
    * fails.
    */
   int i, j;
   double *w, **v, **u, *w2;
   double wmax;
   int returnval = 1;
   
   /* Allocate matrices */
   u = MATRIX (1, nrow, 1, ncol, double);
   w = VECTOR (1, ncol, double);
   w2 = VECTOR (1, ncol, double);
   v = MATRIX (1, ncol, 1, ncol, double);

   /* Copy a to u */
   for (i=1; i<=nrow; i++)
      for (j=1; j<=ncol; j++)
          u[i][j] = a[i][j];

   /* Now do the singular value decomposition */
      {
      const int yes = 1;  /* Accumulate u */
      if (! svdcmp_base1(u, nrow, ncol, w, v, yes))     /* Needs both u and v */
         {
         returnval = 0;
         goto cleanup;
         }
      }

   /* Find the maximum singular value */
   wmax = -1.0;
   for (j=1; j<=ncol; j++)
      {
      /* Threshold on the i-th singular value */
      double wcurr = rhAbs(w[j]);
      if (wcurr > wmax) wmax = wcurr;
      }

   /* Repeat the calculation with the best values */
      {
      double wmin = wmax * NEAR_ZERO;
   
      /* Set near-zero values to zero */
      for (i=1; i<=ncol; i++)
         {
         if ((rhAbs(w[i]) < wmin)) w2[i] = 0.0;
         else w2[i] = w[i];
         }

      /* Back substitute */
      svbksb_base1 (u, w2, v, nrow, ncol, b, x); 
      }

   /* Free the temporaries */
cleanup: 
   FREE (u, 1);
   FREE (w, 1);
   FREE (w2, 1);
   FREE (v, 1);

   /* return success */
   return (returnval);
   }

