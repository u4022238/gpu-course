/* @(#)svd3x3.c 1.7 29 Nov 1995 */
/* Routines for doing Higgins camera modelling stuff */

#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/svalue3_dcl.h>

FUNCTION_DEF ( int svd3x3_base1, (
   double **A,          /* The matrix input (3x3) */
   double **U,          /* The output matrix */
   double *D,           /* The diagonal */
   double **V           /* The other output matrix */
   ))
   {
   /* Does the singular value decomposition of a 3x3 matrix, and
    * checks that the singular values are in descending order and that the
    * matrix V has positive determinant */

   /* Declarations */
   int i, j;            /* Used as loop counters */

   /* If U is NULL, then we must make a new one */
   double **Utemp;
   if (U)
      Utemp = U;
   else
      Utemp = MATRIX (1, 3, 1, 3, double);

   /* Copy A to U */
   for (i=1; i<=3; i++) for (j=1; j<=3; j++) 
      Utemp[i][j] = A[i][j];

   /* Now do the singular value decomposition */
   if (! svdcmp_base1 (Utemp, 3, 3, D, V, (U!=NULL))) return 0;

   /* Just make sure that the singular values are in the right descending
    * order. */

   /* Simply sort the singular values, and change U and V */
   if (D[1] < D[2])
      {
      /* Swap the first two singular values */
      if (U) swap_columns (U, 1, 3, 1, 2);
      if (V) swap_columns (V, 1, 3, 1, 2);

      /* Swap the entries of D */
         {
         double temp;
         temp = D[1]; D[1] = D[2]; D[2] = temp;
         }
      }

   if (D[2] < D[3])
      {
      /* Swap the second and third singular values */
      if (U) swap_columns (U, 1, 3, 2, 3);
      if (V) swap_columns (V, 1, 3, 2, 3);

      /* Swap the entries of D */
         {
         double temp;
         temp = D[2]; D[2] = D[3]; D[3] = temp;
         }
      
      if (D[1] < D[2])
         {
         /* Swap the first and second singular values */
         if (U) swap_columns (U, 1, 3, 1, 2);
         if (V) swap_columns (V, 1, 3, 1, 2);

         /* Swap the entries of D */
            {
            double temp;
            temp = D[1]; D[1] = D[2]; D[2] = temp;
            }
         }
      }

   /* Make sure that the matrix V has positive determinant */
   if (V && det3x3_base1(V) < 0.0)
      {
      /* Change the sign of both U and V */
      for (i=1; i<=3; i++) for (j=1; j<=3; j++)
         {
         if (U) U[i][j] = -U[i][j];
         if (V) V[i][j] = -V[i][j]; 
         }
      }

   /* Return successful value */
   return 1;
   }

FUNCTION_DEF ( int svd2x2_base1, (
   double **A,          /* The matrix input (2x2) */
   double **U,          /* The output matrix */
   double *D,           /* The diagonal */
   double **V           /* The other output matrix */
   ))
   {
   /* Does the singular value decomposition of a 2x2 matrix, and
    * checks that the singular values are in descending order and that the
    * matrix V has positive determinant */

   /* Declarations */
   int i, j;            /* Used as loop counters */

   /* If U is NULL, then we must make a new one */
   double **Utemp;
   if (U)
      Utemp = U;
   else
      Utemp = MATRIX (1, 3, 1, 3, double);

   /* Copy A to Utemp */
   for (i=1; i<=2; i++) for (j=1; j<=2; j++) 
      Utemp[i][j] = A[i][j];

   /* Now do the singular value decomposition */
   if (! svdcmp_base1 (Utemp, 2, 2, D, V, (U != NULL))) return 0;

   /* Just make sure that the singular values are in the right descending
    * order. */

   /* Simply sort the singular values, and change U and V */
   if (D[1] < D[2])
      {
      /* Swap the first two singular values */
      if (U) swap_columns (U, 1, 2, 1, 2);
      if (V) swap_columns (V, 1, 2, 1, 2);

      /* Swap the entries of D */
         {
         double temp;
         temp = D[1]; D[1] = D[2]; D[2] = temp;
         }
      }

   /* Make sure that the matrix V has positive determinant */
   if (V && det2x2_base1(V) < 0.0)
      {
      /* Change the sign of both U and V */
      for (i=1; i<=2; i++) for (j=1; j<=2; j++)
         {
         if (U) U[i][j] = -U[i][j];
         if (V) V[i][j] = -V[i][j]; 
         }
      }

   /* Return successful value */
   return 1;
   }

