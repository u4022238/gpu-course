/* @(#)pseudo.c 1.1 28 Nov 1995 */
/*>>
Solves an overconstrained linear system by the method of pseudo-inverses.
<<*/

#include <stdio.h>
#include <Utilities_cuda/cvar.h>
#include <math.h>
#include <Utilities_cuda/rh_util.h>
#include <Utilities_cuda/alloc.h>
#include <Utilities_cuda/array_utils_dcl.h>
#include <Utilities_cuda/gaussj_dcl.h>
#include <Utilities_cuda/error_message.h>
#include <Utilities_cuda/symmetric_dcl.h>


FUNCTION_DEF ( int solve_pseudo_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol))
   {
   /* Find the least-squares solution to an over-constrained linear system.
    * The inputs are 
    *    a[1..nrow][1..ncol]
    *    b[1..nrow]
    * The output is 
    *    x[1..ncol]
    * It is allowable for x and b to be the same vector.
    *
    * The routine returns 1 on success, 0 on failure.  It fails if the
    * system is of deficient rank.
    */

   int i, j, k;
   int returnval = 1;

   /* Allocate matrices */
   double **aa = MATRIX (1, ncol, 1, ncol, double);
   double *b2  = VECTOR (1, ncol, double);

   /* Clear out aa */
   for (j=1; j<=ncol; j++)
      for (k=j; k<=ncol; k++)
         aa[j][k] = 0.0;

   /* Clear out b2 */
   for (j=1; j<=ncol; j++)
      b2[j] = 0.0;

   /* Form a^t * a */

   /* Compute the upper half only */
   for (i=1; i<=nrow; i++)
      for (j=1; j<=ncol; j++)
         for (k=j; k<=ncol; k++)
             aa[j][k] += a[i][j]*a[i][k];

   /* Fill in the symmetric lower half */
   for (j=1; j<=ncol; j++)
      for (k=j+1; k<=ncol; k++)
           aa[k][j] = aa[j][k];

   /* Also, multiply the goal vector by a^t */
   for (i=1; i<=nrow; i++)
      for (j=1; j<=ncol; j++)
         b2[j] += a[i][j] * b[i];

   /* Solve the set of equations */
   if (! solve_symmetric_base1 (aa, ncol, b2))
      {
      returnval = 0;
      goto cleanup;
      }

   /* Copy the solution to x */
   for (j=1; j<=ncol; j++)
      x[j] = b2[j];

   /* Cleanup */
cleanup : 
   FREE (aa, 1);
   FREE (b2, 1);

   /* return success */
   return returnval;
   }

