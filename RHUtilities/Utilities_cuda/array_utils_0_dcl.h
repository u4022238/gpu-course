#ifndef _array_utils_0_c_dcl_
#define _array_utils_0_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( int p_matrix_0, ( double **a, int m, int n));

EXTERN_C_FUNCTION ( int p_vector_0, ( double *a, int n));

EXTERN_C_FUNCTION ( void matrix_prod_0, (
        double **A,
        double **B,
        int l,
        int m,
        int n,
        double **X));

EXTERN_C_FUNCTION ( void vector_matrix_prod_0, (
        double *b,
        double **A,
        int l,
        int m,
        double *x));

EXTERN_C_FUNCTION ( void matrix_vector_prod_0, (
        double **A,
        double *b,
        int l,
        int m,
        double *x));

EXTERN_C_FUNCTION ( double inner_product_0, (
        double *A,
        double *B,
        int n));

EXTERN_C_FUNCTION ( double vector_length_0, (
        double *A,
        int n));

EXTERN_C_FUNCTION ( int matrix_inverse_0, (
   double **A,		/* The matrix to be inverted */
   double **Ainv,	/* The inverse computed */
   int n		/* Dimension of the matrix */
   ));

EXTERN_C_FUNCTION ( double det3x3_0, (
        double **A));

EXTERN_C_FUNCTION ( double det2x2_0, (
        double **A));

EXTERN_C_FUNCTION ( void cofactor_matrix_3x3_0, (
        double **A,
        double **Aadj));

EXTERN_C_FUNCTION ( void cross_product_0, (
   double *A, double *B,	/* Two vectors 0..2 */
   double *C			/* Their cross product */
   ));

EXTERN_C_FUNCTION ( int lin_solve_0, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ));

EXTERN_C_FUNCTION ( int lin_solve_symmetric_0, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ));

EXTERN_C_FUNCTION ( void identity_matrix_0, (
        double **A,
        int dim));

#endif
