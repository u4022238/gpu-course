/*
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
*/

/* @(#)rh_util.h	3.19 12/02/96 */

/*
 * This is a general utilities macro file, used for both c and c++ 
 * applications.
 */

#ifndef _standard_rih_utils_file
#define _standard_rih_utils_file

#ifdef __cplusplus

//---------------------------------------------------------------------
//
//	This is the C++ version.
//
//---------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <Utilities_cuda/bail_out_dcl.h>
typedef short rhBOOLEAN;

/* External declarations */
#include <stdlib.h>
#include <string.h>
/* #include <alloca.h> */
#ifndef __APPLE__ /* OsX defines malloc in stdlib.h */
#include <malloc.h>
#endif
#include <math.h>

#ifndef TRUE
#   define TRUE    1
#endif

#ifndef FALSE
#   define FALSE   0
#endif

/* Redefinition of fopen */
#ifdef WIN32
#  include <Utilities_cuda/bashfopen_dcl.h>
#else
#  define rhfopen fopen
#  define rhCyglinkName(c) (c)
#endif

/* Maximum and minimum */
const int rhMAXINT  = 2147483647;
inline __device__ __host__ 
int    rhMax(int a, int b) 	{ return (a>b ? a : b); }

inline __device__ __host__ 
double rhMax(double a, double b) { return (a>b ? a : b); }

inline __device__ __host__ 
int    rhMin(int a, int b) 	{ return (a>b ? b : a); }

inline __device__ __host__ 
double rhMin(double a, double b) { return (a>b ? b : a); }

inline __device__ __host__ 
int    rhAbs (int a)    { return (a>0 ? a : -a); }

inline __device__ __host__ 
double rhAbs (double a) { return (a>0.0 ? a : -a); }

/* Allocation stuff */

inline __device__ __host__ void ABORT () 
   { 
#ifndef __CUDA_ARCH__
   fprintf(stderr,"ERROR ***Out of space.***\n"); 
   bail_out(1); 
#else
   printf("ERROR ***Out of space.***\n"); 
   asm("exit;");
#endif
   }

inline __device__ __host__ 
char *MALLOC(size_t len)  
   {
   if(len == 0) return (char*)0;
   register char *zztchar = (char *)malloc(len);
   if (zztchar == (char *)0) ABORT();
   return zztchar;
   }

#define REALLOC(arr,arr_size) \
   if ((arr_size *= 2, arr = realloc(arr,arr_size*sizeof(*arr)))== 0) \
      ABORT()
 
#define TESTALLOC(arr,arr_size,arr_count) \
   if(arr_count>=arr_size) REALLOC(arr,arr_size)

#define NEW(p) ((p *)MALLOC(sizeof(p)))

/*
 * String routines.
 */

inline __device__ __host__ 
char * COPY(const char *str)
   {
   return (str ? strcpy(MALLOC(strlen(str)+1),str) : (char *)0);
   }

/* Macro for opening files */
#undef FOPEN
inline __device__ __host__ 
FILE *FOPEN (const char *fname, const char *amode)
   {
   register FILE *zztFILE=rhfopen((fname),(amode));
   if (zztFILE ==0)
      {
#ifndef __CUDA_ARCH__
      fprintf (stderr, "Unable to open file %s for %s.\n", 
	      (fname),	
         ((amode)[0]=='r') 						
	         ? "read" 						
	         :((amode)[0]=='w'||(amode)[0]=='a' ?"write":"(bad mode)")
	      );

      perror (fname);						
      bail_out(1);
#else
      printf ("Unable to open file %s for %s.\n", 
	      (fname),	
         ((amode)[0]=='r') 						
	         ? "read" 						
	         :((amode)[0]=='w'||(amode)[0]=='a' ?"write":"(bad mode)")
	      );
#endif

      }

   return zztFILE;
   }

#else

/*---------------------------------------------------------------------
//
//	This is the standard C version.
//
//---------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#define TRUE    1
#define FALSE   0
typedef short rhBOOLEAN;

#define rhMAXINT 2147483647

/* External declarations */
#include <string.h>
#ifndef __APPLE__ /* OsX defines malloc in stdlib.h */
#include <malloc.h>
#endif
#include <math.h>
#include <Utilities_cuda/bail_out_dcl.h>
#include <Utilities_cuda/dll.h>
extern char *mktemp();

/* Temporary variables used in macros */
EXTERN_C UTILITIES_DLLDATA char *zztchar;
EXTERN_C UTILITIES_DLLDATA FILE *zztFILE;

/* Maximum and minimum */

#define rhMax(a,b) ((a)>(b) ? (a) : (b))
#define rhMin(a,b) ((a)>(b) ? (b) : (a))
#define rhAbs(a)   ((a)>0 ? (a) : -(a))

/* Macro nefinitions */
#define MALLOC(len)						\
	( (len==0) ? (char *) 0 :				\
	(  ( (zztchar=malloc((unsigned)len)) == 0 ) ?		\
	(  fprintf(stderr,"ERROR ***Out of space.***\n"),	\
	   bail_out(1), (char *) 0  ) :zztchar )  )

#define NEW(p) ((p *)MALLOC(sizeof(p)))

#define STRCAT(str1,str2) \
	(strcat(strcpy(MALLOC(strlen(str1)+strlen(str2)+1),str1),str2))

#define COPY(str) (str?strcpy(MALLOC(strlen(str)+1),str):0)

#define ABORT {fprintf(stderr,"ERROR ***Out of space.***\n"); bail_out(1);}

#define REALLOC(arr,arr_size) 						\
	if ((arr_size *= 2, arr = realloc(arr,arr_size*sizeof(*arr)))== 0) \
	   ABORT

#if 0
/* No longer in use, and causes problems with definition of calloc */
extern char *calloc();
#define CALLOC(arr,arr_size) \
	if ((arr_size = 16, arr = calloc(arr_size, sizeof(*arr))) == 0) ABORT
#endif

#define TESTALLOC(arr,arr_size,arr_count) \
	if(arr_count>=arr_size) REALLOC(arr,arr_size)

/* Macro for opening files */
#define FOPEN(fname,amode) 						\
	((zztFILE=rhfopen((fname),(amode)))==0				\
	? (FILE *)((fprintf (stderr, 					\
			"Unable to open file %s for %s.\n", (fname),	\
	   ((amode)[0]=='r') 						\
		?"read" 						\
		:((amode)[0]=='w'||(amode)[0]=='a' ?"write":"(bad mode)")),\
          perror (fname),						\
	  bail_out(1)))							\
	: zztFILE)
	
#endif

/*---------------------------------------------------------------------
 *
 *
 *	Common to both
 *
 *---------------------------------------------------------------------*/

/* Include allocation routines as well */
#include <Utilities_cuda/alloc.h>
/* #include <Utilities_cuda/concat_dcl.h> */

/* If M_PI did not get defined, then do it now */
#if !defined(M_PI)
  #define M_PI 3.14159265358979323846 /* From non-ANSI <math.h> */
  #define M_1_PI          0.31830988618379067154
  #define M_PI_2          1.57079632679489661923
#endif

#endif
