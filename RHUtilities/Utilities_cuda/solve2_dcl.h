#ifndef _solve2_c_dcl_
#define _solve2_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION ( int solve_2_base1, (
        double **a,
        double *x,
        int nrow,
        int ncol));

#endif
