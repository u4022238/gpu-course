/* @(#)mod.c    1.3 01 Oct 1995 */
/* Mod function that always returns a positive value */

#include <Utilities_cuda/cvar.h>

FUNCTION_DEF ( int mod, (
        int m,
        int n))
   {
   register int k = m % n;
   return (k>=0 ? k : k+n);
   }

