/* @(#)QRdecomp.c       1.4 07 Nov 1994 */

/* */
/* QR decomposition */
/* */

#include <Utilities_cuda/utilities_c.h>
#include <Utilities_cuda/cvar.h>

FUNCTION_DEF ( void QRdecomposition_3x3_base1, (
   double **A,  /* The input matrix */
   double **K,  /* The output upper triangular matrix */
   double **R   /* The output rotation matrix */
   ))
   {
   /*
    * Decomposes A as KR, where K is upper triangular and R is a rotation 
    * This is only for 3 x 3 matrices. 
    * It is allowed that R is the same as A, or K is the same as A.
    * The decomposition will be such that all diagonal entries of K
    * have the same sign.
    */

   double a, b, len;
   double **B = MATRIX (1, 3, 1, 3, double);

   /* Copy A to K */
   matrix_copy (A, K, 1, 3, 1, 3);

   /* First, set R to the identity matrix */
   R[1][1] = 1.0; R[1][2] = 0.0; R[1][3] = 0.0;
   R[2][1] = 0.0; R[2][2] = 1.0; R[2][3] = 0.0;
   R[3][1] = 0.0; R[3][2] = 0.0; R[3][3] = 1.0;

   /* First clear out the (3, 1) entry */
   a = K[3][2];
   b = K[3][1];
   len = sqrt (a*a + b*b);

   if (len > 1.0e-30)
      {
      a /= len;
      b /= len;

      B[1][1] =   a; B[1][2] =   b; B[1][3] = 0.0;
      B[2][1] =  -b; B[2][2] =   a; B[2][3] = 0.0;
      B[3][1] = 0.0; B[3][2] = 0.0; B[3][3] = 1.0;

      matrix_prod_base1 (K, B, 3, 3, 3, K);
      matrix_prod_base1 (R, B, 3, 3, 3, R);
      }

   /* Next clear out the (3, 2) entry */
   a = K[3][3];
   b = K[3][2];
   len = sqrt(a*a + b*b);

   if (len > 1.0e-30)
      {
      a /= len;
      b /= len;

      B[1][1] = 1.0; B[1][2] = 0.0; B[1][3] = 0.0;
      B[2][1] = 0.0; B[2][2] =   a; B[2][3] =   b;
      B[3][1] = 0.0; B[3][2] =  -b; B[3][3] =   a;

      /* Now, carry out the multiplications */
      matrix_prod_base1 (K, B, 3, 3, 3, K);
      matrix_prod_base1 (R, B, 3, 3, 3, R);
      }

   /* Finally clear out the (2, 1) entry */
   a = K[2][2];
   b = K[2][1];
   len = sqrt (a*a + b*b);

   if (len > 1.0e-30)
      {
      a /= len;
      b /= len;

      B[1][1] =   a; B[1][2] =   b; B[1][3] = 0.0;
      B[2][1] =  -b; B[2][2] =   a; B[2][3] = 0.0;
      B[3][1] = 0.0; B[3][2] = 0.0; B[3][3] = 1.0;

      matrix_prod_base1 (K, B, 3, 3, 3, K);
      matrix_prod_base1 (R, B, 3, 3, 3, R);
      }

   /* Then transpose R */
   transpose (R, R, 1, 3, 1, 3);

   /* We also want all the entries of K to have the same sign */
      {
      /* Count the number of rows/columns we swap */
      int i, j;
      int count = 0;

      /* Try the diagonal entries one at a time */
      for (i=1; i<=3; i++)
         if (K[i][i] < 0.0)
            {
            /* Swap signs of column of K and row of R */

            /* Swap signs of column of K */
            for (j=1; j<=i; j++)
               K[j][i] = - K[j][i];

            /* Swap signs of row of R */
            for (j=1; j<=3; j++)
              R[i][j] = - R[i][j];

            /* Count the number of swaps we make */
            count += 1;
            }

      /* See if we have done an odd number of swaps */
      if (count % 2 == 1)
         {      
         /* Multiply both K and R by to make sure that R has det = 1 */
         for (j=1; j<=3; j++)   
           for (i=1; i<=j; i++)
              K[i][j] = -K[i][j];

         for (i=1; i<=3; i++)   
           for (j=1; j<=3; j++)
              R[i][j] = -R[i][j];
         }
      }

   /* Set the entries of K to zero exactly */
   K[2][1] = K[3][1] = K[3][2] = 0.0;

   /* Free B */
   FREE (B, 1);
   }

