#ifndef _bashfopen_c_dcl_
#define _bashfopen_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION (char *rhCyglinkName, (const char *fnamein));

EXTERN_C_FUNCTION (FILE *rhfopen, (const char *fname, const char *mode));

#endif
