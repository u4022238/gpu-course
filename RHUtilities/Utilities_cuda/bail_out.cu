/* @(#)bail_out.c       1.3 01 Oct 1995 */
/* Routine for bailing out of my libraries */
#include <stdlib.h>
#include <Utilities_cuda/cvar.h>
#include <assert.h>

FUNCTION_DEF ( int bail_out, (int n))
   {
#ifndef __CUDA_ARCH__
   if (n > 1) assert (n == 0);
   exit (n);
   return (n);  /* This is never executed */
#else
   asm ("exit;");
   return n;
#endif
   }
