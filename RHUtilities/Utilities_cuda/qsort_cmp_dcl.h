#ifndef _qsort_cmp_c_dcl_
#define _qsort_cmp_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION (int doublecmp_ascending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int doublecmp_descending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int floatcmp_ascending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int floatcmp_descending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int intcmp_ascending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int intcmp_descending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int shortcmp_ascending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int shortcmp_descending , (
	const void *a, 
	const void *b));

EXTERN_C_FUNCTION (int double_ptrcmp, ( 
         const void *ptr1, 
         const void *ptr2));

EXTERN_C_FUNCTION (int int_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int unsigned_int_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int float_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int short_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int char_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int unsigned_short_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int unsigned_char_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int long_ptrcmp, ( const void *ptr1, const void *ptr2));

EXTERN_C_FUNCTION (int unsigned_long_ptrcmp, ( const void *ptr1, const void *ptr2));

#endif
