/* @(#)random.c 1.8 27 Feb 1996 */
/* Various random number generators */

#include <Utilities_cuda/cvar.h>
#include <math.h>
#include <stdlib.h>

#ifdef WIN32
/*----------------------------------------------------------------*/
/* Windows Visual C++ Compiler */

static long my_RAND_MAX = 0x7fffffff;

FUNCTION_DEF (long rh_random, ())
   {
   return
      ((rand()&0xff)      ) |
      ((rand()&0xff) <<  8) |
      ((rand()&0xff) << 16) |
      ((rand()&0x7f) << 24);
   }

FUNCTION_DEF (void rh_srandom, ( unsigned int n ))
   {
   srand (n);
   }

FUNCTION_DEF ( double rh_irandom, ())
   {
   /* Returns a random number between 0.0 and 1.0 */
   long nran = rh_random();
   return (nran  / (double)my_RAND_MAX);
   }

#else
/*----------------------------------------------------------------*/
/* Gnu compiler */

FUNCTION_DEF (long rh_random, ())
   {
   return lrand48();
   }

FUNCTION_DEF (void rh_srandom, ( unsigned int n ))
   {
   srand48 (n);
   }

FUNCTION_DEF ( double rh_irandom, ())
   {
   /* Returns a random number between 0.0 and 1.0 */
   return drand48();
   }

#endif
/*----------------------------------------------------------------*/

FUNCTION_DEF (double urandom, (double mean, double radius))
   {
   return mean - radius + 2.0 * rh_irandom() * radius;
   }

FUNCTION_DEF ( double grandom, (
        double mean,
        double dev))
   {
   /*
    * Generates a random number with Gaussian deviation with mean and
    * deviation as given.
    * This uses the Box-Muller method.  See routine gasdev, p 217 of Numerical
    * recipes in C
    */

   static int iset = 0;
   static double gset;
   double fac, r, v1, v2;
   FUNCTION_DECL (double rh_irandom, ());

   if (iset)
      {
      /* We already have a number, so simply return it */
      iset = 0;
      return (gset*dev + mean);
      }
   else
      {
      /* Generate a new random number pair */
      /* Start by getting a random point in the unit circle */
      do {
         /* pick two uniform numbers in the square [-1, +1] in each direction */
         v1 = 2.0 * rh_irandom() - 1.0;
         v2 = 2.0 * rh_irandom() - 1.0;
         r = v1*v1 + v2*v2;     /* See if they fall in the unit circle */
         } while (r >= 1.0);

      /* Now do the Box-Muller transformation to get two normal deviates */
      /* return one and save the other */
      fac = sqrt(-2.0*log(r)/r);
      gset = v1*fac;
      iset = 1;
      return (v2*fac*dev + mean);
      }
   }

FUNCTION_DEF (int random_choice, (double frac))
   {
   /* Generates 0 or 1, depending whether a random is above or below frac */
   return rh_irandom() < frac;
   }

FUNCTION_DEF ( void circular_random, (double *x, double *y))
   {
   while (1)
      {
      // Try a pair of values
      *x = urandom(0.0, 1.0);
      *y = urandom(0.0, 1.0);

      // If in the circle, then OK
      if ((*x)*(*x) + (*y)*(*y) < 1.0) return;
      }
   }



