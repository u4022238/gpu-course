/* @(#)concat.c 1.7 29 Nov 1995 */
/* Routine for concatenating strings.
 * It will concatenate a list of strings terminated by the null string.
 *
 * Refer to manual section varargs(3) to understand the following code 
 */
#include <Utilities_cuda/cvar.h>
#include <Utilities_cuda/rh_util.h>

#ifndef CPARSE_RIH
#   include <stdarg.h>
#endif

FUNCTION_DEF ( char *CONCAT, (const char* string1, DOTDOTDOT))
   {
   /* Concatenates a variable number of strings terminated by a 0 */
   va_list ap;
   int length = 1;
   char *target;
   char *p;
   
   /* Find the length of the required string */
      {
      char *string;
      va_start (ap, string1);
      string = (char *)string1; /* va_arg(ap, char *); */
      while (string != 0)
         {
         length += strlen(string);
         string = va_arg(ap, char *);
         }
      va_end(ap);
      }

   /* Now allocate enough space to hold the string */
   p = target = MALLOC (length);

   /* Now start to fill in the string */
      {
      char *string;
      va_start (ap, string1);
      string = (char *)string1;
      while (string != 0)
         {
         sprintf (p, "%s", string);
         p += strlen(string);
         string = va_arg(ap, char *);
         }
      va_end(ap);
      }

   *p = '\0';

   return (target);
   }
  
