#ifndef _marqu_c_dcl_
#define _marqu_c_dcl_
#include "Utilities_cuda/cvar.h"


EXTERN_C_FUNCTION (double marquardt_0, (Marquardt_info *mar));

EXTERN_C_FUNCTION (void marquardt_debug, (
   double **alpha, 
   double *beta));

#endif
