#ifndef _imageio_cc_dcl_
#define _imageio_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>
#include <Tjinterface/write_png_dcl.h>
#include <vector>
using namespace std;
/* */

FUNCTION_DECL (void tj_interface_set_output_image_type, ( const char *type ));

#endif
