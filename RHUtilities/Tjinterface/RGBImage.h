#ifndef _RGB_IMAGE_H_
#define _RGB_IMAGE_H_

#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"

//------------------------------------------------------------------------------

class RGBImage : public ReferenceCountable
   {
   public :

      Unsigned_char_matrix r;
      Unsigned_char_matrix g;
      Unsigned_char_matrix b;

      RGBImage () {}
      RGBImage (
            Unsigned_char_matrix &r,
            Unsigned_char_matrix &g,
            Unsigned_char_matrix &b ) : r(r), g(g), b(b) {}

      RGBImage (int idim, int jdim)
         {
         // Makes an image of the right size
         r.Init(0, idim-1, 0, jdim-1);
         g.Init(0, idim-1, 0, jdim-1);
         b.Init(0, idim-1, 0, jdim-1);
         }

      int read (const char *fname)
         {
         // Read the image from a file
         return (int) read_pix_band_to_array (1, fname, r) &&
                      read_pix_band_to_array (2, fname, g) &&
                      read_pix_band_to_array (3, fname, b);
         }

      int write (const char *fname)
         {
         // Write the image to a file
         return put_array_to_pix (fname, r, g, b);
         }

      void clear (int v)
         {
         r.clear(v);
         g.clear(v);
         b.clear(v);
         }

      // Dimensions
      int idim  () { return r.idim();}
      int jdim  () { return r.jdim();}
      int ilow  () { return r.ilow();}
      int jlow  () { return r.jlow();}
      int ihigh () { return r.ihigh();}
      int jhigh () { return r.jhigh();}
   };

typedef SmartPointer<RGBImage> RGBImage_ref;

#endif
