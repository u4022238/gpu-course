// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// Routines for outputting arrays as images, and vice versa

/* SFILE_BEGIN */
#include <Utilities_CC/utilities_CC.h>
// #include <ImageClasses/Image.h>
//#include <ImageClasses/MemoryImage.h>
//#include <ImageClasses/BandedImage.h>
/* SFILE_END */

// TargetJr Image stuff
//#include <ImageClasses/ImageReadWrite.h>
//#include <ImageClasses/InitializeImageLoaders.h>
#include <Tjinterface/imageio_dcl.h>
#include <Tjinterface/imageio_color_dcl.h>
#include <iostream>
using namespace std;

inline int nint(double x) { return (int) floor(x+0.5); }

//
//  Identifying color images
//

FUNCTION_DEF (rhBOOLEAN is_color_image, (
      Image_ref pr
      ))
   {
   // Determines whether the image is a color image

   // Exit on failure
   if (pr == NULL) return 0;

   // Determine the number of bytes per pixel
   int n = pr->GetBytesPixel();

   // 3 or more bytes will mean color
   return n >= 3;
   }

FUNCTION_DEF (rhBOOLEAN is_color_image, (
      const char *infilename
      ))
   {
   Image_ref pr = load_image (infilename);
   return is_color_image (pr);
   }

//
// Filling arrays from an image
//

FUNCTION_DEF (int fill_array_from_image_band, (
      int band,               // The band to use
      Image_ref pr,
      unsigned char **val,    // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,       // Extent of the region
      int /*- fill = 0 -*/
      ))
   {
   // Try reading an image into an array

   // Exit on failure
   if (pr == NULL) return 0;

   // Get the dimensions of the part to be read
   int width = jhi - jlo + 1;
   int height = ihi - ilo + 1;

   // If this is a banded image, then we need to get the specific band
      {
      // Try cast to BandedImage
      BandedImage_ref pr_banded = pr->GetBandedImage();

      // Test for banded pixels
      if (pr_banded && pr_banded->GetReturnType() == BANDED)
         {
         // Get the specified band
         Image_ref pr_band = pr_banded->GetBand(band);

         // Test that this is OK
         if (! pr_band) return 0;

         // Get section on the band
         Unsigned_char_vector buf(0, width * height - 1);
         pr_band->GetSection((unsigned char *)buf, jlo, ilo, width, height);

         // Read the pixels into the array
         unsigned char *ptr = (unsigned char *)buf;

         for (int i=ilo; i<=ihi; i++)
            for (int j=jlo; j<=jhi; j++)
               val[i][j] = *(ptr++);

         // Return directly from here
         return 1;
         }
      }

   // Fill up the buffer with the pixel values
   int n = pr->GetBytesPixel();
   Unsigned_char_vector buf(0, n * width * height - 1);
   void *pbuf = (unsigned char *) buf;
   pr->GetSection ((unsigned char *)buf, jlo, ilo, width, height);

   switch (n)
      {
      case 1 :
         {
         // Now write it to val
         unsigned char *ptr = (unsigned char *)pbuf;

         if (band == 1)
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = *(ptr++);
            }
         else
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = 0;
            }

         break;
         }

      case 2 :
         {
         // Now write it to val
         unsigned short *ptr = (unsigned short *)pbuf;

         if (band == 1)
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = (*(ptr++))>>8;
            }
         else
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = 0;
            }

         break;
         }
      case 3 :
         {
         // 3 bytes per pixel, this a color image.
         // We make the average of the 3 colors.
         unsigned char *ptr = (unsigned char *)pbuf;
         unsigned char R;
         unsigned char G;
         unsigned char B;
         for (int i=ilo; i<=ihi; i++)
            for (int j=jlo; j<=jhi; j++)
               {
               R = *(ptr++);
               G = *(ptr++);
               B = *(ptr++);

               // Now write it to val
               val[i][j] = (band == 1) ? R : 
                           (band == 2) ? G : 
                           (band == 3) ? B : 0;
               }
         break;
         }

      default :
         {
         error_message (
            "fill_array_from_image_band : Must have <=2 bytes per pixel");
         error_message ("   Image \"%s\" has %d bytes per pixel", n);
         return 0;
         }
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int fill_array_from_image_band, (
   int band,         // The band to use
      Image_ref pr,
      unsigned short **val,      // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,    // Extent of the region
      int /*- fill = 0 -*/
      ))
   {
   // Try reading an image into an array

   // Exit on failure
   if (pr == NULL) return 0;

   // Get the dimensions of the part to be read
   int width = jhi - jlo + 1;
   int height = ihi - ilo + 1;

   // Fill up the buffer with the pixel values
   int n = pr->GetBytesPixel();
   Unsigned_char_vector buf(0, n * width * height - 1);
   void *pbuf = (unsigned char *) buf;
   pr->GetSection ((unsigned char *)buf, jlo, ilo, width, height);

   switch (n)
      {
      case 1 :
         {
         // Now write it to val
         unsigned char *ptr = (unsigned char *)pbuf;

         if (band == 1)
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = *(ptr++);
            }
         else
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = 0;
            }

         break;
         }

      case 2 :
         {
         // Now write it to val
         unsigned short *ptr = (unsigned short *)pbuf;

         if (band == 1)
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = *(ptr++);
            }
         else
            {
            for (int i=ilo; i<=ihi; i++)
               for (int j=jlo; j<=jhi; j++)
             val[i][j] = 0;
            }

         break;
         }

      case 3 :
         {
         // 3 bytes per pixel, this a color image.
         // We make the average of the 3 colors.
         unsigned char *ptr = (unsigned char *)pbuf;
         unsigned char R;
         unsigned char G;
         unsigned char B;
         for (int i=ilo; i<=ihi; i++)
            for (int j=jlo; j<=jhi; j++)
               {
               R = *(ptr++);
               G = *(ptr++);
               B = *(ptr++);

               // Now write it to val
               val[i][j] = (band == 1) ? R : 
                           (band == 2) ? G : 
                           (band == 3) ? B : 0;
               }
         break;
         }

      default :
         {
         error_message (
            "fill_array_from_image_band : Must have <=2 bytes per pixel");
         error_message ("   Image \"%s\" has %d bytes per pixel", n);
         return 0;
         }
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int fill_array_from_image_band, (
   int band,         // The band to use
   Image_ref pr,
   Unsigned_char_matrix &arr,
   int fill /*- = 0 -*/
   ))
   {
   // Fills an array from the same pixel locations in an image
   
   // If the array has not been dimensioned, then set it to the size of pr.
   if (arr.idim() == 0)
      arr.Init (0, pr->GetSizeY()-1, 0, pr->GetSizeX()-1);

   return fill_array_from_image_band (
         band,
         pr, 
         (unsigned char **)arr, 
         arr.ilow(), arr.ihigh(), 
         arr.jlow(), arr.jhigh(),
         fill
         );
   }

FUNCTION_DEF (int fill_array_from_image_band, (
   int band,         // The band to use
   Image_ref pr,
   Unsigned_short_matrix &arr,
   int fill /*- = 0 -*/
   ))
   {
   // Fills an array from the same pixel locations in an image

   // If the array has not been dimensioned, then set it to the size of pr.
   if (arr.idim() == 0)
      arr.Init (0, pr->GetSizeY()-1, 0, pr->GetSizeX()-1);

   return fill_array_from_image_band (
         band,
         pr, 
         (unsigned short **)arr, 
         arr.ilow(), arr.ihigh(), 
         arr.jlow(), arr.jhigh(),
         fill
         );
   }

//
// Now a bunch of routines for reading into already allocated arrays
//

FUNCTION_DEF (int read_pix_band_to_array, (
      int band,                  // The number of the band
      const char *infilename,    // Name of the input file
      unsigned char **val,       // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,          // Extent of the region
      int fill /*- = 0 -*/
      ))
   {
   // Read the pix image.  Array must already be allocated
   Image_ref pr = load_image (infilename);

   // Now fill from the image
   int result = fill_array_from_image_band (
      band, pr, val, ilo, ihi, jlo, jhi, fill);

   // If there was an error, identify the image
   if (! result)
      {
      error_message ("Error reading file \"s\"", infilename);
      return 0;
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned char **val,       // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill /*- = 0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated
   return read_pix_band_to_array (band, infilename, 
         val, 0, IDIM-1, 0, JDIM-1, fill);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short **val,      // Array to be filled
   int ilo, int ihi,
   int jlo, int jhi,          // Extent of the region
   int fill /*- = 0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated
   Image_ref pr = load_image (infilename);

   // Now fill from the image
   int result = fill_array_from_image_band (
      band, pr, val, ilo, ihi, jlo, jhi, fill);

   // If there was an error, identify the image
   if (! result)
      {
      error_message ("Error reading file \"s\"", infilename);
      return 0;
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short **val,      // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill /*- = 0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated
   return read_pix_band_to_array (band, infilename, val, 
         0, IDIM-1, 0, JDIM-1, fill);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   float **val,               // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   double fill /*- = 0.0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated

   // First, read into an unsigned char array
   Unsigned_short_matrix uu (0, IDIM-1, 0, JDIM-1);
   if (! read_pix_band_to_array (band, infilename, uu, IDIM, JDIM, (int)fill)) 
   return 0;

   // Now write it to val
   int i, j;
   for_2Dindex (i, j, uu)
      val[i][j] = (float) uu[i][j];

   // Return the success
   return (1);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   int **val,                 // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill /*- = 0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated

   // First, read into an unsigned char array
   Unsigned_short_matrix uu (0, IDIM-1, 0, JDIM-1);
   if (! read_pix_band_to_array (band, infilename, uu, IDIM, JDIM, fill)) 
   return 0;

   // Now write it to val
   int i, j;
   for_2Dindex (i, j, uu)
      val[i][j] = (int) uu[i][j];

   // Return the success
   return (1);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   double **val,              // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   double fill /*- = 0.0 -*/
   )  )
   {
   // Read the pix image.  Array must already be allocated

   // First, read into an unsigned char array
   Unsigned_short_matrix uu (0, IDIM-1, 0, JDIM-1);
   if (! read_pix_band_to_array (band, infilename, uu, IDIM, JDIM, (int)fill)) 
   return 0;

   // Now write it to val
   int i, j;
   for_2Dindex (i, j, uu)
      val[i][j] = (double) uu[i][j];

   // Return the success
   return (1);
   }

// Unassigned arrays -- read the whole image

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned char ***val,      // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  )
   {
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM, 0, *JDIM, unsigned char);

   // Now read the file 
   return read_pix_band_to_array (band, infilename, *val, *IDIM, *JDIM);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   float ***val,              // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  )
   {
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM, 0, *JDIM, float);

   // Now read the file 
   return read_pix_band_to_array (band, infilename, *val, *IDIM, *JDIM);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   int ***val,                // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  )
   {
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM, 0, *JDIM, int);

   // Now read the file 
   return read_pix_band_to_array (band, infilename, *val, *IDIM, *JDIM);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short ***val,     // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  )
   {
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM, 0, *JDIM, unsigned short);

   // Now read the file 
   return read_pix_band_to_array (band, infilename, 
         (unsigned short **) *val, *IDIM, *JDIM);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   double ***val,             // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   ))
   {
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM, 0, *JDIM, double);

   // Now read the file 
   return read_pix_band_to_array (band, infilename, *val, *IDIM, *JDIM);
   }

//
// Shells to the above routines using matrix classes
//

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Unsigned_char_matrix &M    // The array to be filled out
   ))
   {
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_band_to_array (band, infilename, 
         M.data(), M.idim(), M.jdim(), 0);
   }

FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                     // The number of the band
   const char *infilename,       // Name of the input file
   Unsigned_short_matrix &M      // The array to be filled out
   ))
   {
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_band_to_array (band, infilename, 
         M.data(), M.idim(), M.jdim(), 0);
   }
   
FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Float_matrix &M            // The array to be filled out
   ))
   {
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_band_to_array (band, infilename, 
   M.data(), M.idim(), M.jdim(), 0);
   }
   
FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Int_matrix &M              // The array to be filled out
   ))
   {
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_band_to_array (band, infilename, 
   M.data(), M.idim(), M.jdim(), 0);
   }
   
FUNCTION_DEF (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Double_matrix &M           // The array to be filled out
   ))
   {
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_band_to_array (band, infilename, 
   M.data(), M.idim(), M.jdim(), 0);
   }
   
// --------------------------------------------------------------
//
//    Writing arrays
//
// --------------------------------------------------------------

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   unsigned char **red, 
   unsigned char **green, 
   unsigned char **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   // Output an array

   //
   // BUG!!  This does not work properly with SunRasterImage type.
   //        NITF image is a bit suspicious as well.
   //        It works for TIFFImage.  All others are uncertain.
   //

   MemoryImage_ref pr;     // The image to be created 

   // Open a image to hold the value 
   if ((pr = convert_to_MemoryImage (red, green, blue, 
   ilow, ihigh, jlow, jhigh)) == 0)
      return (0);

   // Write it
   return put_image (pixfilename, pr.ptr());
   }

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   float **red, 
   float **green, 
   float **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   MemoryImage_ref pr;     // The image to be created 

   // Open a image to hold the value 
   if ((pr = convert_to_MemoryImage (red, green, blue, 
      ilow, ihigh, jlow, jhigh)) == 0)
      return (0);

   // Write it
   return put_image (pixfilename, pr.ptr());
   }

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   int **red, 
   int **green, 
   int **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   MemoryImage_ref pr;     // The image to be created 

   // Open a image to hold the value 
   if ((pr = convert_to_MemoryImage (red, green, blue, 
      ilow, ihigh, jlow, jhigh)) == 0)
      return (0);

   // Write it
   return put_image (pixfilename, pr.ptr());
   }

//
// Shells to the above routines using matrix classes
//

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   const Unsigned_char_matrix &red,
   const Unsigned_char_matrix &green,
   const Unsigned_char_matrix &blue
   ))
   {
   // Shell to more basic routine
   return put_array_to_pix (pixfilename, red.data(), green.data(), blue.data(),
   red.ilow(), red.ihigh(), red.jlow(), red.jhigh());
   }

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   const Float_matrix &red,
   const Float_matrix &green,
   const Float_matrix &blue
   ))
   {
   // Shell to more basic routine
   return put_array_to_pix (pixfilename, red.data(), green.data(), blue.data(),
   red.ilow(), red.ihigh(), red.jlow(), red.jhigh());
   }

FUNCTION_DEF (int put_array_to_pix, (
   const char *pixfilename, 
   const Int_matrix &red,
   const Int_matrix &green,
   const Int_matrix &blue
   ))
   {
   // Shell to more basic routine
   return put_array_to_pix (pixfilename, red.data(), green.data(), blue.data(),
   red.ilow(), red.ihigh(), red.jlow(), red.jhigh());
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   unsigned char **red, 
   unsigned char **green, 
   unsigned char **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   // Convert an unsigned char array to a memory image

   // Get the dimensions
   int IDIM = ihigh - ilow + 1;
   int JDIM = jhigh - jlow + 1;

   // Open a color image to hold the value
   ImageTemplate it(JDIM, IDIM, 24, 0, 0);
   it.SetImageClass ('C');
   MemoryImage_ref pr = new MemoryImage(&it);

   // Set the blocksize for the Memory Image
      {
      const int BlockSizeX = 128;
      const int BlockSizeY =  64;
      pr->SetBlockSizeX(BlockSizeX);
      pr->SetBlockSizeY(BlockSizeY);
      pr->SetSizeXBlocks((JDIM + BlockSizeX - 1) / BlockSizeX);
      pr->SetSizeYBlocks((IDIM + BlockSizeY - 1) / BlockSizeY);

      if (strcmp (tj_defaultImageType(), "NITFv20Image") == 0)
         pr->SetDescription("RGB");
      }

   // Make a buffer of the right size
   Unsigned_char_vector buf (0, 3*IDIM*JDIM-1);

   // Pack them
   int count=0;
   for (int i=ilow; i<=ihigh; i++)
      for (int j=jlow; j<=jhigh; j++)
    {
    buf[count++] = red[i][j];
    buf[count++] = green[i][j];
    buf[count++] = blue[i][j];
    }

   // Now put the values 
   pr->PutSection(buf, 0, 0, JDIM, IDIM);

   // Else, return success.
   return (pr);
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   int **red, 
   int **green, 
   int **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   // Convert int arrays to a memory image
   
   // Make temporary Unsigned_char_matrix arrays
   Unsigned_char_matrix bred   (ilow, ihigh, jlow, jhigh);
   Unsigned_char_matrix bgreen (ilow, ihigh, jlow, jhigh);
   Unsigned_char_matrix bblue  (ilow, ihigh, jlow, jhigh);

   // Now fill them
   int i, j;
   for_2Dindex (i, j, bred)
      {
      bred  [i][j] = red  [i][j];
      bgreen[i][j] = green[i][j];
      bblue [i][j] = blue [i][j];
      }

   // Now call the other routine
   return convert_to_MemoryImage (bred, bgreen, bblue, 
   ilow, ihigh, jlow, jhigh);
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   float **red, 
   float **green, 
   float **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ))
   {
   // Convert int arrays to a memory image
   
   // Make temporary Unsigned_char_matrix arrays
   Unsigned_char_matrix bred   (ilow, ihigh, jlow, jhigh);
   Unsigned_char_matrix bgreen (ilow, ihigh, jlow, jhigh);
   Unsigned_char_matrix bblue  (ilow, ihigh, jlow, jhigh);

   // Now fill them
   int i, j;
   for_2Dindex (i, j, bred)
      {
      bred  [i][j] = nint(red  [i][j]);
      bgreen[i][j] = nint(green[i][j]);
      bblue [i][j] = nint(blue [i][j]);
      }

   // Now call the other routine
   return convert_to_MemoryImage (bred, bgreen, bblue, 
   ilow, ihigh, jlow, jhigh);
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   Unsigned_char_matrix &red,
   Unsigned_char_matrix &green,
   Unsigned_char_matrix &blue
   ))
   {
   return convert_to_MemoryImage (red, green, blue, 
   red.ilow(), red.ihigh(), 
   red.jlow(), red.jhigh());
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   Int_matrix &red,
   Int_matrix &green,
   Int_matrix &blue
   ))
   {
   return convert_to_MemoryImage (red, green, blue, 
   red.ilow(), red.ihigh(), 
   red.jlow(), red.jhigh());
   }

FUNCTION_DEF (MemoryImage_ref convert_to_MemoryImage, (
   Float_matrix &red,
   Float_matrix &green,
   Float_matrix &blue
   ))
   {
   return convert_to_MemoryImage (red, green, blue, 
   red.ilow(), red.ihigh(), 
   red.jlow(), red.jhigh());
   }
