#include "Utilities/cvar.h"

/* SFILE_BEGIN */
#include <Utilities_CC/utilities_CC.h>
#include <PNG/pngwritefile_dcl.h>
#include <PNG/pngwritefile_16bit_dcl.h>
/* SFILE_END */

/* write a png file */
FUNCTION_DEF (int write_png, (
	const char *file_name, 
	Unsigned_char_matrix R,
	Unsigned_char_matrix G,
	Unsigned_char_matrix B
	))
   {
   return write_png_image (file_name, R, G, B);
   }

FUNCTION_DEF (int write_png, (
	const char *file_name, 
	Unsigned_char_matrix im
	))
   {
   return write_png_image (file_name, im);
   }

FUNCTION_DEF (int write_png, (
	const char *file_name, 
	Unsigned_short_matrix R,
	Unsigned_short_matrix G,
	Unsigned_short_matrix B
	))
   {
   return write_png_image (file_name, R, G, B);
   }

FUNCTION_DEF (int write_png, (
	const char *file_name, 
	Unsigned_short_matrix im
	))
   {
   return write_png_image (file_name, im);
   }



