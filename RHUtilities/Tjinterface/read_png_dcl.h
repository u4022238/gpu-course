#ifndef _read_png_cc_dcl_
#define _read_png_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>
// #include <Tjinterface/imageio_dcl.h>
// #include <Tjinterface/imageio_color_dcl.h>
#include "PNG/png.h"
#include "PNG/pngreadfile_dcl.h"
/* */

FUNCTION_DECL (bool read_png, (Unsigned_short_matrix &im16, const char *fname));

FUNCTION_DECL (bool read_png, (Unsigned_char_matrix &im8, const char *fname));

#endif
