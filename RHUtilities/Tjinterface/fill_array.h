
FUNCTION_DECL (int fill_array_from_image, (
   	Image_ref pr,
   	fa_datatype **val,		// Array to be filled
   	int ilo, int ihi,
   	int jlo, int jhi,		// Extent of the region
   	int fill = 0
   	));

FUNCTION_DECL (int fill_array_from_image, (
	Image_ref pr,
	fa_matrix_type &arr,
	int fill = 0
	));

FUNCTION_DECL (int read_pix_to_array, (
   const char *infilename,		// Name of the input file
   fa_datatype **val,			// Array to be filled
   int ilo, int ihi,
   int jlo, int jhi,			// Extent of the region
   int fill = 0
   ));

FUNCTION_DECL (int read_pix_to_array, (
   const char *infilename,		// Name of the input file
   fa_datatype **val,			// Array to be filled
   int IDIM, int JDIM,			// Size of the array.
   int fill = 0
   ));

FUNCTION_DECL (int read_pix_to_array, (
   const char *infilename,		// Name of the input file
   fa_datatype ***val,			// Array to be filled
   int *IDIM, int *JDIM			// Size of the array.
   )	);

FUNCTION_DECL (int read_pix_to_array, (
   const char *infilename, 		// Name of the input file
   fa_matrix_type &M			// The array to be filled out
   ));

 
FUNCTION_DECL (fa_matrix_type fa_cast_routine, (
  	Image_ref pr
	));

FUNCTION_DECL (Unsigned_char_matrix scale_to_output, (
	const fa_matrix_type &A, 
	int nvalues = 256));

FUNCTION_DECL (Int_matrix scale_to_int, (
	const fa_matrix_type &A, 
	int nvalues));
