#ifndef _imageio_color_cc_dcl_
#define _imageio_color_cc_dcl_
#include "Utilities_light/cvar.h"


/* */
#include <Utilities_CC_light/utilities_CC.h>
// #include <ImageClasses/Image.h>
//#include <ImageClasses/MemoryImage.h>
//#include <ImageClasses/BandedImage.h>
/* */

FUNCTION_DECL (rhBOOLEAN is_color_image, (
      Image_ref pr
      ));

FUNCTION_DECL (rhBOOLEAN is_color_image, (
      const char *infilename
      ));

FUNCTION_DECL (int fill_array_from_image_band, (
      int band,               // The band to use
      Image_ref pr,
      unsigned char **val,    // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,       // Extent of the region
      int  fill = 0 
      ));

FUNCTION_DECL (int fill_array_from_image_band, (
   int band,         // The band to use
      Image_ref pr,
      unsigned short **val,      // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,    // Extent of the region
      int  fill = 0 
      ));

FUNCTION_DECL (int fill_array_from_image_band, (
   int band,         // The band to use
   Image_ref pr,
   Unsigned_char_matrix &arr,
   int fill  = 0 
   ));

FUNCTION_DECL (int fill_array_from_image_band, (
   int band,         // The band to use
   Image_ref pr,
   Unsigned_short_matrix &arr,
   int fill  = 0 
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
      int band,                  // The number of the band
      const char *infilename,    // Name of the input file
      unsigned char **val,       // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,          // Extent of the region
      int fill  = 0 
      ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned char **val,       // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill  = 0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short **val,      // Array to be filled
   int ilo, int ihi,
   int jlo, int jhi,          // Extent of the region
   int fill  = 0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short **val,      // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill  = 0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   float **val,               // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   double fill  = 0.0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   int **val,                 // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   int fill  = 0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   double **val,              // Array to be filled
   int IDIM, int JDIM,        // Size of the array.
   double fill  = 0.0 
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned char ***val,      // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   float ***val,              // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   int ***val,                // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   unsigned short ***val,     // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  );

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   double ***val,             // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Unsigned_char_matrix &M    // The array to be filled out
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                     // The number of the band
   const char *infilename,       // Name of the input file
   Unsigned_short_matrix &M      // The array to be filled out
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Float_matrix &M            // The array to be filled out
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Int_matrix &M              // The array to be filled out
   ));

FUNCTION_DECL (int read_pix_band_to_array, (
   int band,                  // The number of the band
   const char *infilename,    // Name of the input file
   Double_matrix &M           // The array to be filled out
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   unsigned char **red, 
   unsigned char **green, 
   unsigned char **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   float **red, 
   float **green, 
   float **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   int **red, 
   int **green, 
   int **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   const Unsigned_char_matrix &red,
   const Unsigned_char_matrix &green,
   const Unsigned_char_matrix &blue
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   const Float_matrix &red,
   const Float_matrix &green,
   const Float_matrix &blue
   ));

FUNCTION_DECL (int put_array_to_pix, (
   const char *pixfilename, 
   const Int_matrix &red,
   const Int_matrix &green,
   const Int_matrix &blue
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   unsigned char **red, 
   unsigned char **green, 
   unsigned char **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   int **red, 
   int **green, 
   int **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   float **red, 
   float **green, 
   float **blue, 
   int ilow, int ihigh, 
   int jlow, int jhigh
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   Unsigned_char_matrix &red,
   Unsigned_char_matrix &green,
   Unsigned_char_matrix &blue
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   Int_matrix &red,
   Int_matrix &green,
   Int_matrix &blue
   ));

FUNCTION_DECL (MemoryImage_ref convert_to_MemoryImage, (
   Float_matrix &red,
   Float_matrix &green,
   Float_matrix &blue
   ));

#endif
