// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


#include "Utilities_CC/utilities_CC.h"
#include "imageio_dcl.h"

int main (int argc, char *argv[])
   {
   // Test the parameter
   if (argc != 3) 
      {
      error_message ("Usage : testio infile outfile");
      exit (1);
      }

   // Get the file names
   char *infname = argv[1];
   char *outfname = argv[2];

   // Set the output image type
   tj_interface_set_output_image_type ("BMPImage");
   const char* extension = ".bmp";

   // Get an image
   Unsigned_char_matrix xin;
   int result = read_pix_to_array (infname, xin);

   if (! result)
      {
      error_message ("Failed to read image \"%s\"", infname);
      exit (1);
      }

   // Scale to output
   Unsigned_char_matrix scaled = scale_to_output (xin);

   if (! put_array_to_pix (outfname, scaled))
      {
      error_message ("Failed to write image \"%s\"", outfname);
      exit (1);
      }
   
   return 0;
   }
