#ifndef _write_png_cc_dcl_
#define _write_png_cc_dcl_
#include "Utilities_light/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>
#include <PNG/pngwritefile_dcl.h>
#include <PNG/pngwritefile_16bit_dcl.h>
/* */

FUNCTION_DECL (int write_png, (
	const char *file_name, 
	Unsigned_char_matrix R,
	Unsigned_char_matrix G,
	Unsigned_char_matrix B
	));

FUNCTION_DECL (int write_png, (
	const char *file_name, 
	Unsigned_char_matrix im
	));

FUNCTION_DECL (int write_png, (
	const char *file_name, 
	Unsigned_short_matrix R,
	Unsigned_short_matrix G,
	Unsigned_short_matrix B
	));

FUNCTION_DECL (int write_png, (
	const char *file_name, 
	Unsigned_short_matrix im
	));

#endif
