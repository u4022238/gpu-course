// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1998 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
#ifndef _TjInterface_x_h_
#define _TjInterface_x_h_
//-----------------------------------------------------------------------------
//
// .NAME Tjinterface - A bunch of statics for transforming between the
//       RH and the TJ world
// .LIBRARY Tjinterface
// .HEADER RHUtilities
// .INCLUDE TjInterface/Tjinterface.h
// .FILE Tjinterface.h
// .FILE Tjinterface.cc
//
// .SECTION Description
//   Provides a number of methods for translating between rhMatrices, and
//   such things and the corresponding TargetJr objects
//
// .SECTION Author
//   Richard Hartley
//
// .SECTION Modifications
//   <none yet>
//
//-----------------------------------------------------------------------------

#include <Utilities_CC/Matrix.h>
#include <IUCameras/MatrixIUCamera_ref.h>
#include <math/matrix.h>

class Tjinterface
{
   public :

  	static MatrixIUCamera_ref to_MatrixIUCamera_ref (rhMatrix Mi);
	static rhMatrix to_rhMatrix (IUE_matrix<double> mat);
};



#endif
