
FUNCTION_DEF (int fill_array_from_image, (
      Image_ref pr,
      fa_datatype **val,      // Array to be filled
      int ilo, int ihi,
      int jlo, int jhi,    // Extent of the region
      int /*- fill = 0 -*/
      ))
   {
   // Try reading an image into an array

   // Exit on failure
   if (pr == NULL) return 0;

   // Get the dimensions of the part to be read
   int width = jhi - jlo + 1;
   int height = ihi - ilo + 1;

   // Get the parameters of the image
   int n = pr->GetBytesPixel();
   char image_format = pr->GetFormat();
   char image_class = pr->GetImageClass();
   
   // Fill up the buffer with the pixel values
   Unsigned_char_vector buf(0, n * width * height - 1);
   void *pbuf = (unsigned char *) buf;
   pr->GetSection (pbuf, jlo, ilo, width, height);

   // Monochrome images
   if (image_class == 'M')
      {
      // Integer type images
      if (image_format == 'L' || image_format == 'U')
         {
         // Fixed point type images

         switch (n)
            {
            case 1 :
               {
               // Now write it to val
               unsigned char *ptr = (unsigned char *)pbuf;
               for (int i=ilo; i<=ihi; i++)
                  for (int j=jlo; j<=jhi; j++)
                val[i][j] = (fa_datatype)(*(ptr++));
      
               break;
               }
      
            case 2 :
               {
               // Now write it to val
               unsigned short *ptr = (unsigned short *)pbuf;
               for (int i=ilo; i<=ihi; i++)
                  for (int j=jlo; j<=jhi; j++)
                val[i][j] = (fa_datatype)(*(ptr++));
   
               break;
               }
   
            case 4 :
               {
               // Now write it to val
               int *ptr = (int *)pbuf;
               for (int i=ilo; i<=ihi; i++)
                  for (int j=jlo; j<=jhi; j++)
                val[i][j] = (fa_datatype)(*(ptr++));
   
               break;
               }

            default :
               {
               error_message (
                     "fill_array_from_image : Wrong # bytes per pixel");
               error_message ("   Integer type must have 1,2 or 4 bytes");
               error_message ("   Image \"%s\" has %d bytes per pixel", 
                  pr->GetName(), n);
                     return 0;
               }
            }
         }

      else if (image_format == 'A')
         {
         // Floating point images
         switch (n)
            {
            case 4 :
               {
               // Now write it to val
               float *ptr = (float *)pbuf;
               for (int i=ilo; i<=ihi; i++)
                  for (int j=jlo; j<=jhi; j++)
                val[i][j] = (fa_datatype)(*(ptr++));
   
               break;
               }
   
            case 8 :
               {
               // Now write it to val
               double *ptr = (double *)pbuf;
               for (int i=ilo; i<=ihi; i++)
                  for (int j=jlo; j<=jhi; j++)
                val[i][j] = (fa_datatype)(*(ptr++));
   
               break;
               }

            default :
               {
               error_message (
               "fill_array_from_image : Wrong # bytes per pixel");
               error_message ("  Floating point type must have 1,2 or 4 bytes");
               error_message ("  Image \"%s\" has %d bytes per pixel", 
                  pr->GetName(), n);
               return 0;
               }
            }
         }

      else if (image_format == 'Y')
         {
         error_message ("fill_array_from_image : Complex images not supported");
         return 0;
         }
      }

   // Color images
   else if (image_class == 'C')
      {

      // Integer type images
      if (image_format == 'L' || image_format == 'U')
         {
         switch (n)
            {
            case 3 :
               {
               // 3 bytes per pixel, this an image color.
               // We make the average of the 3 colors.
               unsigned char *ptr = (unsigned char *)pbuf;
               unsigned char R;
               unsigned char G;
               unsigned char B;
               for (int i=ilo; i<=ihi; i++)
                 for (int j=jlo; j<=jhi; j++)
                   {
                     R = *(ptr++);
                     G = *(ptr++);
                     B = *(ptr++);
                     // Now write it to val
                     val[i][j] = (fa_datatype )((R+G+B)/3);
                   }
               break;
               }

            case 6 :
               {
               // 6 bytes per pixel, this an image color.
               // We make the average of the 3 colors.
               unsigned short *ptr = (unsigned short *)pbuf;
               unsigned short R;
               unsigned short G;
               unsigned short B;
               for (int i=ilo; i<=ihi; i++)
                 for (int j=jlo; j<=jhi; j++)
                   {
                     R = *(ptr++);
                     G = *(ptr++);
                     B = *(ptr++);
   
                     // Now write it to val
                     val[i][j] = (fa_datatype)((R+G+B)/3);
                   }
               break;
               }

            default :
               {
               error_message (
                 "fill_array_from_image : Color image wrong # bytes per pixel");
               error_message ("   Must have 3 or 6 bytes per pixel");
               error_message ("   Image \"%s\" has %d bytes per pixel", 
                  pr->GetName(), n);
               return 0;
               }
            }
         }

      else if (image_format == 'A' || image_format == 'Y')
         {
         error_message (
            "fill_array_from_image : Floating point color not supported");
         return 0;
         }
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int fill_array_from_image, (
   Image_ref pr,
   fa_matrix_type &arr,
   int fill /*- = 0 -*/
   ))
   {
   // Fills an array from the same pixel locations in an image

   // Put out a warning
   if ( arr.ilow() == 0 && arr.ihigh() == -1 && 
   arr.jlow() == 0 && arr.jhigh() == -1)
      {
      warning_message (
   "fill_array_from_image :: uninitialized array");
      warning_message (
   "  -- maybe you should be calling Image_to_xx_matrix");
      return 1;
      }

   return fill_array_from_image (
         pr, 
         (fa_datatype **)arr, 
         arr.ilow(), arr.ihigh(), 
         arr.jlow(), arr.jhigh(),
         fill
         );
   }

//=============================================================
//
// Now a bunch of routines for reading into already allocated arrays
//

FUNCTION_DEF (int read_pix_to_array, (
   const char *infilename, // Name of the input file
   fa_datatype **val,      // Array to be filled
   int ilo, int ihi,
   int jlo, int jhi,    // Extent of the region
   int fill /*- = 0 -*/
   )  )
   {
   // Read section of an image
   // Read the pix image.  Array must already be allocated
   Image_ref pr = load_image (infilename);

   // Now fill from the image
   int result = fill_array_from_image (pr, val, ilo, ihi, jlo, jhi, fill);

   // If there was an error, identify the image
   if (! result)
      {
      error_message ("Error reading file \"s\"", infilename);
      return 0;
      }

   // Return the success
   return (1);
   }

FUNCTION_DEF (int read_pix_to_array, (
   const char *infilename, // Name of the input file
   fa_datatype **val,      // Array to be filled
   int IDIM, int JDIM,     // Size of the array.
   int fill /*- = 0 -*/
   )  )
   {
   // Read only the part of an image specified by IDIM, JDIM
   // Read the pix image.  Array must already be allocated
   return read_pix_to_array (infilename, val, 0, IDIM-1, 0, JDIM-1, fill);
   }

FUNCTION_DEF (int read_pix_to_array, (
   const char *infilename,    // Name of the input file
   fa_datatype ***val,        // Array to be filled
   int *IDIM, int *JDIM       // Size of the array.
   )  )
   {
   // Read the whole image, returning data and image size
   // Read the image, allocate arrays
   
   // First, get the image size
   if (! get_image_size (infilename, IDIM, JDIM)) return 0;

   // Allocate space for the arrays 
   *val = MATRIX (0, *IDIM-1, 0, *JDIM-1, fa_datatype);

   // Now read the file 
   return read_pix_to_array (infilename, *val, *IDIM, *JDIM);
   }

FUNCTION_DEF (int read_pix_to_array, (
   const char *infilename,       // Name of the input file
   fa_matrix_type &M             // The array to be filled out
   ))
   {
   // Read into a new style array
   // Reads a file into an array

   // First, get the size of the image
   int idim, jdim;
   if (! get_image_size (infilename, &idim, &jdim))
      return 0;

   // Allocate the array
   M.Init(0, idim-1, 0, jdim-1);

   // Now, read in the data
   return read_pix_to_array (infilename, M.data(), M.idim(), M.jdim(), 0);
   }
 
FUNCTION_DEF (fa_matrix_type fa_cast_routine, (
   Image_ref pr
   ))
   {
   // We need to get the size of the array
   int jdim = pr->GetSizeX();
   int idim = pr->GetSizeY();

   // Now allocate space
   fa_matrix_type arr (0, idim-1, 0, jdim-1);

   // Call fill
   fill_array_from_image (pr, arr);

   // Now return it
   return arr;
   }

FUNCTION_DEF (Unsigned_char_matrix scale_to_output, (
   const fa_matrix_type &A, 
   int nvalues /*- = 256 -*/))
   {
   // Scales an unsigned char matrix ready to output 
   int i, j;
   Unsigned_char_matrix B (A.bounds());

   // Now, get the maximum and minimum pixels of the input image
   double maxval = A[A.ilow()][A.jlow()];
   double minval = maxval;

   // Find the minimum and maximum values
   for_2Dindex (i, j, A)
      {
      if (A[i][j] > maxval) maxval = A[i][j];
      if (A[i][j] < minval) minval = A[i][j];
      }

   // Now, find the scale and offset
   double scale = (nvalues-0.1)/(maxval-minval);

   // fprintf (stderr, "Max val = %e, Min val = %e\n", maxval, minval);
   // fprintf (stderr, "scale = %e\n", scale);

   // Now transfer the values
   for_2Dindex (i, j, A)
      B[i][j] = nint((A[i][j] - minval)*scale - 0.45);

   // Return the matrix B
   return B;
   }

FUNCTION_DEF (Int_matrix scale_to_int, (
   const fa_matrix_type &A, 
   int nvalues))
   {
   // Makes an unsigned char matrix ready to output 
   int i, j;
   Int_matrix B (A.bounds());

   // Now, get the maximum and minimum pixels of the input image
   float maxval = A[A.ilow()][A.jlow()];
   float minval = maxval;

   // Find the minimum and maximum values
   for_2Dindex (i, j, A)
      {
      if (A[i][j] > maxval) maxval = A[i][j];
      if (A[i][j] < minval) minval = A[i][j];
      }

   // Now, find the scale and offset
   double scale = (nvalues-0.1)/(maxval-minval);

   // Now transfer the values
   for_2Dindex (i, j, A)
      B[i][j] = nint((A[i][j] - minval)*scale - 0.45);

   // Return the matrix B
   return B;
   }

