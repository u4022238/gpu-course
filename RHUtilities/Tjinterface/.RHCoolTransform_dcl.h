#ifndef _RHCoolTransform_cc_dcl_
#define _RHCoolTransform_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>
#include <cool/Transform.h>
/* */

FUNCTION_DECL (CoolTransform ToCoolTransform3D, (rhMatrix &M));

FUNCTION_DECL (CoolTransform ToCoolTransform2D, (rhMatrix &M));

#endif
