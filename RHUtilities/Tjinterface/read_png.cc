/* SFILE_BEGIN */
#include <Utilities_CC/utilities_CC.h>
// #include <Tjinterface/imageio_dcl.h>
// #include <Tjinterface/imageio_color_dcl.h>
#include "PNG/png.h"
#include "PNG/pngreadfile_dcl.h"
/* SFILE_END */

FUNCTION_DEF (bool read_png, (Unsigned_short_matrix &im16, const char *fname))
   {
   // Reads a file, expecting a short format
   FILE *infile = FOPEN (fname, "rb");
   if (!infile)
      {
      error_message ("read_png: cannot open file \"%s\" for read", fname);
      return false;
      }

   // Now, read the image
   Unsigned_char_matrix bytes;
   int height, width, bit_depth, colour_type, interlace_type;
   int result = read_png_image (infile, bytes,
         &height, &width, &bit_depth, &colour_type, &interlace_type);

   if (!result)
      {
      error_message ("Error reading file \"%s\"", fname);
      return false;
      }

   // Tell about the image
   informative_message (
         "Read file \"%s\": dimensions (%d x %d x %d): size %d x %d", 
         fname, height, width, bit_depth, bytes.idim(), bytes.jdim() );
   informative_message ("     colour_type %d, interlace_type %d",
         colour_type, interlace_type);

   // Now, if bit-depth is 16, then create an unsigned short image
   if (bit_depth == 16 && width * 2 == bytes.jdim())
      {
      // Native 16-bit image
      im16.Init (0, height-1, 0, width-1);
      for2Dindex (i, j, im16)
         {
         unsigned char vhigh = bytes[i][2*j];
         unsigned char vlow  = bytes[i][2*j+1];
         im16[i][j] = (vhigh << 8) + vlow;
         }
      // memcpy (&(im16[0][0]), &(bytes[0][0]), bytes.idim() * bytes.jdim());
      }
   else if (bit_depth == 8 && width == bytes.jdim())
      {
      // Change format
      im16.Init (0, height-1, 0, width-1);
      for2Dindex (i, j, im16)
         im16[i][j] = bytes[i][j];
      }
   else if (bit_depth == 8 && 3*width == bytes.jdim())
      {
      // In this case, the data has three bands, but we only want one
      // Return the sum of the bands
      im16.Init(0, height-1, 0, width-1);

      informative_message ("read_png: created file of size (%d x %d)\n",
            im16.idim(), im16.jdim());

      for2Dindex (i, j, im16)
         im16[i][j] = (bytes[i][3*j] + bytes[i][3*j+1] + bytes[i][3*j+2])/3;
      }
   else
      {
      error_message ("read_png: File \"%s\" is not an 8 or 16-bit image\n",
            fname);
      error_message (
            "  bit_depth = %d, width = %d, colour = %d, interlace = %d",
            bit_depth, width, colour_type, interlace_type);
      return false;
      }

   // Close the file and return
   fclose (infile);
   return true;
   }

FUNCTION_DEF (bool read_png, (Unsigned_char_matrix &im8, const char *fname))
   {
   // Reads a file, expecting a short format
   FILE *infile = fopen (fname, "rb");
   if (!infile)
      {
      error_message ("read_png: cannot open file \"%s\" for read", fname);
      return false;
      }

   // Now, read the image
   Unsigned_char_matrix bytes;
   int height, width, bit_depth, colour_type, interlace_type;
   int result = read_png_image (infile, bytes,
         &height, &width, &bit_depth, &colour_type, &interlace_type);

   if (!result)
      {
      error_message ("Error reading file \"%s\"", fname);
      return false;
      }

   // Tell about the image
   informative_message (
         "Read file \"%s\": dimensions (%d x %d x %d): size %d x %d", 
         fname, height, width, bit_depth, bytes.idim(), bytes.jdim() );
   informative_message ("     colour_type %d, interlace_type %d",
         colour_type, interlace_type);

   // Reformat or not, according to the need
   if (bit_depth == 8 && width == bytes.jdim())
      {
      im8 = bytes;
      }
   else if (bit_depth == 8 && 3*width == bytes.jdim())
      {
      // In this case, the data has three bands, but we only want one
      // Return the sum of the bands
      im8.Init(0, height-1, 0, width-1);

      informative_message ("read_png: created file of size (%d x %d)\n",
            im8.idim(), im8.jdim());

      for2Dindex (i, j, im8)
         im8[i][j] = (bytes[i][3*j] + bytes[i][3*j+1] + bytes[i][3*j+2])/3;
      }
   else
      {
      error_message ("read_png_image: File \"%s\" is not an 8-bit image\n",
            fname);
      return false;
      }

   // Close the file and return
   fclose (infile);
   return true;
   }
