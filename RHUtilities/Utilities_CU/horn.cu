/* @(#)horn.c	1.7 07 Nov 1994 */

/*
 * Implements Horn's algorithm for mapping point sets to point sets
 * by Euclidean transformations
 *
 * See the paper Closed Form Solution of absolute orientation using unit 
 * quaternions. by Berthold Horn, J. Opt Soc. Am. A, Vol 4, No 4, April 1987.
 */

#include "Utilities_CU/utilities_CC.h"
#include <Utilities_cuda/quaternion_dcl.h>
 
FUNCTION_DEF ( void translate_points_rel_centroid, ( rhMatrix &X, rhVector &cent)
   )
   {
   /* Translate each set of points relative to their centroid */

   // Sum down the columns
   rhVector t = X.columnsum();

   /* Average the displacement */
   t /= X.idim();

   /* Now subtract the centroid */
   for2Dindex (i, j, X) X[i][j] -= t[j];

   /* Return the centroid */
   cent = t;
   }

FUNCTION_DEF ( rhMatrix horn_rotation, ( const rhMatrix &X1, const rhMatrix &X2))
   {
   /* Finds the best rotation that maps first points to the second */

   rhMatrix M = X1.transpose() * X2;

   /* Next form the N matrix */

   /* Diagonal entries first */
   rhMatrix N(4, 4);
   N[0][0] =  M[0][0] + M[1][1] + M[2][2];
   N[1][1] =  M[0][0] - M[1][1] - M[2][2];
   N[2][2] = -M[0][0] + M[1][1] - M[2][2];
   N[3][3] = -M[0][0] - M[1][1] + M[2][2];

   /* Now off-diagonal entries */
   N[0][1] = N[1][0] = M[1][2] - M[2][1];
   N[0][2] = N[2][0] = M[2][0] - M[0][2];
   N[0][3] = N[3][0] = M[0][1] - M[1][0];
   N[1][2] = N[2][1] = M[0][1] + M[1][0];
   N[1][3] = N[3][1] = M[0][2] + M[2][0];
   N[2][3] = N[3][2] = M[1][2] + M[2][1];

   /* Now we can find the maximum eigenvector of this matrix, and that is
    * the rotation */
   /* Do this using the method of Jacobi */

   /* Declare the necessary arrays */
   rhMatrix V(4, 4);    // Eigenvectors
   rhVector D(4);	      // Eigenvalues

   /* Find the eigenvalues and vectors in descending order */
   jacobi_0 (N, 4, D, V);

   /* The first column of V is the required eigenvector = rotation */
   rhVector q = V.column(0);

   /* Now translate this into a rotation matrix */
   rhMatrix R(3, 3);
   quaternion_to_matrix (q, R);

   return R;
   }

FUNCTION_DEF ( rhMatrix horn_weighted, ( 
         const rhMatrix &X1in, const rhMatrix &X2in,
         rhVector &weights,
         bool scale_or_not /*- = true -*/
         ))
   {
   /* Weighted version of Horn's algorithm */
   int numpoints = X1in.idim();
   rhMatrix X1 = X1in.copy();
   rhMatrix X2 = X2in.copy();

   for (int i=0; i<numpoints; i++)
      {
      double w = sqrt(weights[i]);
      if (w == 1.0) continue;
      for (int j=0; j<X1.jdim(); j++)
         {
         X1[i][j] = w * X1in[i][j];
         X2[i][j] *= w;
         }
      }

   // Now, call the unweighted version
   if (scale_or_not)
      return horn (X1, X2);
   else
      return horn_noscale (X1, X2);
   }

FUNCTION_DEF ( rhMatrix huber_horn, ( 
         const rhMatrix &X1in, const rhMatrix &X2in, 
         double threshold, 
         bool scale_or_not /*- = true -*/))
   {
   // Runs Horn's algorithm with Huber weighting

   // First of all, check that the dimensions are equal
   if (X1in.idim() != X2in.idim())
      {
      error_message ("huber_horn:  Different numbers of points");
      bail_out (1);
      }

   printf ("Horn:A"); fflush(stdout);

   // Now define some weights
   int npoints = X1in.idim();
   rhVector weight (npoints);
   weight.clear(1.0);

   printf ("B"); fflush(stdout);
   
   // Several iterations
   rhMatrix T;       // The result to be returned

   // Now, run repetitions
   const int MaxReps = 7;
   for (int rep=0; rep<MaxReps; rep++)
      {
      printf ("C"); fflush(stdout);
      
      // Run weighted Huber
      T = horn_weighted (X1in, X2in, weight, scale_or_not);

      printf ("D"); fflush(stdout);

      // Huber reweighting
      for (int i=0; i<npoints; i++)
         {
         // Compute the error for this measurement
         rhVector error = T * X1in.row(i).hom() - X2in.row(i).hom();
         double eps = sqrt(error*error);
         if (eps > threshold) 
            weight[i] = threshold / eps;
         else
            weight[i] = 1.0;
         }

      printf ("E"); fflush(stdout);
      }

   printf ("F\n"); fflush(stdout);

   // The robust solution returned
   return T;
   }

FUNCTION_DEF ( rhMatrix horn, ( const rhMatrix &X1in, const rhMatrix &X2in))
   {

   /* Finds the best transformation that maps first points to the second */
   int numpoints = X1in.idim();

   /* Allocate points */
   rhMatrix X1 = X1in.copy();
   rhMatrix X2 = X2in.copy();

   /* Translate each set of points relative to their centroid */
   rhVector cent1, cent2;
   translate_points_rel_centroid (X1, cent1);
   translate_points_rel_centroid (X2, cent2);

   /* Now find the correct rotation */
   rhMatrix R = horn_rotation (X1, X2);

   // Now determine the scale
   double scale;

#ifdef SYMMETRIC

      // Scale is the quotient of the magnitudes
      scale = sqrt(X2.sumsq() / X1.sumsq());

#else

   /* Next compute the scale - choose the asymmetric method */
      {
      // Rotate the points X2
      rhMatrix X2R = X2 * R;

      // This carries out a scaled Frobenius inner product
      double numerator = 0.0;
	  double denominator = 0.0;
      for2Dindex (i, j, X1)
         {
         numerator += X2R[i][j] * X1[i][j];
         denominator += X1[i][j] * X1[i][j];
         }

      /* Now  we can compute the scale using Horn's formula */
      scale = numerator / denominator;
      }

#endif

   // Now put it all together
   R *= scale;
   rhMatrix T = (R | (cent2 - R*cent1)) & rhVector (0.0, 0.0, 0.0, 1.0);
   return T;
   }

FUNCTION_DEF ( rhMatrix horn_noscale, (
         const rhMatrix &X1in, const rhMatrix &X2in
         ))
   {
   // Finds the best transformation that maps second points to the first
   // without scaling (i.e. unit scale) 

   // Take a copy of the inputs
   rhMatrix  X1 = X1in.copy();
   rhMatrix  X2 = X2in.copy();

   /* Translate each set of points relative to their centroid */
   rhVector cent1, cent2;
   translate_points_rel_centroid (X1, cent1);
   translate_points_rel_centroid (X2, cent2);

   /* Now find the correct rotation */
   rhMatrix R = horn_rotation (X1, X2);

   // Now put the matrices together
   rhMatrix T = (R | (cent2 - R*cent1)) & rhVector(0.0, 0.0, 0.0, 1.0);
   return T;
   }

FUNCTION_DEF ( double test_horn_transform, (
   FILE *outfile, const rhMatrix &X1, const rhMatrix &X2, const rhMatrix M))
   {
   /* Tests how good the given transformation is */

   /* Test to see if the results are the same */
   int numpoints = X1.idim();
   double errorsumsq = 0.0;
   for (int i=0; i<numpoints; i++)
      {
      rhVector x1 = X1.row(i);
      rhVector x2 = X2.row(i);
      rhVector x3 = (M * x1.hom()).dehom();
      rhVector e = x2 - x3;

      if (outfile)
         fprintf (outfile,
   "(%7.1f, %7.1f, %7.1f)->(%7.1f, %7.1f, %7.1f) cf (%7.1f, %7.1f, %7.1f): E=%4.2f\n",
         x1[0], x1[1], x1[2], x3[0], x3[1], x3[2], x2[0], x2[1], x2[2],
         sqrt(e*e));

      // Accumulate the error
      errorsumsq += e*e;
      }

   /* Now print out the RMS error */
   double rms_error = sqrt(errorsumsq / numpoints);
   if (outfile)
   fprintf (stderr, "RMS error = %10.3f\n", rms_error);

   /* Return the RMS error */
   return (rms_error);
   }

FUNCTION_DEF ( double horn_error, (const rhMatrix &X1, const rhMatrix &X2))
   {
   /* Returns the Euclidean error between the point formations */

   /* Compute the transform */
   rhMatrix T = horn (X1, X2);

   /* Compute the error */
   double error = test_horn_transform((FILE *)0, X1, X2, T);

   /* Return the error */
   return (error);
   }

FUNCTION_DEF (int horn_main_cc,  ( int argc, char *argv[] ))
   {
   /* Tests out Horn's algorithm */

   /* First test out the argument list */
   if (argc < 2)
      {
      fprintf (stderr, "Usage : %s numpoints sdeviation\n", argv[0]);
      bail_out (1);
      }

   /* Get the number of points and the deviation */
   int numpoints = atoi (argv[1]);
   double sdev   = atof (argv[2]);

   /* Allocate the coordinates */
   rhMatrix X1 (numpoints, 3);

   /* Generate random coordinates */
   for2Dindex (i, j, X1)
      {
      const double order_of_mag = 10.0;
      X1[i][j] = order_of_mag * rh_irandom();
      }

   /* Now generate a random rotation */
   rhMatrix R(3, 3);
   rhVector v(rh_random(), rh_random(), rh_random());
   VecToMatrix (v, R);

   /* Add a translation */
   rhVector t(3);
   t[0] = 10.0 * rh_random();
   t[1] = 10.0 * rh_random();
   t[2] = 10.0 * rh_random();

   /* Get a random scale */
   double scale = 2.0 * rh_random();

   /* Now put together a transformation */
   rhMatrix M = scale * (R | t);

   /* Now transform the points */
   rhMatrix X1hom = X1 | rhVector::ones(X1.idim());
   rhMatrix X2 = X1hom * M.transpose();

   /* Add a bit of noise to the points */
   for2Dindex (i, j, X2)
      X2[i][j] += grandom (0.0, sdev);

   /* Now we are ready to try out the transformation */
   rhMatrix M2 = horn (X1, X2);

   /* Test the result */
   test_horn_transform (stderr, X1, X2, M2);

   /* Test with the original matrix as well */
   test_horn_transform (stderr, X1, X2, M);

   return 0;
   }

