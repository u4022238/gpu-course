#include "Utilities_CU/utilities_CC.h"
#include "Utilities_CU/Powell.h"

inline double SQR(double x) { return x*x; }

// Function to minimize.  It is taken for granted that the value fval can
// not increase in this function call

double Powell::solve()
     // Minimization of a function func of n variables. 
     //
     // Input:
     //    initial starting point p[1..n]; 
     //    initial matrix xi[1..n][1..n], 
     //       whose columns contain the initial set of directions 
     //       (usually the n unit vectors); 
     //
     // Output:
     //    p is set to the best point found, 
     //    xi is the then-current direction set, 
     //
     // The routine linmin is used.

   {
   // Get the dimension
   int n = p_.dim();

   // Arrays to be needed
   rhVector xit(n);

   // Initial evaluation of the function.  
   // fval always contains value at current accepted point.
   double fval = function(p_);

   // Save the initial point
   rhVector pt = p_.copy();

   // Iterate until convergence
   for (int iter=1; ;iter++) 
      {
      double fp = fval;		// Function val at beginning of iteration
      int ibig = 0;
      double maxdel = 0.0; // Will be the biggest function decrease.

      // Minimize in each direction - one iteration
      for (int i=0; i<n; i++) 
         { 
         //In each iteration, loop over all directions in the set.

         // Take the i-th direction to minimize over
         for (int j=0; j<n; j++) xit[j] = xi_[j][i];

         // Store old error and minimize
         double oldval = fval;
         linmin(p_, xit, &fval); // minimize along it

         // Record the largest increase
         if (oldval - fval > maxdel) 
            { 
            maxdel = oldval - fval;
            ibig = i;
            }
         }

      // Terminate if improvement is too small
      if (2.0*fabs(fp-fval) <= Accuracy*(fabs(fp)+fabs(fval)))
	 if (iter >= Min_Loops)
            return fval;

      // Test for too many iterations
      if (iter >= Num_Loops) 
         informative_message("powell exceeding maximum iterations");

      // Update the average direction and the start
      xit = p_ - pt;
      pt  = p_;

      // Evaluate function at extrapolated point
      double ext_val = function(p_ + xit);

      // See if this helps
      if (ext_val < fp) 
         {
         double t = 2.0*(fp - 2.0*fval + ext_val)*SQR(fp - fval - maxdel)
            - maxdel*SQR(fp - ext_val);

         if (t < 0.0) 
            {
            // Move to the minimum of the new direction, 
            linmin(p_, xit, &fval); 

            // Save the new direction.
            for (int j=0; j<n; j++) 
               {
               xi_[j][ibig] = xi_[j][n];
               xi_[j][n]    = xit[j];
               }
            }
         }
      } // Back for another iteration.
   }
