
//  
// The QRotation class -- rotations are represented as 3-vectors
//
//  

#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/QRotation.h>
#include <Utilities_CU/Pmap.h>

//---------------------------------------------------------------
// 	QRotation methods

// Various constructors
QRotation::QRotation () : rhVector (3) {}

QRotation::QRotation (double x, double y, double z) : rhVector (x, y, z) {}

QRotation::QRotation (rhVector &vec) : rhVector (vec) {}

QRotation::QRotation (const rhMatrix &R) : rhVector (Pmap::MatrixToPvector (R)) {}

// Operators
QRotation QRotation::operator * (QRotation q2)
   {
   // Multiply two QRotations together
   QRotation &q1 = *this;

   // Make them both into matrices
   rhMatrix R1 = q1.ToMatrix();
   rhMatrix R2 = q2.ToMatrix();
 
   // Multiply
   rhMatrix P = R1 * R2;

   // Make back into a QRotation and return
   return QRotation(P);
   }

QRotation QRotation::inverse ()
   {
   QRotation &R = *this;
   rhVector Rinv = -R;
   QRotation Qinv(Rinv);
   return Qinv;
   }

rhMatrix QRotation::ToMatrix () const
   {
   rhMatrix R = Pmap::PvectorToMatrix (*this);
   return R;
   }

QRotation QRotation::identity()
   {
   return QRotation (0.0, 0.0, 0.0);
   }

double QRotation::angle() const
   {
   // Finds the angle of a rotation
   double angle = 2.0 * this->length();
   return angle;
   }

rhVector QRotation::axis() const
   {
   // Be careful with identity
   double angle = this->angle();
   if (1.0 - angle == 1.0)
      return rhVector (1.0, 0.0, 0.0);
   else
      return this->normalized();
   }

