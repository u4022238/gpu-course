// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)lp.cc	1.8 12 Sep 1995

/* Linear Programming stuff from Numerical Recipes */
/* Changed to double, given a decent interface */

/* SFILE_BEGIN */
#include <Utilities_CU/utilities_CC.h>

// #define rhDEBUG

// Error Messages
#define  LP_Success                  0
#define  LP_Unbounded                1
#define  LP_Infeasible               10
#define  LP_IPEqZero                 11
#define  LP_InsufficientConstraints  12
#define  LP_InsufficientRank         13
#define  LP_Feasible		     14
/* SFILE_END */

// static const double EPS = 1.0e-12;
static double EPS = 1.0e-10;

//------------------------------------------------------------------------
// A flag to determine how it works - doing it this way
// makes the code non-reentrant !!
static bool g_feasibility_only = false;

FUNCTION_DEF (void LP_set_feasibility_only, ( bool val ))
   {
   g_feasibility_only = val;
   }

FUNCTION_DEF (bool LP_get_feasibility_only, ( ))
   {
   return g_feasibility_only;
   }
//------------------------------------------------------------------------

FUNCTION_DEF (void print_lpp_problem, (
   FILE *outfile,
   rhMatrix &A,
   rhVector &b,
   rhVector &goal
   ))
   {
   // Prints out the lpp problem appropriate for input
   
   // First, the dimensions
   fprintf (outfile, "%d %d\n", A.jdim(), A.idim());

   // Now print out each row
   int i, j;
   for_1Dindex (i, b)
      {
      // First, the row of A
      for (j=A.jlow(); j<=A.jhigh(); j++)
	 fprintf (outfile, "%16.9e ", A[i][j]);

      // Then the goal element
      fprintf (outfile, "%16.9e\n", b[i]);
      }

   // Next, print out the goal
   goal.print(outfile, "%16.9e\n");
   }   

void simplx_check (rhMatrix &A, int *iposv)
   {
   // Declare a vector to hold the result
   int ncols = A.jdim()-1;
   int nrows = A.idim()-2;

   // Make a vector to hold the result
   rhVector x(ncols+1);
   x.clear(0.0);
   x[0] = 1.0;

   // Now, get the values back out and read them into x
   int j;
   for (j=0; j<nrows; j++)
      {
      // Values greater than ncols denote slack variables
      int n = iposv[j];
      if (n >= ncols+1) continue;

      // Read the value from the output tableau
      double val = A[j+1][0];
      x[n] = val;
      }

   // Multiply out x by the tableau
   rhVector constr = A * x;
   printf ("Checking problem:\n");
   for_1Dindex (j, constr)
      printf ("\t%.3e\n", constr[j]);
   }

FUNCTION_DEF(static void simp1_base1, (
             double **a,
             int mm,	// Look for suitable column in row mm
             int *ll,
             int nll,
             int iabf,
             int *kp,
             double *bmax))
   {
   int k;
   double test;

   if (nll <= 0)
      {
      *bmax = 0.0;
      }
   else
      {
      *kp=ll[1];
      *bmax=a[mm][*kp+1];
      for (k=2;k<=nll;k++) 
         {
         if (iabf == 0)
            test=a[mm][ll[k]+1]-(*bmax);
         else
            test=fabs(a[mm][ll[k]+1])-fabs(*bmax);
         if (test > 0.0) 
            {
            *bmax=a[mm][ll[k]+1];
            *kp=ll[k];
            }
         }
      }
   }

FUNCTION_DEF(static void simp2_base1, (
             double **a,
             int n,
             int m,
             int *ip,
             int kp,
             double *q1))
   {
   int k,ii,i;
   double qp,q0,q;

   // static double EPS = 0.0;

   *ip=0;

   // Any possible pivots
   for (i=1;i<=m;i++) 
      if (a[i+1][kp+1] < -EPS) break;
   if (i > m) return;

   *q1 = -a[i+1][1]/a[i+1][kp+1];
   *ip=i;
   for (i=i+1;i<=m;i++) 
      {
      ii=i;
      if (a[ii+1][kp+1] < -EPS) 
         {
         q = -a[ii+1][1]/a[ii+1][kp+1];
         if (q < *q1) 
            {
            *ip=ii;
            *q1=q;
            } 
         else if (q == *q1) 
            {
            for (k=1;k<=n;k++) 
               {
               qp = -a[*ip+1][k+1]/a[*ip+1][kp+1];
               q0 = -a[ii+1][k+1]/a[ii+1][kp+1];
               if (q0 != qp) break;
               }
            if (q0 < qp) *ip=ii;
            }
         }
      }
   }

FUNCTION_DEF(static void simp3_base1, (
             double **a,
             int nrows,
             int ncols,
             int ip,
             int kp))
   {
   int kk,ii;
   double piv;
   int pcol = kp+1;
   int prow = ip+1;

   piv=1.0/a[prow][pcol];
   for (ii=1; ii<=nrows+1; ii++)
      if (ii != prow) 
         {
         a[ii][pcol] *= piv;
         for (kk=1; kk<=ncols+1; kk++)
            if (kk != pcol)
               {
#ifdef rhDEBUG
               //----------- DEBUGGING ---------------
               double newval = a[ii][kk] - a[prow][kk]*a[ii][pcol];
               if (kk == 1 && newval < 0.0)
                  {
                  printf ("ALARM\n");
                  printf ("Pivot at %d, %d.  row = %d\n", prow, pcol, ii);
                  printf ("Negative value in row %d , column 1: \n", ii);
                  printf (" old = %.3e, new = %.3e\n", a[ii][kk], newval);

                  double aa1   =  a[prow][1];
                  double aacol = -a[prow][pcol];
                  double bb1   =  a[ii][1];
                  double bbcol = -a[ii][pcol] / piv;

                  printf ("Values: %e  %e ; %e  %e\n", aa1, aacol, bb1, bbcol);
                  printf ("Check right pivot:  %e < %e (diff < 0) : %e \n",
                     aa1/aacol, bb1/bbcol, aa1/aacol - bb1/bbcol);
                  printf ("Check neg outcome: %e\n",
                     bb1 - bbcol * (aa1 / aacol));
                  }
               //----------- DEBUGGING ---------------
#endif

               a[ii][kk] -= a[prow][kk]*a[ii][pcol];
               }
         }

      for (kk=1; kk<=ncols+1; kk++)
         if (kk != pcol) a[prow][kk] *= -piv;
      a[prow][pcol]=piv;

#ifdef rhDEBUG
      {
         // Now print out the tableau
         fprintf (stdout, "Tableau (in simp3)\n");
         fprint_matrix ("%10.1e", stdout, a, 1, nrows+1, 1, ncols+1);
      }
#endif
   }

FUNCTION_DEF(static void simplx_base1, (
	     rhMatrix &A,
             double **a,
             int m,
             int n,
             int m1,
             int m2,
             int m3,
             int *icase,
             int *izrov,
             int *iposv))
   {
   int i,ip,ir,is,k,kh,kp,m12,nl1;
   double q1,bmax;
   Int_vector l1(1,n+1);
   Int_vector l3(1,m);

   if (m != (m1+m2+m3)) 
      {
      error_message("Bad input constraint counts in SIMPLX");
      exit (1);
      }

   nl1=n;
   for (k=1;k<=n;k++) l1[k]=izrov[k]=k;

   for (i=1;i<=m;i++) 
      {
      if (a[i+1][1] < 0.0) 
	 {
	 error_message("Bad input tableau in SIMPLX");
	 exit (1);
	 }
      iposv[i]=n+i;
      }

   for (i=1;i<=m2;i++) l3[i]=1;
   ir=0;
   if (m2+m3) 
      {
      ir=1;
      for (k=1;k<=(n+1);k++) 
         {
         q1=0.0;
         for (i=m1+1;i<=m;i++) q1 += a[i+1][k];
         a[m+2][k] = -q1;
         }

      // Main phase-1 loop
      int loopcount = 0;
      do {

	 //-----------------------------------------------------------
         //  RIH added : 9/3/99
	 // Implement something to get out of this loop if we went
	 // around too many times.
         const int maxcount = 100000;
	 if (++loopcount  >= maxcount)
	    {
	    EPS *= 2.0;
	    loopcount = 0;
	    }
	 //-----------------------------------------------------------

         int breaking_out = FALSE;
         simp1_base1(a,m+2,l1,nl1,0,&kp,&bmax);

         if (bmax <= EPS && a[m+2][1] < -EPS) 
            {
            *icase = LP_Infeasible;
            fprintf (stderr, "LP: Exiting because bmax = %e, a[%d][1] = %e\n", 
               bmax, m+2, a[m+2][1]);
            return;
            } 
         else if (bmax <= EPS && a[m+2][1] <= EPS) 
            {
            m12=m1+m2+1;
            if (m12 <= m) 
               {
               for (ip=m12;ip<=m;ip++) 
                  {
                  if (iposv[ip] == (ip+n)) 
                     {
                     simp1_base1(a,ip+1,l1, nl1,1,&kp,&bmax);
                     if (bmax > 0.0)
                        {
                        /* goto one; */
                        breaking_out = TRUE;
                        break; 
                        }
                     }
                  }
               }
     
            if (! breaking_out)
               {
               ir=0;
               --m12;
               if (m1+1 <= m12)
                  for (i=m1+1;i<=m12;i++)
                     if (l3[i-m1] == 1)
                        for (k=1;k<=n+1;k++)
                           a[i+1][k] = -a[i+1][k];
               break;
               }
            }

         if (! breaking_out)
            {
            simp2_base1(a,n,m,&ip,kp,&q1);
            if (ip == 0) 
               {
               *icase = LP_IPEqZero;
               fprintf (stderr, "Exiting because ip == 0\n");
               return;
               }
            }

/*one:*/ 
         breaking_out = FALSE;
         simp3_base1(a,m+1,n,ip,kp);

         if (iposv[ip] >= (n+m1+m2+1)) 
            {
            for (k=1;k<=nl1;k++)
               if (l1[k] == kp) break;
            --nl1;
            for (is=k;is<=nl1;is++) l1[is]=l1[is+1];
            a[m+2][kp+1] += 1.0;
            for (i=1;i<=m+2;i++) a[i][kp+1] = -a[i][kp+1];
            } 
         else 
            {
            if (iposv[ip] >= (n+m1+1)) 
               {
               kh=iposv[ip]-m1-n;
               if (l3[kh]) 
                  {
                  l3[kh]=0;
                  a[m+2][kp+1] += 1.0;
                  for (i=1;i<=m+2;i++)
                     a[i][kp+1] = -a[i][kp+1];
                  }
               }
            }

         is=izrov[kp];
         izrov[kp]=iposv[ip];
         iposv[ip]=is;

#ifdef rhDEBUG
	 simplx_check (A, iposv+1);
#endif
         } while (ir);
      }

   // We have determined it is feasible.  Now refine
#ifdef rhDEBUG
   printf ("Initial solution found - checking solution\n");
   simplx_check (A, iposv+1);
#endif

   // Now, take an opportunity to stop
   if (g_feasibility_only)
      {
      *icase = LP_Feasible;
      return;
      }

   // Phase 2 -- improvement of the solution
   for (;;) 
      {
      simp1_base1(a,1,l1,nl1,0,&kp,&bmax);
      if (bmax <= 0.0) 
         {
         *icase=LP_Success;
         return;
         }
      simp2_base1(a,n,m,&ip,kp,&q1);
      if (ip == 0) 
         {
         *icase=LP_Unbounded;
         return;
         }
      simp3_base1(a,m,n,ip,kp);
      is=izrov[kp];
      izrov[kp]=iposv[ip];
      iposv[ip]=is;
      
#ifdef rhDEBUG
      // Check the problem to see if original constraints hold
      simplx_check (A, iposv+1);
#endif
      }
   }

//
// These are my routines giving a decent interface to the program
//

#include <Utilities_cuda/base1_interface_dcl.h>

FUNCTION_DEF( rhVector lp_constrained , (
             rhMatrix A, 
             rhVector b, 
             rhMatrix Aeq,
             rhVector beq,
             rhVector goal, 
             int *flag
             ))
   {
   // Solve the system A*x +  b >= 0 while maximizing <goal, x>
   // We assume extra constraints that x >= 0;
   int i, j;

   // Get the number of equations and the number of variables
   int nrows = A.nrows();
   int ncols = A.ncols();
   int neq = beq.is_null() ? 0 : beq.dim();
   nrows += neq;

   // Declare a suitable array for the recipes program
   rhMatrix AA (nrows+2, ncols+1);

   // First, fill out the goal
   AA[0][0] = 0.0;
   for (i=0; i<ncols; i++)
      AA[0][i+1] = goal[i];

   // Now, the rest of the tableau
   int count = AA.ilow();
   int m1=0, m2=0, m3=0;

   // First get the leq (less-than) constraints
   for (i=0; i<nrows-neq; i++)
      if (b[i] >= 0)
         {
         // This is a leq constraint
         m1++;
         count++;

         // Fill out the constraints -- each variable becomes two
         for (j=0; j<ncols; j++)
            AA[count][j+1]   =  A[i][j];

         // Fill out the constant term
         AA[count][0] = b[i];
         }

   // Next get the geq (greater-than) constraints
   for (i=0; i<nrows-neq; i++)
      if (b[i] < 0)
         {
         // This is a leq constraint
         m2++;
         count++;

         // Fill out the constraints -- each variable becomes two
         for (j=0; j<ncols; j++)
            AA[count][j+1]   = -A[i][j];

         // Fill out the constant term
         AA[count][0] = -b[i];
         }

   // Equality constraints
   for (i=0; i<neq; i++)
      if (beq[i] >= 0)
         {
         // Equality constraint
         m3++;
         count++;

         // Fill out the constraints -- each variable becomes two
         for (j=0; j<ncols; j++)
            AA[count][j+1]   =  Aeq[i][j];

         // Fill out the constant term
         AA[count][0] = beq[i];
         }

      else
         {
         // Equality constraint
         m3++;
         count++;

         // Fill out the constraints -- each variable becomes two
         for (j=0; j<ncols; j++)
            AA[count][j+1]   = -Aeq[i][j];

         // Fill out the constant term
         AA[count][0] = -beq[i];
         }


   // Now, declare the other things that we need to solve this
   Int_vector iposv (0, nrows-1);
   Int_vector izrov (0, ncols-1);

#ifdef rhDEBUG
   // Now print out the tableau
   fprintf (stdout, "Tableau (before solution)\n");
   fprintf (stdout, "# greater-than = %d, # less-than = %d, # equal = %d\n",
         m1, m2, m3);
   fprint_matrix ("%10.1e", stdout, AA, 0, nrows, 0, ncols);
#endif

   // Now we can call the simplex routine
      {
      // Must switch to base 1 arrays
      double **AA1 = rh_base1 (AA, nrows+2);
      simplx_base1 (AA, AA1, nrows, ncols, m1, m2, m3, flag, izrov-1, iposv-1);
      if (AA1) free (AA1+1);
      }

   // Declare a vector to hold the result
   rhVector x(ncols);
   x.clear(0.0);

   // Do not bother with the result if we only want feasibility
   if (g_feasibility_only) return x;

   // Now, get the values back out and read them into x
   for (j=0; j<nrows; j++)
      {
      // Values greater than ncols denote slack variables
      int n = iposv[j]-1;	// -1 because it is set in a base1 routine
      if (n >= ncols) continue;

      // Read the value from the output tableau
      double val = AA[j+1][0];
      x[n] = val;
      }

#ifdef rhDEBUG
   // Now print out the tableau
   fprintf (stdout, "Tableau (after solution)\n");
   fprint_matrix ("%10.1e", stdout, AA, 0, nrows, 0, ncols);
   fprintf (stdout, "Solution:\n");
   for (j=0; j<ncols; j++) fprintf (stdout, "\t%.3e\n", x[j]);
#endif

   // Now, return
   return x;
   }

FUNCTION_DEF(rhVector lp_solve , (
             rhMatrix A, 
             rhVector b, 
             rhMatrix Aeq,
             rhVector beq,
             rhVector goal, 
             int *flag
             ))
   {
   //
   // This solves the unconstrained linear programming problem by making
   // a transformation.
   // The problem being solved is : 
   //   Maximize goal . x
   //   subject to A * x + b >= 0
   //      where 0 represents a 0-vector and >= 0 means each element is
   //      greater than 0.
   // 

   int i, j, k;

   // We need to find the set of the rows of A that are linearly dependent
   int nrows = A.nrows();
   int ncols = A.ncols();

   // Check that we do have enough equations
   if (nrows < ncols)
      {
      error_message ("linear programming : Insufficient constraints");
      *flag = LP_InsufficientConstraints;
      rhVector soln(ncols);
      soln.clear(0.0);
      return soln;
      }

   // Take a copy of A
   rhMatrix AA = A.copy();

   // Normalize A so that its rows are normalized
   for (i=0; i<nrows; i++)
      {
      double sqlen = 0.0;
      for (j=0; j<ncols; j++)
         sqlen += AA[i][j]*AA[i][j];
      double fac = 1.0 / sqrt (sqlen);
      for (j=0; j<ncols; j++)
         AA[i][j] *= fac;
      }

   // Reduce the rank
   int rank = ncols;
   for (i=0; i<rank; i++)
      {
      // First, find the maximum element in the i-th column
      // in rows > i.
      double maxelement = rhAbs (AA[i][i]);
      int maxindex = i;

      // Start searching after row i;
      for (j=i+1; j<nrows; j++)
         if (rhAbs(AA[j][i]) > maxelement)
            {
            maxindex = j;
            maxelement = rhAbs(AA[j][i]);
            }

         // If the maximum element is too close to zero, then we must stop
         if (maxelement < EPS)
            {
            error_message ("Linear programming : Insufficient rank for matrix");
            *flag = LP_InsufficientRank;
            rhVector soln(ncols);
            soln.clear(0.0);
            return soln;
            }

         // Swap this row to the i-th position
         if (maxindex > i)
            {
            double temp;

            // Swap the rows of the matrix AA
            for (k=i; k<ncols; k++)
               {
               temp = AA[i][k]; 
               AA[i][k] = AA[maxindex][k]; 
               AA[maxindex][k] = temp;
               }

            // Also swap the rows of the matrix A
            for (k=0; k<ncols; k++)
               {
               temp = A[i][k]; 
               A[i][k] = A[maxindex][k]; 
               A[maxindex][k] = temp;
               }

            // Also swap b
            temp = b[i]; b[i] = b[maxindex]; b[maxindex] = temp;
            }

         // Now, clear out the rest of the rows
         for (j=i+1; j<nrows; j++)
            {
            // Work out the multiple of row j to add to row i
            double fac = -AA[j][i] / AA[i][i];

            // Now clear out the row
            AA[j][i] = 0.0;
            for (k=i+1; k<ncols; k++)
               AA[j][k] += fac * AA[i][k];
            }
      }

   //
   // At this point, the top nrows matrices of A give constraints that are
   // invertible
   //
   // Now, we carry out the reduction to the constrained LP problem
   //
   // We make the substitution xred = A1 * x + b1
   // Then the equations become 
   //               xred >= 0
   //               Ared * xred + bred >= 0
   // 

   // Declare the matrices that we need
   rhMatrix A1inv = (A.submatrix (0, ncols-1, 0, ncols-1)).inverse();
   rhMatrix A2 = A.submatrix (ncols, nrows-1, 0, ncols-1);
   rhMatrix Ared = A2 * A1inv;

   // Reduced vectors
   rhVector b1(ncols), b2(nrows-ncols);
   for_1Dindex (i, b1) b1[i] = b[i];
   for_1Dindex (i, b2) b2[i] = b[i+ncols];
   rhVector bred = b2 - Ared*b1;

   // Also for the equality constraints -- if they are given
   rhMatrix Aeq_red;
   rhVector beq_red;

   if (!Aeq.is_null())
      {
      Aeq_red = Aeq * A1inv;
      beq_red = beq - Aeq_red * b1;
      }

   // Change the goal as well
   rhVector gred = goal * A1inv;

   // We can now solve the problem
   rhVector xred = lp_constrained (Ared, bred, Aeq_red, beq_red, gred, flag);

   // Now, get the solution back again
   rhVector x = A1inv * (xred - b1);

#ifdef rhDEBUG
   // Check the solution is positive
   rhVector constraints0 = Ared * xred + bred;
   printf ("Short constraints\n");
   for_1Dindex (i, constraints0)
      printf ("\t%.3e\n", constraints0[i]);
   rhVector constraints = A * x + b;
   printf ("Original constraints\n");
   for_1Dindex (i, constraints)
      printf ("\t%.3e\n", constraints[i]);
#endif

   return x;
   }

// Old interfaces without the equality constraints
FUNCTION_DEF( rhVector lp_constrained , (
             rhMatrix A, 
             rhVector b, 
             rhVector goal, 
             int *flag
             ))
   {
   rhMatrix Aeq;
   rhVector beq;
   return lp_constrained (A, b, Aeq, beq, goal, flag);
   }

FUNCTION_DEF(rhVector lp_solve , (
             rhMatrix A, 
             rhVector b, 
             rhVector goal, 
             int *flag
             ))
   {
   rhMatrix Aeq;
   rhVector beq;
   return lp_solve (A, b, Aeq, beq, goal, flag);
   }

