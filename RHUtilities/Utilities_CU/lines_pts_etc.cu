// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)lines_pts_etc.cc	1.6 11/22/96

//
// Various geometrical entities
//


#include <Utilities_CU/lines_points_etc.h>

namespace rh {

rhPoint2D rhPoint2D::standardized () const
   {
   // Normalize a point so that last entry is 1
   const rhPoint2D &l = *this;
   rhPoint2D l2;
   double fac = 1.0 / l[ l.high()];
   int i;

   for_1Dindex (i, l2)
      l2[i] = l[i]*fac;

   // Return the vector
   return l2;
   }

rhLine3D rhPoint2D::project (const rhCameraMatrix &) const
   {
   // Project a line to a 3D line
   error_message ("rhPoint2D::project : Not yet implemented");
   bail_out (2);

   // Just to please the compiler
   return rhLine3D ();
   }

rhLine2D rhLine2D::standardized () const
   {
   // Normalize a line so that a^2 + b^2 = 1
   const rhLine2D &l = *this;
   rhLine2D l2;
   double fac = 1.0 / sqrt(l[0]*l[0] + l[1]*l[1]);
   int i;

   for_1Dindex (i, l2)
      l2[i] = l[i]*fac;

   // Return the vector
   return l2;
   }

rhPlane3D rhLine2D::project (const rhCameraMatrix &M) const
   {
   // Project a line to a 3D plane
   const rhLine2D &l = *this;
   return l * M;
   }

rhPoint3D rhPoint3D::standardized () const
   {
   // Normalize a point so that last entry is 1
   const rhPoint3D &l = *this;
   rhPoint3D l2;
   double fac = 1.0 / l[l.high()];
   int i;

   for_1Dindex (i, l2)
      l2[i] = l[i]*fac;

   // Return the vector
   return l2;
   }

// Transform a point via a camera transform
rhPoint2D rhPoint3D::transform (const rhCameraMatrix &M) const
   {
   // Transform a point into an image
   return M * (*this);
   }

rhLine3D::rhLine3D (const rhPlane3D &p1, const rhPlane3D &p2) : rhMatrix (2, 4)
   {
   // Line specified by two planes
   // We use the Singular value decomposition to get two points on the planes
   rhMatrix A(2, 4), V;
   rhVector D;

   // Fill out the matrix D with the two lines
   A.setrow (0, p1);
   A.setrow (1, p2);

   // Take its svd
   svd (A, D, V);

   // Take the last two columns of V as the two points
   this->setrow (0, V.column(3));
   this->setrow (1, V.column(2));
   }

// Transform a line via a camera transform
rhLine2D rhLine3D::transform (const rhCameraMatrix &M) const
   {
   // Transform each of the points
   rhPoint2D p1 = M * this->p1();
   rhPoint2D p2 = M * this->p2();

   // Return the line through the two points
   return p1^p2;
   }

rhPoint3D rhPlane3D::operator ^ (const rhLine3D &line) const
   {
   // Intersection of a line with a plane
   const rhPlane3D &plane = *this;
   rhPoint3D p1 = line.p1();
   rhPoint3D p2 = line.p2();

   return p1*(p2*plane) - p2*(p1*plane);
   }

// Other functions

// Distance from point to line in 2D
double distance (const rhPoint2D &p, const rhLine2D &l)
   {
   rhVector pp = p.uvw();
   double scale = pp[2] * sqrt (l[0]*l[0] + l[1]*l[1]);
   double dist = pp*l / scale;

   // Return the absolute value for distance
   return dist > 0.0 ? dist : -dist;
   }

// Distance from point to a plane in 3D
double distance (const rhPoint3D &point, const rhPlane3D &plane)
   {
   double scale = point[3] * 
	sqrt (plane[0]*plane[0] + plane[1]*plane[1] + plane[2]*plane[2]);
   double dist = point*plane / scale;

   // Return the absolute value for distance
   return dist > 0.0 ? dist : -dist;
   }


//
// Transformations
//

rhPoint2D rhTransform2D::operator () (const rhPoint2D &p) const
   {
   // Transform a 2D point
   return (*this)*p.uvw();
   }

rhPoint2D rhTransform2D::operator * (const rhPoint2D &p) const
   {
   // Transform a 2D point
   return (*(rhMatrix*)this)*(p.uvw());
   }
 
rhLine2D rhTransform2D::operator () (const rhLine2D &l) const
   {
   // Transform a line
   return (*this).adjoint() * l;
   }

rhPoint3D rhTransform3D::operator () (const rhPoint3D &p) const
   {
   // Transform a 3D point
   return (*this)*p;
   }
 
rhLine3D rhTransform3D::operator () (const rhLine3D &l) const
   {
   // Transform a line
   const rhTransform3D &M = *this;
  
   // Transform each of two points on the line separately
   return M(l.p1()) ^ M(l.p2());
   }

rhPlane3D rhTransform3D::operator () (const rhPlane3D &p) const
   {
   // Transform a line
   return (*this).adjoint() * p;
   }

// Rotation about an axis

rhMatrix Matrix_from_quaternion (
   const rhQuaternion &q           /* The quaternion */
   )
   {
   /* Given a normalized quaternion,
    *  computes the rotation matrix it represents
    */   
 
   /* Get the components of the quaternion */
   double       c = q[0],
                x = q[1],
                y = q[2],
                z = q[3];/* Components of the quaternion*/
 
   /* Now compute the terms needed in the matrix representation */
   double xx = x * x;
   double yy = y * y;
   double zz = z * z;
   double cc = c * c;
   double xy = x * y;
   double xz = x * z;
   double yz = y * z;
   double cx = c * x;
   double cy = c * y;
   double cz = c * z;
 
   /* Declare a matrix */
   rhMatrix R(3, 3);

   /* Fill out the rotation matrix */
   R[0][0] = cc + xx - yy - zz;
   R[0][1] = 2.0 * (xy - cz);
   R[0][2] = 2.0 * (xz + cy);
 
   R[1][0] = 2.0 * (xy + cz);
   R[1][1] = cc + yy - xx - zz;
   R[1][2] = 2.0 * (yz - cx);
 
   R[2][0] = 2.0 * (xz - cy);
   R[2][1] = 2.0 * (yz + cx);
   R[2][2] = cc + zz - xx - yy;

   /* Return the matrix */
   return R;
   }

rhTransform3D Rotation3D (const rhVector3D &axis, double angle)
   {
   // Computes a transform from an axis and an angle

   // Convert to a quaternion
   rhQuaternion q (cos(angle/2.0), sin(angle/2.0) * (axis.normalized()));

   // Compute the rotation from a quaternion
   return Rotation3D (q);
   }

rhTransform3D Rotation3D (const rhQuaternion &q)
   {
   // Computes the rotation given a quaternion

   // Compute the rotation matrix
   rhMatrix R = Matrix_from_quaternion (q);

   // Now, put it in the matrix M
   rhMatrix M(4, 4);
   
   // Copy R to M
   int i, j;
   for_2Dindex (i, j, R) M[i][j] = R[i][j];

   // Fill out the other parts
   for (i=0; i<=2; i++)
     M[i][3] = M[3][i] = 0.0;
   M[3][3] = 1.0;

   // Now return M
   return rhTransform3D (M);
   }


rhMatrix rhTransform3D::rotation ()
   {

   rhMatrix R(3, 3);
   rhTransform3D &tmp = *this;
 
   // Copy this to R
   int i, j;
   for_2Dindex (i, j, R)
      R[i][j] = tmp[i][j];


   // Now return R
   return R;
   }
   
rhVector3D rhTransform3D::translation ()
   {

   rhVector3D t;
   rhTransform3D &tmp = *this;
 
   // Copy this to t
   int i;
   for_1Dindex (i, t)
      t[i] = tmp[i][3];


   // Now return t
   return t;
   }

rhTransform3D rhTransform3D::inverse ()
   {

   rhMatrix R(3, 3);
   rhVector3D t;
   rhTransform3D &tmp =*this;

   // Rotation part
   R = tmp.rotation ();

   // Inverse it
   R = R.transpose ();

   // Translation part
   t = tmp.translation ();

   // Inverse it
   t = - R * t;

   // Now, put it in the matrix M
   rhMatrix M(4, 4);
   
   // Copy R to M
   int i, j;
   for_2Dindex (i, j, R)
      M[i][j] = R[i][j];

   for_1Dindex (i, t)
      M[i][3] = t[i];
 
   // Fill out the other parts
   for (i=0; i<=2; i++)
     M[3][i] = 0.0; M[3][3] = 1.0;

   // Now return M
   return rhTransform3D (M);
   }

} // End namespace
