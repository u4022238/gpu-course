// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// @(#)Matrix.cc	1.8 April 24, 2000
//

// Routines for matrices

#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/Polynomial.h>

rhPolynomial rhPolynomial::operator * (const rhPolynomial &b) const
   {
   // Product of two polynomials
   const rhPolynomial &a = *this;

   // Make a polynomial of the required degree
   rhPolynomial ab (a.degree() + b.degree() + 1);
   ab.clear(0.0);

   // Now, multiply them out
   int i, j;
   for_1Dindex (i, a)
      for_1Dindex (j, b)
	 ab[i+j] += a[i] * b[j];

   // Return the product
   return ab;
   }

rhPolynomial rhPolynomial::operator + (const rhPolynomial &b) const
   {
   // Sum of two polynomials
   const rhPolynomial &a = *this;

   // Make a polynomial of the required degree
   rhPolynomial ab (rhMax(a.degree(), b.degree()) + 1);
   ab.clear(0.0);

   // Now, multiply them out
   int i;
   for_1Dindex (i, a) ab[i] += a[i];
   for_1Dindex (i, b) ab[i] += b[i];

   // Return the sum
   return ab;
   }

rhPolynomial rhPolynomial::operator - (const rhPolynomial &b) const
   {
   // Sum of two polynomials
   const rhPolynomial &a = *this;

   // Make a polynomial of the required degree
   rhPolynomial ab (rhMax(a.degree(), b.degree()) + 1);
   ab.clear(0.0);

   // Now, multiply them out
   int i;
   for_1Dindex (i, a) ab[i] += a[i];
   for_1Dindex (i, b) ab[i] -= b[i];

   // Return the sum
   return ab;
   }

rhPolynomial rhPolynomial::operator + (double b) const
   {
   // Sum of polynomial and a double
   const rhPolynomial &a = *this;

   // Make a polynomial of the required degree
   rhPolynomial ab = a.copy();

   // Add the value
   ab[0] += b;

   // Return the sum
   return ab;
   }

rhPolynomial rhPolynomial::operator - (double b) const
   {
   // Difference of a polynomial and a double
   const rhPolynomial &a = *this;

   // Make a polynomial of the required degree
   rhPolynomial ab = a.copy();

   // Add the value
   ab[0] -= b;

   // Return the sum
   return ab;
   }

double rhPolynomial::eval (double x) const
   {
   // Evaluate a polynomial
   const rhPolynomial &p = *this;

   double val = p[p.high()];

   // Horner's method
   for (int i=p.high()-1; i>=0; i--)
      val = val * x + p[i];

   // Return the value
   return val;
   }

rhPolynomial rhPolynomial::operator ^ (int n) const
   {
   // Power of a polynomial
   const rhPolynomial &p = *this;
  
   // Must be a non-negative power
   assert (n >= 0);

   // Make a polynomial of the required degree
   rhPolynomial pow2 = p;
   rhPolynomial pow (1.0);

   // Shift and multiply
   while (1)
      {
      // Include this factor if least significant bit of n is set
      if (n & 1)
	 pow = pow * pow2;

      // Shift n right
      n = n >> 1;

      // Exit when n is reduced to zero
      if (n == 0) break;

      // Square the power for the next iteration
      pow2 = pow2 * pow2;
      }

   // Return the power
   return pow;
   }

rhVector rhPolynomial::real_roots ()
   {
   // Finds the real roots of a polynomial
   int i;

   // Get the polynomial
   const rhPolynomial &p = *this;
   int degree = p.degree();

   // Declare the complex values
   fcomplex *roots = new fcomplex[degree];
   complex_roots_real_coefficients_0 (p, degree, roots, 1);

   // Count the real roots
   int count = 0;
   for (i=0; i<degree; i++)
      if (roots[i].i == 0.0)
	 count += 1;

   // Make a vector to hold the values
   rhVector r(count);

   // Fill out the real roots
   count = 0;
   for (i=0; i<degree; i++)
      if (roots[i].i == 0.0)
	 r[count++] = roots[i].r;

   // Delete the complex vector
   delete [] roots;

   // Return the vector of roots
   return r;
   }

rhPolynomial rhPolynomial::derivative ()
   {
   // Finds the real roots of a polynomial
   int i;

   // Get the polynomial
   const rhPolynomial &p = *this;

   // Allocate the derivative
   rhPolynomial deriv (p.degree());
  
   // Take the derivative
   for_1Dindex (i, deriv)
      deriv[i] = (i+1) * p[i+1];

   // Return the vector of roots
   return deriv;
   }

rhPolynomial operator + (double x, const rhPolynomial &a) 
   {
   // Sum of polynomial and a double

   // Make a polynomial of the required degree
   rhPolynomial ab = a.copy();

   // Add the value
   ab[0] += x;

   // Return the sum
   return ab;
   }

rhPolynomial operator - (double x, const rhPolynomial &a) 
   {
   // Sum of polynomial and a double

   // Make a polynomial of the required degree
   rhPolynomial ab = -a;

   // Add the value
   ab[0] += x;

   // Return the sum
   return ab;
   }

