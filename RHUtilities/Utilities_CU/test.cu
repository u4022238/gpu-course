// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// #define _ITERATOR_DEBUG_LEVEL 2
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <Utilities_CU/Matrix.h>
#include <Utilities_CU/utilities_CC.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_CU/array3gen.h>
#include <thread>

class wordentry
   {
   public :
      int id;
      char *aword;
      wordentry (int n, char *w) { id = n; aword = w; }
      void print () { printf ("%-6d : %s\n", id, aword); }
   };

//  Define a table of words
#define Tbl_TYPE WordTable
#define Tbl_DATA class wordentry *
#define Tbl_KEY  char *
#include "Table.h"

/* EXTERN_C_FUNCTION (long random, ()); */
/* extern "C" { long random (); } */

int scmp (wordentry *a, wordentry *b)
   {
   // Compare two wordentries
   char *wa = a->aword;
   char *wb = b->aword;
   return strcmp (wa, wb);
   }

int sfind (char *wa, wordentry *b)
   {
   // Compare a char * with a wordentry
   char *wb = b->aword;
   return strcmp (wa, wb);
   }

char *random_word ()
   {
   const int len = 8;
   char word[len+1];
   word[len] = '\0';
   for (int i=0; i<len; i++)
     word[i] = rh_random() % 26 + 'a';
   return COPY(word);
   }

void put_word_in_table (int i, char *aword, WordTable atab)
   {
   atab.insert (new wordentry (i, aword));
   }

int main0(int argc, char *argv[])
   {
   int i;

   // Debugging entry
   try_crash(argc, argv);

   // Declare a table
   WordTable wordtab(scmp, sfind);

   // Put words in the table
   printf ("Words as entered\n");
   for (i=1; i<=20; i++)
      {
      char *aword = random_word ();
      put_word_in_table (i, aword, wordtab);

      // Find the word in the table
      wordentry *entry;
      if ((entry = (wordentry*)wordtab.find (aword)) != NULL)
	 entry->print();
      else
	 printf ("Insert failed\n");
      }

   // Copy the table for no reason at all
   WordTable newtab = wordtab;

   // Try its depth
   printf ("Table has depth %d\n", newtab.depth());

   // Now print out the words
   printf ("Ordered words\n");
   Tbl_forall (wordentry *, anentry, newtab)
      {
      anentry->print();
      } Tbl_endall

   return EXIT_SUCUESS;
   }

int main1(int, char *argv[])
   {
   int i, j;

   // int nrow = atoi(argv[1]);
   // int ncol = atoi(argv[2]);
   int nrow = 10;
   int ncol = 15;

   rhMatrix A (nrow, ncol);
   rhVector x (ncol);

   // Initialize the matrices
   for_2Dindex (i, j, A)
      A[i][j] = rh_irandom()-0.5;
   for_1Dindex (i, x)
      x[i] = rh_irandom()-0.5;

   // Now make the goal vector -- and add noise
   rhVector b = A * x;
   for_1Dindex (i, b)
      b[i] += grandom(0, 0.1);

   // Print out the equations
   printf ("Matrix:\n");
   A.print();

   // Print the solution
   printf ("Vector:\n");
   b.print();

   // Solve the equations
   x = lin_solve (A, b);

   // Print the solution
   printf ("Solution:\n");
   x.print();

   // Check it
   printf ("Check:\n");
   (b - A*x).print();

   // Return a value to please the compiler
   return EXIT_SUCUESS;
   }

int main2(int, char *argv[])
   {
   int i, j;

   rhMatrix A(3, 3);
   for_2Dindex (i, j, A)
      A[i][j] = i+2*j;

   rhMatrix B(3, 3);
   for_2Dindex (i, j, B)
      B[i][j] = 2*i - j;

   rhMatrix C = A-B;

   C.print();

   // Return a value to please the compiler
   return EXIT_SUCUESS;
   }

// Put one reference to each file in here, to see where the links take us
class LV : public Levenberg
   {
   rhVector function (rhVector &mp); 
   };

rhVector LV::function (rhVector &mp)
   {
   return mp;
   }

#ifdef JUNK
void link_force ()
   {
   // ComplexVector.cc and Vector.cc
   rhVector a;
   ComplexVector b(a);

   // Levenberg.cc
   LV lev;

   // List.cc
   List l;
   
   // Matrix.cc and Matrix2.cc
   rhMatrix m;
   QRdecomposition (m, m, m);

   // String.cc
   rhString ("Hello");

   // double_array.cc
      {
      Double_matrix dm(1, 3, 1, 3);
      Double_vector dv (1, 3);
      Float_matrix fm (1, 3, 1, 3);
      Float_vector fv (1, 3);
      Int_matrix im (1, 3, 1, 3);
      Int_vector iv (1, 3);
      Unsigned_char_matrix um (1, 3, 1, 3);
      Unsigned_char_vector uv (1, 3);
      }

   // double_geom.cc
      {
      Double_point dp;
      Int_point ip;
      rhPoint2D p;
      p = p.standardized();
      }

   // Fourier transform
      {
      rhVector v;
      ComplexVector b = fourier_transform(v, -1);
      }

   // Pyramid
      {
      Pyramid_Image_type A;
      int n = number_of_pyramid_levels (A, 5);
      }

   // read_pts_lines.cc
      {
      rhVector *u;
      rhVector x;
      Boolean_matrix defined;
      write_standard_input_file (stdout, u, u, x, x, x, defined);
      }

   // imageio.cc
   /* 
      {
      Unsigned_short_matrix A;
      Unsigned_char_matrix M = truncate_to_output (A, 256);
      }
   */
   }

#endif

#include <Utilities_CU/Pmap.h>

int main_Pmap (int argc, char *argv[])
   {
   // Tests the Pmap stuff
   int i, j;

   // Opportunity to debut
   try_crash (argc, argv);

   // Test hom / dehom
   int nRepeats = 100;
   int dim = 3;
   for (i=0; i<nRepeats; i++)
      {
      // Make a random vector
      rhVector dvec(dim);

      // Make a random vector
      for_1Dindex (j, dvec)
	      dvec[j] = urandom(0.0, 1.5);

      // Homogenize and dehomogenize
      rhVector hvec = Pmap::hom (dvec);
      rhVector dvec2 = Pmap::dehom (hvec);
      rhVector dvec3 = dvec.copy();
      Pmap::normalize (dvec3);

      // See that the results are the same
      rhVector diff = dvec2 - dvec3;
      printf ("%e\n", diff.length());

      if (diff.length() > 0.01)
	      {
	      printf ("Alarm\n");
	      printf ("dvec  :  "); dvec.print();
	      printf ("hvec  :  "); hvec.print();
	      printf ("dvec2 :  "); dvec2.print();
	      printf ("dvec3 :  "); dvec3.print();
	      printf ("---------------------------\n");
	      }

      // Now convert to a rotation matrix, and then back again
      rhMatrix R = Pmap::PvectorToMatrix (dvec);
      
      // Multiply by random value
      double ranval = urandom (0.0, 5.0);
      R = R * ranval;

      rhVector dvec4 = Pmap::MatrixToPvector (R);
      diff = dvec4 - dvec3;

      if (diff.length() > 0.01)
	      {
	      printf ("Alarm -- matrix\n");
	      printf ("dvec  :  "); dvec.print();
	      printf ("   len = %f\n", dvec.length());
	      printf ("hvec  :  "); hvec.print();
	      printf ("   len = %f\n", hvec.length());
	      printf ("R  :     \n"); R.print();
	      printf ("R * dvec "); (R * dvec).print();
	      printf ("dvec4 :  "); dvec4.print();
	      printf ("   len = %f\n", dvec4.length());
	      printf ("dvec3 :  "); dvec3.print();
	      printf ("   len = %f\n", dvec3.length());
	      printf ("---------------------------\n");
	      }
      }

   return EXIT_SUCUESS;
   }

int main7 (int argc, char *argv[])
   {
   // Try out the TokenIO
   try_crash (argc, argv);

   char *fname = argv[1];
  
   // Make token IO
   TokenIO tok;
   if (!tok.Init (fname)) return EXIT_FAILURE;

   // Now read tokens
   while (1)
      {
      // Read the next line -- break on EOF
      int result = tok.nextline();
      if (result == EOF) break;

      // Print out the tokens
      printf ("File \"%s\", Line %d :\n\t", tok.currentinput(), tok.NR);
      tok.ECHO();
      printf ("tokens \n");
      for (int i=0; i<tok.NF; i++)
	 printf ("   %s\n", tok.token[i]);

      // Try switching to other file
      if (strcmp (tok.token[0], "push") == 0)
	tok.pushinput (tok.token[1]);

      if (strcmp (tok.token[0], "switch") == 0)
	tok.switchinput (tok.token[1]);

      // Try error message
      if (strcmp (tok.token[0], "error") == 0)
	tok.reporterror ();
      }

   return EXIT_SUCUESS;
   }

int main8 (int argc, char *argv[])
   {
   int i, j, k;

      {
      Double_array3D Q;

         {
         Double_array3D P(0, 2, -1, 3, 5, 9);

         double count = 0.0;
         for_3Dindex (i, j, k, P)
            P[i][j][k] = count++;

	      printf ("Dimensions of P: %d %d %d\n", P.idim(), P.jdim(), P.kdim());
         P.print();

         Q = P;
         }

      printf ("Dimensions of Q: %d %d %d\n", Q.idim(), Q.jdim(), Q.kdim());

      Q.print(stdout, "%6.2f ");
      }

      Int_array3D S(2, 3, 5);

      int icount = 0;
      for_3Dindex (i, j, k, S)
         S[i][j][k] = icount++;

      S.print(stdout, "%3d ");

      return EXIT_SUCUESS;
   }

int main_jacobi (int argc, char *argv[])
   {
   // Tests Jacobi
   int i, j;

   int idim = 50;
   int jdim = 30;

   // if (argc >= 2) idim = atoi(argv[1]);
   // if (argc >= 3) jdim = atoi(argv[2]);

   // Opportunity to debut
   try_crash (argc, argv);

      {
      // Make a random matrix
      rhMatrix M(idim, jdim);

      // Make a random vector
      for_2Dindex (i, j, M)
	 M[i][j] = urandom(0.0, 2.0);

      // Now, make symmetric
      rhMatrix A = M.transpose() * M;

      // Run the Jacobi method
      int dim = A.idim();
      rhMatrix V(dim, dim);
      rhVector D(dim);
      jacobi_0 (A, dim, D, V);

      // Print out the values
      printf ("Jacobi results\n");
      printf ("\n V\n");
      V.print();
      printf ("\n D\n");
      for_1Dindex (i, D) D[i] = sqrt(D[i]);
      D.print();

      // Now, do it the other way
      rhMatrix V2;
      rhVector D2;
      svd (M, D2, V2);

      // Print out the last column of V
      printf ("Singular value decomposition\n");
      printf ("\n V\n");
      V2.print();
      printf ("\n D\n");
      D2.print();

      // Print out something that can be verified
      printf ("\nCompare\n");
      for (i=0; i<dim; i++)
	 printf ("%12.4e   %12.4e   %8.5f\n",
		D[i], D2[i], V.column(i) * V2.column(i));

      return EXIT_SUCUESS;
      }
   }

#include <Utilities_CU/Polynomial.h>
int main_polynomial (int argc, char *argv[])
   {
   rhPolynomial x(0.0, 1.0);

   rhPolynomial p1 = (x-1.)^6;
   rhPolynomial p2 = (x+1.)^6;

   (p1+p2).print();
   (p1-p2).print();
   (p1*p2).print();

   rhPolynomial y = (x^3) + 5.0 * (x^2) + 2.0 * x + 1.0;
   printf ("Polynomial\n");
   y.print();
   printf ("Roots are\n");
   rhVector roots = y.real_roots();
   roots.print();

   y = (1. + x) * (x-2.) * (x-2.9999) * (x-2.9998) * (x-2.99985);
   roots = y.real_roots();

   printf ("Polynomial\n");
   y.print();
   printf ("Roots are\n");
   roots.print();

   return EXIT_SUCUESS;
   }

int main_push_back (int argc, char *argv[])
   {
   int i;
   Int_vector a;
   for (i=0; i<50; i++)
      a.push_back(i);

   printf ("Values\n");
   a.print();

   Double_vector b;
   for (i=0; i<30; i++)
      b.push_back((double)i);

   printf ("Values\n");
   b.print();

   rhVector c;
   for (i=0; i<30; i++)
      c.push_back((double)i);

   printf ("Values\n");
   c.print();

   return EXIT_SUCUESS;
   }

#include <vector>
using namespace std;
#include <Utilities_CU/KDTree.h>

int main_kdtree (int argc, char *argv[])
   {
   // Tests the KDTree structure
  
   // This requires arguments
   if (argc < 4)
      {
      error_message (
            "kdtree: Usage: test dimension npoints ntrials <maxtrials>");
      return EXIT_FAILURE;
      }

   int i, j;
   int maxtrials = -1;
   int dimension = atoi(argv[1]);
   int npoints = atoi(argv[2]);
   int ntrials = atoi(argv[3]);
   if (argc > 4)
       maxtrials = atoi(argv[4]);

   // Make an array of things
   vector<rhFloatVector> points(npoints);
   for (i=0; i<npoints; i++)
      {
      // Make a vector
      rhFloatVector v(dimension);

      // Fill with random values
      for_1Dindex (j, v)
	      v[j] = (float) urandom(0.0, 1.0);

      // Put in the array
      points[i] = v; 
      }

// #define TEST_CORRECTNESS
#ifdef TEST_CORRECTNESS
   // Print out the values
   printf ("Points\n");
   for (i=0; i<npoints; i++)
      {
      printf ("%4d ", i);
      points[i].print(stdout, "%7.3f ");
      }
   fflush(stdout);
#endif

   printf ("Check C\n"); fflush(stdout);

   // Message
   timed_message (stdout, "Creating tree ");
   printf ("with %d points\n", npoints);

   // Now, create the tree
   KDTree *kdtree = new KDTree(points);
   kdtree->set_max_trials(maxtrials);

   timed_message (stdout, "Testing ");
   printf ("%d points\n", ntrials);

   // Count of the number of errors
   int numerrors = 0;

#ifdef RANDOM_TEST
   // Now, test it
   for (i=0; i<ntrials; i++)
      {
      // Make a random vector
      rhFloatVector v(dimension);

      // Fill with random values
      for_1Dindex (j, v)
	      v[j] = urandom(0.0, 1.0);

      // Test it
      int match = kdtree->find_nearest_neighbour(v);

// #define TEST_CORRECTNESS
#ifdef TEST_CORRECTNESS
      // Check it
      rhFloatVector diff = points[match] - v;
      double bestdist = diff*diff;

      // Put result
      // printf ("Test %d: ", i);
      // v.print(stdout, "%7.3f ");
      // printf ("dist = %e\n", sqrt(bestdist));

      // Check that this is the closest
      for (j=0; j<npoints; j++)
	      {
	      diff = points[j] - v;
	      double dist = diff*diff;
	      if (j != match && dist < bestdist)
	         {
	         // v.print();
	         // fprintf (stderr, "Point %d closer than supposed minimum %d\n", 
	         //	j, match);
	         // fprintf (stderr, "   Distance %e versus %e\n", dist, bestdist);
	         numerrors++;
	         break;
	         }
	      }
#endif
      }

#else

   // Now, test it
   for (i=0; i<ntrials; i++)
      {
      // Make a random vector
      rhFloatVector v = points[i].copy();

      // Add noise
      for_1Dindex (j, v)
	      v[j] += (float) grandom(0.0, 0.2);

      // Test it
      int match = kdtree->find_nearest_neighbour(v);

      // Result should be the same point
      if (match != i)
	      {
         // Check it
         double diffm = (points[match] - v).length_sq();
         double diffi = (points[i] - v).length_sq();

	      if (diffm > diffi) numerrors++;
	      }
      }
#endif

   printf ("Number errors = %d : Percentage = %f\n", 
        numerrors, 100.0*numerrors / (double) ntrials);

   timed_message (stdout, "Finished\n");

   // Print out the results
   kdtree->print_statistics();

   return EXIT_SUCUESS;
   }

int main_kddimension (int argc, char *argv[])
   {
   // Tests the real dimension of the data
   int i, j;

   // int dimension = atoi(argv[1]);
   // int npoints = atoi(argv[2]);

   int dimension = 50;
   int npoints = 100;

   // Make an array of things
   rhMatrix points(npoints, dimension);
   for (i=0; i<npoints; i++)
      for (j=0; j<dimension; j++)
         {
         points[i][j] = urandom(0.0, 1.0);
         }

   // Determine the singular values
   rhVector D;
   svd (points, D);

   D.print();

   return EXIT_SUCUESS;
   }

#include "Utilities_CU/Timer.h"
int timer_main (int argc, char *argv[])
   {
   double sum = 0.0;

   rhTimer::OpenFile ("timing.out");
   rhTimer::SetTimingOn();

   rhTimer t("Outer loop");
   for (int i=0; i<1000; i++)
      {
      {
      rhTimer t2("Inner loop");
      for (int j=0; j<1000000; j++)
         sum += (double) (i+j);
      }

      rhTimer *t3 = new rhTimer("Second loop");
      for (int j=0; j<1000000; j++)
         sum -= (double) (i+j);
      delete t3;
      }

   return EXIT_SUCUESS;
   }

#include "Utilities_CU/Timer2.h"
int timer2_main (int argc, char *argv[])
   {
   double sum = 0.0;

   rhTimer2::SetTimingOn();

   rhTimer2 *t = new rhTimer2("Outer loop");
   for (int i=0; i<1000; i++)
      {
      {
      rhTimer2 t2("Inner loop");
      for (int j=0; j<1000000; j++)
         sum += (double) (i+j);
      }

      rhTimer2 *t3 = new rhTimer2("Second loop");
      for (int j=0; j<1000000; j++)
         sum -= (double) (i+j);
      delete t3;
      }

   delete t;
   rhTimer2::print_results();

   return EXIT_SUCUESS;
   }

int fourier_main (int argc, char *argv[])
   {
   const int nsamples = 32;
   rhVector sig (nsamples);
   double offs3 = 0.25;       // Random value
   double offs5 = 0.7;
   for1Dindex (i, sig)
      {
      double theta = 2.0 * M_PI * i / nsamples;
      sig[i] = sin (5 * theta + offs5) + sin (3 * theta + offs3);
      }

   // Now, take the fourier transform
   rhVector re, im, re2, im2;
   fourier_transform (sig, re, im, 1);
   rhVector abs(nsamples);
   for1Dindex (i, abs) abs[i] = re[i]*re[i] + im[i]*im[i];
   fourier_transform (abs, re2, im2, -1);

   for1Dindex (i, re)
      printf ("%8.5f\t %8.5f\t %8.5f\t%8.5f\t%8.5f\n", 
            sig[i], re[i], im[i], re2[i], im2[i]);

   return EXIT_SUCUESS;
   }

int memcpy_main (int argc, char *argv[])
   {
   // if (argc != 2) 
   //   {
   //   fprintf (stderr, "Usage test nrepetitions\n");
   //   return EXIT_FAILURE;
   //   }

   // Hard code number of repetitions
   const int nreps  = 1000;

   rhMatrix A(1000, 4096);
   rhMatrix B(1000, 4096);
   for2Dindex (i, j, A)
      A[i][j] = i+j;

   int nbytes = A.jdim() * sizeof(double);

   // Turn timing on
   rhTimer2::SetTimingOn();

   bool same;

      {
      rhTimer2 t("Mem copy");
      for (int rep = 0; rep<nreps; rep++)
         for (int i=0; i<A.idim(); i++)
            memcpy (B[i], A[i], nbytes);

      same = true;
      for2Dindex (i, j, A)
         if (A[i][j] != B[i][j])
            {
            fprintf (stderr, "A and B differ in position (%d, %d)\n", i, j);
            same = false;
            goto end;
            }
      }

      {
      rhTimer2 t("Set row");
      for (int rep = 0; rep<nreps; rep++)
         for (int i=0; i<A.idim(); i++)
            B.setrow(i, A.row(i));

      same = true;
      for2Dindex (i, j, A)
         if (A[i][j] != B[i][j])
            {
            fprintf (stderr, "A and B differ in position (%d, %d)\n", i, j);
            same = false;
            goto end;
            }
      }

      {
      B.clear (27);

      rhTimer2 t("Clear row");
      for (int rep = 0; rep<nreps; rep++)
         B.clear();

      if (0)
         {
         same = true;
         for2Dindex (i, j, B)
            if (B[i][j] != 0)
               {
               fprintf (stderr, "B wrong in position (%d, %d)\n", i, j);
               fprintf (stderr, "Value is %f\n", B[i][j]);
               B.print(stdout, "%4.1f ");
               same = false;
               goto end;
               }
         }
      }

      {
      rhTimer2 t("New set row");
      for (int rep = 0; rep<nreps; rep++)
         for (int i=0; i<A.idim(); i++)
            B.setrow(i, A[i]);

      same = true;
      for2Dindex (i, j, A)
         if (A[i][j] != B[i][j])
            {
            fprintf (stderr, "A and B differ in position (%d, %d)\n", i, j);
            same = false;
            goto end;
            }
      }

   if (0)
      {
      rhTimer2 t("Set column");
      for (int rep = 0; rep<nreps; rep++)
         for (int j=0; j<A.jdim(); j++)
            B.setcolumn(j, A.column(j));

      same = true;
      for2Dindex (i, j, A)
         if (A[i][j] != B[i][j])
            {
            fprintf (stderr, "A and B differ in position (%d, %d)\n", i, j);
            same = false;
            goto end;
            }
      }

end:

   if (same) 
      printf ("Memcpy succeeded\n");
   else 
      printf ("Memcpy failed\n");

   rhTimer2::print_results();

   return EXIT_SUCUESS;
   }

int main_syntax (int argc, char *argv[])
   {
   //-----------------------------
   // Tests the range-for syntax
   rhVector A(7);
   int count = 0;
   for (auto &num : A)
      num = (double) (count++);
   printf ("\ncount = %d\n", count);
   fflush (stdout);

   for (auto &num : A)
      printf ("%lf ", num);
   printf ("\n"); fflush (stdout);

   for (double *num = A.begin(); num != A.end(); num++)
      printf ("%lf ", *num);
   printf ("\n"); fflush (stdout);

   rhMatrix B(3, 4);
   for (auto &num : B)
      num = (double) (count++);

   for (double * num = B.begin(); num != B.end(); num++)
      printf ("%lf ", *num);
   printf ("\n"); fflush (stdout);

   for (auto num : B)
      printf ("%lf ", num);
   printf ("\n"); fflush (stdout);

   vector <double> fred {1.0, 2.0, 3.0, 7.0};
   for (auto num : fred)
      printf ("%lf ", num);
   printf ("\n");
   fflush (stdout);

   return EXIT_SUCUESS;
   }


int main (int argc, char *argv[])
   {
   //-----------------------------

   printf ("Testing begins\n");

   int result;
   result = main_syntax (argc, argv);
   result = timer_main (argc, argv);
   result = timer2_main (argc, argv);
   result = fourier_main (argc, argv);
   result = main_Pmap (argc, argv);
   result =  main0(argc, argv);
   result =  main_kdtree(argc, argv);
   result =  main_jacobi(argc, argv);
   result = memcpy_main (argc, argv);

   rhVector fred(3);
   double fredmed = fred.median();

   return result;
   }
