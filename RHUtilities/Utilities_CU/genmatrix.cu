//
// Compiled matrix_type class members
//

#define USE_MEMCPY

// ============  Error Handler Stuff ===================

void Matrix_standard_error_handler ();
void (* matrix_type::Error_Handler)() = Matrix_standard_error_handler;

void matrix_type::set_Error_Handler (void (*fn)()) const
   {
   // Set the error handler
   Error_Handler = fn;
   }

// ============  end Error Handler Stuff ===================

void matrix_type::dereference ()
   {
   if (reference_count)
      {  
      // First unlink the thing
      *reference_count -= 1;

#     ifdef REF_DEBUG
      fprintf (stderr, 
         "Reference count for matrix %08x decreased to %d\n",
         Val, *reference_count);
#     endif

      // Now, if reference count has fallen to zero, then free
      if (*reference_count <= 0)
         {
#	 ifdef REF_DEBUG
         extern int MatrixBytesAllocated;
         int size = (Ihigh-Ilow+1)*(Jhigh-Jlow+1)*sizeof(element_type);
         MatrixBytesAllocated -= size;
         fprintf (stderr, "Freeing matrix %08x size %d : total = %d\n", 
            Val, size, MatrixBytesAllocated);
# 	 endif
	
         // Free the values
         if (Val != (element_type **)0)
            rhFREE_macro (Val, Ilow);

         // Delete the reference count
         delete reference_count;
         }
      }   

   // Set Val and reference count to initial values
   Val = (element_type **) 0;
   reference_count = (unsigned int *)0;
   Ilow = Ihigh = Jlow = Jhigh = 0;
   }

void matrix_type::Init (int il, int ih, int jl, int jh)
   {
   // First, dereference this array
   dereference ();

   // Now reallocate
   Ilow  = il;
   Ihigh = ih;
   Jlow  = jl;
   Jhigh = jh;
   Val = MATRIX (Ilow, Ihigh, Jlow, Jhigh, element_type);

#  ifdef REF_DEBUG
   extern int MatrixBytesAllocated;
   int size = (Ihigh-Ilow+1)*(Jhigh-Jlow+1)*sizeof(element_type);
   MatrixBytesAllocated += size;
   fprintf (stderr, "Allocate matrix %08x size %d : total = %d\n", 
      Val, size, MatrixBytesAllocated);
#  endif

   // Also the reference count
   reference_count = new unsigned int;
   *reference_count = 1;
#  ifdef REF_DEBUG
   fprintf (stderr,"Reference count for matrix %08x set to 1\n",
   Val);
#  endif
   }

matrix_type matrix_type::zero_origin () const
   {
   // Returns another matrix with the same data but based at (0,0)
   const matrix_type &A = *this;
   matrix_type B (0, A.idim()-1, 0, A.jdim()-1);
   int nbytes = A.idim() * A.jdim() * sizeof (element_type);
   memcpy (&(B[0][0]), &(A[A.ilow()][A.jlow()]), nbytes);
   return B;
   }

// Other actions
void matrix_type::clear () const
   { 
#ifdef USE_MEMCPY
   int nbytes = (Ihigh-Ilow+1) * (Jhigh-Jlow+1) * sizeof(element_type);
   memset (&(Val[Ilow][Jlow]), 0, nbytes);
#else
   for (int i = Ilow; i <= Ihigh; i++)
      for (int j = Jlow; j <= Jhigh; j++)
          Val[i][j] = 0;
#endif
   }

element_type matrix_type::max() const
   {
   // Get an alias for the vector
   const matrix_type &v = *this;
  
   // Initialize with the first element in the array
   element_type mx = v[v.ilow()][v.jlow()];

   // Find the maximum
   int i, j;
   for_2Dindex (i, j, v)
      if (v[i][j] > mx) mx = v[i][j];

   // Return the maximum
   return mx;
   }

element_type matrix_type::min() const
   {
   // Get an alias for the vector
   const matrix_type &v = *this;
  
   // Initialize with the first element in the array
   element_type mx = v[v.ilow()][v.jlow()];

   // Find the minimum
   int i, j;
   for_2Dindex (i, j, v)
      if (v[i][j] < mx) mx = v[i][j];

   // Return the minimum
   return mx;
   }

double matrix_type::sum() const
   {
   // Get an alias for the vector
   const matrix_type &v = *this;
  
   // Initialize the sum
   double msum = 0.0;

   // Find the minimum
   int i, j;
   for_2Dindex (i, j, v)
      msum += (double)(v[i][j]);

   // Return the minimum
   return msum;
   }

double matrix_type::sumsq() const
   {
   // Get an alias for the vector
   const matrix_type &v = *this;
  
   // Initialize the sum
   double msumsq = 0.0;

   // Find the minimum
   int i, j;
   for_2Dindex (i, j, v)
      {
      double val = (double)(v[i][j]);
      msumsq += val * val;
      }

   // Return the minimum
   return msumsq;
   }

double matrix_type::ij_val_interpolated (double i, double j) const
   {
   // Computes the linearly interpolated value of the image at
   // the point (i, j)

   // Get an alias for the current matrix
   const matrix_type &Im = *this;

   // Get the rectangle on either side
   int ii = (int) floor (i);
   int jj = (int) floor (j);
   if (!Im.defined_at (ii, jj) || !Im.defined_at (ii+1, jj+1)) return 0.0;

   // Get the fractional values
   double fi = i - ii;
   double fj = j - jj;

   // Now get the values at the corners
   double v00 = (double)Im[ii]  [jj];
   double v01 = (double)Im[ii]  [jj+1];
   double v10 = (double)Im[ii+1][jj];
   double v11 = (double)Im[ii+1][jj+1];

   double v0 = v00 + fj * (v01 - v00);
   double v1 = v10 + fj * (v11 - v10);
   
   double v  = v0 + fi * (v1 - v0);

   // This is the interpolated value
   return v;
   }

matrix_type matrix_type::submatrix (int ilow, int ihigh, int jlow, int jhigh) const
   {   
   // Takes and returns a submatrix
   const matrix_type &A = *this;
 
   // Get the bounds 
   int idim = ihigh - ilow + 1; 
   int jdim = jhigh - jlow + 1;
 
   // Check the dimensions 
   if (idim <= 0 || jdim <= 0)   
      { 
      error_message ("MatrixType::submatrix : bad matrix dimension (%d x %d)", 
         idim, jdim); 
      Error_Handler ();
      }   

   // Check the dimensions 
   if (ilow < A.ilow() || jlow < A.jlow() ||
       ihigh > A.ihigh() || jhigh > A.jhigh())
      { 
      error_message ("MatrixType::submatrix : out of bounds");
      Error_Handler ();
      }   
 
   // Now make the new matrix 
   matrix_type B (idim, jdim); 
 
   // Fill it out
   int i, j;
   int ioffs = ilow - B.ilow();
   int joffs = jlow - B.jlow();
   for_2Dindex (i, j, B) 
      B[i][j] = A[i+ioffs][j+joffs]; 
 
   // Return the matrix 
   return B;
   }

