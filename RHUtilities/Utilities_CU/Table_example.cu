// Example of use of tables

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <Utilities_CU/Matrix.h>
#include <Utilities_CU/utilities_CC.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_CU/array3gen.h>

class wordentry
   {
   public :
      int id;
      char *aword;
      wordentry (int n, char *w) { id = n; aword = w; }
      void print () { printf ("%-6d : %s\n", id, aword); }
   };

//  Define a table of words
#define Tbl_TYPE WordTable
#define Tbl_DATA class wordentry *
#define Tbl_KEY  char *
#include "Table.h"

/* EXTERN_C_FUNCTION (long random, ()); */
/* extern "C" { long random (); } */

int scmp (wordentry *a, wordentry *b)
   {
   // Compare two wordentries
   char *wa = a->aword;
   char *wb = b->aword;
   return strcmp (wa, wb);
   }

int sfind (char *wa, wordentry *b)
   {
   // Compare a char * with a wordentry
   char *wb = b->aword;
   return strcmp (wa, wb);
   }

char *random_word ()
   {
   const int len = 8;
   char word[len+1];
   word[len] = '\0';
   for (int i=0; i<len; i++)
     word[i] = rh_random() % 26 + 'a';
   return COPY(word);
   }

void put_word_in_table (int i, char *aword, WordTable atab)
   {
   atab.insert (new wordentry (i, aword));
   }

int main(int argc, char *argv[])
   {
   int i;

   // Debugging entry
   try_crash(argc, argv);

   // Declare a table
   WordTable wordtab(scmp, sfind);

   // Put words in the table
   printf ("Words as entered\n");
   for (i=1; i<=20; i++)
      {
      char *aword = random_word ();
      put_word_in_table (i, aword, wordtab);

      // Find the word in the table
      wordentry *entry;
      if ((entry = (wordentry*)wordtab.find (aword)) != NULL)
	 entry->print();
      else
	 printf ("Insert failed\n");
      }

   // Copy the table for no reason at all
   WordTable newtab = wordtab;

   // Try its depth
   printf ("Table has depth %d\n", newtab.depth());

   // Now print out the words
   printf ("Ordered words\n");
   Tbl_forall (wordentry *, anentry, newtab)
      {
      anentry->print();
      } Tbl_endall

   return 0;
   }

