// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


//-----------------------------------------
//
//   Various matrix classes follow
//
//-----------------------------------------

#ifndef __Tensor_h
#define __Tensor_h

#include <Utilities_cuda/utilities_c.h>
#include <Utilities_CU/Matrix.h>

#define for_3Dindex(i, j, k, M) 			      \
   for (i=M.ilow(); i<=M.ihigh(); i++)			\
   for (j=M.jlow(); j<=M.jhigh(); j++)		   \
   for (k=M.klow(); k<=M.khigh(); k++)	

#define for3Dindex(i, j, k, M) 			      \
   for (int i=M.ilow(); i<=M.ihigh(); i++)   \
   for (int j=M.jlow(); j<=M.jhigh(); j++)	\
   for (int k=M.klow(); k<=M.khigh(); k++)	

class Tensor
   {
   private:

      int Ilow, Jlow, Klow;
      int Ihigh, Jhigh, Khigh;

      rhMatrix *Val;
      unsigned int *reference_count;

      void dereference ()
         {
         if (Val != (rhMatrix *)0)
            {  
            // First unlink the thing
            *reference_count -= 1;

            // Now, if reference count has fallen to zero, then free
            if (*reference_count <= 0)
               {
               for (int i=Ilow; i<=Ihigh; i++) Val[i].Dealloc();
               delete [] (Val + Ilow);
               // FREE (Val, Ilow);
               delete reference_count;
               }
            }   

         // Set Val and reference count to initial values
         Val = (rhMatrix *) 0;
         reference_count = (unsigned int *)0;
         Ihigh = Jhigh = Khigh = 0;
         }

   public:

      // For getting the bounds of the array
      int ilow() const { return Ilow; }
      int jlow() const { return Jlow; }
      int klow() const { return Klow; }
      int ihigh() const { return Ihigh; }
      int jhigh() const { return Jhigh; }
      int khigh() const { return Khigh; }
      int idim () const { return Ihigh - Ilow + 1; }
      int jdim () const { return Jhigh - Jlow + 1; }
      int kdim () const { return Khigh - Klow + 1; }

      //
      // For indexing into the array
      //
      int defined_at (int i, int j, int k) const
         { return i >= Ilow && i <= Ihigh && 
         j >= Jlow && j <= Jhigh &&
         k >= Klow && k <= Khigh; }

      // For indexing into the array without checking
      rhMatrix &operator[] (int i) const { return Val[i]; }

      // For getting a handle on the array itself
      rhMatrix *data () const { return Val; }

      // Basically the same thing, but using casting
      operator rhMatrix * () const { return Val; }

      // Constructors
      Tensor ()	
         { 
         Ilow = Jlow = Klow = 0;
         Ihigh = Jhigh = Khigh = -1;
         Val = (rhMatrix *) 0; 
         reference_count = (unsigned int *)0;
         }

      Tensor (int idim, int jdim, int kdim)
         { 
         Val = (rhMatrix *) 0;
         reference_count = (unsigned int *)0;
         Init (idim, jdim, kdim); 
         }

      Tensor (const Tensor &A)
         {
         // Copy constructor
         Val = (rhMatrix *) 0;
         *this = A;
         }

      Tensor (int idim, int jdim, int kdim, double *vals)
         {
         // Tensor from an array of doubles starting at 0
         Val = (rhMatrix *) 0;
         reference_count = (unsigned int *)0;
         Init(idim, jdim, kdim);
         Tensor &ten = *this;

         int i, j, k, count = 0;
         for_3Dindex (i, j, k, ten)
            ten[i][j][k] = vals[count++];
         }

      void Init (int idim, int jdim, int kdim)
         {
         // First, dereference this array
         dereference ();

         // Now reallocate
         Ilow = Jlow = Klow = 0;
         Ihigh = idim-1;
         Jhigh = jdim-1;
         Khigh = kdim-1;

         //Val = VECTOR (Ilow, Ihigh, rhMatrix);
         Val = new rhMatrix [Ihigh-Ilow+1];
         Val -= Ilow;
         for (int i=Ilow; i<=Ihigh; i++)
            Val[i].Init(jdim, kdim);

         // Also the reference count
         reference_count = new unsigned int;
         *reference_count = 1;
         }

      // Destructors
      void Dealloc () { dereference (); }
      ~Tensor() { dereference (); }

      // Assignment operator
      Tensor &operator = (const Tensor &avec)
         {
         // Increment the reference count
         if (avec.Val)
            {
            *(avec.reference_count) += 1;
            }

         // Dereference the current list
         dereference ();

         // Copy the fields
         Ilow = avec.Ilow;
         Jlow = avec.Jlow;
         Klow = avec.Klow;
         Ihigh = avec.Ihigh;
         Jhigh = avec.Jhigh;
         Khigh = avec.Khigh;
         reference_count = avec.reference_count;
         Val = avec.Val;

         // Return the pointer itself
         return *this;
         }

      // Other actions

      void clear (double val = 0.0) const
         { 
         for (int i = Ilow; i <= Ihigh; i++)
            for (int j = Jlow; j <= Jhigh; j++)
               for (int k = Klow; k<=Khigh; k++)
                  Val[i][j][k] = val;
         }

      Tensor copy () const
         {
         // Copy the array including the elements
         int i, j, k;
         const Tensor &A = *this;
         Tensor B (A.idim(), A.jdim(), A.kdim());
         for_3Dindex (i, j, k, B)
            B[i][j][k] = A[i][j][k];
         return B;
         }

      // Finding maximum and minimum values
      // double max () const;
      // double min () const;
      double sum () const;
      double sumsq () const;

      void print (FILE *afile = stdout, char *format = "%15.6e") const
         {
         for (int i = Ilow; i<=Ihigh; i++)
            Val[i].print (afile, format);
         }

   };

#endif
