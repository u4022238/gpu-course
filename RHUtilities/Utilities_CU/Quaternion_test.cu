
#include <Utilities_CU/Quaternion.h>

int main (int argc, char *argv[])
   {

   // Test out the quaternion operations
   rhVector v1(1., 2., 3., -4.);
   rhVector v2(1., -2., 2., -1.);

   // Make some 
   rhQuaternion q1(v1), q2(v2);
   rhQuaternion qid = q1 % (q1.inverse());

   printf ("Printing quaternions\n");
   q1.print();
   q2.print();
   (q1%q2).print();
   q2.inverse().print();
   qid.print();

   // Now, work with unit quaternions
   v1.normalize();
   v2.normalize();

   rhUnitQuaternion r1(v1), r2(v2);
   rhUnitQuaternion rid = r1 % r1.inverse();
   printf ("Printing unit quaternions\n");
   r1.print();
   r2.print();
   (r1 % r2).print();
   r2.inverse().print();
   rid.print();


   return 0;
   }
