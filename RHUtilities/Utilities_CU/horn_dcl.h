#ifndef _horn_cc_dcl_
#define _horn_cc_dcl_
#include "Utilities_cuda/cvar.h"


FUNCTION_DECL ( void translate_points_rel_centroid, ( rhMatrix &X, rhVector &cent)
   );

FUNCTION_DECL ( rhMatrix horn_rotation, ( const rhMatrix &X1, const rhMatrix &X2));

FUNCTION_DECL ( rhMatrix horn_weighted, ( 
         const rhMatrix &X1in, const rhMatrix &X2in,
         rhVector &weights,
         bool scale_or_not  = true 
         ));

FUNCTION_DECL ( rhMatrix huber_horn, ( 
         const rhMatrix &X1in, const rhMatrix &X2in, 
         double threshold, 
         bool scale_or_not  = true ));

FUNCTION_DECL ( rhMatrix horn, ( const rhMatrix &X1in, const rhMatrix &X2in));

FUNCTION_DECL ( rhMatrix horn_noscale, (
         const rhMatrix &X1in, const rhMatrix &X2in
         ));

FUNCTION_DECL ( double test_horn_transform, (
   FILE *outfile, const rhMatrix &X1, const rhMatrix &X2, const rhMatrix M));

FUNCTION_DECL ( double horn_error, (const rhMatrix &X1, const rhMatrix &X2));

FUNCTION_DECL (int horn_main_cc,  ( int argc, char *argv[] ));

#endif
