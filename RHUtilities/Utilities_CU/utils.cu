#include <vtcore.h>
using namespace vt;
#include <assert.h>

template<class T>
T Sum (const CVec<T> &v)
   {
   T sum = (T)0.0;
   for (int i=0; i<v.Size(); i++)
      sum += v[i];
   return sum;
   }

template<class T>
T Mean (const CVec<T> &v)
   { return Sum(v) / v.Size(); }

template<class T>
CVec<T> dehom(const CVec<T> &v)
   {
   int size = v.Size();
   assert (size >= 2);
   T fac = T(1.0/v[size-1]);
   CVec<T> vh(size-1);
   for (int i=0; i<size-1; i++)
      vh[i] = v[i] * fac;
   return vh;
   }

template<class T>
CVec<T> hom(const CVec<T> &v)
   {
   int size = v.Size();
   assert(size >= 1);
   CVec<T> vh(size+1);
   for (int i=0; i<size; i++)
      vh[i] = v[i];
   vh[size] = 1.0;
   return vh;
   }

template<class T>
CVec2<T> dehom(const CVec3<T> &v)
   {
   CVec2<T> vh(v[0]/v[2], v[1]/v[2]);
   return vh;
   }

template<class T>
CVec3<T> hom(const CVec2<T> &v)
   {
   CVec3<T> vh(v[0], v[1], 1.0);
   return vh;
   }

// Normalization procedures
template<class Vec>
Vec norm_unit(const Vec &v)
   {
   Vec vh = v / sqrt(v*v);
   return vh;
   }

template<class Vec>
Vec norm_hom(const Vec &v)
   {
   Vec vh = v / v[v.Size()-1];
   return vh;
   }

// Instantiation
template CVec<float>   dehom(const CVec<float> &v);
template CVec<float>   hom  (const CVec<float> &v);
template CVec<double>  dehom(const CVec<double>&v);
template CVec<double>  hom  (const CVec<double>&v);
template CVec2<double> dehom(const CVec3<double>&v);
template CVec2<float>  dehom(const CVec3<float>&v);
template CVec3<double> hom  (const CVec2<double>&v);
template CVec3<float>  hom  (const CVec2<float>&v);

template CVec <double> norm_unit(const CVec <double> &v);
template CVec2<double> norm_unit(const CVec2<double> &v);
template CVec3<double> norm_unit(const CVec3<double> &v);

template CVec <float> norm_unit(const CVec <float> &v);
template CVec2<float> norm_unit(const CVec2<float> &v);
template CVec3<float> norm_unit(const CVec3<float> &v);

template CVec <double> norm_hom(const CVec <double> &v);
template CVec2<double> norm_hom(const CVec2<double> &v);
template CVec3<double> norm_hom(const CVec3<double> &v);

template CVec <float> norm_hom(const CVec <float> &v);
template CVec2<float> norm_hom(const CVec2<float> &v);
template CVec3<float> norm_hom(const CVec3<float> &v);

template double Sum (const CVec<double> &v);
template float  Sum (const CVec<float> &v);

template double Mean (const CVec<double> &v);
template float  Mean (const CVec<float> &v);
