#pragma once

#include "Utilities_CU/BestSampleHeap.h"

template<class Type>
void BestSampleHeap<Type>::replace_top (Type &item)
   {
   // Filter down the heap
   int node = 0;
   while (1)
      {
      // How many children does the current node have
      int child1 = child1_of (node);
      int child2 = child2_of (node);
      if (child2 < length_)
         {
         // Both children.  Move earlier one up if < item
         int child = (items_[child1] < items_[child2])? child1 : child2;
         if (items_[child] > item) break;
         items_[node] = items_[child];
         node = child;
         }
      else if (child1 < length_)
         {
         // Only one child.  Move up if < item
         if (items_[child1] > item) break;
         items_[node] = items_[child1];
         node = child1;
         }
      else
         {
         // Current node is at the bottom level
         break;
         }
      }

   // Now, we must put the last event in this slot
   items_[node] = item;
 
#ifdef RH_DEBUG
   // Now check the stack
   for (int i=1; i<length_; i++)
      assert(items_[i] >= items_[0]);
#endif
   }

template<class Type>
void BestSampleHeap<Type>::add_at_bottom (Type &item)
   {
   // Add a new item to the item heap

   // Check that heap is big enough to accept another item -- otherwise double.
   if (length_ >= (int)items_.size())
      items_.resize (2*length_);

   // Filter it up
   int node;
   for (node=length_; node>=1; node=parent_of(node))
     {
     // Get the parent node
     int parent=parent_of(node);

     // Break if the parent is larger
     if (item > items_[parent]) break;

     // Move it
     items_[node] = items_[parent];
     }

   // Now place the original value
   items_[node] = item;

   // Add one to the length
   length_ += 1;

#ifdef RH_DEBUG
   // Now check the stack
   for (int i=1; i<length_; i++)
     assert(items_[0] <= items_[i]);
#endif
   }

template<class Type>
bool BestSampleHeap<Type>::add_item (Type &item)
   {
   // Add an item to the queue

   // If the heap is not yet full, then we need to add to bottom
   if (length_ < maxlength_) 
      {
      add_at_bottom (item);
      return true;
      }

   // Otherwise add it at the top if bigger than previous smallest
   else if (item > items_[0])
      {
      replace_top (item);
      return true;
      }

   else return false;
   }

template<class Type>
bool BestSampleHeap<Type>::pop_item (Type *t)
   {
   // Take the next event off the event heap

   // Null if nothing left on heap
   if (length_ == 0) return false;

   // Get the thing we want to replace
   (*t) = items_[0];

   // Get the last item
   length_ = length_-1;
   Type lastitem = items_[length_];

   // Replace this on the top of the heap
   replace_top (lastitem);

   // This has succeeded
   return true;
   }
