// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)String.cc	1.5 11/27/96
// 
// Implementation of the string class
//
#include <Utilities_CU/rhString.h>

rhString::rhString (const char *s) : Char_vector()
      {
      // Make a string from an array.  We copy the array, so as to avoid
      // freeing constant strings.

      // If it is a null pointer, then do not initialize
      if (s == (const char *)0) return;

      // Initialize others
      Init (strlen(s)+1);

      // Now copy the array
      strcpy (data(), s);
      }

rhString rhString::operator + (const rhString &s2 ) const
   {
   // Concatenation of two strings
   
   // Get a name for this string as well
   const rhString &s1 = *this;

   // Now, create a string of the right length
   rhString ss (s1.length() + s2.length() + 1);

   // Copy the string values
   char *p = ss.data();
   strcpy (p, s1);
   p += s1.length();
   strcpy (p, s2);

   // Return the concatenated string
   return (ss);
   }

rhBOOLEAN rhString::operator == (const rhString &s2) const
   {
   // Compares two strings for equality
   const char *c1 = *this;
   const char *c2 = s2;
   if (c2 == 0 && c1 == 0) return 1;
   else if ((c2 == 0 && c1!= 0) || ((c1==0) && (c2 != 0))) return 0;
   else return (strcmp (c1, c2) == 0);
   }
   
