// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// Array.h	automatically resizing 1D Array class		-*- C++ -*-

#ifndef _Array_h
#define _Array_h

#include <Utilities_CU/rhObject.h>

//
// Index class definitions
//

class Array_Index
   {
   private :

      int i;
      class Array *_a;

   public :

      Array_Index (class Array *s = NULL, int it = 0)	{ _a = s; i = it; }
      Array_Index operator ++ ()	{ i++; return *this; }
      Array_Index operator -- ()	{ i--; return *this; }
      Array_Index succ () 		{ return Array_Index (_a, i+1); }
      Array_Index pred () 		{ return Array_Index (_a, i-1); }
      operator int()			{ return i; }
      class Array *array()      	{ return _a; }
      int valid ();
      void print();
      void print (FILE *);

   };

//
// Array class definitions.
//

void allocSizeErr();
void indexRangeErr();

class Array_Node
   {
   friend class Array;

   private:

      rhObject** v;			// dynamic vector of rhObject ptrs
      int sz;				// number of elements held
      int capac;			// number of elements allocated
      int reference_count;

   public:

      Array_Node(int size = 0);

      ~Array_Node();

   };

class Array : public rhObject
   {
   private:

      Array_Node *a;


   public:

      Array(int size = 0);

      Array(Array&);

      ~Array();

      int size() const		{ return a->sz; }

      void size(int sz);		// Change the size of the array.

      int capacity() const	{ return a->capac; }

      void capacity(int cap);		// Change the capacity manually.

      rhObject * & operator[] (int i);

      rhObject * & operator [] (Array_Index ai);

      Array & addAt(int i, rhObject *);	// insert rhObject at index i

      Array & removeAt(int i);		// delete rhObject at index i

      Array & insert_end(rhObject *ob)	{ return addAt(size(), ob); }

      Array & operator = (Array &arr);

      void print(FILE* =stdout) const;

      Array_Index first_index() { return Array_Index (this, 0); }
   
      Array_Index last_index()  { return Array_Index (this, size()-1); }

      

      
   };


inline rhObject *& Array::operator[] (int i)
{
   if (i < 0 || i >= size()) indexRangeErr();
   return (a->v)[i];
}

inline void Array::print(FILE* s) const
{
   for (int i = 0; i < size(); i++)
      (a->v)[i]->print(s);
}

inline rhObject*& Array::operator[] (Array_Index ai)
{
   int i = int(ai);
   return (*this)[i];
}

inline int Array_Index::valid ()
   { 
   return i>=0 && i< _a->size(); 
   }


#endif	// _Array_h
