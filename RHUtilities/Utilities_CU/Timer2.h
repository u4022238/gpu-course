#ifndef _RIH_TIMER2_h_
#define _RIH_TIMER2_h_

#include <stdio.h>
#include <time.h>
#include <Utilities_cuda/utilities_c.h>
//clock_t clock(void);

class TimingEvent
   {
   public: 
      char *name_;
      int depth_;
      long numcalls_;
      double event_time_;
      double total_time_;

      // Constructors
      TimingEvent (const char *name)
         {
         depth_ = 0;
         numcalls_ = 0;
	      name_ = COPY(name);
         event_time_ = 0.0;
         total_time_ = 0.0;
         }

      ~TimingEvent ()
         {
         if (name_) free (name_);
         }
   };

//  Define a table of words
#define Tbl_TYPE TimeTable
#define Tbl_DATA class TimingEvent *
#define Tbl_KEY  char *
#include "Table.h"

class rhTimer2 
   {
   protected :

        char cstring_[80];
        static bool timing_on;

        // Declare a table
        static TimeTable ttab;

   public :

	     rhTimer2 (const char *context_string);
	     ~rhTimer2();

        static void print_results();

        // Set timing on or off
        static void SetTimingOn  () {timing_on = true; }
        static void SetTimingOff () {timing_on = false; }
   };

#endif
