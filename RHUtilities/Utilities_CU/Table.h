// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

/*  */
/* Include file for use with tables */
// #ifndef rh_Table_h
// #define rh_Table_h

#include <Utilities_cuda/table.h>

#define hdr1(txt) txt##_header
#define T_header(a) hdr1(a)
#define Tbl_header T_header(Tbl_TYPE)

// Default types for the table entries
#ifndef Tbl_TYPE
#define Tbl_TYPE Table
#endif

#ifndef Tbl_KEY
#define Tbl_KEY void *
#endif

#ifndef Tbl_DATA
#define Tbl_DATA void *
#endif

//--------------------------------------------------------------------

class Tbl_header 
   {
   friend class Tbl_TYPE;

   private :
	// Reference count stuff
	int reference_count;	/* How many aliases for this table */

	// Actual table data
        TABLE *table;
	int (*cmp_find) (Tbl_KEY, Tbl_DATA);
        int (*cmp_insr) (Tbl_DATA, Tbl_DATA);
  	int size;

	// Creators
	inline Tbl_header (
		int (*cmp)(Tbl_DATA, Tbl_DATA), 
		int (*fnd)(Tbl_KEY, Tbl_DATA));
	inline ~Tbl_header ();
	void dereference () { if (--reference_count <= 0) delete this; };
        void reference ()   { reference_count ++; }
   };

// Constructors
inline Tbl_header::Tbl_header 
	(int (*cmp) (Tbl_DATA, Tbl_DATA), 
	 int (*fnd) (Tbl_KEY,  Tbl_DATA)) :
	reference_count (0),
	table (NULL),
	cmp_find (fnd),
	cmp_insr (cmp),
	size (0)
   {
   // reference_count = 0;
   // cmp_find = fnd;
   // cmp_insr = cmp;
   // table = NULL;
   // size = 0;
   }
 
inline Tbl_header::~Tbl_header ()
   {
   tbl_freetable (table);
   table = NULL;
   }

//--------------------------------------------------------------------
 
class Tbl_TYPE 
   {
   private :

	Tbl_header *start;

   public :

	Tbl_TYPE &operator = (const Tbl_TYPE &tbl); 
	int refcount () const { return start->reference_count; }
	int operator == (Tbl_TYPE tbl2) const { return start == tbl2.start; }

	// Creators, etc
	Tbl_TYPE (int (*cmp) (Tbl_DATA, Tbl_DATA), int (*fnd) (Tbl_KEY, Tbl_DATA));
	Tbl_TYPE ();
   void instantiate (int (*cmp) (Tbl_DATA, Tbl_DATA), int (*fnd) (Tbl_KEY, Tbl_DATA));
	Tbl_TYPE (Tbl_TYPE &tbl);
	~Tbl_TYPE () { start->dereference(); }


  	// Specific Table operators
	rhBOOLEAN insert (Tbl_DATA data);
	TABLEENTRY *findentry (Tbl_KEY key) const;
	Tbl_DATA first () const;
	Tbl_DATA last () const;
	Tbl_DATA find (const Tbl_KEY key) const;
	int depth() const;
	int size() const;
	rhBOOLEAN remove (Tbl_KEY key);
	int is_empty() const { return start->table == NULL; }
	void free_table ();
        TABLE *tab() const { return start->table; }

	TABLEENTRY *nextentry    ( TABLEENTRY *anentry) const;
	TABLEENTRY *preventry    ( TABLEENTRY *anentry) const;
	TABLEENTRY *nextpreorder ( TABLEENTRY *anentry) const;
	TABLEENTRY *firstentry () const;
	TABLEENTRY *lastentry () const;
	int balance ();
	TABLEENTRY *firstentryafter (Tbl_KEY key) const;
	TABLEENTRY *lastentrybefore (Tbl_KEY key) const;
	Tbl_DATA firstafter         (Tbl_KEY key) const;
	Tbl_DATA lastbefore         (Tbl_KEY key) const;
	int check        ( FILE *afile, char *(*idfn) (TABLE *)) const;
	int dumppreorder ( FILE *afile, char *(*idfn) (TABLE *)) const;
	int dumpinorder  ( FILE *afile, char *(*idfn) (TABLE *)) const;
	int freetable ();
   };

// 
// Inline members for Table class.
//

//--------------------------------------------------------------------
// Generic refence counted methods

inline Tbl_TYPE::Tbl_TYPE (int (*cmp) (Tbl_DATA, Tbl_DATA), 
		     int (*fnd) (Tbl_KEY, Tbl_DATA))
   { 
   start = new Tbl_header(cmp, fnd); 
   start->reference();
   }

inline Tbl_TYPE::Tbl_TYPE ()
   { 
   start = NULL;
   }

inline void Tbl_TYPE::instantiate (int (*cmp) (Tbl_DATA, Tbl_DATA), 
		     int (*fnd) (Tbl_KEY, Tbl_DATA))
   { 
   start = new Tbl_header(cmp, fnd); 
   start->reference();
   }

inline Tbl_TYPE::Tbl_TYPE (Tbl_TYPE &tbl)
   { 
   start = tbl.start; 
   start->reference(); 
   }

inline Tbl_TYPE &Tbl_TYPE::operator = (const Tbl_TYPE &tbl)
   {
   start->dereference();
   start = tbl.start;
   start->reference ();
   return *this;
   }

//-----------------------------------------------------------------------------

// Table specific methods

inline rhBOOLEAN Tbl_TYPE::insert (Tbl_DATA data)
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_insr;
   rhBOOLEAN result = tbl_insert ((DATA)data, &(start->table), fn);
   if (result) start->size++;
   return result;
   }

inline TABLEENTRY *Tbl_TYPE::findentry (Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return tbl_findentry ((DATA)key, &(start->table), fn);
   }

inline Tbl_DATA Tbl_TYPE::first () const
   {
   return (Tbl_DATA) tbl_first (start->table);
   }

inline Tbl_DATA Tbl_TYPE::last () const
   {
   return (Tbl_DATA)tbl_last (start->table);
   }

inline Tbl_DATA Tbl_TYPE::find (const Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return (Tbl_DATA)tbl_find ((DATA)key, &(start->table), fn);
   }

inline int Tbl_TYPE::size() const
   {
   return start->size;
   }

inline int Tbl_TYPE::depth() const
   {
   return tbl_depth(start->table);
   }

inline rhBOOLEAN Tbl_TYPE::remove (Tbl_KEY key)
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   rhBOOLEAN result= tbl_delete ((DATA)key, &(start->table), fn);
   if (result) start->size--;
   return result;
   }

inline void Tbl_TYPE::free_table ()
   {
   tbl_free (start->table);
   }

inline TABLEENTRY *Tbl_TYPE::nextentry (TABLEENTRY *anentry) const
   {
   return tbl_nextentry (anentry);
   }

inline TABLEENTRY *Tbl_TYPE::preventry (TABLEENTRY *anentry) const
   {
   return tbl_preventry (anentry);
   }

inline TABLEENTRY *Tbl_TYPE::firstentry () const
   {
   return tbl_firstentry (start->table);
   }

inline TABLEENTRY *Tbl_TYPE::lastentry () const
   {
   return tbl_lastentry (start->table);
   }

inline TABLEENTRY *Tbl_TYPE::nextpreorder (TABLEENTRY *entry ) const
   {
   return tbl_nextpreorder (entry);
   }

inline int Tbl_TYPE::balance ()
   {
   return tbl_balance (&(start->table));
   }

inline TABLEENTRY *Tbl_TYPE::firstentryafter (Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return tbl_firstentryafter ((DATA)key, &(start->table), fn);
   }

inline TABLEENTRY *Tbl_TYPE::lastentrybefore (Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return tbl_lastentrybefore ((DATA)key, &(start->table), fn);
   }

inline Tbl_DATA Tbl_TYPE::firstafter (Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return (Tbl_DATA)tbl_firstafter 
	((DATA)key, &(start->table), fn);
   }

inline Tbl_DATA Tbl_TYPE::lastbefore (Tbl_KEY key) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_find;
   return (Tbl_DATA)tbl_lastbefore 
	((DATA)key, &(start->table), fn);
   }

inline int Tbl_TYPE::check (
   FILE *afile,			/* File for output */
   char *(*idfn) (TABLE *) 	/* Function used to identify a node */
   ) const
   {
   int (*fn)(DATA, DATA) = (int (*)(DATA,DATA))start->cmp_insr;
   return tbl_check (afile, start->table, fn, idfn);
   }

inline int Tbl_TYPE::dumppreorder (
   FILE *afile,			/* File for output */
   char *(*idfn) (TABLE *) 	/* Function used to identify a node */
   ) const
   {
   return tbl_dumppreorder (afile, start->table, idfn);
   }

inline int Tbl_TYPE::dumpinorder (
   FILE *afile,			/* File for output */
   char *(*idfn) (TABLE *)	/* Function used to identify a node */
   ) const
   {
   return tbl_dumpinorder (afile, start->table, idfn);
   }

inline int Tbl_TYPE::freetable ()
   {
   int n = tbl_freetable (start->table);
   start->table = NULL;
   return n;
   }

//------------------------------------------------------------

// Define a macro for running through a table
#define Tbl_forall(type, x, atab) 	\
	tbl_forall(type, x, atab.tab())

#define Tbl_forinorder(type,x,atab)	\
	tbl_forinorder(type,x,atab.tab())

#define Tbl_forbackwards(type,x,y) 	\
	tbl_forbackwards(type,x,y.tab())

#define Tbl_forallentries(tbl_cursor,y)	\
	tbl_forallentries(tbl_cursor,y.tab())

#define Tbl_forpreorder(type,x,y) 	\
	tbl_forpreorder(type,x,y.tab())

#define Tbl_endall }}


//--------------------------------------------------------------
//  Undefine the table types
#undef Tbl_DATA
#undef Tbl_KEY
#undef Tbl_TYPE

// #endif
