#ifndef Pmap_stuff_h
#define Pmap_stuff_h

/* This is just a class for name-scope class -- all methods are static */

class Pmap
   {
   private :

      // DO NOT MAKE THESE PUBLIC !!!
      // These involve the Quaternion class.  You should use rhQuaternion instead

	   static rhMatrix QuaternionToMatrix(const rhVector &q);

	   static rhVector PvectorToQuaternion (const rhVector &v)
	    { return hom(v); }
   
	   static rhVector QuaternionToPvector (const rhVector &q)
	      { return dehom(q); }

      static rhVector MatrixToQuaternion (const rhMatrix R) 
         {
         return PvectorToQuaternion(MatrixToPvector(R));
         }

      static rhVector rhQuaternionToQuaternion (const rhVector p)
         {
         return rhVector (p[1], p[2], p[3], p[0]);
         }

      static rhVector QuaternionTorhQuaternion (const rhVector p)
         {
         return rhVector (p[3], p[0], p[1], p[2]);
         }

   public :

	   static rhVector hom (const rhVector &v);

	   static rhVector hom_theta2 (const rhVector &v);

	   static rhVector dehom (const rhVector &vhom);

	   static void normalize (const rhVector &v);

	   static rhVector MatrixToPvector (const rhMatrix &R);

	   static rhMatrix PvectorToMatrix (const rhVector &v);

	   static rhVector MatrixToAngleAxis (const rhMatrix &R);

	   static rhMatrix AngleAxisToMatrix (const rhVector &v);


      static double sinc (double x)
         {
         // Safe sinc function
         if (1.0 - x == 1.0) return 1.0;
         return sin(x) / x;
         }

      static rhVector MatrixTorhQuaternion (const rhMatrix R) 
         {
         // First get to quaternion in the sense used here
         rhVector q = MatrixToQuaternion(R);
         return QuaternionTorhQuaternion(q);
         }

      static rhVector PvectorTorhQuaternion (const rhVector p)
         {
         rhVector q = PvectorToQuaternion (p);
         return QuaternionTorhQuaternion(q);
         }

      static rhVector rhQuaternionToPvector (const rhVector p)
         {
         return QuaternionToPvector(rhQuaternionToQuaternion(p));
         }

      static rhMatrix rhQuaternionToMatrix (const rhVector p)
         {
         return QuaternionToMatrix(rhQuaternionToQuaternion(p));
         }
   };

#endif
