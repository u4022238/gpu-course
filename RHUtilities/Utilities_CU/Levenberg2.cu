// //
// This gives an object-oriented Levenberg-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector Levenberg2::function(const rhVector &);
//

#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/Levenberg2.h>

Levenberg2::Levenberg2 () : 
Num_Loops (100), 
Min_Loops (1), 
Min_Incr (1.0e-2)
   {}

Levenberg2::~Levenberg2 () 
   {}

//
// Keyboard interrupt stuff
//


rhBOOLEAN Levenberg2::interrupt_pending = FALSE;

#ifndef WIN32
void Levenberg2::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
      interrupt_pending = TRUE; 
   }
#endif

void Levenberg2::new_position_action ( rhVector )
   { /* Default is to do nothing */ }

double Levenberg2::solve ()
   {
   // Declare variables 
   double alamda = 1.0;
   double chisq = 0.0;
   double lastchisq;
   rhBOOLEAN improvement = TRUE;

   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ Levenberg2::keybd_interrupt);
#endif

   // Now iterate 
   for (int icount=1; icount<=Num_Loops && improvement; icount++)
      {
      double increment_ratio;

      // Compute the function value -- also its Jacobian and Hessian
      rhVector ft;
      rhMatrix ftt;
      chisq = function (mparam, &ft, &ftt);

      // Output some debugging info 
      if (icount == 1)
         informative_message ("\n\tInitial error : chisq = %g", chisq);

      // #define WRITE_JACOBIAN
#     ifdef WRITE_JACOBIAN
      fprintf (stdout, "\nHessian (%d x %d) : \n", ftt.idim(), ftt.jdim());
      ftt.print (stdout, "%15.7e ");
      fprintf (stdout, "\n J (%d x %d) : \n", ft.dim(), ft.dim());
      ft.print(stdout, "%15.7e ");
      fprintf (stdout, "\n val  = %-6e: \n", chisq);
#     endif

      // The update equation is ftt dx = -ft
      rhMatrix N = ftt;
      rhVector b = -ft;

      // Now estimate new position with different alambda until improvement 
      do {

         // Copy the values of alpha to aprime, the augmented alpha 
         rhMatrix aprime = N.copy();
         rhVector da = b.copy();

#ifdef TRY_TRACE
         // Just for now, try addition normalization
            {
            int i;
            double trace = 0.0;
            for (i=0; i<aprime.idim(); i++)
               trace += aprime[i][i];
            for (i=0; i<aprime.idim(); i++)
               aprime[i][i] += 1.0e-5;
            }
#endif

         // Augment diagonal elements of aprime 
         for (int j=aprime.ilow(); j<=aprime.ihigh(); j++)
            aprime[j][j] *= 1.0 + (alamda);

         // Matrix solution.  Solution overwrites beta, now called da 
         int ncols = aprime.jdim();
         if (! (lin_solve_symmetric_0(aprime, da, da, ncols)
            || solve_simple_0 (aprime, da, da, ncols, ncols)))
            {
            error_message ("Can not solve normal equations -- exiting");
            bail_out (2);
            }

         //----------------------------------------
         //  Calculate new error at this position
         //----------------------------------------
         rhVector newparams = mparam + da;

         // Get the value of chisq resulting from this error vector 
         double newchisq = function (newparams);

         // Compute the proportional change in the error vector
         increment_ratio = rhAbs((chisq-newchisq)/chisq);

         // Accept or reject it 
         if (newchisq < chisq)
            {
            // Success, accept this new solution 
            alamda *= 0.1;
            chisq = newchisq;

            // Accept the new parameter values 
            mparam = newparams;

            // Call any user function 
            new_position_action (mparam);

            // Signal improvement 
            improvement = TRUE;
            }

         else
            {
            // Failure, increas alamda and return. 
            alamda *= 10.0;
            improvement = FALSE;
            }

         } while ( ! improvement // && increment_ratio >= Min_Incr 
            && (alamda < 1e10));

      // Output some debugging info 
      informative_message ("\talamda = %5.0e, chisq = %g", alamda, chisq);

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if we are spinning wheels
      // This is a bit of a hack to avoid the problem that chisq is
      // worsened due to new_position_action and after iteration we do worse.
      if (icount  == 1 || chisq < lastchisq)
         lastchisq = chisq;
      else
         break;

      // Test to see if the increment_ratio has been too small 
      if (icount >= Min_Loops && (increment_ratio < Min_Incr || alamda >= 1e10))
         break;
      }

   // Return the value of the error 
   return chisq;
   }

