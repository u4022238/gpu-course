// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)List.cc	1.13 11/22/96 

//
// Module Purpose: Utility routines for operating upon lists.
//
// Creation Date: 5/7/87
//
// Class 3: General Electric Proprietary Data
// Copyright (C) 1987 General Electric Company
//

#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/List.h>

//
//  Facilities for allocating and freeing list entries
//
#define LE_INITSIZE 1024
#define LH_INITSIZE 64

// Pointers to the heads of the free space lists
List_Entry  *List_Entry ::headOfEntryList  = NULL;
List List_Entry::entryBlockList;

List_Header *List_Header::headOfHeaderList = NULL;
List List_Header::headerBlockList;

void *List_Entry::operator new (size_t size)
   {
   // Provides a new list entry from a list 

   // Pass to global if the entry is the wrong size
   if (size != sizeof (List_Entry)) return ::new char[size];

   // If no more entries, then get some
   if (headOfEntryList == NULL)
      {
      // Allocate some new memory
      List_Entry *newBlock = 
         (List_Entry *) new char [ LE_INITSIZE * sizeof (List_Entry)];

      // Link it together
      for (int i = 0; i<LE_INITSIZE-1; i++)
         newBlock[i].data = (rhObject *) &newBlock[i+1];

      // Terminate with a null pointer
      newBlock[LE_INITSIZE-1].data = 0;

      // Set p to the head of the list
      headOfEntryList = &newBlock[0];

      // Insert this block in the block list
      entryBlockList.insert (newBlock);
      }

   // Now get the entry off the head of the free list
   List_Entry *p = headOfEntryList;
   headOfEntryList =  (List_Entry *)p->data;
   return p;
   }

void List_Entry::operator delete (void *anentry, size_t size)
   {
   // Returns a list entry to the free list 

   // If the wrong size, then pass to global
   if (size != sizeof (List_Entry))
      {
      ::delete [] ((char *) anentry);
      return;
      }

   // Now return to free list
   List_Entry *temp = (List_Entry *)anentry;
   temp->data = (rhObject *) headOfEntryList;
   headOfEntryList = temp;
   }

rhBOOLEAN List_Entry::is_erasable ()
   {
   // Block is erasable if and only if all the List_Entries have 
   // NULL next and prev pointers
   List_Entry *block = this;

   // Link it together
   for (int i=0; i<LE_INITSIZE-1; i++)
      {
      if (block[i].data == (rhObject *)block) continue;
      if (block[i].next || block[i].prev)
         return FALSE;
      }

   // Must be erasable
   return TRUE;
   }

void List_Entry::delete_block ()
   {
   // Before deleting, we must remove them all from the Entry list

   List_Entry *block = this;

   // Mark for deletion from the entry list
   for (int i = 0; i<LE_INITSIZE; i++)
      block[i].next = block[i].prev = block;

   // Now, go through the entry list, removing them
   List_Entry **preventry = &headOfEntryList;
   for (List_Entry *p = headOfEntryList; p != NULL; p=(List_Entry *)(p->data))
      {
      // Erase those ones that point to block
      if (p->next == block && p->prev == block)
         {
         // Link it out of the list
         *preventry = (List_Entry *)(p->data);
         }

      // Skip over those that do not
      else
         {
         // Update the previous pointer
         preventry = (List_Entry **)&(p->data);
         }
      }

   // Now, erase the block
   delete [] block;
   }

void List_Entry::cleanup ()
   {
   // Frees all possible blocks of list entries

   // First, of all, mark all the entries in the free list
   List_Entry *p = headOfEntryList;
   while (p)
      {
      p->next = p->prev = NULL;
      p = (List_Entry *)(p->data);
      }

   // Now get the entry off the head of the free list
   for (List_Index index = entryBlockList.first_index(); index.valid(); )
      {
      // Get the block
      List_Entry *block = (List_Entry *) entryBlockList[index];

      // %p prints a pointer -- RIH 28/3/2012
      fprintf (stderr, "Block %p\n", (void *)block);

      // If it is erasable, then erase it
      if (block->is_erasable())
         {
         // Move the index along to the next value
         List_Index oldindex = index;
         ++index;

         // Erase the old index
         entryBlockList.remove(oldindex);

         // Free the block
         block->delete_block();
         }

      // Otherwise, just go on to the next one
      else ++index;
      }
   }

void *List_Header::operator new (size_t size)
   {
   // Provides a new list header from a list 

   // Pass to global if the entry is the wrong size
   if (size != sizeof (List_Header)) return ::new char[size];

   // If no more entries, then get some
   if (headOfHeaderList == NULL)
      {  
      // Allocate some new memory
      List_Header *newBlock =
         (List_Header *) new char [ LH_INITSIZE * sizeof (List_Header)];

      // Link it together
      for (int i = 0; i<LH_INITSIZE-1; i++)
         newBlock[i].next = (List_Header *) &newBlock[i+1];

      // Terminate with a null pointer
      newBlock[LH_INITSIZE-1].next = 0;

      // Set p to the head of the list
      headOfHeaderList = &newBlock[0];

      // Insert this block in the block list
      // This can not be done, since it is recursive.
      // headerBlockList.insert (newBlock);
      }  

   // Now get the entry off the head of the free list
   List_Header *p = headOfHeaderList;
   headOfHeaderList =  (List_Header *)p->next;
   return p;
   }

void List_Header::operator delete (void *anentry, size_t size)
   {
   // Returns a list header to the free list 

   // If the wrong size, then pass to global
   if (size != sizeof (List_Header))
      {
      ::delete [] ((char *) anentry);
      return;
      }

   // Now return to free list
   List_Header *temp = (List_Header *)anentry;
   temp->next = headOfHeaderList;
   headOfHeaderList = temp;
   }

rhBOOLEAN List_Header::is_erasable ()
   {
   // Block is erasable if and only if all the List_Entries have 
   // NULL next and prev pointers
   List_Header *block = this;

   // Link it together
   for (int i=0; i<LH_INITSIZE-1; i++)
      {
      if (block[i].next == block) continue;
      if (block[i].prev)
         return FALSE;
      }

   // Must be erasable
   return TRUE;
   }

void List_Header::delete_block ()
   {
   // Before deleting, we must remove them all from the Entry list

   List_Header *block = this;

   // Mark for deletion from the entry list
   for (int i = 0; i<LH_INITSIZE; i++)
      block[i].prev = block;

   // Now, go through the entry list, removing them
   List_Header **preventry = &headOfHeaderList;
   for (List_Header *p = headOfHeaderList; p != NULL;p=(List_Header *)(p->next))
      {
      // Erase those ones that point to block
      if (p->prev == block)
         {
         // Link it out of the list
         *preventry = (List_Header *)(p->next);
         }

      // Skip over those that do not
      else
         {
         // Update the previous pointer
         preventry = (List_Header **)&(p->next);
         }
      }

   // Now, erase the block
   delete [] block;
   }

void List_Header::cleanup ()
   {
   // Frees all possible blocks of list entries

   // First, of all, mark all the entries in the free list
   List_Header *p = headOfHeaderList;
   while (p)
      {
      p->prev = NULL;
      p = (List_Header *)(p->next);
      }

   // Now get the entry off the head of the free list
   for (List_Index index = headerBlockList.first_index(); index.valid(); )
      {
      // Get the block
      List_Header *block = (List_Header *) headerBlockList[index];

      // %p prints a pointer -- RIH 28/3/2012
      fprintf (stderr, "Block %p\n", (void *)block);

      // If it is erasable, then erase it
      if (block->is_erasable())
         {
         // Move the index along to the next value
         List_Index oldindex = index;
         ++index;

         fprintf (stderr, "Erasing ...");

         // Erase the old index
         headerBlockList.remove(oldindex);

         // Free the block
         block->delete_block();

         fprintf (stderr, "Done\n");
         }

      // Otherwise, just go on to the next one
      else ++index;
      }
   }


//>> List utilities follow <<

List_Index List::insertafter (void *dat, List_Index ap)
// Inserts a piece of data after the indicated position 
   {
   List_Entry *temp = new List_Entry;
   List_Node *aposition = ap.node();
   temp->data = (rhObject *)dat;
   temp->next = aposition->next;
   temp->prev = aposition;
   aposition->next = temp;
   temp->next->prev = temp;

   // Fix the length 
   length (length() + 1);

   return (List_Index(ap.list(), temp));
   }

List::List()
// Initialises a list 
   {
   // fprintf (stderr, "--- List() \n"); 
   List_Header *temp = new List_Header;
   temp->next = temp->prev = temp;
   start = temp;
   start->reference_count = 1;

   //fprintf (stderr, "--- List::List : list = %08x, start = %08x\n", 
   //	this, start);
   //fprintf (stderr, "\t reference count incremented to %d\n", refcount());

   length(0);
   }

List & List::joinafter (List lst2, List_Index pos)
// Joins the given list in at the given position.
// It leaves lst as an empty list 
   {
   List_Node *position = pos.node();

   // If the list being grafted in is empty, there is nothing to do 
   if (lst2.is_empty()) return *this;

   // If the two lists being joined are the same, then we must copy 
   //if (*this == lst2) lst2.List::operator=(lst2.copy());
   if (*this == lst2) 
      {
      List &newlst2 (lst2.copy());
      lst2 = newlst2;
      }

   List_Node *subseq = position->next;
   List_Node *last  = lst2.last_index().node();
   List_Node *first = lst2.first_index().node();

   // Graft the second list into the first 
   last->next = subseq;
   first->prev = position;
   position->next = first;
   subseq->prev = last;

   // Set the length of the list 
   length (length() + lst2.length());

   // Set the second list to an empty list 
   lst2.start->next = lst2.start->prev = lst2.start;
   lst2.length(0);

   // Return the list 
   return *this;
   }

List & List::operator * (List lst2)
// Join list 1 into the start of list 2 
   {
   return joinafter (lst2, last_index());
   }

List & List::remove (List_Index ind)
// Removes an entry from a list 
   {
   // Check to see that the entry is valid first -- we can not delete headers
   if (! ind.valid()) return (*this);

   // Relink the list
   List_Node *entry = ind.node();
   entry->next->prev = entry->prev;
   entry->prev->next = entry->next;

   // Delete the list entry
   delete (List_Entry *) entry;

   // Set the length of the list 
   length (length() - 1);

   // Return this pointer so that we can chain
   return (*this);
   }

List & List::remove (void *dat)
// Deletes the data from the list.  
// This is inefficient to use.
   {
   for ( List_Index i = first_index(); i.valid(); ++i)
      if ((*this)[i] == dat)
         {
         remove (i);
         break;
         }

      // Returns the list itself
      return *this;
   }

void * List::dequeue ()
// Dequeues an item from the queue and returns it 
   {
   List lst = *this;
   if (lst.is_empty()) return (NULL);
   List_Index first = lst.first_index();
   rhObject *dat = lst[first];
   lst.remove (first);
   return (dat);
   }

void * List::pop ()
// Dequeues an item from the queue and returns it 
   {
   List lst = *this;
   if (lst.is_empty()) return (NULL);
   List_Index first = lst.first_index();
   rhObject *dat = lst[first];
   lst.remove (first);
   return (dat);
   }

void List::dereference ()
// Frees the storage occupied by the list.
// Does not free the data in the list.
//
   {
   unlink();	// Unlink the list 

   //fprintf (stderr, "--- List::dereference : list = %08x, start = %08x\n", 
   // 	this, start);
   //fprintf (stderr, "\t reference count decremented to %d\n",
   //   refcount());

   // If there are no more references to this list, then unlink it 
   if (refcount() == 0)
      {
      // fprintf (stderr, "Deleting list of length %d.\n", length()); 
      List_Index i = first_index();
      while (i.valid())
         {
         List_Index ti = i.succ();
         delete (List_Entry *) i.node();
         i = ti;
         }

      // Get rid of the list header as well
      delete start;
      }
   }

//>> A bubblesort utility for lists <<
List & List::bubblesort (int (*cmp) (rhObject *, rhObject *))
   {
   // Move along the list until we get to the end 
   // At the end of each step, all nodes up to cursor will be sorted 
   List alist = *this;
   List_Index cursor = alist.first_index();

   for (; cursor.valid(); ++cursor)
      {  
      List_Index c1, c2; 	// Two runners 

      // Save the present data 
      rhObject *temp = alist[cursor];

      // Move backwards comparing 
      c1 = cursor.pred();
      c2 = cursor;

      while ( c1.valid() && (*cmp)(alist[c1], temp) > 0)
         {
         // If the data is out of order, then swap it 
         alist[c2] = alist[c1];
         c2 = c1;
         c1 = c1.pred();
         }

      // Now restore the current data 
      alist[c2] = temp;
      }

   // Return the list 
   return *this;
   }

List::List (const List & lst)
   {
   lst.start->reference_count ++;
   start = lst.start; 
   //fprintf (stderr, "--- List::List : list = %08x, start = %08x\n", 
   //	this, start);
   //fprintf (stderr, "\t reference count incremented to %d\n",
   //   refcount());
   }

#if 0
List::List (List lst)
   {
   lst.start->reference_count ++;
   start = lst.start; 
   //fprintf (stderr, "--- List::List : list = %08x, start = %08x\n", 
   //	this, start);
   //fprintf (stderr, "\t reference count incremented to %d\n",
   //   refcount());
   }
#endif

List & List::operator = (const List &lst)
   {
   // Assign one list to another.  Check the reference counts to free
   // strings that are no longer referenced 

   // fprintf (stderr, "--- operator = (List lst)\n"); 
   lst.start->reference_count ++;

   //fprintf (stderr,"--- List::operator = : list = %08x, start = %08x\n",
   //	&lst, lst.start);
   //fprintf (stderr, "\t reference count incremented to %d\n",
   //    lst.refcount());

   // Delete the list previously referenced 
   dereference();

   start = lst.start; 
   return *this;
   }

List &List::copy () const
   {
   List *newlist = new List;

   // Run down the list copying it 
   for (List_Index i = first_index(); i.valid(); ++i)
      newlist->insert_end ((*this)[i]);

   // Return the new list 
   return *newlist;
   }

void List::print(FILE* s) const
   {
   for (List_Index i = first_index(); i.valid(); ++i)
      {
      rhObject* item = (*this)[i];
      if (item) item->print(s);
      else fputs("(NULL)", s);

      // Put a separator between lines 
      fprintf (s, "\n");
      }
   }


