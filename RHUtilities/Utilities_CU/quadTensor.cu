// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


//-----------------------------------------
//
//   The Trigs Quadrifocal Tensor
//
//-----------------------------------------

// #include <stdio.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_CU/quadTensor.h>

void quadTensor::dereference ()
   {
   if (Val != NULL)
      {  
      // First unlink the thing
      *reference_count -= 1;

      // Now, if reference count has fallen to zero, then free
      if (*reference_count <= 0)
         {
         deallocate();
         delete reference_count;
         }
      }   

   // Set Val and reference count to initial values
   Val = (qptr) 0;
   reference_count = (unsigned int *)0;
   }

// Constructors
quadTensor::quadTensor ()	
   { 
   Val = (qptr ) 0; 
   reference_count = (unsigned int *)0;
   Init();
   }

quadTensor::quadTensor (const quadTensor &A)
   {
   // Copy constructor
   Val = (qptr) 0;
   *this = A;
   }

quadTensor::quadTensor (const rhVector &q)
   {
   // Copy constructor
   Val = (qptr) 0;
   reference_count = (unsigned int *)0;
   Init ();

   // Check the dimensions of q
   if (q.dim() < 81)
      {
      error_message ("Bad size vector (%d) in quadTensor initialization", 
	 q.dim());
      bail_out(2);
      }

   // Fill with values
   int count = 0;
   int i, j, k, l;
   for_quadindex (i, j, k, l)
      Val[i][j][k][l] = q[count++];
   }

void quadTensor::Init ()
   {
   // First, dereference this array
   dereference ();

   // Now reallocate
   allocate ();

   // Also the reference count
   reference_count = new unsigned int;
   *reference_count = 1;
   }

// Assignment operator
quadTensor &quadTensor::operator = (const quadTensor &avec)
   {
   // Increment the reference count
   if (avec.Val)
      *(avec.reference_count) += 1;
 
   // Dereference the current list
   dereference ();
 
   // Copy the fields
   reference_count = avec.reference_count;
   Val = avec.Val;
 
   // Return the pointer itself
   return *this;
   }

// Other actions
void quadTensor::clear () const
   { 
   int i, j, k, l;
   for_quadindex (i, j, k, l)
      Val[i][j][k][l] = 0.0;
   }

void quadTensor::clear (double val) const
   { 
   int i, j, k, l;
   for_quadindex (i, j, k, l)
      Val[i][j][k][l] = val;
   }

quadTensor quadTensor::copy () const
   {
   // Copy the array including the elements
   const quadTensor &A = *this;
   quadTensor B;

   // Copy the data
   int i, j, k, l;
   for_quadindex (i, j, k, l)
      B[i][j][k][l] = A[i][j][k][l];

   return B;
   }

void quadTensor::print (FILE *afile, char *format)
   {
   int i, j, k, l;
   quadTensor &A = *this;

   for (i=0; i<3; i++)
     for (j=0; j<3; j++)
	{
  	fprintf (afile, "Component (%d, %d) :\n", i, j);
	for (k=0; k<3; k++)
	   {
	   for (l=0; l<3; l++)
	      fprintf (afile, format, A[i][j][k][l]);
	   fprintf (afile, "\n");
	   }
	}
   }

void quadTensor::allocate()
   {
   // Allocates a quad tensor
   int low = 0;
   int dim = 3;
   int nptrs = dim + dim*dim + dim*dim*dim;
   int nentries = dim*dim*dim*dim;
   unsigned int entry_size = sizeof (double);

   //
   // We must make sure that double words are allocated on double word
   // boundaries
   // 

   // We must make sure that we have an even number of pointers 
   if (entry_size > sizeof(void *) && nptrs % 2 != 0)
      nptrs += 1;

   // Now, compute the size of space to be allocated 
   int size = nptrs*sizeof(void *) + nentries*entry_size;

   // Allocate m 
   void **m = (void **) MALLOC(size);

   // Start of the data area 
   double *start = (double *) (m + nptrs);   // Count forward nptrs (MTYPE *) 

   // Starts of the different levels of addresses 
   void **pt1 = (void **) m;
   void **pt2 = (void **) (m + dim);
   void **pt3 = (void **) (m + dim + dim*dim);

   // Fill out the addresses 
   int i;
   for (i=0; i<dim; i++)
      pt1[i] = &(pt2[i*dim-low]);

   for (i=0; i<dim*dim; i++)
      pt2[i] = &(pt3[i*dim-low]);

   for (i=0; i<= dim*dim*dim; i++)
      pt3[i] = &(start[i*dim-low]);
   
   // Return pointer to the pointer array 
   Val = (qptr) (m-low);
   }

