#ifndef _Matrix2_cc_dcl_
#define _Matrix2_cc_dcl_
#include "Utilities_cuda/cvar.h"


FUNCTION_DECL (void QRdecomposition, (
	const rhMatrix A, const rhMatrix &K, const rhMatrix &R));

FUNCTION_DECL (int svd, (
	const rhMatrix &A, rhMatrix &U, rhVector &D, rhMatrix& V));

FUNCTION_DECL (int svd, (
	const rhMatrix &A, rhVector &D, rhMatrix& V));

FUNCTION_DECL (int svd, (
	const rhMatrix &A, rhMatrix &U, rhVector &D));

FUNCTION_DECL (int svd, (
	const rhMatrix &A, rhVector &D));

FUNCTION_DECL (int svd_rank, (rhVector D));

FUNCTION_DECL (void jacobi, (
	const rhMatrix &A, rhVector &D, rhMatrix& V));

FUNCTION_DECL (int choleski, (const rhMatrix &A, const rhMatrix &C));

FUNCTION_DECL (rhMatrix householder_rotation, (rhVector x, int n));

FUNCTION_DECL (rhMatrix householder_rotation, (rhVector x));

FUNCTION_DECL (rhMatrix householder_matrix, (rhVector x, int n));

FUNCTION_DECL (rhMatrix householder_matrix, (rhVector x));

FUNCTION_DECL (rhMatrix orthogonal_row_reduce, (rhMatrix M));

FUNCTION_DECL (rhMatrix orthogonal_row_reduce, (rhMatrix M, int rank));

FUNCTION_DECL (rhMatrix rhMtemp, (int idim, int jdim, double **vals));

FUNCTION_DECL (rhVector rhVtemp, (int idim, double *vals));

#endif
