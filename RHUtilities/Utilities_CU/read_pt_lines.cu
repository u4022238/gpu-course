// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)read_pt_lines.cc	1.3 01 Oct 1995

//
// General purpose program for reading input files
//
//

// Caution : All arrays start at 1, except those indexed by view-number,
//           which start at 0.  (Illogical isn't it).

#include <Utilities_CU/utilities_CC.h>
#include <Utilities_cuda/cvar.h>

FUNCTION_DEF (int read_standard_input_file, (
   FILE *afile,			/* The file to read from */
   rhVector *&u,		/* U coordinates to be allocated and read */
   rhVector *&v,		/* V coordinates to be allocated and read */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates to be allocated and read */
   Boolean_matrix &defined	/* A matrix telling which points are defined */
   ))
   {
   // Reads the standard input file
   int i, j;
   int n;		// Number of units read

   // Get the number of points
   int numviews, numpoints;
   if ((n = fscanf (afile, "%d %d", &numviews, &numpoints)) != 2)
      {
      error_message ("Error reading input data file \n");
      return 0;
      }

   // Size the array of defined values
   defined.Init (0, numviews-1, 1, numpoints);
   for (i=0; i<numviews; i++) for (j=0; j<numpoints; j++)
      defined[i][j] = FALSE;

   // Allocate the uv coordinate arrays
   u = GVECTOR (0, numviews-1, rhVector);
   v = GVECTOR (0, numviews-1, rhVector);

   // Allocate space
   for (i=0; i<numviews; i++)
      {  
      u[i].Init(numpoints);
      v[i].Init(numpoints);
      }  

   // Initialize the point arrays as well
   x.Init(numpoints);
   y.Init(numpoints);
   z.Init(numpoints);
 
   // First get the points
   for (i=0; i<numpoints; i++)
      if ((n = fscanf (afile, "%lf %lf %lf", &(x[i]), &(y[i]), &(z[i]))) != 3)
	 {
	 // Here we try parsing the word "VOID"
	 // This is to allow one to miss the points, by putting an alphanumeric

	 // Check to see that the next 4 characters are "VOID"
   	 char c[5];
	 if ((n = fscanf(afile, "%4c", c)) != 1 || strncmp(c,"VOID",4) != 0)
	    {	
	    error_message ("Error reading 3D points from input file");
	    return 0;
	    }
	 else
	    {
 	    // Fill with zeros
	    for (;i<numpoints; i++)
	       x[i] = y[i] = z[i] = 0.0;
	    }
	 }

   // Now, get the control points
   while (1)
      {
      int vw, pt;   // View and point number
 
      // Get the view and point number -- stop on EOF
      n = fscanf (afile, "%d %d", &vw, &pt);
      if (n == EOF) break;
      if (n != 2)
	 {
	 error_message ("In input file, error reading control points");
	 return 0;
	 }

      // Check that they are in range

      // First check the view number
      if (vw >= numviews || vw < 0) 
	 {
	 error_message ("In input file, view number %d out of range (%d, %d)",
		vw, 0, numviews-1);
	 return 0;
  	 }

      // Second check the point number
      if (pt > numpoints || pt < 1)
	 {
	 error_message ("In input file, point number %d out of range (%d, %d)",
		pt, 1, numpoints);
	 return 0;
	 }
   
      // Get the control point
      if ((n = fscanf (afile, "%lf %lf", &(u[vw][pt]), &(v[vw][pt]))) != 2)
	 {
	 error_message ("In input file, error reading control points");
	 return 0;
	 }
 
      // Mark as defined
      defined[vw][pt] = TRUE;
      }

   // Return success
   return 1;
   }

FUNCTION_DEF (int select_points_from_specified_views, (

   /* Input */
   rhVector *&u,		/* U coordinates of all points in all views */
   rhVector *&v,		/* V coordinates of all points in all views */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates of all points */
   Boolean_matrix &defined,	/* A matrix telling which points are defined */
   Int_vector &selected_view,	/* A selected set of views */

   /* Output */
   rhVector *&uout,		/* U coordinates to be selected from u */
   rhVector *&vout,		/* V coordinates to be selected from v */
   rhVector &xout, rhVector &yout, 
   rhVector &zout,	/* Point coordinates to be selected from x, y, z */
   Int_vector &selected_point	/* The points that were selected */

   ))
   {
   //
   // This routine takes only the selected views and selects only
   // those points that belong to all views
   //
   int i, j;
   int nsviews = selected_view.dim();
   int numpoints = defined.jdim();
   int numviews = defined.idim();
  
   // Check that all the selected views are in the range
   for_1Dindex (i, selected_view)
      if (selected_view[i] < 0 || selected_view[i] >= numviews)
	 {
	 error_message ("Selected view number (%d) out of range (%d, %d)\n",
	   	selected_view[i], 0, numviews-1);
	 return 0;
	 }

   // Next, we count the number of points that there will be in
   // the selected views

   // Parameters to count and store the selected point numbers
   int count = 0;
   Int_vector t_selected_point (0, numpoints-1);

   // Run through all points seeing if they qualify
   for (i=0; i<numpoints; i++)
      {
      int point_is_good = TRUE;

      // See if this given point is defined for all the selected points
      for_1Dindex (j, selected_view)
	 {
	 // If not seen in j-th selected view, then skip this point
	 if (! defined[selected_view[j]][i]) 
	    {
	    point_is_good = FALSE;
            break;
	    }
	 }

      // If the point is good, then we skip it
      if (! point_is_good) continue;

      // At this point the point is visible in all the selected views
      // So store the point number in the array
      t_selected_point[++count] = i;
      }

   // Now, allocate the real selected point list, and copy
   selected_point.Init(1, count);
   for_1Dindex (i, selected_point)
      selected_point[i] = t_selected_point[i];

   // Now, we can also allocate the arrays for u, v, etc

   // First, uout and vout
   uout = GVECTOR (0, nsviews-1, rhVector);
   vout = GVECTOR (0, nsviews-1, rhVector);
   for (i=0; i<nsviews; i++)
      {
      uout[i].Init(count);
      vout[i].Init(count);
      }

   // Next, xout, yout, zout
   xout.Init(count);
   yout.Init(count);
   zout.Init(count);

   // Fill these arrays out
   for_1Dindex (j, selected_point)
      {
      // First, fill out the image points
      for_1Dindex (i, selected_view)
         {
	 uout[i][j] = u[selected_view[i]][selected_point[j]];
	 vout[i][j] = v[selected_view[i]][selected_point[j]];
	 }

      // Second, fill out the 3D points
      xout[j] = x[selected_point[j]];
      yout[j] = y[selected_point[j]];
      zout[j] = z[selected_point[j]];
      }

   // Good return value
   return 1;
   }

FUNCTION_DEF (int select_points_from_specified_views_replace, (

   /* Input */
   rhVector *&u,		/* U coordinates of all points in all views */
   rhVector *&v,		/* V coordinates of all points in all views */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates of all points */
   Boolean_matrix &defined,	/* A matrix telling which points are defined */
   Int_vector &selected_view,	/* A selected set of views */
   Int_vector &selected_point	/* The points that were selected (returned) */
   )) 
   {
   //
   // This routine takes only the selected views and selects only
   // those points that belong to all views.  It replaces the input data
   //

   // Define some temporary use variables
   rhVector *usel, *vsel, xsel, ysel, zsel;

   // Call the main selection procedure
   int n = select_points_from_specified_views ( u, v, x, y, z, 
	defined, selected_view, usel, vsel, xsel, ysel, zsel, selected_point);
   if (! n) return (n);
 
   // Now, Replace the points by the selected points only
   GVFREE (u, 0);
   u = usel;
   GVFREE (v, 0);
   v = vsel;
   x = xsel; y = ysel; z = zsel;
 
   // Get the number of views and points
   int numviews = selected_view.dim();
   int numpoints = selected_point.dim();
 
   // Mark all the points as defined
   defined.Init(0, numviews-1, 0, numpoints-1);
   defined.clear(1);

   // Return success
   return 1;
   }

FUNCTION_DEF (int write_standard_input_file, (
   FILE *afile,			/* The file to read from */
   rhVector *&u,		/* U coordinates to be allocated and read */
   rhVector *&v,		/* V coordinates to be allocated and read */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates to be allocated and read */
   Boolean_matrix &defined	/* A matrix telling which points are defined */
   ))
   {
   // Writes the standard input file
   int i, j;

   // Size of the data set
   int numviews = defined.idim();
   int numpoints = defined.jdim();

   // Write the size of the data set
   fprintf (afile, "%d %d\n", numviews, numpoints);

   // Now the points in space
   for_1Dindex (i, x)
      fprintf (afile, "%23.15e %23.15e %23.15e\n", x[i], y[i], z[i]);

   // Now each point
   for_2Dindex (i, j, defined)
      {
      // Skip over the undefined points
      if (!defined[i][j]) continue;

      // Now print out the point
      fprintf (afile, "%4d %4d %23.15e %23.15e\n", i, j, u[i][j], v[i][j]);
      }

   // Return val
   return 1;
   }

