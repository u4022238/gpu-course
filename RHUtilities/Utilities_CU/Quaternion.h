#ifndef _RH_QUATERNION_H__
#define _RH_QUATERNION_H__

#include "Utilities_CU/utilities_CC.h"

class rhQuaternion : public rhVector
   {
   public :

      // Constructor
      rhQuaternion () : rhVector (4) {}

      // Constructor, given the three points
      rhQuaternion (double x, double y, double z, double t) : rhVector (4)
            { 
            (*this)[0] = x; 
            (*this)[1] = y; 
            (*this)[2] = z; 
            (*this)[3] = t;
            }

      rhQuaternion (double re, const rhVector &im) : rhVector (4)
            {
            (*this)[0] = re; 
            (*this)[1] = im[0];
            (*this)[2] = im[1];
            (*this)[3] = im[2];
            }

      // Constructor given a vector
      rhQuaternion (const rhVector &v) : rhVector (v) {}

      // Get the real and imaginary parts
      double   re() { return (*this)[0]; }
      rhVector im() 
         { 
         return rhVector ((*this)[1], (*this)[2], (*this)[4]); 
         }

      rhQuaternion operator % (const rhQuaternion &q2)
         {
         // Does quaternion multiplication
         rhQuaternion &q1 = *this;
         double c = q1[0]*q2[0] - q1[1]*q2[1] - q1[2]*q2[2] - q1[3]*q2[3];
         double i = q1[0]*q2[1] + q1[1]*q2[0] + q1[2]*q2[3] - q1[3]*q2[2];
         double j = q1[0]*q2[2] + q1[2]*q2[0] + q1[3]*q2[1] - q1[1]*q2[3];
         double k = q1[0]*q2[3] + q1[3]*q2[0] + q1[1]*q2[2] - q1[2]*q2[1];
         return rhQuaternion (c, i, j, k);
         }

      rhQuaternion inverse()
        {
        // printf ("In Quaternion::inverse()\n");
        rhQuaternion &q = *this;
        double normsq = q*q;
        return rhQuaternion (q[0], -q[1], -q[2], -q[3]) / normsq;
        }

   };

class rhUnitQuaternion : public rhQuaternion
   {
   public:

      // Empty constructor
      rhUnitQuaternion () : rhQuaternion() {}

      // Constructor, given the four points
      rhUnitQuaternion (double x, double y, double z, double t) : 
      rhQuaternion (x, y, z, t)
         { }

      // Constructor given a vector
      rhUnitQuaternion (const rhVector &v) : rhQuaternion (v) {}

      rhUnitQuaternion inverse()
         {
         // printf ("In UnitQuaternion::inverse()\n");
         rhUnitQuaternion &q = *this;
         return rhUnitQuaternion (q[0], -q[1], -q[2], -q[3]);
         }

     rhUnitQuaternion operator % (const rhUnitQuaternion &q2)
        {
        rhUnitQuaternion &q1 = *this;
        double c = q1[0]*q2[0] - q1[1]*q2[1] - q1[2]*q2[2] - q1[3]*q2[3];
        double i = q1[0]*q2[1] + q1[1]*q2[0] + q1[2]*q2[3] - q1[3]*q2[2];
        double j = q1[0]*q2[2] + q1[2]*q2[0] + q1[3]*q2[1] - q1[1]*q2[3];
        double k = q1[0]*q2[3] + q1[3]*q2[0] + q1[1]*q2[2] - q1[2]*q2[1];
        return rhUnitQuaternion (c, i, j, k);
        }
   };

#endif
