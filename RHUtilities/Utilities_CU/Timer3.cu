
/* SFILE_BEGIN */
#include <stdio.h>
/* SFILE_END */

// Include files for time information
// #include <time.h>
#include <ctime>
#include <chrono>

#include <Utilities_cuda/cvar.h>
#include <Utilities_CU/Timer3.h>
#include <Utilities_cuda/showtime_dcl.h>
#include <string.h>

int scmp (TimingEvent3 *a, TimingEvent3 *b)
   {
   // Compare two wordentries
   char *wa = a->name_;
   char *wb = b->name_;
   return strcmp (wa, wb);
   }

int sfind (char *wa, TimingEvent3 *b)
   {
   // Compare a char * with a TimingEvent3
   char *wb = b->name_;
   return strcmp (wa, wb);
   }


//---------------------------------------------------------

// Static members
bool  rhTimer3::timing_on = false;
TimeTable3 rhTimer3::ttab(scmp, sfind);

rhTimer3::rhTimer3 (const char *context)
   {
   // Turn on/off the messages temporarily during scope

   // Only do if timing is on
   if (!timing_on) return;

   // Record the context name
   strcpy (cstring_, context);

   // Get the current processor time and current wall time
   double ptime = ((double) clock()) / CLOCKS_PER_SEC;
   ChronoTimePoint wtime = std::chrono::high_resolution_clock::now();

   // Find an event in the table and record
   // First, see if this is known
   TimingEvent3 *ev = (TimingEvent3 *)ttab.find(context);
      
   // Unknown event
   if (!ev)
      {
      // Now create an event struct and insert
      ev = new TimingEvent3 (context);
      ttab.insert(ev);
      }

   // Update the depth of the event
   // Update the depth
   ev->depth_++;
   ev->numcalls_++;

   // Only record time, first time in
   if (ev->depth_ == 1) 
      {
      ev->event_processor_time_ = ptime;
      ev->event_wall_time_ = wtime;
      }
   }

rhTimer3::~rhTimer3 ()
   {
   // Only do if timing is on
   if (!timing_on) return;

   // Now write the message

   // Get the processor and wall times
   double ptime = ((double) clock()) / CLOCKS_PER_SEC;
   ChronoTimePoint wtime = std::chrono::high_resolution_clock::now();

   // Find an event in the table and record
   // First, see if this is known
   char *context = cstring_;
   TimingEvent3 *ev = (TimingEvent3 *)ttab.find(context);
      
   // Unknown event
   if (!ev)
      {
      // This is a mistake
      error_message("rhTimer3: Attempt to finish unknown event");
      return;
      }

   // Now, handle the event

   // End event
   ev->depth_--;

   // If depth is zero, then store elapsed time
   if (ev->depth_ == 0)
      {
      // Compute the used time
      double elapsed_processor_time = 
         (ptime - ev->event_processor_time_);// /CLOCKS_PER_SEC;

      double elapsed_wall_time = 
         //(wtime - ev->event_wall_time_);

      std::chrono::duration<double, std::milli>
         (wtime - ev->event_wall_time_).count();

      // Store the time
      ev->total_processor_time_ += elapsed_processor_time;
      ev->total_wall_time_ += elapsed_wall_time;
      }

   // But if it is less than zero, there is a mistake
   if (ev->depth_ < 0)
      {
      error_message ("rhTimer3: Too many ENDS for event \"%s\"\n", context);
      return;
      }
   }

void rhTimer3::print_results()
   {
   // Now, print out the table

   // Do not do anything if timing is not on
   if (! timing_on) return;

   printf ("\n-------------------------------------------------------------------\n");
   printf ("Timing results from rhTimer3\n");
   Tbl_forall (TimingEvent3 *, ev, ttab)
      {
      double total_processor_time = ev->total_processor_time_;
      double total_wall_time = ev->total_wall_time_ / 1000.0;
      long ncalls = ev->numcalls_;
      double avg_time = total_processor_time / ncalls;
      printf ("calls %7d : total %8.3f (%8.3f) : avg %8.2e : %s\n", 
           ncalls, total_processor_time, total_wall_time, avg_time, ev->name_);
      } Tbl_endall
   printf ("-------------------------------------------------------------------\n");
   }

//---------------------------------------------------------------
