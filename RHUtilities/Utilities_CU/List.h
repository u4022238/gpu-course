// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

/* @(#)List.h	1.14 12/03/96 */
/* Include file for use with lists */
#ifndef List_h
#define List_h

#include <stdio.h>
#include <Utilities_CU/rhObject.h>
#include <Utilities_CU/utilities_CC.h>

inline void subrange_error() 
   {
   fprintf (stderr, "Subrange error.\n"); 
   bail_out (2); 
   }

class List_Index
   {
   private :

      class List_Node *_node;
      const class List *_list;
 
   public :

      List_Index ();
      List_Index (const class List *s, List_Node *p);
      List_Index (class List *s);
      List_Index operator ++ ();
      List_Index operator -- ();
      List_Index succ () const;
      List_Index pred () const;
      class List_Node *node() const { return _node; }
      class List const *list() const { return _list; }
      int valid () const;
      void print() const;
      void print (FILE *) const;

   };

class List_Node
   {
   friend class List;
   friend class List_Index;
   protected :
	class List_Node *next, *prev;
   public :
  	void print() const;
	void print(FILE *) const;
   };

class List_Header : public List_Node
   {
   friend class List;

   private :
	static List_Header *headOfHeaderList;
	static List headerBlockList;
	int reference_count;	/* How many aliases for this list */
	int length;		/* Number of elements in the list */

   public :
	void *operator new (size_t size);
        void operator delete (void *deadObject, size_t size);

	// For cleaning up allocated blocks
	rhBOOLEAN is_erasable();
	void delete_block();
	static void cleanup ();
   };

class List_Entry : public List_Node
   {
   friend class List;

   private :
	static List_Entry *headOfEntryList;
	static List entryBlockList;
	rhObject *data;		/* A pointer to the actual data */

   public :
	void *operator new (size_t size);
        void operator delete (void *deadObject, size_t size);

	// For cleaning up allocated blocks
	rhBOOLEAN is_erasable();
	void delete_block();
	static void cleanup ();
   };

class List 
   {
   friend class List_Index;

   private :

	class List_Header *start;

	void length (const int n) { start->length = n; }

	void dereference ();

   public :

	List();

	// List (List);

	List (const List &);

	~List () { dereference();}

	List_Index insertafter (void *dat, List_Index i);

	List_Index insert_end (void *dat);

	List_Index insert (void *dat);

	List_Index insert_start (void *dat);

	List &joinafter (List lst, List_Index position);

     /* Changed lst2 -> l2 because of windows rubbish. */

	List & operator + (List l2);	/* Copy & Join the lists */

	List & operator * (List l2); 	/* Concat the lists */

	int operator == (List l2) const { return start == l2.start; }

	List &operator = (const List &lst); /* Assign list - free other first */

	List &remove (class List_Index entry);

	List &remove (void * dat);

	void clear();

  	List &copy () const;

	void * pop ();

	void push (void *dat);

	void * dequeue ();

	List_Index first_index () const;

        List_Index last_index () const;

	int length () const ;

	List &bubblesort (int (*cmp)(rhObject *, rhObject *));

	int is_empty() const { return start == NULL || start->length == 0; }

	void unlink () const { start->reference_count--; }

	int refcount () const { return start->reference_count; }

	rhObject * &operator [] (List_Index i) const
		{ 
		if (i.node() == start) subrange_error ();
		return ((List_Entry *)(i.node()))->data;
		}

   	void print (FILE *f = stdout) const;

   };


//
// Inline members for List_Node class.
//

inline void List_Node::print (FILE *afile) const
   { 
   // The following commented code assumes 32 bit pointers.  It is replaced
   // by code that is tested on Mac with gcc only -- RIH 28/3/2012
   // fprintf (afile, "List_Node %08x : { next = %08x, prev = %08x }\n", 
   //	(int)this, (int)next, (int)prev); 
   fprintf (afile, "List_Node %p : { next = %p, prev = %p }\n", 
	(void *)this, (void *)next, (void *)prev); 
   }

inline void List_Node::print () const
   {
   print (stdout);
   }

//
// Inline members for List_Index class.
//

inline int List_Index::valid () const
   { 
   return _list && _node != _list->start; 
   }

inline List_Index List_Index::operator ++ () 
   { 
   _node = _node->next; return *this; 
   }

inline List_Index List_Index::operator -- () 
   { 
   _node = _node->prev; return *this; 
   }

inline List_Index::List_Index ()
   { 
   _list = NULL;
   _node = NULL;
   }

inline List_Index::List_Index (const class List *s, List_Node *p) 
   { 
   _node = p; 
   _list = s; 
   }

inline List_Index::List_Index (class List *s)
   { 
   _list = s; 
   _node = _list->start; 
   }

inline List_Index List_Index::succ () const
   {
   return List_Index (_list, _node->next);
   }

inline List_Index List_Index::pred () const
   {
   return List_Index (_list, _node->prev);
   }

inline void List_Index::print (FILE *afile) const
   {
   // The following commented code assumes 32 bit pointers.  It is replaced
   // by code that is tested on Mac with gcc only -- RIH 28/3/2012
   // fprintf (afile, "List_Index %08x { _node = %08x, _list = %08x }\n",
   //	(int)this, (int)_node, (int)_list);
   fprintf (afile, "List_Index %p { _node = %p, _list = %p }\n",
      (void *)this, (void *)_node, (void *)_list);
   _node->print(afile);
   }

inline void List_Index::print () const
   {
   print (stdout);
   }


// 
// Inline members for List class.
//

inline List_Index List::first_index () const
   { 
   return List_Index (this, start->next); 
   }

inline List_Index List::last_index () const
   { 
   return List_Index (this, start->prev); 
   }

inline List_Index List::insert_end (void *dat)
   {
   return insertafter (dat, last_index());
   }

inline List_Index List::insert (void *dat)
   {
   return insertafter (dat, last_index());
   }

inline List_Index List::insert_start (void *dat)
   {
   return insertafter (dat, --first_index());
   }

inline void List::push (void *dat)
   {
   (void) insertafter (dat, --first_index());
   }

inline int List::length() const
   {
   return start->length;
   }

inline void List::clear ()
   {
   // Clear out the whole list
   while ( ! is_empty()) remove(first_index());
   }

// Define a macro for running through a list
#define Lst_forall(type, x, list) 					  \
   {									  \
   List_Index lst_index;						  \
   for (lst_index = (list).first_index(); lst_index.valid(); ++lst_index) \
      {									  \
      type x = (type) ((list)[lst_index]);				

#define Lst_endall }}

#endif
