#include <vector>
using namespace std;
#include <Utilities_CU/SmartPointer.h>
#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/KDTree.h>

class KDOrder
   {
   private :

      static vector<rhFloatVector> *points_;
      static int dim_;

   public :

      static int order_cmp (const void *a, const void *b);
      static void set_points( vector<rhFloatVector> &p) {points_ = &p; }
      static void set_dim (int dim) {dim_ = dim; }
   };

vector<rhFloatVector> *KDOrder::points_;
int KDOrder::dim_;

int KDOrder::order_cmp (const void *a, const void *b)
   {
   // Order according to the value of the designated point in that dimension
   int i1 = *((int *)a);
   int i2 = *((int *)b);

   // Get the two coordinates
   double val1 = (*points_)[i1][dim_];
   double val2 = (*points_)[i2][dim_];

   // Now order
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return 0;
   }

static int power2 (int n)
   {
   // Computes largest power of 2 less than or equal to n
   int pow = 1;
   while (1)
      {
      n >>=1;
      if (n == 0) break;
      pow <<=1;
      }
   return pow;
   }

static int split (int n)
   {
   // Given a left-balanced tree with n leaf nodes, determine the
   // number of nodes that lie in the left branch.
   // A left-balanced tree is one in which the depth of the left branch
   // at any node is equal to the depth of the right branch, or exceeds it
   // by one.  Such a tree can be easily represented by an array such that
   // the children of node n are at 2n and 2n+1 (starting at 1).

   int m = power2(n);
   int remainder = n - m;
   int halfm = m>>1;
   int left = halfm + rhMin (remainder, halfm);
   return left;
   }

double KDPriorityQueueEntry::keyval () 
   { 
   return distance_to_goal_; 
   }

// Constructors, etc
KDTree::KDTree (vector<rhFloatVector> &vecs, bool build) : 
      points_(vecs), 
      node_(NULL),
      tree_(NULL),
      priority_queue_(NULL),

      // Reset the statistics
      points_evaluated_(0),
      nodes_scheduled_(0),
      points_found_ (0),
      max_trials_ (-1)	// Default is to find absolute optimum

   {
   // We need to build the tree here
   int i;

   // Number of points
   int npoints = points_.size();
   point_dimension_ = points_[0].dim();

   // Set up the KDTree node array
   int nnodes = 2*npoints;

   // Set up the node positions, etc
   first_node_ = 1;
   last_node_ = nnodes-1;
   last_nonleaf_node_ = npoints-1;

   if (build)
      {
      node_ = new KDNode[nnodes];
      tree_ = &node_;

      // Set up the array for point ordering
      Int_vector pointorder (npoints);
      for_1Dindex (i, pointorder)
         pointorder[i] = i;

      // Build the tree
      BuildKDTree (points_, node_, first_node_, pointorder, 0, npoints-1);
      }
   }

KDTree::~KDTree ()
   {
   if (priority_queue_) delete priority_queue_;
   if (node_) delete [] node_;
   }

void KDTree::BuildKDTree (
   vector<rhFloatVector> &points,
   KDNode *tree,
   int node, 
   Int_vector pointorder, 
   int startpoint, 
   int endpoint
   )
   {
   // This is a recursive procedure that assigns the values of 
   // coord_number_ and boundary_ to the node in the KDTree,
   // and also sets the value of the node if it is a leaf

   // Determine the number of nodes
   int i, j;
   int npoints = endpoint - startpoint + 1;

   // See if only one point remains
   if (npoints == 1)
      {
      tree[node].pointnumber_ = pointorder[startpoint];
      return;
      }

   // This is not a leaf node, so we must split it

   // First of all, determine which dimension to split, based on the
   // variance of all the dimensions.

   rhVector sum (point_dimension_);
   rhVector sumsq (point_dimension_);
   sum.clear(0.0);
   sumsq.clear(0.0);

   // Sum the points for each coordinate position
   for (i=startpoint; i<=endpoint; i++)
      {
      for_1Dindex (j, sum)
         {
         register double vj = points[pointorder[i]][j];
         sum[j] += vj;
         sumsq[j] += vj*vj;
         }
      }

   // Now compute the best coordinate -- the one with greatest variance
   double maxvariance = 0.0;
   int maxdimension = 0;
   for_1Dindex (j, sum)
      {
      // Compute the variance
      double mean = sum[j] / npoints;
      double var = sumsq[j]/npoints - mean*mean;

      if (var > maxvariance)
         {
         maxdimension = j;
         maxvariance = var;
         }
      }

   // This is the dimension to split this node at
   tree[node].coord_number_ = maxdimension;

   // Next, sort the points between startpoint and endpoint based on the
   // value at this dimension.  We actually sort the array pointorder
   int *order = &(pointorder[startpoint]);
   KDOrder::set_points (points);
   KDOrder::set_dim (maxdimension);
   qsort (order, npoints, sizeof(int), KDOrder::order_cmp);

   // Next find the median value to split at
   int median = startpoint + split(npoints) - 1;

   // Set the value of the boundary
   double lowval  = points[pointorder[median]][maxdimension];
   double highval = points[pointorder[median+1]][maxdimension];
   tree[node].boundary_ = (float) ((lowval + highval) * 0.5);

   // Now, recurse
   BuildKDTree(points, tree, left_branch(node), pointorder, startpoint, median);
   BuildKDTree(points, tree,right_branch(node), pointorder, median+1, endpoint);
   }

int KDTree::find_base_point (KDNode *tree, rhFloatVector goal, int startnode)
   {
   // Returns the number of the point in same region as the goal
   points_evaluated_ += 1;

   int kdnode = startnode;	// Starting node for search
   while (kdnode <= last_nonleaf_node_)
      {
      int dimension = tree[kdnode].coord_number_;
      double val = goal[dimension];
      if (val < tree[kdnode].boundary_)
         kdnode = left_branch(kdnode);
      else
         kdnode = right_branch(kdnode);
      }

   // Return the point number
   return tree[kdnode].pointnumber_;
   }

void KDTree::schedule_node (
   int treenum,
   int searchnode,
   rhFloatVector nearest_point,
   double dist
   )
   {
   // Adds a node and related data to the event queue
   // printf ("Scheduling node %d : Distance = %e\n", searchnode, dist); 
   nodes_scheduled_ += 1;
   PriorityQueueEntry_ref node  = 
      new KDPriorityQueueEntry (treenum, searchnode, nearest_point, dist);
   priority_queue_->add_event (node);
   }

bool KDTree::get_next_node ( 
   int *treenum, int *node, double *dist, rhFloatVector *nearest_point)
   {
   // Get the next event
   PriorityQueueEntry_ref entry = priority_queue_->get_next_event().ptr();

   // Down cast
   KDPriorityQueueEntry_ref kdentry = (KDPriorityQueueEntry *)entry.ptr();

   if (kdentry)
      {
      *treenum = kdentry->tree_number_;
      *node = kdentry->search_node_;
      *dist = kdentry->distance_to_goal_;
      *nearest_point = kdentry->nearest_point_to_goal_;
      // printf ("Got node %d\n", *node);
      return true;
      }
   else
      return false;
   }

void KDTree::schedule_node_search (
   int treenum,		// The tree number to search
   int startnode, 		// Starting node for search
   rhFloatVector goal,		// The goal point
   rhFloatVector nearest_point,	// Nearest point to goal on this node
   double distance,	// Distance to this nearest point
   double maxdistance	// Maximum distance we are interested in
   )
   {
   // Run down the tree starting from the startnode and schedule searches
   // of the other nodes we pass on the way

   KDNode *tree = tree_[treenum];

   int kdnode = startnode;	// Starting node for search
   while (kdnode <= last_nonleaf_node_)
      {
      int dimension = tree[kdnode].coord_number_;
      double gol_val = goal[dimension];
      double bnd_val = tree[kdnode].boundary_;
      int searchnode;	// Neighboring node

      if (gol_val < bnd_val)
         {
         // Go left
         searchnode = right_branch(kdnode); 
         kdnode = left_branch(kdnode);
         }
      else
         {
         // Go right
         searchnode = left_branch(kdnode); 
         kdnode = right_branch(kdnode);
         }

      // We are going to search the search node, if necessary
      // First of all, determine how far the region is away from the goal
      // Recall that all distances are actually squared distances.
      // The only distance that changes is the chosen dimension
      double npt_val = nearest_point[dimension];
      double newdist = distance 
         - kd_square(npt_val - gol_val) 
         + kd_square(bnd_val - gol_val);

      // Now, schedule or not, according to whether it is close enough
      if (newdist < maxdistance)
         {
         rhFloatVector new_nearest_point = nearest_point.copy();
         new_nearest_point[dimension] = (float)bnd_val;
         schedule_node (treenum, searchnode, new_nearest_point, newdist);
         }
      }
   }

int KDTree::find_nearest_neighbour (rhFloatVector goal)
   {
   // Finds the nearest neighbour

   // Keep count of points found
   points_found_ += 1;

   // First, set up the priority queue
   priority_queue_ = new PriorityQueue();

   // First of all, find the base region and get the point
   int best_point_num = find_base_point (tree_[0], goal, first_node_);
   rhFloatVector best_point = points_[best_point_num];
   double mindist = distance(best_point, goal);

   // Now, schedule some nodes for searching
   schedule_node_search (0, first_node_, goal, goal, 0.0, mindist);

   // Now go into a loop of searching
   for (int numtrials = 0; numtrials != max_trials_; numtrials++)
      {
      // If max_trials_ = -1 (default) this will loop until completion

      // Searching ----------------------------------------------

      // Get the next node
      int treenumber; // Not used
      int search_node;
      double search_node_distance;
      rhFloatVector nearest_point;
      bool found = get_next_node (
         &treenumber, &search_node, &search_node_distance, &nearest_point);

      // Exit from here if no more nodes found, or it is too far away
      if (! found || search_node_distance > mindist) break;

      // Now, search on this node
      int new_point_num = find_base_point (tree_[0], goal, search_node);
      rhFloatVector newpoint = points_[new_point_num];
      double newdist = distance(newpoint, goal);

      // See if this is the best node
      if (newdist < mindist)
         {
         mindist = newdist;
         best_point_num = new_point_num;
         }

      // Scheduling ---------------------------------------------
      schedule_node_search (
         0, search_node, goal, nearest_point, search_node_distance, mindist);
      }

   // Delete the priority queue
   delete priority_queue_;
   priority_queue_ = NULL;

   // Return the number of the nearest neighbour
   return best_point_num;
   }

void KDTree::set_max_trials (int n) { max_trials_ = n; }

void KDTree::print_statistics ()
   {
   fprintf (stdout, "KDTree:  neighbours found : %d\n", points_found_);
   fprintf (stdout, "KDTree:  points evaluated : %d average : %f\n", 
      points_evaluated_, points_evaluated_ / (double) points_found_);
   fprintf (stdout, "KDTree:  nodes scheduled  : %d average : %f\n", 
      nodes_scheduled_, nodes_scheduled_ / (double) points_found_);
   }

void KDTree::reset_statistics ()
   {
   points_found_ = 0;
   points_evaluated_ = 0;
   nodes_scheduled_ = 0;
   }
