// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

#include <stdlib.h>
#include <Utilities_CU/Matrix.h>
// #include <Utilities_CU/utilities_CC.h>

// Routines for matrices

void rh_free (void *a)
   {
   free (a);
   }

void Matrix_standard_error_handler ()
   {
   error_message ("Dimension error in Matrix Routines");
   // purify_printf_with_call_chain ("Dimensions error in Matrix Routines");
   bail_out (2);
   };

// rhVector rhMatrix
#define DOUBLE_ONLY
#define VectorType rhVector
#define MatrixType rhMatrix
#define BaseVectorType Double_vector
#define BaseMatrixType Double_matrix
#define realtype   double
#define element_format "%15.6e"
#include <Utilities_CU/RealMatrix.cu>
#undef element_format
#undef VectorType
#undef MatrixType
#undef BaseVectorType
#undef BaseMatrixType
#undef realtype
#undef DOUBLE_ONLY

// rhFloatVector rhFloatMatrix
#define VectorType rhFloatVector
#define MatrixType rhFloatMatrix
#define BaseVectorType Float_vector
#define BaseMatrixType Float_matrix
#define realtype   float
#define element_format "%15.6e"
#include <Utilities_CU/RealMatrix.cu>
#undef element_format
#undef VectorType
#undef MatrixType
#undef BaseVectorType
#undef BaseMatrixType
#undef realtype

