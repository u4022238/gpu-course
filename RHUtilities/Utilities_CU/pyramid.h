// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)pyramid.h	1.2 10/01/95
#ifndef pyramid_h
#define pyramid_h

#include <Utilities_CU/utilities_CC.h>
#include <vector>

// Define the image type
// typedef Float_matrix Pyramid_Image_type;
// typedef Float_vector Pyramid_vector_type;
// typedef float Pixel_type;
typedef Double_matrix Pyramid_Image_type;
typedef Double_vector Pyramid_vector_type;
typedef double Pixel_type;

Pyramid_Image_type reduce (
   Pyramid_Image_type &A		// source array 
   );

Pyramid_Image_type expand ( Pyramid_Image_type &A, int xdim, int ydim );

int number_of_pyramid_levels (Pyramid_Image_type &A, int mindim);

Pyramid_Image_type *gaussian_pyramid (
   Pyramid_Image_type &A,	// An image
   int nlevels 		// Number of bottom level of Gaussian 
   );

void create_gaussian_pyramid (
   Pyramid_Image_type &A,		// An image
   std::vector<Pyramid_Image_type> &pyramid	// The pyramid to be filled out
   );

Pyramid_Image_type *laplacian_pyramid (
   Pyramid_Image_type &A,	// An image
   int nlevels		// Number of bottom level of pyramid
   );

Pyramid_Image_type expand_laplacian_pyramid (Pyramid_Image_type *LL, int nlevels);

Pyramid_Image_type composite_image (Pyramid_Image_type *GG, int nlevels, int arrangement);

#endif
