// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


//-----------------------------------------
//
//   The Trigs Quadrifocal Tensor
//
//-----------------------------------------

#ifndef _rh_quad_tensor_h
#define _rh_quad_tensor_h

// #include <stdio.h>
#include <Utilities_cuda/utilities_c.h>
#include <Utilities_CU/Matrix.h>

#define for_quadindex(i, j, k, l)			      \
	for (i=0; i<3; i++) for(j=0; j<3; j++)	      \
	   for (k=0; k<3; k++) for(l=0; l<3; l++)

//#define REF_DEBUG

class quadTensor
   {
   typedef double **** qptr;

   private:

	qptr Val;
        unsigned int *reference_count;
	     void allocate ();
        void deallocate () { free (Val); }
        void dereference ();

   public:

	//
	// For indexing into the array
	//

	// For indexing into the array without checking
	double *** operator[] (int i) const { return Val[i]; }

	// For getting a handle on the array itself
	qptr data () const { return Val; }

 	// Basically the same thing, but using casting
	operator qptr () const { return Val; }
	operator const qptr () const { return Val; }

	// Constructors
	quadTensor ();
	quadTensor (const quadTensor &A);
	quadTensor (const rhVector &q);
	void Init ();

	// Destructors
	void Dealloc () { dereference (); }
	~quadTensor() { dereference (); }

	// Assignment operator
        quadTensor &operator = (const quadTensor &avec);

	// Other actions
	void clear () const;
	void clear (double val) const;
	quadTensor copy () const;
	void print (FILE *afile = stdout, char *format = "%13.5e ");
   };

#endif
