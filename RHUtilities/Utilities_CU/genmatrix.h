// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//-----------------------------------------
//
//   Various matrix classes follow
//
//-----------------------------------------

// #include <stdio.h>
#include <Utilities_cuda/utilities_c.h>

//#define REF_DEBUG

// Macro for indexing matrices
#ifndef for_2Dindex
#   define for_2Dindex(i, j, im)			\
   for (i = (im).ilow(); i <= (im).ihigh(); i++)	\
      for (j = (im).jlow(); j <= (im).jhigh(); j++)	
#endif

#ifndef for2Dindex
#   define for2Dindex(i, j, im)			        \
   for (int i = (im).ilow(); i <= (im).ihigh(); i++)	\
      for (int j = (im).jlow(); j <= (im).jhigh(); j++)	
#endif


void Matrix_standard_error_handler();

// Forward definition of vector_type
class vector_type;
         
class matrix_type
   {
   private:

   int Ilow, Ihigh;
   int Jlow, Jhigh;

   element_type **Val;
   unsigned int *reference_count;

#ifdef INLINE_MATRIX_ALLOC
   void dereference ()
      {
      if (reference_count)
         {  
         // First unlink the thing
         *reference_count -= 1;

#	 ifdef REF_DEBUG
         fprintf (stderr, "Reference count for matrix %08x decreased to %d\n",
                     Val, *reference_count);
#	 endif

         // Now, if reference count has fallen to zero, then free
         if (*reference_count <= 0)
            {
#	 ifdef REF_DEBUG
            extern int MatrixBytesAllocated;
            int size = (Ihigh-Ilow+1)*(Jhigh-Jlow+1)*sizeof(element_type);
            MatrixBytesAllocated -= size;
            fprintf (stderr, "Freeing matrix %08x size %d : total = %d\n", 
                Val, size, MatrixBytesAllocated);
# 	 endif
   
            // Free the values
            if (Val != (element_type **)0)
               rhFREE_macro (Val, Ilow);

            // Delete the reference count
            delete reference_count;
            }
         }   

      // Set Val and reference count to initial values
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Ilow = Ihigh = Jlow = Jhigh = 0;
      }

#else
   void dereference();

#endif

   public:

   static void (*Error_Handler) ();
   void set_Error_Handler ( void (*fn)()) const;

   void plugin ( int ilow, int ihigh, int jlow, int jhigh, element_type **val)
      {
      // Plugs in a different array and bounds.  The result
      // is an unreference-counted array.

      // First dereference, in case there was an old array
      dereference ();

      // Now put in the new bounds and data
      Ilow = ilow;
      Ihigh = ihigh;
      Jlow = jlow;
      Jhigh = jhigh;
      Val = val;
      }

#if defined (__STDC__) || defined(WIN32)
#   define rh_dptr1(typ) typ##_dptr
#else
#   define rh_dptr1(typ) typ/**/_dptr
#endif
#define rh_dptr(a) rh_dptr1(a)

   typedef element_type ** rh_dptr(matrix_type);

   // For getting the bounds of the array
   int ilow() const { return Ilow; }
   int jlow() const { return Jlow; }
   int ihigh() const { return Ihigh; }
   int jhigh() const { return Jhigh; }
   int idim () const { return Ihigh - Ilow + 1; }
   int jdim () const { return Jhigh - Jlow + 1; }
   
   // For getting the bounds of the array
   element_type *begin() { return &Val[Ilow][Jlow]; }
   element_type *end  () { return &Val[Ilow][Jlow] + idim()*jdim();}

   Int_rectangle bounds() const  
      { return Int_rectangle (Jlow, Ilow, Jhigh, Ihigh); }

   //
   // For indexing into the array
   //
   int defined_at (int i, int j) const
      { return i >= Ilow && i <= Ihigh && j >= Jlow && j <= Jhigh; }

   int defined_at (Int_point apt) const
      {
      return apt.i() >= Ilow && apt.i() <= Ihigh &&
           apt.j() >= Jlow && apt.j() <= Jhigh;
      }

#	ifdef CHECKBOUNDS
   // For indexing into the array while checking
   element_type * operator[] (int i) const
      { 
      // Check the value
      if (i < Ilow || i > Ihigh)
         fprintf (stderr, "Index %d out of range (%d, %d)\n", 
      i, Ilow, Ihigh);
         
      // Return the value nevertheless
      return Val[i]; 
      }

   element_type & operator[] (Int_point p) const
      {
      // Check the array bounds
      if (! defined_at (p))
         fprintf (stderr, "Index (%d, %d) out of range [%d,%d][%d,%d]\n",
       p.i(), p.j(), Ilow, Ihigh, Jlow, Jhigh);
   
      // Return the value nevertheless
      return Val[p.i()][p.j()];
      }
#	else
   // For indexing into the array without checking
   element_type * operator[] (int i) const { return Val[i]; }
   element_type & operator[] (Int_point p) const 
      {return Val[p.i()][p.j()];}
#	endif

   // For getting a handle on the array itself
   element_type **data () const { return Val; }

   // Linear intepolated value in an array
   double ij_val_interpolated (double i, double j) const;

   // Basically the same thing, but using casting
   // operator element_type ** () const { return Val; }
   // operator int ** () const { return (int **) Val; }
   // operator const double ** () const { return Val; }
   operator rh_dptr(matrix_type) () const { return Val; }
   // operator const rh_dptr(element_type) () const { return Val; }

   // Constructors
   matrix_type ()	
      { 
      Ilow = Jlow = 0;
      Ihigh = Jhigh = -1;
      Val = (element_type **) 0; 
      reference_count = (unsigned int *)0;
      }

   bool is_null ()
      {
      if (Ilow == 0 && Ihigh == -1) return true;
      else return false;
      }

   matrix_type (int il, int ih, int jl, int jh) 
      { 
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Init (il, ih, jl, jh); 
      }

   matrix_type (int idim, int jdim)
      { 
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Init (0, idim-1, 0, jdim-1);
      }

   matrix_type (const matrix_type &A)
      {
      // Copy constructor
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Init(A);
      }

   matrix_type (Int_point tl, Int_point br) 
      { 
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Init (tl, br); 
      }

   matrix_type (Int_rectangle r) 
      { 
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;
      Init (r); 
      }

#ifdef INLINE_MATRIX_ALLOC
   void Init (int il, int ih, int jl, int jh)
      {
      // First, dereference this array
      dereference ();

      // Now reallocate
      Ilow  = il;
      Ihigh = ih;
      Jlow  = jl;
      Jhigh = jh;
      Val = MATRIX (Ilow, Ihigh, Jlow, Jhigh, element_type);

#     ifdef REF_DEBUG
      extern int MatrixBytesAllocated;
      int size = (Ihigh-Ilow+1)*(Jhigh-Jlow+1)*sizeof(element_type);
      MatrixBytesAllocated += size;
      fprintf (stderr, "Allocate matrix %08x size %d : total = %d\n", 
             Val, size, MatrixBytesAllocated);
#      endif

      // Also the reference count
      reference_count = new unsigned int;
      *reference_count = 1;
#     ifdef REF_DEBUG
         fprintf (stderr,"Reference count for matrix %08x set to 1\n", Val);
#     endif
      }

#else
   void Init (int il, int ih, int jl, int jh);
#endif

   void Init (Int_point tl, Int_point br)
      { 
      Init (tl.i(), br.i(), tl.j(), br.j());
      }

   void Init (Int_rectangle r)
      { 
      Init (r.ilow(), r.ihigh(), r.jlow(), r.jhigh());
      }

   void Init (const matrix_type &A)
      {
      dereference ();
      Val = (element_type **) 0;
      reference_count = (unsigned int *)0;

      // Now copy
      *this = A;
      }

   // Destructors
   void Dealloc () { dereference (); }
   ~matrix_type() { dereference (); }

   // Assignment operator
   matrix_type &operator = (const matrix_type &avec)
      {
      // Increment the reference count
      if (avec.reference_count)
         {
         *(avec.reference_count) += 1;
#	 ifdef REF_DEBUG
         fprintf (stderr, "Reference count for matrix %08x increased to %d\n",
                     avec.Val, *(avec.reference_count));
#	 endif
         }
 
      // Dereference the current list
      dereference ();
 
      // Copy the fields
      Ilow = avec.Ilow;
      Ihigh = avec.Ihigh;
      Jlow = avec.Jlow;
      Jhigh = avec.Jhigh;
      reference_count = avec.reference_count;
      Val = avec.Val;
 
      // Return the pointer itself
      return *this;
      }

   // Reallocation operators

   void reallocate (int il, int ih, int jl, int jh) 
      { 
      // Reallocates and copies data
      matrix_type newm (il, ih, jl, jh);

           // Now, copy the data
      int i, j;
      for_2Dindex (i, j, newm)
        if (this->defined_at (i, j)) newm[i][j] = (*this)[i][j];
        else newm[i][j] = 0;

      // This is the new matrix
      *this = newm;
      }

   void reallocate (Int_point tl, Int_point br) 
      { 
      reallocate (tl.i(), br.i(), tl.j(), br.j());
      }

   void reallocate (Int_rectangle r) 
      { 
      reallocate (r.ilow(), r.ihigh(), r.jlow(), r.jhigh());
      }

   void reallocate (const matrix_type &A)
      {
      // Copy constructor
      reallocate (A.bounds());
      }

   matrix_type zero_origin () const;

   void clear () const;

   void clear (element_type val) const
      { 
      for (int i = Ilow; i <= Ihigh; i++)
         for (int j = Jlow; j <= Jhigh; j++)
             Val[i][j] = val;
      }

   matrix_type copy () const
      {
      // Copy the array including the elements
      int i, j;
      const matrix_type &A = *this;
           matrix_type B (A.bounds());
           for_2Dindex (i, j, B)
         B[i][j] = A[i][j];
           return B;
      }

   // Finding maximum and minimum values
   element_type max () const;
   element_type min () const;
   double sum () const;
   double sumsq () const;

   // Transpose
   matrix_type transpose () const
      {
      // Takes the transpose of a matrix
 
      // Get the name of the matrix
      const matrix_type &m = *this;

      // Carry out the transposition
      matrix_type trans (m.jlow(), m.jhigh(), m.ilow(), m.ihigh());
      int i, j;
      for_2Dindex (i, j, trans)
         trans[i][j] = m[j][i];

      // Return the matrix
      return trans;
      }

   // Routines for printing them out
   void print (FILE *afile = stdout, const char *format = (char *)0) const
      {
      // Prints the vector
      if (format == (char *)0) format = print_format;
      int nrl = this->ilow(), nrh = this->ihigh();
      int ncl = this->jlow(), nch = this->jhigh();
      const matrix_type &a = *this;
      for (int i=nrl; i<=nrh; i++)
         {
         for (int j=ncl; j<=nch; j++)
            fprintf (afile, format, a[i][j]);
         fprintf (afile, "\n");
         }
      fprintf (afile, "\n");
      }

   void print_ifdebug(FILE *afile = stdout, const char *format = (char *)0) const
      {
      // Prints the vector
      if (debugging_messages_enabled())
         {
         int nrl = this->ilow(), nrh = this->ihigh();
         int ncl = this->jlow(), nch = this->jhigh();
         const matrix_type &a = *this;
         if (format == (char *)0) format = print_format;

         for (int i=nrl; i<=nrh; i++)
            {
            for (int j=ncl; j<=nch; j++)
               fprintf (afile, format, a[i][j]);
               fprintf (afile, "\n");
            }
         fprintf (afile, "\n");
         }
      }


   matrix_type submatrix (int ilow, int ihigh, int jlow, int jhigh) const;

#undef rh_dptr
#undef rh_dptr1
   };


// Non-class functions
matrix_type tensor_product (const vector_type &a, const vector_type &b);
