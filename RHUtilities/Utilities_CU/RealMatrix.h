// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// @(#)Matrix.h	1.7 12/02/96
//

// Routines for matrices
#include <Utilities_CU/arraygen.h>

void Matrix_standard_error_handler();

void rh_free (void * a);

class MatrixType : public BaseMatrixType
   {
   public :

	   // Error code mechanism
	   typedef enum {CANNOT_OPEN_FILE, BAD_MATRIX_DATA_FILE} ErrorCode;
	   static ErrorCode error_code;

      static void (*Error_Handler) ();
	
	   MatrixType () {};

	   MatrixType (int m, int n) { BaseMatrixType::Init (0, m-1, 0, n-1); }

	   MatrixType &Init (int m, int n) 
		   { BaseMatrixType::Init (0, m-1, 0, n-1); return *this; }

	   MatrixType (int m, int n, double **vals);

	   MatrixType (int m, int n, double *vals);

	   MatrixType (int m, int n, const double *vals);

      void resize(int newrows, bool clear = true);

	   MatrixType submatrix (int xlow, int xhigh, int ylow, int yhigh) const;

	   int nrows () const { return this->idim(); }

	   int ncols () const { return this->jdim(); }

	   MatrixType operator - (const MatrixType & m2) const;

	   MatrixType operator + (const MatrixType & m2) const;

	   MatrixType operator * (const MatrixType & m2) const;

	   MatrixType operator * (double k) const;

	   MatrixType operator / (double k) const;

	   void operator -= (const MatrixType & m2);

	   void operator += (const MatrixType & m2);

	   void operator *= (double k);

	   void operator /= (double k);

      // For concatenating matrices
	   MatrixType operator | (const MatrixType & m2) const;
	   MatrixType operator & (const MatrixType & m2) const;
	   MatrixType operator | (const class VectorType & m2) const;
	   MatrixType operator & (const class VectorType & m2) const;

      MatrixType &setrow (int i, const class VectorType &arow);
      MatrixType &setrow (int n, const realtype *src);

      MatrixType &setcolumn (int i, const class VectorType &acol);

	   class VectorType operator * (const class VectorType & v) const;
	   class VectorType operator * (const double * v) const;

	   // Unary minus
	   friend MatrixType operator - (const MatrixType &v);     

	   // Mult by const
	   friend MatrixType operator * (double k, const MatrixType &m); 

	   // Normalized matrix
	   MatrixType normalized () const; 
	   void normalize ();

	   MatrixType inverse () const;
	   MatrixType inverse_or_nearinverse () const;
      MatrixType pseudo_inverse (int rank) const;
      MatrixType pseudo_inverse (double thresh = 1.0e-6) const;

	   MatrixType adjoint () const;
	
	   MatrixType transpose () const;

      MatrixType copy () const;

 	   double Frobenius_norm () const;

	   void copy_to (double **M) const;

	   void copy_to (float **M) const;

	   double det () const;

	   static MatrixType identity (int dim);

	   MatrixType diagonal (const VectorType &v);
	   static MatrixType diagonal (double d1, double d2);
	   static MatrixType diagonal (double d1, double d2, double d3);
	   static MatrixType diagonal (double d1, double d2, double d3, double d4);

	   static MatrixType I0 ();		// Canonic camera matrix

	   static MatrixType zeros (int idim, int jdim);	// Matrix of zeros

	   static MatrixType ones (int idim, int jdim);	// Matrix of ones

	   static MatrixType skew (const VectorType &v);

	   VectorType min_solution () const;

	   class VectorType row (int n) const;
  
	   class VectorType column (int n) const;

      class VectorType diagonal () const;

      class VectorType rowsum () const;
      class VectorType columnsum () const;

	   void set_Error_Handler ( void (*fn)()) const;

	   void print (FILE *afile = stdout, 
		    const char *format = element_format) const;

	   void print_ifdebug (FILE *afile = stdout, 
		    const char *format = element_format) const;

	   void print_latex (FILE *afile = stdout, 
		          const char *format = element_format) const;

#if 0
	   void print (FILE *afile = stdout, 
		    const char *format = "%15.6e") const;

	   void print_ifdebug (FILE *afile = stdout, 
		    const char *format = "%15.6e") const;

	   void print_latex (FILE *afile = stdout, 
		          const char *format = "%15.6e") const;
#endif

    	// Determine if the matrix has any points
  	   rhBOOLEAN is_null () { return idim() <= 0 && jdim() <= 0; }

	   // A couple of things needed for making lists
	   bool operator == (const MatrixType &) const;
 	   bool operator <  (const MatrixType &) const;

	   // Read from a file
	   bool read_from_file (const char *fname);

   };

class VectorType : public BaseVectorType
   {
   public :

      static void (*Error_Handler) ();
	
	   VectorType () {};

	   VectorType (int m) { BaseVectorType::Init (0, m-1); }

	   VectorType (double a, double b) 
	      { 
         Init(2); 
         (*this)[0] = (realtype)a; 
         (*this)[1] = (realtype)b; 
         }

	   VectorType (double a, double b, double c) 
	      { 
         Init(3); 
         (*this)[0] = (realtype)a; 
         (*this)[1] = (realtype)b; 
         (*this)[2] = (realtype)c; 
         }

	   VectorType (double a, double b, double c, double d) 
	      { 
         Init(4); 
         (*this)[0] = (realtype)a; 
         (*this)[1] = (realtype)b; 
	      (*this)[2] = (realtype)c; 
         (*this)[3] = (realtype)d; 
         }

      VectorType (const MatrixType &c); // Makes a vector from matrix elements

	   VectorType &Init (int m) {BaseVectorType::Init (0, m-1); return *this; }

   	VectorType (int m, double *vals);

	   // Tensor product
	   MatrixType operator ^ (const VectorType & v2) const;	

	   // Multiplication by constant
	   VectorType operator * (double) const;	

	   // Division by constant
	   VectorType operator / (double) const;	

	   // Sum of two vectors
	   VectorType operator + (const VectorType & v2) const;	

	   // Difference
	   VectorType operator - (const VectorType & v2) const;	

	   // Inner product
	   double operator * (const VectorType & v2) const;	

	   // row VectorType x MatrixType
	   VectorType operator * (const MatrixType & m2) const;  	

 	   // In situ operators
	   void operator -= (const VectorType & m2);

	   void operator += (const VectorType & m2);

	   void operator *= (double k);

	   void operator /= (double k);

	   // shift the array
	   VectorType operator << (int n) const;  		

	   // Unary minus
	   friend VectorType operator - (const VectorType &v);     

	   // Mult by const
	   friend VectorType operator * (double k, const VectorType &v); 

      // Concatenation operators
	   MatrixType operator | (const MatrixType & m2) const;
	   MatrixType operator & (const MatrixType & m2) const;
	   MatrixType operator | (const class VectorType & m2) const;
	   VectorType operator & (const class VectorType & m2) const;

	   VectorType normalized () const;	// Normalized vector
	   void normalize ();

	   VectorType copy () const;		// Copy of the vector

	   void copy_to (double *M) const;

	   void set_Error_Handler ( void (*fn)()) const;

	   void print (FILE *afile = stdout, 
		    const char *format = "%15.6e") const;

	   void print_ifdebug (FILE *afile = stdout, 
		    const char *format = "%15.6e") const;

	   int dim() const { return num_elements(); }

	   double length () const;

	   double length_sq () const;

	   VectorType dehom () const;

	   double dehom (int i) const;  // Dehomogenized coordinate

	   VectorType hom () const;

  	   rhBOOLEAN is_null () { return dim() <= 0; }

	   static VectorType zeros (int idim);	// Vector of zeros

	   static VectorType ones (int idim);  // Vector of ones

   };

// Some extra function
void print (const MatrixType &m);
void mprint (const MatrixType &m);
void print (VectorType &v);
void vprint (VectorType &v);
VectorType cross_product (const VectorType &A, const VectorType &B);
VectorType lin_solve_old (const MatrixType &A, const VectorType &B);
VectorType lin_solve (const MatrixType &A, const VectorType &B);

void fprint_matrix_cc (
        const char *format,
        FILE *afile,
        realtype **a,
        int nrl,
        int nrh,
        int ncl,
        int nch);

void fprint_vector_cc (
        const char *format,
        FILE *afile,
        realtype **a,
        int ncl,
        int nch);
