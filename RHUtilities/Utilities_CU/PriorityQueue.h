#ifndef __Priority_QUEUE_H__
#define __Priority_QUEUE_H__

#include <vector>
using namespace std;
#include <Utilities_CU/SmartPointer.h>

#include <Utilities_CU/utilities_CC.h>


class PriorityQueueEntry : public ReferenceCountable
   {
   public :
      virtual double keyval() = 0;
   };

typedef SmartPointer<PriorityQueueEntry> PriorityQueueEntry_ref;

class PriorityQueue
   {
   protected :

        int length_;
        vector<PriorityQueueEntry_ref> events_;

   public :

        // Constructor
        PriorityQueue () : length_(0)
           { events_.resize(256); }

        // Add an event to the heap
        void add_event (PriorityQueueEntry_ref event);

        // Get the next event from the heap and delete it
        PriorityQueueEntry_ref get_next_event ();

        // Accessors
        inline int parent_of (int n) {  return (n+1)/2-1; }
        inline int child1_of (int n) {  return 2*n+1; }
        inline int child2_of (int n) {  return 2*n+2; }
   };

#endif
