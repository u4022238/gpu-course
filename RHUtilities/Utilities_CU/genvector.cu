// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)genvector.cc	1.4 10/01/95
//
// Matrix routines
//

vector_type convolution (const vector_type &A, const vector_type &B)
   {
   // Returns the convolution of the two arrays

   // Declare an array of the right size
   vector_type C (A.low() + B.low(), A.high() + B.high());
   C.clear(0);

   // Now, fill it out
   int i, j;
   for_1Dindex (i, A)
      for_1Dindex (j, B)
  	 C[i+j] += A[i]*B[j];

   // Return the convolved vector
   return C;
   }

void vector_type::dereference ()
   {
   if (reference_count)
      {
      // First unlink the thing
      *reference_count -= 1;

#ifdef REF_DEBUG
      fprintf (stderr, 
   	 "Reference count for vector %08x decreased to %d\n", 
	    &(Val[0]), *reference_count);
#endif

      // Now, if reference count has fallen to zero, then free
      if (*reference_count <= 0)
         {
 	 // Free the values
         if (Val != (element_type *)0)
            GVFREE (Val, Xlow);

         // Delete the reference count
         delete reference_count;
#ifdef REF_DEBUG
	 fprintf (stderr, "Freed vector %08x\n", &(Val[0]));
#endif
         }
      }

    // Set Val and reference count to initial values
    Val = (element_type *) 0;
    reference_count = (unsigned int *)0;
    Xlow = 0;
    Xhigh = AllocXhigh = -1;
    }

vector_type &vector_type::Init (int xl, int xh, int axh)
{
   // First dereference this array
   dereference ();

   // Now reallocate
   Xlow  = xl;
   Xhigh = xh;
   AllocXhigh = (axh < xh) ? xh : axh;
   Val = GVECTOR (Xlow, AllocXhigh, element_type);

   // Also the reference count
   reference_count = new unsigned int;
   *reference_count = 1;

   #ifdef REF_DEBUG
    fprintf (stderr, "Reference count for vector %08x set to 1\n", 
   	&(Val[0]));
#   endif

   return *this;
   }

element_type vector_type::max() const
   {
   // Get an alias for the vector
   const vector_type &v = *this;
  
   // Initialize with the first element in the array
   element_type mx = v[v.low()];

   // Find the maximum
   int i;
   for_1Dindex (i, v)
      if (v[i] > mx) mx = v[i];

   // Return the maximum
   return mx;
   }

element_type vector_type::min() const
   {
   // Get an alias for the vector
   const vector_type &v = *this;
  
   // Initialize with the first element in the array
   element_type mx = v[v.low()];

   // Find the minimum
   int i;
   for_1Dindex (i, v)
      if (v[i] < mx) mx = v[i];

   // Return the minimum
   return mx;
   }

double vector_type::sum() const
   {
   // Get an alias for the vector
   const vector_type &v = *this;
  
   // Initialize the sum
   double vsum = 0.0;

   // Find the minimum
   int i;
   for_1Dindex (i, v)
      vsum += (double)(v[i]);

   // Return the minimum
   return vsum;
   }

double vector_type::sumsq() const
   {
   // Get an alias for the vector
   const vector_type &v = *this;
  
   // Initialize the sum
   double vsumsq = 0.0;

   // Find the minimum
   int i;
   for_1Dindex (i, v)
      {
      double val = (double)(v[i]);
      vsumsq += val * val;
      }

   // Return the minimum
   return vsumsq;
   }

#ifdef ptrcmp
double vector_type::median() const
   {
   // Computes the median of a vector -- as written now, it only works
   // for double
   // Get an alias for the array
   const vector_type &a = *this;
   const int n = a.dim();

   // Make an array indexing the entries
   element_type **perm = new element_type *[n];
   for (int i=0; i<n; i++) perm[i] = &(a[i]);

   /* Now sort the array perm */
   qsort ((void *)perm, n, sizeof (element_type *), ptrcmp);

   // The median is the middle value
   int mid = dim()/2;
   double med = (*(perm[mid]) + *(perm[mid+1]))/2.0;

   // Delete the temporary
   delete [] perm;
   return med;
   }
#endif

//
// Reading and writing vectors
//
vector_type & vector_type::write (FILE *outfile)
   {
   // Write a vector to a file

   // Write the low and high bounds
   fwrite ((const char *)&Xlow, sizeof (int), 1, outfile);
   fwrite ((const char *)&Xhigh, sizeof (int), 1, outfile);

   // Now, write the data itself
   fwrite ((const char *)&(Val[Xlow]), sizeof (element_type), 
	Xhigh - Xlow + 1, outfile);

   // Return the vector
   return *this;
   }

vector_type &vector_type::read (FILE *infile)
   {
   // Reads the vector from an open file
  
   // Get an alias for the vector
   vector_type &v = *this;
   
   // First of all, read the bounds
   int xlow, xhigh;
   fread ((char *)&xlow,  sizeof(int), 1, infile);
   fread ((char *)&xhigh, sizeof(int), 1, infile);

   // Check that these bounds make sense
   if (xlow > xhigh) return *this;

   // Otherwise, initialize the vector
   v.Init (xlow, xhigh);

   // Next, read the values
   fread ((char *)&(Val[xlow]), sizeof (element_type), 
	xhigh - xlow + 1, infile);
   
   // Hope that it worked
   return *this;
   }

void vector_type::Resize (int axh)
   {
   // Moves the data into a larger space (or smaller space)
   AllocXhigh = axh;
   element_type *Valnew = GVECTOR (Xlow, AllocXhigh, element_type);

   // Adjust the range if necessary
   if (Xhigh > AllocXhigh) Xhigh = AllocXhigh;
	
   // Now, copy the data
   memcpy (Valnew, Val, (Xhigh - Xlow + 1) * sizeof (element_type));

   // Free the old version and replace by the new
   GVFREE (Val, Xlow);
   Val = Valnew;
   }

vector_type &vector_type::push_back (element_type val)
   {
   // Put a new value on the end of the array
   const int MinAllocationSize = 4;

   // Resize if necessary
   if (Xhigh >= AllocXhigh)
      {
      // Work out the new size to be allocated
      int allocated_size = dim();
      int newsize = rhMax (MinAllocationSize, 2*allocated_size);
      int newaxh = AllocXhigh + newsize - allocated_size;

      // Resize
      Resize(newaxh);
      }

   // Now, assign the new value
   (*this)[++Xhigh] = val;

   // Return itself
   return *this;
   }

//
// Matrix methods
//

matrix_type tensor_product (const vector_type &a, const vector_type &b)
   {
   // Does the tensor product of two vectors, giving a matrix

   // Make a matrix of the right size
   matrix_type p (a.low(), a.high(), b.low(), b.high());

   // Carry out the product
   int i, j;
   for_2Dindex (i, j, p)
      p[i][j] = a[i] * b[j];

   // Return the matrix
   return p;
   }
