// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// Array.cc	Array class					-*- C++ -*-

#include <malloc.h>
#include <Utilities_CU/Array.h>
#include <Utilities_CU/support.h>

// user errors
void allocSizeErr()
   { 
   fprintf (stderr, "invalid allocation size\n"); 
   bail_out (2);
   }

void indexRangeErr()
   { 
   fprintf(stderr, "index out of range\n");
   bail_out (2);
   }

static void allocFailErr()	    
   { 
   fprintf (stderr, "memory (re)allocation failed\n"); 
   bail_out (2);
   }


// memory management routines - use malloc/realloc/free rather than new/delete
// because C++ does not supply a replacement for realloc (which is needed)

inline Object** NEW(unsigned size)
{
   Object** p = (Object**)malloc(size * sizeof(Object*));
   if (!p) allocFailErr();
   return p;
}

inline Object** REALLOC(Object** q, unsigned size)
{
   Object** p = (Object**)realloc((char*)q, size * sizeof(Object*));
   if (!p) allocFailErr();
   return p;
}

inline void DELETE(Object** p) { if (p) free((char*)p); }

Array_Node::Array_Node(int size)
{
   /* Check that the size is positive */
   if (size < 0) allocSizeErr();

   if (size == 0) capac = 16;	/* Suitable default size */
   else capac = size;

   /* Allocate the array */
   v = NEW(capac);

   /* Zero out the array */
   for (int i=0; i<size; i++)
      v[i] = NULL;

   /* Fill the capacity and size fields */
   sz = size;
   reference_count = 1;
}

Array::Array(int size)
{
   /* Allocate a new array node */
   a = new Array_Node(size);
}

Array::Array(Array & arr)
{
   arr.a->reference_count ++;
   a = arr.a;
}

Array::~Array()
{
   delete (a);
}

Array_Node::~Array_Node ()
   /* Frees the storage occupied by the list.
    * Does not free the data in the list.
    */
   {  
   /* fprintf (stderr, "--- ~Array : reference count decremented to %d\n",
          reference_count - 1); */
 
   /* If there are no more references to this list, then unlink it */
   if (--reference_count <= 0)
      {
      /* fprintf (stderr, "Deleting list of length %d.\n", length()); */
      DELETE(v);
      }
   }   

Array & Array::operator = (Array &arr)
   {
   /* Assign one list to another.  Check the reference counts to free
    * strings that are no longer referenced */

   /* fprintf (stderr, "--- operator = (Array arr)\n"); */
   arr.a->reference_count ++;
 
   /* Delete the list previously referenced */
   delete this;
 
   a = arr.a;
   return *this;
   }

void Array::size(int newsize)
{
   /* Check that the size is reasonable */
   if (newsize <= 0) indexRangeErr();

   /* If new size exceeds the capacity, then need to expand */
   if (newsize > capacity()) capacity(newsize);

   if (newsize < size()) 
      {
      fprintf (stderr, "elements lost in Array::capacity()");
      bail_out (2);
      }

   /* Zero out the new part of the array */
   for (int i = size(); i<newsize; i++)
      (a->v)[i] = NULL;

   /* Set the new capacity */
   a->sz = newsize;
}

void Array::capacity(int newcap)
{
   if (newcap <= 0) allocSizeErr();
   if (newcap < size()) 
      {
      fprintf (stderr, "elements lost in Array::capacity()");
      bail_out (2);
      }

   /* reallocate the array */
   a->v = REALLOC(a->v, newcap);

   /* Zero out the new part of the array */
   for (int i = capacity(); i<newcap; i++)
      (a->v)[i] = NULL;

   /* Set the new capacity */
   a->capac = newcap;
}


Array & Array::addAt(int i, Object* ob)
{
   /* fprintf (stderr, "Adding at location %d. size = %d", i, size()); */

   /* Check that the index is in range */
   if (i < 0 || i > size()) indexRangeErr();

   /* fprintf (stderr, "  -- Done\n"); */

   /* Update the size */
   size(size() + 1);

   /* Double the size of the array */
   if (size() > capacity())
      capacity(2 * capacity());

   /* Move all the elements up */
   for (register int j = size()-1; j>i; j--)
      (*this)[j] = (*this)[j-1];

   /* Put the thing in the array */
   (*this)[i] = ob;

   /* Return the array */
   return *this;
}

Array & Array::removeAt(int i)
{
   /* Check range */
   if (i < 0 || i >= size()) indexRangeErr();

   Object *ob = (*this)[i];
   for (register int j = i+1; j<size(); j++)
      (*this)[j-1] = (*this)[j];

   /* Decrease the size */
   size(size()-1);

   /* Clear out the last element */
   (*this)[size()] = NULL;

   /* Return the extracted object */
   return *this;
}

//
// Methods for the Array Index class.
//


