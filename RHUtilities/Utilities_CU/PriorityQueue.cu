#include <Utilities_CU/PriorityQueue.h>
#include <Utilities_CU/utilities_CC.h>

void PriorityQueue::add_event (PriorityQueueEntry_ref event)
   {
   // Add a new event to the event heap
   // The events are ordered on the heap in the order of their x coordinate

   // Check that heap is big enough to accept another event -- otherwise double.
   if (length_ >= (int)events_.size())
      events_.resize (2*length_);

   // Get the x-coord of the event
   double event_x = event->keyval();

   // Filter it up
   int node;
   for (node=length_; node>=1; node=parent_of(node))
     {
     // Get the parent node
     int parent=parent_of(node);

     // Break if the parent is larger
     if (event_x > events_[parent]->keyval()) break;

     // Move it
     events_[node] = events_[parent];
     }

   // Now place the original value
   events_[node] = event;

   // Add one to the length
   length_ += 1;

#ifdef RH_DEBUG
   // Now check the stack
   for (int i=1; i<length_; i++)
     assert(events_[i]->keyval() >= events_[0]->keyval());
#endif
   }

PriorityQueueEntry_ref PriorityQueue::get_next_event ()
   {
   // Take the next event off the event heap

   // Null if nothing left on heap
   if (length_ == 0) return NULL;

   // Get the thing we want to store
   PriorityQueueEntry_ref nextevent = events_[0];

   // Adjust the length of heap, storing last event;
   length_ = length_-1;
   PriorityQueueEntry_ref lastevent = events_[length_];
   double lastx = lastevent->keyval();
   events_[length_] = NULL;

   // Filter down the heap
   int node = 0;
   while (1)
      {
      // How many children does the current node have
      int child1 = child1_of (node);
      int child2 = child2_of (node);
      if (child2 < length_)
         {
         // Both children.  Move earlier one up if < lastx
         int child = child2;
         if (events_[child1]->keyval() < events_[child2]->keyval())
            child = child1;
         if (events_[child]->keyval() > lastx) break;
         events_[node] = events_[child];
         node = child;
         }
      else if (child1 < length_)
         {
         // Only one child.  Move up if < lastx
         if (events_[child1]->keyval() > lastx) break;
         events_[node] = events_[child1];
         node = child1;
         }
      else
         {
         // Current node is at the bottom level
         break;
         }
      }

   // Now, we must put the last event in this slot
   events_[node] = lastevent;

#ifdef RH_DEBUG
   // Now check the stack
   for (int i=1; i<length_; i++)
      assert(events_[i]->keyval() >= events_[0]->keyval());
#endif

   // Now we can return
   return nextevent;
   }
