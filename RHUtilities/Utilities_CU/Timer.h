#ifndef _RIH_TIMER_h_
#define _RIH_TIMER_h_

#include <stdio.h>
#include <time.h>
//clock_t clock(void);

class rhTimer 
   {
   protected :

        char cstring_[80];
	     static FILE *tfile;
        static bool timing_on;

   public :

	     rhTimer (const char *context_string);
	     ~rhTimer();

        // Set timing on or off
        static void SetTimingOn  () {timing_on = true; }
        static void SetTimingOff () {timing_on = false; }

        // Open a file
        static FILE *OpenFile (const char *fname) 
           {
           tfile = fopen (fname, "w");
           return tfile;
           }
   };

#endif
