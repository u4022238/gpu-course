#ifndef _MessageSwitch_h_
#define _MessageSwitch_h_

#include <Utilities_cuda/error_message_dcl.h>

#define MessageStatus_On   1
#define MessageStatus_Off  0

class MessageSwitch 
   {
   protected :

        typedef int MessageStatus;
	MessageStatus previous_status_;

   protected :
	MessageSwitch ();
	virtual ~MessageSwitch();

   };

//-------------------------------------------------------------

class InformativeMessageSwitch : public MessageSwitch
   {
   protected : 
   	InformativeMessageSwitch(MessageStatus status);
   	~InformativeMessageSwitch();
   };

class DebuggingMessageSwitch : public MessageSwitch
   {
   protected : 
   	DebuggingMessageSwitch(MessageStatus status);
   	~DebuggingMessageSwitch();
   };

class WarningMessageSwitch : public MessageSwitch
   {
   protected : 
   	WarningMessageSwitch(MessageStatus status);
   	~WarningMessageSwitch();
   };

class ErrorMessageSwitch : public MessageSwitch
   {
   protected : 
   	ErrorMessageSwitch(MessageStatus status);
   	~ErrorMessageSwitch();
   };

//---------------------------------------------------------------

class EnableInformativeMessages : public InformativeMessageSwitch
   {
   public :
	EnableInformativeMessages () 
		: InformativeMessageSwitch (MessageStatus_On) {}
	~EnableInformativeMessages () {}
   };

class EnableErrorMessages : public ErrorMessageSwitch
   {
   public :
	EnableErrorMessages () 
		: ErrorMessageSwitch (MessageStatus_On) {}
	~EnableErrorMessages () {}
   };

class EnableWarningMessages : public WarningMessageSwitch
   {
   public :
	EnableWarningMessages () 
		: WarningMessageSwitch (MessageStatus_On) {}
	~EnableWarningMessages () {}
   };

class EnableDebuggingMessages : public DebuggingMessageSwitch
   {
   public :
	EnableDebuggingMessages () 
		: DebuggingMessageSwitch (MessageStatus_On) {}
	~EnableDebuggingMessages () {}
   };

//---------------------------------------------------------------

class DisableInformativeMessages : public InformativeMessageSwitch
   {
   public :
	DisableInformativeMessages () 
		: InformativeMessageSwitch (MessageStatus_Off) {}
	~DisableInformativeMessages () {}
   };

class DisableErrorMessages : public ErrorMessageSwitch
   {
   public :
	DisableErrorMessages () 
		: ErrorMessageSwitch (MessageStatus_Off) {}
	~DisableErrorMessages () {}
   };

class DisableWarningMessages : public WarningMessageSwitch
   {
   public :
	DisableWarningMessages () 
		: WarningMessageSwitch (MessageStatus_Off) {}
	~DisableWarningMessages () {}
   };

class DisableDebuggingMessages : public DebuggingMessageSwitch
   {
   public :
	DisableDebuggingMessages () 
		: DebuggingMessageSwitch (MessageStatus_Off) {}
	~DisableDebuggingMessages () {}
   };

#endif
