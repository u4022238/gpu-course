
/* SFILE_BEGIN */
#include <stdio.h>
/* SFILE_END */

#include <time.h>
#include <Utilities_cuda/cvar.h>
#include <Utilities_CU/Timer2.h>
#include <Utilities_cuda/showtime_dcl.h>
#include <string.h>

int scmp (TimingEvent *a, TimingEvent *b)
   {
   // Compare two wordentries
   char *wa = a->name_;
   char *wb = b->name_;
   return strcmp (wa, wb);
   }

int sfind (char *wa, TimingEvent *b)
   {
   // Compare a char * with a TimingEvent
   char *wb = b->name_;
   return strcmp (wa, wb);
   }


//---------------------------------------------------------

// Static members
bool  rhTimer2::timing_on = false;
TimeTable rhTimer2::ttab(scmp, sfind);

rhTimer2::rhTimer2 (const char *context)
   {
   // Turn on/off the messages temporarily during scope

   // Only do if timing is on
   if (!timing_on) return;

   // Record the context name
   strcpy (cstring_, context);

   // Now, put event in the table
   double ttime = ((double) clock()) / CLOCKS_PER_SEC;

   // Find an event in the table and record
   // First, see if this is known
   TimingEvent *ev = (TimingEvent *)ttab.find(context);
      
   // Unknown event
   if (!ev)
      {
      // Now create an event struct and insert
      ev = new TimingEvent (context);
      ttab.insert(ev);
      }

   // Update the depth of the event
   // Update the depth
   ev->depth_++;
   ev->numcalls_++;

   // Only record time, first time in
   if (ev->depth_ == 1) ev->event_time_ = ttime;
   }

rhTimer2::~rhTimer2 ()
   {
   // Only do if timing is on
   if (!timing_on) return;

   // Now write the message
   double ttime = ((double) clock()) / CLOCKS_PER_SEC;
   char *context = cstring_;

   // Find an event in the table and record
   // First, see if this is known
   TimingEvent *ev = (TimingEvent *)ttab.find(context);
      
   // Unknown event
   if (!ev)
      {
      // This is a mistake
      error_message("rhTimer2: Attempt to finish unknown event");
      return;
      }

   // Now, handle the event

   // End event
   ev->depth_--;

   // If depth is zero, then store elapsed time
   if (ev->depth_ == 0)
      {
      // Compute the used time
      double elapsed_time = (ttime - ev->event_time_);// /CLOCKS_PER_SEC;

      // Store the time
      ev->total_time_ += elapsed_time;
      }

   // But if it is less than zero, there is a mistake
   if (ev->depth_ < 0)
      {
      error_message ("rhTimer2: Too many ENDS for event \"%s\"\n", context);
      return;
      }
   }

void rhTimer2::print_results()
   {
   // Now, print out the table
   printf ("\n-------------------------------------------------------------------\n");
   printf ("Timing results from rhTimer2\n");
   Tbl_forall (TimingEvent *, ev, ttab)
      {
      double total_time = ev->total_time_;
      long ncalls = ev->numcalls_;
      double avg_time = total_time / ncalls;
      printf ("calls %9d : total %12.3f : avg %12.4e : %s\n", 
           ncalls, total_time, avg_time, ev->name_);
      } Tbl_endall
   printf ("-------------------------------------------------------------------\n");
   }

//---------------------------------------------------------------
