#ifndef ___KD_TREE2_H_
#define ___KD_TREE2_H_

#include <vector>
using namespace std;
#include <Utilities_CU/SmartPointer.h>
#include <Utilities_CU/utilities_CC.h>
#include <Utilities_CU/PriorityQueue.h>
#include <Utilities_CU/KDTree.h>


class KDTree2 : public KDTree
   {
   protected :
	
   	// The array of nodes
	int num_trees_;

	// The directions
	vector<rhFloatVector> directions_;

	//-------------------------------------
	//           Public methods
	//-------------------------------------

   public :

	// Constructors, etc
	KDTree2 (vector<rhFloatVector> &vecs, int numtrees, bool build = true);
  	~KDTree2 ();

	int find_nearest_neighbour (rhFloatVector goal);
   };

#endif
