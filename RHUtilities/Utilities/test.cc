
#undef BUILDING_RHUtilities_DLL

/* Test library functions */
#include <Utilities/utilities_c.h>
#include <Utilities/chisq_dcl.h>

#ifdef OBSOLETE
int main1 (int argc, char *argv[])
   {
   int i, j, count;
   int n     = atoi(argv[1]);
   int times = atoi(argv[2]);

   /* The algorithm to be run */
   void (*algorithm) (double **, int, double *) = gaussj;

   /* Test gaussian elimination */
   double **A = MATRIX (0, n-1, 0, n-1, double);
   double *b= VECTOR (0, n-1, double);
   double **Atemp = MATRIX (0, n-1, 0, n-1, double);
   double *btemp = VECTOR (0, n-1, double);

   /* Fill them out */
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
	 A[i][j] = rh_irandom()-0.5;

   /* Also the goal vector */
   for (i=0; i<n; i++)
      b[i] = rh_irandom()-0.5;

   /* 
    *  First my version
    */

   for (count=1; count<=times; count++)
      {
      /* Copy the values */
      for (i=0; i<n; i++)
         {
         btemp[i] = b[i];
         for (j=0; j<n; j++)
	    Atemp[i][j] = A[i][j];
         }

      /* Solve */
      algorithm (Atemp, n, btemp);
      }

   /* Print out the result */
   printf ("Result\n");
   for (i=0; i<n; i++)
      printf ("%13.5e\n", btemp[i]);

   /* Done */
   return 0;
   }
#endif
   

/* Test the inverse routine */
int main2 (int argc, char *argv[])
   {
   /* Test the inverse routine */
   int i, j;
   int n     = atoi(argv[1]);

   /* Test gaussian elimination */
   double **A = MATRIX (0, n-1, 0, n-1, double);
   double **Ainv = MATRIX (0, n-1, 0, n-1, double);

   /* Fill them out */
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
	 A[i][j] = rh_irandom()-0.5;

   /* Solve */
   matrix_inverse_0 (A, Ainv, n);

   /* Multiply out the product */
   matrix_prod_0 (A, Ainv, n, n, n, Ainv);

   /* Print out the result */
   printf ("Result\n");
   fprint_matrix ("%13.5e ", stdout, Ainv, 0, n-1, 0, n-1);

   /* Done */
   return 0;
   }


int main3 (int argc, char *argv[])
   {
   int i, j, count;
   int n     = atoi(argv[1]);
   int times = atoi(argv[2]);
   int alg   = atoi(argv[3]);

   /* Test gaussian elimination */
   double **A = MATRIX (0, n-1, 0, n-1, double);
   double **At = MATRIX (0, n-1, 0, n-1, double);
   double *b= VECTOR (0, n-1, double);
   double **Atemp = MATRIX (0, n-1, 0, n-1, double);
   double *btemp = VECTOR (0, n-1, double);

   /* Make a positive definite matrix */
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
	 A[i][j] = rh_irandom()+0.5;

   /* Must take its product with its transpose */
   transpose (A, At, 0, n-1, 0, n-1);
   matrix_prod_0 (A, At, n, n, n, A);

   /* Also the goal vector */
   for (i=0; i<n; i++)
      b[i] = rh_irandom()+0.5;
   
   /* Print out the inputs */
   fprintf (stdout, "A : \n");
   fprint_matrix ("%11.3e ", stdout, A, 0, n-1, 0, n-1);
   fprintf (stdout, "b : \n");
   fprint_vector ("%11.3e ", stdout, b, 0, n-1);

   /* 
    *  First my version
    */

   for (count=1; count<=times; count++)
      {
      /* Copy the values */
      for (i=0; i<n; i++)
         {
         btemp[i] = b[i];
         for (j=0; j<n; j++)
	    Atemp[i][j] = A[i][j];
         }

      /* Solve */

      /* Select the algorithm */
      switch (alg)
         {
         case 0 : solve_simple_0        (Atemp, btemp, btemp, n, n); break;
         case 1 : lin_solve_symmetric_0 (Atemp, btemp, btemp, n); break;
         case 2 : lin_solve_0           (Atemp, btemp, btemp, n); break;
         default : break;
         }
      }

   /* Print out the result */
   printf ("Result\n");
   fprint_vector ("%11.3e ", stdout, btemp, 0, n-1);

   /* Try multiplying it out */
   matrix_vector_prod_0 (A, btemp, n, n, btemp);
   printf ("Multiplied out\n");
   fprint_vector ("%11.3e ", stdout, btemp, 0, n-1);

   printf ("Difference\n");
   for(i=0; i<n; i++) btemp[i] -= b[i];
   fprint_vector ("%11.3e ", stdout, btemp, 0, n-1);

   /* Done */
   return 0;
   }

int main4 (int argc, char *argv[])
   {
   /* Test singular value decomposition */

   FILE *afile = stdout;

   int i, j, count;
   int m     = atoi(argv[1]);
   int n     = atoi(argv[2]);
   int times = atoi(argv[3]);
   int alg   = atoi(argv[4]);

   /* Test gaussian elimination */
   double **A     = MATRIX (0, m-1, 0, n-1, double);
   double **Atemp = MATRIX (0, m-1, 0, n-1, double);
   double **V     = MATRIX (0, n-1, 0, n-1, double);
   double *W      = VECTOR (0, n-1, double);

   /* Just test to see that the matrix has more rows than columns */
   if (m < n)
      {
      error_message ("Matrix must have more rows than columns.");
      exit (1);
      }

   /* Make a rh_random definite matrix */
   for (i=0; i<m; i++)
      for (j=0; j<n; j++)
	 A[i][j] = rh_irandom();

   printf ("Original:\n");
   fprint_matrix ("%11.3e ", afile, A, 0, m-1, 0, n-1);

   /* 
    *  Run the algorithm
    */

   for (count=1; count<=times; count++)
      {
      const int yes = 1;	/* Accumulate U */

      /* Copy the values */
      for (i=0; i<m; i++)
         for (j=0; j<n; j++)
	    Atemp[i][j] = A[i][j];

      /* Solve */

      /* Select the algorithm */
      switch (alg)
         {
         case 0 : svdcmp_0  (Atemp, m, n, W, V, yes); break;
         case 1 : svdcmp_0 (Atemp, m, n, W, V, yes); break;
         default : break;
         }
      }

   /* Print out the result */
   if (m < 20)
      {
      fprintf (afile, "U : \n");
      fprint_matrix ("%11.3e ", afile, Atemp, 0, m-1, 0, n-1);
      fprintf (afile, "V : \n");
      fprint_matrix ("%11.3e ", afile, V, 0, n-1, 0, n-1);
      fprintf (afile, "W : \n");
      fprint_vector ("%11.3e ", afile, W, 0, n-1);
      }

   /* Check */
   /* Transpose V */
   transpose (V, V, 0, n-1, 0, n-1);

   /* Multiply V by W */
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
	 V[i][j] *= W[i];

   /* Now multiply by Atemp */
   matrix_prod_0 (Atemp, V, m, n, n, Atemp);

   /* Print it out */
   printf ("Multiplied out:\n");
   fprint_matrix ("%11.3e ", afile, Atemp, 0, m-1, 0, n-1);
   
   /* Subtract A */
   for (i=0; i<m; i++)
      for (j=0; j<n; j++)
	 Atemp[i][j] = Atemp[i][j] - A[i][j];

   /* Print it out */
   printf ("Difference:\n");
   fprint_matrix ("%11.3e ", afile, Atemp, 0, m-1, 0, n-1);

   /* Statistics */
      {
      double error = 0.0;
      double errorsq = 0.0;
      double errormax = 0.0;
      int count = 0;
      double avge;
      double stdev;

      /* Collect statistics */
      for (i=0; i<m; i++)
         for (j=0; j<n; j++)
	    {
	    double err = fabs(Atemp[i][j]);
	    error += err;
	    errorsq += err*err;
	    count += 1;
	    if (err > errormax) errormax = err;
	    }

      /* Compute statistics */
      avge = error / count;
      stdev = sqrt(errorsq/count - avge*avge);

      /* Print out statistics */
      fprintf (afile, "Errors : \n");
      fprintf (afile, "   Maximum = %12.3e\n", errormax);
      fprintf (afile, "   Average = %12.3e\n", avge);
      fprintf (afile, "   St Dev  = %12.3e\n", stdev);
      }

   /* Done */
   return 0;
   }


int main5 (int argc, char *argv[])
   {
   /* Test jacobi */

   FILE *afile = stdout;

   int i, j;
   int n     = atoi(argv[1]);

   /* Test gaussian elimination */
   double **A     = MATRIX (0, n-1, 0, n-1, double);
   double **V     = MATRIX (0, n-1, 0, n-1, double);
   double *D      = VECTOR (0, n-1, double);

   /* Make a random symmetric matrix */
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
	 A[i][j] = A[j][i] = rh_irandom ()-0.5;

   /* Take the jacobi decomposition */
   jacobi_0 (A, n, D, V);

   /* Print out the vector */
   fprint_vector ("%11.3e ", afile, D, 0, n-1);

   /* Try multiplying out */
      {
      double **Vt = MATRIX (0, n-1, 0, n-1, double);
      transpose (V, Vt, 0, n-1, 0, n-1);
      
      /* Multiply by D */
      for (i=0; i<n; i++)
	 for (j=0; j<n; j++)
	    Vt[i][j] *= D[i];

      /* Multiply on left by V */
      matrix_prod_0 (V, Vt, n, n, n, Vt);

      /* Subtract A */
      for (i=0; i<n; i++)
	 for (j=0; j<n; j++)
	     Vt[i][j] -= A[i][j];

      printf ("Input\n");
      fprint_matrix ("%13.5e ", stdout, A, 0, n-1, 0, n-1);

      printf ("Result\n");
      fprint_matrix ("%13.5e ", stdout, Vt, 0, n-1, 0, n-1);

      FREE (Vt, 0);
      }

   /* Done */
   return 0;
   }

int main6 (int argc, char *argv[])
   {
   /* Remove hash */
   FILE *infile = rhfopen (argv[1], "rb");
   if (infile == NULL) 
      {
      error_message ("Can not open file \"%s\"\n", argv[1]);
      return 1;
      }
  
   while (1)
      {
      int n = getc (infile);
      if (n == EOF) break;
      putchar (n);
      }

   return 0;
   }

int main7 (int argc, char *argv[])
   {
      {
      double x = atof(argv[1]);
      int n = atoi (argv[2]);
      printf ("chisq (%f) = %.6f\n", x, invchisq(x, n));
      }

   return 0;
   }

int main8 (int argc, char *argv[])
   {
   int i;
   int n = atoi(argv[1]);

   /* Space for coefficients and the roots */
   fcomplex *a = VECTOR (0, n, fcomplex);
   fcomplex *r = VECTOR (0, n-1, fcomplex);

   /* Fill out the polynomial coefficients */
   for (i=0; i<=n; i++)
      a[i] = Complex (i+1.0, 0.0);

   /* Find its roots */
   complex_roots_0 (a, n, r, 1);

   /* Print them out */
   for (i=0; i<n; i++)
      fprintf (stdout, "(%20.12e, %20.12e)\n", r[i].r, r[i].i);

   /* Free */
   FREE (r, 0);
   FREE (a, 0);
   
   /* Return success */
   return 0;
   }

int main9 (int argc, char *argv[])
   {
   int i, j, k;

   int ilow = 2;
   int ihigh = ilow + 2;
   int jlow = 7;
   int jhigh = jlow + 3;
   int klow = -3; 
   int khigh = klow + 5;

   /* Make an array */
   int ***A = ARRAY3D (ilow, ihigh, jlow, jhigh, klow, khigh, int);

   int count = 0;
   for (i=ilow; i<=ihigh; i++)
      for (j=jlow; j<=jhigh; j++)
         for (k=klow; k<=khigh; k++)
	    A[i][j][k] = count++;

   for (i=ilow; i<=ihigh; i++)
      for (j=jlow; j<=jhigh; j++)
         for (k=klow; k<=khigh; k++)
	    printf ("%d\n", A[i][j][k]);

   /* Free it */
   FREE(A, ilow);

   /* Return success */
   return 0;
   }

int test_random(int argc, char *argv[])
   {
   // Outputs numbers from the random number generator
   int npoints, i;

   // Check that we are given arguments
   if (argc != 2)
      {
      fprintf(stderr, "Usage: test npoints\n");
      exit(1);
      }

   npoints = atoi(argv[1]);

#ifdef WIN32
   printf ("RAND_MAX = %ld\n", RAND_MAX);
#endif

   for (i=0; i<npoints; i++)
      printf ("%ld\n", rh_random());
 
   return 0;
   }

int main (int argc, char *argv[])
   {
   enable_informative_messages();
   return test_random(argc, argv);
   }
