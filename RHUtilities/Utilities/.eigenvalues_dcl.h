#ifndef _eigenvalues_c_dcl_
#define _eigenvalues_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void eigenvalues_3x3_base1, (
        double **M,
        fcomplex *ev
	));

#endif
