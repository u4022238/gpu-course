/* @(#)marqu.c	1.20 11/26/96 */
/* Routines for carrying out the Marquardt algorithm */

#include <math.h>
#include <Utilities/rh_util.h>
#include <Utilities/marquardt.h>
#include <Utilities/array_utils_dcl.h>
#include <Utilities/error_message_dcl.h>
#include <Utilities/svalue_dcl.h>
#include <Utilities/gaussj_dcl.h>
#include <Utilities/utilities_c.h>

/* Some local variables */
static double **weight;
static double *goal;
static double *mparam;
static double **covar;
static int nrows, ncols;
static int Num_Loops = 100;
static double Min_Incr = 0.05;
static rhBOOLEAN diag;

/*  Some forward declarations */
FUNCTION_DECL (static void (*gen_jacobian), (
   double *mparam,
   double **dyda,
   int nrows,
   int ncols
   ));

FUNCTION_DECL (static void (*function), (
   double *mparams,
   double *newvalue,
   int nrows,
   int ncols));

FUNCTION_DECL (static void (*new_position_action), (
   double *mparam,
   int ncols));

FUNCTION_DECL (static void mrqcof, (
   double **weight, 
   double **alpha, 
   double *beta, 
   double **dyda, 
   double *dy,
   int nrows, 
   int ncols,
   int diag));

FUNCTION_DECL (static double calculate_error, (double *del));

/* Routines for handling an interrupt to tell us to stop after next iteration */
#include <signal.h>
static rhBOOLEAN interrupt_pending = FALSE;

FUNCTION_DEF ( static void keybd_interrupt , (int errno))
   {
#ifndef WIN32
   if (errno == SIGTSTP) interrupt_pending = TRUE;
#endif
   }

FUNCTION_DEF (static void compute_error_vector, (
   double *mparams,	/* The parameters used for the computation */
   double *dy,		/* The error vector */
   int nrows, 		/* Number of elements in mparams */
   int ncols))		/* Number of elements in dy */
   {
   /* Determines what the error would be if atry were the new position */
   double *newvalue = VECTOR (0, nrows-1, double);
   int i;

   /* Call the function to compute the function */
   (*function) (mparams, newvalue, nrows, ncols);

   /* Now compute the error vector */
   for (i=0; i<nrows; i++)
      dy[i] = goal[i] - newvalue[i];
   
   /* Free the temporary */
   FREE (newvalue, 0);
   }

FUNCTION_DEF (static double calculate_error, (double *del))
   {
   /* Computes the weighted value of an error vector del */
   double error = 0.0;
   int i;

   /* Now compute the weighted error */
   if (diag)
      for (i=0; i<nrows; i++)
	 error += del[i] * del[i] * weight[0][i];
   else
      for (i=0; i<nrows; i++)
	 {
	 int j;
	 /* Add twice the off-diagonal error components */
	 for (j=0; j<i; j++)
	    error += 2.0 * del[i] * del[j] * weight[i][j];

	 /* Add once the diagonal contribution */
	 error += del[i] * del[i] * weight[i][i];
	 }

   /* The error computation is now complete */
   return (error);
   }
	 
FUNCTION_DEF (static int compute_covariance, (
   double **dyda, 		/* Jacobian */
   double *dy,			/* Error vector */
   double **weight,		/* Weight matrix or vector */
   rhBOOLEAN diag,		/* Tells whether weight matrix is diagonal */
   int nrows, int ncols,	/* Dimensions of the Jacobian matrix */
   double **covar		/* Returned covariance matrix.  Must already be
				 * allocated */
   ))
   {
   /* This is the Marquardt Minimization algorithm.  Please read the comment
    * in the book Numerical recipes, page 545.
    */
   double *beta;
   int rval;

   /* Return if covar is not allocated */
   if (covar == (double **)NULL) return 0;
   
   /* Allocate an array */
   beta = VECTOR (0, ncols-1, double);

   /* Calculate the matrices */
   mrqcof (weight, covar, beta, dyda, dy, nrows, ncols, diag);

   /* Use gaussj to invert the covar matrix */
   rval = gaussj_check_0(covar, ncols, beta);

   /* Free the temporary */
   FREE (beta, 0);

   /* Return the return value */
   return (rval);
   }

FUNCTION_DEF (static void mrqcof, (
   double **weight, 
   double **alpha, 
   double *beta, 
   double **dyda, 
   double *dy,
   int nrows, 
   int ncols,
   int diag))
   {
   /* Routine used by mrqmin to evaluate the linearized fitting matrix 
    * alpha[1..ncols][1..ncols] and vector beta[1..ncols] 
    * as in Cookbook, (14.4.8). */

   /* Initialize (symmetric) alpha, beta */
      {
      int j, k;
      for (j=0; j<ncols; j++)
         for (k=0; k<=j; k++) 
	    alpha[j][k] = beta[j] = 0.0;
      }

      /* Compute the alpha and beta matrices */

   if (diag)
      {
      /* Calculate J^T*J and J^T*dy */
      register int i;
      for (i=0; i<nrows; i++)
         {
         /* summation loop over all data */
  	 register int j;
         for (j=0; j<ncols; j++)
	    {
 	    register int k;
	    double wt = dyda[i][j]*weight[0][i];
	    for (k=0; k<=j; k++)
	       alpha[j][k] += wt*dyda[i][k];
	    beta[j] += dy[i]*wt;
	    }
         }
      }
   else
      {
      /* Compute  alpha = J^T * C * J and beta = J^T * C * dy */
      register int i;
      for (i=0; i<ncols; i++)
	 {
	 int k;
	 for (k=0; k<nrows; k++)
	    {
	    int j;
	    double JTC_i_k = 0.0;

	    /* Compute the i,k-th entry of J^T * C */
	    for (j=0; j<nrows; j++)
	       JTC_i_k += dyda[j][i] * weight[j][k];

	    /* Now multiply by J */
	    for (j=0; j<=i; j++)
	       alpha[i][j] += JTC_i_k * dyda[k][j];

	    /* Compute J^T*C*dy */
	    beta[i] += JTC_i_k * dy[k];
	    } 
	 }
      }

   /* Fill in the symmetric side */
      {
      register int j, k;
      for (j=1; j<ncols; j++)
         for (k=0; k<=j-1; k++) alpha[k][j] = alpha[j][k];
      }
   }

#define FAC   1.0e-6
#define DELTA 1.0e-6

FUNCTION_DEF (static void compute_jacobian, (
   double *mparam,
   double **dyda,
   int nrows, int ncols))
   {
   /* Computes the jacobian by approximation */
   int i, j;
   double *oldval = VECTOR (0, nrows-1, double);
   double *newval = VECTOR (0, nrows-1, double);

   /* Evaluate the function at the current point */
   (*function) (mparam, oldval, nrows, ncols);

   /* Now change the parameters one after another */
   for (j=0; j<ncols; j++)
      {
      /* Remember the value of the j-th parameter */
      double oldparam = mparam[j];

      /* Increment the j-th parameter */
      double delta = FAC * mparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      mparam[j] += delta;

      /* Now recompute the function */
      (*function) (mparam, newval, nrows, ncols);

      /* Now fill out the jacobian values */
      for (i=0; i<nrows; i++)
	 dyda[i][j] = (newval[i] - oldval[i]) / delta;
      
      /* Restore the value of mparam[j] */
      mparam[j] = oldparam;
      }

   /* Free the temporaries */
   FREE (oldval, 0);
   FREE (newval, 0);
   }
   
FUNCTION_DEF (static double marquardt_go, ())
   {
   /* Declare variables */
   int icount;
   double alamda = 1.0;
   double chisq = 0.0;
   rhBOOLEAN improvement = TRUE;

   /* Allocate a bunch of storage */
   double *da     	= VECTOR (0, ncols-1, double);   
   double **dyda  	= MATRIX (0, nrows-1, 0, ncols-1, double);
   double *dy     	= VECTOR (0, nrows-1, double);
   double *newparams 	= VECTOR (0, ncols-1, double);
   double *newdy    	= VECTOR (0, nrows-1, double);
   double **aprime 	= MATRIX (0, ncols-1, 0, ncols-1, double);
   double *beta		= VECTOR (0, ncols-1, double);
   double **alpha 	= MATRIX (0, ncols-1, 0, ncols-1, double);

   /* Now iterate */
   for (icount=0; icount<Num_Loops && improvement; icount++)
      {
      int j;
      double increment;
   
      /* Compute the error vector */
      compute_error_vector (mparam, dy, nrows, ncols);

      /* Compute an initial value of the error */
      chisq = calculate_error (dy);

      /* Output some debugging info */
      if (icount == 0)
         informative_message ("\tInitial error : chisq = %g, rms = %g", 
		chisq, sqrt(chisq/nrows));
 
      /* Compute the Jacobian */
      if (gen_jacobian)
         (*gen_jacobian) (mparam, dyda, nrows, ncols);
      else
         compute_jacobian (mparam, dyda, nrows, ncols);

#ifdef WRITE_JACOBIAN
      fprintf (stderr, "\nJacobian (%d x %d) : \n", nrows, ncols);
      fprint_matrix ("%11.3f ", stderr, dyda, 1, nrows, 1, ncols);
#endif

      /* Calculate the matrices */
      mrqcof (weight, alpha, beta, dyda, dy, nrows, ncols, diag);

      /* Now estimate new position with different alambda until improvement */
      do {
         double newchisq;
         int i;
	 /* Copy the values of alpha to aprime, the augmented alpha */
	 for (i=0; i<ncols; i++)
	    {
	    /* Copy beta */
      	    da[i] = beta[i];

	    /* Copy alpha */
	    for (j=0; j<ncols; j++)
	       aprime[i][j] = alpha[i][j];
	    }
	      
         /* Augment diagonal elements of aprime */
         for (j=0; j<ncols; j++)
            aprime[j][j] *= 1.0 + (alamda);

         /* Matrix solution.  Solution overwrites beta, now called da */
         if (! (lin_solve_symmetric_0(aprime, da, da, ncols)
                || solve_simple_0 (aprime, da, da, ncols, ncols)))
            {
            error_message ("Can not solve normal equations -- exiting");
            bail_out (2);
            }

         /* Compute the length of the increment */
         increment = 0.0;
         for (j=0; j<ncols; j++)
	    increment += da[j] * da[j];

         /* Calculate new error at this position */
            {
	    int i;

	    /* Add the delta to get new provisional parameter set */
            for (i=0; i<ncols; i++)
               newparams[i] = mparam[i] + da[i];

	    /* Compute the error vector at this value */
            compute_error_vector (newparams, newdy, nrows, ncols);

            /* Get the value of chisq resulting from this error vector */
            newchisq = calculate_error(newdy);
	    }

         /* Accept or reject it */
         if (newchisq < chisq)
            {
            /* Success, accept this new solution */
            alamda *= 0.1;
            chisq = newchisq;
   	
	    /* Accept the new parameter values */
	    for (i=0; i<ncols; i++) mparam[i] = newparams[i];
   
            /* Call any user function */
            if (new_position_action)
               (*new_position_action) (mparam, ncols);

	    /* Signal improvement */
	    improvement = TRUE;
            }
         else
            {
            /* Failure, increas alamda and return. */
            alamda *= 10.0;
	    improvement = FALSE;
            }
         } while ( ! improvement && increment >= Min_Incr && (alamda < 1e10));
        
      /* Output some debugging info */
      informative_message ("\talamda = %5.0e, chisq = %g, rms = %g", 
	 alamda, chisq, sqrt(chisq/nrows));

      /* Test for an interrupt */
      if (interrupt_pending) break;

      /* Test to see if the increment has been too small */
      if (increment < Min_Incr) break;
      }

   /* Compute the covariance matrix */
   if (covar)
      if (! compute_covariance(
		dyda, dy, weight, diag, nrows, ncols, covar))
         error_message ("Can not compute covariance matrix");

   /* Free the storage */
   FREE(da,0);
   FREE(dyda,0);
   FREE(dy,0);
   FREE(newparams,0);
   FREE(newdy,0);
   FREE(aprime,0);
   FREE(beta,0);	
   FREE(alpha,0);

   /* Return the value of the error */
   return chisq;
   }

FUNCTION_DEF (double marquardt_0, (Marquardt_info *mar))
   {
   /* Do marquardt iteration */

   double return_val;			/* The error of fit */
   rhBOOLEAN free_weights = FALSE;	/* Free if we allocated here */

   /* Pick the local variables off the marquardt descriptor structure */
   if (mar->Num_Loops > 0) Num_Loops = mar->Num_Loops;
   if (mar->Min_Incr >= 0.0) Min_Incr = mar->Min_Incr;
   nrows = mar->nrows;
   ncols = mar->ncols;
   weight = mar->weight;
   goal = mar->goal;
   mparam = mar->param;
   diag = mar->diag;
   covar = mar->covar;
   gen_jacobian = mar->gen_jacobian;
   function = mar->function;
   new_position_action = mar->new_position_action;

   /* Set up the interrupt_pending value */
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ keybd_interrupt);
#endif

   /* If the weight matrix is not known, then make it the identity matrix */
   if (! weight)
      {
      int i;
      weight = MATRIX (0, 0, 0, nrows-1, double);
      free_weights = TRUE;
      for (i=0; i<nrows; i++) weight[0][i] = 1.0;
      diag = TRUE;
      }

   /* Now that house-keeping is done, start the routine */
   return_val = marquardt_go();

   /* Free the weights */
   if (free_weights) FREE (weight, 0);

   /* Return success of failure */
   return return_val;
   }

FUNCTION_DEF (void marquardt_debug, (
   double **alpha, 
   double *beta))
   {
   /* Outputs debug info to test the new algorithm */
   int i, j;
   int d1 = 12;
   int d2 = 24;
   double **A = MATRIX (0, d1-1, 0, d1-1, double);
   double **BT = alpha+d1;
   double **C = MATRIX (0, d2-1, 0, d2-1, double);
   double **B = MATRIX (0, d1-1, 0, d2-1, double);
   double *K1 = VECTOR (0, d1-1, double);
   double *K2 = beta + d1;

   /* Transfer matrixes C and B */
   for (i=0; i<d2; i++)
      for (j=0; j<d2; j++)
   	 C[i][j] = alpha[i+d1][j+d1];

   for (i=0; i<d1; i++)
      for (j=0; j<d2; j++)
	 B[i][j] = alpha [i][j+d1];

   /* Invert C in place */
   if (! gaussj2_check_0 (C, d2, (double **) 0, 0))
      {  
      error_message ("Can not invert matrix");
      bail_out (2);
      }  

   /* Multiply it on the right by B */
   matrix_prod_0 (B, C, d1, d2, d2, B);

   /* Now multiply it into A */
   matrix_prod_0 (B, BT, d1, d2, d1, A);
   
   /* Change the sign and add the elements of alpha */
   for (i=0; i<d1; i++)
      for (j=0; j<d1; j++)
  	 A[i][j] = alpha[i][j] - A[i][j];

   /* Now do the same thing for the goal vector */
   matrix_vector_prod_0 (B, K2, d1, d2, K1);
   for (i=0; i<d1; i++)
      K1[i] = beta[i] - K1[i];

   /* Now output the two matrices */
   fprintf (stderr, "\nGOAL VECTOR\n");
   fprint_vector ("%10.3e ", stderr, K1, 0, d1-1);
   fprintf (stderr, "\nNORMAL MATRIX\n");
   fprint_matrix ("%10.3e ", stderr, A, 0, d1-1, 0, d1-1);

   /* Free the temporaries */
   FREE (A, 0);
   FREE (C, 0);
   FREE (B, 0);
   FREE (K1, 0);
   }

   
   
