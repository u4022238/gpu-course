/* @(#)determinant.c	1.4 20 Dec 1994 */
/* Routines for doing Higgins camera modelling stuff */

#include <math.h>
#include <Utilities/rh_util.h>
#include <Utilities/alloc.h>
#include <Utilities/cvar.h>
#include <Utilities/array_utils_dcl.h>

FUNCTION_DEF (static double det2_base1, ( double **A, int n))
   {
   /* Takes the determinant, destructive on matrix A */
   int i, j, k;
   double det = 1.0;

   /* Diagonalize the matrix row by row */
   for (i=1; i<n; i++)
      {
      /* Find the largest element in the row */
      double big=fabs(A[i][i]);
      int index = i;
      double temp;
      for (j=i+1;j<=n;j++)
	 {
         if ((temp=fabs(A[i][j])) > big) 
	    {
	    big = temp;
	    index = j;
	    }
	 }

      /* If this is zero, then determinant is zero */
      if (big == 0.0) return (0.0);

      /* Otherwise, swap this column with the i-th column */
      if (index != i)
	 {
	 swap_columns (A, i, n, index, i);
	 det = -det;
	 }

      /* Now use this element to sweep out the entries below it */
      for (k=i+1; k<=n; k++)
	 {
	 /* Eliminate element (k, i) */
	 double factor = A[k][i] / A[i][i];

	 /* Do the row operation */
	 for (j=i; j<=n; j++)
	    A[k][j] -= factor * A[i][j];
	 }
      }

   /* Now compute the determinant by multiplying diagonal entries */
   for (i=1; i<=n; i++)
      det *= A[i][i];

   /* Return the determinant value */
   return (det);
   }

FUNCTION_DEF ( double determinant_base1, (
        double **A,
        int n))
   {
   int i, j;
   double d;

   /* Declare a matrix to copy A to */
   double **Af = MATRIX (1, n, 1, n, double);

   /* Do the copying */
   for (i=1; i<=n; i++) for (j=1; j<=n; j++)
      Af[i][j] = A[i][j];

   /* Now do the lu decomposition of Af */
   d = det2_base1 (Af, n);

   /* Free the array */
   FREE (Af, 1);

   /* Return the value */
   return d;
   }

