/* @(#)svalue2.c	1.5 11/22/96 */
/*>>
 Singular value decomposition modelled on the algorithms given in
 " Numerical Recipes in C " (Cambridge University Press 1988)
 and in the book Golub and Van Loan, Matrix Computations.
<<*/

#include <Utilities/cvar.h>
#include <math.h>
#include <Utilities/rh_util.h>
#include <Utilities/alloc.h>
#include <Utilities/array_utils_dcl.h>
#include <Utilities/error_message_dcl.h>

/* Redefinition of isnan for Visual C++ */
#ifdef WIN32
#define isnan(x) _isnan(x)
#define isinf(x) _isinf(x)
#endif

#ifdef rhDEBUG
double **Atemp;
#endif

/*>> 
Singular value decomposition routines.
<<*/

#define NUMITER 30

static double at, bt, ct;
/* Pythag computes (sqrt(a*a + b*b)) without destructive over or under flow */
#define PYTHAG(a,b) ((at=fabs(a)) > (bt=fabs(b)) ?	\
	(ct=bt/at, at*sqrt(1.0+ct*ct)) :		\
	(bt ? (ct=at/bt,bt*sqrt(1.0+ct*ct)):0.0))

#define SIGN(a,b)	((b) >= 0.0 ? fabs(a) : -fabs(a))


FUNCTION_DEF (static double column_norm_base1, (double **a, int m, int n, int i))
   {
   /* Returns the column norm of an m x n matrix below position (i, i) */
   int k;

   /* First of all, scale the column elements to avoid overflow */
   double scale = 0.0;
   double scaleinv;
   double normsq = 0.0;

   /* Sum of absolute values in the column equals the scale */
   for (k=i; k<=m; k++) scale += fabs(a[k][i]);

   /* If all the values are zero, then skip */
   if (scale == 0.0)
      return 0.0;

   /* Scale the values and compute the norm */
   scaleinv = 1.0 / scale;

   /* Scale the i-th column to avoid overflow */
   for (k=i; k<=m; k++)
      {
      double val = a[k][i] * scaleinv;
      normsq += val * val;
      }

   /* Get the norm */
   return sqrt(normsq) * scale;
   }

FUNCTION_DEF (static double row_norm_base1, (double **a, int m, int n, int i))
   {
   /* Returns the column norm of an m x n matrix past position (i, i+1) */
   int k;

   /* First of all, scale the column elements to avoid overflow */
   double scale = 0.0;
   double scaleinv;
   double normsq = 0.0;
   int l = i+1;

   /* Sum of absolute values in the column equals the scale */
   for (k=l; k<=n; k++) scale += fabs(a[i][k]);

   /* If all the values are zero, then skip */
   if (scale == 0.0)
      return 0.0;

   /* Scale the values and compute the norm */
   scaleinv = 1.0 / scale;

   /* Scale the i-th column to avoid overflow */
   for (k=l; k<=n; k++)
      {
      double val = a[i][k] * scaleinv;
      normsq += val * val;
      }

   /* Get the norm */
   return sqrt(normsq) * scale;
   }

#ifdef rhDEBUG

FUNCTION_DEF (int check_nan_base1, (
   double **U,
   double **V,
   double *w,
   double *w2,
   int m, int n
   ))
   /* Check for NAN */
   {
   int i, j;
   int error_present = 0;

   /* Check matrix A */
   for (i=1; i<=m; i++)
      for (j=1; j<=n; j++)
         if (isnan (U[i][j])) 
            {
            error_message ("NaN in matrix a");
            error_present = 1;
	    break;
            }

   /* Check the matrix V */
   for (i=1; i<=n; i++)
      for (j=1; j<=n; j++)
         if (isnan (V[i][j])) 
            {
            error_message ("NaN in matrix V");
            error_present = 1;
	    break;
            }

   /* Check the diagonals */
   for (i=1; i<n; i++)
      if (isnan (w[i])) 
         {
         error_message ("NaN in vector w");
         error_present = 1;
	 break;
         }

   /* Check the diagonals */
   for (i=1; i<n; i++)
      if (isnan (w2[i])) 
         {
         error_message ("NaN in vector w2");
         error_present = 1;	
	 break;
         }

   /* Check matrix A */
   for (i=1; i<=m; i++)
      for (j=1; j<=n; j++)
         if (isinf (U[i][j])) 
            {
            error_message ("Inf in matrix a");
            error_present = 1;
	    break;
            }

   /* Check the matrix V */
   for (i=1; i<=n; i++)
      for (j=1; j<=n; j++)
         if (isinf (V[i][j])) 
            {
            error_message ("Inf in matrix V");
            error_present = 1;
	    break;
            }

   /* Check the diagonals */
   for (i=1; i<n; i++)
      if (isinf (w[i])) 
         {
         error_message ("Inf in vector w");
         error_present = 1;
	 break;
         }

   /* Check the diagonals */
   for (i=1; i<n; i++)
      if (isinf (w2[i])) 
         {
         error_message ("Inf in vector w2");
         error_present = 1;	
	 break;
         }

   /* If we made a mistake here, then give up */
   if (error_present) return 0;
   else return 1;
   }

FUNCTION_DEF (void check_svd_base1, (
   double **A,
   double **U,
   double *w,
   double *w2,
   double **V,
   int m, int n
   ))
   {
   /* Check the accuracy of the SVD result */
   int i, j;
   double maxdiff = 0.0;
   double **B = MATRIX (1, m, 1, n, double);
   double **Vt    = MATRIX (1, n, 1, n, double);

   /* First of all, check for nan */
   if (! check_nan (U, V, w, w2, m, n))
      {
      FREE (B, 1);
      FREE (Vt, 1);
      return;
      }

   /* Multiply out w/w2 * U */
   for (i=1; i<=m; i++)
      for (j=1; j<=n; j++)
         if (j == 0)
            B[i][j] = U[i][j]*w[j];
         else
            B[i][j] = U[i][j]*w[j] + U[i][j-1]*w2[j];

   /* Multiply out the product */
   transpose (V, Vt, 1, n, 1, n);
   matrix_prod (B, Vt, m, n, n, B);

   /* Now check */
   for (i=1; i<=m; i++)
      for (j=1; j<=m; j++)
         {
         double diff = rhAbs (B[i][j] - A[i][j]);
         if (diff > maxdiff) maxdiff = diff;
         }

   printf ("Maximum difference = %20.5e\n", maxdiff);

#  if 0
   if (maxdiff > 0.01)
      {
      printf ("Matrix A :\n");
      fprint_matrix ("%9.1f ", stdout, A, 1, m, 1, n);
      printf ("Matrix B :\n");
      fprint_matrix ("%9.1f ", stdout, B, 1, m, 1, n);
      }
#   endif

   /* Free the temporaries */
   FREE (B, 1);
   FREE (Vt, 1);
   }

#endif

FUNCTION_DEF (int diagonalize_bidiagonal_base1, (
   double *w,		/* The diagonal */
   double *w2,		/* The super-diagonal */
   int m, int n,	/* Dimensions */
   double **U,		/* Right hand transformations */
   double **V		/* Left hand transformations */
   ))
   {
   /******************************************************/
   /*   Diagonalization of the bidiagonal matrix.        */
   /******************************************************/

   int i, j, k;

   /* First, compute anorm */
   double anorm = 0.0;
   for (i=1; i<=n; i++)
      {
      double val = fabs(w[i]) + fabs(w2[i]);
      if (anorm < val) anorm = val;
      }

   /* Extract the singular values from the bottom */
   for (k=n; k>=1; k--)
      {
      int its;
      for (its = 1; its <=NUMITER; its++) /* Loop over allowed iterations */
         {
         int split = 1;

         /* Look for an unsplit region */
         int l;
         for (l=k; l>=1; l--)
            {
            double rv_plus_anorm, m_plus_anorm;

            /* Check for zero superdiagonal entry */
            /* Must at least find this when l = 1, since w2[i] is zero */
            /* Bug fix, RIH : Change of the convergence criteria */
            rv_plus_anorm = (double)(fabs(w2[l]) + anorm);
            if ((rv_plus_anorm == anorm) 
         	|| ((its==NUMITER) && (rv_plus_anorm <= 1.00001 * anorm))) 
               {
               split = 0;
               break;
               }

            /* Check for zero diagonal entry */
            m_plus_anorm = (double)(fabs(w[l-1]) + anorm);
            if ((m_plus_anorm == anorm) ||
          	((its==NUMITER) && (m_plus_anorm <= 1.00001 * anorm)))
               break; /* Bug fix, RIH */
            }

         /*
          * If we have hit a zero diagonal element, then we must split.
          * We sweep the non-zero element w2[l] out to the right.
          */
         if (split)
            {
	    /* At this point, w[l-1] = 0 and w2[l] != 0 */
            double c =  0.0;
            double s = -1.0;

#	    ifdef rhDEBUG
  	    printf ("Before split between (%d, %d)\n", l, k);
	    check_svd (Atemp, U, w, w2, V, m, n);
#           endif

            /* Sweep the zero element across the matrix */
            for (i=l; i<=k; i++)
               {
	       /* Finish the multiplication from the previous iteration */
               double f = -s*w2[i];  /* Entry in position [l-1][i] */
	       w2[i] *= c;

               if ((double)(fabs(f) + anorm) != anorm) /* Bug fix, RIH */
                  {
                  double g = w[i];
                  double h = PYTHAG(f, g);
                  c = g/h;
                  s = f/h;
                  w[i] = h;
                  for (j=1; j<=m; j++)
                     {
	             double t0 = U[j][l-1];
       		     double t1 = U[j][i];
       		     U[j][l-1] = t0*c - t1*s;
       		     U[j][i]   = t0*s + t1*c;
       		     }
    		  }

	       else /* Entry has been cleaned out */
		  break;
               }

#	    ifdef rhDEBUG
  	    printf ("After split\n");
	    check_svd (Atemp, U, w, w2, V, m, n);
#	    endif
     	    }

         /*----------------------------------------------------------
   	  * At this time, the region between l and k bounds a maximal
   	  * unsplittable region.
   	  *---------------------------------------------------------*/

         /* Check for convergence */
  	 if ( l == k)
     	    {
     	    /* Convergence */
     	    if (w[k] < 0.0)
               {
               /* Singular value is made non-negative */
               w[k] = -w[k];
               for (j = 1; j<=n; j++) V[j][k] = (-V[j][k]);
               }
            break;
            }

         /* Check for too many iterations */
         if (its >= NUMITER) return 0;

         /* Shifting */
            {
            /* First, find the eigenvalue of bottom minor */

            /* Components of a^T * a */
            double amm = w[k-1]*w[k-1] + w2[k-1]*w2[k-1];
            double ann = w[k]*w[k] + w2[k]*w2[k];
            double amn = w[k-1]*w2[k];

            /* Compute the eigenvalue (G&VL Algorithm 8.2.2) */
            double d = (amm - ann)/2.0;
            double mu = ann - amn*amn / (d + SIGN(PYTHAG(d, amn), d));

            /* Now determine the initial sweep */
            double a11 = w[l]*w[l];
            double a12 = w[l]*w2[l+1];

            /* Specific elements in the matrix being swept.
             * They are in the configuration
             *
             *    y   z
             *    f   g
             *    0   h
             * 
             * for the column operations, and the transposed locations
             * for the row operations 
             */

            double y = a11 - mu;
            double z = a12;
            double f = w[l];
            double g = w2[l+1];

            /* Sweep out */
            for (i=l; i<k; i++)
               {
               /* Get the relevant parts of the matrix */
               double h = w[i+1];

               /********************/
               /* Column Operation */
               /********************/

               /* Get the sine and cosine of Givens rotation */
               double norm = PYTHAG(y,z);
               double c = y/norm;
               double s = z/norm;

               /* Multiply */
               y = c*f + g*s;
               z = s*h;
               f = c*g - f*s;
               g = c*h;

               /* Accumulate the product */
                  {
                  int jj;
                  for (jj = 1; jj <= n; jj++)
                     {
                     double t0 = V[jj][i];
                     double t1 = V[jj][i+1];
                     V[jj][i]   = t0*c + t1*s;
                     V[jj][i+1] = t1*c - t0*s;
                     }
                  }

               /* Reset off-diagonal value */
               if (i > l) w2[i] = norm;

               /********************/
               /*   Row Operation  */
               /********************/

               /* Now, same thing for left transformation */
               /* Get the next off-diagonal */
               if (i == k-1) h = 0.0;
               else h = w2[i+2];

               /* Get the sine and cosine of Givens rotation */
               if ((norm = PYTHAG(y,z)) > 0.0)
		  {
                  c = y/norm;
                  s = z/norm;
		  }
	      else
		  {
		  c = 1.0;
		  s = 0.0;
		  }

               /* Multiply */
               y = c*f + g*s;
               z = s*h;
               f = c*g - f*s;
               g = c*h;

               /* Accumulate the product */
                  {
                  int jj;
                  for (jj=1; jj<=m; jj++)
                     {
                     double t0 = U[jj][i];
                     double t1 = U[jj][i+1];
                     U[jj][i]   = t0*c + t1*s;
                     U[jj][i+1] = t1*c - t0*s;
                     }
 	          }

               /* Reset diagonal value */
               w[i] = norm;
               }

            /* Write back the matrix elements */
            w[k] = f;
            w2[k] = y;
            }
         }
      }

   /* Successful diagonalization */
   return 1;
   }

FUNCTION_DEF (void reduce_to_bidiagonal_base1, (
   double **a,		/* The input matrix, also the left hand transform */
   int m, int n,	/* Its dimensions */
   double **v,		/* Right hand transformation */
   double *w,		/* Diagonal */
   double *w2		/* Superdiagonal */
   ))
   {
   /* Householder reduction to bidiagonal form */

   int i, j, k;

   for (i=1; i<=n; i++)
      {
      /******************************************************/
      /* First, row Householder transform to clear column i */
      /******************************************************/

      /* Get the sign of the leading component */
      int asign = a[i][i] > 0.0 ? 1 : -1;

      /* Determine the householder vector */
      double norm = column_norm (a, m, n, i);

      /* Store the diagonal value */
      w[i] = -asign*norm;

      /* Store the householder vector */
      if (norm > 0.0)
	 {
	 /* Use algorithm 5.1.1 of G & VL, but normalize so that norm = 1 */
	 a[i][i] = a[i][i]/norm + asign;
	 for (k=i+1; k<=m; k++)
	    a[k][i] /= norm;
	 }
      else
         {
         /* Store a zero vector */
	 a[i][i] = 2.0;
         for (k=i+1; k<=m; k++)
            a[k][i] = 0.0;
         }

      /* Apply the householder transformation to rows.
       * See Golub & VL, algorithm 5.1.2
       */
         {
         /* Compute the value of -2 / vv^t vv */
         double beta = -1.0/rhAbs(a[i][i]);
   
         /* Now, clear out the column */
         for (j=i+1; j<=n; j++)
            {
            double wj = 0.0;
            for (k=i; k<=m; k++) wj += a[k][i] * a[k][j];
            wj = wj*beta;
            for (k=i; k<=m; k++) a[k][j] += wj*a[k][i];
            }
	 }

      /*********************************************/
      /* Next transformations to clear out the row */
      /*********************************************/
      if (i+1 <= n)
	 {
	 /* This is the column that we start clearing from */
	 int l = i+1;

         /* Get the sign of the leading component */
         int asign = a[i][l] > 0.0 ? 1 : -1;

         /* Determine the householder vector */
         double norm = row_norm (a, m, n, i);

         /* Store the diagonal value */
         w2[l] = -asign * norm;

         /* Store the householder vector */
  	 if (norm > 0.0)
	    {
            a[i][l] = a[i][l]/norm + asign;
	    for (k=l+1; k<=n; k++)
	       a[i][k] /= norm;
	    }
	 else
	    {
	    a[i][l] = 2.0;
	    for (k=l+1; k<=n; k++)
	       a[i][k] = 0.0;
	    }

         /* Apply the householder transformation to rows.
          * See Golub & VL, algorithm 5.1.2
          */
            {
            /* Compute the value of -2 / vv^t vv */
            double beta = -1.0 / rhAbs(a[i][l]);

            /* Now, clear out the rows */
            for (j=l; j<=m; j++) 
	       {
               double wj = 0.0;
  	       for (k=l; k<=n; k++) wj += a[i][k] * a[j][k];
	       wj = wj*beta;
	       for (k=l; k<=n; k++) a[j][k] += wj*a[i][k];
	       }
	    }
	 }
      }

   /******************************************************/
   /*   Accumulation of the orthogonal matrices          */
   /******************************************************/

   /******************************/
   /* Right-hand transformations */
   /******************************/

   /* First of all, make the matrix V into the identity */
   for (i=1; i<=n; i++)
      {
      for (j=1; j<=n; j++)
	 v[i][j] = 0.0;
      v[i][i] = 1.0;
      }

   /* Now accumulate the products */
   for (i=n-1; i>=1; i--)
      {
      int l = i+1;
      double beta = -1.0 / rhAbs(a[i][l]);

      /* Now, clear out the rows */
      for (j=l; j<=n; j++)
         {
         double wj = 0.0;
         for (k=l; k<=n; k++) wj += a[i][k] * v[k][j];
         wj = wj*beta;
         for (k=l; k<=n; k++) v[k][j] += wj*a[i][k];
	 }
      }

   /******************************/
   /* Left-hand transformations. */
   /******************************/

   /* Zero out the top half of the matrix */
   for (i=1; i<n; i++)
      for (j=i+1; j<=n; j++)
	 a[i][j] = 0.0;

   /* Multiply the matrices in inverse order */
   for (i=n; i>=1; i--)
      {
      double v0 = a[i][i];
      double beta = -1.0 / rhAbs(v0);

      /* Fix up the columns past the current column */
      for (j=i+1; j<=n; j++)
         {
         double wj = 0.0;
         for (k=i+1; k<=m; k++) wj += a[k][i]*a[k][j];
         wj = wj * beta;
         for (k=i; k<=m; k++) a[k][j] += wj * a[k][i];
         }

      /* Fix up the current column */
         {
         double fac = beta * v0;
         for (j=i; j<=m; j++) a[j][i] *= fac;
              a[i][i] += 1.0;
         }
      }
   }

FUNCTION_DEF ( int svdcmp_base1, (
        double **a,
        int m,
        int n,
        double *w,
        double **v))
   {
   /* Given a matrix a[1..m][1..n], this routine computes its singular value
      decomposition, A = U.W.V^T.  The matrix U replaces a on output.  
      The diagonal matrix of singular values is output as a vector w[1..n].
      The matrix V (not the transpose V^T) is output as v[1..n][1..n].
      m must be greater or equal to n;  If it is smaller, then a should
      be filled up to square with zero rows.

      The routine returns 1 on success, 0 on failure.  In case of failure, 
      a good thing to do is to permute the rows and try again.
   */

   /* Assume successful completion */
   int success = 1;

   /* Make a temporary vector to hold the superdiagonal */
   double *w2;

#  ifdef rhDEBUG
   /* Print out the matrix */
   fprintf (stderr, "SVD input matrix\n");
   fprint_matrix ("%12.3e", stderr, a, 1, m, 1, n);
#  endif

   /* Test the input matrix */
      {
      int i, j;
      for (i=1; i<m; i++)
	 for (j=1; j<n; j++)
	 {
	 if (isinf(a[i][j]))
	    {
	    error_message ("Error : Input to svdcmp has Inf entries");
	    bail_out(2);
	    }
	 if (isnan(a[i][j]))
	    {
	    error_message ("Error : Input to svdcmp has NaN entries");
	    bail_out(2);
	    }
	 }
      }

#  ifdef rhDEBUG
      {
      extern double **Atemp;
      Atemp = MATRIX (1, m, 1, n, double);

      /* Copy the matrix */
         {
         int i, j;
         for (i=1; i<=m; i++)
            for (j=1; j<=n; j++)
	       Atemp[i][j] = a[i][j];
         }
      }
#  endif

   /* Check the bounds of the array */
   if (m < n)
      {
      error_message ("SVDCMP : matrix has fewer rows (%d) than columns (%d)",
         m, n);
      return 0;
      }  

   /* Make an array for the super-diagonal elements of the bidiagonal form */
   w2 = VECTOR(1,n,double);
   w2[1] = 0.0;		/* First column is zero */

   /* reduce to bidiagonal form */
   reduce_to_bidiagonal (a, m, n, v, w, w2);

#  ifdef rhDEBUG
   /* Check the result */ 
   printf ("After SVD, step 1\n");
   check_svd (Atemp, a, w, w2, v, m, n);
#  endif

   /* Now do diagonalization */
   success = diagonalize_bidiagonal (w, w2, m, n, a, v);
   if (! success)
      error_message ("SVD sweep failed");

#  ifdef rhDEBUG
   /* Check the result */ 
   printf ("After SVD, step 2\n");
   check_svd (Atemp, a, w, w2, v, m, n);

   /* Free the temporary */
   FREE (Atemp, 1);
#  endif

   /* Test the output to see that we made no mistake */
      {
      int i, j;

      /* First, check A */
      for (i=1; i<m; i++)
	 for (j=1; j<n; j++)
	    if (isnan(a[i][j]))
	       {
	       error_message (
			"Internal error :Output of svdcmp has NaN entries");
	       bail_out(2);
	       }

      /* First, check A */
      for (i=1; i<n; i++)
	 for (j=1; j<n; j++)
	    if (isnan(v[i][j]))
	       {
	       error_message (
			"Internal error :Output of svdcmp has NaN entries");
	       bail_out(2);
	       }

      /* Then check w */
      for (i=1; i<n; i++)
	 if (isnan(w[i]))
	    {
	    error_message ("Internal error :Output of svdcmp has NaN entries");
	    bail_out(2);
	    }

      /* First, check A */
      for (i=1; i<m; i++)
	 for (j=1; j<n; j++)
	    if (isinf(a[i][j]))
	       {
	       error_message (
			"Internal error :Output of svdcmp has Inf entries");
	       bail_out(2);
	       }

      /* First, check A */
      for (i=1; i<n; i++)
	 for (j=1; j<n; j++)
	    if (isinf(v[i][j]))
	       {
	       error_message (
			"Internal error :Output of svdcmp has Inf entries");
	       bail_out(2);
	       }

      /* Then check w */
      for (i=1; i<n; i++)
	 if (isinf(w[i]))
	    {
	    error_message ("Internal error :Output of svdcmp has Inf entries");
	    bail_out(2);
	    }
      }

   /* Free the temporaries */
   FREE (w2, 1);

   /* Return success */
   return (success);
   }


