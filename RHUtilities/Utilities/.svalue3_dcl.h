#ifndef _svalue3_c_dcl_
#define _svalue3_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (int check_nan_base1, (
   double **U,
   double **V,
   double *w,
   double *w2,
   int m, int n
   ));

EXTERN_C_FUNCTION (void check_svd_base1, (
   double **A,
   double **U,
   double *w,
   double *w2,
   double **V,
   int m, int n
   ));

EXTERN_C_FUNCTION (int diagonalize_bidiagonal_base1, (
   double *w,		/* The diagonal */
   double *w2,		/* The super-diagonal */
   int m, int n,	/* Dimensions */
   double **U,		/* Right hand transformations */
   double **V,		/* Left hand transformations */
   int accumulate_u	/* Whether to accumulate u or not */
   ));

EXTERN_C_FUNCTION (void reduce_to_bidiagonal_base1, (
   double **a,		/* The input matrix, also the left hand transform */
   int m, int n,	/* Its dimensions */
   double **v,		/* Right hand transformation */
   double *w,		/* Diagonal */
   double *w2,		/* Superdiagonal */
   int accumulate_u	/* Whether to accumulate or not */
   ));

EXTERN_C_FUNCTION ( int svdcmp_base1, (
        double **a,
        int m,
        int n,
        double *w,
        double **v,
	int accumulate_u
	));

#endif
