/*
 * Module Purpose: Contains the standard error routine used by the parser.
 *
 * Creation Date:  87/10/23
 *
 * Compiler Version:
 *
 * Hardware Used:  SUN 3
 *
 * Operating Systems:  UNIX -- SUN Release 3.4
 *
 *
 * Class 3: General Electric Proprietary Data
 * Copyright (C) 1987 General Electric Company
 *
 */

#include <Utilities/cvar.h>
#include <stdio.h>
/* SFILE_BEGIN */
#include <Utilities/rh_util.h>
/* SFILE_END */

#ifndef CPARSE_RIH
#   include <stdarg.h>
#endif

static rhBOOLEAN output_informative_messages = FALSE;
static rhBOOLEAN output_warning_messages = TRUE;
static rhBOOLEAN output_error_messages = TRUE;
static rhBOOLEAN output_debugging_messages = FALSE;
static FILE *error_file = (FILE *)0;
static FILE *warning_file = (FILE *)0;
static FILE *debugging_file = (FILE *)0;
static FILE *info_file = (FILE *)0;

/* Routines for setting the error enable flags */

FUNCTION_DEF (void set_error_output_file, (FILE *afile))
   {
   error_file = afile;
   }

FUNCTION_DEF (FILE *error_output_file, ())
   {
   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   return error_file;
   }

FUNCTION_DEF (void set_info_output_file, (FILE *afile))
   {
   info_file = afile;
   }

FUNCTION_DEF (FILE *info_output_file, ())
   {
   /* Set default for info_file */
   if (info_file == (FILE *) 0) info_file = stdout;

   return info_file;
   }

FUNCTION_DEF (void set_warning_output_file, (FILE *afile))
   {
   warning_file = afile;
   }

FUNCTION_DEF (FILE *warning_output_file, ())
   {
   /* Set default for warning_file */
   if (warning_file == (FILE *) 0) warning_file = stderr;

   return warning_file;
   }

FUNCTION_DEF (void set_debugging_output_file, (FILE *afile))
   {
   debugging_file = afile;
   }

FUNCTION_DEF (FILE *debugging_output_file, ())
   {
   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   return debugging_file;
   }

FUNCTION_DEF ( rhBOOLEAN enable_debugging_messages, ())
   {
   rhBOOLEAN oldvalue = output_debugging_messages;

   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   output_debugging_messages = TRUE;
   // fprintf (debugging_file, "enable_debugging_messages\n");
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN enable_informative_messages, ())
   { 
   rhBOOLEAN oldvalue = output_informative_messages;
   output_informative_messages = TRUE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN enable_warning_messages, ())
   { 
   rhBOOLEAN oldvalue = output_warning_messages;
   output_warning_messages = TRUE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN enable_error_messages, ())
   { 
   rhBOOLEAN oldvalue = output_error_messages;
   output_error_messages = TRUE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN disable_informative_messages, ())
   { 
   rhBOOLEAN oldvalue = output_informative_messages;
   output_informative_messages = FALSE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN disable_warning_messages, ())
   { 
   rhBOOLEAN oldvalue = output_warning_messages;
   output_warning_messages = FALSE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN disable_error_messages, ())
   { 
   rhBOOLEAN oldvalue = output_error_messages;
   output_error_messages = FALSE; 
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN disable_debugging_messages, ())
   {
   rhBOOLEAN oldvalue = output_debugging_messages;
   output_debugging_messages = FALSE;
   return oldvalue;
   }

FUNCTION_DEF ( rhBOOLEAN warning_messages_enabled, ())
   { return output_warning_messages; }

FUNCTION_DEF ( rhBOOLEAN informative_messages_enabled, ())
   { return output_informative_messages; }

FUNCTION_DEF ( rhBOOLEAN error_messages_enabled, ())
   { return output_error_messages; }

FUNCTION_DEF ( rhBOOLEAN debugging_messages_enabled, ())
   { return output_debugging_messages; }

/*>>
Routines for outputting the messages.
<<*/

FUNCTION_DEF ( void debugging_message, (const char* fmt, ... /* DOTDOTDOT*/))
   {
   /* Get the message and print the specific error message */

   /* Set default for debugging_file */
   if (debugging_file == (FILE *) 0) debugging_file = stdout;

   /* Check to see whether we should output the message */
   if (! output_debugging_messages) return;

   /* Output the message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (debugging_file, fmt, args);

      /* Print a line feed */
      fprintf (debugging_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (debugging_file);
   }

FUNCTION_DEF ( void informative_message, (const char* fmt, DOTDOTDOT))
   {
   /* Get the message and print the specific error message */

   /* Set default for info_file */
   if (info_file == (FILE *) 0) info_file = stdout;

   /* Check to see whether we should output the message */
   if (! output_informative_messages) return;

   /* Output the message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (info_file, fmt, args);

      /* Print a line feed */
      fprintf (info_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (info_file);
   }

FUNCTION_DEF ( void warning_message, (const char* fmt, DOTDOTDOT))
   {
   /* Print the general message header */

   /* Set default for warning_file */
   if (warning_file == (FILE *) 0) warning_file = stderr;

   /* Check to see whether we should output the message */
   if (! output_warning_messages) return;

   fprintf (warning_file, "Warning : ");

   /* Get the message and print the specific error message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (warning_file, fmt, args);

      /* Print a line feed */
      fprintf (warning_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (warning_file);
   }

FUNCTION_DEF ( void error_message, (const char* fmt, DOTDOTDOT))
   {
   /* Print the general message header */

   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   /* Check to see whether we should output the message */
   if (! output_error_messages) return;

   fprintf (error_file,"Error : ");

   /* Get the message and print the specific error message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (error_file, fmt, args);

      /* Print a line feed */
      fprintf (error_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (error_file);
   }

FUNCTION_DEF ( void fatal_error_message, (const char* fmt, DOTDOTDOT))
   {

   /* Set default for error_file */
   if (error_file == (FILE *) 0) error_file = stderr;

   /* Print the general message header */
   fprintf (error_file,"Fatal error ...\n");

   /* Get the message and print the specific error message */
      {  
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      vfprintf (error_file, fmt, args);

      /* Print a line feed */
      fprintf (error_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }  

   /* Exit the program */
   fprintf (error_file, "\t... Exiting ...\n");
   fflush (error_file);
   bail_out (2);
   }
   
