#include <math.h>
#include <Utilities/alloc.h>
#include <Utilities/cvar.h>

#define SWAP(a,b) {double tempr=(a);(a)=(b);(b)=tempr;}

static void print_complex_data_base1 ( double *data, int n)
   {
   /* Prints the data */
   int i;
   for (i=1; i<=n; i+=2)
      printf ("\t%3d : (%12.4f, %12.4f) -> %12.4f\n", 
	      (i+1)/2, data[i], data[i+1], data[i]-data[i+1]);
   }

FUNCTION_DEF ( void four1, (
        double *data,
	     int istart,		/* Start index for four1 */
        int nn,
        int sign))
   {
   int blocksize = 1;
   double M_2PI = 2.0 * M_PI;
   int n = nn << 1;

   /* Start and end array indices */
   int iend = istart + n - 1;

   /* Bit reversal */
      {
      int i, j;

      for (i=istart, j=istart; i<iend; i+=2) 
         {
	      int m = nn;
         if (j > i) 
            {
            SWAP(data[j],data[i]);
            SWAP(data[j+1],data[i+1]);
            }
         while (m >= 2 && j+1-istart > m) 
	         {
            j -= m;
            m >>= 1;
            }
         j += m;
         }
      }

   /* Butterflies */

#define SPECIAL2
#ifdef SPECIAL2
   /* Special cases of blocksize 2 */
   blocksize = 2;
   if (blocksize < n)
      {
      int i;
      int step = blocksize << 1;
      for (i=istart; i<=iend; i+=step) 
         {
         /* Butterfly between nodes i and j */
         int j = i + blocksize;

         /* Temporary for data */
         double tempr = data[j];
         double tempi = data[j+1];

         /* data[j] = data[i] - data[j] */
         data[j]      = data[i]   - tempr;
         data[j+1]    = data[i+1] - tempi;

         /* data[i] = data[i] + data[j] */
         data[i]     += tempr;
         data[i+1]   += tempi;
         }
      }

#define SPECIAL4
#ifdef SPECIAL4

   /* Block size 4 */
   blocksize = 4;
   if (blocksize < n)
      {
      int i;
      int step = blocksize << 1;
      for (i=istart; i<=iend; i+=step) 
         {
         /* Butterfly between nodes i and j */
         int j = i + blocksize;
     
         /* Multiply data[j] by w^k; k = m-i */
         double tempr = data[j];
         double tempi = data[j+1];

         /* data[j] = data[i] - w^k*data[j] */
         data[j]      = data[i]   - tempr;
         data[j+1]    = data[i+1] - tempi;

         /* data[i] = data[i] + w^k*data[j] */
         data[i]     += tempr;
         data[i+1]   += tempi;
         }

      for (i=istart+2; i<=iend; i+=step) 
         {
         /* Butterfly between nodes i and j */
         int j = i + blocksize;

         /* Multiply data[j] by w^k; k = m-i */
         double tempr = -sign*data[j+1];
         double tempi =  sign*data[j];

         /* data[j] = data[i] - w^k*data[j] */
         data[j]      = data[i]   - tempr;
         data[j+1]    = data[i+1] - tempi;

         /* data[i] = data[i] + w^k*data[j] */
         data[i]     += tempr;
         data[i+1]   += tempi;
         }
      }

#endif /* SPECIAL4 */
#endif /* SPECIAL2 */

   /* Remaining block sizes */
   for (blocksize = 2*blocksize; blocksize < n; blocksize = blocksize << 1)
      {
      int m;
      double theta = sign*M_2PI/blocksize;
      double wtemp = sin(0.5*theta);
      double wpr   = -2.0*wtemp*wtemp;	/* cos(theta) - 1 */
      double wpi   = sin(theta);
      double wr    = 1.0;
      double wi    = 0.0;

      for (m=istart; m<blocksize/2+istart-1; m+=2) 
	      {
	      int i;
         int step = blocksize << 1;
         for (i=m; i<=iend; i+=step) 
	         {
	         /* Butterfly between nodes i and j */
            int j = i + blocksize;

	         /* Multiply data[j] by w^k; k = m-i */
            double tempr = wr*data[j]   - wi*data[j+1];
            double tempi = wr*data[j+1] + wi*data[j];

	         /* data[j] = data[i] - w^k*data[j] */
            data[j]      = data[i]   - tempr;
            data[j+1]    = data[i+1] - tempi;

	         /* data[i] = data[i] + w^k*data[j] */
            data[i]     += tempr;
            data[i+1]   += tempi;
            }

         for (i=m+blocksize/2; i<=iend; i+=step) 
	         {
	         /* Butterfly between nodes i and j */
            int j = i + blocksize;

	         /* Multiply data[j] by w^k; k = m-i */
            double tempr = -sign *(wi*data[j]   + wr*data[j+1]);
            double tempi = -sign *(wi*data[j+1] - wr*data[j]);

	         /* data[j] = data[i] - w^k*data[j] */
            data[j]      = data[i]   - tempr;
            data[j+1]    = data[i+1] - tempi;

	         /* data[i] = data[i] + w^k*data[j] */
            data[i]     += tempr;
            data[i+1]   += tempi;
            }

	      /* Compute the next power of w^k */
	         {
	         double newwr = wr*wpr - wi*wpi + wr;
            wi = wi*wpr + wr*wpi + wi;
	         wr = newwr;
	         }
         }
      }

   /* If this is the inverse transform, divide by nn */
   if (sign == -1)
      {
      int i;
      double nninv = 1.0 / nn;
      for (i=istart; i<=iend; i++)
	      data[i] *= nninv;
      }
   }

FUNCTION_DEF ( void fourier_filter_base1, (
   double *signal,	/* The inpute real data */
   int nn,		/* Number of data points (must be power of 2) */
   int numtaps)		/* Highest numbered non-zero Fourier bin */
   )
   {
   /* Filters the data by truncating the Fourier spectrum to numtaps places */
   int i;

   /* Declare an array to hold the Fourier data */
   double *data = VECTOR (1, 2*nn, double);

   /* Repack the data */
   for (i=1; i<=nn; i++)
      {
      data[2*i-1] = signal[i];	/* Real part */
      data[2*i]   = 0.0;	/* Imaginary part */
      }

   /* Now carry out the FFT */
   four1 (data, 1, nn, 1);

   /* Set the high bins to zero */
   for (i=2*numtaps+3; i<=2*(nn-numtaps); i++)
      data[i] = 0.0;

   /* Now do the inverse transform */
   four1 (data, 1, nn, -1);

   /* Now unpack the data */
   for (i=1; i<=nn; i++)
      signal[i] = data[2*i-1];

   /* Free the data */
   FREE (data, 1);
   }

FUNCTION_DEF ( void correlation_base1, (
   double *s1, double *s2,	/* Two real input signals */
   double *corr,		/* The correlated signal */
   int nn)			/* Number of data points (must be power of 2) */
   )
   {
   /* Computes the correlation of two signals */
   int i;

   /* Declare an array to hold the Fourier data */
   double *d1 = VECTOR (1, 2*nn, double);
   double *d2 = VECTOR (1, 2*nn, double);
   double *c  = VECTOR (1, 2*nn, double);

   /* Pack the data */
   for (i=1; i<=nn; i++)
      {
      d1[2*i-1] = s1[i];	/* Real part */
      d1[2*i]   = 0.0;		/* Imaginary part */

      d2[2*i-1] = s2[i];	/* Real part */
      d2[2*i]   = 0.0;		/* Imaginary part */
      }

   /* Now carry out the FFT */
   four1 (d1, 1, nn, 1);
   four1 (d2, 1, nn, 1);

   /* Now, multiply the two things together (that is d1 * conj(d2)) */
   for (i=1; i<=2*nn; i+=2)
      {
      c[i]   = d1[i]*d2[i] + d1[i+1]*d2[i+1];	/* Real part */
      c[i+1] = d1[i+1]*d2[i] - d1[i]*d2[i+1];   /* Imaginary part */
      }

   /* Now do the inverse transform */
   four1 (c, 1, nn, -1);

   /* Now unpack the data */
   for (i=1; i<=nn; i++)
      corr[i] = c[2*i-1];

   /* Free the temporaries */
   FREE (d1, 1);
   FREE (d2, 1);
   FREE (c,  1);
   }


#undef SWAP

