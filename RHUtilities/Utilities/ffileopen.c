/* @(#)ffileopen.c	1.2 01 Oct 1995 */
/* Open file in which lines are truncated by # character */

/* SFILE_BEGIN */
#include <stdio.h>
/* SFILE_END */

#include <stdlib.h>
#include <Utilities/cvar.h>

#ifdef WIN32
#define popen(a,b) _popen((a),(b))
#endif

/*--------------------------------------------------------*/
/* Unix Version */

FUNCTION_DEF (FILE *m4_fopen, (
   const char *infilename
   ))
   {
   /* Open a pipe to sed */
   FILE *instream;

   /* Construct the command */
   char command[256];
   sprintf (command, "m4 %s", infilename);

   /* Now open the pipe */
   instream = popen (command, "r");

   /* Return the stream */
   return instream;
   }

FUNCTION_DEF (FILE *filter_fopen, (
   const char *infilename
   ))
   {
   /* Open a pipe to sed */
   FILE *instream;

   /* Construct the command */
   char command[256];
#ifdef WIN32
   sprintf (command, "copyinput.exe %s", infilename);
#else
   sprintf (command, "sed 's/#.*$//' %s", infilename);
#endif

   /* Now open the pipe */
   instream = popen (command, "r");

   /* Return the stream */
   return instream;
   }


