/* @(#)io.c	1.9 01 Oct 1995 */

/* Routines for doing io */

/* SFILE_BEGIN */
#include "Utilities/dll.h"
/* SFILE_END */

#include <Utilities/rh_util.h>
#include <Utilities/rhlist.h>
#include <Utilities/io_dcl.h>

/* The tokens in the line */
#define MAXTOKENS 512
#define LINELENGTH 8192

/* External declarations - SFILE_BEGIN */
/* For the meanings of these variables, see the file io.c */
EXTERN_C UTILITIES_DLLDATA char *_[];
EXTERN_C UTILITIES_DLLDATA int _NF; 
EXTERN_C UTILITIES_DLLDATA int _NR;
EXTERN_C UTILITIES_DLLDATA FILE *_infile;
EXTERN_C UTILITIES_DLLDATA char *_currentinput;	
/* SFILE_END */

/* Definitions of the externals */
char *_[MAXTOKENS];		/* The tokens */
int _NF = -1;			/* Number of tokens in the current line */
int _NR = 0;			/* Number of the current line */
char *_currentinput = NULL;	/* Name of the current input file */
FILE *_infile = NULL;		/* The current input file */

/* Local variables */
static char comment_char = '#';	/* The input line */
static char line[LINELENGTH];	/* The input line */
static LIST *filestack = NULL;	/* For keeping a stack of input files */

/* Forward declarations */
FUNCTION_DECL ( static void gettokens, ());
FUNCTION_DECL ( static int io_getline, (FILE *infile, char *line));

/* Datatypes for a stack of files */
typedef struct fdata
   {
   char *filename;
   int linenumber;
   FILE *infile;
   } fdata;

FUNCTION_DEF (void set_comment_char, (char new_comment_char))
   {
   /* Set the character that marks the begining of a comment
	to what ever is passed in.  The line starting with
	new_comment_char will be skipped now on */

   comment_char = new_comment_char;
   }

FUNCTION_DEF (char get_comment_char, ())
   {
   /* Returns the character that marks the begining of a comment. */
   return comment_char;
   }


FUNCTION_DEF ( int _io_init, (
        FILE *newinfile,
        char *fname))
   {
   /* Makes the file and its name known */
   _infile = newinfile;
   if (_currentinput) free (_currentinput);
   _currentinput = COPY(fname);
   if (filestack) lst_free (filestack);
   filestack = lst_init();
   _NR = 0;
   _NF = -1;
   return 1;
   }

FUNCTION_DEF ( void _io_cleanup, ())
   {
   /* Cleans up and terminates the io routines */
   _infile = NULL;
   free (_currentinput);
   _currentinput = NULL;
   if (filestack)
      lst_free (filestack);
   filestack = NULL;

    /* Free the tokens */
      {
      int i;
      for (i=0; i<=_NF; i++)
	 free (_[i]);
      _NF = -1;
      }

   _NR = 0;
   }

FUNCTION_DEF ( int _switchinput, (
        FILE *newfile,
        char *newfname))
   {
   fdata *newdata = NEW(fdata);
   newdata->linenumber = _NR;
   newdata->filename = _currentinput;
   newdata->infile = _infile;
   _infile = newfile;
   if (filestack == NULL) filestack = lst_init();
   lst_push (newdata, filestack);
   _NR = 0;
   _currentinput = COPY(newfname);
   return 1;
   }

FUNCTION_DEF ( rhBOOLEAN _popinput, ())
   {
   /* Get the old context off the stack */
   fdata *olddata = (fdata *) lst_pop(filestack);

   /* If stack is empty, then return FALSE */
   if (olddata == NULL)
      return (FALSE);

   /* Otherwise, close the file, restore old context */
   fclose (_infile);
   _NR = olddata->linenumber;
   free (_currentinput);
   _currentinput = olddata->filename;
   _infile = olddata->infile;
   free ((char *)olddata);

   /* Return success */
   return (TRUE);
   }
   
FUNCTION_DEF ( int _reporterror, ())
   {
   fprintf (stderr, "Bad line : File %s line %d\n", _currentinput, _NR);
   fprintf (stderr, "%s\n", line);
   if (filestack && ! lst_isempty (filestack))
      {
      fprintf (stderr, "Traceback :\n");
      lst_forall (fdata *, afile, filestack)
         {
         fprintf (stderr, "\tFile %s line %d\n", 
		   afile->filename, afile->linenumber);
         } lst_endall
      }
   return 1;
   }

FUNCTION_DEF ( int _nextline, (
        FILE *infile))
   {
   /* Gets the next line and splits it into tokens.
    * Returns EOF on end-of-file */
   if (io_getline(infile, line)== EOF)
      return (EOF);
   gettokens();
   return (1);
   }

FUNCTION_DEF (static int io_getline, (FILE *infile, char *line))
   {
   /* Get the next line skipping comment lines and blank lines */
   /* Returns EOF on end-of-file */
   char *p = line;
   int c;
   while ((c = getc(infile)) == ' ' || c == '\t' || c == '\n')
      if (c == '\n') _NR++;
   ungetc (c, infile);
   while ((c = getc(infile)) != '\n' && c != EOF) *(p++) = c;
   if (c == EOF) 
      return (EOF);
   *p = '\0';
   _NR ++;
   if (line[0] == comment_char) return (io_getline(infile, line));
   
   /* See if the line ends with a '\' */
      {
      char *q = p-1;

      /* Run backwards to find the last non-blank caracter */
      while (*q == ' ' || *q == '\t') q--;
	
      if (*q == '\\')
	 {
	 /* Continue the line */
	 *q = ' ';
	 return (io_getline (infile, q+1));
	 }
      
      else return (1);
      }
   }

FUNCTION_DEF (static void gettokens, ())
   {
   /* Divides the line into tokens */
   char *p = line;

   /* Free the previous tokens */
      {
      int i;
      for (i=0; i<=_NF; i++)
	 free (_[i]);
      }

   /* Reset _NF to zero */
   _NF = -1;

   /* Make _0 the full line */
   _[++_NF] = COPY(line);

   /* Take the other tokens */
   while (1)
      {
      char save;
      char *start = p;

      /* Skip to the next blank */
      while (*p != ' ' && *p != '\t' && *p != '\0') p++;
	
      /* Set this think to a null */
      save = *p;
      *p = '\0';

      /* Copy the word */
      _[++_NF] = COPY(start);
     *p = save;
      
      /* Skip to the next non-blank */
      while ((*p == ' ' || *p == '\t') && (*p != '\0')) p++;
      if (*p == '\0') break;
      }
   }

FUNCTION_DEF ( int _ECHO, ())
   {
   printf ("%s\n", _[0]);
   return 1;
   }

