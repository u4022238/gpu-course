
#include <Utilities/utilities_c.h>

FUNCTION_DEF (double ** rh_base1, (double **a, int n))
   {
   if (a == NULL) return NULL;
   else
      {
      double **a1 = VECTOR (1, n, double*);
      int i;
      for (i=1; i<=n; i++)
         a1[i] = a[i-1]-1;
      return a1;
      }
   }

FUNCTION_DEF (int gaussj2_check_0, (
   	double **a,
   	int n,
	double **b,
	int m
	))
   {
   /* Calls the base1 routine */
   double **a1 = rh_base1 (a, n);
   double **b1 = rh_base1 (b, n);
   int result = gaussj2_check_base1 (a1, n, b1, m);
   if (a1) free (a1+1);
   if (b1) free (b1+1);
   return result;
   }

FUNCTION_DEF (int gaussj_check_0, (
   	double **a,
   	int n,
	double *b
	))
   {
   /* Calls the base1 routine */
   double **a1 = rh_base1 (a, n);
   int result = gaussj_check_base1 (a1, n, b-1);
   if (a1) free (a1+1);
   return result;
   }
	
FUNCTION_DEF ( void QRdecomposition_3x3_0, (
	double **A, double **K, double **R
	))
   {
   double **A1 = rh_base1 (A, 3);
   double **K1 = rh_base1 (K, 3);
   double **R1 = rh_base1 (R, 3);
   QRdecomposition_3x3_base1 (A1, K1, R1);
   if (A1) free (A1+1);
   if (K1) free (K1+1);
   if (R1) free (R1+1);
   }

FUNCTION_DEF ( int choleski_RRT_0, ( double **A, int n, double **K ))
   {
   double **A1 = rh_base1 (A, n);
   double **K1 = rh_base1 (K, n);
   int result = choleski_RRT_base1 (A1, n, K1);
   if (A1) free (A1+1);
   if (K1) free (K1+1);
   return result;
   }

FUNCTION_DEF ( double determinant_0, (double **A, int n ))
   {
   double **A1 = rh_base1 (A, n);
   double result = determinant_base1 (A1, n);
   if (A1) free (A1+1);
   return result;
   }


FUNCTION_DEF ( void jacobi_0, (double **a, int n, double *d, double **v ))
   {
   double **a1 = rh_base1 (a, n);
   double **v1 = rh_base1 (v, n);
   jacobi_base1 (a1, n, d-1, v1);
   if (a1) free (a1+1);
   if (v1) free (v1+1);
   }

FUNCTION_DEF ( void min_solve_0, ( double **A, double *x, int m, int n ))
   {
   double **A1 = rh_base1 (A, m);
   min_solve_base1 (A1, x-1, m, n);
   if (A1) free (A1+1);
   }

FUNCTION_DEF ( int solve_pseudo_0, (
	double **a, 
	double *x,
	double *b,
	int nrow,
	int ncol
	))
   {
   double **a1 = rh_base1 (a, nrow);
   int result = solve_pseudo_base1 (a1, x-1, b-1, nrow, ncol);
   if (a1) free (a1+1);
   return result;
   }

FUNCTION_DEF ( int solve_simple_0, (
	double **a,
	double *x,
	double *b,
	int nrow,
	int ncol
	))
   {
   double **a1 = rh_base1 (a, nrow);
   int result = solve_simple_base1 (a1, x-1, b-1, nrow, ncol);
   if (a1) free (a1+1);
   return result;
   }

FUNCTION_DEF ( int sorted_svd_0, (
	double **A,
	int m, int n,
	double **U, double *D, double **V 
	))
   {
   double **A1 = rh_base1 (A, m);
   double **U1 = rh_base1 (U, m);
   double **V1 = rh_base1 (V, n);
   int result = sorted_svd_base1 (A1, m, n, U1, D-1, V1);
   if (A1) free (A1+1);
   if (U1) free (U1+1);
   if (V1) free (V1+1);
   return result;
   }

FUNCTION_DEF ( int svd3x3_0, (
 	double **A,
	double **U,
	double *D,
	double **V
	))
   {
   double **A1 = rh_base1 (A, 3);
   double **U1 = rh_base1 (U, 3);
   double **V1 = rh_base1 (V, 3);
   int result = svd3x3_base1 (A1, U1, D-1, V1);
   if (A1) free (A1+1);
   if (U1) free (U1+1);
   if (V1) free (V1+1);
   return result;
   }

FUNCTION_DEF ( int svdcmp_0, (
	double **a, 
	int m,
	int n,
	double *w,
	double **v,
	int accumulate_u
	))
   {
   double **a1 = rh_base1 (a, m);
   double **v1 = rh_base1 (v, n);
   int result = svdcmp_base1 (a1, m, n, w-1, v1, accumulate_u);
   if (a1) free (a1+1);
   if (v1) free (v1+1);
   return result;
   }

FUNCTION_DEF (void complex_roots_0, (
	fcomplex a[],
	int m,
	fcomplex roots[],
	int polish
	))
   {
   complex_roots_base1 (a, m, roots-1, polish);
   }

FUNCTION_DEF ( int choleski_3x3_0, (
   double **Ain, 	/* Input symmetric positive definite matrix */
   double **C		/* Output such that Ain = C*C^T */
   ))
   {
   double **A1 = rh_base1(Ain, 3);
   double **C1 = rh_base1(C, 3);
   int result = choleski_3x3_base1 (A1, C1);
   if (A1) free (A1+1);
   if (C1) free (C1+1);
   return result;
   }

FUNCTION_DEF ( void min_solve_direct_0, (
   double **X,			/* The system to be solved */
   double *x,			/* The solution */
   int n			/* The dimensions of the system */
   ))
   {
   double **X1 = rh_base1 (X, n);
   min_solve_direct_base1 (X1, x-1, n);
   if (X1) free (X1+1);
   }

FUNCTION_DEF ( void polydet_0, (
   double **Y1,		/* Matrix of constant terms */
   double **Yk,		/* Matrix of first degree terms */
   int n,		/* Size of the determinant */
   double *det)		/* The determinant as a polynomial */
   )
   {
   double **Y11 = rh_base1 (Y1, n);
   double **Yk1 = rh_base1 (Yk, n);
   polydet_base1 (Y11, Yk1, n, det-1);
   if (Y11) free (Y11+1);
   if (Yk1) free (Yk1+1);
   }

FUNCTION_DEF ( void complex_roots_real_coefficients_0, (
        double *a,
        int m,
        fcomplex *roots,
        int polish))
   {
   complex_roots_real_coefficients_base1 (a, m, roots-1, polish); 
   }
