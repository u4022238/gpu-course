/* @(#)bail_out.c	1.3 01 Oct 1995 */
/* Routine for bailing out of my libraries */
#include <stdlib.h>
#include <Utilities/cvar.h>
#include <assert.h>

FUNCTION_DEF ( int bail_out, (
        int n))
   {
   if (n > 1) assert (n == 0);
   exit (n);
   return (n);  /* This is never executed */
   }
