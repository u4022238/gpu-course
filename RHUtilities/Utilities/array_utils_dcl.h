#ifndef _array_utils_c_dcl_
#define _array_utils_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( char **alloca_matrix, (
        int nrl,
        int nrh,
        int ncl,
        int nch,
        int entry_size,
        char **m));

EXTERN_C_FUNCTION ( char **malloc_matrix, (
        int nrl,
        int nrh,
        int ncl,
        int nch,
        int entry_size));

EXTERN_C_FUNCTION ( void *malloc_array3D, (
        int ilow, int ihigh,
        int jlow, int jhigh,
        int klow, int khigh,
        int entry_size));

EXTERN_C_FUNCTION ( void fprint_intmatrix, (
        char *format,
        FILE *afile,
        int **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( void fprint_shortmatrix, (
        char *format,
        FILE *afile,
        short **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( void fprint_floatmatrix, (
        char *format,
        FILE *afile,
        float **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( void fprint_doublematrix, (
        char *format,
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( void fprint_matrix, (
        const char *format,
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( void print_matrix, (
        FILE *afile,
        double **a,
        int nrl,
        int nrh,
        int ncl,
        int nch));

EXTERN_C_FUNCTION ( int p_matrix_base1, ( double **a, int m, int n));

EXTERN_C_FUNCTION ( void fprint_vector, (
        const char *format,
        FILE *afile,
        double *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( void fprint_intvector, (
        char *format,
        FILE *afile,
        int *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( void fprint_floatvector, (
        char *format,
        FILE *afile,
        float *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( void print_vector, (
        FILE *afile,
        double *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( void print_floatvector, (
        FILE *afile,
        float *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( void print_intvector, (
        FILE *afile,
        int *v,
        int nrl,
        int nrh));

EXTERN_C_FUNCTION ( int p_vector_base1, ( double *a, int n));

EXTERN_C_FUNCTION ( void matrix_prod_base1, (
        double **A,
        double **B,
        int l,
        int m,
        int n,
        double **X));

EXTERN_C_FUNCTION ( void vector_matrix_prod_base1, (
        double *b,
        double **A,
        int l,
        int m,
        double *x));

EXTERN_C_FUNCTION ( void matrix_vector_prod_base1, (
        double **A,
        double *b,
        int l,
        int m,
        double *x));

EXTERN_C_FUNCTION ( double inner_product_base1, (
        double *A,
        double *B,
        int n));

EXTERN_C_FUNCTION ( double vector_length_base1, (
        double *A,
        int n));

EXTERN_C_FUNCTION ( void transpose, (
   double **A,		/* The matrix to be transposed */
   double **At,		/* Its transpose (returned) */
   int nrl, int nrh,	/* X dimension */
   int ncl, int nch	/* Y dimension */
   ));

EXTERN_C_FUNCTION ( int matrix_inverse_base1, (
   double **A,		/* The matrix to be inverted */
   double **Ainv,	/* The inverse computed */
   int n		/* Dimension of the matrix */
   ));

EXTERN_C_FUNCTION ( char **sub_matrix, (
   char **A,			/* Base array */
   int nrl, int ncl,		/* Origin of the new array */
   int rdim,			/* Number of rows of the sub-array */
   int orgi, int orgj,		/* Starting index for new array */
   int entry_size		/* Size of an entry */
   ));

EXTERN_C_FUNCTION ( double det3x3_base1, (
        double **A));

EXTERN_C_FUNCTION ( double det2x2_base1, (
        double **A));

EXTERN_C_FUNCTION ( void cofactor_matrix_3x3_base1, (
        double **A,
        double **Aadj));

EXTERN_C_FUNCTION ( void matrix_copy, (
   double **A, double **B,	/* The matrices to copy from and to */
   int i0, int i1,		/* Row bounds */
   int j0, int j1		/* Column bounds */
   ));

EXTERN_C_FUNCTION ( void cross_product_base1, (
   double *A, double *B,	/* Two vectors 1..3 */
   double *C			/* Their cross product */
   ));

EXTERN_C_FUNCTION ( int lin_solve_base1, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ));

EXTERN_C_FUNCTION ( int lin_solve_symmetric_base1, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ));

EXTERN_C_FUNCTION ( void identity_matrix_base1, (
        double **A,
        int dim));

EXTERN_C_FUNCTION ( void swap_columns, (
   double **M,		/* The matrix whose columns are to be swapped */
   int rowl, int rowh,	/* Bounds of row indices */
   int col1, int col2	/* Columns to be swapped */
   ));

#endif
