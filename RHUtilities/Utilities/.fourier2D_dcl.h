#ifndef _fourier2D_c_dcl_
#define _fourier2D_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( int is_2_power, (
        int n));

EXTERN_C_FUNCTION ( int fourier2D, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int istart, int jstart,	/* Starting indices for the block */
   int nrows, int ncols,	/* The dimensions of the image */
   int isign)			/* The direction of the transform */
   );

EXTERN_C_FUNCTION ( int block_fourier2D, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int istart, int jstart,	/* Starting index for the image */
   int nrows, int ncols,	/* The dimensions of the image */
   int blocksize,		/* The size of the blocks */
   int isign)			/* The direction of the transform */
   );

#endif
