#ifndef _fileopen_c_dcl_
#define _fileopen_c_dcl_
#include "Utilities/cvar.h"


/* */
typedef struct
   {
   char *fname;
   char *expanded_name;
   } fn_record;

/* */

EXTERN_C_FUNCTION ( char *findfile, (
   char *filename		/* The name of the file to find */
   ));

EXTERN_C_FUNCTION ( FILE *openinput, (
   char *newfilename,
   char **fullname    /* Returned full name of file with directory prefix */
   ));

EXTERN_C_FUNCTION ( int getdirlist, (
        int *argc,
        char *argv[]));

#endif
