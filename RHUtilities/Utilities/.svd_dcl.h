#ifndef _svd_c_dcl_
#define _svd_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void sort_svd_base1, (
   double **U, double *D, double **V,	/* Components of a SVD */
   int m, int n				/* Sizes of the U and V matrices */
   ));

EXTERN_C_FUNCTION ( int sorted_svd_base1, (
   double **A,				/* The matrix to be decomposed */
   int m, int n,			/* Size of the matrix */
   double **U, double *D, double **V	/* Components of the SVD */
   ));

#endif
