/* @(#)poly.c	1.4 20 Dec 1994 */
/* Routines handling polynomials */

#include <Utilities/rh_util.h>
#include <Utilities/alloc.h>
#include <Utilities/cvar.h>
#include <Utilities/array_utils_dcl.h>

FUNCTION_DEF ( double evaluate_poly, (
   double *p,		/* The polynomial */
   int n,		/* Its degree */
   double x)		/* The value to evaluate at */
   )
   {
   /* Evaluates a polynomial at a value */
   double val = 0.0;
   int i;

   /* Evaluate the polynomial using Horner's rule */
   for (i=n; i>=0; i--)
      val = x * val + p[i];

   return val;
   }

FUNCTION_DEF ( void poly_product, (
   double *p,		/* The first polynomial */
   int n,		/* Its degree */
   double *q,		/* The second polynomial */
   int m,		/* Its degree */
   double *pq)		/* The product */
   )
   {
   /* Takes the product of two polynomials */
   int i, j;
   int deg = m+n;      /* Degree of the product */

   /* First zero out the product */
   for (i=0; i<=deg; i++) pq[i] = 0.0;

   /* Now carry out the multiplication */
   for (i=0; i<=n; i++)
      for (j=0; j<=m; j++)
 	 pq[i+j] += p[i] * q[j];
   }

FUNCTION_DEF ( void poly_division, (
   double *a,		/* The dividend */
   int n,		/* Its degree */
   double *b,		/* The divisor */
   int m,		/* Its degree */
   double *q,		/* The quotient */
   double *r)		/* The remainder */
   )
   {
   /* Divides polynomial a by b giving a quotient q and remainder r.
    * It is supposed that all polynomials are already allocated, and that
    * q has the same number of elements at a,
    * r has the same number of elements as b.
    */

   int i, j;
   double *A = VECTOR (0, n, double);

   int db;  /* The real degree of the divisor */

   /* Take a copy of a */
   for (i=0; i<=n; i++) A[i] = a[i];

   /* Zero out the quotient */
   for (i=0; i<=n; i++) q[i] = 0.0;
   
   /* Determine the real degree of the divisor */
   db = m;
   while (b[db] == 0.0 && db > 0) db--;

   /* Now reduce it starting at the term of highest degree of a */
   for (i=n; i>=db; i--)
      {
      /* Determine the factor */
      int term_degree = i - db;		/* The degree of term being found */
      double qq = A[i] / b[db];

      /* Store this term in the quotient */
      q[term_degree] = qq;

      /* Now reduce B */
      A[i] = 0.0;
      for (j=0; j<db; j++)
	 A[j+term_degree] -= b[j] * qq;
      }

   /* What is left is the quotient */
   for (i=0; i<=m; i++)
      r[i] = A[i];

   /* Free the copy */
   FREE (A, 0);
   }

/* Routines for taking polynomial determinants */
FUNCTION_DEF ( void cofactor_base1, (
   double **M,			/* The matrix to take a cofactor of */
   int ci, int cj,		/* The row and column to remove */
   int n,			/* The dimension */
   double **cof)		/* The cofactor matrix */
   )
   {
   /* Returns the ci,cj-th cofactor matrix of M */
   int i, j;
   int row, column;

   row = 0;

   /* Run through the matrix transfering data, omitting the ci-th row and
    * cj-th column */
   for (i=1; i<=n; i++)
      {
      /* Omit the ci-th row */
      if (i == ci) continue;
      else row++;

      /* Start a count on the columns */
      column = 0;

      /* Run down the column */
      for (j=1; j<=n; j++)
	 {
	 /* Omit the j-th column */
	 if (j == cj) continue;
	 else column++;

	 /* Transfer the data */
	 cof[row][column] = M[i][j];
	 }
      }
   }
   
FUNCTION_DEF ( void polydet_base1, (
   double **Y1,		/* Matrix of constant terms */
   double **Yk,		/* Matrix of first degree terms */
   int n,		/* Size of the determinant */
   double *det)		/* The determinant as a polynomial */
   )
   {
   /* Take the determinant by cofactor expansion */
   int i;

   /* In the case where n = 1, we return the only entry of the matrix */
   if (n == 1) 
      {
      det[0] = Y1[1][1];
      det[1] = Yk[1][1];
      return;
      }

   else
      {
      /* Declare some cofactor matrices */
      double **cof1 = MATRIX (1, n-1, 1, n-1, double);
      double **cofk = MATRIX (1, n-1, 1, n-1, double);
      double *newdet = VECTOR (0, n-1, double);
      double sign = 1;

      /* Clear out the determinant */
      for (i=0; i<=n; i++) det[i] = 0.0;

      /* Now do the cofactor expansion along the first row */
      for (i=1,sign=1;  i<=n;  i++,sign=-sign)
         {
         /* Take the i-th cofactor along the 1st row */
	 int j;
         double c1 = sign * Y1[1][i];
         double ck = sign * Yk[1][i];

         /* Construct the cofactor matrices */
         cofactor_base1 (Y1, 1, i, n, cof1);
         cofactor_base1 (Yk, 1, i, n, cofk);
      
         /* Take the determinant */
         polydet_base1 (cof1, cofk, n-1, newdet);

         /* Now add the determinant to the accumulated determinant */
         for (j=0; j<=n-1; j++)
            {
	    det[j]   += c1 * newdet[j];
	    det[j+1] += ck * newdet[j];
	    }
         }

      /* Free temporaries */
      FREE (cof1, 1);
      FREE (cofk, 1);
      FREE (newdet, 0);
      }
   }

FUNCTION_DEF ( void characteristic_polynomial_base1, (
   double **A, 
   int n, 
   double *det))
   {
   /* Returns the characteristic polynomial of a matrix */
   /* Better use this on small matrices only */
   int i, j;

   /* Make a diagonal matrix */
   double **lam = MATRIX (1, n, 1, n, double);

   /* Make lam equal to minus the identity matrix */
   for (i=1; i<=n; i++)
      {
      for (j=1; j<=n; j++)
	 lam[i][j] = 0.0;
      lam[i][i] = -1.0;
      }

   /* Now compute the determinant */
   polydet_base1 (A, lam, n, det);

   /* Free the temporary */
   FREE (lam, 1);
   }

