#ifndef _svd3x3_c_dcl_
#define _svd3x3_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( int svd3x3_base1, (
   double **A,		/* The matrix input (3x3) */
   double **U,		/* The output matrix */
   double *D,		/* The diagonal */
   double **V		/* The other output matrix */
   ));

EXTERN_C_FUNCTION ( int svd2x2_base1, (
   double **A,		/* The matrix input (2x2) */
   double **U,		/* The output matrix */
   double *D,		/* The diagonal */
   double **V		/* The other output matrix */
   ));

#endif
