#ifndef _io_c_dcl_
#define _io_c_dcl_
#include "Utilities/cvar.h"


/* */
#include "Utilities/dll.h"
/* */

/* */
/* For the meanings of these variables, see the file io.c */
EXTERN_C UTILITIES_DLLDATA char *_[];
EXTERN_C UTILITIES_DLLDATA int _NF; 
EXTERN_C UTILITIES_DLLDATA int _NR;
EXTERN_C UTILITIES_DLLDATA FILE *_infile;
EXTERN_C UTILITIES_DLLDATA char *_currentinput;	
/* */

EXTERN_C_FUNCTION (void set_comment_char, (char new_comment_char));

EXTERN_C_FUNCTION (char get_comment_char, ());

EXTERN_C_FUNCTION ( int _io_init, (
        FILE *newinfile,
        char *fname));

EXTERN_C_FUNCTION ( void _io_cleanup, ());

EXTERN_C_FUNCTION ( int _switchinput, (
        FILE *newfile,
        char *newfname));

EXTERN_C_FUNCTION ( rhBOOLEAN _popinput, ());

EXTERN_C_FUNCTION ( int _reporterror, ());

EXTERN_C_FUNCTION ( int _nextline, (
        FILE *infile));

EXTERN_C_FUNCTION ( int _ECHO, ());

#endif
