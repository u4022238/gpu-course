#include "Utilities/cvar.h"

FUNCTION_DEF (int doublecmp_ascending , (
	const void *a, 
	const void *b))
   {
   // Compares double values
   double val1 = *((double *)a);
   double val2 = *((double *)b);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int doublecmp_descending , (
	const void *a, 
	const void *b))
   {
   // Compares double values
   double val1 = *((double *)a);
   double val2 = *((double *)b);
   if (val1 > val2) return -1;
   else if (val1 < val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int floatcmp_ascending , (
	const void *a, 
	const void *b))
   {
   // Compares float values
   float val1 = *((float *)a);
   float val2 = *((float *)b);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int floatcmp_descending , (
	const void *a, 
	const void *b))
   {
   // Compares float values
   float val1 = *((float *)a);
   float val2 = *((float *)b);
   if (val1 > val2) return -1;
   else if (val1 < val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int intcmp_ascending , (
	const void *a, 
	const void *b))
   {
   // Compares int values
   int val1 = *((int *)a);
   int val2 = *((int *)b);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int intcmp_descending , (
	const void *a, 
	const void *b))
   {
   // Compares int values
   int val1 = *((int *)a);
   int val2 = *((int *)b);
   if (val1 > val2) return -1;
   else if (val1 < val2) return 1;
   else return 0;
   }


FUNCTION_DEF (int shortcmp_ascending , (
	const void *a, 
	const void *b))
   {
   // Compares short values
   short val1 = *((short *)a);
   short val2 = *((short *)b);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int shortcmp_descending , (
	const void *a, 
	const void *b))
   {
   // Compares short values
   short val1 = *((short *)a);
   short val2 = *((short *)b);
   if (val1 > val2) return -1;
   else if (val1 < val2) return 1;
   else return 0;
   }

FUNCTION_DEF (int double_ptrcmp_ascending, ( 
         const void *ptr1, 
         const void *ptr2))
   {
   /* Compares two double pointers based on the value they point to */
   double val1 = **((double **)ptr1);
   double val2 = **((double **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int int_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   int val1 = **((int **)ptr1);
   int val2 = **((int **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int unsigned_int_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned int pointers based on the value they point to */
   unsigned int val1 = **((unsigned int **)ptr1);
   unsigned int val2 = **((unsigned int **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int float_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two float pointers based on the value they point to */
   float val1 = **((float **)ptr1);
   float val2 = **((float **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int short_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two short pointers based on the value they point to */
   short val1 = **((short **)ptr1);
   short val2 = **((short **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int char_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two char pointers based on the value they point to */
   char val1 = **((char **)ptr1);
   char val2 = **((char **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int unsigned_short_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned_short_ pointers based on the value they point to */
   unsigned short val1 = **((unsigned short **)ptr1);
   unsigned short val2 = **((unsigned short **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int unsigned_char_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned_char pointers based on the value they point to */
   unsigned char val1 = **((unsigned char **)ptr1);
   unsigned char val2 = **((unsigned char **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int long_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two long pointers based on the value they point to */
   long val1 = **((long **)ptr1);
   long val2 = **((long **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

FUNCTION_DEF (int unsigned_long_ptrcmp_ascending, ( const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned long pointers based on the value they point to */
   unsigned long val1 = **((unsigned long **)ptr1);
   unsigned long val2 = **((unsigned long **)ptr2);
   if (val1 < val2) return -1;
   else if (val1 > val2) return 1;
   else return (0);
   }

//-------------------------------------------
// Same but for descending order
//-------------------------------------------
FUNCTION_DEF (int double_ptrcmp_descending, ( 
         const void *ptr1, 
         const void *ptr2))
   {
   return double_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int int_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   return int_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int unsigned_int_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned int pointers based on the value they point to */
   return unsigned_int_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int float_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two float pointers based on the value they point to */
   return float_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int short_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two short pointers based on the value they point to */
   return short_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int char_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two char pointers based on the value they point to */
   return char_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int unsigned_short_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned_short_ pointers based on the value they point to */
   return unsigned_short_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int unsigned_char_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned_char pointers based on the value they point to */
   return unsigned_char_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int long_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two long pointers based on the value they point to */
   return long_ptrcmp_ascending (ptr2, ptr1);
   }

FUNCTION_DEF (int unsigned_long_ptrcmp_descending, ( 
         const void *ptr1, const void *ptr2))
   {
   /* Compares two unsigned long pointers based on the value they point to */
   return unsigned_long_ptrcmp_ascending (ptr2, ptr1);
   }












