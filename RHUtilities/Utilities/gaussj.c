/* @(#)gaussj.c	1.9 28 Nov 1995 */

/* Solves a set of linear equations by Gaussian elimination with
 * back substitution.
 */

/* Author : Richard Hartley */

#include <Utilities/cvar.h>
#include <math.h>
#include <Utilities/alloc.h>
#include <Utilities/error_message_dcl.h>
#include <Utilities/array_utils_dcl.h>

#define SWAP(a,b) {double temp = (a); (a)=(b); (b) = temp;}

FUNCTION_DEF ( int gaussj_check_base1, (
        double **a,
        int n,
        double *b))
   {
   /* Linear equation solution to  Gauss-Jordan elimination.
      a[1..n][1..n] is an input matrix of n by n elements.
      b[1..n] is an input vector of size n containing the
      right-hand side vector.
      On output, b is replaced by the corresponding solution vector.
    */
   int i, j, k;
   int returnval = 1;
   
   /* Make an array of column indices */
   int *colindex = VECTOR (1, n, int);

   /* Start reducing to upper triangular form */
   for (i=1; i<=n; i++)
      {
      /* Do the i-th iteration of pivoting */
      /*
       * We only deal with the part of the matrix from the 
       * i-th row and column onwards
       */
      
      /* Search for the largest pivot */
      double big = 0.0;
      int row = 0, col = 0;;
      for (j=i; j<=n; j++) for (k=i; k<=n; k++)
	 {
	 if (fabs(a[j][k]) > big)
	    {	
	    big = fabs(a[j][k]);
	    row = j;
	    col = k;
	    }
	 }

      /* If there are no non-zero elements left, then the matrix is singular */
      if (row == 0)
	 {
	 warning_message("GAUSSJ : Singular Matrix");
	 returnval = 0;
         goto cleanup;
	 }

      /* Record the column index */
      colindex[i] = col;

      /* Swap the row up into the i-th position */
      if (row != i)
	 {
	 /* Swap the rows of the matrix */
	 for (j=i; j<=n; j++)
	    SWAP (a[row][j], a[i][j]);

	 /* Swap the element of the goal vector */
	 SWAP (b[row], b[i]);
	 }

      /* Swap the column into the i-th position */
      if (col != i)
	 {
	 /* Swap the columns of the matrix -- must be the complete column */
	 for (j=1; j<=n; j++)
	    SWAP (a[j][col], a[j][i]);
	 }

      /* Normalize the i-th row so that a[i][i] = 1 */
         {
	 /* Compute the factor */
	 double pinv = 1.0 / a[i][i];

	 /* Now reset the matrix values */
         /* We leave the diagonal entry unchanged - really should be set to 1 */
	 for (j=i+1; j<=n; j++)
	    a[i][j] *= pinv;

	 /* The goal vector value */
	 b[i] *= pinv;
	 }

      /* Now use this to clean out the other rows */
      for (j=i+1; j<=n; j++)
	 {
	 /* Clean out row j */

	 /* Compute the multiple of the i-th row to subtract from it */
	 double mul = a[j][i];

	 /* Clean out the row -- don't bother to set a[j][i] = 0 */
	 for (k=i+1; k<=n; k++)
	    a[j][k] -= mul * a[i][k];

	 /* Also the goal value */
	 b[j] -= mul * b[i];
	 }
      }

   /*
    * The matrix is now in upper triangular form with the diagonals 
    * nominally equal to 1.0.
    * Now we do back-substitution.
    */

   for (i=n-1; i>=1; i--)
      {
      /* Back substitute to find b[i] */
      for (j=i+1; j<=n; j++)
	 b[i] -= a[i][j]*b[j];
      }

   /*
    * Finally, we need to permute the elements of b to account for
    * column permutations.
    */
   for (j=n; j>=1; j--)
      if (j != colindex[j])
	 SWAP (b[j], b[colindex[j]]);

   /* Now free the temporary */
cleanup :
   FREE (colindex, 1);

   /* Return the value */
   return returnval;
   }


#if 0

/* This is much the same as the other gaussj_check, except that
 * we do partial pivoting instead of full pivoting.
 */

FUNCTION_DEF ( int gaussj_check_base1, (
        double **a,
        int n,
        double *b))
   {
   /* Linear equation solution to  Gauss-Jordan elimination.
      a[1..n][1..n] is an input matrix of n by n elements.
      b[1..n] is an input matrix of size n containing the
      right-hand side vector.
      On output, b is replaced by the corresponding solution vector.
      This is the partial pivoting version.
    */
   int i, j, k;
   
   /* Start reducing to upper triangular form */
   for (i=1; i<=n; i++)
      {
      /* Do the i-th iteration of pivoting */
      /*
       * We only deal with the part of the matrix from the 
       * i-th row and column onwards
       */
      
      /* Search for the largest pivot in the i-th column */
      double big = 0.0;
      int row = 0;
      for (j=i; j<=n; j++)
	 {
	 if (fabs(a[j][i]) > big)
	    {	
	    big = fabs(a[j][i]);
	    row = j;
	    }
	 }

      /* If there are no non-zero elements left, then the matrix is singular */
      if (row == 0)
	 {
	 warning_message("GAUSSJ : Singular Matrix");
	 return 0;
	 }

      /* Swap the row up into the i-th position */
      if (row != i)
	 {
	 /* Swap the rows of the matrix */
	 for (j=i; j<=n; j++)
	    SWAP (a[row][j], a[i][j]);

	 /* Swap the element of the goal vector */
	 SWAP (b[row], b[i]);
	 }

      /* Normalize the i-th row so that a[i][i] = 1 */
         {
	 /* Compute the factor */
	 double pinv = 1.0 / a[i][i];

	 /* Now reset the matrix values */
         /* We leave the diagonal entry unchanged - really should be set to 1 */
	 for (j=i+1; j<=n; j++)
	    a[i][j] *= pinv;

	 /* The goal vector value */
	 b[i] *= pinv;
	 }

      /* Now use this to clean out the other rows */
      for (j=i+1; j<=n; j++)
	 {
	 /* Clean out row j */

	 /* Compute the multiple of the i-th row to subtract from it */
	 double mul = a[j][i];

	 /* Clean out the row -- don't bother to set a[j][i] = 0 */
	 for (k=i+1; k<=n; k++)
	    a[j][k] -= mul * a[i][k];

	 /* Also the goal value */
	 b[j] -= mul * b[i];
	 }
      }

   /*
    * The matrix is now in upper triangular form with the diagonals 
    * nominally equal to 1.0.
    * Now we do back-substitution.
    */

   for (i=n-1; i>=1; i--)
      {
      /* Back substitute to find b[i] */
      for (j=i+1; j<=n; j++)
	 b[i] -= a[i][j]*b[j];
      }

   /* Return the value */
   return 1;
   }

#endif

FUNCTION_DEF ( int gaussj2_check_base1, (
        double **a,
        int n,
  	double **b,
	int m
	))
   {
   /* Find the inverse of a matrix.
      a[1..n][1..n] is an input matrix of n by n elements.
      This is the partial pivoting version.
      After this, for compatibility, it multiplies this inverse
      by b so as to solve the equations a*x = b
    */

   int i, j, k;
   int returnval = 1;

   /* Take indices of the rows */
   int *index = VECTOR(1, n, int);
   
   /* Start reducing to upper triangular form */
   for (i=1; i<=n; i++)
      {
      /* Do the i-th iteration of pivoting */
      /*
       * We only deal with the part of the matrix from the 
       * i-th row and column onwards
       */
      
      /* Search for the largest pivot in the i-th column */
      double big = 0.0;
      int row = 0;
      for (j=i; j<=n; j++)
	 {
	 if (fabs(a[j][i]) > big)
	    {	
	    big = fabs(a[j][i]);
	    row = j;
	    }
	 }

      /* Store the index */
      index[i] = row;

      /* If there are no non-zero elements left, then the matrix is singular */
      if (row == 0)
	 {
	 warning_message("GAUSSJ : Singular Matrix");
  	 returnval = 0;
	 goto cleanup;
	 }

      /* Swap the row up into the i-th position */
      if (row != i)
	 {
	 /* Swap the rows of the matrix */
	 for (j=1; j<=n; j++)
	    SWAP (a[row][j], a[i][j]);
	 }

      /* Normalize the i-th row so that a[i][i] = 1 */
         {
	 /* Compute the factor */
	 double pinv = 1.0 / a[i][i];

	 /* Now reset the matrix values */
         /* We set the diagonal element to pinv */
         a[i][i] = 1.0;
	 for (j=1; j<=n; j++)
	    a[i][j] *= pinv;
	 }

      /* Now use this to clean out the other rows */
      for (j=1; j<=n; j++)
         if (j != i)
	    {
	    /* Clean out row j */

	    /* Compute the multiple of the i-th row to subtract from it */
	    double mul = a[j][i];

	    /* Clean out the row  */
	    a[j][i] = 0.0;
	    for (k=1; k<=n; k++)
	       a[j][k] -= mul * a[i][k];
	    }
      }

   /*
    * At this point we have taken the inverse of a matrix with permuted rows
    * We must undo the permutation.  This means by permuting the columns
    * of the inverse.
    */
   for (j=n; j>=1; j--)
      if (j != index[j])
	 {
	 for (k=1; k<=n; k++)
	    SWAP (a[k][j], a[k][index[j]]);
	 }

   /* Now, multiply by b to solve linear equations */
   if (m > 0) matrix_prod_base1 (a, b, n, n, m, b);

   /* Clean up */
cleanup:
   FREE (index, 1);

   /* Return the value */
   return returnval;
   }

FUNCTION_DEF ( void gaussj_base1, (
        double **a,
        int n,
        double *b))
   {
   /* Check for solution without returning a value */
   if ( ! gaussj_check_base1(a, n, b))
      bail_out(2);
   }

FUNCTION_DEF ( void gaussj2_base1, (
        double **a,
        int n,
  	double **b,
	int m ))
   {
   /* Check for solution without returning a value */
   if ( ! gaussj2_check_base1(a, n, b, m))
      bail_out(2);
   }

