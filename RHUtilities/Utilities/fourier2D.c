/* @(#)fourier2D.c	1.3 07 Nov 1994 */

/* Two dimensional fourier transform routines */

#include <Utilities/utilities_c.h>
#include <Utilities/cvar.h>
#include <Utilities/fourier_dcl.h>

FUNCTION_DEF ( int is_2_power, (
        int n))
   {
   /* Determines whether the number n is a power of 2 */
   while (n > 1 && n % 2 == 0) n /= 2;

   /* If we reach here, then it is a power of 2 */
   if (n == 1) return 1;
   else return 0;
   }

FUNCTION_DEF ( int fourier2D, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int istart, int jstart,	/* Starting indices for the block */
   int nrows, int ncols,	/* The dimensions of the image */
   int isign)			/* The direction of the transform */
   )
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (0, nrows-1) x (0, ncols-1) */
   int i, j, k;

   /* Starting point for the arrays */
   static const int fstart = 1;		/* Expected starting index for four1 */

   /* Ending indices */
   int iend = istart + nrows - 1;
   int jend = jstart + ncols - 1;
  
   /* Declare an array to hold the Fourier data */
   double *arow;
   double *acol;

   /* Check that the dimensions of the image are powers of 2 */
   if (! is_2_power (nrows) || ! is_2_power (ncols))
      {
      error_message ("fourier2D: Image size not power of 2");
      return 0;
      }

   /* Allocate temporaries */
   arow = VECTOR (fstart, 2*ncols + fstart - 1, double);
   acol = VECTOR (fstart, 2*nrows + fstart - 1, double);

   /* Now, take the transform one row at a time */
   for (i=istart; i<=iend; i++)
      {
      /* Take the transform of the i-th row */
      
      /* First, repack the data */
      for (j=jstart, k=fstart; j<=jend; j++)
         {
         arow[k++] = re[i][j];
         arow[k++] = im[i][j];
         }

      /* Take the transform */
      four1 (arow, fstart, ncols, isign);

      /* Put it in the array */
      for (j=jstart, k=fstart; j<=jend; j++)
	 {
	 re[i][j] = arow[k++];
	 im[i][j] = arow[k++];
	 }
      }

   /* Now take the transforms in the opposite direction */
   for (j=jstart; j<=jend; j++)
      {
      /* Take the transform of the columns */

      /* First, build a column */
      for (i=istart, k=fstart; i<=iend; i++)
	 {
	 acol[k++] = re[i][j];
	 acol[k++] = im[i][j];
	 }

      /* Take the transform */
      four1 (acol, fstart, nrows, isign);
	
      /* Put it back in the arrays */
      for (i=istart, k=fstart; i<=iend; i++)
	 {
	 re[i][j] = acol[k++];
	 im[i][j] = acol[k++];
	 }
      }

   /* Now, free the data */
   FREE (arow, fstart);
   FREE (acol, fstart);

   /* Return success */
   return 1;
   }

FUNCTION_DEF ( int block_fourier2D, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int istart, int jstart,	/* Starting index for the image */
   int nrows, int ncols,	/* The dimensions of the image */
   int blocksize,		/* The size of the blocks */
   int isign)			/* The direction of the transform */
   )
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions 
    * (istart, istart+nrows-1) x (jstart, jstart+ncols-1) */
   int i, j;	/* Index within each block */
   int k, l;	/* Block counters */
   int m;	/* Index into arow or acol */

   /* Starting indices */
   static const int fstart = 1;		/* Required starting index for four1 */

   /* Declare an array to hold the Fourier data */
   double *arow;
   double *acol;

   /* Check that the size of the blocks is good */
   if (! is_2_power (blocksize))
      {
      error_message ("fourier2D: Block size (%d) not power of 2", blocksize);
      return 0;
      }

   /* Check that there are an exact number of blocks */
   if (nrows % blocksize != 0)
      {
      error_message ("fourier2D: Block size (%d) does not divide # rows (%d)",
	 blocksize, nrows);
      return 0;
      }
   if (ncols % blocksize != 0)
      {
      error_message (
	"fourier2D: Block size (%d) does not divide # columns (%d)",
	 blocksize, ncols);
      return 0;
      }

   /* Allocate temporary vectors */
   arow = VECTOR (fstart, 2*ncols + fstart - 1, double);
   acol = VECTOR (fstart, 2*nrows + fstart - 1, double);

   /* Now, take the transform one block at a time */
   for (k=0; k<nrows/blocksize; k++)
      for (l=0; l<ncols/blocksize; l++)
	 {
	 /* Get offset of the block that we are doing */
	 int rowoffset = k*blocksize + istart;
	 int coloffset = l*blocksize + jstart;
	 int iend = rowoffset + blocksize - 1;
	 int jend = coloffset + blocksize - 1;

         /* Now, carry out the transform one row at a time */
         for (i=rowoffset, m=fstart; i<=iend; i++)
            {
            /* Take the transform of the i-th row */
      
            /* First, repack the data */
            for (j=coloffset, m=fstart; j<=jend; j++)
               {
               arow[m++] = re[i][j];
               arow[m++] = im[i][j];
               }

            /* Take the transform */
            four1 (arow, fstart, blocksize, isign);

            /* Put it in the array */
            for (j=coloffset, m=fstart; j<=jend; j++)
	       {
	       re[i][j] = arow[m++];
	       im[i][j] = arow[m++];
	       }
            }

         /* Now take the transforms in the opposite direction */
         for (j=coloffset; j<=jend; j++)
            {
            /* Take the transform of the columns */

            /* First, build a column */
            for (i=coloffset, m=fstart; i<=iend; i++)
	       {
	       acol[m++] = re[i][j];
	       acol[m++] = im[i][j];
	       }

            /* Take the transform */
            four1 (acol, fstart, blocksize, isign);
	
            /* Put it back in the arrays */
            for (i=coloffset, m=fstart; i<=iend; i++)
	       {
	       re[i][j] = acol[m++];
	       im[i][j] = acol[m++];
	       }
            }
	 }

   /* Now, free the data */
   FREE (arow, fstart);
   FREE (acol, fstart);

   /* Return success */
   return 1;
   }
