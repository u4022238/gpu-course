#include <math.h>
#include <Utilities/utilities_c.h>
#include <Utilities/fourier2D_dcl.h>

#define SWAP(a,b) {double tempr=(a);(a)=(b);(b)=tempr;}

#ifdef rhDEBUG
static void print_data_array ( double *data, int n)
   {
   /* Prints the data */
   int i;
   for (i=1; i<=n; i++)
      printf ("\t%3d : %12.4f\n", i, data[i]);
   }
#endif

FUNCTION_DEF ( void fft_unpack, (
	double *data, 		/* Packed real fft */
        int i0,			/* Start index for data array */
	int n,			/* Dimension of data array */
	double *real,
	double *imag		/* Real and imaginary parts of unpacked */
	))
   {
   /*
    * Unpacks the output of the real FFT into real and imaginary parts.
    */

   int i1, i2;

   /* Define a quarter block */
   int imid = i0 + n/2;

   /* The real term first */
   real[i0] = data[i0];
   imag[i0] = 0.0;

   for (i1=i0+1, i2=n+i0-1; i1<=imid; i1++, i2--)
      {
      /* Get the pixels */
      double a = data[i1];
      double b = data[i2];

      /* Write out the real and imaginary parts */
      real[i1] = real[i2] = (a+b)/2;
      imag[i1] = (b-a)/2;
      imag[i2] = (a-b)/2;
      }
   }


FUNCTION_DEF ( void fft_2D_unpack, (
	double **data, 		/* Packed real fft */
	double i0, int j0,	/* Starting indices for the array */
	int idim, int jdim, 
	double **real,
	double **imag		/* Real and imaginary parts of unpacked */
	))
   {
   /*
    * Unpacks the output of the real FFT into real and imaginary parts.
    *
    * Either of real or imag may be the same as the input array, data.
    */

   int i1, j1;

   /* Define a quarter block */
   int imid = i0 + idim/2;
   int jmid = j0 + jdim/2;

   for (i1=i0; i1<=imid; i1++)
      {
      /* Get the two coordinate positions */
      int i2 = (i1==i0) ? i0 : idim - i1 + i0;

      for (j1=j0; j1<=jmid; j1++)
         {
         /* Get the two coordinates positions */
         int j2 = (j1==j0) ? j0 : jdim - j1 + j0;

         /* Get the pixels */
         double a = data[i1][j1];
         double b = data[i2][j1];
         double c = data[i1][j2];
         double d = data[i2][j2];

         /* Write out the real and imaginary parts */
         real[i1][j1] = real[i2][j2] = (b+c)/2;
  	 real[i1][j2] = real[i2][j1] = (a+d)/2;
         imag[i1][j1] = (d-a)/2;
  	 imag[i2][j2] = (a-d)/2;
         imag[i2][j1] = (c-b)/2;
  	 imag[i1][j2] = (b-c)/2;
         }
      }
   }

FUNCTION_DEF ( void fft_real, ( double *data, int istart, int n))
   {
   /* Does a Fourier transform of a real array of size n.
    *
    * The output is :
    *    A[0], A[1].real - A[1].imag, A[1].real + A[1].imag,  ... , A[n-1]
    * where A is the Fourier transformed function.
    *
    * Thus both input and output are arrays of length n.

    * !! NOTE : Actually, the arrays start at istart, not 0.
    *
    * Amazingly enough, the forward and backward transforms are identical,
    * except for division at the end.
    */

   int blocksize = 1;
   double M_2PI = 2.0 * M_PI;

   /* End array index */
   int iend = istart + n - 1;

   /* Bit reversal */
      {
      int i, j;

      for (i=istart, j=istart; i<iend; i++) 
         {
	 int m = n/2;
         if (j > i) 
            SWAP(data[j],data[i]);

         while (m >= 2 && j+1-istart > m) 
	    {
            j -= m;
            m >>= 1;
            }
         j += m;
         }
      }

   /* Butterflies */

   /* Special cases of blocksize 2 */
   if (n >= 2)
      {
      int i;

      for (i=istart; i<=iend; i+=2)
         {
         /* Butterfly between nodes i and i+1 */

         /* Temporary for data */
	    {
            double temp  = data[i] + data[i+1];
            data[i+1]    = data[i] - data[i+1];
            data[i]      = temp;
	    }
         }
      }

   /* Remaining block sizes */
   for (blocksize = 4; blocksize <= n; blocksize *= 2)
      {
      int blockoffset;
      double theta = M_2PI/blocksize;
      double wtemp = sin(0.5*theta);
      double wpr   = -2.0*wtemp*wtemp;	/* cos(theta) - 1 */
      double wpi   = sin(theta);
      double wr    = wpr + 1.0;
      double wi    = wpi;

      /* Do each block */
      /* First the real entries */
      int i;
      for (i=istart; i<iend; i+=blocksize)
    	 {
	 /* Get the last entry in the block */
	 int j = i+blocksize/2;

	 /* Compute the transformed entries */
         double temp  = data[i] + data[j];
         data[j]      = data[i] - data[j];
         data[i]      = temp;
	 }

      /* The middle entry */
      for (i=istart+blocksize/4; i<iend; i+=blocksize)
    	 {
	 /* Get the last entry in the block */
	 int j = i+blocksize/2;

	 /* Fix this up */
         double temp  = data[i] - data[j];
         data[j]      = data[i] + data[j];
         data[i]      = temp;
	 }

      /* 
       * blockoffset is the offset of the pair within the block.
       * We treat each offset running over all blocks, since they
       * share the same values of w
       */
      for (blockoffset=2; blockoffset<=blocksize/4; blockoffset++) 
	 {
	 int i, j, k, l;
	 
	 /* This loop runs over the blocks */
	 /* Do the same butterfly in each block */
         for ( 
		/* Starting conditions for loop */
		i   = blockoffset + istart - 1, 
		j   = blocksize/2 - blockoffset + istart + 1,
		k   = blockoffset + blocksize/2 + istart - 1,
		l   = blocksize   - blockoffset + istart + 1;
		
		/* Stopping condition */
		i <= iend; 

	 	/* Update at end of loop */
	       	i  += blocksize, 
		j  += blocksize,
		k  += blocksize,
		l  += blocksize
	     ) 
	    {
	    /* Butterfly between nodes i and j */

	    /* Multiply data[j] by w^k; k = m-i */
            double tempr = wr*data[k] - wi*data[l];
            double tempi = wr*data[l] + wi*data[k];

	    /* Put new values */
            data[k] = data[i] - tempr;
            data[l] = data[j] + tempi;
	    data[i] = data[i] + tempr;
	    data[j] = data[j] - tempi;
            }

	 /* Compute the next power of w^k */
	    {
	    double newwr = wr*wpr - wi*wpi + wr;
            wi = wi*wpr + wr*wpi + wi;
	    wr = newwr;
	    }
         }
      }
   }

FUNCTION_DEF ( int fourier2D_real, (
   double **re,			/* The image */
   int istart, int jstart,	/* Starting indices */
   int nrows, int ncols		/* The dimensions of the image */
   ))
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (1, nrows) x (1, ncols) */
   int i, j;

   /* Ending indices */
   int iend = istart + nrows - 1;
   int jend = jstart + ncols - 1;
  
   /* Declare an array to hold the columns */
   double *acol;

   /* Check that the dimensions of the image are powers of 2 */
   if (! is_2_power (nrows) || ! is_2_power (ncols))
      {
      error_message ("fourier2D: Image size not power of 2");
      return 0;
      }

   /* Now, take the transform one row at a time */
   for (i=istart; i<=iend; i++)
      {
      /* The row is the i-th row of the array.  Start indexing at 1 */
      double *arow = re[i];

      /* Take the transform */
      fft_real (arow, jstart, ncols);
      }

   /* Declare an array to hold columns */
   acol = VECTOR (istart, iend, double);

   /* Now take the transforms in the opposite direction */
   for (j=jstart; j<=jend; j++)
      {
      /* Take the transform of the columns */

      /* First, build a column */
      for (i=istart; i<=iend; i++)
	 acol[i] = re[i][j];

      /* Take the transform */
      fft_real (acol, istart, nrows);
	
      /* Put it back in the arrays */
      for (i=istart; i<=iend; i++)
	 re[i][j] = acol[i];
      }

   /* Now, free the data */
   FREE (acol, istart);

   /* Return success */
   return 1;
   }

FUNCTION_DEF ( int block_fourier2D_real, (
   double **re,			/* The image */
   int istart, int jstart,	/* Starting indices for the array */
   int nrows, int ncols,	/* The dimensions of the image */
   int blocksize		/* The size of the blocks */
   ))
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (1, nrows) x (1, ncols) */
   int i, j;	/* Index within each block */
   int k, l;	/* Block counters */
  
   /* Declare an array to hold the Fourier data */
   double *acol;

   /* Check that the size of the blocks is good */
   if (! is_2_power (blocksize))
      {
      error_message ("fourier2D: Block size (%d) not power of 2", blocksize);
      return 0;
      }

   /* Check that there are an exact number of blocks */
   if (nrows % blocksize != 0)
      {
      error_message ("fourier2D: Block size (%d) does not divide # rows (%d)",
	 blocksize, nrows);
      return 0;
      }
   if (ncols % blocksize != 0)
      {
      error_message (
	"fourier2D: Block size (%d) does not divide # columns (%d)",
	 blocksize, ncols);
      return 0;
      }

   /* Allocate temporary vectors */
   acol = VECTOR (0, blocksize-1, double);

   /* Now, take the transform one block at a time */
   for (k=0; k<nrows/blocksize; k++)
      for (l=0; l<ncols/blocksize; l++)
	 {
	 /* Get offset of the block that we are doing */
	 int rowoffset = k*blocksize+istart;
	 int coloffset = l*blocksize+jstart;

         /* Now, carry out the transform one row at a time */
         for (i=rowoffset; i<rowoffset+blocksize; i++)
            {
            /* Take the transform of the i-th row */
            double *arow = re[i];

            /* Take the transform */
            fft_real (arow, coloffset, blocksize);
            }

         /* Now take the transforms in the opposite direction */
         for (j=coloffset; j<coloffset+blocksize; j++)
            {
            /* Take the transform of the columns */

            /* First, build a column */
            for (i=0; i<blocksize; i++)
	       acol[i] = re[i+rowoffset][j];

            /* Take the transform */
            fft_real (acol, 0, blocksize);
	
            /* Put it back in the arrays */
            for (i=0; i<blocksize; i++)
	       re[i+rowoffset][j] = acol[i];
            }
	 }

   /* Now, free the data */
   FREE (acol, 0);

   /* Return success */
   return 1;
   }

#undef SWAP

