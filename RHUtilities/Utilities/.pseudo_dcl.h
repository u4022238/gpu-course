#ifndef _pseudo_c_dcl_
#define _pseudo_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( int solve_pseudo_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol));

#endif
