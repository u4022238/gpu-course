/*
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
*/

/* @(#)marquardt.h	1.2 6/2/93 */
/* Structures for controlling the Marquardt algorithm */

#ifndef marquardt_h
#define marquardt_h

#include <Utilities/cvar.h>

typedef struct
   {
   /* Parameters controlling the flow of the problem */
   int 	Num_Loops;	/* Maximum number of iterations */
   double Min_Incr;  	/* Square of smallest significant parameter change */
   int diag;		/* 1 means weight matrix is diagonal, 0 = square */

   /* The size of the problem */
   int nrows, ncols;	/* Size of the problem (Jacobian dimensions) */

   /* Input and output */
   double **weight;	/* Input covariance weight matrix */
   double **covar;	/* Output covariance matrix */
   double *param;	/* Array of pointers to parameter values */
   double *goal;		/* Goal vector */

   /* Functions */
   FUNCTION_DECL (void (* gen_jacobian), (
      double *mparam,
      double **dyda,
      int nrows,
      int ncols
      ));   		/* Routine to compute Jacobian and goal vector */

   FUNCTION_DECL (void (* function), (
      double *mparams,
      double *newvalue,
      int nrows,
      int ncols
      )); 		/* Routine for computing value of the function */

   FUNCTION_DECL (void (* new_position_action), (
      double *mparam,
      int ncols));  	/* Called when parameter values change */

   } Marquardt_info;

/*>> Explanation :

   The foregoing structure controls the marquardt algorithm.  The meaning
of each parameter is as follows.

Num_Loops	Controls the number of times the program will go
around the loop of iterative improvement.  The algorithm will
terminate when this number of iterations have been done or it is
determined that futher iterations will not lead to improvement. A
value of Num_Loops = 100 - 500 will be plenty, probably excessive.
Note that the algorithm normally does not go this many iterations,
because it converges first.

Min_Incr	The minimum square sum of changes to the parameters
that is considered significant.  If an iteration causes an increment
less than this value, then the algorithm will be deemed to have
converged and will terminate.

diag		The algorithm will handle either diagonal or full
symmetric covariance matrices.  A value of diag = 1 means that the
covariance (weight) matrix contains just diagonal entries.  In this
case, the matrix "weight" must be represented as a matrix of dimensions 
1 x nrows (not as might be thought a nrows x nrows matrix with just
diagonal entries).  The first row represents the diagonal entries of
the covariance matrix.  

	In the case where diag = 0, the algorithm expects a full nrows
x nrows matrix of covariance values between the measured values being
approximated.

weight		Weight is the inverse covariance matrix of the covariances of
the measured values being approximated.  In case weight is a diagonal
matrix (diag = 1) the diagonal entries represent the reciprocals of
the variances of the measured values, each measurement assumed to be
independent.  In the case weight is a non-diagonal matrix it must be
symmetric and then represents the inverse of the full covariance
matrix of the measurements.

	As explained above, if weight is a diagonal matrix, then it
must be stored in one row, the single row representing the actual
diagonal entries.

	If weight is a null pointer, then a diagonal identity matrix
is assumed.

covar		The output covariance matrix.  This gives the
covariance matrix for the outputs at the computed optimum parameter
setting.  Covar is a matrix of dimension ncols x ncols and must be
allocated previous to calling marquardt.  If it has the value NULL,
then the covariance matrix will not be computed.

params		This is an array of length ncols containing 
the parameters to be varied.  This vector is the means by which the
parameter values are updated.  All parameters must be double.
On input, the vector contains the initial values of the parameters and
on output the final values.

goal	This is an array of length nrows giving the goal values of the
function.

get_jacobian	This is the address of subroutine that must be
supplied for computing the Jacobian matrix
and the goal vector at the present parameter setting.  
It is called with the sequence 

(void) (* get_jacobian) (params, dyda, dy, nrows, ncols);
   double *params;
   double **dyda;
   double *dy;
   int nrows, ncols;

the matrix dyda is a matrix of size nfows x ncols and dy is a vector
of length nrows.  Both are already allocated inside the marquardt
routine.  If the get_jacobian field contains a NULL function pointer,
then the marquardt routine will find its own way to compute the
jacobian.

function	This is a function supplied to the routine.  The
routine seeks to minimize the difference between the value of this
function and a given goal vector.  The function is called with the
sequence

(void ) (* function ) (inputvalues, outputvalues, nrows, ncols)
   double *inputvalues;
   double *outputvalues;
   int nrows, ncols;

The vector inputvalues is an array of length ncols and outputvalues is
an array of length nrows.  The function must return the output
values.

new_position_action  is a routine called whenever the values of the
parameters are updated.  This routine may do various things, for
instance change the values of the parameters to yet a different value
if the value determined by the marquardt iteration is not acceptable.
The routine is called as follows

new_position_action (params, ncols)
  double *params;
  int ncols;

<<*/

#include <Utilities/marqu_dcl.h>
#endif
