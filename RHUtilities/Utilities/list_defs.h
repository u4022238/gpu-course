/*
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
*/

/* @(#)list_defs.h	3.9  17 Jun 1994 */
/* Include file for use with lists */
#ifndef list_defs_h
#define list_defs_h
#ifndef DATADEF
#define DATADEF
typedef void *DATA;
#endif

typedef struct rhlist
 	{
	DATA data;	/* A pointer to the actual data */
	struct rhlist *lst_next, *lst_prev;
	} LIST, LISTENTRY;

/* Macros for moving down lists */
#define lst_nextentry(x) ((x)->lst_next)
#define lst_preventry(x) ((x)->lst_prev)
#define lst_firstentry(lst) ((lst)->lst_next)
#define lst_lastentry(lst) ((lst)->lst_prev)
#define lst_first(lst) ((lst)->lst_next->data)
#define lst_last(lst) ((lst)->lst_prev->data)
#define lst_end(lst) (lst)
#define lst_isempty(lst) ((lst) == (lst)->lst_next)

#define lst_forallpairs(type1,x1,lst1,type2,x2,lst2)\
	{\
	register type1 (x1);\
	register type2 (x2);\
	register LISTENTRY *lst_cursor1;\
	register LISTENTRY *lst_cursor2;\
	if((lst1) && (lst2))\
	   for( lst_cursor1=(lst1)->lst_next,\
		lst_cursor2=(lst2)->lst_next;\
		(lst_cursor1!=(lst1))&&(lst_cursor2!=(lst2))?\
			((x1)=(type1)lst_cursor1->data,\
			 (x2)=(type2)lst_cursor2->data,TRUE):FALSE;\
		lst_cursor1=lst_cursor1->lst_next,\
		lst_cursor2=lst_cursor2->lst_next)\
	      {

#define lst_forall(type,x,lst)\
	{\
	register type x;\
	register LISTENTRY *lst_cursor;\
	if(lst)\
	   for(lst_cursor=(lst)->lst_next; \
		(lst_cursor!=(lst))?(x=(type)lst_cursor->data,TRUE):FALSE;\
		lst_cursor=lst_cursor->lst_next)\
	      {

#define lst_forbackwards(type,x,lst) \
	{\
	register type x;\
	register LISTENTRY *lst_cursor;\
	if(lst)\
	   for(lst_cursor=(lst)->lst_prev;\
	      (lst_cursor!=(lst))?(x=(type)lst_cursor->data,TRUE):FALSE;\
	      lst_cursor=lst_cursor->lst_prev)\
	      {

#define lst_forallentries(lst_cursor,lst) \
	{\
	register LISTENTRY *lst_cursor;\
	if(lst)\
	   for(lst_cursor=(lst)->lst_next;\
	      (lst_cursor!=(lst));\
	      lst_cursor=lst_cursor->lst_next)\
	      {

#define lst_endall }}

/* Macros for simple insert operations */

#define lst_insertstart(dat,lst) lst_insertafter((DATA)(dat),(lst))
#define lst_insertend(dat,lst) lst_insertafter((DATA)(dat),lst_lastentry(lst))
#define lst_insert(dat,lst) lst_insertstart((dat),(lst))
#define lst_push(dat,stack) lst_insertstart((dat), (stack))
#define lst_enqueue(dat, que) lst_insertend((dat), (que))

#endif
