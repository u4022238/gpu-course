/* 
 * Routines for computing error functions, and chisq
 */

#include <Utilities/utilities_c.h>

#include <math.h>
#define MAXITER 100
#define EPS 3.0e-7

static float gammln(float xx)
   {
   double x, tmp, ser;
   static double cof[6] = {
	 76.18009173,
	-86.50532033,
	 24.01409822, 
	 -1.231739516,
	  0.120858003e-2,
	 -0.536382e-5};
   int j;

   x = xx - 1.0;
   tmp = x + 5.5;
   tmp -= (x + 0.5)*log(tmp);
   ser = 1.0;

   for (j=0; j<=5; j++) 
      {
      x += 1.0;
      ser += cof[j]/x;
      }

   return -tmp + log(2.50662827465*ser);
   }

static void gcf(float *gammcf, float a, float x, float *gln)
   {
   int n;
   float gold = 0.0, g, fac = 1.0, b1 = 1.0;
   float b0 = 0.0, anf, ana, an, a1, a0 = 1.0;

   *gln = gammln(a);
   a1 = x;
   for (n=1; n<=MAXITER; n++) 
      {
      an = (float) n;
      ana = an - a;
      a0 = (a1 + a0*ana)*fac;
      b0 = (b1 + b0*ana)*fac;
      anf = an*fac;
      a1 = x*a0 + anf*a1;
      b1 = x*b0 + anf*b1;
      if (a1) 
         {
         fac = 1.0/a1;
         g = b1*fac;
         if (fabs((g - gold)/g) < EPS) 
	    {
            *gammcf = exp(-x + a*log(x) - (*gln))*g;
            return;
            }
         gold = g;
         }
      }

   error_message("a too large, MAXITER too small in routine GCF");
   bail_out(2);
   }

static void gser(float *gamser, float a, float x, float *gln)
   {
   int n;
   float sum, del, ap;

   *gln = gammln(a);

   if (x <= 0.0) 
      {
      if (x < 0.0) 
	 {
	 error_message("x less than 0 in routine GSER");
	 bail_out(2);
	 }
      *gamser = 0.0;
      return;
      } 
   else 
      {
      ap = a;
      del = sum = 1.0/a;
      for (n=1; n<=MAXITER; n++) 
	 {
         ap += 1.0;
         del *= x/ap;
         sum += del;
         if (fabs(del) < fabs(sum)*EPS) 
	    {
            *gamser = sum*exp(-x + a*log(x) - (*gln));
            return;
            }
         }

      error_message ("a too large, MAXITER too small in routine GSER");
      bail_out(2);
      return;
      }
   }

FUNCTION_DEF (float gammp, (float a, float x))
   {
   float gamser, gammcf, gln;

   if (x < 0.0 || a <= 0.0) 
      {
      error_message("Invalid arguments in routine GAMMP");
      bail_out(2);
      }
   if (x < (a + 1.0)) 
      {
      gser(&gamser, a, x, &gln);
      return gamser;
      } 
   else 
      {
      gcf(&gammcf, a, x, &gln);
      return 1.0 - gammcf;
      }
   }

FUNCTION_DEF (float gammq, (float a, float x))
   {
   float gamser, gammcf, gln;

   if (x < 0.0 || a <= 0.0) 
      {
      error_message("Invalid arguments in routine GAMMQ");
      bail_out(2);
      }
   if (x < (a + 1.0)) 
      {
      gser(&gamser, a, x, &gln);
      return 1.0 - gamser;
      } 
   else 
      {
      gcf(&gammcf, a, x, &gln);
      return gammcf;
      }
   }

FUNCTION_DEF (float rherf, (float x))
   {
   return x < 0.0 ? -gammp(0.5, x*x) : gammp(0.5, x*x);
   }

FUNCTION_DEF (float rherfc, (float x))
   {
   return x < 0.0 ? 1.0 + gammp(0.5, x*x) : gammq(0.5, x*x);
   }

FUNCTION_DEF (float rherfcc, (float x))
   {
   float t, z, ans;

   z = fabs(x);
   t = 1.0/(1.0 + 0.5*z);
   ans = t*exp(-z*z - 
	1.26551223 + 
	t*( 1.00002368 + 
	t*( 0.37409196 + 
	t*( 0.09678418 + 
        t*(-0.18628806 + 
	t*( 0.27886807 + 
	t*(-1.13520398 + 
	t*( 1.48851587 + 
        t*(-0.82215223 + 
	t*  0.17087277
	)))))))));

   return  x >= 0.0 ? ans : 2.0 - ans;
   }

FUNCTION_DEF (float chisq , (double x, int n))
   {
   /* Gives the chisq lower tail probability */
   return gammp(n/2.0, x/2.0);
   }

FUNCTION_DEF (float chisq_lower_tail , (double x, int n))
   {
   return chisq(x, n);
   }

FUNCTION_DEF (float chisq_upper_tail, (double x, int n))
   {
   return 1.0 - chisq(x, n);
   }

FUNCTION_DEF (float invchisq, (double x, int n))
   {
   /* Determines the inverse of the chisq function for a given
    * value of n.  Proceeds by binary search.
    * This is used for questions like : If I want to get x percent
    * of samples, then at what value do I need to set the threshold.
    * n is the number of degrees of freedom.
    */
   double low = 0.0;
   double high = 1.0;
   double mid, val, newval;

   /* First, check the input */
   if (x < 0.0 || x>= 1.0)
      {
      error_message ("Bad input (%e) to invchisq", x);
      bail_out(2);
      }

   /* Special case */
   if (x == 0.0) return 0.0;

   /* First, get a point above the goal */
   while (chisq(high, n) < x) high *= 2.0;

   /* Now, do a search */
   mid = (low + high) * 0.5;
   newval = chisq(mid, n);

   /* Binary search */
   do {
      /* Value from last iteration becomes the current */
      val = newval;

      /* Restrict the interval */
      if (val < x)
	 low = mid;
      else
	 high = mid;

      /* find the new mid point */
      mid = (low+high) * 0.5;

      /* Evaluate the function */
      newval = chisq(mid, n);
      } while (newval != val);

   return (mid);
   }
