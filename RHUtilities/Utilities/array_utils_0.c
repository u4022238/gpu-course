/* @(#)array_utils.c	1.18 11/26/96 */

/*>>
Utility routines for the Numerical recipes cookbook programs 
<<*/

#include <Utilities/cvar.h>
#include <Utilities/rh_util.h>
#include <Utilities/alloc.h>
#include <math.h>
#include <Utilities/gaussj_dcl.h>
#include <Utilities/symmetric_dcl.h>
#include <Utilities/array_utils_dcl.h>
#include <Utilities/complex_dcl.h>
#include <Utilities/base1_interface_dcl.h>


FUNCTION_DEF ( int p_matrix_0, ( double **a, int m, int n))
   {
   /* Print out a matrix of double values */
   /* Returns a value, so we can display at break points at break points */
   fprint_matrix ("%12.5e ", stdout, a, 0, m-1, 0, n-1);
   return 1;
   }

FUNCTION_DEF ( int p_vector_0, ( double *a, int n))
   {
   /* Print out a vector of double values */
   /* Returns a value, so we can display at break points at break points */
   fprint_vector ("%12.5e ", stdout, a, 0, n-1);
   return 1;
   }

FUNCTION_DEF ( void matrix_prod_0, (
        double **A,
        double **B,
        int l,
        int m,
        int n,
        double **X))
   {
   /* Multiplies a k x m matrix (A) by an m x n matrix (B) to get matrix X */
   /* All indices start at 0 */
   /* The goal matrix may be the same as one of the operands */
   int i, j, k;
   double **T = MATRIX (0, l-1, 0, n-1, double);     /* Temporary product */

   /* Clear the matrix */
   for (i=0; i<l; i++)
      for (j=0; j<n; j++)
         T[i][j] = 0.0;

   /* Do the multiplication */
   for (i=0; i<l; i++)
      for (j=0; j<n; j++)
         for (k=0; k<m; k++)
            T[i][j] += A[i][k] * B[k][j];

   /* Clear the matrix */
   for (i=0; i<l; i++)
      for (j=0; j<n; j++)
         X[i][j] = T[i][j];

   /* Free T */
   FREE (T, 0);
   }

FUNCTION_DEF ( void vector_matrix_prod_0, (
        double *b,
        double **A,
        int l,
        int m,
        double *x))
   {
   /* Multiplies a vector (b) by an  l x m matrix (A) to get vector x */
   /* All indices start at 0 */
   /* The goal vector may be the same as b */
   int i, k;
   double *T = VECTOR (0, m-1, double);     /* Temporary product */

   /* Clear the vector */
   for (i=0; i<m; i++)
      T[i] = 0.0;

   /* Do the multiplication */
   for (i=0; i<m; i++)
      for (k=0; k<l; k++)
         T[i] += b[k] * A[k][i];

   /* Transfer the vector */
   for (i=0; i<m; i++)
      x[i] = T[i];

   /* Free T */
   FREE (T, 0);
   }

FUNCTION_DEF ( void matrix_vector_prod_0, (
        double **A,
        double *b,
        int l,
        int m,
        double *x))
   {
   /* Multiplies a l x m matrix (A) by an m vector (b) to get vector x */
   /* All indices start at 0 */
   /* The goal vector may be the same as b */
   int i, k;
   double *T = VECTOR (0, l-1, double);     /* Temporary product */

   /* Clear the vector */
   for (i=0; i<l; i++)
      T[i] = 0.0;

   /* Do the multiplication */
   for (i=0; i<l; i++)
      for (k=0; k<m; k++)
         T[i] += A[i][k] * b[k];

   /* Transfer the vector */
   for (i=0; i<l; i++)
      x[i] = T[i];

   /* Free T */
   FREE (T, 0);
   }


FUNCTION_DEF ( double inner_product_0, (
        double *A,
        double *B,
        int n))
   {
   /* Returns the inner product of two vectors */
   int i;
   double prod = 0.0;

   /* Do the multiplication */
   for (i=0; i<n; i++)
      prod += A[i] * B[i];

   /* Return the product */
   return (prod);
   }

FUNCTION_DEF ( double vector_length_0, (
        double *A,
        int n))
   {
   /* Returns the length of the vector */
   return (sqrt (inner_product_0 (A, A, n)));
   }

FUNCTION_DEF ( int matrix_inverse_0, (
   double **A,		/* The matrix to be inverted */
   double **Ainv,	/* The inverse computed */
   int n		/* Dimension of the matrix */
   ))
   {
   /* Inverts the matrix */
   /* Returns 0 if matrix is not invertible */
   int i, j;
   
   /* First copy the matrix to Ainv */
   for (i=0; i<n; i++) for (j=0; j<n; j++)
      Ainv[i][j] = A[i][j];

   /* Now invert it on the spot */
   return gaussj2_check_0 (Ainv, n, (double **)0, 0);
   }

FUNCTION_DEF ( double det3x3_0, (
        double **A))
   {
   /* Takes a 3 by 3 determinant */
   return A[0][0]*A[1][1]*A[2][2] + A[0][1]*A[1][2]*A[2][0] + 
	  A[0][2]*A[1][0]*A[2][1] - A[0][0]*A[1][2]*A[2][1] - 
	  A[0][1]*A[1][0]*A[2][2] - A[0][2]*A[1][1]*A[2][0];
   }

FUNCTION_DEF ( double det2x2_0, (
        double **A))
   {
   /* Takes a 2 by 2 determinant */
   return A[0][0]*A[1][1] - A[0][1]*A[1][0];
   }

FUNCTION_DEF ( void cofactor_matrix_3x3_0, (
        double **A,
        double **Aadj))
   {
   /* Takes the cofactor matrix of a given 3x3 matrix */
   int i, j;
   
   /* First, take a copy of A */
   double **A2 = MATRIX (0, 2, 0, 2, double);
   for (i=0; i<3; i++) for (j=0; j<3; j++)
      A2[i][j] = A[i][j];

   /* Now take the cofactors */
   Aadj[0][0] = A2[1][1]*A2[2][2] - A2[1][2]*A2[2][1];
   Aadj[0][1] = A2[1][2]*A2[2][0] - A2[1][0]*A2[2][2];
   Aadj[0][2] = A2[1][0]*A2[2][1] - A2[1][1]*A2[2][0];

   Aadj[1][0] = A2[2][1]*A2[0][2] - A2[2][2]*A2[0][1];
   Aadj[1][1] = A2[2][2]*A2[0][0] - A2[2][0]*A2[0][2];
   Aadj[1][2] = A2[2][0]*A2[0][1] - A2[2][1]*A2[0][0];

   Aadj[2][0] = A2[0][1]*A2[1][2] - A2[0][2]*A2[1][1];
   Aadj[2][1] = A2[0][2]*A2[1][0] - A2[0][0]*A2[1][2];
   Aadj[2][2] = A2[0][0]*A2[1][1] - A2[0][1]*A2[1][0];

   /* Free the temporary matrix */
   FREE (A2, 0);
   }

FUNCTION_DEF ( void cross_product_0, (
   double *A, double *B,	/* Two vectors 0..2 */
   double *C			/* Their cross product */
   ))
   {
   C[0] = A[1]*B[2] - A[2]*B[1];
   C[1] = A[2]*B[0] - A[0]*B[2];
   C[2] = A[0]*B[1] - A[1]*B[0];
   }

FUNCTION_DEF ( int lin_solve_0, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ))
   {
   /* Solves a square linear system of equations */
   int i, j;
   int return_val;
   double **A2 = MATRIX (0, n-1, 0, n-1, double);
   double *b2  = VECTOR (0, n-1, double);

   /* Copy the values */
   for (i=0; i<n; i++) for (j=0; j<n; j++)
      A2[i][j] = A[i][j];
   for (i=0; i<n; i++)
      b2[i] = b[i];

   /* Now solve using gaussj */
   return_val = gaussj_check_0 (A2, n, b2);

   /* Now copy the values */
   if (return_val)
      for (i=0; i<n; i++)
         x[i] = b2[i];

   /* Free the temporary matrices */
   FREE (A2, 0);
   FREE (b2, 0);

   /* Return the appropriate value */
   return return_val;
   }
   
FUNCTION_DEF ( int lin_solve_symmetric_0, (
   double **A,		/* The matrix of coefficients */
   double *x,		/* The vector of unknowns */
   double *b,		/* The goal vector */
   int n		/* Size of the system */
   ))
   {
   /* Solves a square linear system of equations */
   int i, j;
   int return_val;
   double **A2 = MATRIX (0, n, 0, n, double);
   double *b2  = VECTOR (0, n, double);

   /* Copy the values */
   for (i=0; i<n; i++) for (j=0; j<n; j++)
      A2[i][j] = A[i][j];
   for (i=0; i<n; i++)
      b2[i] = b[i];

   /* Now solve using gaussj */
   return_val = solve_symmetric_0 (A2, n, b2);

   /* Now copy the values */
   if (return_val)
      for (i=0; i<n; i++)
         x[i] = b2[i];

   /* Free the temporary matrices */
   FREE (A2, 0);
   FREE (b2, 0);

   /* Return the appropriate value */
   return return_val;
   }
   
FUNCTION_DEF ( void identity_matrix_0, (
        double **A,
        int dim))
   { 
   /* Initializes the matrix A to be the identity */
   int i, j;
   for (i=0; i<dim; i++) 
      { 
      A[i][i] = 1.0; 
      for (j=i+1; j<dim; j++)
         A[i][j] = A[j][i] = 0.0;
      } 
   } 

