#ifndef _symmetric_c_dcl_
#define _symmetric_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (int ldl_decomposition_base1, (double **A, int n));

EXTERN_C_FUNCTION (void backsolve_ldl_base1, (double **A, double *x, int n));

EXTERN_C_FUNCTION (int solve_symmetric_base1, (double **A, int n, double *x));

EXTERN_C_FUNCTION (void backsolve_choleski_base1, (double **A, double *x, int n));

EXTERN_C_FUNCTION (int choleski_decomposition_base1, (double **A, int n));

EXTERN_C_FUNCTION (int solve_choleski, (double **A, int n, double *x));

EXTERN_C_FUNCTION (int ldl_decomposition_0, (double **A, int n));

EXTERN_C_FUNCTION (void backsolve_ldl_0, (double **A, double *x, int n));

EXTERN_C_FUNCTION (int solve_symmetric_0, (double **A, int n, double *x));

EXTERN_C_FUNCTION (void backsolve_choleski_0, (double **A, double *x, int n));

EXTERN_C_FUNCTION (int choleski_decomposition_0, (double **A, int n));

EXTERN_C_FUNCTION (int solve_choleski_0, (double **A, int n, double *x));

#endif
