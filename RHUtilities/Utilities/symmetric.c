/* @(#)symmetric.c	1.1 28 Nov 1995 */

/* Solves sets of linear equations, in the symmetric case.
 */

/* Author : Richard Hartley */

#include <Utilities/cvar.h>
#include <math.h>
#include <Utilities/alloc.h>
#include <Utilities/error_message_dcl.h>
#include <Utilities/array_utils_dcl.h>

FUNCTION_DEF (int ldl_decomposition_base1, (double **A, int n))
   {
   /* Does the LDL^T decomposition of a matrix */
   int i, j, k;

   /* Do it row by row */
   for (j=1; j<=n; j++)
      {
      /* Compute v */
      for (i=1; i<=j-1; i++)
	 A[i][j] = A[j][i] * A[i][i];

      /* Now, the diagonal element */
      for (k=1; k<=j-1; k++)
	 A[j][j] -= A[j][k]*A[k][j];

      /* If this is zero, then we are rank deficient */
      if (A[j][j] == 0.0)	/* An unlikely event, in floating point */
	 return 0;

      /* Now, fix up the elements in the j-th column */
      for (i=j+1; i<=n; i++)
	 {
	 double temp = A[i][j];
	 for (k=1; k<j; k++)
	    temp -= A[i][k]*A[k][j];
	 A[i][j] = temp / A[j][j];
	 }
      }

   /* Return success */
   return 1;
   }

FUNCTION_DEF (void backsolve_ldl_base1, (double **A, double *x, int n))
   {
   /* Assumes that A is in LDL form. Solves A b = x.
    * The solution, b overwrites x.
    */
   int i, j;

   /* First step of back substitution */
   for (i=2; i<=n; i++)
      for (j=1; j<i; j++)
         x[i] -= A[i][j] * x[j];

   /* Second set of back substitution */
   for (i=n; i>=1; i--)
      {
      for (j=i+1; j<=n; j++)
	 x[i] -= A[i][j] * x[j];
      x[i] /= A[i][i];
      }
   }

FUNCTION_DEF (int solve_symmetric_base1, (double **A, int n, double *x))
   {
   /* Solves a symmetric set of equations */
   /* On return, A is in LU form and x holds the solution */
 
   /* First, get the decomposition */
   if (! ldl_decomposition_base1 (A, n)) return 0;

   /* Now, do back substitution */
   backsolve_ldl_base1 (A, x, n);

   /* Return success */
   return 1;
   }

/*****************************************************************/
/*      Solving linear equations based on Choleski               */
/*****************************************************************/

FUNCTION_DEF (void backsolve_choleski_base1, (double **A, double *x, int n))
   {
   /* Assumes that A is in Choleski form.  In other works, only the
    * bottom half L of A is used.
    * Solves L L^T b  = x.
    * The solution, b overwrites x.
    */
   int i, j;

   /* First step of back substitution */
   for (i=1; i<=n; i++)
      {
      for (j=1; j<i; j++)
         x[i] -= A[i][j] * x[j];
      x[i] /= A[i][i];
      }

   /* Second set of back substitution */
   for (i=n; i>=1; i--)
      {
      for (j=i+1; j<=n; j++)
	 x[i] -= A[j][i] * x[j];
      x[i] /= A[i][i];
      }
   }

FUNCTION_DEF (int choleski_decomposition_base1, (double **A, int n))
   {
   /* Does the choleski decomposition of a matrix */
   int i, j, k;

   /* Do it row by row */
   for (j=1; j<=n; j++)
      {
      /* Reduce the j-th column */
      if (j > 1)
	 for (i=j; i<=n; i++)
	    for (k=1; k<j; k++)
	       A[i][j] -= A[i][k]*A[j][k];

      /* Divide by the square root */
      if (A[j][j] > 0.0)
         {
	 /* Work out the scaling factor */
	 double d = sqrt(A[j][j]);
	 double fac = 1.0 / d;

	 /* Update them */
	 A[j][j] = d;
	 for (i=j+1; i<=n; i++)
	    A[i][j] *= fac;
	 }
      else
	 return 0;
      }

   /* Return success */
   return 1;
   }

FUNCTION_DEF (int solve_choleski, (double **A, int n, double *x))
   {
   /* Solves a symmetric set of equations */
   /* On return, A is replaced by a matrix such that L L^T = A.
    * and x holds the solution */
   /*
    * This routine does not seem to be as good as solve_symmetric.
    *   + It is a bit slower.
    *   + It requires positive definite matrices.
    */
 
   /* First, get the decomposition */
   if (! choleski_decomposition_base1 (A, n)) return 0;

   /* Now, do back substitution */
   backsolve_choleski_base1 (A, x, n);

   /* Return success */
   return 1;
   }

//=============================================================================
//                           Base 0 versions
//=============================================================================

FUNCTION_DEF (int ldl_decomposition_0, (double **A, int n))
   {
   /* Does the LDL^T decomposition of a matrix */
   int i, j, k;

   /* Do it row by row */
   for (j=0; j<n; j++)
      {
      /* Compute v */
      for (i=0; i<j; i++)
	 A[i][j] = A[j][i] * A[i][i];

      /* Now, the diagonal element */
      for (k=0; k<j; k++)
	 A[j][j] -= A[j][k]*A[k][j];

      /* If this is zero, then we are rank deficient */
      if (A[j][j] == 0.0)	/* An unlikely event, in floating point */
	 return 0;

      /* Now, fix up the elements in the j-th column */
      for (i=j+1; i<n; i++)
	 {
	 double temp = A[i][j];
	 for (k=0; k<j; k++)
	    temp -= A[i][k]*A[k][j];
	 A[i][j] = temp / A[j][j];
	 }
      }

   /* Return success */
   return 1;
   }

FUNCTION_DEF (void backsolve_ldl_0, (double **A, double *x, int n))
   {
   /* Assumes that A is in LDL form. Solves A b = x.
    * The solution, b overwrites x.
    */
   int i, j;

   /* First step of back substitution */
   for (i=1; i<n; i++)
      for (j=0; j<i; j++)
         x[i] -= A[i][j] * x[j];

   /* Second set of back substitution */
   for (i=n-1; i>=0; i--)
      {
      for (j=i+1; j<n; j++)
	 x[i] -= A[i][j] * x[j];
      x[i] /= A[i][i];
      }
   }

FUNCTION_DEF (int solve_symmetric_0, (double **A, int n, double *x))
   {
   /* Solves a symmetric set of equations */
   /* On return, A is in LU form and x holds the solution */
 
   /* First, get the decomposition */
   if (! ldl_decomposition_0 (A, n)) return 0;

   /* Now, do back substitution */
   backsolve_ldl_0 (A, x, n);

   /* Return success */
   return 1;
   }

/*****************************************************************/
/*      Solving linear equations based on Choleski               */
/*****************************************************************/

FUNCTION_DEF (void backsolve_choleski_0, (double **A, double *x, int n))
   {
   /* Assumes that A is in Choleski form.  In other works, only the
    * bottom half L of A is used.
    * Solves L L^T b  = x.
    * The solution, b overwrites x.
    */
   int i, j;

   /* First step of back substitution */
   for (i=0; i<n; i++)
      {
      for (j=0; j<i; j++)
         x[i] -= A[i][j] * x[j];
      x[i] /= A[i][i];
      }

   /* Second set of back substitution */
   for (i=n-1; i>=0; i--)
      {
      for (j=i+1; j<n; j++)
	 x[i] -= A[j][i] * x[j];
      x[i] /= A[i][i];
      }
   }

FUNCTION_DEF (int choleski_decomposition_0, (double **A, int n))
   {
   /* Does the choleski decomposition of a matrix */
   int i, j, k;

   /* Do it row by row */
   for (j=0; j<n; j++)
      {
      /* Reduce the j-th column */
      if (j > 0)
	 for (i=j; i<n; i++)
	    for (k=0; k<j; k++)
	       A[i][j] -= A[i][k]*A[j][k];

      /* Divide by the square root */
      if (A[j][j] > 0.0)
         {
	 /* Work out the scaling factor */
	 double d = sqrt(A[j][j]);
	 double fac = 1.0 / d;

	 /* Update them */
	 A[j][j] = d;
	 for (i=j+1; i<n; i++)
	    A[i][j] *= fac;
	 }
      else
	 return 0;
      }

   /* Return success */
   return 1;
   }

FUNCTION_DEF (int solve_choleski_0, (double **A, int n, double *x))
   {
   /* Solves a symmetric set of equations */
   /* On return, A is replaced by a matrix such that L L^T = A.
    * and x holds the solution */
   /*
    * This routine does not seem to be as good as solve_symmetric.
    *   + It is a bit slower.
    *   + It requires positive definite matrices.
    */
 
   /* First, get the decomposition */
   if (! choleski_decomposition_0 (A, n)) return 0;

   /* Now, do back substitution */
   backsolve_choleski_0 (A, x, n);

   /* Return success */
   return 1;
   }

