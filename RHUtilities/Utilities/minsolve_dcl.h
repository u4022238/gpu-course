#ifndef _minsolve_c_dcl_
#define _minsolve_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void check_min_solve_base1, (
   double **A,			/* Coefficient matrix */
   double *x,			/* The solution vector */
   int m, int n			/* The size of the system */
   ));

EXTERN_C_FUNCTION ( void min_solve_base1, (
   double **A,		/* Coefficient matrix */
   double *x,		/* The solution vector */
   int m, int n		/* The size of the system */
   ));

EXTERN_C_FUNCTION ( void min_solve_direct_base1, (
   double **X,			/* The system to be solved */
   double *x,			/* The solution */
   int n			/* The dimensions of the system */
   ));

#endif
