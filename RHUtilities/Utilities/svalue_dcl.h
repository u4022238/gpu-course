#ifndef _svalue_c_dcl_
#define _svalue_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void svbksb_base1, (
        double **u,
        double w[],
        double **v,
        int m,
        int n,
        double b[],
        double x[]));

EXTERN_C_FUNCTION ( int solve_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol));

EXTERN_C_FUNCTION ( int solve_simple_base1, (
        double **a,
        double *x,
        double *b,
        int nrow,
        int ncol));

#endif
