#ifndef _error_message_c_dcl_
#define _error_message_c_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities/rh_util.h>
/* */

EXTERN_C_FUNCTION (void set_error_output_file, (FILE *afile));

EXTERN_C_FUNCTION (FILE *error_output_file, ());

EXTERN_C_FUNCTION (void set_info_output_file, (FILE *afile));

EXTERN_C_FUNCTION (FILE *info_output_file, ());

EXTERN_C_FUNCTION (void set_warning_output_file, (FILE *afile));

EXTERN_C_FUNCTION (FILE *warning_output_file, ());

EXTERN_C_FUNCTION (void set_debugging_output_file, (FILE *afile));

EXTERN_C_FUNCTION (FILE *debugging_output_file, ());

EXTERN_C_FUNCTION ( rhBOOLEAN enable_debugging_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN enable_informative_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN enable_warning_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN enable_error_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN disable_informative_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN disable_warning_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN disable_error_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN disable_debugging_messages, ());

EXTERN_C_FUNCTION ( rhBOOLEAN warning_messages_enabled, ());

EXTERN_C_FUNCTION ( rhBOOLEAN informative_messages_enabled, ());

EXTERN_C_FUNCTION ( rhBOOLEAN error_messages_enabled, ());

EXTERN_C_FUNCTION ( rhBOOLEAN debugging_messages_enabled, ());

EXTERN_C_FUNCTION ( void debugging_message, (const char* fmt, ... /* DOTDOTDOT*/));

EXTERN_C_FUNCTION ( void informative_message, (const char* fmt, DOTDOTDOT));

EXTERN_C_FUNCTION ( void warning_message, (const char* fmt, DOTDOTDOT));

EXTERN_C_FUNCTION ( void error_message, (const char* fmt, DOTDOTDOT));

EXTERN_C_FUNCTION ( void fatal_error_message, (const char* fmt, DOTDOTDOT));

#endif
