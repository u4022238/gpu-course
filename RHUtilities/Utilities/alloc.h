/*
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
*/

/* @(#)alloc.h	1.11 21 Dec 1994 */
/* Macros for allocating and freeing matrices and vectors */

#ifndef alloc_h
#define alloc_h

#include <Utilities/rh_util.h>
#include <Utilities/dll.h>

#ifdef SUN4
#include <alloca.h>
#endif

void rh_free (void *a);


#ifndef __cplusplus

EXTERN_C UTILITIES_DLLDATA char *zztchar;

#endif

/* Allocate storage on the heap */
#define VECTOR(nl, nh, type)                                            \
        (type *) (MALLOC(((nh)-(nl)+1)*sizeof(type)) - (nl)*sizeof(type))

/* Allocate storage on the heap */
#define MATRIX(xl,xh,yl,yh,type)                			\
        (type **) malloc_matrix ((xl), (xh), (yl), (yh), sizeof(type))

#define ARRAY3D(xl,xh,yl,yh,zl,zh,type)                			\
        (type ***) malloc_array3D ((xl), (xh), (yl), (yh), (zl), (zh), \
		sizeof(type))
 
#define FREE(m,nrl) 				\
   {						\
   if (m) free((char *)((m)+(nrl)));		\
   m = NULL;					\
   }

#define rhFREE_macro(m,nrl) 				\
   {						\
   if (m) rh_free((char *)((m)+(nrl)));		\
   m = NULL;					\
   }

#define SUBMATRIX(A,xl,yl,xdim,ydim,orgx,orgy,type)		\
	(type **) submatrix (A, xl, yl, xdim, orgx, orgy, sizeof(type))

#define ARRAY(n,p) VECTOR (0, (n)-1, p)

/*--------------------------------------------------------------------
 * These macros should be used instead of VECTOR and FREE, when
 * we mant to make vectors of class members.
 * The difference is that in C++ we will call the empty constructor
 * methods for vectors of class members, such as VECTOR (1, 3, Vector).
 *-------------------------------------------------------------------*/

#ifdef __cplusplus

#   define GVECTOR(nl, nh, type) ((new type [(nh)-(nl)+1]) - (nl))
#   define GVFREE(m,nrl) delete [] ((m) + (nrl))

#else

/* Included just in case we use this with ordinary data types in a c program */
#   define GVECTOR(nl, nh, type) VECTOR(nl, nh, type)
#   define GVFREE(m,nrl) FREE(m,nrl)

#endif


#endif
