/* @(#)table.c	3.12 11/26/96 */

/*
 * Module Purpose: Utility routines for operating upon tables.
 *
 * Creation Date: 5/7/87
 *
 * Class 3: General Electric Proprietary Data
 * Copyright (C) 1987 General Electric Company
 *
 */


/****************************************************************/
/* This is a set of programs for handling tables.		*/
/* It consists of routines for entering entries in a table,	*/
/* for finding entries in a table, and for running through 	*/
/* the table in forwards or backwards order.			*/
/* 								*/
/* The table is stored as a threaded binary tree.		*/
/* Although the declaration indicates that the table may only	*/
/* hold a character string as data, it may of course (without	*/
/* change) be used to store a pointer to any structure.  For	*/
/* comparison of data types, it is necessary when inserting	*/
/* or finding an entry to supply the name of a function used	*/
/* to compare different data entries.  See manual entry		*/
/* for strcmp() to see the sort of function which is required.	*/
/*								*/
/* The manner of using the routines is as follows :		*/
/*								*/
/* #include <Utilities/table.h>						*/
/* TABLE x;							*/
/* TABLE cursor;						*/
/* TABLE entry;							*/
/* struct something *dat;					*/
/* int (*compare)();						*/
/*								*/
/* To insert dat into the table x :				*/
/*    entry = insert (dat, &x, compare);			*/
/* entry will point to the entry containing the data.		*/
/*								*/
/* To find the entry containing the data dat in table x,	*/
/*    entry = findentry (dat, &x, compare);			*/
/* entry will point to the entry containg the data.  		*/
/* Then, refer to the data as entry->data			*/
/*								*/
/* To run through the elements in the table in sorted order	*/
/*   for (cursor=firstentry(x); cursor; cursor=nextentry(cursor))*/
/*      {							*/
/*      cursor->data contains the data.				*/
/*      }							*/
/*								*/
/* To run backwards through all entries in order:		*/
/*   for (cursor=lastentry(x); cursor; cursor=preventry(cursor))*/
/*      {							*/
/*      cursor->data contains the data.				*/
/*      }							*/
/****************************************************************/

#include <Utilities/cvar.h>
# include <Utilities/rh_util.h>
# include <Utilities/table.h>
# include <Utilities/rhlist.h>
# define left tbl_left
# define right tbl_right
# define leftleaf tbl_leftleaf
# define rightleaf tbl_rightleaf
#define BF tbl_BF

#ifdef TBL_FASTALLOC
/*>> Facilities for allocating and freeing table entries <<*/
#define INITSIZE 1024
static unsigned num_freeentries = 0;   /* Index of the first available entry */
static TABLEENTRY **addresslist = NULL;/* List of free entries */
static unsigned  addresslist_size;     /* The size of the list of free entries*/

FUNCTION_DEF (static TABLEENTRY *tbl_newentry, ())
   {
   /* Provides a new table entry from a list */
   /* See if the entrylist has been initialized */
   if (num_freeentries == 0)
      {
      /* Need more entries */
      int i;

      /* Allocate a bunch of entries */
      TABLEENTRY *newblock = ARRAY (INITSIZE, TABLEENTRY);

      /* Check that the address list has been initialized */
      if (addresslist == NULL)
	 {
         addresslist_size = INITSIZE;
	 addresslist = ARRAY (INITSIZE, TABLEENTRY *);
	 }

      /* Record the addresses of the free entries */
      for (i=0; i<INITSIZE; i++)
	 addresslist[i] = newblock+i;

      /* Point num_freeentries to the top of the stack */
      num_freeentries = INITSIZE;
      }

   /* Now return the required block */
   return (addresslist[--num_freeentries]);
   }

FUNCTION_DEF (static void tbl_freeentry, (TABLEENTRY *anentry))
   {
   /* Returns a list entry to the free list */
   /* First make sure that there is enough room to hold the free list */
   if (num_freeentries >= addresslist_size)

     /* below statement is equivalent to REALLOC, */
     /* but it includes the (TABLEENTRY**) cast   */

     if ((addresslist_size *= 2,
	  addresslist = (TABLEENTRY **) realloc (addresslist,
				addresslist_size*sizeof(*addresslist)))== NULL)
       ABORT
   /* Now return the entry */
   addresslist[num_freeentries++] = anentry;
   }
#else
#define tbl_newentry() 		NEW(TABLEENTRY)
#define tbl_freeentry(x) 	free(x)
#endif

FUNCTION_DEF ( rhBOOLEAN tbl_insert, (
    DATA data,		/* Data to be entered in the table */
    TABLE **table,	/* Address of pointer to the table */
    FUNCTION_DECL (int (*compare), (DATA, DATA))
			/* Function for comparing data */
   ))

    /* This routine will insert entries in a table. 
     * It returns FALSE and does nothing if the entry was already present.
     */

    /* The algorithm is an adaptation of the one in Horowitz and Sahni,
     * Data Structures, page 454 */
    {
    TABLEENTRY *Y;
    TABLEENTRY **F, **cursor;
   
    /* Special case, empty tree */
    if (*table == NULL)
	{
	*table = tbl_newentry();
	(*table)->left = (*table)->right = NULL;
   	(*table)->leftleaf = (*table)->rightleaf = TRUE;
  	(*table)->BF = 0;
	(*table)->data = data;
	return (TRUE);
	}
    
    /* Locate insertion point for data */
    F = cursor = table;
    while (1) 
	{
	register TABLEENTRY *P = *cursor;
   	int n;
	if (P->BF != 0) F = cursor;
	if ((n=(*compare)(data, P->data)) < 0)
	    {
	    cursor = &(P->left);
	    if (P->leftleaf) 
		{
		Y = tbl_newentry();
		Y->leftleaf = Y->rightleaf = TRUE;
		Y->data = data;
		Y->BF = 0;
		Y->right = P;
		Y->left = P->left;
		P->left = Y;
		P->leftleaf = FALSE;
		break;
		}
	    }
	else if (n > 0)
	    {
	    cursor = &(P->right);
	    if (P->rightleaf) 
	 	{
		Y = tbl_newentry();
		Y->leftleaf = Y->rightleaf = TRUE;
		Y->data = data;
		Y->BF = 0;
		Y->left = P;
		Y->right = P->right;
		P->right = Y;
		P->rightleaf = FALSE;
		break;
		}
	    }
	else /* n = 0 */
	    {
	    return (FALSE);
	    }
	}

    /* Now update the BF values on the path from A to Q. */
        {
        TABLEENTRY *P = *F;
        while (P != Y)
	    {
	    if ((*compare)(data, P->data) < 0)
	        {
	        P->BF += 1;
	        P = P->left;
	        }
	    else
	        {
	        P->BF -= 1;
	        P = P->right;
	        }
	    }
	}

    /* Now balance the tree at F */
    tbl_balance (F);
    
    /* Return the new node */
    return (TRUE);
    }

FUNCTION_DEF ( TABLEENTRY *tbl_findentry, (
        DATA data,
        TABLE **ptable,
        int (*compare)(DATA, DATA)))
   {
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table);
      else if (n<0)
	 if (table->leftleaf) return (NULL);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (NULL);
	 else table = table->right;
   } 

 
FUNCTION_DEF ( TABLEENTRY *tbl_nextentry, (
        TABLEENTRY *anentry))
   {
   if (anentry->rightleaf)
      return (anentry->right);
   else
      {
      anentry = anentry->right;
      while (! anentry->leftleaf) anentry = anentry->left;
      return (anentry);
      }
   }

 
FUNCTION_DEF ( TABLEENTRY *tbl_preventry, (
        TABLEENTRY *anentry))
   {
   if (anentry->leftleaf)
      return (anentry->left);
   else
      {
      anentry = anentry->left;
      while (! anentry->rightleaf) anentry = anentry->right;
      return (anentry);
      }
   }

FUNCTION_DEF ( TABLEENTRY *tbl_firstentry, (
        TABLE *atable))
   {
   if (atable == NULL) return (NULL);
   while (! atable->leftleaf) atable = atable->left;
   return (atable);
   }

FUNCTION_DEF ( TABLEENTRY *tbl_lastentry, (
        TABLE *atable))
   {
   if (atable == NULL) return (NULL);
   while (! atable->rightleaf) atable = atable->right;
   return (atable);
   }

FUNCTION_DEF ( DATA tbl_first, (
        TABLE *atable))
   {
   if (atable == NULL) return (NULL);
   while (! atable->leftleaf) atable = atable->left;
   return (atable->data);
   }

FUNCTION_DEF ( DATA tbl_last, (
        TABLE *atable))
   {
   if (atable == NULL) return (NULL);
   while (! atable->rightleaf) atable = atable->right;
   return (atable->data);
   }


FUNCTION_DEF ( DATA tbl_find, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))

   /* The same as tbl_findentry, except that it returns the data */
   {
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table->data);
      else if (n<0)
	 if (table->leftleaf) return (NULL);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (NULL);
	 else table = table->right;
   } 

FUNCTION_DEF ( TABLEENTRY *tbl_nextpreorder, (
        TABLEENTRY *entry))
   /* Returns the next entry in preorder */
   {
   if (!entry->leftleaf) return(entry->left);
   else while (entry && (entry->rightleaf)) entry = entry->right;
   if (entry) return (entry->right);
   else return (NULL);
   }

FUNCTION_DEF ( int tbl_balance, (
        TABLE **F))

    /* F is the address of a pointer to a table.
     * This routine balances the table at this node by applying AVL
     * rotations.  It returns a value equal to the decrease in height
     * of the table.
     */
    {
    TABLEENTRY *A = *F;

    if ((A->BF<2) && (A->BF>-2)) return (0);	/* Tree already balanced */

    if (A->BF == 2)		/* left imbalance */
   	{
	TABLEENTRY *B = A->left;
	if (B->BF == 1)		/* LL imbalance */
	    {
	    /* Fix the pointers */
	    A->left = B->right;
	    B->right = A;
	    *F = B;

  	    /* Fix the balance factors */
	    A->BF = B->BF = 0;

	    /* Fix up the threads */
	    if (B->rightleaf)
		{
		A->left = B;
		B->rightleaf = FALSE;
		A->leftleaf = TRUE;
		}
	    return (1);
	    }

	else if (B->BF == 0)		/* LL imbalance */
	    {
	    /* Fix the pointers */
	    A->left = B->right;
	    B->right = A;
	    *F = B;

  	    /* Fix the balance factors */
	    A->BF = 1;
	    B->BF = -1;

	    /* Threads must be correct */
	    return (0);
	    }

	else 		/* LR imbalance */
	    {
	    TABLEENTRY *C = B->right;

	    /* Fix the pointers */
	    B->right = C->left;
	    A->left = C->right;
	    C->left = B;
	    C->right = A;
	    *F = C;

	    /* Fix the balance factors */
	    if (C->BF == 1)
	     	{
		A->BF = -1;
		B->BF = 0;
		C->BF = 0;
		}
	    else if (C->BF == -1)
		{
		A->BF = 0;
		B->BF = 1;
		C->BF = 0;
		}
	    else
		A->BF = B->BF = C->BF = 0;

	    /* Fix the threads */
	    if (C->leftleaf)
		{
		B->right = C;
		C->leftleaf = FALSE;
		B->rightleaf = TRUE;
		}
	    if (C->rightleaf)
		{
		A->left = C;
		C->rightleaf = FALSE;
		A->leftleaf = TRUE;
		}
	  
	    return (1);
	    }
	}

    else if (A->BF == -2)			/* R imbalance */
   	{
	TABLEENTRY *B = A->right;
	if (B->BF == -1)		/* RR imbalance */
	    {
	    /* Fix the pointers */
	    A->right = B->left;
	    B->left = A;
	    *F = B;

	    /* Fix the balance factors */
	    A->BF = B->BF = 0;

	    /* Fix up the threads */
	    if (B->leftleaf)
		{
		A->right = B;
		B->leftleaf = FALSE;
		A->rightleaf = TRUE;
		}
	  
	    return (1);
	    }

	else if (B->BF == 0)		/* RR imbalance */
	    {
	    /* Fix the pointers */
	    A->right = B->left;
	    B->left = A;
	    *F = B;

  	    /* Fix the balance factors */
	    A->BF = -1;
	    B->BF = 1;

	    /* Threads must be correct */
	    return (0);
	    }

	else			/* RL imbalance */
	    {
	    TABLEENTRY *C = B->left;

	    /* Fix the pointers */
	    B->left = C->right;
	    A->right = C->left;
	    C->right = B;
	    C->left = A;
	    *F = C;

	    /* Fix the balance factors */
	    if (C->BF == -1)
	     	{
		A->BF = 1;
		B->BF = 0;
		C->BF = 0;
		}
	    else if (C->BF == 1)
		{
		A->BF = 0;
		B->BF = -1;
		C->BF = 0;
		}
	    else
		A->BF = B->BF = C->BF = 0;

	    /* Fix the threads */
	    if (C->rightleaf)
		{
		B->left = C;
		C->rightleaf = FALSE;
		B->leftleaf = TRUE;
		}
	    if (C->leftleaf)
		{
		A->right = C;
		C->leftleaf = FALSE;
		A->rightleaf = TRUE;
		}

	    return (1);
	    }
	}

    else
      	{
	fprintf (stderr, "Internal error : Bad balance factor in tree\n");
	abort();
        return (0);	/* Do not actually get here */
	}
    }
		    

FUNCTION_DEF ( int tbl_depth, (
        TABLE *table))
   {
   /* Tests the depth of a table and checks it out */
   if (table == NULL) return (0);
   if (table->rightleaf && table->leftleaf) 
      return (1);
   if (table->rightleaf)
      return (tbl_depth (table->left) + 1);
   if (table->leftleaf)
      return (tbl_depth (table->right) + 1);
   else
      {
      int nl = tbl_depth (table->left);
      int nr = tbl_depth (table->right);
      return (nl>nr?nl+1:nr+1);
      }
   }

FUNCTION_DEF ( rhBOOLEAN tbl_delete, (
        DATA data,
        TABLE **table,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))
   /* Delete the data from the table.  
    * Returns FALSE if the data was not found.
    */
   {
   TABLEENTRY **cursor = table;
   TABLEENTRY *P = *cursor;
   LIST *nodelist; 

   /* Special case, empty tree */
   if (*table == NULL) return (FALSE);

   /* First of all we must find the data.  On the way, build a list
    * of the nodes visited;
    */
   nodelist = lst_init();

   while (1)
   	{
	int n = (*compare)(data, P->data);
	if (n < 0)
	    {
	    if (P->leftleaf) 
		{
		lst_free (nodelist);
		return (FALSE);
		}
	    lst_insert (cursor, nodelist);
	    cursor = &(P->left);
	    P = *cursor;
	    }
	else if (n > 0)
	    {
	    if (P->rightleaf) 
		{
		lst_free (nodelist);
		return (FALSE);
		}
	    lst_insert (cursor, nodelist);
	    cursor = &(P->right);
	    P = *cursor;
	    }
	else break;
	}
		
   /* At this point cursor points at the node found.
    * Now continue on down the tree until we find a leaf */
   while (!(P->leftleaf && P->rightleaf))
	{
	TABLEENTRY *entry = P;
      	if (!P->leftleaf)
	    {
	    lst_insert (cursor, nodelist);
	    cursor = &(P->left);
	    P = *cursor;
            while (!P->rightleaf) 
	    	{
	        lst_insert (cursor, nodelist);
		cursor = &(P->right);
		P = *cursor;
		}
             }
	else 
            {
	    lst_insert (cursor, nodelist);
	    cursor = &(P->right);
	    P = *cursor;
	    while (!P->leftleaf)
		{
	        lst_insert (cursor, nodelist);
		cursor = &(P->left);
		P = *cursor;
		}
	    }
	entry->data = P->data;
	}
   
   /* Delete a leaf */
      {
      TABLEENTRY *ee;
      if ((ee = P->left) && ee->right == P)
         {
         /* Delete a right-hanging leaf */
         ee->right = P->right;
         ee->rightleaf = TRUE;
         }
      else if ((ee = P->right) && ee->left == P)
         {
         /* Delete a left hanging leaf */
         ee->left = P->left;
         ee->leftleaf = TRUE;
         }
      else if ((P->left == NULL) && (P->right == NULL))
         *table = NULL;

      /* Free the entry */
      tbl_freeentry(P);
      }

    /* Now return up the tree balancing as we go */
    lst_forall (TABLEENTRY **, F, nodelist)
     	{
	int n;
	if (cursor == &((*F)->left))
	    n = --((*F)->BF);
	else n = ++((*F)->BF);
	if (n==1 || n==-1) break;
	else if (n!=0 && tbl_balance(F)==0) break;
	cursor = F;
	} lst_endall;
   
   /* Finally delete the list and return */
   lst_free (nodelist);
   return (TRUE);
   }

FUNCTION_DEF ( TABLEENTRY *tbl_firstentryafter, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))
   {
   /* Returns the next entry in the table which would fall after the key */
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table);
      else if (n<0)
	 if (table->leftleaf) return (table);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (table->right);
	 else table = table->right;
   } 

FUNCTION_DEF ( TABLEENTRY *tbl_lastentrybefore, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))
   {
   /* Returns the lat entry in the table which falls before the key */
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table);
      else if (n<0)
	 if (table->leftleaf) return (table->left);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (table);
	 else table = table->right;
   } 

FUNCTION_DEF ( DATA tbl_firstafter, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))
   /* The same as tbl_firstentryafter, except that it returns the data */
   {
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table->data);
      else if (n<0)
	 if (table->leftleaf) return (table->data);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (table->right?table->right->data:NULL);
	 else table = table->right;
   } 

 
FUNCTION_DEF ( DATA tbl_lastbefore, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	))
   /* The same as tbl_firstentryafter, except that it returns the data */
   {
   register TABLE *table = *ptable;
   register int n;
   if (table == NULL) return (NULL);
   
   while (1)
      if ((n=(*compare)(data, table->data))==0)
      	 return (table->data);
      else if (n<0)
	 if (table->leftleaf) return (table->left?table->left->data:NULL);
	 else table = table->left;
      else /* n > 0 */
      	 if (table->rightleaf) return (table->data);
	 else table = table->right;
   } 

/*>>
Various diagnostic routines
<<*/

FUNCTION_DEF ( int tbl_check, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (int (*compare), (DATA, DATA)),
   			/* Function used for comparison */
   FUNCTION_DECL (char *(*idfn), (TABLE *))
			/* Function used to identify a node */
   ))
   {
   /* Tests the depth of a table and checks it out */
   char *nodeid;
   if (idfn)
      nodeid = (*idfn)(table);
   else nodeid = "UNKNOWN";

   if (table == NULL) return (0);

   /* Check the ordering */
   if (! table->rightleaf)
      {
      /* Check the node against its right leaf */
      if ((*compare)(table->data, table->right->data) >= 0)
	 {
	 fprintf (afile, "Order error at node \"%s\"\n", nodeid);
	 fprintf (afile, "\tRight descendent = \"%s\"\n", 
		(*idfn)(table->right));
  	 }
      }

   if (! table->leftleaf)
      {
      /* Check the node against its right leaf */
      if ((*compare)(table->data, table->left->data) <= 0)
	 {
	 fprintf (afile, "Order error at node \"%s\"\n", nodeid);
	 fprintf (afile, "\tLeft descendent = \"%s\"\n", 
		(*idfn)(table->left));
  	 }
      }

   /* Check the threading */
   if (table->leftleaf)
      {
      TABLEENTRY *p = table->left;
      if (p)
  	 {
	 p = p->right;
         while (! p->leftleaf) p = p->left;
         if (p != table)
	    fprintf (afile, "Threading error at node \"%s\"\n", nodeid);
	 }
      }
         
   if (table->rightleaf)
      {
      TABLEENTRY *p = table->right;
      if (p)
  	 {
	 p = p->left;
         while (! p->rightleaf) p = p->right;
         if (p != table)
	    fprintf (afile, "Threading error at node \"%s\"\n", nodeid);
	 }
      }
         

   /* Now check the balance */
   if (table->rightleaf && table->leftleaf) 
      {
      if (table->BF != 0)
	 fprintf (afile, "At node \"%s\" :0 leaf table->BF = %d\n", 
	    nodeid, table->BF);
      return (1);
      }
   if (table->rightleaf)
      {
      int n = tbl_check (afile, table->left, compare, idfn);
      if ((n != 1) || (table->BF != 1)) 
	 fprintf (afile, "At node \"%s\" :1 n = %d, table->BF = %d\n", 
		nodeid, n, table->BF);
      return (n + 1);
      }
   if (table->leftleaf)
      {
      int n = tbl_check (afile, table->right, compare, idfn);
      if ((n != 1) || (table->BF != -1))
	 fprintf (afile, "At node \"%s\" :2 n = %d, table->BF = %d\n" , 
		nodeid, n, table->BF);
      return (n + 1);
      }
   else
      {
      int nl = tbl_check (afile, table->left, compare, idfn);
      int nr = tbl_check (afile, table->right, compare, idfn);
      int b = nl - nr;
      if ((b != table->BF) || (b < -1) || (b > 1))
         fprintf (afile, "At node \"%s\" :3 table->BF = %d, nl = %d, nr = %d\n",
		nodeid, table->BF, nl, nr);
      return (nl>nr?nl+1:nr+1);
      }
   }
   
FUNCTION_DEF ( int tbl_dumppreorder, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (char *(*idfn), (TABLE *))
			/* Function used to identify a node */
   ))
   {
   /* Dumps the table in order */
   fprintf (afile, "%08x, %08x (%s)", 
	(unsigned) table, (unsigned) table->data, (*idfn)(table));
   if (!table->leftleaf && !table->rightleaf) fprintf (afile, "\n");
   else
      fprintf (afile, "(%s,%s)\n",
	  table->leftleaf ? 
		(table->left ? (*idfn)(table->left) : "(NULL)") : "-",
	  table->rightleaf ? 
		(table->right ? (*idfn)(table->right) : "(NULL)") : "-");

   if (! table->leftleaf )
      tbl_dumppreorder (afile, table->left, idfn);

   if (! table->rightleaf)
      tbl_dumppreorder (afile, table->right, idfn);

   /* Return a value */
   return 1;
   }


FUNCTION_DEF ( int tbl_dumpinorder, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (char *(*idfn), (TABLE *))	
			/* Function used to identify a node */
   ))
   {
   /* Dumps the table in order */
   if (! table->leftleaf )
      tbl_dumpinorder (afile, table->left, idfn);

   fprintf (afile, "%s", (*idfn)(table));
   if (!table->leftleaf && !table->rightleaf) fprintf (afile, "\n");
   else
      fprintf (afile, "(%s,%s)\n",
	  table->leftleaf ? 
		(table->left ? (*idfn)(table->left) : "(NULL)") : "-",
	  table->rightleaf ? 
		(table->right ? (*idfn)(table->right) : "(NULL)") : "-");

   if (! table->rightleaf)
      tbl_dumpinorder (afile, table->right, idfn);
   
   /* Return something  */
   return 1;
   }

FUNCTION_DEF ( int tbl_freetable, (
        TABLE *atable))
   {
   /* Frees the table without referencing freed data */
   
   /* If table is empty, then return */
   if (atable == NULL) return 1;

   /* Free the right subtree */
   if (! atable->rightleaf)
      tbl_freetable (atable->right);

   /* Free the left subtree */
   if (!atable->leftleaf)
      tbl_freetable (atable->left);

   /* Free the top node */
   tbl_freeentry (atable);

   /* Return something */
   return 1;
   }
