#ifndef _base1_interface_c_dcl_
#define _base1_interface_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (double ** rh_base1, (double **a, int n));

EXTERN_C_FUNCTION (int gaussj2_check_0, (
   	double **a,
   	int n,
	double **b,
	int m
	));

EXTERN_C_FUNCTION (int gaussj_check_0, (
   	double **a,
   	int n,
	double *b
	));

EXTERN_C_FUNCTION ( void QRdecomposition_3x3_0, (
	double **A, double **K, double **R
	));

EXTERN_C_FUNCTION ( int choleski_RRT_0, ( double **A, int n, double **K ));

EXTERN_C_FUNCTION ( double determinant_0, (double **A, int n ));

EXTERN_C_FUNCTION ( void jacobi_0, (double **a, int n, double *d, double **v ));

EXTERN_C_FUNCTION ( void min_solve_0, ( double **A, double *x, int m, int n ));

EXTERN_C_FUNCTION ( int solve_pseudo_0, (
	double **a, 
	double *x,
	double *b,
	int nrow,
	int ncol
	));

EXTERN_C_FUNCTION ( int solve_simple_0, (
	double **a,
	double *x,
	double *b,
	int nrow,
	int ncol
	));

EXTERN_C_FUNCTION ( int sorted_svd_0, (
	double **A,
	int m, int n,
	double **U, double *D, double **V 
	));

EXTERN_C_FUNCTION ( int svd3x3_0, (
 	double **A,
	double **U,
	double *D,
	double **V
	));

EXTERN_C_FUNCTION ( int svdcmp_0, (
	double **a, 
	int m,
	int n,
	double *w,
	double **v,
	int accumulate_u
	));

EXTERN_C_FUNCTION (void complex_roots_0, (
	fcomplex a[],
	int m,
	fcomplex roots[],
	int polish
	));

EXTERN_C_FUNCTION ( int choleski_3x3_0, (
   double **Ain, 	/* Input symmetric positive definite matrix */
   double **C		/* Output such that Ain = C*C^T */
   ));

EXTERN_C_FUNCTION ( void min_solve_direct_0, (
   double **X,			/* The system to be solved */
   double *x,			/* The solution */
   int n			/* The dimensions of the system */
   ));

EXTERN_C_FUNCTION ( void polydet_0, (
   double **Y1,		/* Matrix of constant terms */
   double **Yk,		/* Matrix of first degree terms */
   int n,		/* Size of the determinant */
   double *det)		/* The determinant as a polynomial */
   );

EXTERN_C_FUNCTION ( void complex_roots_real_coefficients_0, (
        double *a,
        int m,
        fcomplex *roots,
        int polish));

#endif
