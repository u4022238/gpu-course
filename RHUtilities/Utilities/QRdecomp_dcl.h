#ifndef _QRdecomp_c_dcl_
#define _QRdecomp_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void QRdecomposition_3x3_base1, (
   double **A, 	/* The input matrix */
   double **K, 	/* The output upper triangular matrix */
   double **R	/* The output rotation matrix */
   ));

#endif
