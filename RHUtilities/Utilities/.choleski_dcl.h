#ifndef _choleski_c_dcl_
#define _choleski_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (int choleski_RRT_base1, (
   double **A,
   int n,
   double **K
   ));

EXTERN_C_FUNCTION ( int choleski_3x3_base1, (
   double **Ain, 	/* Input symmetric positive definite matrix */
   double **C		/* Output such that Ain = C*C^T */
   ));

#endif
