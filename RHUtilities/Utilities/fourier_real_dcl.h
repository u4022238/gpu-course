#ifndef _fourier_real_c_dcl_
#define _fourier_real_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void fft_unpack, (
	double *data, 		/* Packed real fft */
        int i0,			/* Start index for data array */
	int n,			/* Dimension of data array */
	double *real,
	double *imag		/* Real and imaginary parts of unpacked */
	));

EXTERN_C_FUNCTION ( void fft_2D_unpack, (
	double **data, 		/* Packed real fft */
	double i0, int j0,	/* Starting indices for the array */
	int idim, int jdim, 
	double **real,
	double **imag		/* Real and imaginary parts of unpacked */
	));

EXTERN_C_FUNCTION ( void fft_real, ( double *data, int istart, int n));

EXTERN_C_FUNCTION ( int fourier2D_real, (
   double **re,			/* The image */
   int istart, int jstart,	/* Starting indices */
   int nrows, int ncols		/* The dimensions of the image */
   ));

EXTERN_C_FUNCTION ( int block_fourier2D_real, (
   double **re,			/* The image */
   int istart, int jstart,	/* Starting indices for the array */
   int nrows, int ncols,	/* The dimensions of the image */
   int blocksize		/* The size of the blocks */
   ));

#endif
