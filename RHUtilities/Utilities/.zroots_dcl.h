#ifndef _zroots_c_dcl_
#define _zroots_c_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities/complex_dcl.h>
/* */

EXTERN_C_FUNCTION ( void complex_roots_base1, (
        fcomplex a[],
        int m,
        fcomplex roots[],
        int polish));

EXTERN_C_FUNCTION ( void complex_roots_real_coefficients_base1, (
        double *a,
        int m,
        fcomplex roots[],
        int polish));

#endif
