#ifndef _ffileopen_c_dcl_
#define _ffileopen_c_dcl_
#include "Utilities/cvar.h"


/* */
#include <stdio.h>
/* */

EXTERN_C_FUNCTION (FILE *m4_fopen, (
   const char *infilename
   ));

EXTERN_C_FUNCTION (FILE *filter_fopen, (
   const char *infilename
   ));

#endif
