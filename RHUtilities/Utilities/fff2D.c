/* @(#)fourier2D.c	1.3 07 Nov 1994 */

/* Two dimensional fourier transform routines */

#include <Utilities/utilities_c.h>
#include <Utilities/cvar.h>
#include <Utilities/fourier_dcl.h>

FUNCTION_DEF ( int is_2_power, (
        int n))
   {
   /* Determines whether the number n is a power of 2 */
   while (n > 1 && n % 2 == 0) n /= 2;

   /* If we reach here, then it is a power of 2 */
   if (n == 1) return 1;
   else return 0;
   }

FUNCTION_DEF ( int fourier2D_base1, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int nrows, int ncols,	/* The dimensions of the image */
   int isign)			/* The direction of the transform */
   )
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (1, nrows) x (1, ncols) */
   int i, j;
  
   /* Declare an array to hold the Fourier data */
   double *arow;
   double *acol;

   /* Check that the dimensions of the image are powers of 2 */
   if (! is_2_power (nrows) || ! is_2_power (ncols))
      {
      error_message ("fourier2D: Image size not power of 2");
      return 0;
      }

   /* Allocate temporaries */
   arow = VECTOR (1, 2*ncols, double);
   acol = VECTOR (1, 2*nrows, double);

   /* Now, take the transform one row at a time */
   for (i=1; i<=nrows; i++)
      {
      /* Take the transform of the i-th row */
      
      /* First, repack the data */
      for (j=1; j<=ncols; j++)
         {
         arow[2*j-1] = re[i][j];
         arow[2*j]   = im[i][j];
         }

      /* Take the transform */
      four1 (arow, ncols, isign);

      /* Put it in the array */
      for (j=1; j<=ncols; j++)
	 {
	 re[i][j] = arow[2*j-1];
	 im[i][j] = arow[2*j];
	 }
      }

   /* Now take the transforms in the opposite direction */
   for (j=1; j<=ncols; j++)
      {
      /* Take the transform of the columns */

      /* First, build a column */
      for (i=1; i<=nrows; i++)
	 {
	 acol[2*i-1] = re[i][j];
	 acol[2*i]   = im[i][j];
	 }

      /* Take the transform */
      four1 (acol, nrows, isign);
	
      /* Put it back in the arrays */
      for (i=1; i<=nrows; i++)
	 {
	 re[i][j] = acol[2*i-1];
	 im[i][j] = acol[2*i];
	 }
      }

   /* Now, free the data */
   FREE (arow, 1);
   FREE (acol, 1);

   /* Return success */
   return 1;
   }

FUNCTION_DEF ( int block_fourier2D_base1, (
   double **re,			/* The real part of the image */
   double **im,			/* The imaginary part of the image */
   int nrows, int ncols,	/* The dimensions of the image */
   int blocksize,		/* The size of the blocks */
   int isign)			/* The direction of the transform */
   )
   {
   /* Takes a 2D fourier transform of the image */
   /* Image is assumed to be of dimensions (0, nrows-1) x (0, ncols-1) */
   int i, j;	/* Index within each block */
   int k, l;	/* Block counters */
  
   /* Declare an array to hold the Fourier data */
   double *arow;
   double *acol;

   /* Check that the size of the blocks is good */
   if (! is_2_power (blocksize))
      {
      error_message ("fourier2D: Block size (%d) not power of 2", blocksize);
      return 0;
      }

   /* Check that there are an exact number of blocks */
   if (nrows % blocksize != 0)
      {
      error_message ("fourier2D: Block size (%d) does not divide # rows (%d)",
	 blocksize, nrows);
      return 0;
      }
   if (ncols % blocksize != 0)
      {
      error_message ("fourier2D: Block size (%d) does not divide # columns (%d)",
	 blocksize, ncols);
      return 0;
      }

   /* Allocate temporary vectors */
   arow = VECTOR (1, 2*ncols, double);
   acol = VECTOR (1, 2*nrows, double);

   /* Now, take the transform one block at a time */
   for (k=0; k<nrows/blocksize; k++)
      for (l=0; l<ncols/blocksize; l++)
	 {
	 /* Get offset of the block that we are doing */
	 int rowoffset = k*blocksize+1;
	 int coloffset = l*blocksize+1;

         /* Now, carry out the transform one row at a time */
         for (i=rowoffset; i<rowoffset+blocksize; i++)
            {
            /* Take the transform of the i-th row */
      
            /* First, repack the data */
            for (j=0; j<blocksize; j++)
               {
               arow[2*j+1] = re[i][j+coloffset];
               arow[2*j+2] = im[i][j+coloffset];
               }

            /* Take the transform */
            four1 (arow, blocksize, isign);

            /* Put it in the array */
            for (j=0; j<blocksize; j++)
	       {
	       re[i][j+coloffset] = arow[2*j+1];
	       im[i][j+coloffset] = arow[2*j+2];
	       }
            }

         /* Now take the transforms in the opposite direction */
         for (j=coloffset; j<coloffset+blocksize; j++)
            {
            /* Take the transform of the columns */

            /* First, build a column */
            for (i=0; i<blocksize; i++)
	       {
	       acol[2*i+1] = re[i+rowoffset][j];
	       acol[2*i+2] = im[i+rowoffset][j];
	       }

            /* Take the transform */
            four1 (acol, blocksize, isign);
	
            /* Put it back in the arrays */
            for (i=0; i<blocksize; i++)
	       {
	       re[i+rowoffset][j] = acol[2*i+1];
	       im[i+rowoffset][j] = acol[2*i+2];
	       }
            }
	 }

   /* Now, free the data */
   FREE (arow, 1);
   FREE (acol, 1);

   /* Return success */
   return 1;
   }
