/* @(#)quaternion.c	1.9 07 Nov 1994 */
/* Routines to support the quaternion representation of rotations */

#include <Utilities/alloc.h>
#include <math.h>
#include <Utilities/cvar.h>

FUNCTION_DEF (double sinc, (double x))
   {
   /* Robust way of computing the sinc function */
   if (1.0 - x*x/6.0 == 1.0) return 1.0;
   else return sin(x) / x;
   }

FUNCTION_DEF ( double **quaternion_to_matrix, (
   double *q,		/* The quaternion */
   double **R)		/* The rotation matrix */
   )
   {
   /* Given a normalized quaternion,
    *  computes the rotation matrix it represents 
    */

   /* Get the components of the quaternion */
   double 	c = q[0], 
		x = q[1], 
		y = q[2], 
		z = q[3];/* Components of the quaternion*/

   /* Now compute the terms needed in the matrix representation */
   double xx = x * x;
   double yy = y * y;
   double zz = z * z;
   double cc = c * c;
   double xy = x * y;
   double xz = x * z;
   double yz = y * z;
   double cx = c * x;
   double cy = c * y;
   double cz = c * z;

   /* Fill out the rotation matrix */
   R[0][0] = cc + xx - yy - zz;
   R[0][1] = 2.0 * (xy - cz);
   R[0][2] = 2.0 * (xz + cy);

   R[1][0] = 2.0 * (xy + cz);
   R[1][1] = cc + yy - xx - zz;
   R[1][2] = 2.0 * (yz - cx);

   R[2][0] = 2.0 * (xz - cy);
   R[2][1] = 2.0 * (yz + cx);
   R[2][2] = cc + zz - xx - yy;

   /* Return the rotation matrix */
   return R;
   }

FUNCTION_DEF ( double **VecToMatrix, (
   double *V,	/* Input vector, representing a rotation */
   double **R	/* Output rotation matrix */
   ))
   {
   /* Inputs a vector representing a rotation, and outputs a matrix
    * representation of the same rotation.
    *
    * The theory of this is based on quaternions.  
    *
    * Given a rotation through angle theta about an axis represented by
    * a unit vector (a, b, c), we represent this as a quaternion
    * q = (cos(theta/2), a.sin(theta/2), b.sin(theta/2), c.sin(theta/2)).
    * Now a point p = (0, x, y, z) is transformed by this rotation to
    * the point q.p.q*.
    *
    * Now, given the input vector V, this represents a rotation through 
    * an angle |V| about the unit axis vector V/|V|.  We then use the
    * above quarternion formulation to compute the effect of the rotation
    * on the three coordinate axes, and hence get the rotation matrix as
    * computed in this routine.
    *
    * Robustified for the case of 0 rotation: RIH, Sept 5, 2002
    */

   /* Compute the length of the input vector */
   double theta = sqrt (V[0] * V[0] + V[1] * V[1] + V[2] * V[2]);
   double th2 = theta / 2.0;

   /* Compute the components of the quaternion (c, x, y, z) */
   double q[4];
   double fac = 0.5 * sinc(th2);
   q[0] = cos (th2);
   q[1] = fac * V[0];
   q[2] = fac * V[1];
   q[3] = fac * V[2];

   /* Now return the rotation matrix */
   return quaternion_to_matrix (q, R);
   }


#ifdef TEST

FUNCTION_DEF ( int getinput, (
        double *V))
   {
   /* Returns the number of arguments got, or EOF */
   int n;		/* Number of arguments read */

   while (1)
      {
      /* Break out of this loop on EOF or successful read */

      /* Prompt */
      printf ("Enter vector (or ^D to quit) : ");
      fflush (stdout);

      /* Read three arguments, then go to the end of the line */
      n = scanf (" %lf %lf %lf", &V[0], &V[1], &V[2]);
      if (n != EOF)
         while ( getchar() != '\n');

      if (n == EOF) return (EOF);	/* End of file */
      else if (n == 3) return (3);	/* Successful read */
      else if (n < 3)			/* Bad read, try again */
         printf ("Bad input skipped\n");
      }
   }
	 

FUNCTION_DEF ( int quaternion_main, (
        int argc,
        char *argv[]))
   {
   /* Test the program */
   double **R = MATRIX (0, 2, 0, 2, double);	/* The rotation matrix */
   double *V = VECTOR (0, 2, double);		/* Rotation vector */

   /* Get into a loop of reading and computing */
   while (1)
      {
      /* Get the input */
      if (getinput(V) == EOF) break;

      /* Now do the computation, and print it */
      fprint_matrix ("%11.3f", stdout, VecToMatrix (V, R), 0, 2, 0, 2);
      }

   FREE (R, 0);
   FREE (V, 0); 
   }

#endif

	
	 


	 

