#ifndef _chisq_c_dcl_
#define _chisq_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (float gammp, (float a, float x));

EXTERN_C_FUNCTION (float gammq, (float a, float x));

EXTERN_C_FUNCTION (float rherf, (float x));

EXTERN_C_FUNCTION (float rherfc, (float x));

EXTERN_C_FUNCTION (float rherfcc, (float x));

EXTERN_C_FUNCTION (float chisq , (double x, int n));

EXTERN_C_FUNCTION (float chisq_lower_tail , (double x, int n));

EXTERN_C_FUNCTION (float chisq_upper_tail, (double x, int n));

EXTERN_C_FUNCTION (float invchisq, (double x, int n));

#endif
