#include <math.h>
#include <Utilities/rh_util.h>
#include <Utilities/cvar.h>
/* SFILE_BEGIN */
#include <Utilities/complex_dcl.h>
/* SFILE_END */
#include <Utilities/error_message_dcl.h>

#define EPSS 6.e-16
#define MAXIT 100

FUNCTION_DEF ( static void laguerre, 
	(
        fcomplex a[],
        int n,
        fcomplex *x,
        double eps,
        int polish
	))
   {
	/* Iterative search for a root */

	/* Previous increment -- to be used as stopping criterion */
	double dxold = Cabs(*x);

	int iter;
	for (iter=1; iter<=MAXIT; iter++) 
	   {
	   int j;
	   double abx = Cabs(*x);

	   /* Compute the polynomial value, plus first two derivatives at x 
	    *   b   = polynomial value
    	    *   d   = first derivative
	    *   f   = second derivative
	    */

	   fcomplex b = a[n];
	   fcomplex d = Complex(0.0, 0.0);
 	   fcomplex f = Complex(0.0, 0.0);
	   double err = Cabs(b);

	   for (j=n-1; j>=0; j--) 
	      {
	      f = Cadd(Cmul(*x, f), d);		/* f = x*f + d */
	      d = Cadd(Cmul(*x, d), b);		/* d = x*d + b */
	      b = Cadd(Cmul(*x, b), a[j]);	/* b = x*b + a[j] */
	      err = Cabs(b) + abx*err;
	      }

	   /* If close to solution, then stop */
	   if (Cabs(b) <= err*EPSS) return;

	   /* Compute the increment to be subtracted from x,
	    * using Laguerre's formula */
	      {
	      fcomplex dx;
	      fcomplex G = Cdiv(d, b);
	      fcomplex G2 = Cmul(G, G);
	      fcomplex H = Csub(G2, RCmul(2.0, Cdiv(f, b)));
	
	      /* sqrt((n-1) * (n*H - G^2)) */
	      fcomplex sq = 
	          Csqrt(RCmul((double) (n-1), Csub(RCmul((double) n, H), G2)));

	      /* Denominator of Laguerre with opposite signs */
	      fcomplex gp = Cadd(G, sq);
	      fcomplex gm = Csub(G, sq);

	      /* Take the largest denominator */
	      if (Cabs(gp) < Cabs(gm)) gp = gm;

	      /* Compute the increment = n / denom */
	      dx = Cdiv(Complex((double) n, 0.0), gp);

	      /* Subtract increment from x and return if no change */
	         {
	         fcomplex x1 = Csub(*x, dx);
	         if (x->r == x1.r && x->i == x1.i) return;
	         *x = x1;
	         }

	      /* Return after 6 iterations, if increment is greater than last */
	         {
	         double cdx = Cabs(dx);
	         if (iter > 6 && cdx >= dxold) return;

	         /* Store the absolute value of increment */
	         dxold = cdx;

	         /* Exit if the increment is too small */
	         if (!polish)
		      if (cdx <= eps*Cabs(*x)) return;
	         }
	      }
	   }

	/* We should have returned from inside the loop */
	fatal_error_message("Too many iterations in routine LAGUER");
   }

#undef EPSS
#undef MAXIT

#define EPS 2.0e-6

FUNCTION_DEF ( void complex_roots_base1, (
        fcomplex a[],
        int m,
        fcomplex roots[],
        int polish))
   {
   int j;

   /* Take a copy of the input */ 
   fcomplex *ad = VECTOR (0, m, fcomplex);
   for (j=0; j<=m; j++) 
      ad[j] = a[j];

   /* Now, find the roots one by one */
   for (j=m; j>=1; j--) 
      {
      /* Find a root, starting from a guess of 0 */
      fcomplex x = Complex(0.0, 0.0);
      laguerre(ad, j, &x, EPS, 0);

      /* Roots very close to being real are made real */
      if (fabs(x.i) <= (2.0*EPS*fabs(x.r))) 
	 x.i = 0.0;

      /* Store the root */
      roots[j] = x;

      /* Divide by the root (deflate) */
	 {
	 int jj;
         fcomplex b = ad[j];
         for (jj=j-1; jj>=0; jj--) 
	    {
	    fcomplex c = ad[jj];
	    ad[jj] = b;
	    b = Cadd(Cmul(x, b), c);
	    }
	 }
      }

   /* Polish the roots one by one */
   if (polish)
      for (j=1; j<=m; j++)
	 laguerre(a, m, &roots[j], EPS, 1);

#ifdef SORT_ROOTS
   /* Sort the roots - inefficient sorting routine */
   for (j=2; j<=m; j++) 
      {
      int i;
      fcomplex x = roots[j];
      for (i=j-1; i>=1; i--) 
	 {
	 if (roots[i].r <= x.r) break;
	 roots[i+1] = roots[i];
	 }
      roots[i+1] = x;
      }
#endif

   /* Free the temporary storage */
   FREE (ad, 0);
   }

FUNCTION_DEF ( void complex_roots_real_coefficients_base1, (
        double *a,
        int m,
        fcomplex roots[],
        int polish))
   {
   /* Roots of a real polynomial.  Some day modify this to a native real root
    * routine */
   int j;

   /* Take a copy of the input as a complex vector */
   fcomplex *ad = VECTOR (0, m, fcomplex);
   for (j=0; j<=m; j++) 
      ad[j] = Complex (a[j], 0.0);

   /* Now, run the complex routine */
   complex_roots_base1 (ad, m, roots, polish);

   FREE (ad, 0);
   }

#undef EPS


