#ifndef _table_c_dcl_
#define _table_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( rhBOOLEAN tbl_insert, (
    DATA data,		/* Data to be entered in the table */
    TABLE **table,	/* Address of pointer to the table */
    FUNCTION_DECL (int (*compare), (DATA, DATA))
			/* Function for comparing data */
   ));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_findentry, (
        DATA data,
        TABLE **ptable,
        int (*compare)(DATA, DATA)));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_nextentry, (
        TABLEENTRY *anentry));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_preventry, (
        TABLEENTRY *anentry));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_firstentry, (
        TABLE *atable));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_lastentry, (
        TABLE *atable));

EXTERN_C_FUNCTION ( DATA tbl_first, (
        TABLE *atable));

EXTERN_C_FUNCTION ( DATA tbl_last, (
        TABLE *atable));

EXTERN_C_FUNCTION ( DATA tbl_find, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_nextpreorder, (
        TABLEENTRY *entry));

EXTERN_C_FUNCTION ( int tbl_balance, (
        TABLE **F));

EXTERN_C_FUNCTION ( int tbl_depth, (
        TABLE *table));

EXTERN_C_FUNCTION ( rhBOOLEAN tbl_delete, (
        DATA data,
        TABLE **table,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_firstentryafter, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( TABLEENTRY *tbl_lastentrybefore, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( DATA tbl_firstafter, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( DATA tbl_lastbefore, (
        DATA data,
        TABLE **ptable,
        FUNCTION_DECL (int (*compare), (DATA, DATA))
	));

EXTERN_C_FUNCTION ( int tbl_check, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (int (*compare), (DATA, DATA)),
   			/* Function used for comparison */
   FUNCTION_DECL (char *(*idfn), (TABLE *))
			/* Function used to identify a node */
   ));

EXTERN_C_FUNCTION ( int tbl_dumppreorder, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (char *(*idfn), (TABLE *))
			/* Function used to identify a node */
   ));

EXTERN_C_FUNCTION ( int tbl_dumpinorder, (
   FILE *afile,		/* File for output */
   TABLE *table,	/* The table to be checked */
   FUNCTION_DECL (char *(*idfn), (TABLE *))	
			/* Function used to identify a node */
   ));

EXTERN_C_FUNCTION ( int tbl_freetable, (
        TABLE *atable));

#endif
