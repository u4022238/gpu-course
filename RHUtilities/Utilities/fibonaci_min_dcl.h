#ifndef _fibonaci_min_c_dcl_
#define _fibonaci_min_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( double fibonnacci_min, (
   FUNCTION_DECL ( double (*f), (double)), /* The function to be minimized */
   double low, double high,	/* The bounds in which the minimum lies */
   FUNCTION_DECL (int (*time_to_stop), 	/* The maximum acceptable error */
	(double, double, double, double, double, double))
   ));

#endif
