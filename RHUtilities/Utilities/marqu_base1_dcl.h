#ifndef _marqu_base1_c_dcl_
#define _marqu_base1_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION (double marquardt_base1, (Marquardt_info *mar));

EXTERN_C_FUNCTION (void marquardt_debug_base1, (
   double **alpha, 
   double *beta));

#endif
