LIBSOURCES =								\
	bail_out.c array_utils.c fibonaci_min.c fileopen.c gaussj.c	\
	io.c list.c marqu.c mod.c random.c solve2.c svalue.c table.c	\
	error_message.c concat.c zztchar.c ffileopen.c pseudo.c		\
	symmetric.c showtime.c QRdecomp.c choleski.c 			\
	determinant.c fourier.c minsolve.c svd.c svd3x3.c fourier2D.c 	\
	fourier_real.c jacobi.c chisq.c complex.c			\
	polynomials.c zroots.c eigenvalues.c svalue3.c			\
	array_utils_0.c base1_interface.c marqu_base1.c try_crash.c	\
	qsort_cmp.c bashfopen.c	quaternion.c

# svalue2.c	-- Replaced by svalue3.c

