/* @(#)list.c	3.11 11/26/96 */

/*
 * Module Purpose: Utility routines for operating upon lists.
 *
 * Creation Date: 5/7/87
 *
 * Class 3: General Electric Proprietary Data
 * Copyright (C) 1987 General Electric Company
 *
 */

#include <Utilities/cvar.h>
#include <Utilities/rh_util.h>
#include <Utilities/error_message_dcl.h>

/* Include in the .s file SFILE_BEGIN */
#include <Utilities/list_defs.h>
/* SFILE_END */

#define next lst_next
#define prev lst_prev
#define lastentry lst_lastentry
#define firstentry lst_firstentry

#define LST_FASTALLOC
#ifdef LST_FASTALLOC
/*>> Facilities for allocating and freeing list entries <<*/
#define INITSIZE 1024
static unsigned num_freeentries = 0;  /* Index of the first available entry */
static LISTENTRY **addresslist = NULL;/* List of free entries */
static unsigned addresslist_size;    /* The size of the list of free entries */

/* Keeps count of allocated lists */
static unsigned lst_total_list_nodes_allocated = 0;	

static int Free_List_Marker = 1;	/* Set to 0 to turn off checking */

FUNCTION_DEF (LISTENTRY *lst_newentry, ())
   {
   /* Provides a new list entry from a list */
   /* See if the entrylist has been initialized */
   if (num_freeentries == 0)
      {
      /* Need more entries */
      int i;

      /* Allocate a bunch of entries */
      LISTENTRY *newblock = ARRAY (INITSIZE, LISTENTRY);

      /* Count the number of list nodes  allocated */
      lst_total_list_nodes_allocated += INITSIZE;

      /* Check that the address list has been initialized */
      if (addresslist == NULL)
	 {
         addresslist_size = INITSIZE;
	 addresslist = ARRAY (INITSIZE, LISTENTRY *);
	 }

      /* Record the addresses of the free entries */
      for (i=0; i<INITSIZE; i++)
	 addresslist[i] = newblock+i;

      /* Point num_freeentries to the top of the stack */
      num_freeentries = INITSIZE;
      }

   /* Now return the required block */
   return (addresslist[--num_freeentries]);
   }

FUNCTION_DEF (static void lst_freeentry, (LISTENTRY *anentry))
   {
   /* Returns a list entry to the free list */
   /* First make sure that there is enough room to hold the free list */
   if (num_freeentries >= addresslist_size)
      {
      addresslist_size *= 2;
      addresslist = (LISTENTRY **) realloc (addresslist, 
	 addresslist_size * sizeof(*addresslist));
      if (addresslist == NULL) ABORT;
      }

   /* Check that this has not been freed before */
   if (anentry->data == (DATA)&Free_List_Marker && Free_List_Marker)
      {
      error_message ("Free list node being freed again");
      exit (1);
      }

   /* Make sure that the data pointer is pointing to free marker */
   anentry->data = (DATA) &Free_List_Marker;

   /* Now return the entry */
   addresslist[num_freeentries++] = anentry;
   }
#else
#define lst_newentry() 		NEW(LISTENTRY)
#define lst_freeentry(x)	free(x)
#endif

/*>> List utilities follow <<*/

FUNCTION_DEF ( LISTENTRY *lst_insertafter, (
   /* Inserts a piece of data after the indicated position */
   DATA dat,		/* The data to insert */
   LISTENTRY *aposition/* Position to insert the list */
   ))
   {
   LISTENTRY *temp;
   temp = lst_newentry();
   temp->data = dat;
   temp->next = aposition->next;
   temp->prev = aposition;
   aposition->next = temp;
   temp->next->prev = temp;
   return (temp);
   }

FUNCTION_DEF ( LIST *lst_init, ())
   /* Initialises a list */
   {
   LIST *temp;
   temp = lst_newentry();
   temp->next = temp->prev = temp;
   temp->data = NULL;
   return (temp);
   }


FUNCTION_DEF ( void lst_joinafter, (
   LIST *lst,		/* The list to be grafted in */
   LISTENTRY *position	/* Where it should be placed */
   ))

   /* Joins the given list in at the given position.
    * It leaves lst as an empty list */

   {
   register LISTENTRY *subseq=position->next;	/* Position after the insert */
   register LISTENTRY *last = lastentry(lst);
   register LISTENTRY *first = firstentry(lst);
   if (lst_isempty(lst)) return;
   last->next = subseq;
   first->prev = position;
   position->next = first;
   subseq->prev = last;
   lst->next = lst->prev = lst;
   }
 

FUNCTION_DEF ( LIST *lst_join, (
        LIST *l1,
        LIST *l2))
   {
   /* Join list 1 into the start of list 2 */
   lst_joinafter (l1, firstentry(l2)->prev);
   return (l2);
   }
   
FUNCTION_DEF ( void lst_deleteentry, (
        LISTENTRY *entry))
   /* Deletes an entry from a list */
   {
   entry->next->prev = entry->prev;
   entry->prev->next = entry->next;
   lst_freeentry (entry);
   }

FUNCTION_DEF ( DATA lst_delete, (
        DATA dat,
        LIST *lst))
   /* Deletes the data from the list.  
    * Returns NULL if the entry is not found.  Otherwise
    * returns the data.
    * This is inefficient to use.
    * Try to use lst_deleteentry instead.
    */
   {
   lst_forallentries (d, lst)
      if (d->data == dat)
         {
	 lst_deleteentry(d);
	 return (dat);
	 }
   lst_endall;
   return (NULL);
   }
 

FUNCTION_DEF ( DATA lst_pop, (
        LIST *que))
   /* Dequeues an item from the queue and returns it */
   {
   LISTENTRY *first;
   DATA dat;
   if (que == que->next) return (NULL);
   first = firstentry(que);
   dat = first->data;
   lst_deleteentry (first);
   return (dat);
   }

FUNCTION_DEF ( DATA lst_dequeue, (
        LIST *que))
   /* Dequeues an item from the queue and returns it */
   {
   LISTENTRY *first;
   DATA dat;
   if (que == que->next) return (NULL);
   first = firstentry(que);
   dat = first->data;
   lst_deleteentry (first);
   return (dat);
   }

FUNCTION_DEF ( void lst_free, (
        LIST *alist))
   /* Frees the storage occupied by the list.
    * Does not free the data in the list.
    */
   {
   lst_forallentries (entry, alist)
      lst_freeentry (entry);
   lst_endall;
   lst_freeentry (alist);
   }


FUNCTION_DEF ( int lst_length, (
        LIST *alist))
   /* Calculates the length of a list */
   {
   register int length = 0;
   lst_forallentries (entry, alist)
      length++;
   lst_endall;
   return (length);
   } 

/*>> A bubblesort utility for lists <<*/
FUNCTION_DEF ( void lst_bubblesort, (
   LIST *alist,                 	/* A list to be sorted */
   FUNCTION_DECL (int (*cmp), (DATA, DATA))/* Function for comparing elements */
   ))
   {
   /* Move along the list until we get to the end */
   /* At the end of each step, all nodes up to cursor will be sorted */
   lst_forallentries (cursor, alist)
      {  
      LISTENTRY *c1, *c2;       /* Two runners */

      /* Save the present data */
      DATA temp = cursor->data;

      /* Move backwards comparing */
      c1 = lst_preventry (cursor);
      c2 = cursor;
      while ( c1 != alist && (*cmp)(c1->data, temp) > 0)
         {
         /* If the data is out of order, then swap it */
         c2->data = c1->data;
         c2 = c1;
         c1 = lst_preventry (c1);
         }
 
      /* Now restore the current data */
      c2->data = temp;
      } lst_endall;
    }


FUNCTION_DEF ( void lst_check, (
        LIST *alist))
   /* Calculates the length of a list */
   {
   /* Checks out the list.  Used for debugging */
   rhBOOLEAN errors = FALSE;

   /* Enable error and informative messages */
   rhBOOLEAN reset_error_messages = enable_error_messages();
   rhBOOLEAN reset_informative_messages = enable_informative_messages();

   /* Previous entry to the one being seen */
   LISTENTRY *prev = alist;

   /* For keeping track of the length */
   int length = 0;

   /* Check out the header node */

   /* Check that the forward pointer from the header is not null */
   if (alist->lst_next == NULL)
      {
      error_message ("Error in list %08x :", alist);
      error_message ("   NULL forward pointer from header node");
      errors = TRUE;
      }

   /* Check that the backward pointer from the header is not null */
   if (alist->lst_prev == NULL)
      {
      error_message ("Error in list %08x :", alist);
      error_message ("   NULL backwards pointer from header node");
      errors = TRUE;
      }

   /* Stop here if there have been errors */
   if (errors) return;

   /* Now, run through all nodes of the list */
   lst_forallentries (entry, alist)
      {
      length ++;

      /* Check the back pointer */
      if (lst_preventry(entry) != prev)
	 {
	 error_message ("Error in list %08x", alist);
	 error_message (
	    "   At depth %d : Back pointer not to previous entry", length);
	 error_message ( "   Actual : %08x, Expected : %08x", 
		lst_preventry(entry), prev);
	 errors = TRUE;
	 }

      /* Check the forward pointer */
      if (lst_nextentry(entry) == NULL)
	 {
	 error_message ("Error in list %08x", alist);
	 error_message (
	    "   At depth %d : NULL forward pointer", length);
	 errors = TRUE;
	 }

      /* Stop here if errors */
      if (errors) return;

      /* Get ready to go to the next node */
      prev = entry;
      } lst_endall;

   /* Check the back pointer from the header node */
   if (lst_preventry(alist) != prev)
      {
      error_message ("Error in list %08x", alist);
      error_message (
         "   At header node : Back pointer not to previous entry");
      error_message ( "   Actual : %08x, Expected : %08x", 
	     lst_preventry(alist), prev);
      errors = TRUE;
      }

   /* If no errors, then say so */
   if (! errors)
      {
      /* Print to stdout in case informative messages are off */
      informative_message ("List is OK : Length = %d\n", length);
      }

   /* Reset the error messages, etc */
   if (reset_error_messages) enable_error_messages();
   if (reset_informative_messages) enable_informative_messages();
   } 

FUNCTION_DEF (int lst_usage_summary, ())
   {
   /* Gives a count of the number of list allocated and in use */

   /* Enable error and informative messages */
   rhBOOLEAN reset_informative_messages = enable_informative_messages();

   /* Now give the message */
   informative_message ("list nodes allocated = %d", 
	lst_total_list_nodes_allocated);
   informative_message ("Free list size       = %d", num_freeentries);
   informative_message ("Number still in use  = %d", 
	lst_total_list_nodes_allocated - num_freeentries);

   /* Reset the error messages, etc */
   if (reset_informative_messages) enable_informative_messages();

   /* Return the number of entries in use */
   return (lst_total_list_nodes_allocated - num_freeentries);
   }

FUNCTION_DEF (int lst_usage, ())
   {
   /* Gives a count of the number of list allocated and in use */

   /* Return the number of entries in use */
   return (lst_total_list_nodes_allocated - num_freeentries);
   }

