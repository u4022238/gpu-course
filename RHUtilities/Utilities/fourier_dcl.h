#ifndef _fourier_c_dcl_
#define _fourier_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( void four1, (
        double *data,
	     int istart,		/* Start index for four1 */
        int nn,
        int sign));

EXTERN_C_FUNCTION ( void fourier_filter_base1, (
   double *signal,	/* The inpute real data */
   int nn,		/* Number of data points (must be power of 2) */
   int numtaps)		/* Highest numbered non-zero Fourier bin */
   );

EXTERN_C_FUNCTION ( void correlation_base1, (
   double *s1, double *s2,	/* Two real input signals */
   double *corr,		/* The correlated signal */
   int nn)			/* Number of data points (must be power of 2) */
   );

#endif
