#ifndef _gaussj_c_dcl_
#define _gaussj_c_dcl_
#include "Utilities/cvar.h"


EXTERN_C_FUNCTION ( int gaussj_check_base1, (
        double **a,
        int n,
        double *b));

EXTERN_C_FUNCTION ( int gaussj_check_base1, (
        double **a,
        int n,
        double *b));

EXTERN_C_FUNCTION ( int gaussj2_check_base1, (
        double **a,
        int n,
  	double **b,
	int m
	));

EXTERN_C_FUNCTION ( void gaussj_base1, (
        double **a,
        int n,
        double *b));

EXTERN_C_FUNCTION ( void gaussj2_base1, (
        double **a,
        int n,
  	double **b,
	int m ));

#endif
