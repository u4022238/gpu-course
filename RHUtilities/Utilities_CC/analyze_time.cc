#include <stdio.h>
#include <string.h>
#include <Utilities_CC/utilities_CC.h>
#include <time.h>

//---------------------------------------------------------------
// Constants
static const int Start = 0;
static const int End = 1;

#if 0
class TimingEvent
   {
   public: 
      char *name_;
      int depth_;
      long numcalls_;
      double event_time_;
      double total_time_;

      // Constructors
      TimingEvent (char *name)
         {
         depth_ = 0;
         numcalls_ = 0;
	      name_ = COPY(name);
         event_time_ = 0.0;
         total_time_ = 0.0;
         }

      ~TimingEvent ()
         {
         if (name_) free (name_);
         }
   };

//  Define a table of words
#define Tbl_TYPE TimeTable
#define Tbl_DATA class TimingEvent *
#define Tbl_KEY  char *
#include "Table.h"

#endif

// Example of use of tables

#include <stdio.h>

int scmp (TimingEvent *a, TimingEvent *b)
   {
   // Compare two wordentries
   char *wa = a->name_;
   char *wb = b->name_;
   return strcmp (wa, wb);
   }

int sfind (char *wa, TimingEvent *b)
   {
   // Compare a char * with a TimingEvent
   char *wb = b->name_;
   return strcmp (wa, wb);
   }

//---------------------------------------------------------------

int parse_time_string (
      FILE *infile, 
	   double &ttime, 
	   int  &start_or_end, 
	   char *&context
	   )
   {
   // Parses a time string and gives an integer
   double time;
   static char start_end_str[80];
   static char context_str[80];

   // printf ("Scanning\n");

   // Scan the string
   int n = fscanf (infile, "%lf %s", &time, start_end_str);
   if (n == EOF) return EOF;

   // Now, scan to end of line
   char *cursor = context_str;
   bool started = false;
   while(1)
      {
      // Get the next character
      int ch = getc(infile);

      // After leading blanks, transfer to context_str
      if (ch != ' ') started = true;
      if (started) *(cursor++) = ch;

      // Trim from the end
      if (ch == '\n') 
         {
         // Change the EOL to a null and go back to previous character
         *(--cursor) = '\0';
         cursor--;

         // Now delete trailing blanks
         while ((*cursor == ' ' || *cursor == 13) && cursor != context_str )
            *(cursor--) = '\0';

         // Finished parsing string
         break;
         }
      }

   // Successful return
   ttime = time;
   start_or_end = (strcmp ("Start", start_end_str) == 0) ? Start : End;
   context = context_str;
   
   return 1;
   }

int main(int argc, char *argv[])
   {
   // Debugging entry
   try_crash(argc, argv);

   // Check for the arguments
   if (argc != 2)
      {
      error_message ("Usage : analyze_time timefile");
      bail_out (1);
      }

   // Get the input file names
   char *fname1 = argv[1];
   FILE *infile = FOPEN(fname1, "r");

   // Declare a table
   TimeTable ttab(scmp, sfind);

   // Parse and put things in the table
   double ttime; 
   int  start_or_end;
   char *context;
   while (parse_time_string (infile, ttime, start_or_end, context) != EOF)
      {
      // First, see if this is known
      TimingEvent *ev = (TimingEvent *)ttab.find(context);
      
      // Unknown event
      if (!ev)
         {
         // Now create an event struct and insert
         ev = new TimingEvent (context);
         ttab.insert(ev);
         }

      // Now, handle the event

      // Update the depth of the event
      if (start_or_end == Start) 
         {
         // Update the depth
         ev->depth_++;
         ev->numcalls_++;

         // Only record time, first time in
         if (ev->depth_ == 1) ev->event_time_ = ttime;
         }

      else
         {
         // End event
         ev->depth_--;

         // If depth is zero, then store elapsed time
         if (ev->depth_ == 0)
            {
            // Compute the used time
            double elapsed_time = 
                ((double) ttime - ev->event_time_);// /CLOCKS_PER_SEC;

            // In storing the total time, try to be robust to restarting
#if 0
	    if (ev->numcalls_ > 10 && elapsed_time > 1000.0* (ev->total_time_ / ev->numcalls_))
               ev->numcalls_ --;
            else
#endif
               ev->total_time_ += elapsed_time;
            }

         // But if it is less than zero, there is a mistake
         if (ev->depth_ < 0)
            {
            error_message ("Too many ENDS for event \"%s\"\n", context);
            bail_out(1);
            }
         }
      }

   // Now, print out the table
   Tbl_forall (TimingEvent *, ev, ttab)
      {
      double total_time = ev->total_time_;
      long ncalls = ev->numcalls_;
      double avg_time = total_time / ncalls;
      printf ("calls %9d : total %12.3f : avg %12.4e : %s\n", 
           ncalls, total_time, avg_time, ev->name_);
      } Tbl_endall

   return 0;
   }

