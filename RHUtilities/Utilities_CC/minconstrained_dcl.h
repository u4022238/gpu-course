#ifndef _minconstrained_cc_dcl_
#define _minconstrained_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>
/* */

FUNCTION_DECL (rhVector min_eigen_vector_super_slow, (const rhMatrix &A));

FUNCTION_DECL (rhVector min_eigen_vector, (const rhMatrix &A));

FUNCTION_DECL (rhVector minimize_with_constraints_0, (
   const rhMatrix &AA,	// The metric matrix
   const rhMatrix &C	// The constraint matrix
   ));

FUNCTION_DECL (rhVector minimize_with_constraints_new_0, (
   const rhMatrix &A,	// The metric matrix
   const rhMatrix &C,	// The constraint matrix
   int rank_in  = -1 	// Estimated rank
   ));

FUNCTION_DECL (rhVector minimize_with_mod1_constraints, (
        const rhMatrix &A, const rhMatrix &C, int rankC));

#endif
