// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

#include <stdio.h>
#include <stdlib.h>
#include <Utilities_CC/List.h>

class Rint
   {
   private :
      int _n;

   public :

	Rint (int n) { _n = n; };

	void print () { fprintf (stdout, "%d\n", _n); }
   };

void doprint (List numbers)
   {
   for (List_Index j = numbers.first_index(); j.valid(); j++)
      {
      Rint *r = (Rint *) numbers[j];
      r->print();
      }
   }

void doit (List numbers)
   {

   for (int i=0; i<10; i++)
      {
      Rint *newrint = new Rint((int)rh_random());
      numbers.insert_end((rhObject *)newrint);
      }

   }

int main()
   {

   {
   List t;

   fprintf (stderr, "Starting\n");
 
   for (int i=0; i<10; i++)
      {
      Rint *newrint = new Rint(rh_random());
      t.insert_end((rhObject *)newrint);
      }

   // doprint (t);

   }

   fprintf (stderr, "About to try something funny\n");
   delete (List_Entry *) 0;

   return (0);
   }

#ifdef JUNK
main ()
   {
   fprintf (stderr, "About to declare A\n");
   List A;
   fprintf (stderr, "About to declare B\n");
   List B;
   fprintf (stderr, "About to Assign B to A\n");
   B = A;
   fprintf (stderr, "About to finish\n");
   return (0);
   }
#endif
