// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// @(#)Matrix.cc	1.8 April 24, 2000
//

// ============  Error Handler Stuff ===================

void Matrix_standard_error_handler ();
void (* MatrixType::Error_Handler)() = Matrix_standard_error_handler;
void (* VectorType::Error_Handler)() = Matrix_standard_error_handler;

// ======================================================

// We are going to use memcpy to transfer data
#define USE_MEMCPY

// More gentle error handling, passing error codes
MatrixType::ErrorCode MatrixType::error_code;

// 
// The next routines would be a lot faster if we were sure
// that the input and matrix type matched.
//
MatrixType::MatrixType (int idim, int jdim, double *vals)
   {
   // Vector from an array of doubles
   Init(idim, jdim);
   MatrixType &ten = *this;

   // Fill out the values
   int count = 0;
   for2Dindex (i, j, ten)
      ten[i][j] = (realtype) (vals[count++]);
   }

MatrixType::MatrixType (int idim, int jdim, const double *vals)
   {
   // Vector from an array of doubles
   Init(idim, jdim);
   MatrixType &ten = *this;

   // Fill out the values
   int count = 0;
   for2Dindex (i, j, ten)
      ten[i][j] = (realtype) (vals[count++]);
   }

MatrixType::MatrixType (int idim, int jdim, double **vals)
   {
   // Vector from an array of doubles
   Init(idim, jdim);
   MatrixType &ten = *this;

   // Fill out the values
   int i, j;
   for_2Dindex (i, j, ten)
      ten[i][j] = (realtype) (vals[i][j]);
   }

MatrixType MatrixType::submatrix (int ilow, int ihigh, int jlow, int jhigh) const
   {   
   // Takes and returns a submatrix
   const MatrixType &A = *this;
 
   // Get the bounds 
   int idim = ihigh - ilow + 1; 
   int jdim = jhigh - jlow + 1;
 
   // Check the dimensions 
   if (idim <= 0 || jdim <= 0)   
      { 
      error_message ("MatrixType::submatrix : bad matrix dimension (%d x %d)", 
         idim, jdim); 
      Error_Handler ();
      }   

   // Check the dimensions 
   if (ilow < A.ilow() || jlow < A.jlow() ||
       ihigh > A.ihigh() || jhigh > A.jhigh())
      { 
      error_message ("MatrixType::submatrix : out of bounds");
      Error_Handler ();
      }   
 
   // Now make the new matrix 
   MatrixType B (idim, jdim); 
 
   // Fill it out
   int i, j;
   int ioffs = ilow - B.ilow();
   int joffs = jlow - B.jlow();
   for_2Dindex (i, j, B) 
      B[i][j] = A[i+ioffs][j+joffs]; 
 
   // Return the matrix 
   return B;
   }

MatrixType MatrixType::operator - (const MatrixType & m2) const
   {
   // Difference of two matrices
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.idim() || m1.jdim() != m2.jdim())
      {
      error_message ("MatrixType::op(-) : matrices not of same dimension");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   MatrixType prod (m1.idim(), m1.jdim());
   for_2Dindex (i, j, m1)
      prod [i][j] = m1[i][j] - m2[i][j];
  
   // Return the product matrix
   return prod;
   }

MatrixType MatrixType::operator + (const MatrixType & m2) const
   {
   // Sum of two matrices
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.idim() || m1.jdim() != m2.jdim())
      {
      error_message ("MatrixType::op(+) : matrices not of same dimension");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   MatrixType prod (m1.idim(), m1.jdim());
   for_2Dindex (i, j, m1)
      prod [i][j] = m1[i][j] + m2[i][j];
  
   // Return the product matrix
   return prod;
   }

void MatrixType::operator -= (const MatrixType & m2)
   {
   // Subtract a given matrix
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.idim() || m1.jdim() != m2.jdim())
      {
      error_message ("MatrixType::op(-=) : matrices not of same dimension");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   for_2Dindex (i, j, m1)
      m1[i][j] -= m2[i][j];
   }

void MatrixType::operator += (const MatrixType & m2)
   {
   // Add a given matrix
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.idim() || m1.jdim() != m2.jdim())
      {
      error_message ("MatrixType::op(+=) : matrices not of same dimension");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   for_2Dindex (i, j, m1)
      m1[i][j] += m2[i][j];
   }

MatrixType MatrixType::operator | (const MatrixType & m2) const
   {
   // Concatenate two matrices side by side
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.idim())
      {
      error_message (
	"MatrixType::op(| MatrixType) : matrices have different # rows");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.idim(), m1.jdim() + m2.jdim());
   int offset = m1.jdim();

   // Copy the data
   for_2Dindex (i, j, m1)
      m1m2[i][j] = m1[i][j];

   for_2Dindex (i, j, m2)
      m1m2[i][j+offset] = m2[i][j];

   // Return the result
   return m1m2;
   }

MatrixType MatrixType::operator | (const VectorType & m2) const
   {
   // Concatenate two matrices side by side
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.idim() != m2.dim())
      {
      error_message (
	"MatrixType::op(| VectorType) : matrix and vector have different # rows");
      error_message ("   (%d x %d) and (%d x 1)", 
            m1.idim(), m1.jdim(), m2.dim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.idim(), m1.jdim() + 1);
   int offset = m1.jdim();

   // Copy the data
   for_2Dindex (i, j, m1)
      m1m2[i][j] = m1[i][j];

   for_1Dindex (i, m2)
      m1m2[i][offset] = m2[i];

   // Return the result
   return m1m2;
   }

MatrixType MatrixType::operator & (const MatrixType & m2) const
   {
   // Concatenate two matrices one above the other
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.jdim() != m2.jdim())
      {
      error_message (
	"MatrixType::op(& MatrixType) : matrices have different # cols");
      error_message ("   (%d x %d) and (%d x %d)", 
            m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.idim() + m2.idim(), m1.jdim());
   int offset = m1.idim();

   // Copy the data
   for_2Dindex (i, j, m1)
      m1m2[i][j] = m1[i][j];

   for_2Dindex (i, j, m2)
      m1m2[i+offset][j] = m2[i][j];

   // Return the result
   return m1m2;
   }

MatrixType MatrixType::operator & (const VectorType & m2) const
   {
   // Concatenate two matrices side by side
   int i, j;
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.jdim() != m2.dim())
      {
      error_message (
      "MatrixType::op(& VectorType) : matrix and vector have different # cols");
      error_message ("   (%d x %d) and %d", 
            m1.idim(), m1.jdim(), m2.dim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.idim()+1, m1.jdim());
   int offset = m1.idim();

   // Copy the data
   for_2Dindex (i, j, m1)
      m1m2[i][j] = m1[i][j];

   for_1Dindex (j, m2)
      m1m2[offset][j] = m2[j];

   // Return the result
   return m1m2;
   }

#ifdef DOUBLE_ONLY

VectorType MatrixType::operator * (const VectorType & v) const
   {
   // Matrix * vector product
   const MatrixType &m = *this;

   // Test the bounds
   if (m.jdim() != v.dim())
      {
      error_message (
      "matrix-vector muliplication : incompatible dimensions (%d x %d) by %d",
         m.idim(), m.jdim(), v.dim());
      Error_Handler (); 
      }

   // Carry out the product
   VectorType prod (m.idim());
   matrix_vector_prod_0 (m, v, m.idim(), m.jdim(), prod);
  
   // Return the product matrix
   return prod;
   }

VectorType MatrixType::operator * (const double *v) const
   {
   // Matrix * vector product when v is just a double *
   const MatrixType &m = *this;

   // Carry out the product
   VectorType prod (m.idim());
   matrix_vector_prod_0 (m, (double *)v, m.idim(), m.jdim(), prod);
  
   // Return the product matrix
   return prod;
   }

MatrixType MatrixType::operator * (const MatrixType & m2) const
   {
   // Product of two matrices
   const MatrixType &m1 = *this;

   // Test the bounds
   if (m1.jdim() != m2.idim())
      {
      error_message (
         "matrix x matrix : incompatible dimensions, (%dx%d) by (%dx%d)",
         m1.idim(), m1.jdim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   MatrixType prod (m1.idim(), m2.jdim());
   matrix_prod_0 (m1, m2, m1.idim(), m2.idim(), m2.jdim(), prod);
  
   // Return the product matrix
   return prod;
   }

MatrixType MatrixType::inverse () const
   {
   // Takes the inverse of a matrix
 
   // Get the name of the matrix
   const MatrixType &m = *this;

   // First, see that the matrix is square
   if (m.idim() != m.jdim())
      {
      error_message ("MatrixType::inverse : Matrix not square: (%dx%d)",
            m.idim(), m.jdim());
      Error_Handler ();
      }

   // Carry out the inversion operation
   MatrixType inv (m.idim(), m.jdim());
   matrix_inverse_0 (m, inv, m.idim());

   // Return the matrix
   return inv;
   }

MatrixType MatrixType::pseudo_inverse (double thresh) const
   {
   // Takes the pseudo-inverse of a square matrix
   int i, j;
 
   // Get the name of the matrix
   const MatrixType &m = *this;

   // First, see that the matrix is square
   if (m.idim() != m.jdim())
      {
      error_message ("MatrixType::inverse : Matrix not square (%d x %d)",
            m.idim(), m.jdim());
      Error_Handler ();
      }

   MatrixType U, V;
   VectorType D;
   svd (m, U, D, V);

   // Now invert the singular values and put in a diagonal matrix
   double dmin = D[0] * thresh;
   for (int i=0; i<D.dim(); i++)
       {
       if (D[i] > dmin)
          D[i] = 1.0 / D[i];
       else
          D[i] = 0.0;
       }
      
   // Multiply the columns of V
   for_2Dindex (i, j, V)
      V[i][j] *= D[j];

   // Multiply out the result
   MatrixType inv = V * U.transpose();
   
   // Return it
   return inv;
   }

MatrixType MatrixType::pseudo_inverse (int rank) const
   {
   // Takes the pseudo-inverse of a square matrix
   int i, j;
 
   // Get the name of the matrix
   const MatrixType &m = *this;

   // First, see that the matrix is square
   if (m.idim() != m.jdim())
      {
      error_message ("MatrixType::pseudo_inverse : Matrix not square (%d x %d",
         m.idim(), m.jdim());
      Error_Handler ();
      }

   if (rank > m.jdim())
      {
      error_message ("MatrixType::pseudo_inverse: rank %d exceeds dimension %d",
            rank, m.jdim());
      Error_Handler ();
      }

   MatrixType U, V;
   VectorType D;
   svd (m, U, D, V);

   // Now invert the singular values and put in a diagonal matrix
   for (int i=0; i<rank; i++)
       D[i] = 1.0 / D[i];
      
   for (int i=rank; i<D.dim(); i++)
       D[i] = 0.0;

   // Multiply the columns of V
   for_2Dindex (i, j, V)
      V[i][j] *= D[j];

   // Multiply out the result
   MatrixType inv = V * U.transpose();
   
   // Return it
   return inv;
   }

MatrixType MatrixType::inverse_or_nearinverse () const
   {
   // Takes the inverse of a matrix
 
   // Get the name of the matrix
   const MatrixType &m = *this;

   // First, see that the matrix is square
   if (m.idim() != m.jdim())
      {
      error_message ("MatrixType::inverse : Matrix not square (%d x %d)",
         m.idim(), m.jdim());
      Error_Handler ();
      }

   // Carry out the inversion operation
   MatrixType inv (m.idim(), m.jdim());
   if (matrix_inverse_0 (m, inv, m.idim()))
      return inv;

   //--------------------------------------------------
   // If it did not succeed, then we need to fix try the pseudo-inverse
   MatrixType U, V;
   VectorType D;
   svd (m, U, D, V);

   // Get an approximate identity
   MatrixType II (m.idim(), m.idim());
   II.clear(0.0);
   
   // Now invert the singular values and put in a diagonal matrix
   double limit = D[0] * 1.0e-8;
   int i;
   for_1Dindex (i, D)
      if (D[i] > limit)
	 II[i][i] = 1.0 / D[i];

   // Multiply out the result
   inv = V * II * U.transpose();
   
   // Return it
   return inv;
   }

MatrixType MatrixType::adjoint () const
   {
   // Takes the cofactor matrix of a 3x3 matrix

   // Get a name for this matrix
   const MatrixType &m = *this;

   // Dimensions 2x2
   if (m.idim() == 2 && m.jdim() == 2)
      {
      MatrixType adj (2, 2);
      adj[0][0] =  m[1][1]; adj[0][1] = -m[1][0];
      adj[1][0] = -m[0][1]; adj[1][1] = m[0][0];
      return adj;
      }

   // Dimension 3x3
   if (m.idim() == 3 && m.jdim() == 3)
      {
      MatrixType adj (3, 3);
      cofactor_matrix_3x3_0 (m, adj);
      return adj;
      }

   // Otherwise, error for now
   error_message ("Adjoint of matrix of size %dx%d not implemented",
	      m.idim(), m.jdim());
   Error_Handler();

   // Just to keep the compiler happy
   return *this;
   }

MatrixType MatrixType::transpose () const
   {
   // Takes the transpose of a matrix
 
   // Get the name of the matrix
   const MatrixType &m = *this;

   // Carry out the transposition
   MatrixType trans (m.jdim(), m.idim());
   ::transpose (m, trans, m.ilow(), m.ihigh(), m.jlow(), m.jhigh());

   // Return the matrix
   return trans;
   }

#endif

VectorType MatrixType::row (int n) const
   {
   // Returns a vector consisting of the n-th row of the matrix

   // Get the name of the matrix
   const MatrixType &m = *this;

   // Get the n-th row
   VectorType arow (m.jdim());

#ifdef USE_MEMCPY

   const int nbytes = m.jdim() * sizeof (realtype);
   memcpy (&arow[0], m[n], nbytes);

#else

   for (int i=m.jlow(); i<=m.jhigh(); i++)
      arow[i] = m[n][i];

#endif

   // Return the row
   return arow;
   }

VectorType MatrixType::column (int n) const
   {
   // Returns a vector consisting of the n-th column of the matrix
   int i;

   // Get the name of the matrix
   const MatrixType &m = *this;

   // Get the n-th column
   VectorType acolumn (m.idim());
   for (i=ilow(); i<=m.ihigh(); i++)
      acolumn[i] = m[i][n];

   // Return the row
   return acolumn;
   }

VectorType MatrixType::rowsum () const
   {
   // Returns a vector equal to the sums along each row

   // Get the name of the matrix
   const MatrixType &M = *this;

   // Start with a zero vector
   VectorType sum = VectorType::zeros(M.idim());

   // Sum up
   for2Dindex (i, j, M )
      sum[i] += M[i][j];

   // Return
   return sum;
   }

VectorType MatrixType::columnsum () const
   {
   // Returns a vector equal to the sums down each column

   // Get the name of the matrix
   const MatrixType &M = *this;

   // Start with a zero vector
   VectorType sum = VectorType::zeros(M.jdim());

   // Sum up
   for2Dindex (i, j, M )
      sum[j] += M[i][j];

   // Return
   return sum;
   }

double MatrixType::Frobenius_norm () const
   {
   // Computes the Frobenius norm of the matrix
   const MatrixType &M = *this;
   
   // Find the sum of squares of the matrix
   double sumsq = 0.0;
   int i, j;
   for_2Dindex (i, j, M)
      sumsq += (M[i][j]*M[i][j]);

   // Return square root = Frobenius norm
   return sqrt(sumsq);
   }

MatrixType &MatrixType::setcolumn (int n, const class VectorType &acol)
   {
   // Sets the n-th row of the matrix to a vector
   MatrixType &M = *this;

   // Check the bounds
   if (M.idim() != acol.dim())
      {
      error_message (
	      "MatrixType::setcolumn : vector not of same dimension %d as matrix (%d x %d)",
            acol.dim(), M.idim(), M.jdim());
      Error_Handler (); 
      }

   // Now, fill out the row
   int i;
   for_1Dindex (i, acol) M[i][n] = acol[i];

   // Return the modified matrix
   return *this;
   }

MatrixType &MatrixType::setrow (int n, const class VectorType &arow)
   {
   // Sets the n-th row of the matrix to a vector
   MatrixType &M = *this;

   // Check the bounds
   const int dim = arow.dim();
   if (M.jdim() != dim)
      {
      error_message (
	   "MatrixType::setrow: vector not of same dimension %d as matrix (%d x %d)",
            arow.dim(), M.idim(), M.jdim());
      Error_Handler (); 
      }

#ifdef USE_MEMCPY

   int nbytes = dim * sizeof(realtype);
   memcpy (M[n], &arow[0], nbytes);

#else
   
   // Now, fill out the row
   int i;
   for_1Dindex (i, arow) M[n][i] = arow[i];

#endif

   // Return the modified matrix
   return *this;
   }

MatrixType &MatrixType::setrow (int n, const realtype *src)
   {
   // Sets a row of the matrix from a double pointer.
   // It cannot check the bounds of src to see that it is valid
   
   // Sets the n-th row of the matrix to a vector
   MatrixType &M = *this;

#define USE_MEMCPY
#ifdef USE_MEMCPY

   int nbytes = M.jdim() * sizeof(realtype);
   memcpy (M[n], src, nbytes);

#else

   for (int j=M.jlow(); j<=M.jhigh(); j++)
      M[n][j] = src[j];

#endif

   // Return the modified matrix
   return *this;
   }

void fprint_matrix_cc (
        const char *format,
        FILE *afile,
        realtype **a,
        int nrl,
        int nrh,
        int ncl,
        int nch)
   {
   /* Print out a matrix of double values */
   int i, j;
   for (i=nrl; i<=nrh; i++)
      {
      for (j=ncl; j<=nch; j++)
         fprintf (afile, format, a[i][j]);
      fprintf (afile, "\n");
      }
   fprintf (afile, "\n");
   }

void fprint_vector_cc (
        const char *format,
        FILE *afile,
        realtype *v,
        int nrl,
        int nrh)
   {
   /* Print out a vector of double values */
   int i;
   for (i=nrl; i<=nrh; i++)
      fprintf (afile, format, v[i]);
   fprintf (afile, "\n");
   }

void MatrixType::print (FILE *afile, const char *format) const
   {
   // Prints the matrix
   fprint_matrix_cc (format, afile,
	*this, this->ilow(), this->ihigh(), this->jlow(), this->jhigh());
   }

void MatrixType::print_ifdebug (FILE *afile, const char *format) const
   {
   // Prints the matrix
   if (debugging_messages_enabled())
      fprint_matrix_cc (format, afile,
	  *this, this->ilow(), this->ihigh(), this->jlow(), this->jhigh());
   }

void MatrixType::print_latex (FILE *afile, const char *format) const
   {
   // Prints the matrix suitable for latex processing
   int i, j;
   const MatrixType &A = *this;

   // Put out the format statements
   fprintf (afile, "\\mbegin{");
   for (j=jlow(); j<=jhigh(); j++) fprintf (afile, "r");
   fprintf (afile, "}\n");
  
   // Output the rows of the matrix
   for (i=ilow(); i<=ihigh(); i++)
      {
      for (j=jlow(); j<=jhigh(); j++)
	      {
	      fprintf (afile, format, A[i][j]);
	      if (j < jhigh()) fprintf (afile, " & ");
	      }

      // Final end of row
      if (i < ihigh()) fprintf (afile, " \\\\ \n");
      else fprintf (afile, "\n");
      }

   // End of the formatting
   fprintf (afile, "\\mend\n");
   }

void print (const MatrixType &m) { m.print (); }
void mprint (const MatrixType &m) { m.print (); }

void MatrixType::set_Error_Handler (void (*fn)()) const
   {
   // Set the error handler
   Error_Handler = fn;
   }

MatrixType MatrixType::operator * (double k) const
   {
   // Scalar product
   int i, j;
   const MatrixType &m = *this;

   MatrixType prod (m.idim(), m.jdim());
   for_2Dindex (i, j, m)
      prod[i][j] = (realtype) (k * m[i][j]);
   return prod;
   }

void MatrixType::operator *= (double k)
   {
   // Multiply by a scalar
   int i, j;
   const MatrixType &m = *this;

   for_2Dindex (i, j, m)
      m[i][j] = (realtype) (m[i][j] * k);
   }

void MatrixType::operator /= (double k)
   {
   // Scalar product
   int i, j;
   const MatrixType &m = *this;

   for_2Dindex (i, j, m)
      m[i][j] = (realtype) (m[i][j] / k);
   }

MatrixType MatrixType::operator / (double k) const
   {
   // Scalar product
   int i, j;
   const MatrixType &m = *this;

   MatrixType prod (m.idim(), m.jdim());
   for_2Dindex (i, j, m)
      prod[i][j] = (realtype) (m[i][j] / k);
   return prod;
   }

MatrixType MatrixType::normalized() const
   {
   int i, j;

   // Take an alias for the first vector
   const MatrixType &A = *this;

   // Create a matrix of the same size
   MatrixType B (A.idim(), A.jdim());

   // Find sum of squares of entries
   double sumsq = 0.0;
   for_2Dindex (i, j, A)
      sumsq += A[i][j] * A[i][j];

   // Compute the factor
   double invlen = 1.0/sqrt(sumsq);	// Length of the vector

   // Multiply entries by the factor
   for_2Dindex (i, j, B)
      B[i][j] = (realtype) (A[i][j] * invlen);

   // Return the normalized matrix
   return B;
   }

void MatrixType::normalize()
   {
   int i, j;

   // Take an alias for the first vector
   const MatrixType &A = *this;

   // Find sum of squares of entries
   double sumsq = 0.0;
   for_2Dindex (i, j, A)
      sumsq += A[i][j] * A[i][j];

   // Compute the factor
   double invlen = 1.0/sqrt(sumsq);	// Length of the vector

   // Multiply entries by the factor
   for_2Dindex (i, j, A)
      A[i][j] = (realtype) (A[i][j] * invlen);
   }

MatrixType MatrixType::copy() const
   {

   // Take an alias for the first vector
   const MatrixType &A = *this;

   // Create a matrix of the same size
   MatrixType B (A.idim(), A.jdim());

   // Multiply entries by the factor
#ifdef USE_MEMCPY

   int nbytes = A.idim() * A.jdim() * sizeof (realtype);
   memcpy (&B[0][0], &A[0][0], nbytes);

#else

   for2Dindex (i, j, B)
      B[i][j] = A[i][j];

#endif

   // Return the normalized matrix
   return B;
   }

void MatrixType::resize(int newrows, bool clear)
   {
   // Just resizes the number of rows. Number of columns is fixed
   // The matrix passed is resized in place by reallocating new
   // memory.  Any other matrices that may refer to the same location
   // will be left referring to the old location

   // Take an alias for the first vector
   MatrixType &A = *this;
   const int oldrows = A.idim();
   const int jdim = A.jdim();

   // Take an alias for it
   MatrixType B = A;

   // Now, reinitialize A -- so now A and B point to different memory
   A.Init(newrows, jdim);

   // Work out how many rows to copy
   int nrows_to_copy = (newrows > oldrows) ? oldrows : newrows;
   int nbytes = nrows_to_copy * jdim * sizeof (realtype);

   // Copy using memcpy for speed
   memcpy (&A[0][0], &B[0][0], nbytes);

   // Clear the other values if required
   if (clear && newrows > oldrows)
      {
      int nrows_to_clear = newrows - oldrows;
      int nbytes = nrows_to_clear * jdim * sizeof (realtype);
      memset(&(A[nrows_to_copy][0]), 0, nbytes);
      }
   }

// 
// These would be faster if we knew that the input type and matrix types matched
//
void MatrixType::copy_to(double **a) const
   {
   const MatrixType &A = *this;

   int i, j;
   for_2Dindex (i, j, A)
      a[i][j] = A[i][j];
   }

void MatrixType::copy_to(float **a) const
   {
   const MatrixType &A = *this;

   int i, j;
   for_2Dindex (i, j, A)
      a[i][j] = (float) A[i][j];
   }

//
// Vector methods
//

VectorType VectorType::operator - (const VectorType & v2) const
   {
   // Difference of two vectors
   int i;
   const VectorType &v1 = *this;

   // Test the bounds
   if (v1.dim() != v2.dim())
      {
      error_message ("VectorType::op(-) : vectors not of same dimension %d, %d",
            v1.dim(), v2.dim());
      Error_Handler (); 
      }

   // Carry out the product
   VectorType prod (v1.dim());
   for_1Dindex (i, v1)
      prod [i] = v1[i] - v2[i];
  
   // Return the product matrix
   return prod;
   }

VectorType VectorType::operator + (const VectorType & v2) const
   {
   // Difference of two vectors
   int i;
   const VectorType &v1 = *this;

   // Test the bounds
   if (v1.dim() != v2.dim())
      {
      error_message ("VectorType::op(+) : vectors not of same dimension %d, %d",
            v1.dim(), v2.dim());
      Error_Handler (); 
      }

   // Carry out the product
   VectorType prod (v1.dim());
   for_1Dindex (i, v1)
      prod [i] = v1[i] + v2[i];
  
   // Return the product matrix
   return prod;
   }

void VectorType::operator += (const VectorType & v2)
   {
   // Add a vector
   int i;
   const VectorType &v1 = *this;

   // Test the bounds
   if (v1.dim() != v2.dim())
      {
      error_message("VectorType::op(+=) : vectors not of same dimension %d, %d",
            v1.dim(), v2.dim());
      Error_Handler (); 
      }

   // Carry out the product
   for_1Dindex (i, v1)
      v1 [i] += v2[i];
   }

void VectorType::operator *= (double k)
   {
   // Multiply by a constant
   int i;
   const VectorType &v1 = *this;

   // Carry out the product
   for_1Dindex (i, v1)
      v1[i] = (realtype) (v1[i] * k);
   }

void VectorType::operator /= (double k)
   {
   // Scalar product
   int i;
   const VectorType &v = *this;

   for_1Dindex (i, v)
      v[i] = (realtype) (v[i] / k);
   }

void VectorType::operator -= (const VectorType & v2)
   {
   // Subtract a vector
   int i;
   const VectorType &v1 = *this;

   // Test the bounds
   if (v1.dim() != v2.dim())
      {
      error_message("VectorType::op(-=) : vectors not of same dimension %d, %d",
            v1.dim(), v2.dim());
      Error_Handler (); 
      }

   // Carry out the product
   for_1Dindex (i, v1)
      v1 [i] -= v2[i];
   }

double VectorType::operator * (const VectorType & v2) const
   {
   // Inner product of two vectors
   int i;
   const VectorType &v1 = *this;

   // Test the bounds
   if (v1.dim() != v2.dim())
      {
      error_message("VectorType::op(*) : vectors not of same dimension %d, %d",
            v1.dim(), v2.dim());
      Error_Handler (); 
      }

   // Carry out the product
   double prod = 0.0;
   for_1Dindex (i, v1)
      prod += v1[i] * v2[i];
  
   // Return the product matrix
   return prod;
   }

#ifdef DOUBLE_ONLY
VectorType VectorType::operator * (const MatrixType & m) const
   {
   // VectorType x matrix
   const VectorType &v = *this;

   // Test the bounds
   if (v.dim() != m.idim())
      {
      error_message ("VectorType-matrix multiplication : Incompatible sizes");
      error_message ("   %d  and (%d x %d)", 
            v.dim(), m.idim(), m.jdim());
      Error_Handler (); 
      }

   // Carry out the product
   VectorType prod (m.jdim());
   vector_matrix_prod_0 (v, m, m.idim(), m.jdim(), prod);
  
   // Return the product matrix
   return prod;
   }
#endif

MatrixType VectorType::operator ^ (const VectorType & v2) const
   {
   // Returns the product v1 * v2^T
   // VectorType x matrix
   const VectorType &v1 = *this;

   // Declare array for the result
   MatrixType tensor (v1.dim(), v2.dim());

   // Carry out the tensor product
   int i, j;
   for_1Dindex (i, v1) for_1Dindex(j, v2)
      tensor [i][j] = v1[i]*v2[j];
  
   // Return the product matrix
   return tensor;
   }

void VectorType::print (FILE *afile, const char *format) const
   {
   // Prints the matrix
   fprint_vector_cc (format, afile, *this, this->low(), this->high());
   }

void VectorType::print_ifdebug (FILE *afile, const char *format) const
   {
   // Prints the matrix
   if (debugging_messages_enabled())
      fprint_vector_cc (format, afile, *this, this->low(), this->high());
   }

void print (VectorType &v) { v.print (); }
void vprint (VectorType &v) { v.print (); }

void VectorType::set_Error_Handler (void (*fn)()) const
   {
   // Set the error handler
   Error_Handler = fn;
   }

VectorType VectorType::operator * (double k) const
   {
   // Scalar product
   int i;
   const VectorType &v = *this;
   VectorType prod (v.dim());
   for_1Dindex (i, v)
      prod [i] = (realtype) (k * v[i]);
   return prod;
   }

VectorType VectorType::operator / (double k) const
   {
   // Scalar product
   int i;
   const VectorType &v = *this;
   VectorType prod (v.dim());
   for_1Dindex (i, v)
      prod [i] = (realtype) (v[i] / k);
   return prod;
   }

double VectorType::length() const
   {
   const VectorType &v = *this;
   return sqrt (v*v);
   }

double VectorType::length_sq() const
   {
   const VectorType &v = *this;
   return v*v;
   }

VectorType VectorType::normalized() const
   {
   // Returns a unit vector in the same direction as the input vector
   int i;

   // Make an alias for the input vector
   const VectorType &A = *this;

   // Create the vector to be returned
   VectorType B (A.dim());

   // Compute a scaling factor
   double invlen = 1.0/A.length();	// Length of the vector

   // Multiply each entry
   for_1Dindex (i, B)
      B[i] = (realtype) (A[i]*invlen);

   // Return the vector itself
   return B;
   }

void VectorType::normalize()
   {
   // Normalizes the vector in situ
   int i;

   // Make an alias for the input vector
   const VectorType &A = *this;

   // Compute a scaling factor
   double invlen = 1.0/A.length();	// Length of the vector

   // Multiply each entry
   for_1Dindex (i, A)
      A[i] = (realtype)(A[i] * invlen);
   }

VectorType VectorType::copy() const
   {
   // Returns a unit vector in the same direction as the input vector

   // Make an alias for the input vector
   const VectorType &A = *this;

   // Create the vector to be returned
   VectorType B (A.dim());

   // Multiply each entry
#ifdef USE_MEMCPY
   int nbytes = A.dim() * sizeof (realtype);
   memcpy (&B[0], &A[0], nbytes);
#else
   for1Dindex (i, B)
      B[i] = A[i];
#endif

   // Return the vector itself
   return B;
   }

// Faster if we know that VectorType is double
void VectorType::copy_to(double *a) const
   {
   const VectorType &A = *this;

   int i;
   for_1Dindex (i, A)
      a[i] = A[i];
   }

//
// Other routines useful with matrices
//

#ifdef DOUBLE_ONLY
VectorType cross_product (const VectorType &A, const VectorType &B)
   {
   // Takes the cross product of vectors

   // Test the size
   if (A.dim() != 3 || B.dim() != 3)
      {
      error_message ("cross_product : Wrong size vectors (%d x %d)",
	      A.dim(), B.dim());
      A.Error_Handler();
      }

   // Otherwise, take the product
   VectorType C(3);
   cross_product_0 (A, B, C);
   return C;
   }

VectorType lin_solve_old (const MatrixType &A, const VectorType &B)
   {
   // Solves a square set of linear equations

   // Check the dimensions
   if (A.idim() < A.jdim() || A.idim() != B.dim())
      {
      error_message ("lin_solve : Bad dimensions for linear system");
      A.Error_Handler();
      }

   // Solve the system
   VectorType X (A.jdim());

   // Solve by gaussian elimination if it is a square system
   int rval;
   if (A.idim() == A.jdim()) 
      rval = lin_solve_0 (A, X, B, A.idim());

   // Find least-squares solution if over constrainted
   else
      rval = solve_simple_0 (A, X, B, A.idim(), A.jdim());
   
   // Return the value
   if (!rval)
      {
      error_message ("lin_solve : Solution failed");
      A.Error_Handler();
      }

   // Return the solution
   return X;
   }

VectorType lin_solve (const MatrixType &A, const VectorType &B)
   {
   // Finds the least-squares solution to a set of linear equations
   // This is just the ordinary solution if A is square.
   // Uses the normal equations method so as to avoid Numerical Recipes

   // Check the dimensions
   if (A.idim() < A.jdim() || A.idim() != B.dim())
      {
      error_message ("lin_solve : Bad dimensions for linear system");
      A.Error_Handler();
      }

   // Solve the system
   VectorType X (A.jdim());

   // Solve by gaussian elimination if it is a square system
   int rval;
   if (A.idim() == A.jdim()) 
      rval = lin_solve_0 (A, X, B, A.idim());

   // Find least-squares solution if over constrainted
   else
      rval = solve_pseudo_0 (A, X, B, A.idim(), A.jdim());

   // Either of these methods would have failed with deficient rank systems
   if (! rval)
      rval = solve_simple_0 (A, X, B, A.idim(), A.jdim());
   
   // Check for success
   if (!rval)
      {
      error_message ("lin_solve : Solution failed");
      A.Error_Handler();
      }

   // Return the solution
   return X;
   }

MatrixType MatrixType::identity (int dim)
   {
   // Returns an identity matrix
   MatrixType M(dim, dim);
   identity_matrix_0 (M, dim);
   return M;
   }
#endif

MatrixType MatrixType::diagonal (const VectorType &v)
   {
   int dim = v.dim();
   MatrixType M = MatrixType::zeros(dim, dim);
   for1Dindex (i, v)
      M[i][i] = v[i];
   return M;
   }

MatrixType MatrixType::diagonal (double d1, double d2)
   {
   MatrixType M = MatrixType::zeros(2, 2);
   M[0][0] = (realtype) d1;
   M[1][1] = (realtype) d2;
   return M;
   }

MatrixType MatrixType::diagonal (double d1, double d2, double d3)
   {
   MatrixType M = MatrixType::zeros(3, 3);
   M[0][0] = (realtype) d1;
   M[1][1] = (realtype) d2;
   M[2][2] = (realtype) d3;
   return M;
   }

MatrixType MatrixType::diagonal (double d1, double d2, double d3, double d4)
   {
   MatrixType M = MatrixType::zeros(4, 4);
   M[0][0] = (realtype) d1;
   M[1][1] = (realtype) d2;
   M[2][2] = (realtype) d3;
   M[3][3] = (realtype) d4;
   return M;
   }

VectorType MatrixType::diagonal () const
   {
   int dim = rhMin(this->idim(), this->jdim());
   MatrixType min = *this;
   VectorType vec(dim);
   for1Dindex (i, vec)
      vec[i] = min[i][i];
   return vec;
   }

MatrixType MatrixType::zeros (int idim, int jdim)
   {
   // Matrix of zeros
   MatrixType M(idim, jdim);
   memset(&(M[0][0]), 0, idim * jdim * sizeof(realtype));
   return M;
   }

MatrixType MatrixType::ones (int idim, int jdim)
   {
   // Matrix of ones
   MatrixType M(idim, jdim);
   M.clear(1.0);
   return M;
   }

MatrixType MatrixType::I0 ()
   {
   // Standard canonic camera matrix [I | 0]
   MatrixType M(3, 4);
   M.clear(0.0);
   M[0][0] = M[1][1] = M[2][2] = 1.0;
   return M;
   }

MatrixType MatrixType::skew (const VectorType &v)
   {
   // Makes the skew matrix out of v
   MatrixType M(3, 3);
   M[0][0] =   0.0;  M[0][1] = -v[2]; M[0][2] =  v[1];
   M[1][0] =  v[2];  M[1][1] =   0.0; M[1][2] = -v[0];
   M[2][0] = -v[1];  M[2][1] =  v[0]; M[2][2] =   0.0;

   return M;
   }

VectorType VectorType::operator << (int n) const
   {
   // Shifts the vector n places.  In other words, out[i] = A[i+n]

   // Get an alias for the input vector
   const VectorType &A = *this;
   int dim = A.dim();
   int low = A.low();
   int high = A.high();

   // Create the output vector
   VectorType out (A.dim());

   // Make it into a positive shift
   if (n < dim) n += dim;

   // Now, shift the vector
      {
      int i;
      register int count = low;
      for (i=high-n+1; i<=high; i++)
	 out[i] = A[count++];
      for (i=low; i<=high-n; i++)
	 out[i] = A[count++];
      }

   // Return the shifted vector
   return out;
   }

// Unary minus operators
VectorType operator - (const VectorType &v)
   {
   // Unary minus
   int i;
   VectorType v2 (v.dim());
   for_1Dindex (i, v) v2[i] = -v[i];
   return v2;
   }

MatrixType operator - (const MatrixType &m)
   {
   // Unary minus
   int i, j;
   MatrixType m2 (m.idim(), m.jdim());
   for_2Dindex (i, j, m) m2[i][j] = -m[i][j];
   return m2;
   }

MatrixType operator * (double k, const MatrixType &m)
   { 
   // Multiplication by a constant
   return m*k; 
   }

VectorType operator * (double k, const VectorType &v)
   { 
   // Mult by const
   return v*k; 
   }

MatrixType VectorType::operator | (const MatrixType & m2) const
   {
   // Concatenate vector followed by a matrix
   int i, j;
   const VectorType &m1 = *this;

   // Test the bounds
   if (m1.dim() != m2.idim())
      {
      error_message (
         "VectorType::op(| MatrixType) : arrays have different # rows");
      error_message ("   %d  and (%d x %d)", m1.dim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.dim(), 1 + m2.jdim());

   // Copy the data
   for_1Dindex (i, m1)
      m1m2[i][0] = m1[i];

   for_2Dindex (i, j, m2)
      m1m2[i][j+1] = m2[i][j];

   // Return the result
   return m1m2;
   }

MatrixType VectorType::operator | (const VectorType & m2) const
   {
   // Concatenate two vectors side by side
   int i;
   const VectorType &m1 = *this;

   // Test the bounds
   if (m1.dim() != m2.dim())
      {
      error_message (
"VectorType::op(| VectorType) : vectors have different dimensions %d  and %d",
         m1.dim(), m2.dim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m1.dim(), 2);

   // Copy the data
   for_1Dindex (i, m1)
      m1m2[i][0] = m1[i];

   for_1Dindex (i, m2)
      m1m2[i][1] = m2[i];

   // Return the result
   return m1m2;
   }

MatrixType VectorType::operator & (const MatrixType & m2) const
   {
   // Concatenate a vector above a matrix
   int i, j;
   const VectorType &m1 = *this;

   // Test the bounds
   if (m1.dim() != m2.jdim())
      {
      error_message (
        "VectorType::op(& MatrixType) : arrays have different dimensions");
      error_message ("   %d  and (%d x %d)", m1.dim(), m2.idim(), m2.jdim());
      Error_Handler (); 
      }

   // Now, make a new matrix
   MatrixType m1m2(m2.idim()+1,  m2.jdim());
   int offset = 1;

   // Copy the data
   for_1Dindex (j, m1)
      m1m2[0][j] = m1[j];

   for_2Dindex (i, j, m2)
      m1m2[i+offset][j] = m2[i][j];

   // Return the result
   return m1m2;
   }

VectorType VectorType::operator & (const VectorType & m2) const
   {
   // Concatenate two vectors into one vector
   int i;
   const VectorType &m1 = *this;

   // Now, make a new matrix
   VectorType m1m2(m1.dim() + m2.dim());
   int offset = m1.dim();

   // Copy the data
   for_1Dindex (i, m1)
      m1m2[i] = m1[i];

   for_1Dindex (i, m2)
      m1m2[offset+i] = m2[i];

   // Return the result
   return m1m2;
   }

// -----------------------------------------------------
// Could be faster if realtype were the same as double
// -----------------------------------------------------
VectorType::VectorType (int m, double *vals)
   {
   // Makes a vector from an array of doubles
   int i;
   Init(m);
   VectorType &vec = *this;
   for_1Dindex (i, vec)
      vec[i] = (realtype) vals[i];
   }

VectorType::VectorType (const MatrixType &M)
   {
   // Makes a vector from matrix elements

   // Get the dimension of the vector and initialize it
   int dim = M.idim() * M.jdim();
   Init(dim);

   // Transfer the elements
   VectorType &vec = *this;

#ifdef USE_MEMCPY
   int nbytes = dim * sizeof(realtype);
   memcpy (&vec[0], &M[0][0], nbytes);
#else
   int count = vec.low();
   for_2Dindex (i, j, M)
      vec[count++] = M[i][j];
#endif
   }

VectorType VectorType::dehom () const
   {
   // Dehomogenizes the vector, resulting in a vector of length one less

   // Declare a vector for output
   const VectorType &v = *this;
   VectorType h (v.dim() - 1);

   // Compute the scale factor -- hope it is not infinity
   double fac = 1.0 / v[v.high()];

   // Multiply v by the scale factor
   int i;
   for_1Dindex (i, h)
      h[i] = (realtype) (v[i] * fac);

   // Return the homogenized vector
   return h;
   }

double VectorType::dehom (int i) const
   {
   // Gives the dehomogenized coordinate value
   const VectorType &v = *this;
   return v[i] / v[v.high()];
   }

VectorType VectorType::hom () const
   {
   // Homogenizes the vector, resulting in a vector of length one more

   // Declare a vector for output
   const VectorType &v = *this;
   VectorType h (v.dim() + 1);

   // Copy the vector and put a 1 on the end
   int i;
   for_1Dindex (i, v)
      h[i] = v[i];
   h[h.high()] = 1.0;

   // Return the homogenized vector
   return h;
   }

//#ifdef DOUBLE_ONLY
bool MatrixType::read_from_file (const char *fname)
   {
   // Read a matrix from a file -- it is assumed that the matrix is
   // one row per row of input ascii file
   int i;

   // First, try to open the file
   FILE *infile = fopen (fname, "r");
   if (! infile)
      {
      error_code = MatrixType::CANNOT_OPEN_FILE;
      return 0;
      }

   // Now, try to read it
   _io_init (infile, (char *)fname);
   int linecount = 0;
   int numfields = 0;
   while (1)
      {
      // Get a line divided into tokens
      int n = _nextline (infile);

      // If not at EOF, then count the line
      if (n == EOF) break;

      // Count the line
      linecount ++;

      // Get the number of fields in a line
      if (linecount == 1) numfields = _NF;
   
      // Check that they all have the same number of tokens
      if (linecount != 1 && _NF != numfields)
	 {
	 fprintf (stderr, "Wrong number of tokens in line\n");
	 _reporterror ();
	 error_code = MatrixType::BAD_MATRIX_DATA_FILE;
	 return 0;
	 }
      }

   // Now, make a matrix of the right size
   MatrixType A (linecount, numfields);

   // Close the file and start again
   fclose (infile);
   _io_cleanup();

   // -------------------------------------------------------
   // Second pass
   // Start again -- this time to read the data
   //--------------------------------------------------------

   // First, try to open the file
   infile = fopen (fname, "r");
   if (! infile)
      {
      error_code = MatrixType::CANNOT_OPEN_FILE;
      return 0;
      }

   // Now, try to read it
   _io_init (infile, (char *)fname);
   linecount = 0;
   while (1)
      {
      // Get a line divided into tokens
      int n = _nextline (infile);

      // If not at EOF, then count the line
      if (n == EOF) break;

      // Count the line
      linecount ++;

      // Check, but this should not happen
      // Check that they all have the same number of tokens
      if (linecount != 1 && _NF != numfields)
	 {
	 fprintf (stderr, "Wrong number of tokens in line\n");
	 _reporterror ();
	 error_code = MatrixType::BAD_MATRIX_DATA_FILE;
	 return 0;
	 }

      // Read the data into the matrix
      for (i=1; i<=numfields; i++)
	 A[linecount-1][i-1] = (realtype) (atof (_[i]));
      }

   // Close the file and start again
   fclose (infile);
   _io_cleanup();

   // Finally, transfer the data to the input array
   MatrixType &Ain = *this;
   Ain.Init (linecount, numfields);
   A.copy_to (Ain);

   // Successful return
   return 1;
   }

//#endif

VectorType VectorType::zeros (int idim)
   {
   // Standard canonic camera matrix [I | 0]
   VectorType M(idim);
   memset(&(M[0]), 0, idim * sizeof(realtype));
   return M;
   }

VectorType VectorType::ones (int idim)
   {
   // Vector of ones
   VectorType v(idim);
   v.clear(1.0);
   return v;
   }

// A couple of things needed for making lists
bool MatrixType::operator == (const MatrixType &b) const { return this == &b; }
bool MatrixType::operator <  (const MatrixType &) const { return true; }

