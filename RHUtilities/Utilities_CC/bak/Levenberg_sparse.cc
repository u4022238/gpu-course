// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// This gives an object-oriented Levenberg-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector Levenberg_sparse::function(const rhVector &);
//

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Levenberg_sparse.h>

Levenberg_sparse::Levenberg_sparse () : 
	xparam (NULL), Num_Loops (100), Min_Incr (1.0e-2)
   {}

Levenberg_sparse::~Levenberg_sparse () 
   {
   if (xparam) delete [] xparam;
   }

//
// Keyboard interrupt stuff
//

rhBOOLEAN Levenberg_sparse::interrupt_pending = FALSE;

#ifndef WIN32
void Levenberg_sparse::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
   interrupt_pending = TRUE; 
   }
#endif

rhMatrix Levenberg_sparse::jx ( int i, rhVector &cparam, rhVector &xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (i, cparam, xparam);

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), xparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, xparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = xparam[j];

      // Increment the j-th parameter 
      double delta = FAC * xparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      xparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (i, cparam, xparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);
      
      // Restore the value of xparam[j] 
      xparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

rhMatrix Levenberg_sparse::jcam ( int i, rhVector &cparam, rhVector &xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (i, cparam, xparam);

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), cparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, cparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = cparam[j];

      // Increment the j-th parameter 
      double delta = FAC * cparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      cparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (i, cparam, xparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);
      
      // Restore the value of cparam[j] 
      cparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

void Levenberg_sparse::new_position_action ( rhVector &, rhVector *, int )
   { /* Default is to do nothing */ }

rhVector concatenate_rhVectors (rhVector *v, int numv)
   {
   // Concatenates a bunch of vectors into one
   int i, j;
   int length;   // Total length of vectors

   // Compute the total length
   for (i=0, length=0; i<numv; i++) length += v[i].dim();

   // Now allocate a vector
   rhVector vall (length);

   // Fill it out
   int count = 1;
   for (i=0; i<numv; i++)
      for_1Dindex (j, v[i])
	 vall[count++] = v[i][j];

   // Return the concatenated vector
   return vall;
   }

rhVector Levenberg_sparse::total_function (
	const rhVector &cparam, rhVector *xparam, int numpoints)
   {
   // The total concatenated set of functions
   
   // First of all, just run the separate functions
   rhVector *v = new rhVector [numpoints];

   // Run the separate functions
   for (int i=0; i<numpoints; i++)
      v[i] = function (i, cparam, xparam[i]);

   // Concatenate
   rhVector vall = concatenate_rhVectors (v, numpoints);

   // Free the function values
   delete [] v;

   // Return the function vector
   return vall;
   }

void augment (rhMatrix &M, double lambda)
   {
   // Augment diagonal elements of M, assumed square
   for (int j=M.ilow(); j<=M.ihigh(); j++)
      M[j][j] *= 1.0 + (lambda);
   }

void Levenberg_sparse::compute_increments (
	rhMatrix *AA, rhMatrix *BB, rhVector *epsilon,   // Inputs
	int numpoints,
	double lambda, 		// Regularization factor
	rhVector &cdel,		// Camera parameter increments
	rhVector *xdel		// Point increments
        )
   {
   // Computes the increments
   int i;

   // First declare the matrix
   int dim = AA[0].jdim();
   rhMatrix N(dim, dim);
   N.clear (0.0);

   // Make some storage for temporary matrices
   rhMatrix *BTBinv = new rhMatrix [numpoints];
   rhMatrix *ATB    = new rhMatrix [numpoints];

   // Also the vector
   rhVector b(dim);
   b.clear(0.0);
   
   // Now, fill it
   for (i=0; i<numpoints; i++)
      {
      // Take aliases for A and B
      rhMatrix A = AA[i];
      rhMatrix B = BB[i];
      rhMatrix AT = A.transpose();
      rhMatrix BT = B.transpose();
    
      // First compute the values (B_i^T B_i)^-1
      rhMatrix BTB = BT*B;
      augment (BTB, lambda);
      BTBinv[i] = BTB.inverse();

      // Now, ATA
      rhMatrix ATA = AT * A;
      augment (ATA, lambda);
     
      // ATB
      ATB[i] = AT * B;

      // Put the whole thing together
      N = N + ATA - ATB[i] * BTBinv[i] * ATB[i].transpose();

      // Now, the vector
      b = b + (AT - ATB[i] * BTBinv[i] * BT) * epsilon[i];
      }

   // Matrix solution -- solution overwrites b
   if (! (lin_solve_symmetric_0(N, b, b, dim)
          || solve_simple_0 (N, b, b, dim, dim)))
      {
      error_message ("Can not solve normal equations -- exiting");
      bail_out (2);
      }

   // b is the delta to cameras
   cdel = b;

   // Back substitute to get the values of all the parameters
   for (i=0; i<numpoints; i++)
      {
      // Compute it in transposed form
      xdel[i] = (epsilon[i] * BB[i] - cdel * ATB[i]) * BTBinv[i];
      }

   // Now free the temporaries
   delete [] ATB;
   delete [] BTBinv;
   }
   
double Levenberg_sparse::solve ()
   {
   // Declare variables 
   int i;
   double chisq = 0.0;
   double lambda = 1.0;
   rhBOOLEAN improvement = TRUE;

   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ Levenberg_sparse::keybd_interrupt);
#endif

   // Set up some needed data structures
   rhVector *epsilon    = new rhVector [ numpoints ];
   rhVector *new_xparam = new rhVector [ numpoints ];
   rhVector *xdel       = new rhVector [ numpoints ];
   rhMatrix *A          = new rhMatrix [ numpoints ];
   rhMatrix *B          = new rhMatrix [ numpoints ];
   rhVector new_cparam;
   rhVector cdel;

   // Now iterate 
   for (int icount=0; icount<Num_Loops && improvement; icount++)
      {
      double increment_ratio;
   
      // Compute the function value 
      for (i=0; i<numpoints; i++)
         epsilon[i] = function (i, cparam, xparam[i]);

      // Compute an initial value of the error 
      rhVector dy = concatenate_rhVectors (epsilon, numpoints);
      chisq = dy * dy;

      // Output some debugging info 
      if (icount == 0)
         informative_message ("\tInitial error : chisq = %g, rms = %g", 
		chisq, sqrt(chisq/dy.dim()));
 
      // Compute the Jacobians
      for (i=0; i<numpoints; i++)
	 {
	 A[i] = jcam (i, cparam, xparam[i]);
	 B[i] = jx   (i, cparam, xparam[i]);
	 }

// #define WRITE_JACOBIAN
#     ifdef WRITE_JACOBIAN
      // Write out the partial derivatives
      for (i=0; i<numpoints; i++)
	 {
	 fprintf (stderr, "\nPoint %d\n", i);

	 // The camera derivatives
         fprintf (stderr, "\nDeriv wrt cameras (%d x %d) : \n", 
		A[i].idim(), A[i].jdim());
	 A[i].print (stderr, "%11.3f ");

	 // The point derivatives
         fprintf (stderr, "\nDeriv wrt point %d :  (%d x %d) : \n", 
		i, B[i].idim(), B[i].jdim());
	 B[i].print (stderr, "%11.3f ");
 	 }
#     endif

      // Now estimate new position with different alambda until improvement 
      do {
	 //---------------------------------------
	 // Compute the increments
	 //---------------------------------------

	 compute_increments (A, B, epsilon, numpoints, lambda, cdel, xdel);
	 new_cparam = cparam - cdel;
         for (i=0; i<numpoints; i++) new_xparam[i] = xparam[i] - xdel[i];
	 
	 //----------------------------------------
         //  Calculate new error at this position
	 //----------------------------------------

         // Compute the function value 
         rhVector newdy = total_function (new_cparam, new_xparam, numpoints);

         // Get the value of chisq resulting from this error vector
         double newchisq = newdy * newdy;

         // Compute the proportional change in the error vector
   	    {
  	    rhVector del = newdy-dy;
	    double len_del = sqrt(del*del);
	    double len_dy = sqrt (chisq);
            increment_ratio = len_del / len_dy;
	    }

         // Accept or reject it 
         if (newchisq < chisq)
            {
            // Success, accept this new solution 
            lambda *= 0.1;
            chisq = newchisq;
   	
	    // Accept the new parameter values 
	    cparam = new_cparam;
	    for (i=0; i<numpoints; i++)
	       xparam[i] = new_xparam[i];
   
            // Call any user function 
            new_position_action (cparam, new_xparam, numpoints);

	    // Signal improvement 
	    improvement = TRUE;
            }
         else
            {
            // Failure, increas lambda and return. 
            lambda *= 10.0;
	    improvement = FALSE;
            }
         } while ( ! improvement && increment_ratio >= 
		        Min_Incr && (lambda < 1e10));
        
      // Output some debugging info 
      informative_message ("\tlambda = %5.0e, chisq = %g, rms = %g", 
	 lambda, chisq, sqrt(chisq/dy.dim()));

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if the increment_ratio has been too small 
      if (increment_ratio < Min_Incr || lambda >= 1e10) break;
      }

   // Free the temporaries
   delete [] new_xparam;
   delete [] epsilon;
   delete [] A;
   delete [] B;

   // Return the value of the error 
   return chisq;
   }

rhMatrix Levenberg_sparse::covarianceMatrix (rhVector &mparam)
   {
   // This is not correct right now
   rhMatrix J; return J;
   }

void Levenberg_sparse::setParameters (rhVector &cp, rhVector *xp, int npts) 
   {
   // Creates the data
   numpoints = npts;

   // Set up the new parameters
   if (xparam) delete [] xparam; 
   xparam = new rhVector [numpoints];
   for (int i=0; i<numpoints; i++)
      xparam[i] = xp[i].copy();

   // And copy the camera parameters
   cparam = cp.copy();
   }

void Levenberg_sparse::getParameters (rhVector &cp, rhVector **xp, int *npts)
   {
   // Initialize the xp parameters
   *xp = new rhVector [numpoints];

   // Copy them
   cp = cparam.copy();
   for (int i=0; i<numpoints; i++)
      (*xp)[i] = xparam[i].copy();

   // Return the number of points
   *npts = numpoints;
   }
