// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// A string class
//

#ifndef string_utilities_h
#define string_utilities_h

#include <Utilities_CC/arraygen.h>

class rhString : public Char_vector
   {
   public :

   void Init (int len)
      {
      // Just calls the Char_vector initializer
      Char_vector::Init (0, len-1);
      }

   // Constructors.  The initial index is always 0
   rhString () : Char_vector () {}
   rhString (int len) : Char_vector (0, len-1) {}
   rhString (const char *s);

   // Concatenation operator
   rhString operator + (const rhString &s1) const;

   // Comparison operator
   rhBOOLEAN operator == (const rhString &s2) const;
   rhBOOLEAN operator == (const char *s2) const {return *this == rhString(s2);}
   rhBOOLEAN operator != (const rhString &s2) const {return !(*this == s2); }
   rhBOOLEAN operator != (const char *s2) const {return !(*this == s2); }

   // String length operator
   int length () const { return (int) strlen (this->data()); }
   };

#endif
