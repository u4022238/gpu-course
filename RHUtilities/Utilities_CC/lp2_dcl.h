#ifndef _lp2_cc_dcl_
#define _lp2_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>

//#define rhDEBUG

// Error Messages
#define  LP_Success                  0
#define  LP_Unbounded                1
#define  LP_Infeasible               10
/* */

FUNCTION_DECL( rhVector my_lp_constrained , (
             rhMatrix &A, 
             rhVector &b, 
             rhMatrix &Aeq,
             rhVector &beq,
             rhVector &goal, 
             int *result,
             bool feasibility_only
             ));

FUNCTION_DECL( rhVector my_lp_constrained , (
	rhMatrix &A, 
	rhVector &b, 
	rhVector &goal, 
	int *flag,
        bool feasibility_only
	));

#endif
