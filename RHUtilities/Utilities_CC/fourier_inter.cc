// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)fourier_inter.cc	1.4 01 Oct 1995

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/fourier_interface.h>
#include <Utilities_CC/ComplexVector.h>

// Routines for doing Fourier transform

void fourier_transform (
   const rhVector &sig,		// The input signal
   rhVector &re,		// Real part of the transform
   rhVector &im,		// Imaginary part of the transform
   int dir			// Forward or backward
   )
   {
   // Does a fourier transform of a real signal
   
   // Take the transform of the real input signal
   ComplexVector z2 = fourier_transform (ComplexVector(sig), dir);

   // Extract the real and imaginary parts
   re = z2.real();
   im = z2.imag();
   }

void fourier_transform (
   const rhVector &in_re,	// The input signal
   const rhVector &in_im,	// The input signal
   rhVector &re,		// Real part of the transform
   rhVector &im,		// Imaginary part of the transform
   int dir		// Forward or backward
   )
   {
   // Does a fourier transform of a complex signal
   
   // Take the transform of the real input signal
   ComplexVector z2 = fourier_transform (ComplexVector(in_re, in_im), dir);

   // Extract the real and imaginary parts
   re = z2.real();
   im = z2.imag();
   }

ComplexVector fourier_transform (
   const ComplexVector &sig,	// The input signal
   int dir		// Forward or backward
   )
   {
   // Does a fourier transform

   // Get the size of the vector
   int nn = sig.cdim();

   // Check that the size is a power of two
   while (nn % 2 == 0) nn /= 2;
   if (nn != 1)
      {
      error_message ("Fourier : Bad size of vector");
      return ComplexVector();
      }
   nn = sig.cdim();

   // Make an array for holding the result
   ComplexVector z2 (sig.cdim());

   // Copy the data
   int i;
   for_1Dindex (i, z2) z2[i] = sig[i];

   // Now carry out the FFT 
   four1 (z2.data(), 0, nn, dir);

   // Return the output signal
   return z2;
   }

