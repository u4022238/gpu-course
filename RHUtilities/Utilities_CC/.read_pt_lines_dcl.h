#ifndef _read_pt_lines_cc_dcl_
#define _read_pt_lines_cc_dcl_
#include "Utilities/cvar.h"


FUNCTION_DECL (int read_standard_input_file, (
   FILE *afile,			/* The file to read from */
   rhVector *&u,		/* U coordinates to be allocated and read */
   rhVector *&v,		/* V coordinates to be allocated and read */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates to be allocated and read */
   Boolean_matrix &defined	/* A matrix telling which points are defined */
   ));

FUNCTION_DECL (int select_points_from_specified_views, (

   /* Input */
   rhVector *&u,		/* U coordinates of all points in all views */
   rhVector *&v,		/* V coordinates of all points in all views */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates of all points */
   Boolean_matrix &defined,	/* A matrix telling which points are defined */
   Int_vector &selected_view,	/* A selected set of views */

   /* Output */
   rhVector *&uout,		/* U coordinates to be selected from u */
   rhVector *&vout,		/* V coordinates to be selected from v */
   rhVector &xout, rhVector &yout, 
   rhVector &zout,	/* Point coordinates to be selected from x, y, z */
   Int_vector &selected_point	/* The points that were selected */

   ));

FUNCTION_DECL (int select_points_from_specified_views_replace, (

   /* Input */
   rhVector *&u,		/* U coordinates of all points in all views */
   rhVector *&v,		/* V coordinates of all points in all views */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates of all points */
   Boolean_matrix &defined,	/* A matrix telling which points are defined */
   Int_vector &selected_view,	/* A selected set of views */
   Int_vector &selected_point	/* The points that were selected (returned) */
   )) ;

FUNCTION_DECL (int write_standard_input_file, (
   FILE *afile,			/* The file to read from */
   rhVector *&u,		/* U coordinates to be allocated and read */
   rhVector *&v,		/* V coordinates to be allocated and read */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates to be allocated and read */
   Boolean_matrix &defined	/* A matrix telling which points are defined */
   ));

#endif
