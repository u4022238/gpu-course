// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

#define USE_MEMCPY

//-----------------------------------------
//
//   Various 1D array classes follow
//
//-----------------------------------------

#include <stdio.h>
#include <Utilities/rh_util.h>

// #define REF_DEBUG

// Macro for indexing vectors
#ifndef for_1Dindex
#   define for_1Dindex(i, im)       \
   for (i = (im).low(); i <= (im).high(); i++)
#endif

#ifndef for1Dindex
#   define for1Dindex(i, im)        \
   for (int i = (im).low(); i <= (im).high(); i++)
#endif

class vector_type
   {
   private:

   int Xlow, Xhigh;
   int AllocXhigh;
   element_type *Val;
   unsigned int *reference_count;

#ifdef INLINE_VECTOR_ALLOC
   void dereference ()
      {
      if (reference_count)
         {
         // First unlink the thing
         *reference_count -= 1;

#ifdef REF_DEBUG
         fprintf (stderr, 
          "Reference count for vector %08x decreased to %d\n", 
          &(Val[0]), *reference_count);
#endif

         // Now, if reference count has fallen to zero, then free
         if (*reference_count <= 0)
            {
            // Free the values
            if (Val != (element_type *)0)
               GVFREE (Val, Xlow);

            // Delete the reference count
            delete reference_count;
#ifdef REF_DEBUG
            fprintf (stderr, "Freed vector %08x\n", &(Val[0]));
#endif
            }
         }

       // Set Val and reference count to initial values
       Val = (element_type *) 0;
       reference_count = (unsigned int *)0;
       Xlow = 0;
       Xhigh = AllocXhigh = -1;
       }

#else

   void dereference ();
#endif

   protected:

   public:

   void plugin ( int low, int high, element_type *val)
       {
       // Plugs in a different array and bounds.  The result
       // is an unreference-counted array.

       // First dereference, in case there was an old array
       dereference ();

       // Now put in the new bounds and data
       Xlow = low;
       AllocXhigh = Xhigh = high;
       Val = val;
       }

#if defined (__STDC__) || defined (WIN32)
#   define rhvec_ptr1(typ) typ##_sptr
#else
#   define rhvec_ptr1(typ) typ/**/_sptr
#endif
#define rhvec_ptr(a) rhvec_ptr1(a)

   typedef element_type * rhvec_ptr(vector_type);

   // For getting the bounds of the array
   element_type *begin() { return Val; }
   element_type *end  () { return Val + Xhigh + 1; }
   const element_type *cbegin() const 
      { return (const element_type *) Val; }
   const element_type *cend  () const
      { return (const element_type *) (Val + Xhigh + 1); }

   int low() const   { return Xlow; }
   int high() const  { return Xhigh; }
   int num_elements() const{ return Xhigh - Xlow + 1; }
   int dim         () const{ return Xhigh - Xlow + 1; }

   // For testing if an index is in bounds
   rhBOOLEAN defined_at (int i) const { return i >= Xlow && i <= Xhigh; }

   // For indexing into the array

#ifdef CHECKBOUNDS
   element_type & operator[] (int x) const
      {
      // First of all check the bounds
      if (x < Xlow || x > Xhigh)
         fprintf (stderr, "Index %d out of range [%d, %d]\n",
            x, Xlow, Xhigh);
      
      // Return the value in any case
      return Val[x];
      }
#else
   element_type & operator[] (int x) const {return Val[x];}
#endif

   // For getting a pointer to the array itself
   element_type *data () const { return Val; }

   // Basically the same thing, but using casting
   operator rhvec_ptr(vector_type) () const {return Val; }
   /* operator element_type * () const { return Val; } */

   // Constructors
   vector_type () 
      { 
      Xlow = 0;
      AllocXhigh = Xhigh = -1;
      Val = (element_type *) 0; 
      reference_count = (unsigned int *)0;
      }

   vector_type (int xl, int xh) 
      { 
      Val = (element_type *) 0;
      reference_count = (unsigned int *)0;
      Init (xl, xh); 
      }

   vector_type (int dim) 
      { 
      Val = (element_type *) 0;
      reference_count = (unsigned int *)0;
      Init (0, dim-1); 
      }

   vector_type (const vector_type &A)
      {
      // Copy constructor
      Val = (element_type *) 0;
      reference_count = (unsigned int *)0;
      *this = A;
      }

   bool is_null ()
      {
      if (Xlow == 0 && Xhigh == -1) return true;
      else return false;
      }


#ifdef INLINE_VECTOR_ALLOC

   vector_type &Init (int xl, int xh, int axh = -1)
      {
      // First dereference this array
      dereference ();

      // Now reallocate
      Xlow  = xl;
      Xhigh = xh;
      AllocXhigh = (axh < xh) ? xh : axh;
   
      Val = GVECTOR (Xlow, AllocXhigh, element_type);

      // Also the reference count
      reference_count = new unsigned int;
      *reference_count = 1;
#     ifdef REF_DEBUG
       fprintf (stderr, "Reference count for vector %08x set to 1\n", 
         &(Val[0]));
#     endif

      return *this;
      }

   void Resize (int axh)
      {
      // Moves the data into a larger space (or smaller space)
      AllocXhigh = axh;
      element_type *Valnew = GVECTOR (Xlow, AllocXhigh, element_type);

      // Adjust the range if necessary
      if (Xhigh > AllocXhigh) Xhigh = AllocXhigh;
   
      // Now, copy the data
      memcpy (Valnew, Val, (Xhigh - Xlow + 1) * sizeof (element_type));

      // Free the old version and replace by the new
      GVFREE (Val, Xlow);
      Val = Valnew;
      }

#else
   vector_type &Init (int xl, int xh, int axh = -1);

   void Resize (int xh);
#endif

   // Reallocate

   // Destructors
   void Dealloc () { dereference (); }
   ~vector_type () { dereference (); }

   // Assignment operator
   vector_type &operator = (const vector_type &avec)
      {
      // Increment the reference count
      if (avec.reference_count)
         {
         *(avec.reference_count) += 1;
#        ifdef REF_DEBUG
            {
            fprintf (stderr, 
               "Reference count for vector %08x increased to %d\n",
               &(avec.Val[0]), *(avec.reference_count));
            }
#        endif
         }

      // Dereference the current list
      dereference ();

      // Copy the fields
      Xlow = avec.Xlow;
      Xhigh = avec.Xhigh;
      AllocXhigh = avec.AllocXhigh;
      reference_count = avec.reference_count;
      Val = avec.Val;

      // Return the pointer itself
      return *this;
      }

   // Other actions
   vector_type &clear (element_type val)
      { 
      for (int i = Xlow; i <= Xhigh; i++)
          Val[i] = val;
      return *this;
      }

   vector_type &clear ()
      { 
#ifdef USE_MEMCPY
      int nbytes = (Xhigh - Xlow + 1) * sizeof(element_type);
      memset (&Val[0], 0, nbytes);
#else
      for (int i = Xlow; i <= Xhigh; i++)
          Val[i] = (element_type) 0;
#endif
      return *this;
      }

   vector_type copy () const
      {
      // Copy the array including the elements
      int i; 
      const vector_type &A = *this;  
      vector_type B (A.low(), A.high()); 
      for_1Dindex (i, B) B[i] = A[i];
      return B; 
      }

   // Pushback a new value
   vector_type &push_back (element_type val);

   // Finding maximum and minimum values
   element_type max () const;
   element_type min () const;
   double sum () const;
   double sumsq () const;
   double median () const;

   // Reading and writing from/to a file
   vector_type & read  (FILE *infile);
   vector_type & write (FILE *outfile);

   // For printing them out

#ifdef print_format
   void print (FILE *afile = stdout, const char *format = (char *)0) const
      {
      // Prints the vector
      if (format == (char *)0) format = print_format;
         int nrl = this->low(), nrh = this->high();
      const vector_type &a = *this;
      for (int i=nrl; i<=nrh; i++)
           fprintf (afile, format, a[i]);
      fprintf (afile, "\n");
      }

   void print_ifdebug(
         FILE *afile = stdout, const char *format = (char *)0) const
      {
      // Prints the vector
      if (debugging_messages_enabled())
         {
         int nrl = this->low(), nrh = this->high();
         const vector_type &a = *this;
         if (format == (char *)0) format = print_format;

         for (int i=nrl; i<=nrh; i++)
              fprintf (afile, format, a[i]);
         fprintf (afile, "\n");
         }
      }
#endif

#  undef rhvec_ptr
#  undef rhvec_ptr1
   };


//
// Declarations of vector routines
//

vector_type convolution (const vector_type &A, const vector_type &B);

