// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// Classes of geometric objects
//
#include <math.h>
#include <Utilities/rh_util.h>

class typ(point)	// For example Int_point
   {
   private:

   	data_type X, Y;
   
   public: 

	data_type x() const {return X;}
	data_type y() const {return Y;}
	data_type i() const {return Y;}
	data_type j() const {return X;}
	typ(point) & x(data_type xx) { X = xx; return *this; };
	typ(point) & y(data_type yy) { Y = yy; return *this; };
	typ(point) & j(data_type xx) { X = xx; return *this; };
	typ(point) & i(data_type yy) { Y = yy; return *this; };
	typ(point) (data_type x = 0, data_type y = 0) { X = x; Y = y; }
	typ(point) operator + (const typ(point) & pt2) const
	    		{ return typ(point) (X + pt2.x(), Y + pt2.y()); }
	typ(point) operator - (const typ(point) & pt2) const
	    		{ return typ(point) (X - pt2.x(), Y - pt2.y()); }
	double norm () const { return sqrt ((double) X*X + Y*Y); }
	double arg ()  const  { return atan2((double)Y, (double)X); }
	rhBOOLEAN operator != (const typ(point) & pt2) const
			{ return (X != pt2.X || Y != pt2.Y); }
	rhBOOLEAN operator == (const typ(point) & pt2) const
			{ return (X == pt2.X && Y == pt2.Y); }
	friend typ(point) operator - (const typ(point) &v);
	
   };

class typ(rectangle)	// For example Int_rectangle
   {
   private:

   	data_type Xlow, Xhigh;
   	data_type Ylow, Yhigh;

   public:

	// Reading rectangle bounds
	data_type xlow()  const { return Xlow; }
	data_type xhigh() const { return Xhigh; }
	data_type ylow()  const { return Ylow; }
	data_type yhigh() const { return Yhigh; }

	data_type jlow()  const { return Xlow; }
	data_type jhigh() const { return Xhigh; }
	data_type ilow()  const { return Ylow; }
	data_type ihigh() const { return Yhigh; }

	typ(point) top_left() const 
		{return typ(point) (Xlow, Ylow); }
	typ(point) bottom_right() const 
		{return typ(point) (Xhigh, Yhigh); }

	// Setting rectangle bounds
	typ(rectangle) & xlow (data_type xx) 
		{Xlow  = xx; return *this; }
	typ(rectangle) & xhigh(data_type xx) 
		{Xhigh = xx; return *this; }
	typ(rectangle) & ylow (data_type yy) 
		{Ylow  = yy; return *this; }
	typ(rectangle) & yhigh(data_type yy) 
		{Yhigh = yy; return *this; }

 	// Same things with a different name
	typ(rectangle) & jlow (data_type jj) 
		{Xlow  = jj; return *this; }
	typ(rectangle) & jhigh(data_type jj) 
		{Xhigh = jj; return *this; }
	typ(rectangle) & ilow (data_type ii) 
		{Ylow  = ii; return *this; }
	typ(rectangle) & ihigh(data_type ii) 
		{Yhigh = ii; return *this; }

	typ(rectangle) & top_left(typ(point) tl)
		{ Xlow = tl.x(); Ylow = tl.y(); return *this; }

	typ(rectangle) & bottom_right(typ(point) br)
		{ Xhigh = br.x(); Yhigh = br.y(); return *this; }
      
	// Constructors
	typ(rectangle) (const typ(point) tl, const typ(point) br)
		{ 
		Xlow = tl.x();
		Ylow = tl.y();
		Xhigh = br.x();
		Yhigh = br.y();
		}

	typ(rectangle) (const data_type lx = 0, const data_type ly = 0, 
			const data_type hx = 0, const data_type hy = 0)
		{
		Xlow = lx;
		Ylow = ly;
		Xhigh = hx;
		Yhigh = hy;
		}

	 // Allows us to transform easily between int and double rectangles
	 typ(rectangle) (const class Int_rectangle &ir);
	 typ(rectangle) (const class Double_rectangle &ir);

	 // Containment
	 rhBOOLEAN contains (double x, double y)
	    { return ( x >= Xlow && x <= Xhigh && y >= Ylow && y <= Yhigh); }

   };

// Union of two rectangles -- actually the rectangle that contains them
inline typ(rectangle) rectangle_union (
	const typ(rectangle) &p1, const typ(rectangle) &p2)
   {
   return typ(rectangle) (
	rhMin(p1.jlow(),  p2.jlow()),
	rhMin(p1.ilow(),  p2.ilow()),
	rhMax(p1.jhigh(), p2.jhigh()),
	rhMax(p1.ihigh(), p2.ihigh()));
   }

class typ(WeightedPoint)	// For example Int_point
   {
   private:

   	data_type X, Y;
	double Weight;
   
   public: 

	data_type x() const {return X;}
	data_type y() const {return Y;}
	double weight() const {return Weight;}
	typ(WeightedPoint) & x(data_type xx) { X = xx; return *this; };
	typ(WeightedPoint) & y(data_type yy) { Y = yy; return *this; };
	typ(WeightedPoint) & weight(double w) { Weight = w; return *this; };
	typ(WeightedPoint) (data_type x = 0, data_type y = 0, double w = 1.0) 
		{ X = x; Y = y; Weight = w; }
	typ(WeightedPoint) (typ(point) apt, double w = 1.0) 
		{ X = apt.x(); Y = apt.y(); Weight = w; }
	typ(point) point() const { return typ(point) (X, Y); }
   };
