// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G% 
// Levenberg Marquardt classes 

#ifndef _Levenberg_sparse_h_
#define _Levenberg_sparse_h_

#include <Utilities_CC/utilities_CC.h>
#include <signal.h>
#include <vector>
using namespace std;

class Levenberg_sparse
   {
   // Some local variables 
   protected :

      // Minimization parameters
      rhVector cparam;
      rhVector *xparam;
      int numpoints;

      int Num_Loops;
      double Min_Incr;

      double lambda;	   // augmentation factor

      int pointrank_;	// This is an estimate of number of independent 
                        // params for points

      double rms_;	   // The RMS error

      // Routines for handling an interrupt to stop after next iteration
      static rhBOOLEAN interrupt_pending;
      static void keybd_interrupt (int errno);

      // String for informative message
      char junkstring[256];

   public : 

      // To be carried out when a new value of the parameters is found
      virtual void new_position_action 
		   (rhVector &cparam, rhVector *xparam, int numpoints);

      // Set parameters
      void setNumLoops (int n) { Num_Loops = n; }
      void setMinIncr (double incr) { Min_Incr = incr; }
      void setLambda (double lam) { lambda = lam; }
      void setPointrank (int rank) { pointrank_ = rank; }
      double getLambda () const { return lambda; }

      // Set the values of the input parameters
      void setParameters (rhVector &cp, rhVector *xp, int numpoints);
      void getParameters (rhVector &cp, rhVector **xp, int *numpoints) const;

      // Same interface except with vectors
      void setParameters (
		   rhVector &cp, 
		   const vector<rhVector> &xp
         );

      void getParameters (
		   rhVector &cp, 
		   vector<rhVector> &xp 
		   ) const;

      double get_rms_error () { return rms_; }

      // Constructors, etc
      Levenberg_sparse ();
      virtual ~Levenberg_sparse();

      // Solve the problem
      double solve ();

      // The function to be minimized -- must be supplied in subclass
      virtual rhVector function ( 
	      int i, 
         const rhVector &cparams, 
         const rhVector &xparam ,
         bool new_cparams = true
         ) = 0;

      rhVector total_function ( 
	      const rhVector &cparams, rhVector *xparams, int numpoints);

      // Compute the cost of a given increment
      double cost_of_increment (rhVector cdel, rhVector *xdel, double factor);

      // Also, the error in the camera parameters
      virtual rhVector parameter_error (const rhVector &cparams);

      // Compute the jacobian
      virtual void jcam (
         vector<rhMatrix> &A, rhVector &cparam, rhVector *xparam );
      virtual void jx   ( 
         vector<rhMatrix> &B, rhVector &cparam, rhVector *xparam );
      virtual rhMatrix jcam0 (rhVector &cparam);

      // Compute the incremented values of the parameters

      void compute_temporaries (
         vector<rhMatrix> &AA,
         vector<rhMatrix> &BB,
	      rhMatrix J0,
	      rhVector *epsilon,   // Inputs
	      rhVector epsilon0,	// Camera parameter error
	      int numpoints,

	      // Values to be returned
	      rhMatrix *BTB,
	      rhMatrix *ATB,
	      rhMatrix &N,
	      rhVector &b
         );

      void compute_increments (
         vector<rhMatrix> &BB,
	      const rhVector *epsilon,      // Inputs
	      const rhMatrix *BTB,
	      const rhMatrix *ATA,
	      const rhMatrix &Nin,
	      const rhVector &b,
	      int numpoints,
	      double lambda, 	// Regularization factor
	      rhVector &cdel,	// Camera parameter increments
	      rhVector *xdel		// Point increments
         );

      // Compute the covariance matrix 
      rhMatrix normal_matrix ();
      rhMatrix covarianceMatrix (rhVector &mparam);

   protected :

	   void do_line_search (
		   rhVector cdel,
 		   rhVector *xdel,	// Direction in which to seek
		   double *pfactor,	// Factor of this increment to use
		   double *oldchisq,
		   rhVector &old_dy,	// Old function value
		   double *pnewchisq,// Value of cost at new position
		   rhVector &new_dy	// New function value
   		);

	   double cost_of_increment ( 
		   rhVector cdel,
 		   rhVector *xdel,	// Direction in which to seek
		   double factor,		// How far to go in this direction
		   rhVector &dy
		   );

	   void print_increment_debug_info (
		   rhVector cdel,
 		   rhVector *xdel,		// Direction in which to seek
		   double factor,		// Factor of this increment to use
		   rhVector old_dy,
		   rhVector new_dy		// The error vectors
   		) const;

	   void write_jacobians (rhMatrix *A, rhMatrix *B) const;

   private :
      // Message output
      // To use in MATLAB, overload this in a derived class and use mexPrintf
      virtual void informative_message (const char *str);

   };


// A useful function
rhVector concatenate_rhVectors (rhVector *v, int numv);

#endif
