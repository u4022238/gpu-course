// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

#ifndef array3_h
#define array3_h

#include <Utilities/rh_util.h>

#ifdef GNU_COMPILER
#include <std/complex.h>
#endif

//-----------------------------------------
//
//   Various matrix classes follow
//
//-----------------------------------------

#define array3_type Double_array3D
#define element_type double
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Int_array3D
#define element_type int
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Long_array3D
#define element_type long
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Unsigned_short_array3D
#define element_type unsigned short   
#       include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Short_array3D
#define element_type short
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Unsigned_char_array3D
#define element_type unsigned char
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Float_array3D
#define element_type float
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#ifdef GNU_COMPILER
#define array3_type Complex_float_array3D
#define element_type float_complex
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type

#define array3_type Complex_double_array3D
#define element_type double_complex
#	include <Utilities_CC/Array3.h>
#undef array3_type
#undef element_type
#endif


/* A few other aliases for different types */
typedef Unsigned_char_array3D Boolean_array3D;

#endif

