// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// This gives an object-oriented InverseComposition-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector InverseComposition::function(const rhVector &);
//

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Timer2.h>
#include <Utilities_CC/InverseComposition.h>

InverseComposition::InverseComposition () : Num_Loops (100), Min_Loops (1), Min_Incr (1.0e-2)
   {}

InverseComposition::~InverseComposition () 
   {}

//
// Keyboard interrupt stuff
//


rhBOOLEAN InverseComposition::interrupt_pending = FALSE;

#ifndef WIN32
void InverseComposition::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
      interrupt_pending = TRUE; 
   }
#endif

rhMatrix InverseComposition::jacobian ( const rhVector &mparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (mparam);

   // Declare the jacobian matrix
   rhMatrix J (oldval.dim(), mparam.dim());

   // Now change the parameters one after another 
   for1Dindex (j, mparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = mparam[j];

      // Increment the j-th parameter 
      double delta = FAC * mparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      mparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (mparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      J.setcolumn (j, del);

      // Restore the value of mparam[j] 
      mparam[j] = oldparam;
      }

   // Return the matrix
   return J;
   }

void InverseComposition::new_position_action ( rhVector )
   { /* Default is to do nothing */ }

double InverseComposition::solve ()
   {
   // Carry out iterative updates
   rhTimer2 t("InverseComposition::solve()");

   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ InverseComposition::keybd_interrupt);
#endif

   // Compute the Jacobian  -- once only
   rhMatrix J = jacobian (mparam);

// #define WRITE_JACOBIAN
#  ifdef WRITE_JACOBIAN
   fprintf (stdout, "\nJacobian (%d x %d) : \n", 
      J.idim(), J.jdim());
   J.print (stdout, "%11.7f ");
   fprintf (stdout, "\n JTJ (%d x %d) : \n", J.jdim(), J.jdim());
   (J.transpose() * J).print(stdout, "%9.5f ");
#  endif

   debugging_message ("InverseComposition::solve: A\n");

   // Calculate the normal equations
   rhMatrix J_trans = J.transpose();
   rhMatrix N = (J_trans*J).inverse() * J_trans;

   debugging_message ("InverseComposition::solve: B\n");

   // Evaluate the function
   rhVector goal = getgoal();
   rhVector fval = function (mparam);
   rhVector residual   = goal - fval;
   double cost = cost_function (residual);

#if 0
   fprintf (stdout, "InverseComposition::solve()\n");
   fprintf (stdout, "goal\n");
   goal.print();
   fprintf (stdout, "fval\n");
   fval.print();
   fprintf (stdout, "residual\n");
   residual.print();
   fprintf (stdout, "cost = %f\n", cost);
#endif

   // Output some debugging info
   informative_message ("\n\tInitial error : cost = %g, rms = %g", 
      cost, sqrt(cost/residual.dim()));

   // Now, iterate
   bool improvement = TRUE;
   for (int icount=1; icount<=Num_Loops && improvement; icount++)
      {
      // Inner loop of changing alpha (damping)
      double increment_ratio;
      double alamda = 1.0;

      do {
         //----------------------------------------
         //  Calculate the update params
         //----------------------------------------
         rhVector newparams = alamda * (N * residual);

         // Compute the error vector at this value
         fval = function (newparams);
         rhVector newresidual = goal - fval;
         double newcost = cost_function (newresidual);

         // Compute the proportional change in the residual
            {
            rhVector delta = newresidual-residual;
            double len_delta = sqrt(delta*delta);
            double len_residual = sqrt (residual*residual);
            increment_ratio = len_delta / len_residual;
            }

         // Accept or reject it 
         if (newcost < cost)
            {
            // Success, accept this new solution 
            alamda = 1.0;
            residual = newresidual;
            cost = newcost;

            // Accept the new parameter values 
            mparam = newparams;

            // Call any user function 
            new_position_action (mparam);

            // Signal improvement 
            improvement = TRUE;
            }
         else
            {
            // Failure, increas alamda and return. 
            alamda *= 0.25;
            improvement = FALSE;
            }
         } while ( ! improvement && increment_ratio >= Min_Incr 
            && (alamda < 1e10));

      // Output some debugging info 
      informative_message ("\talamda = %5.0e, cost = %g, rms = %g", 
         alamda, cost, sqrt(cost/residual.dim()));

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if the increment_ratio has been too small 
      if (icount >= Min_Loops && 
         (increment_ratio < Min_Incr || alamda >= 1e10))
         break;
      }

   // Return the value of the error 
   return cost;
   }

rhMatrix InverseComposition::covarianceMatrix (const rhVector &mparam)
   {
   rhMatrix J = jacobian (mparam);
   rhMatrix C = J.transpose() * J;
   return C;
   }
