// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G% 
// Powell Minimizer classes 

#ifndef _Powell_h_
#define _Powell_h_

#include <Utilities_CC/utilities_CC.h>
#include <signal.h>

class Powell
   {
   // Some local variables 
   protected :

      // Holds the parameters and their increments
      rhVector p_;
      rhMatrix xi_;

      int Num_Loops;	// Do no more than this number of loops
      int Min_Loops;	// Must do at least this many loops
      double Accuracy;  // Required precision of convergence

      // To be carried out when a new value of the parameters is found
      virtual void new_position_action ( rhVector mparam );

      // Routines for handling an interrupt to stop after next iteration
      static rhBOOLEAN interrupt_pending;
      static void keybd_interrupt (int errno);

   public : 

      // Set parameters
      void setNumLoops (int n) { Num_Loops = n; }
      void setMinLoops (int n) { Min_Loops = n; }
      void setAccuracy (double incr) { Accuracy = incr; }

      // Set the values of the input parameters
      void setParameters (const rhVector &param) { p_ = param.copy(); }
      rhVector getParameters () const { return p_.copy(); }

      // Constructors, etc
      Powell ();
      virtual ~Powell();

      // Solve the problem
      double solve ();

      // The function to be minimized -- must be supplied in subclass
      virtual double function ( const rhVector &mparams ) = 0;
      virtual void linmin (const rhVector p, const rhVector xi, double *fval) 
		const = 0;

      // Compute the covariance matrix 
      rhMatrix covarianceMatrix (const rhVector &mparam);
   };

#endif
