// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// This gives an object-oriented Levenberg-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector Levenberg_sparse::function(const rhVector &);
//

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Levenberg_sparse.h>

// #define rhDEBUG

static FILE *debugfile = NULL;

Levenberg_sparse::Levenberg_sparse () : 
xparam (NULL), Num_Loops (100), Min_Incr (1.0e-2), lambda(1.0e-3)
   {}

Levenberg_sparse::~Levenberg_sparse () 
   {
   if (xparam) delete [] xparam;
   }

//
// Keyboard interrupt stuff
//

rhBOOLEAN Levenberg_sparse::interrupt_pending = FALSE;

#ifndef WIN32
void Levenberg_sparse::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
      interrupt_pending = TRUE; 
   }
#endif

void Levenberg_sparse::jx (
         vector<rhMatrix> &B, rhVector &cparam, rhVector *xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   B.resize(numpoints);

   // Now change the parameters one after another 
   for (int i=0; i<numpoints; i++)
      {
      // Compute the present value of the error
      rhVector oldval = function (i, cparam, xparam[i], i==0);

      // Allocate the Jacobian
      B[i].Init(oldval.dim(), xparam[i].dim());

      // Now, change the parameters one by one
      for1Dindex (j, xparam[i])
         {
         // Remember the value of the j-th parameter 
         double oldparam = xparam[i][j];

         // Increment the j-th parameter 
         double delta = FAC * xparam[i][j];
         if (fabs (delta) < DELTA) delta = DELTA;
         xparam[i][j] += delta;

         // Now recompute the function 
         bool new_cparams = false;
         rhVector newval = function (i, cparam, xparam[i], new_cparams);
   
         // Now fill out the jacobian values 
         rhVector del = (newval - oldval) * (1.0/delta);
         B[i].setcolumn (j, del);

         // Restore the value of xparam[i][j] 
         xparam[i][j] = oldparam;
         }
      }
   }

void Levenberg_sparse::jcam ( 
         vector<rhMatrix> &A,
         rhVector &cparam, 
         rhVector *xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   A.resize(numpoints);
   vector <rhVector> oldval(numpoints);
   for (int i=0; i<numpoints; i++)
      {
      // Compute the initial function value
      oldval[i] = function (i, cparam, xparam[i], i==0);

      // Also, allocate the Jacobian
      A[i].Init(oldval[i].dim(), cparam.dim());
      }

   // Now change the parameters one after another 
   for1Dindex (j, cparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = cparam[j];

      // Increment the j-th parameter 
      double delta = FAC * cparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      cparam[j] += delta;

      for (int i=0; i<numpoints; i++)
         {
         // Now recompute the function 
         rhVector newval = function (i, cparam, xparam[i], i==0);

         // Now fill out the jacobian values 
         rhVector del = (newval - oldval[i]) * (1.0/delta);
         A[i].setcolumn (j, del);
         }

      // Restore the value of cparam[j] 
      cparam[j] = oldparam;
      }
   }

rhVector Levenberg_sparse::parameter_error (const rhVector &cparam)
   {
   rhVector nm;
   return nm;
   }

rhMatrix Levenberg_sparse::jcam0 (rhVector &cparam)
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = parameter_error (cparam);

   // See if this is null (no parameter errors used)
   if (oldval.is_null())
      return rhMatrix(0, cparam.dim());

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), cparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, cparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = cparam[j];

      // Increment the j-th parameter 
      double delta = FAC * cparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      cparam[j] += delta;

      // Now recompute the function 
      rhVector newval = parameter_error (cparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);

      // Restore the value of cparam[j] 
      cparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

void Levenberg_sparse::new_position_action ( rhVector &, rhVector *, int )
   { /* Default is to do nothing */ }

rhVector concatenate_rhVectors (rhVector *v, int numv)
   {
   // Concatenates a bunch of vectors into one
   int i, j;
   int length;   // Total length of vectors

   // Compute the total length
   for (i=0, length=0; i<numv; i++) length += v[i].dim();

   // Now allocate a vector
   rhVector vall (length);

   // Fill it out
   int count = 0;
   for (i=0; i<numv; i++)
      for_1Dindex (j, v[i])
      vall[count++] = v[i][j];

   // Return the concatenated vector
   return vall;
   }

rhVector Levenberg_sparse::total_function (
   const rhVector &cparam, rhVector *xparam, int numpoints)
   {
   // The total concatenated set of functions

   // First of all, just run the separate functions
   rhVector *v = new rhVector [numpoints];

   // Run the separate functions
   bool new_cparams = true;
   for (int i=0; i<numpoints; i++)
      {
      v[i] = function (i, cparam, xparam[i], new_cparams);
      new_cparams = false;
      }

   // Concatenate
   rhVector vall = concatenate_rhVectors (v, numpoints);

   // Free the function values
   delete [] v;

   // Return the function vector
   return vall;
   }

static void augment (rhMatrix &M, double lambda)
   {
   // Augment diagonal elements of M, assumed square
   for (int j=M.ilow(); j<=M.ihigh(); j++)
      M[j][j] *= 1.0 + (lambda);
   }

void Levenberg_sparse::compute_temporaries (
      vector<rhMatrix> &AA,
      vector<rhMatrix> &BB,
      rhMatrix J0,
      rhVector *epsilon,      // Inputs
      rhVector epsilon0,	// Camera parameter error
      int numpoints,

      // Values to be returned
      rhMatrix *BTB,
      rhMatrix *ATB,
      rhMatrix &N,
      rhVector &b
      )
   {
   // Computes the increments

   // Get the dimension of the normal matrix
   int dim = AA[0].jdim();

   // If the J0 is defined, then we need to initialize N and b
   if (J0.is_null())
      {
      N.Init(dim, dim);
      N.clear(0.0);

      b.Init(dim);
      b.clear(0.0);
      }
   else
      {
      // Initialize using the camera parameter increments
      N = J0.transpose() * J0;
      b = J0.transpose() * epsilon0;
      }

   // Now, fill it
   for (int i=0; i<numpoints; i++)
      {
      // Take aliases for A and B
      rhMatrix A = AA[i];
      rhMatrix B = BB[i];
      rhMatrix AT = A.transpose();
      rhMatrix BT = B.transpose();

      // First compute the values (B_i^T B_i)^-1
      BTB[i] = BT * B;
      ATB[i] = AT * B;

      // Increment N
      N = N + AT * A;

      // Now, the vector
      b = b + AT * epsilon[i];
      }
   }

void Levenberg_sparse::compute_increments (
      vector<rhMatrix> &BB,
      const rhVector *epsilon,   // Inputs
      const rhMatrix *BTB,
      const rhMatrix *ATB,
      const rhMatrix &Nin,
      const rhVector &bin,
      int numpoints,
      double lambda, 	         // Regularization factor
      rhVector &cdel,	         // Camera parameter increments
      rhVector *xdel		         // Point increments
      )
   {
   // Computes the increments

   // Make some storage for temporary matrices
   rhMatrix *BTBinv = new rhMatrix [numpoints];

   // Copy and augment
   rhMatrix N = Nin.copy();
   augment(N, lambda);
   rhVector b = bin.copy();

   // Now, fill it
   for (int i=0; i<numpoints; i++)
      {
      // First compute the values (B_i^T B_i)^-1
      rhMatrix BTBi = BTB[i].copy();
      augment (BTBi, lambda);
      BTBinv[i] = BTBi.inverse_or_nearinverse();

      // Put the whole thing together
      N = N - ATB[i] * BTBinv[i] * ATB[i].transpose();

      // Now, the vector
      rhMatrix BT = BB[i].transpose();
      b = b - ATB[i] * (BTBinv[i] * (BT * epsilon[i]));
      }

   // Matrix solution -- solution overwrites b
   if (debugging_messages_enabled())
      timed_message (stderr, "Levenberg_sparse: Solving normal equations\n");

   if (! (lin_solve_symmetric_0(N, b, b, N.idim())))
      {
      warning_message ("LevenbergSparse::Symmetric solution method failed");

      // Try SVD-based method
      if (! solve_simple_0 (N, b, b, N.idim(), N.idim()))
         {
         error_message ("Can not solve normal equations -- exiting");
         bail_out (2);
         }
      }

   // b is the delta to cameras
   cdel = b;

   // Back substitute to get the values of all the parameters
   for (int i=0; i<numpoints; i++)
      {
      // Compute it in transposed form
      rhVector temp = epsilon[i] * BB[i] - cdel * ATB[i];
      xdel[i] = temp * BTBinv[i];
      }

   // Now free the temporaries
   delete [] BTBinv;

   if (debugging_messages_enabled())
      timed_message (stderr, "Levenberg_sparse: Finished solving equations\n");
   }

double Levenberg_sparse::solve ()
   {
   // Declare variables 
   int i;
   double chisq = 0.0;
   // double lambda = 1.0e-3;		// This is now a class member
   rhBOOLEAN improvement = TRUE;


   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ Levenberg_sparse::keybd_interrupt);
#endif

   // Set up some needed data structures
   rhVector *epsilon    = new rhVector [ numpoints ];
   rhVector *new_xparam = new rhVector [ numpoints ];
   rhVector *xdel       = new rhVector [ numpoints ];
   vector<rhMatrix> A;
   vector<rhMatrix> B;
   rhMatrix *BTB 	= new rhMatrix [ numpoints ];
   rhMatrix *ATB 	= new rhMatrix [ numpoints ];
   rhVector new_cparam;
   rhVector cdel;

   // Also structures to handle error-measurements in camera parameters
   rhMatrix J0;
   rhVector epsilon0;
   rhVector dy;

   // Now iterate 
   for (int icount=0; icount<Num_Loops && improvement; icount++)
      {
      double increment_ratio;

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "Levenberg_sparse: Setting up the minimization equations\n");

      // Compute the function value 
      bool new_cparams = true;
      for (i=0; i<numpoints; i++)
         {
         epsilon[i] = function (i, cparam, xparam[i], new_cparams);
         new_cparams = false;
         }

      // Compute an initial value of the error 
      dy = concatenate_rhVectors (epsilon, numpoints);
      chisq = dy * dy;

      // Output some debugging info 
      if (icount == 0)
         {
         sprintf (junkstring, "\n\tInitial error : chisq = %g, rms = %g\n", 
            chisq, sqrt(chisq/dy.dim()));
         informative_message (junkstring);
         }

      // Compute the Jacobians
      jcam (A, cparam, xparam);
      jx   (B, cparam, xparam);

      // Also the one for the camera parameters
      J0 = jcam0 (cparam);
      epsilon0 = parameter_error (cparam);

      // Make some storage for temporary matrices
      rhMatrix N;
      rhVector b;

      // Compute the temporaries
      compute_temporaries (A, B, J0, epsilon, epsilon0, numpoints, BTB,
         ATB, N, b);

      // Now estimate new position with different lambda until improvement 
      do {
         //---------------------------------------
         // Compute the increments
         //---------------------------------------
         compute_increments (
            B, epsilon, BTB, ATB, N, b, numpoints, lambda, cdel, xdel);

         // Increment the parameters
         new_cparam = cparam - cdel;
         for (i=0; i<numpoints; i++) 
            new_xparam[i] = xparam[i] - xdel[i];

         // Evaluate the new parameters
         rhVector newdy = total_function (new_cparam, new_xparam, numpoints);
         double newchisq = newdy * newdy;

         // Compute the proportional change in the error vector
            {
            rhVector del = newdy-dy;
            double len_del = sqrt(del*del);
            double len_dy = sqrt (chisq);
            increment_ratio = len_del / len_dy;
            }

         // Accept or reject it 
         if (newchisq < chisq)
            {
            // Success, accept this new solution 
            lambda *= 0.1;
            chisq = newchisq;

            // Accept the new parameter values 
            cparam = cparam - cdel;
            for (i=0; i<numpoints; i++)
               xparam[i] = xparam[i] - xdel[i];

            // Call any user function 
            new_position_action (cparam, new_xparam, numpoints);

            // Signal improvement 
            improvement = TRUE;
            }
         else
            {
            // Failure, increas lambda and return. 
            lambda *= 10.0;
            improvement = FALSE;
            }
         } while ( ! improvement && increment_ratio >= Min_Incr 
                  && (lambda < 1e10));

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if the increment_ratio has been too small 
      if (increment_ratio < Min_Incr || lambda >= 1e10) break;

      // Output some debugging info 
      sprintf (junkstring, "\tlambda = %5.0e, chisq = %g, rms = %g\n", 
         lambda, chisq, sqrt(chisq/dy.dim()));
      informative_message (junkstring);
      }

   // Output some debugging info 
   sprintf (junkstring, "\tFinal error    : chisq = %g, rms = %g\n", 
      chisq, sqrt(chisq/dy.dim()));
   informative_message (junkstring);

   // Free the temporaries
   delete [] epsilon;
   delete [] new_xparam;
   delete [] xdel;
   delete [] BTB;
   delete [] ATB;

   // Return the value of the error 
   return chisq;
   }

rhMatrix Levenberg_sparse::covarianceMatrix (rhVector &mparam)
   {
   // This is not correct right now
   rhMatrix J; return J;
   }

void Levenberg_sparse::setParameters (rhVector &cp, rhVector *xp, int npts) 
   {
   // Creates the data
   numpoints = npts;

   // Set up the new parameters
   if (xparam) delete [] xparam; 
   xparam = new rhVector [numpoints];
   for (int i=0; i<numpoints; i++)
      xparam[i] = xp[i].copy();

   // And copy the camera parameters
   cparam = cp.copy();
   }

void Levenberg_sparse::getParameters (
      rhVector &cp, 
      rhVector **xp, 
      int *npts) const
   {
   // Initialize the xp parameters
   *xp = new rhVector [numpoints];

   // Copy them
   cp = cparam.copy();
   for (int i=0; i<numpoints; i++)
      (*xp)[i] = xparam[i].copy();

   // Return the number of points
   *npts = numpoints;
   }

void Levenberg_sparse::setParameters (
      rhVector &cp, 
      const vector<rhVector> &xp
      )
   {
   // Creates the data
   numpoints = xp.size();

   // Set up the new parameters
   if (xparam) delete [] xparam; 
   xparam = new rhVector [numpoints];
   for (int i=0; i<numpoints; i++)
      xparam[i] = xp[i].copy();

   // And copy the camera parameters
   cparam = cp.copy();
   }

void Levenberg_sparse::getParameters (
      rhVector &cp, 
      vector<rhVector> &xp
      ) const
   {
   // Initialize the xp parameters
   xp.resize(numpoints);

   // Copy them
   cp = cparam.copy();
   for (int i=0; i<numpoints; i++)
      xp[i] = xparam[i].copy();
   }

rhMatrix Levenberg_sparse::normal_matrix ()
   {
   // Returns the normal matrix (used for computing the covariance of
   // the camera parameters.

   // First declare the matrix
   rhMatrix N;

   // Compute the Jacobians
   vector<rhMatrix> A;  // One matrix for each point
   vector<rhMatrix> B;
   jcam (A, cparam, xparam);
   jx   (B, cparam, xparam);

   // Now, fill it
   for (int i=0; i<numpoints; i++)
      {
      // First compute (B_i^T B_i)^-1
      rhMatrix BTBinv = (B[i].transpose()*B[i]).inverse_or_nearinverse();

      // Now, ATA and ATB
      rhMatrix AT = A[i].transpose();
      rhMatrix ATA = AT * A[i];
      rhMatrix ATB = AT * B[i];

      // Put the whole thing together
      rhMatrix Ni = ATA - ATB * BTBinv * ATB.transpose();

      // Now, sum them
      if (i == 0) N = Ni;
      else N = N + Ni;
      }

   // Return the normal matrix
   return N;
   }

#include <stdarg.h>

void Levenberg_sparse::informative_message (const char* str)
   {
   printf (str);
   }

