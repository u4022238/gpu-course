// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


#include <Utilities_CC/utilities_CC.h>

#ifdef GNU_COMPILER
#include <std/complex.h>
#endif

//-----------------------------------------
//
//   Various matrix classes follow
//
//-----------------------------------------

#define matrix_type Double_matrix
#define vector_type Double_vector
#define element_type double
#define ptrcmp double_ptrcmp_ascending
#define print_format "%15.6e"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Int_matrix
#define vector_type Int_vector
#define element_type int
#define ptrcmp int_ptrcmp_ascending
#define print_format " %d"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Long_matrix
#define vector_type Long_vector
#define element_type long
#define print_format " %d"
#define ptrcmp long_ptrcmp_ascending
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Unsigned_short_matrix
#define vector_type Unsigned_short_vector
#define element_type unsigned short   
#define print_format " %5d"
#define ptrcmp unsigned_short_ptrcmp_ascending
#       include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Short_matrix
#define vector_type Short_vector
#define element_type short
#define print_format " %6d"
#define ptrcmp short_ptrcmp_ascending
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Unsigned_char_matrix
#define vector_type Unsigned_char_vector
#define element_type unsigned char
#define print_format " %3d"
#define ptrcmp unsigned_char_ptrcmp_ascending
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#define matrix_type Char_matrix
#define vector_type Char_vector
#define element_type char
#define ptrcmp char_ptrcmp_ascending
#define print_format " %4d"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp


#define matrix_type Float_matrix
#define vector_type Float_vector
#define element_type float
#define ptrcmp float_ptrcmp_ascending
#define print_format "%15.6e"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#undef ptrcmp

#ifdef GNU_COMPILER
#define matrix_type Complex_float_matrix
#define vector_type Complex_float_vector
#define element_type float_complex
#define print_format "%15.6e"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format

#define matrix_type Complex_double_matrix
#define vector_type Complex_double_vector
#define element_type double_complex
#define print_format "%15.6e"
#	include <Utilities_CC/genmatrix.cc>
#	include <Utilities_CC/genvector.cc>
#undef matrix_type
#undef vector_type
#undef element_type
#undef print_format
#endif

#if 0
//-----------------------------------------
//
//   Various vector classes follow
//
//-----------------------------------------

#define vector_type Float_vector
#define vector_type Float_vector
#define element_type float
#define print_format "%15.6e"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Double_vector
#define element_type double
#define print_format "%15.6e"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#ifdef GNU_COMPILER
#define vector_type Complex_double_vector
#define element_type double_complex
#define print_format "%15.6e"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Complex_float_vector
#define element_type float_complex
#define print_format "%15.6e"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format
#endif

#define vector_type Int_vector
#define element_type int
#define print_format " %d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Long_vector
#define element_type long
#define print_format " %d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Unsigned_short_vector
#define element_type unsigned short
#define print_format " %d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Short_vector
#define element_type short
#define print_format " %d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Unsigned_long_vector
#define element_type unsigned long
#define print_format " %d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Unsigned_char_vector
#define element_type unsigned char
#define print_format "%3d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Char_vector
#define element_type char
#define print_format "%4d"
#	include <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Double_point_vector
#define element_type Double_point
#define print_format "%15.6e"
#include        <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#define vector_type Pointer_vector
#define element_type void *
#define print_format "%15.6e"
#include        <Utilities_CC/genvector.cc>
#undef vector_type
#undef element_type
#undef print_format

#endif
