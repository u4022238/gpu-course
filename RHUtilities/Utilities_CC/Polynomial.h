// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
//

#ifndef _rhPolynomial_h_
#define _rhPolynomial_h_

// Routines for matrices
#include <Utilities_CC/Matrix.h>

class rhPolynomial : public rhVector
   {
   public :

        static void (*Error_Handler) ();
	
	rhPolynomial () {};

	rhPolynomial (int m) : rhVector (m) {}

	rhPolynomial (double a) : rhVector (1) { (*this[0]) = a;}

	rhPolynomial (double a, double b) : rhVector (a, b) {}

	rhPolynomial (double a, double b, double c) : rhVector (a, b, c) {}

	rhPolynomial (double a, double b, double c, double d) : 
		rhVector (a, b, c, d) {}

   	rhPolynomial (int m, double *vals) : rhVector (m, vals) {}

   	rhPolynomial (const rhVector &b) : rhVector (b) {}

	// rhPolynomial copy () const;		// Copy of the vector

	int degree() const { return num_elements() - 1; }

	// Operators on polynomials
	rhPolynomial operator * (const rhPolynomial &b) const;
	rhPolynomial operator + (const rhPolynomial &b) const ;
	rhPolynomial operator - (const rhPolynomial &b) const ;
	rhPolynomial operator + (double b) const ;
	rhPolynomial operator - (double b) const ;
	double eval (double x) const ;
	rhPolynomial operator ^ (int n) const ;
   	rhVector real_roots();
   	rhPolynomial derivative();

	friend rhPolynomial operator + (double x, const rhPolynomial &v); 
	friend rhPolynomial operator - (double x, const rhPolynomial &v); 
   };


#endif



	

