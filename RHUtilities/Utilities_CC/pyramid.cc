// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)pyramid.cc	1.5 11/27/96
#include <Utilities_CC/utilities_CC.h>
#include <vector>

// Code for making pyramids

static double ww[] = { 0.05, 0.25, 0.4, 0.25, 0.05 };

static Double_matrix weight_matrix()
   {
   // Initialize the matrix of weights 

   // First allocate the matrix 
   Double_matrix w(-2, 2, -2, 2);

   // Fill it out
   int i, j;
   for (i = -2; i <= 2; i++)
      for (j = -2; j <= 2; j++)
         w[i][j] = ww[i + 2] * ww[j + 2];

   // Return it
   return w;
   }

static Pyramid_Image_type enlarge_matrix(
   Pyramid_Image_type &A,		// Array to get boundary 
   int n			// Width of the boundary 
)
   {
   // Puts a boundary on the array n values wide.  It is assumed that the
   // array is of a size appropriate to take it 
   register int i, j;
   register int idm1 = A.idim() - 1;
   register int jdm1 = A.jdim() - 1;

   // Declare an array to return and copy A to it
   Pyramid_Image_type B(-n, idm1 + n, -n, jdm1 + n);
   for_2Dindex(i, j, A) B[i][j] = A[i][j];

   // Put on left and right boundaries 
   for (i = 1; i <= n; i++)
      for (j = 0; j <= jdm1; j++)
         {
         B[-i][j] = 2 * B[0][j] - B[i][j];
         B[idm1 + i][j] = 2 * B[idm1][j] - B[idm1 - i][j];
         }

   // Now the top and bottom boundaries 
   for (i = -n; i <= idm1 + n; i++)
      for (j = 1; j <= n; j++)
         {
         B[i][-j] = 2 * B[i][0] - B[i][j];
         B[i][jdm1 + j] = 2 * B[i][jdm1] - B[i][jdm1 - j];
         }

   // Return the expanded matrix
   return B;
   }

Pyramid_Image_type reduce(Pyramid_Image_type &A)
   {
   // Does one level of pyramid reduction 
   // We assume that the image starts with indices at zero

   register int i, j;
   int Aidim = A.idim(), Ajdim = A.jdim();
   int Ajhigh = A.jhigh(), Aihigh = A.ihigh();
   int Bidim = Aidim / 2 + 1, Bjdim = Ajdim / 2 + 1;

   // Allocate B
   Pyramid_Image_type B(0, Bidim - 1, 0, Bjdim - 1);

   // Get the weights.
   // So as to avoid one multiplication we divide by ww[2] for horizontal
   // filtering, and multiply by it for vertical filtering.
   double wj0 = ww[0] / ww[2], wj1 = ww[1] / ww[2];
   double wi0 = ww[0] * ww[2], wi1 = ww[1] * ww[2], wi2 = ww[2] * ww[2];

   // Also, we need a buffer to keep filtered values in the j direction
   Pyramid_vector_type buf(-2, Ajhigh + 3);

   // Now carry out the reduction step 
   for (i = 0; i < Bidim; i++)
      {
      // Get the indices for the 5 elements required in the row
      Pixel_type *A0 = A[2 * i - 2];
      Pixel_type *A1 = A[2 * i - 1];
      Pixel_type *A2 = A[2 * i];
      Pixel_type *A3 = A[2 * i + 1];
      Pixel_type *A4 = A[2 * i + 2];

      // Adjust for end conditions - the strategy is to
      // repeat the image reflected at the edges.
      //
      // Note that 
      //    Bidim = Aidim/2 + 1, so 
      //    2*(Bidim-1) <= Aidim = Aihigh + 1  thus
      //    2*i <= Aihigh + 1
      //

      if (i == 0)
         {
         A0 = A[1];
         A1 = A[0];
         }
      else if (2 * i + 2 > Aihigh)
         {
         // Three cases, according to how much overlap
         if (2 * i == Aihigh + 1)
            {
            A2 = A[Aihigh];
            A3 = A[Aihigh - 1];
            A4 = A[Aihigh - 2];
            }
         else if (2 * i == Aihigh)
            {
            A3 = A[Aihigh];
            A4 = A[Aihigh - 1];
            }
         else // 2*i == Aihigh-1
            {
            A4 = A[Aihigh];
            }
         }

      // Now run across the row, filling out the line
      for (j = 0; j <= Ajhigh; j++)
         {
         // Filter in the j direction, and put the value in buf
         buf[j] = (Pixel_type) (wj0 * (A0[j] + A4[j]) + wj1 * (A1[j] + A3[j]) + A2[j]);
         }

      // Fill out the ends of the buffer
      buf[-2] = buf[1];
      buf[-1] = buf[0];
      buf[Ajhigh + 1] = buf[Ajhigh];
      buf[Ajhigh + 2] = buf[Ajhigh - 1];
      buf[Ajhigh + 3] = buf[Ajhigh - 2];

      // Finally, fill out B from the buffer
      Pixel_type *Bi = B[i];
      Pixel_type *tbuf = &(buf[-2]);  // Points to the start
      for (j = 0; j < Bjdim; j++)
         {
         // Compute the convolution
         Bi[j] = (Pixel_type) (
            wi0 * (tbuf[0] + tbuf[4]) +
            wi1 * (tbuf[1] + tbuf[3]) +
            wi2 *  tbuf[2]
            );

         // Update the pointer into the buffer
         tbuf += 2;
         }
      }

   // Return the reduced image
   return B;
   }

//
// Warning : The code of reduce and expand is horrible.  However, it is 
// necessary to write it this way to get maximum speed in this critical
// section.  Beware of changing this code.
// 

// Note:  Mar 8th, 2007.  This one seems faster than the other one above.
// Not sure why, since it seems to have more operations.

Pyramid_Image_type reduce_old(
   Pyramid_Image_type &A		// source array 
)
   {
   // Does one level of pyramid reduction 
   register int i, j;
   int idim = A.idim(), jdim = A.jdim();
   int Bidim = (idim + 1) / 2, Bjdim = (jdim + 1) / 2;

   // Allocate B
   Pyramid_Image_type B(0, Bidim - 1, 0, Bjdim - 1);

   // Get the weights
   Double_matrix w = weight_matrix();
   double w00 = w[0][0], w11 = w[1][1], w22 = w[2][2];
   double w01 = w[0][1], w02 = w[0][2], w12 = w[1][2];

   // First of all, put a boundary on the source array 
   Pyramid_Image_type Aa = enlarge_matrix(A, 2);

   // Now carry out the reduction step 
   // This code is a little obscure, since it is optimized for speed 
   // This is the most time consuming step of the algorithm, so speed counts 
   // It is assumed here that the weight kernel is symmetric and 5 x 5 
   for (i = 0; i < Bidim; i++)
      {
      // To save time in the inner loop, look up first index of arrays 
      Pixel_type *Bi = B[i];
      register Pixel_type *A2b, *A1b, *A0, *A1, *A2;
      A2b = Aa[2 * i - 2];
      A1b = Aa[2 * i - 1];
      A0 = Aa[2 * i];
      A1 = Aa[2 * i + 1];
      A2 = Aa[2 * i + 2];

      for (j = 0; j < Bjdim; j++)
         {
         // Save time by doing index arithmetic first 
         register int j2b, j1b, j0, j1, j2;
         j2 = 1 + (j1 = 1 + (j0 = 2 * j));
         j2b = (j1b = j0 - 1) - 1;

         // Now compute the entry 
         Bi[j] = (Pixel_type)(w22 * (A2[j2] + A2[j2b] + A2b[j2] + A2b[j2b])
            + w11 * (A1[j1] + A1[j1b] + A1b[j1] + A1b[j1b])
            + w00 * (A0[j0])
            + w02 * (A2[j0] + A2b[j0] + A0[j2] + A0[j2b])
            + w01 * (A1[j0] + A1b[j0] + A0[j1] + A0[j1b])
            + w12 * (A2[j1] + A2[j1b] + A2b[j1] + A2b[j1b]
               + A1[j2] + A1[j2b] + A1b[j2] + A1b[j2b]));
         }
      }

   // Return the reduced image
   return B;
   }

Pyramid_Image_type expand(Pyramid_Image_type &A, int idim, int jdim)
   {
   // Expand matrix A
   int i, j;

   // Declare a matrix of the correct size
   Pyramid_Image_type B(0, idim - 1, 0, jdim - 1);

   // Get the weights
   Double_matrix w = weight_matrix();
   double w00 = 4.0*w[0][0], w11 = 4.0*w[1][1], w22 = 4.0*w[2][2];
   double w01 = 4.0*w[0][1], w02 = 4.0*w[0][2], w12 = 4.0*w[1][2];

   // Put a boundary on A 
   Pyramid_Image_type Aa = enlarge_matrix(A, 1);

   // Now go ahead and expand 
   for (i = 0; i < idim / 2; i++)
      {
      register Pixel_type *A1b = Aa[i - 1], *A0 = Aa[i], *A1 = Aa[i + 1];
      Pixel_type *B2i = B[2 * i], *B2i1 = B[2 * i + 1];
      register int jind = 0;

      for (j = 0; j < jdim / 2; j++)
         {
         // Expand : We do this in 4 steps corresponding to 4 parities 
         // This is a little complicated, but necessary for speed 

      // Here we do the optimizer's work for it, in case it doesn't 
         register int j1 = j + 1, j1b = j - 1;
         B2i[jind] = (Pixel_type)(w00 *  A0[j]
            + w22 * (A1b[j1b] + A1b[j1] + A1[j1b] + A1[j1])
            + w02 * (A1b[j] + A1[j] + A0[j1b] + A0[j1]));
         B2i1[jind++] = (Pixel_type)(w01 * (A1[j] + A0[j])
            + w12 * (A0[j1b] + A0[j1] + A1[j1b] + A1[j1]));
         B2i1[jind] = (Pixel_type)(w11 * (A0[j]
            + A0[j1] + A1[j] + A1[j1]));
         B2i[jind++] = (Pixel_type)(w01 * (A0[j1] + A0[j])
            + w12 * (A1b[j1] + A1b[j] + A1[j1] + A1[j]));
         }

      // Look after the case where jdim is odd - one extra point 
      if (jdim % 2)
         {
         register int j1 = j + 1, j1b = j - 1;
         B2i[jind] = (Pixel_type)(w00 *  A0[j]
            + w22 * (A1b[j1b] + A1b[j1] + A1[j1b] + A1[j1])
            + w02 * (A1b[j] + A1[j] + A0[j1b] + A0[j1]));
         B2i1[jind++] = (Pixel_type)(w01 * (A1[j] + A0[j])
            + w12 * (A0[j1b] + A0[j1] + A1[j1b] + A1[j1]));
         }
      }

   // Look after the case where idim is odd - one extra point 
   if (idim % 2)
      {
      register Pixel_type *A1b = Aa[i - 1], *A0 = Aa[i], *A1 = Aa[i + 1];
      Pixel_type *B2i = B[2 * i];
      register int jind = 0;

      for (j = 0; j < jdim / 2; j++)
         {
         // Expand : We do this in 4 steps corresponding to 4 parities 
         // This is a little complicated, but necessary for speed 

      // Here we do the optimizer's work for it, in case it doesn't 
         register int j1 = j + 1, j1b = j - 1;
         B2i[jind++] = (Pixel_type)(w00 *  A0[j]
            + w22 * (A1b[j1b] + A1b[j1] + A1[j1b] + A1[j1])
            + w02 * (A1b[j] + A1[j] + A0[j1b] + A0[j1]));
         B2i[jind++] = (Pixel_type)(w01 * (A0[j1] + A0[j])
            + w12 * (A1b[j1] + A1b[j] + A1[j1] + A1[j]));
         }

      // Look after the case where jdim is odd - one extra point 
      if (jdim % 2)
         {
         register int j1 = j + 1, j1b = j - 1;
         B2i[jind] = (Pixel_type)(w00 *  A0[j]
            + w22 * (A1b[j1b] + A1b[j1] + A1[j1b] + A1[j1])
            + w02 * (A1b[j] + A1[j] + A0[j1b] + A0[j1]));
         }
      }

   // Return the expanded image
   return B;
   }

int number_of_pyramid_levels(Pyramid_Image_type &A, int mindim)
   {
   // Computes the number of levels for the pyramid

   // Get the dimensions of the array
   int idim = A.idim(), jdim = A.jdim();

   // Count the number of levels
   int n = 0;
   while (idim > mindim && jdim > mindim)
      {
      // Now go to the next level 
      idim = (idim + 1) / 2;
      jdim = (jdim + 1) / 2;
      n += 1;
      }

   // Return the number of levels we have done 
   return n;
   }

Pyramid_Image_type *gaussian_pyramid(
   Pyramid_Image_type &A,	// An image
   int nlevels 		// Number of bottom level of Gaussian 
)
   {
   // Runs down the pyramid filling it out 
   // Assumes that level 0 of the pyramid is already there 
   int i;

   // Make space for array for this image 
   Pyramid_Image_type *GG = new Pyramid_Image_type[nlevels];

   // Put the top level of the pyramid 
   GG[0] = A;

   // Fill out the pyramid
   for (i = 0; i < nlevels - 1; i++)
      GG[i + 1] = reduce(GG[i]);

   // Return the pyramid
   return GG;
   }

void create_gaussian_pyramid(
   Pyramid_Image_type &A,		// An image
   std::vector<Pyramid_Image_type> &pyramid	// The pyramid to be filled out
)
   {
   // Runs down the pyramid filling it out.
   // Returns the pyramid in a vector.  This must already be initialized

   // Put the top level of the pyramid 
   pyramid[0] = A;

   // Fill out the pyramid
   for (int i = 1; i < (int)pyramid.size(); i++)
      pyramid[i] = reduce(pyramid[i - 1]);
   }

Pyramid_Image_type *laplacian_pyramid(
   Pyramid_Image_type &A,	// An image
   int nlevels		// Number of bottom level of pyramid
)
   {
   // Makes the laplacian pyramid from an image.  Assumes that the top
   // level of the pyramid is the image.

   // First, make the gaussian pyramid
   Pyramid_Image_type *LL = gaussian_pyramid(A, nlevels);

   // Now, expand and subtract
   int i, j, k;
   for (i = 0; i < nlevels - 1; i++)
      {
      // Expand
      Pyramid_Image_type temp = expand(LL[i + 1], LL[i].idim(), LL[i].jdim());

      // Subtract it from the level of the pyramid
      for_2Dindex(j, k, LL[i])
         LL[i][j][k] -= temp[j][k];
      }

   // Return the pyramid
   return LL;
   }

Pyramid_Image_type expand_laplacian_pyramid(Pyramid_Image_type *LL, int nlevels)
   {
   // Expands the pyramid to reassemble the image
   Pyramid_Image_type A = LL[nlevels - 1];	// The starting point

   int i, j, k;
   for (i = nlevels - 2; i >= 0; i--)
      {
      // Expand one level
      A = expand(A, LL[i].idim(), LL[i].jdim());

      // Add the image at this level
      for_2Dindex(j, k, A)
         A[j][k] += LL[i][j][k];
      }

   // A is the expanded image
   return A;
   }

Pyramid_Image_type composite_image(Pyramid_Image_type *GG, int nlevels, int arrangement)
   {
   // Makes a composite image of a pyramid, so that one may view it easily
   // Arrangement = 0 -> horizontal; 1 -> vertical.
   static const int horizontal = 0 /*, vertical = 1 */;

   printf ("composite_image: A\n"); fflush(stdout);

   // Work out the dimensions of the image
   int idim = GG[0].idim();
   int jdim = GG[0].jdim();

   // Allocate an array of sufficient size for the image
   if (arrangement == horizontal) idim += GG[1].idim();
   else jdim += GG[1].jdim();
   Pyramid_Image_type im(0, idim - 1, 0, jdim - 1);
   printf ("composite_image: B\n"); fflush(stdout);
   printf ("  size:  (%d x %d)\n", im.idim(), im.jdim());

   // Clear out the image
   im.clear((Pixel_type)0);
   printf ("composite_image: C\n"); fflush(stdout);

   // Now fill it out with the imagery
   int i, j, k;
   int xoffs = 0, yoffs = 0;
   for (i = 0; i < nlevels; i++)
      {
      // Determine where this image goes
   printf ("composite_image: D\n"); fflush(stdout);

      // First image goes at the origin
      if (i == 0) xoffs = yoffs = 0;

      // Next image goes to right (horizontal) or below (vertical)
      else if (i == 1)
         if (arrangement == horizontal)
            xoffs = GG[0].idim();
         else
            yoffs = GG[0].jdim();

      // Subsequent images go below (horizontal) or to right (vertical)
      else
         if (arrangement == horizontal)
            yoffs += GG[i - 1].jdim();
         else
            xoffs += GG[i - 1].idim();

      // Copy the image
   printf ("composite_image: E\n"); fflush(stdout);
   printf ("Level %d size(%d, %d) at position (%d, %d)\n", 
         i, GG[i].idim(), GG[i].jdim(), xoffs, yoffs); fflush(stdout);
      for_2Dindex(j, k, GG[i])
         im[j + xoffs][k + yoffs] = GG[i][j][k];
   printf ("composite_image: F\n"); fflush(stdout);
      }

   // Return the image
   return im;
   }




