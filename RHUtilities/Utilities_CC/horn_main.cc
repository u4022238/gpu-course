/* @(#)horn.c	1.7 07 Nov 1994 */

/*
 * Implements Horn's algorithm for mapping point sets to point sets
 * by Euclidean transformations
 *
 * See the paper Closed Form Solution of absolute orientation using unit 
 * quaternions. by Berthold Horn, J. Opt Soc. Am. A, Vol 4, No 4, April 1987.
 */

#include "Utilities_CC/utilities_CC.h"
// #include <Utilities/quaternion_dcl.h>
 
int main ( int argc, char *argv[])
   {
   /* Tests out Horn's algorithm */

   /* First test out the argument list */
   if (argc < 2)
      {
      fprintf (stderr, "Usage : horn filename\n");
      bail_out (1);
      }

   /* Get the number of points and the deviation */
   char *fname = argv[1];

   /* Open the file */
   FILE *infile = FOPEN (fname, "r");

   /* Get the number of points */
   int numpoints;
   fscanf (infile, "%d", &numpoints);

   /* Allocate the coordinates */
   rhMatrix X1 (numpoints, 3);
   rhMatrix X2 (numpoints, 3);

   /* Now read the points */
   for (int i=0; i<numpoints; i++)
      {
      int n = fscanf (infile, "%lf %lf %lf %lf %lf %lf", 
            &(X1[i][0]), &(X1[i][1]), &(X1[i][2]), 
            &(X2[i][0]), &(X2[i][1]), &(X2[i][2]));
      if (n != 6)
         {
         fprintf (stderr, "Error reading file \"%s\"", fname);
         bail_out (1);
         }
      }

   /* Now we are ready to try out the transformation */
   // rhMatrix M = horn (X1, X2);
   // rhMatrix M = horn_noscale (X1, X2);
   const bool scale_or_not = false;
   const double HuberThreshold = 1.0;
   rhMatrix M = huber_horn (X1, X2, HuberThreshold, scale_or_not);

   printf ("horn: Transformation matrix is:\n");
   M.print(stdout, "%8.5f ");
   printf ("\n");
   fflush (stdout);

   /* Test the result */
   test_horn_transform (stdout, X1, X2, M);

   return 0;
   }

   

   

   

   
