#include <math.h>
#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Pmap.h>

/*-----------------------------------------------------------------------

Here is the theory:

A Pvector $theta v$ where $v$ is a unit vector is a 3-vector
representing a rotation through angle 2*theta about axis v.

A vector $theta v$ where $v$ is a unit vector 
represents a unit (homogeneous) vector

	(v sin(theta), cos(theta)).

The vector $theta v$ will be called a p-vector.

Any homogeneous vector can be represented by a p-vector for which
$0 <= theta <= pi/2$, which is called the normalized p-vector representative.
In fact, except for the case $theta = pi/2$ the correspondence
between unit homogeneous vector and normalized p-vector is
one-to-one.

As a quaternion, the unit vector

	(v sin(theta), cos(theta))

represents a rotation through angle $2 theta$
about the axis given by v.

*******************************************************************************
** NOTE:  Too late to change this now.  A Pvector v*theta
**        represents a rotation through 2*theta about the axis v.
**        This is different from the usual exponential map formula.  Also
**        note that quaternions are written here with the real part last!!
*******************************************************************************
**        For angle-axis vector representation, use Pmap::AngleAxisToMatrix or 
**        Pmap::MatrixToAngleAxis
*******************************************************************************

------------------------------------------------------------------------*/

rhVector Pmap::hom (const rhVector &v)
   {
   // Makes a homogeneous vector out of the original one

   // Make a vector of one size greater
   int dim = v.dim();
   rhVector vhom (dim+1);

   // Take cos and sinc of norm
   double normv = v.length(); 
   double c = cos(normv);
   double s = sinc(normv);

   // Now fill out the vector
   int i;
   for_1Dindex (i, v)
      vhom[i] = s * v[i];
   vhom[dim] = c;

   // This is the vector to return
   return vhom;
   }

rhVector Pmap::dehom (const rhVector &vhom)
   {
   // Makes the pmap representative of a homogeneous vector
   int i;

   // Make a vector of one size smaller
   int dim = vhom.dim() - 1;
   rhVector v (dim);

   // Work out the argument of this vector
   double c = rhAbs(vhom[dim]);

   double s = 0.0;
   for (i=0; i<vhom.dim() - 1; i++)
      s += vhom[i]*vhom[i];
   s = sqrt(s);

   // Get the argument -- since both c and s are positive,
   // norm must be less than Pi/2
   double norm = atan2 (s, c);

   // Now, compute the factor -- since norm < Pi/2, sinc(norm) is not
   // close to zero.
   double fac = 1.0 / sinc(norm);
   if (vhom[dim] < 0.0) fac = -fac;

   // Finally full out the vector
   for_1Dindex (i, v)
      v[i] = vhom[i] * fac;

   // Return the vector
   return v;
   }

static double PI_ON_2_SQUARED = (M_PI * M_PI) / 4.0;
void Pmap::normalize (const rhVector &v)
   {
   // Renormalize the vector so that it has
   // norm less than Pi/2
   double normsq = v*v;

   // If too large, then normalize
   if (normsq > PI_ON_2_SQUARED)
      {
      // Compute the normalization factor
      double norm = sqrt(normsq);
      double fac = 1.0 - M_PI / norm;
      int i;
      for_1Dindex (i, v)
	 v[i] *= fac;
      }
   }

rhVector Pmap::MatrixToPvector (const rhMatrix &Rin)
   {
   // From a rotation matrix, gives a p-vector representing
   // the quaternion representation of the matrix.
   // Thus, a p-vector $theta v$, where $v$ is unit vector
   // represents a rotation through angle 2 theta about the axis
   // represented by vector v.

   // Determine the axis of the rotation -- eigenvalue
   // corresponding to unit eigenvector.

   // Reduce to unit determinant, to take account of scaled homogeneous matrices
   rhMatrix R = Rin.copy();
   double dval = det3x3_0 (R);
   double dfac;
   if (dval > 0.0)
      dfac =  1.0/ pow (dval, 1.0/3);
   else
      dfac = -1.0/ pow(-dval, 1.0/3);
   R = R * dfac;

   // Now find the unit eigenvector
   rhMatrix M = R.copy();
   M[0][0] -= 1.0;
   M[1][1] -= 1.0;
   M[2][2] -= 1.0;

   // Solve for the eigenvector
   rhMatrix V;
   rhVector D;
   svd (M, D, V);

   // Multiply it back out, and we should get a rotation about
   // the Z axis
   rhMatrix Zrot = V.transpose() * R * V;

   // Get the sin and the cosine of the angle.
   // Do this in a way that would be unchanged if M were multiplied
   // by a constant.
   double c = Zrot[0][0];
   double s = Zrot[1][0];

   // Now, determine the rotation angle : 2 theta in range -Pi to Pi
   double two_theta = atan2 (s, c);

   // If the angle is negative, then we need to take its absolute value,
   // to make it positive.
   // Rotation through 2 theta about an axis is the same as rotation through
   // -2 theta about the oppositely oriented axis.

   double sign = 1.0;  // Indicates whether to take V or -V as the axis
   if (two_theta < 0.0)
      {
      two_theta = -two_theta;
      sign = -sign;
      }

   // Also, check that two_theta is not greater than PI.  Should not
   // happen according to the spec of atan2.
   if (two_theta > M_PI)
      {
      two_theta = 2*M_PI - two_theta;
      sign = -sign;
      }

   // Halve to get theta between 0 and Pi/2
   double theta = two_theta * 0.5;

   // If V has negative determinant, then we should be using -V
   if (det3x3_0 (V) < 0.0)
      sign = -sign;

   // Now, we can write the vector down
   rhVector vec(3);
   double fac = theta * sign;
   vec[0] = V[0][2] * fac;
   vec[1] = V[1][2] * fac;
   vec[2] = V[2][2] * fac;

#ifdef PMAP_DEBUG
   // Verify
      {
      double trace = R[0][0] + R[1][1] + R[2][2];
      double ctheta = (trace - 1.0) / 2.0;
      
      double cth2 = cos(2.0 * vec.length());
      printf ("Checking MatrixToPvector :  %e vs %e\n", ctheta, cth2);
      printf ("Should be zero :\n");
      (R * vec - vec).print();
      }
#endif

   // Return the vector
   return vec;
   }

rhMatrix Pmap::QuaternionToMatrix (const rhVector &q)
   {
   //
   // Given a normalized quaternion,
   //  computes the rotation matrix it represents 
   //

   // Get the components of the quaternion 
   // Note that in Pmap class, the real part comes last 
   double 	c = q[3], 
		x = q[0], 
		y = q[1], 
		z = q[2];// Components of the quaternion

   // Now compute the terms needed in the matrix representation 
   double xx = x * x;
   double yy = y * y;
   double zz = z * z;
   double cc = c * c;
   double xy = x * y;
   double xz = x * z;
   double yz = y * z;
   double cx = c * x;
   double cy = c * y;
   double cz = c * z;

   // Fill out the rotation matrix 
   rhMatrix R(3, 3);
   R[0][0] = cc + xx - yy - zz;
   R[0][1] = 2.0 * (xy - cz);
   R[0][2] = 2.0 * (xz + cy);

   R[1][0] = 2.0 * (xy + cz);
   R[1][1] = cc + yy - xx - zz;
   R[1][2] = 2.0 * (yz - cx);

   R[2][0] = 2.0 * (xz - cy);
   R[2][1] = 2.0 * (yz + cx);
   R[2][2] = cc + zz - xx - yy;

   // Return the matrix
   return R;
   }


rhMatrix Pmap::PvectorToMatrix (const rhVector &v)
   {
   // Given a pvector, output the rotation matrix that its corresponding
   // quaternion represents.

   // Convert to a unit quaternion
   rhVector hvec = Pmap::hom (v);

   // Now get the corresponding rotation
   rhMatrix R = Pmap::QuaternionToMatrix (hvec);

#ifdef PMAP_DEBUG
   // Verify
      {
      double trace = R[0][0] + R[1][1] + R[2][2];
      double ctheta = (trace - 1.0) / 2.0;
      
      double cth2 = cos(2.0 * v.length());
      printf ("Checking PvectorToMatrix :  %e vs %e\n", ctheta, cth2);

      printf ("Should be zero :\n");
      (R * v - v).print();
      }
#endif

   // Return the rotation matrix
   return R;
   }

rhVector Pmap::hom_theta2 (const rhVector &v)
   {
   // Makes a homogeneous vector out of the original one

   // Make a vector of one size greater
   int dim = v.dim();
   rhVector vhom (dim+1);

   // Take cos and sinc of norm
   double theta = v.length(); 
   double c = cos(theta/2.0);
   double s = sinc(theta/2.0)/2.0;

   // Now fill out the vector
   int i;
   for_1Dindex (i, v)
      vhom[i] = s * v[i];
   vhom[dim] = c;

   // This is the vector to return
   return vhom;
   }

rhMatrix Pmap::AngleAxisToMatrix (const rhVector &v)
   {
   // Given a angle-axis vector, output the rotation matrix.

   // Convert to a unit quaternion by theta/2
   rhVector hvec = Pmap::hom_theta2 (v);

   // Now get the corresponding rotation
   rhMatrix R = Pmap::QuaternionToMatrix (hvec);

   // Return the rotation matrix
   return R;
   }

rhVector Pmap::MatrixToAngleAxis (const rhMatrix &Rin)
   {
   // From a rotation matrix, gives an angle-axis vector representing
   // the quaternion representation of the matrix.

   // Determine the axis of the rotation -- eigenvalue
   // corresponding to unit eigenvector.

   // Reduce to unit determinant, to take account of scaled homogeneous matrices
   rhMatrix R = Rin.copy();
   double dval = det3x3_0 (R);
   double dfac;
   if (dval > 0.0)
      dfac =  1.0/ pow (dval, 1.0/3);
   else
      dfac = -1.0/ pow(-dval, 1.0/3);
   R = R * dfac;

   // Now find the unit eigenvector
   rhMatrix M = R.copy();
   M[0][0] -= 1.0;
   M[1][1] -= 1.0;
   M[2][2] -= 1.0;

   // Solve for the eigenvector
   rhMatrix V;
   rhVector D;
   svd (M, D, V);

   // Multiply it back out, and we should get a rotation about
   // the Z axis
   rhMatrix Zrot = V.transpose() * R * V;

   // Get the sin and the cosine of the angle.
   // Do this in a way that would be unchanged if M were multiplied
   // by a constant.
   double c = Zrot[0][0];
   double s = Zrot[1][0];

   // Now, determine the rotation angle : 2 theta in range -Pi to Pi
   double two_theta = atan2 (s, c);

   // If the angle is negative, then we need to take its absolute value,
   // to make it positive.
   // Rotation through 2 theta about an axis is the same as rotation through
   // -2 theta about the oppositely oriented axis.

   double sign = 1.0;  // Indicates whether to take V or -V as the axis
   if (two_theta < 0.0)
      {
      two_theta = -two_theta;
      sign = -sign;
      }

   // Also, check that two_theta is not greater than PI.  Should not
   // happen according to the spec of atan2.
   if (two_theta > M_PI)
      {
      two_theta = 2*M_PI - two_theta;
      sign = -sign;
      }

   // Halve to get theta between 0 and Pi/2
   double theta = two_theta * 0.5;

   // If V has negative determinant, then we should be using -V
   if (det3x3_0 (V) < 0.0)
      sign = -sign;

   // Now, we can write the vector down
   rhVector vec(3);
   double fac = two_theta * sign; // Two_theta used for Angle-Axis
   vec[0] = V[0][2] * fac;
   vec[1] = V[1][2] * fac;
   vec[2] = V[2][2] * fac;

#ifdef PMAP_DEBUG
   // Verify
      {
      double trace = R[0][0] + R[1][1] + R[2][2];
      double ctheta = (trace - 1.0) / 2.0;
      
      double cth2 = cos(2.0 * vec.length());
      printf ("Checking MatrixToPvector :  %e vs %e\n", ctheta, cth2);
      printf ("Should be zero :\n");
      (R * vec - vec).print();
      }
#endif

   // Return the vector
   return vec;
   }


