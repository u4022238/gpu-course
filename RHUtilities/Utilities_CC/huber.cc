
#include <Utilities_CC/utilities_CC.h>

#ifdef WIN32
#include <float.h>
#define isinf(x) (! _finite(x))
#else
#include <float.h>
/*#include <ieeefp.h>*/
#endif

FUNCTION_DEF(double welsch1, (double x, double tail))
   {
   // Huber function -- positive x only
   if (isinf(x)) 
      return 1 + 1/tail;
   else 
      return (x + tail * (x - 1.0)) / (1.0 + tail * (x - 1.0));
   }

FUNCTION_DEF (double welschd1, (double x, double tail))
   {
   // Huber derivative -- positive x only
   if (isinf(x)) return 0;
   else 
      {
      double t = 1.0 / (1.0 + tail * (x - 1.0));
      return t*t;
      }
   }

FUNCTION_DEF (double welsch_fn, (
	  double x 
	, double thresh /*- = 1.0 -*/
	, double tail   /*- = 4.0 -*/
	))
   {
   // Positive tail
   if (x > thresh) 
      return thresh * welsch1(x/thresh, tail);

   // Negative tail
   if (x < -thresh) 
      return -thresh * welsch1(-x/thresh, tail);

   // Central region
   return x;
   }

FUNCTION_DEF (double welsch_deriv, (
	double dx
	, double x
	, double thresh /*- = 1.0 -*/
	, double tail   /*- = 4.0 -*/
	))
   {
   // Let us get rid of the negative case straight away

   // Positive tail
   if (x > thresh)
      {
      if (isinf(x)) return 0.0;
      else return welschd1(x/thresh, tail) * dx;
      }

   // Negative tail
   else if (x < -thresh)
      {
      if (isinf(x)) return 0.0;
      else return welschd1(-x/thresh, tail) * dx;
      }

   // Central region
   else
      return dx;
   }

#ifdef SCALED_VALS
FUNCTION_DEF (double huber_fn_slope1, (
   	double x,
	   double thresh /*- = 0.03 -*/
	))
   {
   // This gives a Huber function for estimation -- 
   // gives slope 1 in linear region
   if (x > thresh)
      return sqrt(x - 0.5*thresh);
   else if (x < -thresh)
      return -sqrt(-x +0.5*thresh);
   else
      return x/sqrt(2*thresh);
   }

FUNCTION_DEF (double huber_lin_fn_slope1, (
   	double x,
	   double thresh /*- = 0.03 -*/
	))
   {
   return huber_fn_slope1(x, thresh);
   }
#endif

FUNCTION_DEF (double huber_fn, (
   	double x,
	   double thresh /*- = 0.01 -*/
	))
   {
   // This gives a Huber function for estimation -- 
   // gives slope 1 in linear region
   if (x > thresh)
      return  sqrt( (2*x - thresh)*thresh);
   else if (x < -thresh)
      return -sqrt(-(2*x + thresh)*thresh);
   else
      return x;
   }

FUNCTION_DEF (double huber_lin_fn, (
   	double x,
	   double thresh /*- = 0.01 -*/
	))
   {
   return huber_fn (x, thresh);
   }

FUNCTION_DEF (rhVector huber_fn, (
   	const rhVector &X,
	   double thresh /*- = 0.01 -*/
	))
   {
   // This gives a Huber function for estimation -- 
   // gives slope 1 in linear region
   double lenx = X.length();
   double lenxh = huber_fn(lenx, thresh);
   return X * (lenxh / lenx);
   }

