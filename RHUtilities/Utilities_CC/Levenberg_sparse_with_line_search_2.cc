// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// This gives an object-oriented Levenberg-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector Levenberg_sparse::function(const rhVector &);
//

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Levenberg_sparse.h>

#include "mex.h"     // If we are using mex

// #define rhDEBUG

static FILE *debugfile = NULL;

Levenberg_sparse::Levenberg_sparse () : 
      xparam    (NULL), 
      Num_Loops (100), 
      Min_Incr  (1.0e-2), 
      lambda    (1.0e-3),
      pointrank_(0)
   {}

Levenberg_sparse::~Levenberg_sparse () 
   {
   if (xparam) delete [] xparam;
   }

//
// Keyboard interrupt stuff
//

rhBOOLEAN Levenberg_sparse::interrupt_pending = FALSE;

#ifndef WIN32
void Levenberg_sparse::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
      interrupt_pending = TRUE; 
   }
#endif

rhMatrix Levenberg_sparse::jx ( int i, rhVector &cparam, rhVector &xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (i, cparam, xparam);

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), xparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, xparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = xparam[j];

      // Increment the j-th parameter 
      double delta = FAC * xparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      xparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (i, cparam, xparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);

      // Restore the value of xparam[j] 
      xparam[j] = oldparam;
      }

// #define rhDEBUG
#ifdef rhDEBUG
      {
      // Print out the normal matrix

      if (! debugfile)
         debugfile = FOPEN ("LSparse_debug.txt", "w");

      // Write out the normal matrix
      fprintf (debugfile, "\n\nJb for point %d\n\n", i);
      dyda.print(debugfile);

      fprintf (debugfile, "\nCam params\n");
      cparam.print(debugfile);
      fprintf (debugfile, "\nPoint params\n");
      xparam.print(debugfile);

      // Flush
      fflush(debugfile);
      }
#endif

      // Return the matrix
      return dyda;
   }

rhMatrix Levenberg_sparse::jcam ( int i, rhVector &cparam, rhVector &xparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (i, cparam, xparam);

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), cparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, cparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = cparam[j];

      // Increment the j-th parameter 
      double delta = FAC * cparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      cparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (i, cparam, xparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);

      // Restore the value of cparam[j] 
      cparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

rhVector Levenberg_sparse::parameter_error (const rhVector &cparam)
   {
   rhVector nm;
   return nm;
   }

rhMatrix Levenberg_sparse::jcam0 (rhVector &cparam)
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = parameter_error (cparam);

   // See if this is null (no parameter errors used)
   if (oldval.is_null())
      return rhMatrix(0, cparam.dim());

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), cparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, cparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = cparam[j];

      // Increment the j-th parameter 
      double delta = FAC * cparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      cparam[j] += delta;

      // Now recompute the function 
      rhVector newval = parameter_error (cparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);

      // Restore the value of cparam[j] 
      cparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

void Levenberg_sparse::new_position_action ( rhVector &, rhVector *, int )
   { /* Default is to do nothing */ }

rhVector concatenate_rhVectors (rhVector *v, int numv)
   {
   // Concatenates a bunch of vectors into one
   int i, j;
   int length;   // Total length of vectors

   // Compute the total length
   for (i=0, length=0; i<numv; i++) length += v[i].dim();

   // Now allocate a vector
   rhVector vall (length);

   // Fill it out
   int count = 0;
   for (i=0; i<numv; i++)
      for_1Dindex (j, v[i])
      vall[count++] = v[i][j];

   // Return the concatenated vector
   return vall;
   }

rhVector Levenberg_sparse::total_function (
   const rhVector &cparam, rhVector *xparam, int numpoints)
   {
   // The total concatenated set of functions

   // First of all, just run the separate functions
   rhVector *v = new rhVector [numpoints];

   // Run the separate functions
   for (int i=0; i<numpoints; i++)
      v[i] = function (i, cparam, xparam[i]);

   // Concatenate
   rhVector vall = concatenate_rhVectors (v, numpoints);

   // Free the function values
   delete [] v;

   // Return the function vector
   return vall;
   }

static void augment (rhMatrix &M, double lambda)
   {
   // Augment diagonal elements of M, assumed square
   for (int j=M.ilow(); j<=M.ihigh(); j++)
      M[j][j] *= 1.0 + (lambda);
   }

void Levenberg_sparse::compute_temporaries (
   rhMatrix *AA, rhMatrix *BB, 
   rhMatrix J0,
   rhVector *epsilon,      // Inputs
   rhVector epsilon0,	// Camera parameter error
   int numpoints,

   // Values to be returned
   rhMatrix *BTB,
   rhMatrix *ATB,
   rhMatrix &N,
   rhVector &b
   )
   {
   // Computes the increments

   // Get the dimension of the normal matrix
   int dim = AA[0].jdim();

   // If the J0 is defined, then we need to initialize N and b
   if (J0.is_null())
      {
      N.Init(dim, dim);
      N.clear(0.0);

      b.Init(dim);
      b.clear(0.0);
      }
   else
      {
      // Initialize using the camera parameter increments
      N = J0.transpose() * J0;
      b = J0.transpose() * epsilon0;
      }

   // Now, fill it
   for (int i=0; i<numpoints; i++)
      {
      // Take aliases for A and B
      rhMatrix A = AA[i];
      rhMatrix B = BB[i];
      rhMatrix AT = A.transpose();
      rhMatrix BT = B.transpose();

      // First compute the values (B_i^T B_i)^-1
      BTB[i] = BT * B;
      ATB[i] = AT * B;

      // Increment N
      N = N + AT * A;

      // Now, the vector
      b = b + AT * epsilon[i];
      }
   }

void Levenberg_sparse::compute_increments (
   const rhMatrix *BB, 
   const rhVector *epsilon,      // Inputs
   const rhMatrix *BTB,
   const rhMatrix *ATB,
   const rhMatrix &Nin,
   const rhVector &bin,
   int numpoints,
   double lambda, 		// Regularization factor
   rhVector &cdel,		// Camera parameter increments
   rhVector *xdel		// Point increments
   )
   {
   // Computes the increments
   int i;

   // Make some storage for temporary matrices
   rhMatrix *BTBinv = new rhMatrix [numpoints];

   // Copy and augment
   rhMatrix N = Nin.copy();
   augment(N, lambda);
   rhVector b = bin.copy();

   // Now, fill it
   for (i=0; i<numpoints; i++)
      {
      // First compute the values (B_i^T B_i)^-1
      rhMatrix BTBi = BTB[i].copy();
      augment (BTBi, lambda);
      BTBinv[i] = BTBi.inverse_or_nearinverse();

      // Put the whole thing together
      N = N - ATB[i] * BTBinv[i] * (ATB[i].transpose());

      // Now, the vector
      rhMatrix BT = BB[i].transpose();
      b = b - ATB[i] * (BTBinv[i] * (BT * epsilon[i]));
      }

// #define rhDEBUG
#ifdef rhDEBUG

   // Write out the normal matrix
      {
      if (debugfile == NULL)
         debugfile = FOPEN ("LSparse_debug.txt", "w");

      // Now, write out the normal matrix
      fprintf (debugfile, "\n\nNormal Matrix - standard\n\n");
      N.print(debugfile);

      // Write out the goal vector
      // fprintf (debugfile, "\n\nGoal Vector\n\n");
      // b.print(debugfile);

      // Get the normal matrix for comparison
      rhMatrix NN = normal_matrix();
      fprintf (debugfile, "\nNormal matrix - comparison\n");
      NN.print(debugfile);

      // Print out the final ATA sum
      fprintf (debugfile, "Total ATA contribution - standard\n");
      Nin.print(debugfile);

      // Flush
      fflush (debugfile);
      }

#undef rhDEBUG
#endif 

   // Matrix solution -- solution overwrites b
   if (debugging_messages_enabled())
      timed_message (stderr, "Levenberg_sparse_with_line_search: Solving normal equations\n");

   if (! (lin_solve_symmetric_0(N, b, b, N.idim())))
      {
      warning_message ("Symmetric solution method failed");

      // Try SVD-based method
      if (! solve_simple_0 (N, b, b, N.idim(), N.idim()))
         {
         error_message ("Levenberg_sparse_with_line_search: Can not solve normal equations -- exiting");
         bail_out (2);
         }
      }

   // b is the delta to cameras
   cdel = b;

   // Back substitute to get the values of all the parameters
   for (i=0; i<numpoints; i++)
      {
      // Compute it in transposed form
      rhVector temp = epsilon[i] * BB[i] - cdel * ATB[i];
      xdel[i] = temp * BTBinv[i];

// #define rhDEBUG
#ifdef rhDEBUG

         {
         // Print out the normal matrix

         if (! debugfile)
            debugfile = FOPEN ("LSparse_debug.txt", "w");

         // Write out the normal matrix
         fprintf (debugfile, "\n\nBack-sub point %d\n", i);

         fprintf (debugfile, "\nBTBinv: \n");
         BTBinv[i].print(debugfile);

         fprintf (debugfile, "Vector = \n");
         temp.print(debugfile);

         // Flush
         fflush(debugfile);
         }

#undef rhDEBUG
#endif

      }

   // Now free the temporaries
   delete [] BTBinv;

   if (debugging_messages_enabled())
      timed_message (stderr, "Levenberg_sparse_with_line_search: Finished solving equations\n");
   }

double Levenberg_sparse::cost_of_increment ( 
   rhVector cdel,
   rhVector *xdel,		// Direction in which to seek
   double factor,		// How far to go in this direction
   rhVector &dy
   )
   {
   // Construct the suggested new values of the parameters
   rhVector new_cparam = cparam - factor * cdel;
   rhVector *new_xparam = new rhVector [ numpoints ];
   for (int i=0; i<numpoints; i++) 
      new_xparam[i] = xparam[i] - factor * xdel[i];

   // Evaluate the function -- this gets returned
   dy = total_function (new_cparam, new_xparam, numpoints);
   double newchisq = dy * dy;

   // Delete again
   delete [] new_xparam;

   // Return the value
   return newchisq;
   }

static bool time_to_stop (
      double low_x, double low_val, 
      double mid_x, double mid_val, 
      double high_x, double high_val
      ) 
   {
   // Time to stop the iterative search
   if ((high_x - low_x) < 0.01) return true;
   else return false;
   }

void Levenberg_sparse::do_line_search (
      rhVector cdel,
      rhVector *xdel,		// Direction in which to seek
      double *pfactor,	// Factor of this increment to use
      double *oldchisq,
      rhVector &old_dy_out,	// Old function value
      double *pnewchisq,	// Value of cost at new position
      rhVector &new_dy_out	// New function value
      )
   {
   /* First, set the golden ratio */
   double GoldenRatio = 0.5 * (sqrt(5.0) - 1);

   // We do a line search in the given direction

   // First part is to bound a region that has a minimum
   // Region to search in and the corresponding function values
   double   low_x,   mid_x,   high_x;
   double   low_val, mid_val, high_val;
   rhVector low_dy,  mid_dy,  high_dy;

   // Initial region is between factors of 0 and 1
   rhVector dy0, dy1;
   double chisq0 = cost_of_increment (cdel, xdel, 0.0, dy0);
   double chisq1 = cost_of_increment (cdel, xdel, 1.0, dy1);

   // Return the initial value
   *oldchisq = chisq0;
   old_dy_out = dy0;

   // If this increment is better then we search in both directions
   if (chisq1 < chisq0)
      {
      // Initial estimates for bounded region
      rhVector dyf;
      double factor = 1.0 / GoldenRatio;
      low_val = chisq0;
      mid_val = chisq1;
      high_val = cost_of_increment (cdel, xdel, factor, dyf);

      // And the current values
      low_x = 0.0;
      mid_x = 1.0;
      high_x = factor;

      // Error vectors
      low_dy  = dy0;
      mid_dy  = dy1;
      high_dy = dyf;

      // Now keep going in this direction until we start increasing
      while (high_val < mid_val)
         {
         // Evaluate at the next position
         factor = factor / GoldenRatio;

         // Update the region
         low_val  = mid_val;
         mid_val  = high_val;
         high_val = cost_of_increment (cdel, xdel, factor, dyf);

         // And the current values
         low_x  = mid_x;
         mid_x  = high_x;
         high_x = factor;

         // Error vectors
         low_dy  = mid_dy;
         mid_dy  = high_dy;
         high_dy = dyf;
         }
      }

   // Second possibility is that the initial step failed
   else
      {
      // Initial estimates for bounded region
      rhVector dyf;
      double factor = GoldenRatio;
      low_val = chisq0;
      mid_val = cost_of_increment (cdel, xdel, factor, dyf);
      high_val = chisq1;

      // And the current values
      low_x = 0.0;
      mid_x = factor;
      high_x = 1.0;

      // Error vectors
      low_dy = dy0;
      mid_dy = dyf;
      high_dy = dy1;

      // Now keep going in this direction until we start increasing
      while (high_val < mid_val)
         {
         // Evaluate at the next position
         factor = factor * GoldenRatio;

         // Update the region
         high_val = mid_val;
         mid_val = cost_of_increment (cdel, xdel, factor, dyf);

         // And the current values
         high_x = mid_x;
         mid_x = factor;

         // Error vectors
         high_dy = mid_dy;
         mid_dy = dyf;
         }
      }

   // Now, that we have bounded the region with a minimum, continue
   // to subdivide seeking the minimum, using Fibonacci search

   /* Find the minimum by recursion */
   while (1)
      {
      /* Recursive search for the minimum */
      rhVector new_dy;

      /* See if we have reached convergence */
      if (time_to_stop (low_x, low_val, mid_x, mid_val, high_x, high_val)) 
         {
         *pfactor = mid_x;
         *pnewchisq = mid_val;
         new_dy_out = mid_dy;
         return;
         }

      /* Otherwise do fibonnacci division */

      /* There are two cases */
      if(2*mid_x > high_x + low_x)
         {
         /* mid point past half way */
         double new_x = low_x + GoldenRatio * (mid_x - low_x);
         double new_val = cost_of_increment (cdel, xdel, new_x, new_dy);

         /* Now choose the new three points depending on result */
         if(new_val > mid_val)
            {
            low_x   = new_x; 
            low_val = new_val;
            low_dy  = new_dy;
            }
         else
            {
            high_x   = mid_x;   
            high_val = mid_val;
            high_dy  = mid_dy;

            mid_x   = new_x; 
            mid_val = new_val;
            mid_dy  = new_dy;
            }
         }

      else
         {
         /* mid point before half way */
         double new_x = mid_x + (1.0-GoldenRatio) * (high_x - mid_x);
         double new_val = cost_of_increment (cdel, xdel, new_x, new_dy);

         /* Now choose the new three points depending on result */
         if(new_val > mid_val)
            {
            high_x   = new_x; 
            high_val = new_val;
            high_dy  = new_dy;
            }
         else
            {
            low_x   = mid_x;    
            low_val = mid_val;
            low_dy  = mid_dy;

            mid_x   = new_x; 
            mid_val = new_val;
            mid_dy  = new_dy;
            }
         }
      }
   }

void Levenberg_sparse::print_increment_debug_info (
   rhVector cdel,
   rhVector *xdel,		// Direction in which to seek
   double factor,		// Factor of this increment to use
   rhVector old_dy,
   rhVector new_dy		// The error vectors
   ) const
   {
   // Print out the normal matrix
   // This is for debugging only
   int i;

   if (! debugfile)
      debugfile = FOPEN ("LSparse_debug.txt", "w");

   // Write out the normal matrix
   fprintf (debugfile, "\n\nDelta cam\n\n");
   (cdel*factor).print(debugfile);

   for (i=0; i<numpoints; i++)
      {
      fprintf (debugfile, "\n\nDelta x[%d]\n", i);
      (xdel[i]*factor).print(debugfile);
      }

   // Write out the normal matrix
   fprintf (debugfile, "\n\nNew cam\n\n");
   (cparam + factor*cdel).print(debugfile);

   for (i=0; i<numpoints; i++)
      {
      fprintf (debugfile, "\n\nNew x[%d]\n", i);
      (xparam[i] + factor*xdel[i]).normalized().print(debugfile);
      }

   fprintf (debugfile, "\n\nOld error vector\n\n");
   old_dy.print (debugfile);

   fprintf (debugfile, "\n\nNew error vector\n\n");
   new_dy.print (debugfile);

   fprintf (debugfile, "\n\nNew chisq : %e  (old = %e)\n", 
      new_dy * new_dy, old_dy * old_dy);

   // Flush
   fflush(debugfile);
   }

void Levenberg_sparse::write_jacobians (rhMatrix *A, rhMatrix *B) const
   {
   // Write out the partial derivatives
   for (int i=0; i<numpoints; i++)
      {
      fprintf (stderr, "\nPoint %d\n", i);

      // The camera derivatives
      fprintf (stderr, "\nDeriv wrt cameras (%d x %d) : \n", 
         A[i].idim(), A[i].jdim());
      A[i].print (stderr, "%11.3f ");

      // The point derivatives
      fprintf (stderr, "\nDeriv wrt point %d :  (%d x %d) : \n", 
         i, B[i].idim(), B[i].jdim());
      B[i].print (stderr, "%11.3f ");
      }
   }

double Levenberg_sparse::solve ()
   {
   // Declare variables 
   int i;
   double chisq = 0.0;
   rms_ = -1.0;			// Negative means not computed
   rhBOOLEAN improvement = TRUE;

// #define rhDEBUG
#ifdef rhDEBUG

      {
      // Print out the normal matrix

      if (! debugfile)
         debugfile = FOPEN ("LSparse_debug.txt", "w");

      // Write out the normal matrix
      fprintf (debugfile, "\n\nBeginning to solve\n\n");

      // Flush
      fflush (debugfile);
      }

#undef rhDEBUG
#endif

   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ Levenberg_sparse::keybd_interrupt);
#endif

   // Set up some needed data structures
   rhVector *epsilon    = new rhVector [ numpoints ];
   rhVector *xdel       = new rhVector [ numpoints ];
   rhMatrix *A          = new rhMatrix [ numpoints ];
   rhMatrix *B          = new rhMatrix [ numpoints ];
   rhMatrix *BTB 	= new rhMatrix [ numpoints ];
   rhMatrix *ATB 	= new rhMatrix [ numpoints ];
   rhVector cdel;

   // Also structures to handle error-measurements in camera parameters
   rhMatrix J0;
   rhVector epsilon0;
   rhVector dy;

   // Now iterate 
   for (int icount=0; icount<Num_Loops && improvement; icount++)
      {
      double increment_ratio;

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "Setting up the minimization equations\n");

      // Compute the function value 
      for (i=0; i<numpoints; i++)
         epsilon[i] = function (i, cparam, xparam[i]);

      // Compute an initial value of the error 
      dy = concatenate_rhVectors (epsilon, numpoints);
      chisq = dy * dy;

      // Output some debugging info 
      if (icount == 0)
         {
         this->informative_message ("\n\tInitial error : chisq = %g, rms = %g", 
            chisq, sqrt(chisq/dy.dim()));

	      rms_ = sqrt(chisq/dy.dim());	// Stores the value
         }

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "About to compute Jacobians\n");

      // Compute the Jacobians
      for (i=0; i<numpoints; i++)
         {
         A[i] = jcam (i, cparam, xparam[i]);
         B[i] = jx   (i, cparam, xparam[i]);
         }

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "Finished\n");

#ifdef rhDEBUG
      // Write out the Jacobians
      write_jacobians (A, B);
#endif

      // Also the one for the camera parameters
      J0 = jcam0 (cparam);
      epsilon0 = parameter_error (cparam);

      // Make some storage for temporary matrices
      rhMatrix N;
      rhVector b;

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "About to compute temporaries\n");

      // Compute the temporaries
      compute_temporaries (A, B, J0, epsilon, epsilon0, numpoints, BTB,
         ATB, N, b);

      // Give some timing information
      if (debugging_messages_enabled())
         timed_message (stderr, "Computed temporaries\n");

      // Now estimate new position with different lambda until improvement 
      double factor;
      do {
         //---------------------------------------
         // Compute the increments
         //---------------------------------------

         // Compute a direction in which to move
         compute_increments (
            B, epsilon, BTB, ATB, N, b, numpoints, lambda, cdel, xdel);

         // Do a line search in this direction
         double oldchisq;
         double newchisq;
         rhVector old_dy, new_dy;
         do_line_search (cdel, xdel, &factor, 
            &oldchisq, old_dy, 
            &newchisq, new_dy);

// #define rhDEBUG
#ifdef rhDEBUG
         print_increment_debug_info (cdel, xdel, factor, old_dy, new_dy);
#endif

         // Compute the proportional change in the error vector
            {
            rhVector del = new_dy - old_dy;
            double len_del = sqrt(del*del);
            double len_dy = sqrt (oldchisq);
            increment_ratio = len_del / len_dy;
            }

         // Accept or reject it 
         if (newchisq < oldchisq)
            {
            // Success, accept this new solution 
            chisq = newchisq;
            rms_ = sqrt(chisq/dy.dim());	// Stores the value
            dy = new_dy;

            // Accept the new parameter values 
            cparam = cparam - factor * cdel;
            for (i=0; i<numpoints; i++)
               xparam[i] = xparam[i] - factor * xdel[i];

            // Call any user function 
            new_position_action (cparam, xparam, numpoints);

            // Signal improvement 
            improvement = TRUE;

            // Do we increment lambda
            if (factor >= 0.3)
               lambda *= 0.1;
            else
               lambda *= 10.0;
            }
         else
            {
            // Failure, increas lambda and return. 
            lambda *= 10.0;
            improvement = FALSE;
            }
         } while ( ! improvement && increment_ratio >= Min_Incr 
            && (lambda < 1e10));

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if the increment_ratio has been too small 
      if (increment_ratio < Min_Incr || lambda >= 1e10) break;

      // Output some debugging info 
      this->informative_message (
         "\tlambda = %5.0e, chisq = %g, rms = %g, factor = %.2f", 
         lambda, chisq, sqrt(chisq/dy.dim()), factor);
      }

   // Output some debugging info 
   this->informative_message ("\tFinal error    : chisq = %g, rms = %g", 
      chisq, sqrt(chisq/dy.dim()));

   // Free the temporaries
   delete [] epsilon;
   delete [] xdel;
   delete [] A;
   delete [] B;
   delete [] BTB;
   delete [] ATB;

   // Return the value of the error 
   return chisq;
   }

rhMatrix Levenberg_sparse::covarianceMatrix (rhVector &mparam)
   {
   // This is not correct right now
   rhMatrix J; return J;
   }

void Levenberg_sparse::setParameters (rhVector &cp, rhVector *xp, int npts) 
   {
   // Creates the data
   numpoints = npts;

   // Set up the new parameters
   if (xparam) delete [] xparam; 
   xparam = new rhVector [numpoints];
   for (int i=0; i<numpoints; i++)
      xparam[i] = xp[i].copy();

   // And copy the camera parameters
   cparam = cp.copy();
   }

void Levenberg_sparse::getParameters (
        rhVector &cp, 
        rhVector **xp, 
        int *npts) const
   {
   // Initialize the xp parameters
   *xp = new rhVector [numpoints];

   // Copy them
   cp = cparam.copy();
   for (int i=0; i<numpoints; i++)
      (*xp)[i] = xparam[i].copy();

   // Return the number of points
   *npts = numpoints;
   }

void Levenberg_sparse::setParameters (
        rhVector &cp, 
        const vector<rhVector> &xp, 
        int npts)
   {
   // Creates the data
   numpoints = npts;

   // Set up the new parameters
   if (xparam) delete [] xparam; 
   xparam = new rhVector [numpoints];
   for (int i=0; i<numpoints; i++)
      xparam[i] = xp[i].copy();

   // And copy the camera parameters
   cparam = cp.copy();
   }

void Levenberg_sparse::getParameters (
        rhVector &cp, 
        vector<rhVector> &xp, 
        int *npts
        ) const
   {
   // Initialize the xp parameters
   xp.resize(numpoints);

   // Copy them
   cp = cparam.copy();
   for (int i=0; i<numpoints; i++)
      xp[i] = xparam[i].copy();

   // Return the number of points
   *npts = numpoints;
   }

rhMatrix Levenberg_sparse::normal_matrix ()
   {
   // Returns the normal matrix (used for computing the covariance of
   // the camera parameters.

   // First declare the matrix
   rhMatrix N;

   // Now, fill it
   for (int i=0; i<numpoints; i++)
      {
      // Compute the Jacobians
      rhMatrix A = jcam (i, cparam, xparam[i]);
      rhMatrix B = jx   (i, cparam, xparam[i]);

      // First compute (B_i^T B_i)^-1.  Use pseudo-inverse
      rhMatrix BTB = B.transpose() * B;
      
      // Compute the pseudo-inverse with the given rank
      int rank = BTB.idim();
      if (pointrank_ > 0) rank = pointrank_;
      rhMatrix BTBinv = BTB.pseudo_inverse(rank);

      // Now, ATA and ATB
      rhMatrix AT = A.transpose();
      rhMatrix ATA = AT * A;
      rhMatrix ATB = AT * B;

      // Put the whole thing together
      rhMatrix Ni = ATA - ATB * BTBinv * (ATB.transpose());

      // Now, sum them
      if (i == 0) N = Ni;
      else N = N + Ni;
      }

   // Finally, add the component for the camera parameters
   rhMatrix J0 = jcam0 (cparam);
   if (!J0.is_null())
      N += J0.transpose() * J0;

   // Return the normal matrix
   return N;
   }

#include <stdarg.h>

void Levenberg_sparse::informative_message (const char* fmt, DOTDOTDOT)
   {
   /* Get the message and print the specific error message */

   /* Set default for info_file */
   //if (info_file == (FILE *) 0) info_file = stdout;
   FILE *info_file = stdout;

   /* Check to see whether we should output the message */
   // if (! output_informative_messages) return;

   /* Output the message */
      {
      va_list args;

      /* Initialize the parameter list */
      va_start (args, fmt);

      /* Print the message */
      /* vfprintf is basically the same as _doprnt */
      // vfprintf (info_file, fmt, args);
      mexPrintf (fmt, args);

      /* Print a line feed */
      fprintf (info_file, "\n");

      /* Finish with the arguments */
      va_end (args);
      }

   // Flush
   fflush (info_file);
   }
