#include <Utilities_CC/MessageSwitch.h>
#include <Utilities/error_message_dcl.h>
#include <Utilities/rh_util.h>

MessageSwitch::MessageSwitch () {}
MessageSwitch::~MessageSwitch () {}

//---------------------------------------------------------

InformativeMessageSwitch::InformativeMessageSwitch (
	MessageStatus status
	)
   {
   // Turn on/off the messages temporarily during scope
   if (status == MessageStatus_On)
      {
      rhBOOLEAN val = enable_informative_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning informative messages on\n");
      }

   else
      {
      rhBOOLEAN val = disable_informative_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning informative messages off\n");
      }
   }

InformativeMessageSwitch::~InformativeMessageSwitch ()
   {
   if (previous_status_ == MessageStatus_On)
      {
      enable_informative_messages();
      // fprintf (stderr, "Turning informative messages back on\n");
      }
   else
      {
      disable_informative_messages();
      // fprintf (stderr, "Turning informative messages back off\n");
      }
   }

//---------------------------------------------------------

ErrorMessageSwitch::ErrorMessageSwitch (
	MessageSwitch::MessageStatus status
	)
   {
   // Turn on/off the messages temporarily during scope
   if (status == MessageStatus_On)
      {
      rhBOOLEAN val = enable_error_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning error messages on\n");
      }

   else
      {
      rhBOOLEAN val = disable_error_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning error messages off\n");
      }
   }

ErrorMessageSwitch::~ErrorMessageSwitch ()
   {
   if (previous_status_ == MessageStatus_On)
      {
      enable_error_messages();
      // fprintf (stderr, "Turning error messages back on\n");
      }
   else
      {
      disable_error_messages();
      // fprintf (stderr, "Turning error messages back off\n");
      }
   }

//---------------------------------------------------------

DebuggingMessageSwitch::DebuggingMessageSwitch (
	MessageSwitch::MessageStatus status
	)
   {
   // Turn on/off the messages temporarily during scope
   if (status == MessageStatus_On)
      {
      rhBOOLEAN val = enable_debugging_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning debugging messages on\n");
      }

   else
      {
      rhBOOLEAN val = disable_debugging_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning debugging messages off\n");
      }
   }

DebuggingMessageSwitch::~DebuggingMessageSwitch ()
   {
   if (previous_status_ == MessageStatus_On)
      {
      enable_debugging_messages();
      // fprintf (stderr, "Turning debugging messages back on\n");
      }
   else
      {
      disable_debugging_messages();
      // fprintf (stderr, "Turning debugging messages back off\n");
      }
   }

//---------------------------------------------------------

WarningMessageSwitch::WarningMessageSwitch (
	MessageSwitch::MessageStatus status
	)
   {
   // Turn on/off the messages temporarily during scope
   if (status == MessageStatus_On)
      {
      rhBOOLEAN val = enable_warning_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning warning messages on\n");
      }

   else
      {
      rhBOOLEAN val = disable_warning_messages ();
      previous_status_ = val ? MessageStatus_On : MessageStatus_Off;
      // fprintf (stderr, "Turning warning messages off\n");
      }
   }

WarningMessageSwitch::~WarningMessageSwitch ()
   {
   if (previous_status_ == MessageStatus_On)
      {
      enable_warning_messages();
      // fprintf (stderr, "Turning warning messages back on\n");
      }
   else
      {
      disable_warning_messages();
      // fprintf (stderr, "Turning warning messages back off\n");
      }
   }


