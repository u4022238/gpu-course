// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)ComplexVector.h	1.3 10/01/95
#ifndef Complex_vector_h
#define Complex_vector_h

#include <Utilities_CC/utilities_CC.h>

class ComplexVector : public Double_vector
   {
   // Stores a complex vector in packed double format.
   public :

	ComplexVector () {};	   	 // Don't need to do anything
   ComplexVector (Double_vector& v) : Double_vector(v) {};
	ComplexVector (int n) : Double_vector (0, 2*n-1){};
   ComplexVector (const rhVector &v);  // v = real part of signal
   ComplexVector (const rhVector &re, const rhVector &im); 
	ComplexVector fft () const;	  // Takes a fft of a signal
	ComplexVector fftinv () const;   // Takes an inverse fft of a signal
   ComplexVector conj () const;     // Takes the conjugate
	rhVector real () const;	  // Takes the real part
   rhVector imag () const;     // Takes the imaginary part

	// Some elementwise operators on complex signals
	ComplexVector operator * (const ComplexVector &p2) const;
	ComplexVector operator + (const ComplexVector &p2) const;
	ComplexVector operator - (const ComplexVector &p2) const;

	// For getting the complex dimension of the signal
        int cdim() const {return dim()/2; }

	// Printing out the vector
        void print (FILE *afile = stdout,
                    const char *format = "%15.6e") const
	   { fprint_vector (format, afile, (double *)(*this), low(), high());}
   };

#endif
