// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//
// @(#)Matrix2.cc	1.4 01 Oct 1995
//

// Routines for matrices that require the numerical Q library

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Matrix.h>
#include <assert.h>

//
// Other routines useful with matrices
//

rhVector rhMatrix::min_solution () const
   {
   // Finds the minimum eigenvector of A^T * A
   const rhMatrix &M = *this;

   // Find the solution
   rhVector x (M.jdim()); 
   min_solve_0 (M, x, M.idim(), M.jdim());
   return x;
   }

double rhMatrix::det () const
   {
   // Takes the determinant
   const rhMatrix &A = *this;

   // Test the dimension
   if (A.idim() != A.jdim())
      {
      error_message ("rhMatrix::det() : Determinant of non-square matrix");
      Error_Handler ();
      }

   // Otherwise, take the determinant
   if (A.idim() == 3) return det3x3_0 (A);
   else return determinant_0 (A, A.idim());
   }

FUNCTION_DEF (void QRdecomposition, (
	const rhMatrix A, const rhMatrix &K, const rhMatrix &R))
   {
   // Does a 3x3 QR decomposition.  Matrices must already be allocated
   
   // Check that the size of A is 3x3
   if (A.idim() != 3 || A.jdim() != 3)
      {
      error_message (
	"Have not implemented QR decomposition for %d x %d matrices",
	A.idim(), A.jdim());
      A.Error_Handler();
      }

   // Otherwise, do the decomposition
   QRdecomposition_3x3_0 (A, K, R);
   }

FUNCTION_DEF (int svd, (
	const rhMatrix &A, rhMatrix &U, rhVector &D, rhMatrix& V))
   {
   // Does a singular value decomposition

   // First, initialize the matrices
   int nrows = A.idim();
   int ncols = A.jdim();
   U.Init(nrows, ncols);
   D.Init(ncols);
   V.Init(ncols, ncols);

   // Special case for 3x3 SVD (who knows why)
   if (A.idim() == 3 && A.jdim() == 3)
      return svd3x3_0 (A, U, D, V);

   // Arbitrary dimension
   return sorted_svd_0 (
	A, A.idim(), A.jdim(), U, D, V);
   }

FUNCTION_DEF (int svd, (
	const rhMatrix &A, rhVector &D, rhMatrix& V))
   {
   // Does a singular value decomposition

   // First, initialize the matrices
   int nrows = A.idim();
   int ncols = A.jdim();
   D.Init(ncols);
   V.Init(ncols, ncols);

   // Special case for 3x3 SVD (who knows why)
   if (A.idim() == 3 && A.jdim() == 3)
      return svd3x3_0 (A, NULL, D, V);

   // Arbitrary dimension
   return sorted_svd_0 (
	A, A.idim(), A.jdim(), NULL, D, V);
   }

FUNCTION_DEF (int svd, (
	const rhMatrix &A, rhMatrix &U, rhVector &D))
   {
   // Does a singular value decomposition

   // First, initialize the matrices
   int nrows = A.idim();
   int ncols = A.jdim();
   U.Init(nrows, ncols);
   D.Init(ncols);

   // Special case for 3x3 SVD (who knows why)
   if (A.idim() == 3 && A.jdim() == 3)
      return svd3x3_0 (A, U, D, NULL);

   // Arbitrary dimension
   return sorted_svd_0 (
	A, A.idim(), A.jdim(), U, D, NULL);
   }

FUNCTION_DEF (int svd, (
	const rhMatrix &A, rhVector &D))
   {
   // Does a singular value decomposition

   // First, initialize the matrices
   int nrows = A.idim();
   int ncols = A.jdim();
   D.Init(ncols);

   // Special case for 3x3 SVD (who knows why)
   if (A.idim() == 3 && A.jdim() == 3)
      return svd3x3_0 (A, NULL, D, NULL);

   // Arbitrary dimension
   return sorted_svd_0 (
	A, A.idim(), A.jdim(), NULL, D, NULL);
   }

FUNCTION_DEF (int svd_rank, (rhVector D))
   {
   // Determines the rank of a matrix after singular value decomp by
   // looking at the diagonals
   const double threshold = 1.0e-10;    // Singular values less than this
                                       // are supposed to be zero
   // Determine the rank of C
   int rank;
   double minval = D[1]*threshold;
   for (rank=D.dim(); D[rank] < minval; rank--);

   return rank;
   }

FUNCTION_DEF (void jacobi, (
	const rhMatrix &A, rhVector &D, rhMatrix& V))
   {
   // Does an eigenvalue expansion
   // It is assumed that the matrix is symmetric

   // First, initialize the matrices
   int nrows = A.idim();
   int ncols = A.jdim();
   int dim = rhMin (nrows, ncols);
   D.Init(dim);
   V.Init(dim, dim);

   // Arbitrary dimension
   jacobi_0 ( A, dim, D, V);
   }

FUNCTION_DEF (int choleski, (const rhMatrix &A, const rhMatrix &C))
   {
   // Does a 3x3 Choleski factorization.  Matrices must already be allocated

   // Test the dimension
   if (A.idim() != A.jdim())
      {
      error_message ("choleski() : Factorization of non-square matrix");
      A.Error_Handler ();
      }
   
   // Otherwise, do the decomposition
   return choleski_RRT_0 (A, A.idim(), C);
   // return choleski_3x3 (A, C);
   }

FUNCTION_DEF (rhMatrix householder_rotation, (rhVector x, int n))
   {
   // Returns a matrix H such that H * x = (||x||, 0, ..., 0)
   // The matrix WILL have determinant 1 and will map x to a vector
   // with positive coordinate
   int k;

   // Check that n is in range
   assert (n >= x.low() && n <= x.high());

   // Take a copy of x
   rhVector v = x.copy();

   // Now, augment x
   int sign;
   double len = v.length();
   if (v[n] < 0.0) 
      {
      v[n] = v[n] - len;
      sign = 1;
      }
   else 
      {
      v[n] = v[n] + len;
      sign = -1;
      }

   // Form the householder matrix
   rhMatrix H = rhMatrix::identity(x.dim()) - (v^v)*(2.0/(v*v));

   // The determinant of H
   int det = -1;

   // We want to make sure that the thing is mapped directly to the positive
   // coordinate vector
   if (sign == -1) 
      {
      // Repace row of H by its negative, so that x goes to (+1, 0, 0,   )
      for_1Dindex (k, x)
	      H[n][k] = - H[n][k];

      // Keep track of the determinant of H
      det = -det;
      }

   // We also want to make sure that the determinant of H is positive
   if (det == -1 && x.dim() > 1)
      {
      // Fix by changing sign of one of the rows
      int s = (n == 0) ? 1 : 0;		// Row to change sign
      for_1Dindex (k, x)
	      H[s][k] = - H[s][k];
      }

   // Return the matrix
   return H;
   }

FUNCTION_DEF (rhMatrix householder_rotation, (rhVector x))
   {
   // Default set 1st element to 1
   return householder_rotation (x, 0);
   }

FUNCTION_DEF (rhMatrix householder_matrix, (rhVector x, int n))
   {
   // Returns a matrix H such that H * x = +/-(||x||, 0, ..., 0)
   // Modified to make sure it returns H*x = (||x||, 0, ..., 0)

   // Check that n is in range
   assert (n >= x.low() && n <= x.high());

   // Take a copy of x
   rhVector v = x.copy();

   // Make the matrix
   rhMatrix H;

   // Now, augment x
   double len = v.length();
   if (v[n] < 0.0) 
      {
      v[n] = v[n] - len;
      H = rhMatrix::identity(x.dim()) - (v^v)*(2.0/(v*v));
      }

   else 
      {
      v[n] = v[n] + len;
      H = (v^v)*(2.0/(v*v)) - rhMatrix::identity(x.dim());
      }

   // Return the matrix
   return H;
   }

FUNCTION_DEF (rhMatrix householder_matrix, (rhVector x))
   {
   // Default set 1st element to 1
   return householder_matrix (x, 0);
   }

FUNCTION_DEF (rhMatrix orthogonal_row_reduce, (rhMatrix M))
   {
   // Returns a row-reduced matrix with no more rows than columns
 
   // If it has fewer rows than columns, then return a copy
   if (M.idim() <= M.jdim()) return M.copy();

   // We will do it by SVD right now -- better ways exist
   rhMatrix V;
   rhVector D;
   svd (M, D, V);

   // Now, multiply D by V
   rhMatrix Mout = V.transpose();
   int i, j;
   for_2Dindex (i, j, Mout)
      Mout[i][j] *= D[i];

   // Return this matrix
   return Mout;
   }

FUNCTION_DEF (rhMatrix orthogonal_row_reduce, (rhMatrix M, int rank))
   {
   // Returns a row-reduced matrix with no more than rank rows

   // Rank at most number of columns
   if (rank > M.jdim()) rank = M.jdim();
 
   // If matrix has fewer rows already then required, then return it
   if (M.idim() <= rank) return M.copy();

   // We will do it by SVD right now -- better ways exist
   rhMatrix V;
   rhVector D;
   svd (M, D, V);

   // Now, multiply D by V
   rhMatrix Mout (rank, M.jdim());
   int i, j;
   for_2Dindex (i, j, Mout)
      Mout[i][j] = D[i] * V[j][i];

   // Return this matrix
   return Mout;
   }

FUNCTION_DEF (rhMatrix rhMtemp, (int idim, int jdim, double **vals))
   {
   // Makes a temporary matrix from a double array.  Temporary
   // means not reference counted.
   rhMatrix M; 
   M.plugin (0, idim-1, 0, jdim-1, vals);
   return M;
   }

FUNCTION_DEF (rhVector rhVtemp, (int idim, double *vals))
   {
   // Makes a temporary vector from a double array.  Temporary
   // means not reference counted.
   rhVector V; 
   V.plugin (0, idim-1, vals);
   return V;
   }

