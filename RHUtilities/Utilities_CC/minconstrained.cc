
// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

//----------------------------------------------------------------------------
//
//     Minimizing solution to homogeneous equations subject to a constraint
//
//----------------------------------------------------------------------------

/* SFILE_BEGIN */
#include <Utilities_CC/utilities_CC.h>
/* SFILE_END */

FUNCTION_DEF (rhVector min_eigen_vector_super_slow, (const rhMatrix &A))
   {
   // Returns the eigenvector corresponding to the least eigenvector.
   int dim = A.idim();

   // Declare matrices to carry the eigenvalues and eigenvectors
   rhMatrix V(dim, dim);
   rhVector D(dim);

   // Find the eigenvalues and vectors in descending order
   jacobi_0 (A, dim, D, V);

   // Return the eigenvector corresponding to the least eigenvector
   return V.column(V.jhigh());
   }

FUNCTION_DEF (rhVector min_eigen_vector, (const rhMatrix &A))
   {
   // Returns the eigenvector corresponding to the least eigenvector.
   // Uses SVD

   rhMatrix V;
   rhVector D;
   svd (A, D, V);
   return V.column(V.jhigh());
   }

FUNCTION_DEF (rhVector minimize_with_constraints_0, (
   const rhMatrix &AA,	// The metric matrix
   const rhMatrix &C	// The constraint matrix
   ))
   {
   // Finds the x that minimizes x^T AA x subject to Cx = 0 and x^T x = 1
   // AA is supposed to be a symmetric matrix, AA = A^T * A

   // Get the dimensions of the problem
   int ncols = C.jdim();
   
   // First, take the SVD of C
   rhMatrix V;
   rhVector D;
   svd(C, D, V);

   // Determine the rank of C
   int rank = svd_rank(D);

   // Now make a matrix from V2 that has just the last columns of V
   rhMatrix V2 (ncols, ncols-rank);
   
   // Fill it out
   int i, j;
   for (i=0; i<ncols; i++)
      for (j=rank; j<ncols; j++)
         V2[i][j-rank] = V[i][j];

   // Make a reduced set of equations
   rhMatrix A = V2.transpose() * AA * V2;

   // Now, find the eigenvector corresponding to the minimum eigen value
   rhVector x = min_eigen_vector (A);

   // Now, multiply by V2 to give the final solution
   rhVector x2 = V2 * x;

   // This is the vector to return
   return x2;
   }

FUNCTION_DEF (rhVector minimize_with_constraints_new_0, (
   const rhMatrix &A,	// The metric matrix
   const rhMatrix &C,	// The constraint matrix
   int rank_in /*- = -1 -*/	// Estimated rank
   ))
   {
   // Finds the x that minimizes ||A x|| subject to Cx = 0 and x^T x = 1

   // Get the dimensions of the problem
   int ncols = C.jdim();
   
   // First, take the SVD of C
   rhMatrix V;
   rhVector D;
   svd(C, D, V);

   // Determine the rank of C
   int rank;
   if (rank_in >= 0) 
      rank = rank_in;
   else
      rank = svd_rank(D);

   // Now make a matrix from V2 that has just the last columns of V
   rhMatrix V2 (ncols, ncols-rank);
   
   // Fill it out
   int i, j;
   for (i=0; i<ncols; i++)
      for (j=rank; j<ncols; j++)
         V2[i][j-rank] = V[i][j];

   // Make a reduced set of equations
   rhMatrix AV2 = A * V2;

   // Now, find the eigenvector corresponding to the minimum eigen value
   rhVector x = AV2.min_solution();

   // Now, multiply by V2 to give the final solution
   rhVector x2 = V2 * x;

   // This is the vector to return
   return x2;
   }

FUNCTION_DEF (rhVector minimize_with_mod1_constraints, (
        const rhMatrix &A, const rhMatrix &C, int rankC))
   {
   // Finds the vector that minimizes ||A C x|| subject to || C x || = 1
   // We assume that rankC is given.
   int i, j;

   // Take the SVD
   rhMatrix U, V;
   rhVector D;
   svd (C, U, D, V);

   // Take the first rankC columns of U
   rhMatrix U1(U.idim(), rankC);
   for_2Dindex (i, j, U1)
      U1[i][j] = U[i][j];

   // Take the SVD again
   rhMatrix U2, V2;
   rhVector D2;
   svd (A*U1, U2, D2, V2);
   rhVector x = V2.column(V2.jhigh());

   // Compute the value of the original function
   for_1Dindex (i, x)
      x[i] /= D[i];
   rhVector x2 (V.jdim());
   x2.clear(0.0);
   for_1Dindex (i, x)
      x2[i] = x[i];
   x = V * x2;

   // Return the vector
   return x;
   }

