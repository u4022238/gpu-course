#ifndef ___KD_TREE_H_
#define ___KD_TREE_H_

#include <vector>
using namespace std;
#include <Utilities_CC/SmartPointer.h>
#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/PriorityQueue.h>

class KDPriorityQueueEntry :  public PriorityQueueEntry
   {
   public :

        int search_node_;
	rhFloatVector nearest_point_to_goal_;
	double distance_to_goal_;
	int tree_number_;

        // Constructor
        KDPriorityQueueEntry (
		int treenum, 
		int node, 
		rhFloatVector nearest_point, 
		double dist):
			tree_number_(treenum),
                	search_node_(node), 
			nearest_point_to_goal_ (nearest_point),
			distance_to_goal_ (dist)
	   {}

	// Provide the virtual function
	double keyval();
   };

typedef SmartPointer<KDPriorityQueueEntry> KDPriorityQueueEntry_ref;

class KDNode
   {
   public :

   	short coord_number_;		// The coordinate to split at
	union  
	   {
   	   float boundary_;		// The value at which to split
	   int pointnumber_;		// Is -1 if not a leaf
	   };
   };

class KDTree
   {
   protected :
	
	//-------------------------------------
	//               Data
	//-------------------------------------

	// Dimensions of the array
	int first_node_;
	int last_node_;
	int last_nonleaf_node_;
	int point_dimension_;

	// Variables affecting performance
 	int max_trials_;

	// Statistics
	int points_evaluated_;
	int nodes_scheduled_;
	int points_found_;

   	// The array of nodes
	KDNode *node_;		// An array of KDNodes
	KDNode **tree_;		// Array of trees -- only one in base version

	// A heap for priority scheduling
	PriorityQueue *priority_queue_;

	// The array of points
   	vector<rhFloatVector> points_;

	//-------------------------------------
	//           Private methods
	//-------------------------------------

   	// We are going to have the nodes start with 1, to simplify arithmetic
   	inline int left_branch (int n)  { return n<<1; }
   	inline int right_branch (int n) { return (n<<1) + 1; }
   	inline int parent_of (int n)    { return n>>1; }
   	inline double kd_square (double x) { return x*x; }
	inline double distance (rhFloatVector v1, rhFloatVector v2) 
	   { rhFloatVector diff = v1 - v2; return diff*diff; }

	// Basic methods
	int find_base_point (KDNode *tree, rhFloatVector goal, int startnode);

	void schedule_node_search (
		int treenum,		// The number of the tree to search
		int startnode, 		// Starting node for search
		rhFloatVector goal,	// The goal point
		rhFloatVector nearest_point,// Nearest point to goal on node
		double distance,	// Distance to this nearest point
		double maxdistance	// Maximum distance we are interested in
		);

	void schedule_node (
		int treenum,
		int searchnode,
		rhFloatVector new_nearest_point,
		double newdist
		);

	bool get_next_node(
	   int *treenum, int *node, double *distance, rhFloatVector *nearest);

	void BuildKDTree ( 
		vector<rhFloatVector> &points,
		KDNode *tree,
		int node, 
		Int_vector pointorder, 
		int startpoint, 
		int endpoint
		);

	//-------------------------------------
	//           Public methods
	//-------------------------------------

   public :

	// Constructors, etc
	KDTree (vector<rhFloatVector> &vecs, bool build=true);
  	virtual ~KDTree ();

	// Setting parameters
	virtual void set_max_trials (int ntrials);

	// The main calling routine
	virtual int find_nearest_neighbour (rhFloatVector goal);

	// Statistics
	virtual void print_statistics ();
	virtual void reset_statistics ();
   };

#endif
