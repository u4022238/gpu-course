#ifndef _huber_cc_dcl_
#define _huber_cc_dcl_
#include "Utilities/cvar.h"


FUNCTION_DECL(double welsch1, (double x, double tail));

FUNCTION_DECL (double welschd1, (double x, double tail));

FUNCTION_DECL (double welsch_fn, (
	  double x 
	, double thresh  = 1.0 
	, double tail    = 4.0 
	));

FUNCTION_DECL (double welsch_deriv, (
	double dx
	, double x
	, double thresh  = 1.0 
	, double tail    = 4.0 
	));

FUNCTION_DECL (double huber_fn_slope1, (
   	double x,
	   double thresh  = 0.03 
	));

FUNCTION_DECL (double huber_lin_fn_slope1, (
   	double x,
	   double thresh  = 0.03 
	));

FUNCTION_DECL (double huber_fn, (
   	double x,
	   double thresh  = 0.01 
	));

FUNCTION_DECL (double huber_lin_fn, (
   	double x,
	   double thresh  = 0.01 
	));

FUNCTION_DECL (rhVector huber_fn, (
   	const rhVector &X,
	   double thresh  = 0.01 
	));

#endif
