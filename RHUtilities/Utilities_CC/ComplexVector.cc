// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)ComplexVector.cc	1.3 01 Oct 1995

#include <Utilities_CC/utilities_CC.h>

ComplexVector::ComplexVector (const rhVector &v)
   {
   // Makes a complex signal from a real signal

   int nn = v.dim();	// Dimension of the input array
   
   // The stored value must be twice the dimensions of the input
   Double_vector::Init (0, 2*nn-1);

   // Get a handle on the data array
   double *Z = data();

   // Pack the data 
   for (int i=0; i<nn; i++)
      {  
      Z[2*i]   = v[i];         // Real part 
      Z[2*i+1] = 0.0;          // Imaginary part 
      }
   }

ComplexVector::ComplexVector (
   const rhVector &re, 
   const rhVector &im
   )
   {
   // Makes a complex signal from real and imaginary parts

   int nn = re.dim();	// Dimension of the input array
   
   // The stored value must be twice the dimensions of the input
   Double_vector::Init (0, 2*nn-1);

   // Get a handle on the data array
   double *Z = data();

   // Pack the data 
   for (int i=0; i<nn; i++)
      {  
      Z[2*i]   = re[i];         // Real part 
      Z[2*i+1] = im[i];          // Imaginary part 
      }
   }

rhVector ComplexVector::real () const
   {
   // Returns the real part of a complex signal
   
   // Make an alias for the current signal
   const ComplexVector &z = *this;

   // Make a signal of the right size
   rhVector re (z.cdim());

   // Now unpack the real part
   int i;
   for_1Dindex (i, re) 
      re[i] = z[2*i];

   // Return the real part
   return re;
   }

rhVector ComplexVector::imag () const
   {
   // Returns the imaginary part of a complex signal
   
   // Make an alias for the current signal
   const ComplexVector &z = *this;

   // Make a signal of the right size
   rhVector im (z.cdim());

   // Now unpack the real part
   int i;
   for_1Dindex (i, im) 
      im[i] = z[2*i+1];

   // Return the imaginary part
   return im;
   }

ComplexVector ComplexVector::conj () const
   {
   // Returns the imaginary part of a complex signal
   
   // Make an alias for the current signal
   const ComplexVector &z = *this;

   // Make a signal of the right size
   ComplexVector con (z.cdim());

   // Now copy while conjugating
   for (int i=0; i<z.dim(); i+=2)
      {
      con[i]   =  z[i];
      con[i+1] = -z[i+1];
      }

   // Return the imaginary part
   return con;
   }

ComplexVector ComplexVector::fft() const
   {
   // Does and returns the fourier transform of a complex signal

   // Get an alias for the input signal
   const ComplexVector &sig = *this;

   // Do a forward fft
   return fourier_transform (sig, 1);
   }
   
ComplexVector ComplexVector::fftinv() const
   {
   // Does and returns the fourier transform of a complex signal

   // Get an alias for the input signal
   const ComplexVector &sig = *this;

   // Do a forward fft
   return fourier_transform (sig, -1);
   }

ComplexVector ComplexVector::operator * (const ComplexVector &z2) const
   {
   // Computes the elementwise product of the two vectors.
   // More precisely, it computes the z1 * z2.conj.
   // NOTE: The second signal is conjugated.
   int i;

   // Get an alias for the first descriptor
   const ComplexVector &z1 = *this;

   // Get the dimension
   int n1 = z1.cdim();
   int n2 = z2.cdim();
   int nn = n1;

   // If the two dimensions are different, then this is a mistake
   if (n1 != n2) 
      {
      error_message (
	 "Correlation: Two arrays have different dimension (%d and %d)",
	  n1, n2);
      return ComplexVector();	// Return an empty vector
      }

   // Now, allocate a vector to be the product of the two signals
   ComplexVector c (z2.cdim());

   // Carry out the correlation computation.
   // multiply the two things together (that is z1 * conj(z2)) 
   for (i=0; i<2*nn; i+=2)
      {  
      c[i]   = z1[i]*z2[i]   + z1[i+1]*z2[i+1];   // Real part 
      c[i+1] = z1[i+1]*z2[i] - z1[i]*z2[i+1];     // Imaginary part 
      }  

   // Return this correlation
   return c;
   }

ComplexVector ComplexVector::operator + (const ComplexVector &z2) const
   {
   // Computes the product of the two signals
   int i;

   // Get an alias for the first descriptor
   const ComplexVector &z1 = *this;

   // Get the dimension
   int n1 = z1.cdim();
   int n2 = z2.cdim();

   // If the two dimensions are different, then this is a mistake
   if (n1 != n2) 
      {
      error_message (
	 "Correlation: Two arrays have different dimension (%d and %d)",
	  n1, n2);
      return ComplexVector();	// Return an empty vector
      }

   // Now, allocate a vector to be the sum of the two signals
   ComplexVector c (z1.cdim());

   // Add the two vectors 
   for_1Dindex (i, c)
      c[i] = z1[i] + z2[i];

   // Return result
   return c;
   }

ComplexVector ComplexVector::operator - (const ComplexVector &z2) const
   {
   // Computes the product of the two signals
   int i;

   // Get an alias for the first descriptor
   const ComplexVector &z1 = *this;

   // Get the dimension
   int n1 = z1.cdim();
   int n2 = z2.cdim();

   // If the two dimensions are different, then this is a mistake
   if (n1 != n2) 
      {
      error_message (
	 "Correlation: Two arrays have different dimension (%d and %d)",
	  n1, n2);
      return ComplexVector();	// Return an empty vector
      }

   // Now, allocate a vector to be the difference of the two signals
   ComplexVector c(z1.cdim());

   // Subtract the two signals
   for_1Dindex (i, c)
      c[i] = z1[i] - z2[i];

   // Return result
   return c;
   }

