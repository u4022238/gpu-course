#pragma once

#include <vector>

template<class Type> class BestSampleHeap 
   {
   protected :

	std::vector<Type > items_;
	int maxlength_;
   	int length_;

	void replace_top   (Type &item);
	void add_at_bottom (Type &item);
	int inline child1_of (int n) { return 2*n+1;}
	int inline child2_of (int n) { return 2*n+2;}
 	int inline parent_of (int n) { return (n-1)/2; }

   public :

    	bool add_item (Type &item);
    	bool pop_item (Type *item);
    	void clear () { length_ = 0;} 

	// Constructors
	BestSampleHeap (int maxlength)
	   {
	   maxlength_ = maxlength;
	   length_ = 0;
	   items_.resize(maxlength);
	   }

	~BestSampleHeap ()
	   { }
   };

