
/* SFILE_BEGIN */
#include <stdio.h>
/* SFILE_END */

#include <time.h>
#include <Utilities/cvar.h>
#include <Utilities_CC/Timer.h>
#include <Utilities/showtime_dcl.h>
#include <string.h>

//---------------------------------------------------------

// The output file
FILE* rhTimer::tfile = (FILE *)0;
bool  rhTimer::timing_on = false;

rhTimer::rhTimer (const char *context_name)
   {
   // Turn on/off the messages temporarily during scope

   // Only do if timing is on
   if (!timing_on) return;

   // Record the context name
   strcpy (cstring_, context_name);

   // Open file if necessary
   if (tfile == (FILE *) 0)
      tfile = fopen ("TIMING_FILE.txt", "w");

   // Now write the message
   double cl = ((double) clock()) / CLOCKS_PER_SEC;
   fprintf (tfile, "%.6f Start %s\n", cl, cstring_);
   }

rhTimer::~rhTimer ()
   {
   // Only do if timing is on
   if (!timing_on) return;

   // Now write the message
   double cl = ((double) clock()) / CLOCKS_PER_SEC;
   fprintf (tfile, "%.6f End %s\n", cl, cstring_);
   }

#if 0
// ====================================================================
// OBSOLETE

rhTimer::rhTimer (const char *context_name)
   {
   // Turn on/off the messages temporarily during scope
   strcpy (cstring_, context_name);

   // Now write the message
   long tm = (long) time(0);
   fprintf (tfile, "%10ld Start %s\n", tm, cstring_);
   }

rhTimer::~rhTimer ()
   {
   // Now write the message
   long tm = (long) time(0);
   fprintf (tfile, "%10ld End %s\n", tm, cstring_);
   }

====================================================================
#endif
