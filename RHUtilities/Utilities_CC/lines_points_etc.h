// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)lines_points_etc.h	1.5 12/02/96

//
// Various geometrical entities
//
#ifndef lines_points_etc_h
#define lines_points_etc_h

#include <Utilities_CC/utilities_CC.h>

class rhQuaternion;

namespace rh {

class rhPoint2D : public rhVector
   {
   public :

	// Constructor
	rhPoint2D () : rhVector (3) 
		{ (*this)[0] = (*this)[1] = (*this)[2] = 0.0; }

	// Constructor, given the three points
	rhPoint2D (double u, double v, double w = 1.0) : rhVector (3)
		{ (*this)[0] = u; (*this)[1] = v; (*this)[2] = w; }

   	// Constructor given a vector
	rhPoint2D (const rhVector &v) : rhVector (v) {}

	// Line through two points
	class rhLine2D operator ^ (const rhPoint2D &point2) const;

	// Get the two coordinates
	double u() const { return (*this)[0]/(*this)[2]; }
	double v() const { return (*this)[1]/(*this)[2]; }

	// Return homogeneous and non-homogeneous vectors
	rhVector uv () const { return rhVector (u(), v()); }
	rhVector uvw() const { return *this; }

	// Normalize a point so that c = 1.0
	rhPoint2D standardized () const;

	// Project a line to a 3D line
	class rhLine3D project (const class rhCameraMatrix &M) const;

	// Test if point is valid point
	bool is_valid () const { return 
		(*this)[0] != 0.0 ||
		(*this)[1] != 0.0 ||
		(*this)[2] != 0.0; }
   };

class rhLine2D : public rhVector
   {
   public :
	
	// Constructor
	rhLine2D () : rhVector (3) 
		{ (*this)[0] = (*this)[1] = (*this)[2] = 0.0; }

	// Constructor, given the three points
	rhLine2D (double a, double b, double c) : rhVector (3)
		{ (*this)[0] = a; (*this)[1] = b; (*this)[2] = c; }

   	// Constructor given a vector
	rhLine2D (const rhVector &v) : rhVector (v) {}

	// Intersection of two lines
	class rhPoint2D operator ^ (const rhLine2D &line2) const;

	// Normalize a line so that a^2 + b^2 = 1
	rhLine2D standardized () const;

	// Project a line to a 3D plane
	class rhPlane3D project (const class rhCameraMatrix &M) const;
   };

class rhVector2D : public rhVector
   {
   public :

	// Constructor
	rhVector2D () : rhVector (2) {}

	// Constructor, given the two coordinates
	rhVector2D (double x, double y) : rhVector (2)
		{ 
		(*this)[0] = x; 
		(*this)[1] = y; 
		}

   	// Constructor given a vector
	rhVector2D (const rhVector &v) : rhVector (v) {}

	// Get the two coordinates
	double x() { return (*this)[0]; }
	double y() { return (*this)[1]; }
   };

class rhVector3D : public rhVector
   {
   public :

	// Constructor
	rhVector3D () : rhVector (3) {}

	// Constructor, given the three points
	rhVector3D (double x, double y, double z) : rhVector (3)
		{ 
		(*this)[0] = x; 
		(*this)[1] = y; 
		(*this)[2] = z; 
		}

   	// Constructor given a vector
	rhVector3D (const rhVector &v) : rhVector (v) {}

	// Get the two coordinates
	double x() { return (*this)[0]; }
	double y() { return (*this)[1]; }
	double z() { return (*this)[2]; }
   };

class rhPoint3D : public rhVector
   {
   public :

	// Constructor
	rhPoint3D () : rhVector (4) 
		{
		// Set initially to invalid value
		(*this)[0] = (*this)[1] = (*this)[2] = (*this)[3] = 0.0;
		}

	// Constructor, given the three points
	rhPoint3D (double x, double y, double z, double t = 1.0) : rhVector (4)
		{ 
		(*this)[0] = x; 
		(*this)[1] = y; 
		(*this)[2] = z; 
		(*this)[3] = t;
		}

   	// Constructor given a vector
	rhPoint3D (const rhVector &v) : rhVector (v) {}

	// Line through two points
	class rhLine3D operator ^ (const rhPoint3D &point2) const;

	// Get the two coordinates
	double x() { return (*this)[0]/(*this)[3]; }
	double y() { return (*this)[1]/(*this)[3]; }
	double z() { return (*this)[2]/(*this)[3]; }

	// Normalize a point so that c = 1.0
	rhPoint3D standardized () const;

	// Transform a point via a 3D transform
	class rhPoint3D transform (const class rhTransform3D &) const;

	// Transform a point via a camera transform
	class rhPoint2D transform (const class rhCameraMatrix &) const;

	// Test if point is valid point
	bool is_valid () const { return 
		(*this)[0] != 0.0 ||
		(*this)[1] != 0.0 ||
		(*this)[2] != 0.0 ||
		(*this)[3] != 0.0 ; }

   };

class rhLine3D : public rhMatrix
   {
   public :

	// Constructor : Lines are stored as pairs of points
	rhLine3D () : rhMatrix(2, 4) {}

	// Line specified by two points
	rhLine3D (const rhPoint3D &p1, const rhPoint3D &p2) : rhMatrix(2, 4)
		{ 
		this->setrow(0, p1);
		this->setrow(1, p2);
		}

   	// Constructor given a vector
	rhLine3D (const rhMatrix &m)  : rhMatrix (m) {}

	// Line specified by two planes
	rhLine3D (const rhPlane3D &p1, const rhPlane3D &p2);

	// Intersection of a line and a plane
	class rhPoint3D operator ^ (const rhPlane3D &plane) const;

	// Transform a line via a 3D transform
	class rhLine3D transform (const class rhTransform3D &) const;

	// Transform a line via a camera transform
	rhLine2D transform (const rhCameraMatrix &) const;

	// Return the two points on a line
	class rhPoint3D p1 () const { return rhPoint3D(this->row(0)); };
	class rhPoint3D p2 () const { return rhPoint3D(this->row(1)); };
   };

class rhPlane3D : public rhVector
   {
   public :

	// Constructor
	rhPlane3D () : rhVector (4) {}

	// Constructor, given the three points
	rhPlane3D (double x, double y, double z, double t = 1.0) : rhVector (4)
		{ 
		(*this)[0] = x; 
		(*this)[1] = y; 
		(*this)[2] = z; 
		(*this)[3] = t;
		}

   	// Constructor given a vector
	rhPlane3D (const rhVector &v) : rhVector (v) {}

	// Line intersection of two planes
	class rhLine3D operator ^ (const rhPlane3D &plane2) const;

	// Intersection of a line and a plane
	class rhPoint3D operator ^ (const rhLine3D &line) const;

	// Normalize a point so that c = 1.0
	rhPlane3D standardized () const;
   };

class rhTransform2D : public rhMatrix
   {
   public :

	// Constructor
	rhTransform2D () : rhMatrix (3, 3) {}

   	// Constructor given a vector
	rhTransform2D (const rhMatrix &v) : rhMatrix (v) {}

	// Transform a point
 	class rhPoint2D operator () (const rhPoint2D &p) const;
	class rhPoint2D operator * (const rhPoint2D &p) const;

	// Transform a line
 	class rhLine2D operator () (const rhLine2D &l) const;
   };

class rhTransform3D : public rhMatrix
   {
   public :

	// Constructor
	rhTransform3D () : rhMatrix (4, 4) {}

   	// Constructor given a vector
	rhTransform3D (const rhMatrix &v) : rhMatrix (v) {}

	// Transform a point
 	class rhPoint3D operator () (const rhPoint3D &p) const;

	// Transform a line
 	class rhLine3D operator () (const rhLine3D &l) const;

	// Transform a plane
 	class rhPlane3D operator () (const rhPlane3D &p) const;

        // Rotaion part
        rhMatrix rotation ();

        // Translation part
        rhVector3D translation ();

        // Inverse transformation
        rhTransform3D inverse ();
   };

class rhCameraMatrix : public rhMatrix
   {
   public :

	// Constructor
	rhCameraMatrix () : rhMatrix (3, 4) {}

   	// Constructor given a vector
	rhCameraMatrix (const rhMatrix &v) : rhMatrix (v) {}

	// Transform a point
 	class rhPoint2D operator () (const rhPoint3D &p) const
		{ return p.transform (*this); }

	// Transfer a line
 	class rhLine2D operator () (const rhLine3D &l) const
		{ return l.transform (*this); }
   };


// Other functions

// Distance from point to line in 2D
double distance (const rhPoint2D &p, const rhLine2D &l);
inline double distance (const rhLine2D &l, const rhPoint2D &p) 
		{ return distance (p, l); }

// Distance from point to a plane in 3D
double distance (const rhPoint3D &point, const rhPlane3D &plane);
inline double distance (const rhPlane3D &plane, const rhPoint3D &point)
		{ return distance (point, plane); }

// Distances between points
inline double distance (const rhPoint3D &p1, const rhPoint3D &p2)
		{ return ((p1-p2).length()); }

inline double distance (const rhPoint2D &p1, const rhPoint2D &p2)
		{ return ((p1.uv()-p2.uv()).length()); }

// Line through two points
inline rhLine2D rhPoint2D::operator ^ (const rhPoint2D &point2) const
   { return cross_product (*this, point2); }

// Intersection of two lines
inline rhPoint2D rhLine2D::operator ^ (const rhLine2D &line2) const
   { return cross_product (*this, line2); }

// Line through two points
inline rhLine3D rhPoint3D::operator ^ (const rhPoint3D &point2) const
   { return rhLine3D (*this, point2); }

// Intersection of a line and a plane
inline class rhPoint3D rhLine3D::operator ^ (const rhPlane3D &plane) const
   { return plane ^ (*this); }

// Rotation about an axis
rhTransform3D Rotation3D (const rhVector3D &axis, double angle);
rhTransform3D Rotation3D (const rhQuaternion &q);

} using namespace rh;


#endif
