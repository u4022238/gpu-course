#include "Utilities_CC/pyramid.h"
#include "Tjinterface/imageio.h"

int main (int argc, char *argv[])
   {
   // Test out the pyramid

   // Turn off the informative messages 
   enable_informative_messages ();
 
   // Get the parameters
   // Skip over the program name
   char *program_name = argv[0];
   int num_pyramid_levels = 5;         // Seems like a good number
   const int horizontal = 1;
   const int vertical = 0;
   int arrangement = vertical;

   argv++; argc--;
 
   // Read the parameters
   while (argc > 0)
      {  
      if (argv[0][0] != '-') break;
 
      // parse the option
      switch (argv[0][1])
         {
         case 'n' :
            {
            // Specifies the method to be used
  	         num_pyramid_levels = atoi(argv[0]+2);
            break;
            }
         case 'h' :
            {
            // Specifies the method to be used
            arrangement = horizontal;
            break;
            }
         case 'v' :
            {
            // Specifies the method to be used
  	         arrangement = vertical;
            break;
            }
         default :
            {
            error_message ("%s : Unknown option \"%s\"",
                program_name, argv[0]);
            exit (1);
            break;
            }
         }

      // Skip to the next argument
      argv++; argc--;
      }  

   // Check for the arguments
   if (argc != 2)
      {
      error_message ("Usage : pyramid [-nxx] [-h] [-v] infilename outfilename");
      bail_out (1);
      }

   // Get the input file names
   char *infname = argv[0];
   char *outfname = argv[1];

   // Now, read the image
   Unsigned_char_matrix R, G, B;
   for (int band=1; band<=3; band++)
      {
      // Do one band at a time
      Unsigned_char_matrix A;
      bool success = read_pix_band_to_array (band, infname, A);
      // bool success = read_png (A, infname);
      if (! success)
         fatal_error_message ("Cannot read image file \"%s\" for input", 
               infname);

      // Change this to an unsigned char image
      // Find the maximum and minimum pixels
      unsigned short pmax = A[0][0];
      unsigned short pmin = A[0][0];
      for2Dindex (i, j, A)
         {
         unsigned short val = A[i][j];
         if (val < pmin) pmin = val;
         if (val > pmax) pmax = val;
         }

      float scale = 1.0 / (pmax - pmin);

      // Now, transfer it to a double image
      Pyramid_Image_type fA(A.bounds());
      for2Dindex (i, j, A)
         fA[i][j] = scale *  (A[i][j] - pmin);

      // Make the pyramid image
      Pyramid_Image_type *gA = gaussian_pyramid (fA, num_pyramid_levels);
      Pyramid_Image_type cA = composite_image 
         (gA, num_pyramid_levels, arrangement);

      // Now, make it back into an unsigned char matrix
      Unsigned_char_matrix Aout (cA.bounds());
      for2Dindex (i, j, Aout)
         Aout[i][j] = (unsigned char) (255 * cA[i][j]);

      // This is Red, green or blue band
      switch (band)
         {
         case 1: R = Aout; break;
         case 2: G = Aout; break;
         case 3: B = Aout; break;
         }
      }

   // Finally, write out this image
   write_png (outfname, R, G, B);

   // Success
   return EXIT_SUCCESS;
   }
