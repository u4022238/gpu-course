#include <vector>
using namespace std;
#include <Utilities_CC/SmartPointer.h>
#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/KDTree2.h>

static rhFloatVector householder_rotate (rhFloatVector u, rhFloatVector x)
   {
   return x - u * (2.0 * (u * x));
   }

// Constructors, etc
KDTree2::KDTree2 (vector<rhFloatVector> &vecs, int ntrees, bool build) : 
	KDTree (vecs, false), // No build
	num_trees_ (ntrees)

   {
   // We need to build the tree here
   int i, j;

   if (build)
      {
      // Get some dimensions
      int npoints = points_.size();
      int nodes_per_tree = 2*npoints;

      node_ = new KDNode[nodes_per_tree * ntrees];
      tree_ = new KDNode * [ntrees];
      for (i=0; i<num_trees_; i++)
         tree_[i] = &(node_[i*nodes_per_tree]);

      // Set up the array for point ordering -- this only needs to
      // be a permutation.  It will be reordered by the BuildKDTree routine
      Int_vector pointorder (npoints);
      for_1Dindex (i, pointorder)
         pointorder[i] = i;

      // Get a bunch of random directions
      directions_.resize(num_trees_);

      // Fill out the directions -- first one is no rotation
      for (i=0; i<num_trees_; i++)
         {
         // Make a random direction
         rhFloatVector dir(point_dimension_);
         for_1Dindex (j, dir)
	         dir[j] = (float) grandom(0.0, 1.0);
         dir.normalize();

         directions_[i] = dir;
         }
      directions_[0].clear(0.0);
   
      // Build the tree
      vector<rhFloatVector> vec_rotated (npoints);
      for (i=0; i<num_trees_; i++)
         {
         // Rotate all the points
         for (j=0; j<npoints; j++)
	    vec_rotated[j] = householder_rotate (directions_[i], points_[j]);
	 
         BuildKDTree (
	    vec_rotated, tree_[i], first_node_, pointorder, 0, npoints-1);
         }
      }
   }

KDTree2::~KDTree2 ()
   {
   // Delete temporaries
   if (priority_queue_) delete priority_queue_;
   if (node_) delete [] node_;
   if (tree_) delete [] tree_;
  
   // Set them to zero so they are not released again
   priority_queue_ = NULL;
   node_ = NULL;
   tree_ = NULL;
   }

int KDTree2::find_nearest_neighbour (rhFloatVector goal)
   {
   // Finds the nearest neighbour
   int i;

   // Keep count of points found
   points_found_ += 1;

   // First of all, rotate all the goals
   vector<rhFloatVector> rotated_goal(num_trees_);
   for (i=0; i<num_trees_; i++)
      rotated_goal[i] = householder_rotate (directions_[i], goal);

   // First, set up the priority queue
   priority_queue_ = new PriorityQueue();

   // First of all, find the base region in all the trees
   int best_point_num;
   double mindist;
   for (i=0; i<num_trees_; i++)
      {
      // Find the best node in the i-th tree
      int new_point_num = find_base_point (
	 tree_[i], rotated_goal[i], first_node_);

      // Get the new point and its distance 
      rhFloatVector new_point = points_[new_point_num];
      double newdist = distance(new_point, goal);

      // Get the new point
      if (i==0 || newdist < mindist)
	 {
	 best_point_num = new_point_num;
	 mindist = newdist;
	 }
      }

   // Now, schedule some nodes for searching
   for (i=0; i<num_trees_; i++)
      schedule_node_search (i, first_node_, 
	rotated_goal[i], rotated_goal[i], 0.0, mindist);

   // Now go into a loop of searching
   for (int numtrials = 0; numtrials != max_trials_; numtrials++)
      {
      // If max_trials_ = -1 (default) this will loop until completion

      // Searching ----------------------------------------------

      // Get the next node
      int search_node;
      double search_node_distance;
      rhFloatVector nearest_point;
      int treenum;
      bool found = get_next_node (
	 &treenum, &search_node, &search_node_distance, &nearest_point);

      // Exit from here if no more nodes found, or it is too far away
      if (! found || search_node_distance > mindist) break;

      // Now, search on this node
      int new_point_num = find_base_point (
	   tree_[treenum], rotated_goal[treenum], search_node);
      rhFloatVector newpoint = points_[new_point_num];
      double newdist = distance(newpoint, goal);

      // See if this is the best node
      if (newdist < mindist)
	 {
	 mindist = newdist;
	 best_point_num = new_point_num;
	 }

      // Scheduling ---------------------------------------------
      schedule_node_search ( 
	treenum,
	search_node, 
	rotated_goal[treenum], 
	nearest_point, search_node_distance, mindist);
      }

   // Delete the priority queue
   delete priority_queue_;

   // Return the number of the nearest neighbour
   return best_point_num;
   }

