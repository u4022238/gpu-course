#ifndef _lp_cc_dcl_
#define _lp_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include <Utilities_CC/utilities_CC.h>

// #define rhDEBUG

// Error Messages
#define  LP_Success                  0
#define  LP_Unbounded                1
#define  LP_Infeasible               10
#define  LP_IPEqZero                 11
#define  LP_InsufficientConstraints  12
#define  LP_InsufficientRank         13
#define  LP_Feasible		     14
/* */

FUNCTION_DECL (void LP_set_feasibility_only, ( bool val ));

FUNCTION_DECL (bool LP_get_feasibility_only, ( ));

FUNCTION_DECL (void print_lpp_problem, (
   FILE *outfile,
   rhMatrix &A,
   rhVector &b,
   rhVector &goal
   ));

FUNCTION_DECL( rhVector lp_constrained , (
             rhMatrix A, 
             rhVector b, 
             rhMatrix Aeq,
             rhVector beq,
             rhVector goal, 
             int *flag
             ));

FUNCTION_DECL(rhVector lp_solve , (
             rhMatrix A, 
             rhVector b, 
             rhMatrix Aeq,
             rhVector beq,
             rhVector goal, 
             int *flag
             ));

FUNCTION_DECL( rhVector lp_constrained , (
             rhMatrix A, 
             rhVector b, 
             rhVector goal, 
             int *flag
             ));

FUNCTION_DECL(rhVector lp_solve , (
             rhMatrix A, 
             rhVector b, 
             rhVector goal, 
             int *flag
             ));

#endif
