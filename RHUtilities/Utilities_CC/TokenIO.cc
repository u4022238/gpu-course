#include <Utilities_CC/TokenIO.h>

TokenIO::TokenIO ()
   {
   // Constructor -- sets things up

   // Store the file
   infile_ = NULL;

   // Store the current file name
   currentinput_ = NULL;

   // Initialize the file stack
   filestack_ = lst_init();

   // Set the default comment character
   comment_char = '#';

   // Make array of tokens
   token = new char * [MAXTOKENS];

   // Set counters to initial values
   NR = 0;
   NF = 0;

   // Initially we do not pop on EOF
   pop_on_EOF_ = false;
   }

TokenIO::~TokenIO()
   {
   // Cleans up and terminates the io routines 
   // Does not close the file, however 
   infile_ = NULL;

   // Frees the "currentinput" string
   if (currentinput_) free (currentinput_);

   // Deletes the file stack
   if (filestack_)
      lst_free (filestack_);

   // Free the tokens -- range 0 to NF-1 
   for (int i=0; i<NF; i++)
     free (token[i]);
   delete [] token;
   }

int TokenIO::Init (char *fname)
   {
   // Try opening the file of this name
   FILE *infile = fopen (fname, "r");
 
   // If failure, then make a message and return 0
   if (!infile) 
      {
      error_message ("TokenIO: Can not open file \"%s\" for read", fname); 
      return 0;
      }

   // Initialize the file
   infile_ = infile;
   currentinput_ = COPY(fname);

   return 1;
   }

void TokenIO::set_comment_char (char new_comment_char)
   {
   // Set the character that marks the begining of a comment
   //	to what ever is passed in.  The line starting with
   //	new_comment_char will be skipped now on 

   comment_char = new_comment_char;
   }

char TokenIO::get_comment_char ()
   {
   // Returns the character that marks the begining of a comment. 
   return comment_char;
   }

int TokenIO::switchinput (char *newfname)
   {
   // Switches to a new input file

   // First, try to open the file
   FILE *newfile = fopen (newfname, "r");
   if (! newfile)
      {
      error_message ("TokenIO: Can not open file \"%s\" for read", newfname); 
      return 0;
      }

   // New data structure to put on the list
   TokenIOContext *newdata = new TokenIOContext;
   newdata->NR = NR;
   newdata->currentinput = currentinput_;
   newdata->infile = infile_;
   newdata->pop_on_EOF = pop_on_EOF_;

   // Put on the file stack
   if (filestack_ == NULL) filestack_ = lst_init();
   lst_push (newdata, filestack_);

   // Now switch to the new file
   NR = 0;
   currentinput_ = COPY(newfname);
   infile_ = newfile;
   pop_on_EOF_ = false;

   // If we got this far, then the switch worked
   return 1;
   }

int TokenIO::pushinput (char *newfname)
   {
   // Pushes to a new input file, with intention to return to old

   // Try switching
   int success = switchinput(newfname);

   // If success, then set to pop
   if (success) 
      pop_on_EOF_ = true;

   // Return 1 if the push succeeded
   return success;
   }

bool TokenIO::popinput ()
   {
   // Get the old context off the stack 
   TokenIOContext *oldcontext = (TokenIOContext *) lst_pop(filestack_);

   // If stack is empty, then return false 
   if (oldcontext == NULL)
      return false;

   // Otherwise, close the file, restore old context 
   fclose (infile_);
   free (currentinput_);
   NR            = oldcontext->NR;
   currentinput_ = oldcontext->currentinput;
   infile_       = oldcontext->infile;
   pop_on_EOF_   = oldcontext->pop_on_EOF;
   delete oldcontext;

   // Return success 
   return true;
   }
   
int TokenIO::reporterror ()
   {
   fprintf (stderr, "Bad line : File %s line %d\n", currentinput_, NR);
   fprintf (stderr, "%s\n", line);
   if (filestack_ && ! lst_isempty (filestack_))
      {
      fprintf (stderr, "Traceback :\n");
      lst_forall (TokenIOContext *, afile, filestack_)
         {
         fprintf (stderr, "\tFile %s line %d\n", 
		   afile->currentinput, afile->NR);
         } lst_endall
      }
   return 1;
   }

int TokenIO::nextline ()
   {
   // Gets the next line and splits it into tokens.
   // Returns EOF on end-of-file 

   // Get the next line -- if no line, return EOF
   if (getline(line)== EOF)
      return EOF;

   // Break into tokens
   gettokens();

   // Return success 
   return 1;
   }

int TokenIO::getline (char *line)
   {
   // Get the next line skipping comment lines and blank lines 
   // Returns EOF on end-of-file 
   char *p = line;
   int c;

   // Skip over blanks, counting new lines if necessary
   while ((c = getc(infile_)) == ' ' || c == '\t' || c == '\n')
      if (c == '\n') NR++;

   // Back up to before the new non-blank character
   ungetc (c, infile_);

   // Now, read characters into buffer, until end of line or EOF
   while ((c = getc(infile_)) != '\n' && c != EOF) *(p++) = c;

   // If we get EOF, then need to pop input or return EOF
   if (c == EOF) 
      {
      if (pop_on_EOF_)
	 {
	 popinput();
	 return getline (line);
	 }

      else
         return EOF;
      }
   
   // Otherwise, we have a new line -- replace newline by null character
   *p = '\0';
   NR ++;

   // But if line starts with a comment character, then try next line
   if (line[0] == comment_char) return (getline(line));
   
   // See if the line ends with a '\' 
      {
      char *q = p-1;

      // Run backwards to find the last non-blank character 
      while (*q == ' ' || *q == '\t') q--;
	
      if (*q == '\\')
	 {
	 // Continue the line 
	 *q = ' ';
	 return (getline (q+1));
	 }
      
      else return 1;
      }
   }

void TokenIO::gettokens ()
   {
   // Divides the line into tokens 
   char *p = line;

   // Free the previous tokens 
      {
      int i;
      for (i=0; i<NF; i++)
	 free (token[i]);
      }

   // Reset NF to zero 
   NF = 0;

   // Take the other tokens 
   while (1)
      {
      char save;
      char *start = p;

      // Skip to the next blank 
      while (*p != ' ' && *p != '\t' && *p != '\0') p++;
	
      // Set this think to a null 
      save = *p;
      *p = '\0';

      // Copy the word 
      token[NF++] = COPY(start);
     *p = save;
      
      // Skip to the next non-blank 
      while ((*p == ' ' || *p == '\t') && (*p != '\0')) p++;
      if (*p == '\0') break;
      }
   }

int TokenIO::ECHO ()
   {
   printf ("%s\n", line);
   return 1;
   }

