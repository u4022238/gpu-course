#ifndef _SMART_POINTER_H__
#define _SMART_POINTER_H__

// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>
// ---------------------------------------------------------------------------

//
// Builds off the ReferenceCountable to give smart pointers
//
#include <Utilities_CC/ReferenceCountable.h>

//--------------------------------------------------------------------

#ifdef _HERE_IS_AN_EXAMPLE_

	// Here is an example of how to make a smart pointer class.
	// A smart pointer can be made from any class that subclasses off
	// ReferenceCountable.  For example, image.
	// Note : This code is just for example, and is commented out

	typedef SmartPointer<Image> Image_ref;

	// To instantiate create a .C file in the Templates directory of
	// the class you want the smart pointer of.  In the file do this:

	// start SmartPointer+Image-.C 
	#include <Image/Image.h>
	#include <Utilities_CC/SmartPointer.h>

        INSTANTIATE_RHSMART_PTR(Image)
	// end SmartPointer+Image-.C 

	// If you use smart pointers, then you should not get memory leaks

#endif

//--------------------------------------------------------------------

template <class T>
class SmartPointer 
{
private:

  bool _protected;	
  T* ptr_;

public:

  SmartPointer () 
  {
    _protected = true;
    ptr_ = 0;
  }

  SmartPointer (const SmartPointer<T> &p)
  { 
    _protected = true;
    ptr_ = p.ptr_; 
    ref(); 
  }
  
  SmartPointer (T *p)
  { 
    _protected = true;
    ptr_ = p; 
    ref(); 
  }				

  ~SmartPointer ()
  {
    unref();
  }

  T *operator -> () const
  { 
    return ptr_; 
  }

  T *ptr () const 
  { 
    // This returns the pointer.  
    return ptr_; 
  }

  // -- used for getting around circular references
  void UnProtect()
  {
    unref();
  }

  // Comparison of pointers
  bool operator < (const SmartPointer &r)
  { return (void*)ptr_ < (void*) r.ptr_; }

  bool operator > (const SmartPointer &r)
  { return (void*)ptr_ > (void*) r.ptr_; }

  bool operator <= (const SmartPointer &r)
  { return (void*)ptr_ <= (void*) r.ptr_; }

  bool operator >= (const SmartPointer &r)
  { return (void*)ptr_ >= (void*) r.ptr_; }

  SmartPointer &operator = (const SmartPointer &r)
  { 
    return this->operator = (r.ptr()); 
  }
  
  SmartPointer &operator = (T *r)
  {								 
    if (ptr_ != r)
      {
	unref();
	ptr_ = r;
	ref();
      }
    return *this;
  }

  // Dereferencing the pointer
  T &operator * () const { return *ptr_; }

  // Cast to T * 
  operator T * () const { return ptr_; }

private:

  void ref();
  void unref();
};

//---------------------------------------------------------------
// Macro for instantiating smart pointers
#define INSTANTIATE_RHSMART_PTR(Type)				\
   template <>								\
   void SmartPointer<Type>::ref()				\
     {								\
       if (ptr_) 						\
         {							\
	   ptr_->Protect(); 					\
	   _protected = true;					\
         }							\
     }								\
   template <>								\
   void SmartPointer<Type>::unref()				\
     { 								\
       if (ptr_ && _protected) 					\
         {							\
	   ptr_->UnProtect(); 					\
	   _protected = false; /* sure we do not do it twice */	\
         }							\
     }		 						\
								\
   template class SmartPointer<Type>;				\
   								
//---------------------------------------------------------------

#endif
