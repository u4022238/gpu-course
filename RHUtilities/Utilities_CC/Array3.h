// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>


//-----------------------------------------
//
//   Various 3D array classes
//
//-----------------------------------------

#include <Utilities/utilities_c.h>
#include <Utilities_CC/Matrix.h>

#ifndef for_3Dindex
#define for_3Dindex(i, j, k, M) 			\
   for (i=M.ilow(); i<=M.ihigh(); i++)			\
      for (j=M.jlow(); j<=M.jhigh(); j++)		\
         for (k=M.klow(); k<=M.khigh(); k++)	
#endif

class array3_type
   {
   private:

	int Ilow, Jlow, Klow;
        int Ihigh, Jhigh, Khigh;

        element_type ***Val;
        unsigned int *reference_count;

        void dereference ()
           {
           if (reference_count)
              {  
              // First unlink the thing
              *reference_count -= 1;

              // Now, if reference count has fallen to zero, then free
              if (*reference_count <= 0)
                 {
		 if (Val != (element_type ***) 0) 
                    rhFREE_macro (Val, Ilow);

                 delete reference_count;
                 }
              }   

            // Set Val and reference count to initial values
            Val = (element_type ***) 0;
            reference_count = (unsigned int *)0;
            Ihigh = Jhigh = Khigh = Ilow = Jlow = Klow = 0;
            }

   public:

	// For getting the bounds of the array
	int ilow() const { return Ilow; }
	int jlow() const { return Jlow; }
	int klow() const { return Klow; }
	int ihigh() const { return Ihigh; }
	int jhigh() const { return Jhigh; }
	int khigh() const { return Khigh; }
	int idim () const { return Ihigh - Ilow + 1; }
	int jdim () const { return Jhigh - Jlow + 1; }
	int kdim () const { return Khigh - Klow + 1; }

	//
	// For indexing into the array
	//
	int defined_at (int i, int j, int k) const
	   { return i >= Ilow && i <= Ihigh && 
	   	    j >= Jlow && j <= Jhigh &&
	   	    k >= Klow && k <= Khigh; }

	// For indexing into the array without checking
	 element_type ** operator[] (int i) const { return Val[i]; }

	// For getting a handle on the array itself
	element_type ***data () const { return Val; }

 	// Basically the same thing, but using casting
 	operator element_type *** () const { return Val; }

	// Constructors
	array3_type ()	
		{ 
		Ilow = Jlow = Klow = 0;
		Ihigh = Jhigh = Khigh = -1;
		Val = (element_type ***) 0; 
		reference_count = (unsigned int *)0;
		}

	array3_type (
	   int ilow, int ihigh, 
	   int jlow, int jhigh, 
	   int klow, int khigh)
		{ 
		Val = (element_type ***) 0;
		reference_count = (unsigned int *)0;
		Init (ilow, ihigh, jlow, jhigh, klow, khigh);
		}

	array3_type (int idim, int jdim, int kdim)
		{ 
		Val = (element_type ***) 0;
		reference_count = (unsigned int *)0;
		Init (idim, jdim, kdim); 
		}

	array3_type (const class array3_type &A)
		{
		// Copy constructor
		Val = (element_type ***) 0;
		*this = A;
		}

	array3_type (int idim, int jdim, int kdim, element_type *vals)
	   	{
   		// 3D array from an array of element_type starting at 0
		Val = (element_type ***) 0;
		reference_count = (unsigned int *)0;
   		Init(idim, jdim, kdim);
   		array3_type &ten = *this;

   		int i, j, k, count = 0;
   		for_3Dindex (i, j, k, ten)
      		  ten[i][j][k] = vals[count++];
   		}

	void Init(int ilow, int ihigh, int jlow, int jhigh, int klow, int khigh)
	      	{
		// First, dereference this array
		dereference ();

		// Set the bounds
	        Ilow = ilow;
	        Ihigh = ihigh;
	        Jlow = jlow;
	        Jhigh = jhigh;
	        Klow = klow;
	        Khigh = khigh;

		Val = ARRAY3D 
		   (ilow, ihigh, jlow, jhigh, klow, khigh, element_type);

		// Also the reference count
                reference_count = new unsigned int;
                *reference_count = 1;
		}

	void Init (int idim, int jdim, int kdim)
	      	{
		Init (0, idim-1, 0, jdim-1, 0, kdim-1);
		}

	// Destructors
	void Dealloc () { dereference (); }
	~array3_type() { dereference (); }

	// Assignment operator
        array3_type &operator = (const array3_type &avec)
           {
           // Increment the reference count
           if (avec.Val)
              {
              *(avec.reference_count) += 1;
              }
 
           // Dereference the current list
           dereference ();
 
           // Copy the fields
	   Ilow = avec.Ilow;
	   Jlow = avec.Jlow;
	   Klow = avec.Klow;
           Ihigh = avec.Ihigh;
	   Jhigh = avec.Jhigh;
	   Khigh = avec.Khigh;
           reference_count = avec.reference_count;
           Val = avec.Val;
 
           // Return the pointer itself
           return *this;
           }

	// Other actions

	void clear (element_type val = 0.0) const
		{ 
		for (int i = Ilow; i <= Ihigh; i++)
		   for (int j = Jlow; j <= Jhigh; j++)
		      for (int k = Klow; k<=Khigh; k++)
		          Val[i][j][k] = val;
		}

	array3_type copy () const
		{
		// Copy the array including the elements
		int i, j, k;
		const array3_type &A = *this;
	        array3_type B (A.idim(), A.jdim(), A.kdim());
	        for_3Dindex (i, j, k, B)
	 	   B[i][j][k] = A[i][j][k];
	        return B;
		}

        // Finding maximum and minimum values
        element_type max () const;
        element_type min () const;
        element_type sum () const;
        element_type sumsq () const;

	void print (FILE *afile = stdout, const char *format = "%15.6e") const
	   {
	   for (int i = Ilow; i<=Ihigh; i++)
	      {
	      for (int j=Jlow; j<=Jhigh; j++)
	 	 {
	         for (int k=Klow; k<=Khigh; k++)
		    fprintf (afile, format, (*this)[i][j][k]);
		 fprintf (afile, "\n");
		 }

	      fprintf (afile, "\n");
	      }
	   }

   };
	      
