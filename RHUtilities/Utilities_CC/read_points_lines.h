// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// @(#)read_points_lines.h	1.3 10/01/95

#ifndef read_points_lines_h
#define read_points_lines_h

//
// General purpose program for reading input files
//
//

// Caution : All arrays start at 1, except those indexed by view-number,
//           which start at 0.  (Illogical isn't it).

#include <Utilities_CC/utilities_CC.h>
#include <Utilities/cvar.h>

FUNCTION_DECL (int read_standard_input_file, (
   FILE *afile,			/* The file to read from */
   rhVector *&u,		/* U coordinates to be allocated and read */
   rhVector *&v,		/* V coordinates to be allocated and read */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates to be allocated and read */
   Boolean_matrix &defined	/* A matrix telling which points are defined */
   ));

FUNCTION_DECL (int select_points_from_specified_views, (

   /* Input */
   rhVector *&u,		/* U coordinates of all points in all views */
   rhVector *&v,		/* V coordinates of all points in all views */
   rhVector &x, rhVector &y, 
   rhVector &z,		/* Point coordinates of all points */
   Boolean_matrix &defined,	/* A matrix telling which points are defined */
   Int_vector &selected_view,	/* A selected set of views */

   /* Output */
   rhVector *&uout,		/* U coordinates to be selected from u */
   rhVector *&vout,		/* V coordinates to be selected from v */
   rhVector &xout, rhVector &yout, 
   rhVector &zout,	/* Point coordinates to be selected from x, y, z */
   Int_vector &selected_point	/* The points that were selected */

   ));

 
FUNCTION_DECL (int select_points_from_specified_views_replace, (
 
   /* Input & Output */
   rhVector *&u,               /* U coordinates of all points in all views */
   rhVector *&v,               /* V coordinates of all points in all views */
   rhVector &x, rhVector &y,
   rhVector &z,                /* Point coordinates of all points */
   Boolean_matrix &defined,     /* A matrix telling which points are defined */
   Int_vector &selected_view,   /* A selected set of views */
   Int_vector &selected_point   /* The points that were selected (returned) */
   ));

FUNCTION_DECL (int write_standard_input_file, (
   FILE *afile,                 /* The file to read from */
   rhVector *&u,               /* U coordinates to be allocated and read */
   rhVector *&v,               /* V coordinates to be allocated and read */
   rhVector &x, rhVector &y,
   rhVector &z,                /* Point coordinates to be allocated and read */
   Boolean_matrix &defined      /* A matrix telling which points are defined */
   ));


#endif
