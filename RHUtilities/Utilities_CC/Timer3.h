#ifndef _RIH_TIMER3_h_
#define _RIH_TIMER3_h_

#include <chrono>
#include <stdio.h>
#include <time.h>
#include <Utilities/utilities_c.h>
//clock_t clock(void);

// This is the type that chrono keeps its time in
// This for gnu
#ifdef GNU_CHRONO
typedef std::chrono::system_clock::time_point ChronoTimePoint;
#else
// This for windows
typedef std::chrono::steady_clock::time_point ChronoTimePoint;
#endif

class TimingEvent3
   {
   public: 
      char *name_;
      int depth_;
      long numcalls_;
      double event_processor_time_;
      ChronoTimePoint event_wall_time_;
      double total_processor_time_;
      double total_wall_time_;

      // Constructors
      TimingEvent3 (const char *name)
         {
         depth_ = 0;
         numcalls_ = 0;
	      name_ = COPY(name);
         event_processor_time_ = 0.0;
         // event_wall_time_ = 0.0;
         total_processor_time_ = 0.0;
         total_wall_time_ = 0.0;
         }

      ~TimingEvent3 ()
         {
         if (name_) free (name_);
         }
   };

//  Define a table of words
#define Tbl_TYPE TimeTable3
#define Tbl_DATA class TimingEvent3 *
#define Tbl_KEY  char *
#include "Table.h"

class rhTimer3 
   {
   protected :

        char cstring_[80];
        static bool timing_on;

        // Declare a table
        static TimeTable3 ttab;

   public :

	     rhTimer3 (const char *context_string);
	     ~rhTimer3();

        static void print_results();

        // Set timing on or off
        static void SetTimingOn  () {timing_on = true; }
        static void SetTimingOff () {timing_on = false; }
   };

#endif
