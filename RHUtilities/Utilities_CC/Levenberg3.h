
// Levenberg Marquardt classes 

#ifndef _Levenberg3_h_
#define _Levenberg3_h_

#include <Utilities_CC/utilities_CC.h>
#include <signal.h>

class Levenberg3
   {
   // Some local variables 
   protected :

      rhVector mparam;
      int Num_Loops;	// Do no more than this number of loops
      int Min_Loops;	// Must do at least this many loops
      double Min_Incr;

      // To be carried out when a new value of the parameters is found
      virtual void new_position_action ( rhVector mparam );

      // Routines for handling an interrupt to stop after next iteration
      static rhBOOLEAN interrupt_pending;
      static void keybd_interrupt (int errno);

   public : 

      // Set parameters
      void setNumLoops (int n) { Num_Loops = n; }
      void setMinLoops (int n) { Min_Loops = n; }
      void setMinIncr (double incr) { Min_Incr = incr; }

      // Set the values of the input parameters
      void setParameters (const rhVector &param) { mparam = param.copy(); }
      rhVector getParameters () const { return mparam.copy(); }

      // Constructors, etc
      Levenberg3 ();
      virtual ~Levenberg3();

      // Solve the problem
      double solve ();

      // The function to be minimized -- must be supplied in subclass
      // It also has to return the Jacobian and Hessian
      virtual rhVector function ( 
		   const rhVector &mparams, 
		   rhMatrix *f = NULL
		   ) = 0;
   };

#endif
