#ifndef _TokenIO_h_
#define _TokenIO_h_

/* Routines for doing io */

#include <Utilities/utilities_c.h>

/* The tokens in the line */
#define MAXTOKENS 512
#define LINELENGTH 8192

class TokenIO 
   {
   private :

	/* Datatypes for a stack of files */
	typedef struct TokenIOContext
   	   {
   	   char *currentinput;
   	   int NR;
   	   FILE *infile;
	   bool pop_on_EOF;
   	   } TokenIOContext;

   private :

	// Data related to current context
	char *currentinput_;	/* Name of the current input file */
	FILE *infile_;		/* The current input file */
	bool pop_on_EOF_;	/* pop or return EOF at end of current file */

	/* Local variables */
	char comment_char;	/* The input line */
	char line[LINELENGTH];	/* The input line */
	LIST *filestack_;	/* For keeping a stack of input files */

	// Methods for internal use only
	int getline (char *line);
	void gettokens ();

   public :

	// Constructors
	TokenIO ();
	~TokenIO();

	// Getting and writing a complete line
	int nextline ();
	int ECHO ();

	// The tokens and input data
	char **token;		/* The tokens */
	int NF;			/* Number of tokens in current line */
	int NR;			/* Number of the current line */

	// Setting and getting the comment character
	void set_comment_char (char new_comment_char);
	char get_comment_char ();

        // Control of the input file
	int Init (char *fname);
	int pushinput (char *newfname);
	int switchinput (char *newfname);
	bool popinput ();
   
	// Error message -- gives traceback
	int reporterror ();
	char *currentinput() const { return currentinput_; }
   };

#endif
