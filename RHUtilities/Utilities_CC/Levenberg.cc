// <begin copyright notice>
// ---------------------------------------------------------------------------
//
//                   Copyright (c) 1999 TargetJr Consortium
//               GE Corporate Research and Development (GE CRD)
//                             1 Research Circle
//                            Niskayuna, NY 12309
//                            All Rights Reserved
//              Reproduction rights limited as described below.
//                               
//      Permission to use, copy, modify, distribute, and sell this software
//      and its documentation for any purpose is hereby granted without fee,
//      provided that (i) the above copyright notice and this permission
//      notice appear in all copies of the software and related documentation,
//      (ii) the name TargetJr Consortium (represented by GE CRD), may not be
//      used in any advertising or publicity relating to the software without
//      the specific, prior written permission of GE CRD, and (iii) any
//      modifications are clearly marked and summarized in a change history
//      log.
//       
//      THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
//      WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//      IN NO EVENT SHALL THE TARGETJR CONSORTIUM BE LIABLE FOR ANY SPECIAL,
//      INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND OR ANY
//      DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
//      WHETHER OR NOT ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR ON
//      ANY THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE
//      USE OR PERFORMANCE OF THIS SOFTWARE.
//
// ---------------------------------------------------------------------------
// <end copyright notice>

// %W% %G%
//
// This gives an object-oriented Levenberg-Marquardt solver
//
// To use this, one must make a subclass, most particularly subclassing
// the function: rhVector Levenberg::function(const rhVector &);
//

#include <Utilities_CC/utilities_CC.h>
#include <Utilities_CC/Levenberg.h>

Levenberg::Levenberg () : Num_Loops (100), Min_Loops (1), Min_Incr (1.0e-2)
   {}

Levenberg::~Levenberg () 
   {}

//
// Keyboard interrupt stuff
//


rhBOOLEAN Levenberg::interrupt_pending = FALSE;

#ifndef WIN32
void Levenberg::keybd_interrupt (int errno)
   { 
   if (errno == SIGTSTP) 
      interrupt_pending = TRUE; 
   }
#endif

rhMatrix Levenberg::jacobian ( const rhVector &mparam )
   {
   // Default routine for computing the Jacobian
   // Computes the jacobian by approximation 

   // Constants to use in numeric differentiation
   const double FAC   = 1.0e-6;
   const double DELTA = 1.0e-6;

   // Evaluate the function at the current point 
   rhVector oldval = function (mparam);

   // Declare the jacobian matrix
   rhMatrix dyda (oldval.dim(), mparam.dim());

   // Now change the parameters one after another 
   int j;
   for_1Dindex (j, mparam)
      {
      // Remember the value of the j-th parameter 
      double oldparam = mparam[j];

      // Increment the j-th parameter 
      double delta = FAC * mparam[j];
      if (fabs (delta) < DELTA) delta = DELTA;
      mparam[j] += delta;

      // Now recompute the function 
      rhVector newval = function (mparam);

      // Now fill out the jacobian values 
      rhVector del = (newval - oldval) * (1.0/delta);
      dyda.setcolumn (j, del);

      // Restore the value of mparam[j] 
      mparam[j] = oldparam;
      }

   // Return the matrix
   return dyda;
   }

void Levenberg::new_position_action ( rhVector )
   { /* Default is to do nothing */ }

double Levenberg::solve ()
   {
   // Declare variables 
   double alamda = 1.0;
   double chisq = 0.0;
   rhBOOLEAN improvement = TRUE;

   // Set up the interrupt_pending value 
   interrupt_pending = FALSE;
#ifndef WIN32
   signal (SIGTSTP, /*(SIG_PF)*/ Levenberg::keybd_interrupt);
#endif

   // Now iterate 
   for (int icount=1; icount<=Num_Loops && improvement; icount++)
      {
      double increment_ratio;

      // Compute the function value 
      rhVector dy = function (mparam);

      // Compute an initial value of the error 
      chisq = dy*dy;

      // Output some debugging info 
      if (icount == 1)
         informative_message ("\n\tInitial error : chisq = %g, rms = %g", 
         chisq, sqrt(chisq/dy.dim()));

      // Compute the Jacobian 
      rhMatrix dyda = jacobian (mparam);

      // #define WRITE_JACOBIAN
#     ifdef WRITE_JACOBIAN
      fprintf (stdout, "\nJacobian (%d x %d) : \n", 
         dyda.idim(), dyda.jdim());
      dyda.print (stdout, "%11.7f ");
      fprintf (stdout, "\n JTJ (%d x %d) : \n", dyda.jdim(), dyda.jdim());
      (dyda.transpose() * dyda).print(stdout, "%9.5f ");
#     endif

      // Calculate the normal equations
      rhMatrix dyda_trans = dyda.transpose();
      rhMatrix N = dyda_trans * dyda;
      rhVector b = -dyda_trans * dy;

      // Now estimate new position with different alambda until improvement 
      do {

         // Copy the values of alpha to aprime, the augmented alpha 
         rhMatrix aprime = N.copy();
         rhVector da = b.copy();

         // Augment diagonal elements of aprime 
         for (int j=aprime.ilow(); j<=aprime.ihigh(); j++)
            aprime[j][j] *= 1.0 + (alamda);

         // Matrix solution.  Solution overwrites beta, now called da 
         int ncols = aprime.jdim();
         if (! (lin_solve_symmetric_0(aprime, da, da, ncols)
            || solve_simple_0 (aprime, da, da, ncols, ncols)))
            {
            error_message ("Can not solve normal equations -- exiting");
            bail_out (2);
            }

         //----------------------------------------
         //  Calculate new error at this position
         //----------------------------------------
         rhVector newparams = mparam + da;

         // Compute the error vector at this value 
         rhVector newdy = function (newparams);

         // Get the value of chisq resulting from this error vector 
         double newchisq = newdy * newdy;

         // Compute the proportional change in the error vector
            {
            rhVector del = newdy-dy;
            double len_del = sqrt(del*del);
            double len_dy = sqrt (chisq);
            increment_ratio = len_del / len_dy;
            }

         // Accept or reject it 
         if (newchisq < chisq)
            {
            // Success, accept this new solution 
            alamda *= 0.1;
            chisq = newchisq;

            // Accept the new parameter values 
            mparam = newparams;

            // Call any user function 
            new_position_action (mparam);

            // Signal improvement 
            improvement = TRUE;
            }
         else
            {
            // Failure, increas alamda and return. 
            alamda *= 10.0;
            improvement = FALSE;
            }
         } while ( ! improvement && increment_ratio >= Min_Incr 
            && (alamda < 1e10));

      // Output some debugging info 
      informative_message ("\talamda = %5.0e, chisq = %g, rms = %g", 
         alamda, chisq, sqrt(chisq/dy.dim()));

      // Test for an interrupt 
      if (interrupt_pending) break;

      // Test to see if the increment_ratio has been too small 
      if (icount >= Min_Loops && 
         (increment_ratio < Min_Incr || alamda >= 1e10))
         break;
      }

   // Return the value of the error 
   return chisq;
   }

rhMatrix Levenberg::covarianceMatrix (const rhVector &mparam)
   {
   rhMatrix J = jacobian (mparam);
   rhMatrix C = J.transpose() * J;
   return C;
   }
