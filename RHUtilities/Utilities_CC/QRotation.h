
#ifndef QRotation_h_
#define QRotation_h_

//  
// Class that represents a rotation as a 3-vector
//

#include "Utilities_CC/utilities_CC.h"

class QRotation : public rhVector
   {
   public : 

	// Various constructors
	QRotation ();
	QRotation (double, double, double);
	QRotation (rhVector &vec);
	QRotation (const rhMatrix &R);

#ifdef __GNUG__
	// Should not need this, but we do
	// QRotation (rhMatrix R);
#endif

	// Operators
	QRotation operator * (QRotation q2);
	QRotation inverse ();
	static QRotation identity();
	rhMatrix ToMatrix() const;

	// Axis and angle of the rotation
	double angle() const;
	rhVector axis() const;
   };

#endif
