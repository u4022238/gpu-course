/* SFILE_BEGIN */
#include <Utilities_CC/utilities_CC.h>

//#define rhDEBUG

// Error Messages
#define  LP_Success                  0
#define  LP_Unbounded                1
#define  LP_Infeasible               10
/* SFILE_END */

// static const double EPS = 1.0e-12;
static double EPS = 1.0e-10;

FUNCTION_DEF (static rhMatrix normalized_tableau, (
              rhMatrix &Tab,
              Int_vector &index_i,
              Int_vector &index_j
              ))
   {
   // We need to create a two matrices, one for the y's and one for the x's
   int nrows = Tab.idim();
   int ncols = Tab.jdim();

   // Create the matrices
   rhMatrix Y(nrows, nrows);
   rhMatrix X(nrows, ncols);
   X.clear(0.0);
   Y.clear(0.0);

   // Now, fill them out from the tableau
   int i, j;
   for_2Dindex (i, j, Tab)
      {
      // Get the indices
      int ti = index_i[i];
      int tj = index_j[j];

      // Fill out the left hand variable
      if (ti < ncols)
         X[i][ti] = -1.0;
      else
         Y[i][ti-ncols] = -1.0;

      // Right hand variable
      if (tj < ncols)
         X[i][tj] = Tab[i][j];
      else
         Y[i][tj-ncols] = Tab[i][j];
      }

   // Now, create the composite
   rhMatrix M = Y.inverse() * X;
   return M;
   }

FUNCTION_DEF (static void print_tableau_column, (
	rhMatrix &Tab,
	int ipiv,
	int jpiv,
	Int_vector &index_i
	))
   {
   // Now print out the tableau
   fprintf (stdout, "Column %d\n", jpiv);

   // Get the multiplier value
   double mult = Tab[ipiv][0] / Tab[ipiv][jpiv];

   for (int i=0; i<Tab.idim(); i++) 
      {
      // Row name -- variable number
      char *xory = (char *)"x";
      int varno = index_i[i];
      if (varno >= Tab.jdim())
         {
         varno -= Tab.jdim();
         xory = (char *)"y";
         }
      fprintf (stdout, "%3d: %s%02d:", i, xory, varno);

      // Now the elements in the row
      double newval = Tab[i][0] - Tab[i][jpiv]*mult;
      fprintf (stdout, "%15.5e   %15.5e -> %15.5e\n", 
		Tab[i][0], Tab[i][jpiv], newval);
      }
   }

FUNCTION_DEF (static void print_tableau, (
	rhMatrix &Tab,
	Int_vector &index_i,
	Int_vector &index_j,
	rhMatrix &A,
	rhVector &b,
	rhMatrix &Aeq,
	rhVector &beq,
	char *title
	))
   {
   // Now print out the tableau
   fprintf (stdout, "\nTableau %d x %d %s\n", Tab.idim(), Tab.jdim(), title);

   // Maximum number of columns to print
   int maxcols = Tab.jdim(); //rhMin(Tab.jdim(), 10);

   // Print column headers
   fprintf (stdout, "         ");
   for (int j=0; j<maxcols; j++)
      {
      char *xory = (char *)"x";
      int varno = index_j[j];
      if (varno >= Tab.jdim())
         {
         varno -= Tab.jdim();
         xory = (char *)"y";
         }
      fprintf (stdout, "   %s_%03d  ", xory, varno);

      // Make more readable
      if (j%6 == 1 && j != 1)
         fprintf (stdout, "\n                             ");
      }
   fprintf (stdout, "\n");

   for (int i=0; i<Tab.idim(); i++) 
      {
      // Row name -- variable number
      char *xory = (char *)"x";
      int varno = index_i[i];
      if (varno >= Tab.jdim())
         {
         varno -= Tab.jdim();
         xory = (char *)"y";
         }
      fprintf (stdout, "%3d: %s%02d:", i, xory, varno);

      // Now the elements in the row
      for (int j=0; j<maxcols; j++)
         {
         fprintf (stdout, "%10.1e", Tab[i][j]);

         // Make more readable
         if (j%6 == 1 && j != 1)
            fprintf (stdout, "\n                             ");
         }

      fprintf (stdout, "\n");
      }

   //-------------------------------------------------------
   // Now, checking the original results
   // Now, get the values back out and read them into x
   rhVector x(Tab.jdim()-1);
   x.clear(0.0);

   for (int i=0; i<Tab.idim(); i++)
      {
      // Values greater than ncols denote slack variables
      int n = index_i[i];
      if (n < Tab.jdim()) 
         x[n-1] = Tab[i][0];
      }

   // Compute the product
   rhVector prod = A * x + b;
   fprintf (stdout, "Inequalities\n");
   for (int i=0; i<prod.dim(); i++)
      fprintf (stdout, "%12.3e\n", prod[i]);

   if (! Aeq.is_null())
      {
      prod = Aeq * x + beq;
      fprintf (stdout, "Equalities\n");
      for (int i=0; i<prod.dim(); i++)
         fprintf (stdout, "%12.3e\n", prod[i]);
      }
   }

FUNCTION_DEF(static void simp1, (
   rhMatrix &A,
   int lastcol,
   int *jpiv
   ))
   {
   // Looks for the largest member in row 0

   // Initialize to column 1 (not 0)
   *jpiv = 1;
   double bmax = A[0][1];

   for (int j=2; j<=lastcol; j++) 
      if (A[0][j] > bmax)
         {
         bmax = A[0][j];
         *jpiv = j;
         }
   }

FUNCTION_DEF(static void simp2, (
      rhMatrix &A,
      int *ipiv,
      int jpiv
      ))
   {
   // Any possible pivots in column jpiv -- do not look for pivots in row 0
   int i;

   const double EPS = 0.0;

   // First, look for an initial negative member
   for (i=0; i<A.idim(); i++) 
      if (A[i][jpiv] < -EPS) break;

   // Return with ipiv = 0, if there is no suitable pivot
   if (i>=A.idim()) 
      {
      *ipiv = -1;
      return;
      }

   // Get the value of the update limit for the i-th row
   // Note that bestq, q, etc will be negative, if zeroth column is positive.
   int besti = i;
   double bestq = A[i][0]/A[i][jpiv];

   // Try the other values in this column
   for (i=besti+1; i<A.idim(); i++) 
      if (A[i][jpiv] < -EPS) 
         {
         double q = A[i][0]/A[i][jpiv];
         if (q > bestq) 
            {
            besti = i;
            bestq = q;
            } 
         else if (q == bestq) 
            {
            double qp, q0;
            for (int k=1; k<A.jdim(); k++) 
               {
               qp = A[besti][k]/A[besti][jpiv];
               q0 = A[i][k]/A[i][jpiv];
               if (q0 != qp) break;
               }

            if (q0 > qp) 
               besti=i;
            }
         }

   // Return the required values
   *ipiv = besti;
   }

FUNCTION_DEF(static void simp3, (
             rhMatrix &A,
             int ipiv,		// Element to pivot around
             int jpiv,
             Int_vector &index_i,	// Index arrays
             Int_vector &index_j
             ))
   {
#ifdef rhDEBUG
#if 0
   // Print out the normalized tableau before
   fprintf (stdout, "Normalized tableau before pivot\n");
   rhMatrix NT1 = normalized_tableau(A, index_i, index_j);
   NT1.print(stdout);
#endif
#endif

   // Inverse of the pivot value
   double piv=1.0/A[ipiv][jpiv];

   // Fill out the all the rows, except the pivot row
   for (int i=0; i<A.idim(); i++)
      if (i != ipiv) 
         {
         // The pivot column
         double mult = A[i][jpiv] * piv;
         A[i][jpiv] = mult;

         // Other columns
         if (mult != 0.0)
            for (int j=0; j<A.jdim(); j++)
               if (j != jpiv)
                  A[i][j] -= A[ipiv][j]*mult;
         }

      // Fill out the pivot row
      for (int j=0; j<A.jdim(); j++)
         if (j != jpiv) 
            A[ipiv][j] *= -piv;

      // Finally the pivot point itself
      A[ipiv][jpiv]=piv;

      // Update the two index arrays - left and right variables change places
      int temp      = index_j[jpiv];
      index_j[jpiv] = index_i[ipiv];
      index_i[ipiv] = temp;

#ifdef rhDEBUG
#if 0
      {
      // Print out the normalized tableau before
      fprintf (stdout, "Normalized tableau after pivot\n");
      rhMatrix NT2 = normalized_tableau(A, index_i, index_j);
      NT2.print(stdout);

      fprintf (stdout, "Difference\n");
      (NT2 - NT1).print(stdout);
   
      double maxdiff = 0.0;
      int i, j;
      for_2Dindex (i, j, NT1)
         {
         double diff = rhAbs(NT2[i][j] - NT1[i][j]);
         if (diff > maxdiff) maxdiff = diff;
         }
     fprintf (stdout, "Maximum difference = %.3e\n", maxdiff);
     }
#endif
#endif
   }

FUNCTION_DEF (static void remove_equalities, (
              rhMatrix &A,
              int neq,			// Number of equalities
              Int_vector &index_i,	// Index arrays
              Int_vector &index_j
              ))
   {
   // This routine pivots out the equality constraint rows.
   // It then swaps the columns corresponding to the residuals (must be zero)
   // to the last column, so that they can not be pivoted on.

   // Dimensions
   int lastrow = A.ihigh();
   int lastcol = A.jhigh();

   // Remove them one by one
   for (int i=0; i<neq; i++)
      {
      // Get rid of the last row
      int ipiv = lastrow - i;

      // Seek largest positive of negative, according to sign of first element
      int jpiv = 1;
      if (A[ipiv][0] >=0)
         {
         // Seek the smallest element
         for (int j=2; j<=lastcol; j++) 
            if (A[ipiv][j] < A[ipiv][jpiv])
               jpiv = j;
         }
      else
         {
         // seek the largest element
         for (int j=2; j<=lastcol; j++) 
            if (A[ipiv][j] > A[ipiv][jpiv])
               jpiv = j;
         }

      // Now, pivot around this element
      simp3 (A, ipiv, jpiv, index_i, index_j);

      // Swap with last column
      for (int i2=0; i2<=lastrow; i2++)
         {
         double temp = A[i2][jpiv];
         A[i2][jpiv] = A[i2][lastcol];
         A[i2][lastcol] = temp;
         }

      // Also swap the indices
      int tind = index_j[jpiv];
      index_j[jpiv] = index_j[lastcol];
      index_j[lastcol] = tind;

      // Make sure this column does not get changed again
      lastcol--;
      }
   }

FUNCTION_DEF(static void simplx_phase2, (
             rhMatrix &Tab,
             int lastcol,
             Int_vector & index_i,
             Int_vector & index_j,
             int *result,
             rhMatrix &A,
             rhVector &b,
             rhMatrix &Aeq,
             rhVector &beq,
             bool feasibility_only
             ))
   {
   // Does simplex method, assuming that solution is alread feasible

   // Now, go into a loop until the best is found
   while (1)
      {
      // The position to pivot around
      int ipiv, jpiv;

      // Test for feasibility condition
      if (Tab[0][0] > EPS && feasibility_only)
         {
         *result = LP_Success;
         return;
         }

      // Find the pivot column
      simp1(Tab, lastcol, &jpiv);
      if (Tab[0][jpiv] <= EPS)
         {
  	 if (Tab[0][0] >= 0.0)
	    *result = LP_Success;
         else
            *result=LP_Infeasible;
         return;
         }

      // Find the pivot row
      simp2(Tab, &ipiv, jpiv);
      if (ipiv == -1)  // No negative value to pivot on
         {
         *result=LP_Unbounded;
         return;
         }

      // Make a decision whether to proceed or not
      if (-Tab[ipiv][0]/Tab[ipiv][jpiv] > 1.0e6 || -Tab[ipiv][jpiv] < EPS)
         {
         // Too great a gain in this direction
         *result = LP_Unbounded;
         return;
         }

#ifdef rhDEBUG
      fprintf (stdout, "Pivoting round element %d, %d, pivot = %e\n", 
         ipiv, jpiv, Tab[ipiv][jpiv]);
      fprintf (stdout, "Pivot column:\n");
      print_tableau_column (Tab, ipiv, jpiv, index_i);
#endif

      // Pivot
      simp3(Tab, ipiv, jpiv, index_i, index_j);

#ifdef rhDEBUG
      print_tableau (Tab, index_i, index_j, A, b, Aeq, beq,
         "(after pivot)");
#endif
      }
   }

FUNCTION_DEF (static void simp_init, (
              rhMatrix &Tab,
              Int_vector &index_i,
              Int_vector &index_j
              ))
   {
   // Now, initialize
   int nrows = Tab.idim();
   int ncols = Tab.jdim();

   int ipiv;
   int jpiv = 1;
   simp2 (Tab, &ipiv, jpiv);
   simp3 (Tab, ipiv, jpiv, index_i, index_j);

   // Swap first row with this row to put it on top
   for (int j=0; j<ncols; j++)
      {
      double temp = Tab[ipiv][j];
      Tab[ipiv][j] = Tab[0][j];
      Tab[0][j] = temp;
      }

   // Update the index
   int tind = index_i[ipiv];
   index_i[ipiv] = index_i[0];
   index_i[0] = tind;
   }

//
// These are my routines giving a decent interface to the program
//

FUNCTION_DEF( rhVector my_lp_constrained , (
             rhMatrix &A, 
             rhVector &b, 
             rhMatrix &Aeq,
             rhVector &beq,
             rhVector &goal, 
             int *result,
             bool feasibility_only
             ))
   {
   // Solve the system A*x +  b >= 0 while maximizing <goal, x>
   // We assume extra constraints that x >= 0;
   int i, j;

   // Get the number of equations and the number of variables
   int nrows = A.nrows();
   int nvars = A.ncols();	// Number of variables actually active
   int nineq = b.dim();
   int neq = beq.is_null() ? 0 : beq.dim();
   nrows += neq;

   // Declare a suitable array for the recipes program
   rhMatrix Tab (nrows, nvars+1);

   // Now, the rest of the tableau
   int count = 0;
   int mineq=0, meq=0;

   // First get the inequality constraints
   for (i=0; i<nrows-neq; i++)
      {
      // This is a leq constraint
      mineq++;

      // Fill out the constraints
      for (j=0; j<A.ncols(); j++)
         Tab[count][j+1] =  A[i][j];

      // Fill out the constant term
      Tab[count][0] = b[i];

      count++;
      }

   // Equality constraints
   for (i=0; i<neq; i++)
      {
      // Equality constraint
      meq++;

      // Fill out the constraints -- each variable becomes two
      for (j=0; j<nvars; j++)
         Tab[count][j+1] = Aeq[i][j];

      // Fill out the constant term
      Tab[count][0] = beq[i];

      count++;
      }

   // Now, declare the other things that we need to solve this
   Int_vector index_i (0, Tab.idim()-1);
   Int_vector index_j (0, Tab.jdim()-1);

   // Fill up these indices
   for_1Dindex (i, index_i) index_i[i] = Tab.jdim()+i;
   for_1Dindex (i, index_j) index_j[i] = i;

#ifdef rhDEBUG
   print_tableau (Tab, index_i, index_j, A, b, Aeq, beq,
      "(before solution)");
#endif

   // Remove the equalities 
   // Last columns of tableau not to allow further pivots
   remove_equalities (Tab, meq, index_i, index_j);
   nvars -= meq;

#ifdef rhDEBUG
   print_tableau (Tab, index_i, index_j, A, b, Aeq, beq,
   	"(after removing equalities)");
#endif

   // Initialize
   simp_init (Tab, index_i, index_j);

#ifdef rhDEBUG
   print_tableau (Tab, index_i, index_j, A, b, Aeq, beq,
   	"(after initialization)");
#endif

   // Now we can call the simplex routine
   simplx_phase2 (Tab, nvars, index_i, index_j, result,
      A, b, Aeq, beq, feasibility_only);

#ifdef rhDEBUG
   print_tableau (Tab, index_i, index_j, A, b, Aeq, beq,
	"(after solution)");
#endif

   // Now, get the values back out and read them into x
   rhVector x(Tab.jdim()-1);
   x.clear(0.0);

   for (int i=0; i<Tab.idim(); i++)
      {
      // Values greater than ncols denote slack variables
      int n = index_i[i];
      if (n < Tab.jdim()) 
         x[n-1] = Tab[i][0]; // n-1, because x0 is the constant term
      }

   // Now, return
   return x;
   }

// Old interfaces without the equality constraints
FUNCTION_DEF( rhVector my_lp_constrained , (
	rhMatrix &A, 
	rhVector &b, 
	rhVector &goal, 
	int *flag,
        bool feasibility_only
	))
   {
   rhMatrix Aeq;
   rhVector beq;
   return my_lp_constrained (A, b, Aeq, beq, goal, flag, feasibility_only);
   }
