/* SFILE_BEGIN */
#include "Utilities_CC/utilities_CC.h"
/* SFILE_END */

FUNCTION_DEF (bool change_extension, (char *fname, char *ext1, char *ext2))
   {
   // Changes the name in-situ
   // Find the name of the file name
   int flen = strlen (fname);
   int ext1_len = strlen (ext1);
   int ext2_len = strlen (ext2);

   if (ext1_len < ext2_len)
      warning_message ("change_extension: truncating extension name");

   if (flen < ext1_len)
      {
      error_message ("change_extension: File name \"%s\" is too short\"",
            fname);
      return false;
      }

   char *extension = fname + (flen-ext1_len);

   // Just check that the extension is right
   int n = strcmp (extension, ext1);
   if (n != 0)
      {
      error_message (
            "change_extension: File \"%s\" has wrong extension: \"%s\"",
            fname, extension);
      return false;
      }

   // Now, just write new extension in its place
   strncpy (extension, ext2, ext1_len);
   }

FUNCTION_DEF (bool change_name_bif_to_tif, ( char *fname))
   {
   return change_extension (fname, ".bif", ".tif");
   }

FUNCTION_DEF (bool change_name_pix_to_bin, (char *fname))
   {
   return change_extension (fname, ".tif", ".bin");
   }

FUNCTION_DEF (Unsigned_char_matrix  histogram_equalize, (
      const Unsigned_short_matrix &im,
      Unsigned_char_vector &trans
      ))
   {
   // Make the translation table if it is not supplied
   if (trans.is_null())
      {
      // Histogram equalizes and puts in a 256 bit image
      const int nvals = 2<<16;
      int hist [nvals];
      for (int i=0; i<nvals; i++) hist[i] = 0;

      // Make a new image
      Unsigned_char_matrix imc (im.ilow(), im.ihigh(), im.jlow(), im.jhigh());

      // Run over the image putting in bins
      int npixels = 0;
      for2Dindex (i, j, im)
         {
         hist[im[i][j]]++;
         npixels++;
         }

      // Now make a translation table
      trans.Init(0, nvals-1);
      int nbins = 255;
      int sum = 0;
      for (int i=0; i<nvals; i++)
         {
         sum += hist[i];
         trans[i] = (unsigned char ) (sum * nbins / npixels);
         }
      }

   // Now go through and translate
   Unsigned_char_matrix imc (im.ilow(), im.ihigh(), im.jlow(), im.jhigh());
   for2Dindex (i, j, im)
      imc[i][j] = trans[im[i][j]];

   // Return the histogrammed image
   return imc;
   }

FUNCTION_DEF (Unsigned_char_matrix  histogram_equalize, (
      const Unsigned_char_matrix &im,
      Unsigned_char_vector &trans
      ))
   {
   // Make the translation table if it is not supplied
   if (trans.is_null())
      {
      // Histogram equalizes and puts in a 256 bit image
      const int nvals = 2<<8;
      int hist [nvals];
      for (int i=0; i<nvals; i++) hist[i] = 0;

      // Make a new image
      Unsigned_char_matrix imc (im.ilow(), im.ihigh(), im.jlow(), im.jhigh());

      // Run over the image putting in bins
      int npixels = 0;
      for2Dindex (i, j, im)
         {
         hist[im[i][j]]++;
         npixels++;
         }

      // Now make a translation table
      trans.Init(0, nvals-1);
      int nbins = 255;
      int sum = 0;
      for (int i=0; i<nvals; i++)
         {
         sum += hist[i];
         trans[i] = (unsigned char ) (sum * nbins / npixels);
         }
      }

   // Now go through and translate
   Unsigned_char_matrix imc (im.ilow(), im.ihigh(), im.jlow(), im.jhigh());
   for2Dindex (i, j, im)
      imc[i][j] = trans[im[i][j]];

   // Return the histogrammed image
   return imc;
   }


