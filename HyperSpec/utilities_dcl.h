#ifndef _utilities_cc_dcl_
#define _utilities_cc_dcl_
#include "Utilities/cvar.h"


/* */
#include "Utilities_CC/utilities_CC.h"
/* */

FUNCTION_DECL (bool change_extension, (char *fname, char *ext1, char *ext2));

FUNCTION_DECL (bool change_name_bif_to_tif, ( char *fname));

FUNCTION_DECL (bool change_name_pix_to_bin, (char *fname));

FUNCTION_DECL (Unsigned_char_matrix  histogram_equalize, (
      const Unsigned_short_matrix &im,
      Unsigned_char_vector &trans
      ));

FUNCTION_DECL (Unsigned_char_matrix  histogram_equalize, (
      const Unsigned_char_matrix &im,
      Unsigned_char_vector &trans
      ));

#endif
