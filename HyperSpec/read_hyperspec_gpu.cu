#include <stdlib.h>
#include <stdio.h>
#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"
#include "PNG/pngwritefile_dcl.h"
#include "./utilities_dcl.h"
#include <vector>
using namespace std;

__global__ void compute_scatter (
      double *data, double *cov, int nbands, int vals_per_band)
   {
   // Each thread computes a different entry of the covariance matrix
   int threadid = threadIdx.x + blockIdx.x * blockDim.x;

   // Get the device number
   int dummy; cudaGetDeviceCount (&dummy);   // Dummy to initialize
   int dnum; cudaGetDevice (&dnum);

   int k = (threadid / nbands);
   int m = (threadid % nbands);
   if ((k > m && dnum == 0) || (k<= m && dnum == 1)) return;         // Only compute the above-centre entries
   // if (k > m) return;         // Only compute the above-centre entries

   int startk = k * vals_per_band;
   int startm = m * vals_per_band;
   double *pk = &data[startk];
   double *pm = &data[startm];

   double val = 0.0;
   for (int i=0; i<vals_per_band; i++)
      val += (*pk++) * (*pm++);

   // printf ("Entry %d  %d on device %d: %.4e\n", k, m, dnum, val);

   // Now set this value into the covariance matrix
   cov[threadid] = val;
   }

__global__ void average_band (
      unsigned short *d_im, 
      double *d_imf, 
      int vals_per_band)
   {
   int band = threadIdx.x + blockIdx.x * blockDim.x;
   unsigned short *p = &d_im[band * vals_per_band];
   double *q = &d_imf[band * vals_per_band];

   // Find the sum of the band
   double sum = 0.0;
   unsigned short *pp = p;
   for (int i=0; i<vals_per_band; i++)
      sum += *(pp++);

   // The average
   double avg = sum / vals_per_band;

   // Subtract the average
   for (int i=0 ; i<vals_per_band; i++)
      *(q++) = *(p++) - avg;
   }

Unsigned_char_matrix KL_transform ( 
      const vector<Unsigned_short_matrix> &im,  // Input multi-band image
      int n       // The number of bands to keep
      )
   {
   // Do a KL transform on the image

   // Get the size of the input
   int nbands = (int) im.size();
   int idim = im[0].idim();
   int jdim = im[0].jdim();
   const int vals_per_band = idim * jdim;
   const int nvals = nbands * vals_per_band;

   // Make some device data
   unsigned short *d_im;
   double *d_imf;
   cudaMallocManaged ((void **) &d_im, nvals * sizeof(unsigned short));
   cudaMallocManaged ((void **) &d_imf, nvals * sizeof(double));

   // Copy imf to d_imf -- flattens the data
   rhTimer2 *tc = new rhTimer2 ("Reformatting data");
   for (int ch=0; ch < (int)im.size(); ch++)
      memcpy (&(d_im[ch*vals_per_band]), 
              &(im[ch][0][0]), 
              vals_per_band * sizeof(unsigned short));
   delete (tc);

   // Now, average each band

   rhTimer2 *tav = new rhTimer2 ("average bands");

   // Do it on two devices
   average_band <<<nbands, 1>>> (d_im, d_imf, vals_per_band);

   cudaDeviceSynchronize();
   delete (tav);

   // Clean up
   cudaFree (d_im);

   printf ("Finished averaging images\n"); fflush(stdout);

   // Now, make and populate a scatter matrix -- upper half first
   // Allocate some managed data
   double *d_cov;
   cudaMallocManaged ((void **) &d_cov, nbands * nbands * sizeof(double));
   for (int i=0; i<nbands*nbands; i++)
      d_cov[i] = 0.12121212121212;

   // Now, call GPU to generate scatter matrix
   rhTimer2 *cs = new rhTimer2 ("compute_scatter");
   const int nentries = nbands * nbands;

   // Start kernel on two devices
   //cudaSetDevice (0);
   compute_scatter<<<nentries, 1>>> (d_imf, d_cov, nbands, vals_per_band);
   cudaSetDevice (1);
   compute_scatter<<<nentries, 1>>> (d_imf, d_cov, nbands, vals_per_band);
   cudaDeviceSynchronize();
   delete (cs); // Stop the timer

   // Transfer data back to a matrix
   rhMatrix cov (nbands, nbands, d_cov);

   // Finished with the device data
   cudaFree (d_cov);
   cudaFree (d_imf);

   // Fill out bottom part, symmetrically
   rhTimer2 *sm = new rhTimer2 ("Filling out bottom");
   for (int k=1; k<nbands; k++)
      for (int m=0; m<k; m++)
         cov[k][m] = cov[m][k];
   delete (sm);

   printf ("Computed scatter matrix\n"); fflush(stdout);

   // Print it out
   for2Dindex (i, j, cov)
      printf ("Host entry %d  %d = %3e\n", i, j, cov[i][j]);

   // Now, take the eigenvalue expansion of cov
   rhMatrix V;
   rhVector D;
   jacobi (cov, D, V);

   printf ("Finished eigenvalue expansion\n"); fflush(stdout);

   // Now, use this to generate the RBG image
   printf ("Eigenvalues:\n");
   double sum = D.sum();
   double accum = 0.0;
   for1Dindex (i, D)
      {
      accum += D[i];
      double percent = (accum / sum) * 100.0;
      printf ("%03d: %.3e  = %6.3f %%\n", i, D[i], percent);
      }

   // Get the vector
   rhVector v = V.column(0);

   // Now, make the image
   rhMatrix fim(idim, jdim);

   // Resample the image
   rhTimer2 *trs = new rhTimer2 ("compute principal image");
   for2Dindex (i, j, fim)
      {
      fim[i][j] = 0.0;
      for (int band=0; band<nbands; band++)
         fim[i][j] += v[band] * im[band][i][j];
      }
   delete (trs);

   // Work out the smallest and largest values
   double minval = fim[0][0];
   double maxval = fim[0][0];
   for2Dindex (i, j, fim)
      {
      if (fim[i][j] < minval) minval = fim[i][j];
      if (fim[i][j] > maxval) maxval = fim[i][j];
      }

   // Make an unsigned image - at the same time, transpose and take mirror image
   double scale = 255.0 / (maxval - minval);
   Unsigned_char_matrix uim (fim.jlow(), fim.jhigh(), fim.ilow(), fim.ihigh());
   for2Dindex (i, j, uim)
      uim[i][j] = (unsigned int) 
            (scale * (fim[j][uim.ihigh() + uim.ilow() - i] - minval) + 0.49);

   // Return this image
   return uim;
   }

void get_percentile_bounds (
      const vector<Unsigned_short_matrix> &im16,
      double low,
      double high,
      int *lowthreshold,
      int *highthreshold
      )
   {
   // Gets percentile bounds to scale the image

   // Make a histogram
   const int hist_size = 1<<16;
   Int_vector hist(0, hist_size-1);
   hist.clear(0);

   // Make the histogram
   for (int ch=0; ch<im16.size(); ch++)
      for2Dindex (i, j, im16[ch])
         hist[im16[ch][i][j]]++;

   // Find the total number of pixels
   int total = hist.sum();
   double lowval = low * total;
   double highval = high * total;

   // Now, find high and low values
   int accum = 0;
   bool lowset = false;
   for1Dindex (i, hist)
      {
      accum += hist[i];
      if (!lowset && accum >= lowval)   
         {
         *lowthreshold = i;
         lowset = true;
         }
      if (accum >= highval)
         {
         *highthreshold = i;
         break;
         }
      }
   }

int main (int argc, char *argv[])
   {
   rhTimer2::SetTimingOn();
   rhTimer2 *t = new rhTimer2("Whole program");

   // Set default image type
   char outfname[80];   // For constructing names 
   tj_interface_set_output_image_type ("PNGImage");
   const char *file_extension = "_lin.png";

   // The first argument is the file name
   if (argc != 2)
      {
      error_message ("Usage: infile.bin");
      return (EXIT_FAILURE);
      }

   // Get the file name
   char *fname = argv[1];

   // Open the file
   FILE *infile = FOPEN (fname, "rb");

   // Now, try reading the file
   // Assume that this works
   // Read the header of a bin file
   unsigned int w, h, nchannels, bytesperchannel;
   fread (&w, sizeof(int), 1, infile);
   fread (&h, sizeof(int), 1, infile);
   fread (&nchannels, sizeof(int), 1, infile);
   fread (&bytesperchannel, sizeof(int), 1, infile);

   // Now, print them out
   printf ("width           = %d\n", w);
   printf ("height          = %d\n", h);
   printf ("nchannels       = %d\n", nchannels);
   printf ("bytesperchannel = %d\n", bytesperchannel);

   // Now, try to read in the data
   if (bytesperchannel == 2)
      {
      // To carry the slices
      vector<Unsigned_short_matrix> im16 (nchannels);
      for (unsigned int i=0; i<nchannels; i++)
         im16[i].Init(0, w-1, 0, h-1);

      // To hold a pixel
      Unsigned_short_vector pixel (0, nchannels-1);

      // Now, read in pixel by pixel
      unsigned short tmax = 0;
      int tmaxch = 0;
      for2Dindex (i, j, im16[0])
         {
         // Read in a set of values
         // fread (&(pixel[0]), bytesperchannel, nchannels, infile);
         fread (&(pixel[0]), 1, bytesperchannel*nchannels, infile);

         // Now, distribute to the different images
         unsigned short vmax = 0;
         int maxch = 0;
         for1Dindex (ch, pixel)
            {
            if (pixel[ch] > vmax) 
               {
               vmax = pixel[ch];
               maxch = ch;
               }

            im16[ch][i][j] = pixel[ch];
            }

         if (vmax > tmax) 
            {
            tmax = vmax;
            tmaxch = maxch;
            }
         }

      printf ("All pixels : max = %d in channel %d\n", tmax, tmaxch); 
      fflush(stdout);

      // Now, write out each image

      // First, get bounds
      const double lowbound = 0.01;
      const double highbound = 0.99;
      int lowthreshold, highthreshold;
      get_percentile_bounds (im16, lowbound, highbound, 
            &lowthreshold, &highthreshold);

      printf ("Low threshold = %d,  high = %d\n", lowthreshold, highthreshold);
      fflush(stdout);

      Unsigned_char_matrix im08 (im16[0].bounds());
      const bool save_images = false;
      for (unsigned int ch=0; ch<nchannels; ch++)
         {
         if (! save_images) break;

         // Non-copying alias to the current channel
         Unsigned_short_matrix im = im16[ch];

         // Set thresholds for rescaling
         int maxval = highthreshold;
         int minval = lowthreshold;

         double scale = 255.0 / (maxval - minval);
         for2Dindex (i, j, im)
            {
            // Offset by some amount
            // int half = im.idim() / 2;
            // int ii = (i+half) % im.idim();
            int ii = i;
            int jj = j;
            int val = (int) floor((im[i][j]-minval) * scale + 0.49);
            if (val > 255) val = 255;
            if (val < 0) val = 0;
            im08[ii][jj] = val;
            }

         // Now, write it out
         sprintf (outfname, "channel_%03d%s", ch, file_extension);

         // Output file
         write_png_image (outfname, im08);
         }

      // Do Karhunen-Loeve transform
      printf ("Doing KL-transform\n"); fflush (stdout);

      rhTimer2 klt ("KL_transform");
      Unsigned_char_matrix uim = KL_transform (im16, 1);

      // Make the outfile name
      change_extension (fname, ".bin", ".png");
      Unsigned_char_vector trans;
      //write_png_image (fname, histogram_equalize(uim, trans));
      }

   // Stop the timer and print the results
   delete (t);
   rhTimer2::print_results();

   // Done
   return EXIT_SUCCESS;
   }
