#include <stdlib.h>
#include <stdio.h>
#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"

double interpolated_value (Unsigned_char_matrix im, double i, double j)
   {
   // Computes the linearly interpolated value of the image at
   // the point (i, j), with wrap-around

   // We need to wrap in the j dimension
   int jdim = im.jdim();
   int jlow = im.jlow();
   int jhigh= im.jhigh();

   // Now, wrap
   while (j > im.jdim()) j-= im.jdim();
   while (j < 0) j+= im.jdim();

   // j = ((int) floor(j/jdim)) * jdim;

   // Return the interpolated value
   // return im.ij_val_interpolated (i, j);

   // Get the rectangle on either side
   int ii = (int) floor (i);
   int jj = (int) floor (j);
   if (ii<im.ilow() || ii>=im.ihigh()) return 0.0;

   // Get the fractional values
   double fi = i - ii;
   double fj = j - jj;

   // Now get the values at the corners, wrapping around
   double v00, v01, v10, v11;
   v00 = (double) im[ii]  [jj];
   v01 = (double) ((j<jhigh) ? im[ii][jj+1]: im[ii][jlow]);
   v10 = (double) im[ii+1][jj];
   v11 = (double) ((j<jhigh) ? im[ii+1][jj+1]: im[ii+1][jlow]);

   double v0 = v00 + fj * (v01 - v00);
   double v1 = v10 + fj * (v11 - v10);
   
   double v  = v0 + fi * (v1 - v0);

   // This is the interpolated value
   return v;
   }

void resample_image (
      Unsigned_char_matrix im,   // The input image
      Unsigned_char_matrix rim,  // Resampled image
      rhMatrix T                 // Transform
      )
   {
   // This is a special sort of resampling, where it wraps around.
   // The output image rim must already be allocated (so as to give dimensions)

   // Take the inverse transform
   rhMatrix Tinv = T.inverse();

   // Now, do the output pixel by pixel
   for2Dindex (i, j, rim)
      {
      // Get the transformed pixel
      rhVector y ((double)i, (double)j, 1.0);
      rhVector x = (Tinv*y).dehom();

      // Get the interpolated value, with wrapping
      rim[i][j] = interpolated_value (im, x[0], x[1]);
      // rim[i][j] = im.ij_val_interpolated (x[0], x[1]);
      }
   }

void get_transform (
      const rhVector u1, const rhVector v1,
      const rhVector u2, const rhVector v2,
      int j2dim,
      double stretch,
      rhMatrix &T)
   {
   // Gets the transformation that take points 1 to points 2
   // stretch is the stretch in the j-dimension

   // First, work out what the actual transform without wrap is
   const int npoints = u1.dim();

   // Work out an offset -- this is rough -- assumes the first point is good
   double offs = v2[0] - v1[0]*stretch;

   printf (
      "get_transform: stretch = %.2f, offs = %.2f, (v1, v2) = (%.3f, %.3f)\n", 
      stretch, offs, v1[0], v2[0]);
   fflush(stdout);

   // A vector for the wrapped-around v1 points
   rhVector vv2(npoints);

   for (int i=0; i<npoints; i++)
      {
      // Get the difference without offset
      double x = v1[i]*stretch + offs;
      double y = v2[i];    // The target
      double diff = y - x;

      // Now, adjust by adding multiples of j2dim to get closest
      while (diff > j2dim/2) diff -= j2dim;
      while (diff < -j2dim/2) diff += j2dim;
      y = diff + x;

      // Now, this is the best value for where it maps to
      vv2[i] = y;
      }

   // Find the transform that best matches the points
   rhMatrix A(2*npoints, 3);
   rhVector b(2*npoints);
   int c = 0;
   for (int i=0; i<npoints; i++)
      {
      // x agreement
      A[c][0] = u1[i]; A[c][1] = 1.0; A[c][2] = 0.0;
      b[c] = u2[i];
      c++;

      A[c][0] = 0.0;   A[c][1] = 0.0; A[c][2] = 1.0;
      b[c] = vv2[i] - v1[i]*stretch;
      c++;
      }

   // Now, solve
   rhVector s = lin_solve(A, b);

   // Now, fill out the transform
   T.Init(3, 3);
   T[0][0] = s[0];  T[0][1] =  0.0;    T[0][2] = s[1];
   T[1][0] =  0.0;  T[1][1] = stretch; T[1][2] = s[2];
   T[2][0] =  0.0;  T[2][1] =  0.0;    T[2][2] =  1.0;

   printf ("T1\n");
   T.print();
   fflush(stdout);
   }

void read_matches (rhVector &u1, rhVector &v1, rhVector &u2, rhVector &v2)
   {
   // For now, just fake it
   u1.Init(3);
   v1.Init(3);
   u2.Init(3);
   v2.Init(3);

   u1[0] = 178.;  v1[0] = 1807.;  u2[0] = 263.;  v2[0] = 226.;
   u1[1] = 487.;  v1[1] = 1830.;  u2[1] = 406.;  v2[1] = 242.;
   u1[2] = 771.;  v1[2] = 1977.;  u2[2] = 544.;  v2[2] = 312.;
   }

int main (int argc, char *argv[])
   {
   // Set timing
   rhTimer2::SetTimingOn();
   rhTimer2 *t = new rhTimer2("Whole program");

   // Enable messages
   enable_informative_messages ();
   enable_error_messages ();

   // Set default image type
   char fname[80];   // For constructing names 
   tj_interface_set_output_image_type ("PNGImage");
   const char *file_extension = ".png";

   // The first argument is the file name
   if (argc != 3)
      {
      error_message ("Usage: header fname1 fname2");
      return (EXIT_FAILURE);
      }

   // Get the file name
   char *fname1 = argv[1];
   char *fname2 = argv[2];

   // Read the two .png images
   Unsigned_char_matrix im1, im2;

   sprintf (fname, "%s%s", fname1, file_extension);
   read_png (im1, fname);
   informative_message ("Read file \"%s\" of size (%d x %d)", 
         fname, im1.idim(), im1.jdim());

   sprintf (fname, "%s%s", fname2, file_extension);
   read_png (im2, fname);
   informative_message ("Read file \"%s\" of size (%d x %d)", 
         fname, im2.idim(), im2.jdim());

   // Read the matches
   rhVector u1, v1, u2, v2;
   read_matches (u1, v1, u2, v2);

   // Get the transform
   rhMatrix T1;
   double stretch = im2.jdim() / (double) im1.jdim();
   get_transform (u1, v1, u2, v2, im2.jdim(), stretch, T1);

   // Reverse transform
   rhMatrix T2 = T1.inverse();

   Unsigned_char_matrix rim1, rim2;
   rim1.Init(im2.bounds());
   rim2.Init(im1.bounds());

   resample_image (im1, rim1, T1);
   resample_image (im2, rim2, T2);
   
   // Write both the images
   sprintf (fname, "%s_res%s", fname1, file_extension);
   write_png (fname, rim1);
   sprintf (fname, "%s_res%s", fname2, file_extension);
   write_png (fname, rim2);

   // Stop the timer and print out the results
   delete (t);
   rhTimer2::print_results();

   // Done
   return EXIT_SUCCESS;
   }
