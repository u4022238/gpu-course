#include <stdlib.h>
#include <stdio.h>
#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"

void  h_equalize (Unsigned_short_matrix im, Unsigned_char_matrix &him)
   {
   // Do histogram equalization on the two images

   // Initialize the him matrix
   him.Init(im.bounds());

   // First, create a histogram
   const unsigned int nbins = 1<<16;
   Int_vector hist (nbins);
   hist.clear();

   // Form the histogram
   for2Dindex (i, j, im)
      {
      unsigned short pix = im[i][j];
      hist[pix]++;
      }

   // Get the number of pixels
   const int npixels = im.idim() * im.jdim();

   // Now, a translation table
   Unsigned_char_vector trans(1<<16);

   // Now, create the translation table
   int count = 0;
   for (int i=0; i<hist.dim(); i++)
      {
      unsigned char val = (255*count) / npixels;
      trans[i] = val;
      count += hist[i];
      }

   // Now, translate
   for2Dindex (i, j, im)
      him[i][j] = trans[im[i][j]];
   }

int main (int argc, char *argv[])
   {
   // Set timing
   rhTimer2::SetTimingOn();
   rhTimer2 *t = new rhTimer2("Whole program");

   // Enable messages
   enable_informative_messages ();
   enable_error_messages ();

   // Set default image type
   char fname[80];   // For constructing names 
   tj_interface_set_output_image_type ("PNGImage");
   const char *file_extension = ".png";

   // The first argument is the file name
   if (argc != 2)
      {
      error_message ("Usage: fname");
      return (EXIT_FAILURE);
      }

   // Get the file name
   char *fname1 = argv[1];

   // Read the image
   printf ("main: A\n"); fflush(stdout);
   sprintf (fname, "%s%s", fname1, file_extension);
   Unsigned_short_matrix im;
   printf ("main: B\n"); fflush(stdout);
   read_png (im, fname);
   printf ("main: C\n"); fflush(stdout);
   informative_message ("Read file \"%s\" of size (%d x %d)", 
         fname, im.idim(), im.jdim());

   // Now do histogram equalization
   printf ("main: D\n"); fflush(stdout);
   Unsigned_char_matrix him;
   h_equalize (im, him);
   printf ("main: E\n"); fflush(stdout);
   
   // Write both the images
   sprintf (fname, "%s_heq%s", fname1, file_extension);
   write_png (fname, him);

   // Stop the timer and print out the results
   delete (t);
   rhTimer2::print_results();

   // Done
   return EXIT_SUCCESS;
   }
