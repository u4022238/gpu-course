#include <stdlib.h>
#include <stdio.h>
#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"
// #include "CoreAlignment/thresholding_dcl.h"
// #include "CoreAlignment/utilities_dcl.h"
#include "PNG/png.h"
#include <vector>
using namespace std;

Unsigned_char_matrix KL_transform ( 
      const vector<Unsigned_short_matrix> &im,  // Input multi-band image
      int n       // The number of bands to keep
      )
   {
   // Do a KL transform on the image
   rhTimer2 t ("KL_transform");

   // Get the size of the input
   int nbands = (int) im.size();
   int idim = im[0].idim();
   int jdim = im[0].jdim();
   int npix = idim * jdim;

   // Now, average each band
   vector<rhMatrix> imf(nbands);
   for (int band = 0; band<nbands; band++)
      {
      rhTimer2 t2 ("KL_transform: Computing average");
      // Get the average pixel in the band
      double sum = (double) im[band].sum();
      double avg = sum / npix;

      // Allocate the double image
      imf[band].Init(idim, jdim);

      for2Dindex (i, j, im[band])
         imf[band][i][j] = im[band][i][j] - avg;
      }

   printf ("Finished averaging images\n"); fflush(stdout);

   // Now, make and populate a scatter matrix -- upper half first
   rhTimer2 *t3  = new rhTimer2("Computing scatter matrix");
   rhMatrix cov (nbands, nbands); cov.clear();
   for (int k=0; k<nbands; k++)
      {
      printf ("Doing scatter matrix: band %d\n", k); fflush(stdout);
      for (int m=k; m<nbands; m++)
         for (int i=0; i<idim; i++)
            for (int j=0; j<jdim; j++)
               cov[k][m] += imf[k][i][j] * imf[m][i][j];
      }

   // Fill out bottom part, symmetrically
   for (int k=1; k<nbands; k++)
      for (int m=0; m<k; m++)
         cov[k][m] = cov[m][k];

   // Stop timer
   delete (t3);

   printf ("Computed scatter matrix\n"); fflush(stdout);

   // Now, take the eigenvalue expansion of cov
   rhTimer2 *jac = new rhTimer2("Jacobi decomposition");
   rhMatrix V;
   rhVector D;
   jacobi (cov, D, V);

   printf ("Finished eigenvalue expansion\n"); fflush(stdout);

   // Now, use this to generate the RBG image
   printf ("Eigenvalues:\n");
   double sum = D.sum();
   double accum = 0.0;
   for1Dindex (i, D)
      {
      accum += D[i];
      double percent = (accum / sum) * 100.0;
      printf ("%03d: %.3e  = %6.3f %%\n", i, D[i], percent);
      }

   // Get the vector
   rhVector v = V.column(0);

   // Now, make the image
   rhMatrix fim(idim, jdim);

   // Resample the image
   for2Dindex (i, j, fim)
      {
      fim[i][j] = 0.0;
      for (int band=0; band<nbands; band++)
         fim[i][j] += v[band] * im[band][i][j];
      }

   // Work out the smallest and largest values
   double minval = fim[0][0];
   double maxval = fim[0][0];
   for2Dindex (i, j, fim)
      {
      if (fim[i][j] < minval) minval = fim[i][j];
      if (fim[i][j] > maxval) maxval = fim[i][j];
      }

   // Make an unsigned image - at the same time, transpose and take mirror image
   double scale = 255.0 / (maxval - minval);
   Unsigned_char_matrix uim (fim.jlow(), fim.jhigh(), fim.ilow(), fim.ihigh());
   for2Dindex (i, j, uim)
      uim[i][j] = (unsigned int) 
            (scale * (fim[j][uim.ihigh() + uim.ilow() - i] - minval) + 0.49);

   // Return this image
   return uim;
   }

void get_percentile_bounds (
      const vector<Unsigned_short_matrix> &im16,
      double low,
      double high,
      int *lowthreshold,
      int *highthreshold
      )
   {
   // Gets percentile bounds to scale the image

   // Make a histogram
   const int hist_size = 1<<16;
   Int_vector hist(0, hist_size-1);
   hist.clear(0);

   // Make the histogram
   for (int ch=0; ch<im16.size(); ch++)
      for2Dindex (i, j, im16[ch])
         hist[im16[ch][i][j]]++;

   // Find the total number of pixels
   int total = hist.sum();
   double lowval = low * total;
   double highval = high * total;

   // Now, find high and low values
   int accum = 0;
   bool lowset = false;
   for1Dindex (i, hist)
      {
      accum += hist[i];
      if (!lowset && accum >= lowval)   
         {
         *lowthreshold = i;
         lowset = true;
         }
      if (accum >= highval)
         {
         *highthreshold = i;
         break;
         }
      }
   }

int main (int argc, char *argv[])
   {
   // Set timing
   rhTimer2::SetTimingOn();
   rhTimer2 *t = new rhTimer2("Whole program");

   // Set default image type
   char outfname[80];   // For constructing names 
   tj_interface_set_output_image_type ("PNGImage");
   const char *file_extension = "n.png";

   // The first argument is the file name
   if (argc != 2)
      {
      error_message ("Usage: header fname");
      return (EXIT_FAILURE);
      }

   // Get the file name
   char *fname = argv[1];



   // Open the file
   FILE *infile = FOPEN (fname, "rb");

   // Now, try reading the file
   // Assume that this works
   // Read the header of a bin file
   unsigned int w, h, nchannels, bytesperchannel;
   fread (&w, sizeof(int), 1, infile);
   fread (&h, sizeof(int), 1, infile);
   fread (&nchannels, sizeof(int), 1, infile);
   fread (&bytesperchannel, sizeof(int), 1, infile);

   // Now, print them out
   printf ("width           = %d\n", w);
   printf ("height          = %d\n", h);
   printf ("nchannels       = %d\n", nchannels);
   printf ("bytesperchannel = %d\n", bytesperchannel);

   // Now, try to read in the data
   if (bytesperchannel == 2)
      {
      // To carry the slices
      vector<Unsigned_short_matrix> im16 (nchannels);
      for (unsigned int i=0; i<nchannels; i++)
         im16[i].Init(0, w-1, 0, h-1);

      // To hold a pixel
      Unsigned_short_vector pixel (0, nchannels-1);

      // Now, read in pixel by pixel
      rhTimer2 *t1 = new rhTimer2("Reading image");

      unsigned short tmax = 0;
      int tmaxch = 0;
      for2Dindex (i, j, im16[0])
         {
         // Read in a set of values
         // fread (&(pixel[0]), bytesperchannel, nchannels, infile);
         fread (&(pixel[0]), 1, bytesperchannel*nchannels, infile);

         // Now, distribute to the different images
         unsigned short vmax = 0;
         int maxch = 0;
         for1Dindex (ch, pixel)
            {
            if (pixel[ch] > vmax) 
               {
               vmax = pixel[ch];
               maxch = ch;
               }

            im16[ch][i][j] = pixel[ch];
            }

         if (vmax > tmax) 
            {
            tmax = vmax;
            tmaxch = maxch;
            }

         printf ("Pixel %4d  %4d : max = %d in channel %d\n", 
               i, j, vmax, maxch); fflush(stdout);
         }

      printf ("All pixels : max = %d in channel %d\n", tmax, tmaxch); 
      fflush(stdout);

      // Stop the timer
      delete (t1);

      // Now, write out each image

      // First, get bounds
      const double lowbound = 0.01;
      const double highbound = 0.99;
      int lowthreshold, highthreshold;
      get_percentile_bounds (im16, lowbound, highbound, 
            &lowthreshold, &highthreshold);

      printf ("Low threshold = %d,  high = %d\n", lowthreshold, highthreshold);
      fflush(stdout);

      Unsigned_char_matrix im08 (im16[0].bounds());
      for (unsigned int ch=0; ch<nchannels; ch++)
         {
         // Set timer
         rhTimer2 t2 ("Writing slices");

         // Non-copying alias to the current channel
         Unsigned_short_matrix im = im16[ch];

         // Set thresholds for rescaling
         int maxval = highthreshold;
         int minval = lowthreshold;

         double scale = 255.0 / (maxval - minval);
         for2Dindex (i, j, im)
            {
            rhTimer2 t3 ("Rescaling images");
            int ii = i;
            int jj = j;
            int val = (int) floor((im[i][j]-minval) * scale + 0.49);
            if (val > 255) val = 255;
            if (val < 0) val = 0;
            im08[ii][jj] = val;
            }

         // Now, write it out
         sprintf (outfname, "channel_%03d%s", ch, file_extension);

         // Output file
         rhTimer2 *t4 = new rhTimer2("Actual write slice");
         write_png (outfname, im08);
         delete (t4);
         }

      // Do Karhunen-Loeve transform
      printf ("Doing KL-transform\n"); fflush (stdout);

      Unsigned_char_matrix uim = KL_transform (im16, 1);
      sprintf (outfname, "principal%s", file_extension);
      // put_array_to_pix (outfname, uim);
      write_png (outfname, uim);
      }

   // Stop the timer and print out the results
   delete (t);
   rhTimer2::print_results();

   // Done
   return EXIT_SUCCESS;
   }
