#include <stdlib.h>
#include <stdio.h>
#include "Utilities_CC/utilities_CC.h"
#include "Tjinterface/imageio.h"
#include "PNG/pngwritefile_dcl.h"
#include "./errChk.h"
#include "./utilities_dcl.h"
#include <vector>
using namespace std;

// This does Karhunen-Loeve transform on a hyperspectral image.  The main
// difference between this and read_hyperspec_gpu2 is that it does calcultions
// on short data instead of float / double
// We can also choose between double and float operations.


// It does make quite a difference in accuracy if we use only float,
// though it is faster than double.
// Double is recommended, because of greater accuracy.

// #define real float
#define real double

__global__ void compute_scatter (
      unsigned short *data, real *cov, 
      int nbands, int vals_per_band, int ndevices)
   {
   // Each thread computes a different entry of the covariance matrix
   int threadid = threadIdx.x + blockIdx.x * blockDim.x;

   // Get the device number
   int dummy; cudaGetDeviceCount (&dummy);   // Dummy to initialize
   int dnum; cudaGetDevice (&dnum);

   // Different bands read above or below the diagonal
   int k = (threadid / nbands);
   int m = (threadid % nbands);

   // We only fill the upper part, and alternate the diagonals
   if (k > m  || (m-k)%ndevices != dnum) return;

   int startk = k * vals_per_band;
   int startm = m * vals_per_band;
   unsigned short *pk = &data[startk];
   unsigned short *pm = &data[startm];

   real val = 0.0;
   for (int i=0; i<vals_per_band; i++)
      val += (*pk++) * (*pm++);

   // Now set this value into the covariance matrix
   cov[threadid] = val;
   }

__global__ void average_band (
      unsigned short *d_im, 
      real *d_avg, 
      int vals_per_band)
   {
   // Which band to do
   int band = threadIdx.x + blockIdx.x * blockDim.x;

   // Find the sum of the band
   real sum = 0.0;
   unsigned short *p = &d_im[band * vals_per_band];
   for (int i=0; i<vals_per_band; i++)
      sum += *(p++);

   // The average
   d_avg[band] = sum / vals_per_band;
   }

Unsigned_char_matrix KL_transform ( 
      const vector<Unsigned_short_matrix> &im,  // Input multi-band image
      int n       // The number of bands to keep
      )
   {
   // Do a KL transform on the image

   // Get the size of the input
   int nbands = (int) im.size();
   int idim = im[0].idim();
   int jdim = im[0].jdim();
   const int vals_per_band = idim * jdim;
   const int nvals = nbands * vals_per_band;

   // Copy im to flat_im -- flattens the data
   rhTimer2 *tc = new rhTimer2 ("Reformatting data");
   unsigned short *flat_im = (unsigned short *)
      malloc_chk(nvals * sizeof(unsigned short));

   // Copy data to flat_im
   for (int ch=0; ch < nbands; ch++)
      memcpy (&(flat_im[ch*vals_per_band]), 
              &(im[ch][0][0]), 
              vals_per_band * sizeof(unsigned short));

   delete (tc);

   //-----------------------------------------
   // Now, average each band
   //-----------------------------------------

   // Set a timer
   rhTimer2 *tav = new rhTimer2 ("average bands");

   printf ("Starting averaging images\n"); fflush(stdout);

   // Allocate device data
   unsigned short *d_im;      // Data on the device
   real *d_avg;             // Average on the device
   cudaMalloc_chk((void **) &d_im, nvals * sizeof(unsigned short));
   cudaMalloc_chk((void **) &d_avg, nbands * sizeof(real));

   // Copy flat image to the device
   int size = nvals * sizeof(unsigned short);
   cudaMemcpy_chk (d_im, flat_im, size, cudaMemcpyHostToDevice);

   // Run the averaging kernel
   average_band <<<nbands, 1>>> (d_im, d_avg, vals_per_band);

   // Copy averaged image back
   size = nbands * sizeof(real);
   real *avg = (real *)malloc_chk (size);
   gpuErrChk(cudaMemcpy (avg, d_avg, size, cudaMemcpyDeviceToHost));

   // Finish timer
   delete (tav);

   // Clean up
   cudaFree (d_im);

   // Averages computed
   printf ("Finished averaging images\n"); fflush(stdout);

   //-----------------------------------------

   // Allocate some managed data -- one set for each device.
   const int MaxDevices = 10;
   real *d_cov2[MaxDevices];
   real *h_cov2[MaxDevices];         // Host version 
   unsigned short *d_im2[MaxDevices];  // Data on the device

   // Now, call GPU to generate scatter matrix
   rhTimer2 *cs = new rhTimer2 ("compute_scatter");
   const int nentries = nbands * nbands;

   // Start kernel on two devices
   int ndevices; 
   cudaGetDeviceCount_chk (&ndevices);   // Get number of devices

   // Message on number of devices
   if (ndevices > MaxDevices) ndevices = MaxDevices;
   printf ("Generating scatter matrix on %d devices\n", ndevices);

   for (int device=0; device < ndevices; device++)
      {
      // Set the present device for the various transactions
      cudaSetDevice_chk (device);

      // Allocate device data -- should allocate on current device
      cudaMalloc_chk ((void **) &(d_im2[device]), nvals * sizeof(unsigned short));

      // Allocate thing for return values
      cudaMalloc_chk (
            (void **)&(d_cov2[device]), nbands * nbands * sizeof(real));

      // Transfer data to the device
      int size = nvals * sizeof(unsigned short);
      cudaMemcpy_chk (d_im2[device], flat_im, size, cudaMemcpyHostToDevice);

      // Start the kernel
      compute_scatter<<<nentries, 1>>> 
         (d_im2[device], d_cov2[device], nbands, vals_per_band, ndevices);
      }

   // Wait for them both to finish
   cudaDeviceSynchronize_chk();

   // Now, copy values back
   for (int device=0; device < ndevices; device++)
      {
      // See that we are talking with the right device
      cudaSetDevice_chk (device);

      // Copy data back
      int size = nbands * nbands * sizeof(real);
      h_cov2[device] = (real *)malloc_chk (size);
      cudaMemcpy_chk (
            h_cov2[device], d_cov2[device], size, cudaMemcpyDeviceToHost);
      }

   // Transfer data back to a matrix
   rhMatrix cov (nbands, nbands);
   for (int i=0; i<nbands; i++)
      for (int j=i; j<nbands; j++)
         {
         int whichband = (j-i) % ndevices;
         cov[i][j] = h_cov2[whichband][i*nbands+j]/vals_per_band - avg[i]*avg[j];
         }

   // The bottom part
   for (int i=0; i<nbands; i++)
      for (int j=0; j<i; j++)
         cov[i][j] = cov[j][i];

   // Finished with the device data
   for (int i=0; i<ndevices; i++)
      {
      cudaFree (d_cov2[i]);
      cudaFree (d_im2[i]);
      free (h_cov2[i]);
      }

   delete (cs); // Stop the timer

   printf ("Computed scatter matrix\n"); fflush(stdout);
   // cov.print (stdout, "%12.6e ");

   // Now, take the eigenvalue expansion of cov
   rhMatrix V;
   rhVector D;
   jacobi (cov, D, V);

   printf ("Finished eigenvalue expansion\n"); fflush(stdout);

   // Now, use this to generate the RBG image -- do this calculation in double
   printf ("Eigenvalues:\n");
   double sum = D.sum();
   double accum = 0.0;
   for1Dindex (i, D)
      {
      accum += D[i];
      double percent = (accum / sum) * 100.0;
      printf ("%03d: %.3e  = %6.3lf %%\n", i, D[i], percent);
      }

   // Get the vector
   rhVector v = V.column(0);

   // Now, make the image
   rhMatrix fim(idim, jdim);

   // Resample the image
   rhTimer2 *trs = new rhTimer2 ("compute principal image");
   for2Dindex (i, j, fim)
      {
      fim[i][j] = 0.0;
      for (int band=0; band<nbands; band++)
         fim[i][j] += v[band] * im[band][i][j];
      }
   delete (trs);

   // Work out the smallest and largest values
   real minval = fim[0][0];
   real maxval = fim[0][0];
   for2Dindex (i, j, fim)
      {
      if (fim[i][j] < minval) minval = fim[i][j];
      if (fim[i][j] > maxval) maxval = fim[i][j];
      }

   // Make an unsigned image - at the same time, transpose and take mirror image
   double scale = 255.0 / (maxval - minval);
   Unsigned_char_matrix uim (fim.jlow(), fim.jhigh(), fim.ilow(), fim.ihigh());
   for2Dindex (i, j, uim)
      uim[i][j] = (unsigned int) (scale * (fim[j][i] - minval) + 0.49);

   // Return this image
   return uim;
   }

void get_percentile_bounds (
      const vector<Unsigned_short_matrix> &im16,
      real low,
      real high,
      int *lowthreshold,
      int *highthreshold
      )
   {
   // Gets percentile bounds to scale the image

   // Make a histogram
   const int hist_size = 1<<16;
   Int_vector hist(0, hist_size-1);
   hist.clear(0);

   // Make the histogram
   for (int ch=0; ch<im16.size(); ch++)
      for2Dindex (i, j, im16[ch])
         hist[im16[ch][i][j]]++;

   // Find the total number of pixels
   int total = hist.sum();
   real lowval = low * total;
   real highval = high * total;

   // Now, find high and low values
   int accum = 0;
   bool lowset = false;
   for1Dindex (i, hist)
      {
      accum += hist[i];
      if (!lowset && accum >= lowval)   
         {
         *lowthreshold = i;
         lowset = true;
         }
      if (accum >= highval)
         {
         *highthreshold = i;
         break;
         }
      }
   }

int main (int argc, char *argv[])
   {
   rhTimer2::SetTimingOn();
   rhTimer2 *t = new rhTimer2("Whole program");

   // Set default image type
   tj_interface_set_output_image_type ("PNGImage");

   // The first argument is the file name
   if (argc != 2)
      {
      error_message ("Usage: infile.bin");
      return (EXIT_FAILURE);
      }

   // Get the file name
   char *fname = argv[1];

   // Open the file
   FILE *infile = FOPEN (fname, "rb");

   // Now, try reading the file
   // Assume that this works
   // Read the header of a bin file
   unsigned int w, h, nchannels, bytesperchannel;
   fread (&w, sizeof(int), 1, infile);
   fread (&h, sizeof(int), 1, infile);
   fread (&nchannels, sizeof(int), 1, infile);
   fread (&bytesperchannel, sizeof(int), 1, infile);

   // Now, print them out
   printf ("width           = %d\n", w);
   printf ("height          = %d\n", h);
   printf ("nchannels       = %d\n", nchannels);
   printf ("bytesperchannel = %d\n", bytesperchannel);

   // Now, try to read in the data
   if (bytesperchannel == 2)
      {
      // To carry the slices
      vector<Unsigned_short_matrix> im16 (nchannels);
      for (unsigned int i=0; i<nchannels; i++)
         im16[i].Init(0, w-1, 0, h-1);

      // To hold a pixel
      Unsigned_short_vector pixel (0, nchannels-1);

      // Now, read in pixel by pixel
      unsigned short tmax = 0;
      int tmaxch = 0;
      for2Dindex (i, j, im16[0])
         {
         // Read in a set of values
         // fread (&(pixel[0]), bytesperchannel, nchannels, infile);
         fread (&(pixel[0]), 1, bytesperchannel*nchannels, infile);

         // Now, distribute to the different images
         unsigned short vmax = 0;
         int maxch = 0;
         for1Dindex (ch, pixel)
            {
            if (pixel[ch] > vmax) 
               {
               vmax = pixel[ch];
               maxch = ch;
               }

            im16[ch][i][j] = pixel[ch];
            }

         if (vmax > tmax) 
            {
            tmax = vmax;
            tmaxch = maxch;
            }
         }

      printf ("All pixels : max = %d in channel %d\n", tmax, tmaxch); 
      fflush(stdout);

      // Now, write out each image

      // First, get bounds
      const real lowbound = 0.01;
      const real highbound = 0.99;
      int lowthreshold, highthreshold;
      get_percentile_bounds (im16, lowbound, highbound, 
            &lowthreshold, &highthreshold);

      printf ("Low threshold = %d,  high = %d\n", lowthreshold, highthreshold);
      fflush(stdout);

      // Do Karhunen-Loeve transform
      printf ("Doing KL-transform\n"); fflush (stdout);

      rhTimer2 klt ("KL_transform");
      Unsigned_char_matrix uim = KL_transform (im16, 1);

      // Make the outfile name
      change_extension (fname, ".bin", ".png");
      Unsigned_char_vector trans;
      Unsigned_char_matrix ucm = histogram_equalize(uim, trans);
      write_png_image (fname, ucm);
      }

   // Stop the timer and print the results
   delete (t);
   rhTimer2::print_results();

   // Done
   return EXIT_SUCCESS;
   }
