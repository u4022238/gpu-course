
# Modify the following to satisfy your directory structure and GPU architecture
declare -x OperatingSystem=Linux

if [ $HOSTNAME == "woodside" ]; then
   declare -x CUDADIR="/usr/local/cuda-9.0"
   declare -x MY_CUDA_ARCH=sm_52 # Titan X
elif [ $HOSTNAME == "djcam" ]; then
   declare -x CUDADIR="/usr/local/cuda-10.0"
   declare -x MY_CUDA_ARCH=sm_61 # GTX 1080 Ti
elif [ $HOSTNAME == "hartley-linux" ]; then
   declare -x CUDADIR="/usr/local/cuda-10.0"
   declare -x MY_CUDA_ARCH=sm_61 # GTX 1080
elif [ $HOSTNAME == "cecs-039614" ]; then
   declare -x CUDADIR="/usr/local/cuda-8.0"
   declare -x MY_CUDA_ARCH=sm_61 # GTX 1080 Ti
else
   declare -x CUDADIR="/usr/local/cuda-10.0"
   declare -x MY_CUDA_ARCH=sm_61 # GTX 1080
fi

#alias nvcc='$CUDADIR/bin/nvcc -arch=sm_61 -std=c++11 -DCUDA_DEVICE=__device__ -DCUDA_GLOBAL=__global__ -DCUDA="__host__ __device__"'
#alias nvprof="$CUDADIR/bin/nvprof"

#----------------------------------------------------
#  Linux operating sytem
#----------------------------------------------------
if  [ $OperatingSystem == 'Linux' ]; then 

   alias ls="ls -F"
   declare PS1="`hostname`-GPU-course > "
   declare -x uid="`id -u`"

   declare -x COMPILER=gcc
   declare -x PLATFORM=linux
   declare -x OS=linux

   # Turn off caps lock key -- this is 40 years obsolete
   alias nocaps='xmodmap -e "keycode 66 = Shift_L NoSymbol Shift_L"'
   #xmodmap -e "keycode 66 = Shift_L NoSymbol Shift_L"

   # Allow a big pointer, and also the "see" command
   alias bigpointer="dconf write /org/gnome/desktop/interface/cursor-size 48"
   alias see='thunar . >& /dev/null &'

   # Set up a /tmp directory

fi

# ============================================================================

# Environment variables
declare -x HOME=~
declare -x ARCH=${OS}-${COMPILER}
declare -x IUPACKAGEHOME="`pwd`"
declare -x TMP="${IUPACKAGEHOME}/tmp"

# Make the TMP directory if it does not exist already
if ! [ -d ${TMP} ] 
then
   mkdir ${TMP}
fi

#=============================================================================

# Define the PATH
declare -x PATH=\
:/usr/local/bin\
:/usr/bin\
:$IUPACKAGEHOME/bin\
:$IUPACKAGEHOME/Scripts/Shell\
:$IUPACKAGEHOME/Scripts/Perl\
:$IUPACKAGEHOME/lib.${ARCH}\
:$CUDADIR/bin\
:$PATH

#-----------------------------------------------------------
#
#		Sophisticated cd capability
#
#-----------------------------------------------------------
alias cn='if ( -e .dexit ) source .dexit ; declare src_cd=`pwd` ; if ( { chdir \!* } ) chdir \!* ; declare dest_cd = `pwd` ; 		\
   if ( "$src_cd" \!= "$dest_cd" ) then 		\
      declare back=$src_cd ; 			\
      pwd >> ${TMP}/dir$$				\
      pwd >> ${TMP}/alldirs			\
   endif ;					\
   if ( -e .denter ) source .denter'


# Enable my own command selection command
alias x='history | egrep "^[ 	:0-9]*\!*" > ${TMP}/commands$$ ; getdir ${TMP}/commands$$ ${TMP}/command$$ ; sed "'"s/[ 	:0-9]*//"'" ${TMP}/command$$ > ${TMP}/comm$$ ; source ${TMP}/comm$$ ; rm ${TMP}/commands$$ ${TMP}/command$$ ${TMP}/comm$$'

# Enable command for searching for past directories
alias dir='cat -n ${TMP}/${uid}_dir$$ | sort -r | sort +1 -u | sort | sed "'"s/[ 	0-9]*//"'" > ${TMP}/dd$$ ; mv ${TMP}/dd$$ ${TMP}/${uid}_dir$$ ; getdir ${TMP}/${uid}_dir$$ ${TMP}/adir$$ ; cd `cat ${TMP}/adir$$` ; pwd'

alias xdir='comb -i < ${TMP}/${uid}_alldirs > ${TMP}/dd$$ ; mv ${TMP}/dd$$ ${TMP}/${uid}_alldirs ; getdir ${TMP}/${uid}_alldirs ${TMP}/adir$$ ; cd `cat ${TMP}/adir$$` ; pwd'

alias xmake='comb -i < ${TMP}/${uid}_makedirs > ${TMP}/mk$$ ; mv ${TMP}/mk$$ ${TMP}/${uid}_makedirs ; getdir ${TMP}/${uid}_makedirs ${TMP}/mk$$ ; declare -x xmake_popdir="`pwd`" ; \cd `cat ${TMP}/mk$$` ; make ; \cd $xmake_popdir'

alias xaliases='comb -i < ${TMP}/${uid}_aliasdirs > ${TMP}/mk$$ ; mv ${TMP}/mk$$ ${TMP}/${uid}_aliasdirs ; getdir ${TMP}/${uid}_aliasdirs ${TMP}/mk$$ ; declare -x xalias_popdir="`pwd`" ; \cd `cat ${TMP}/mk$$` ; aliases ; \cd $xmake_popdir'

alias xvi='comb -i < ${TMP}/${uid}_allavi > ${TMP}/dd$$ ; mv ${TMP}/dd$$ ${TMP}/${uid}_allavi ; getdir ${TMP}/${uid}_allavi ${TMP}/adir$$ ; vi `cat ${TMP}/adir$$`'

alias xvid='comb -i < ${TMP}/${uid}_vi$$ > ${TMP}/dd$$ ; mv ${TMP}/dd$$ ${TMP}/${uid}_vi$$ ; getdir ${TMP}/${uid}_vi$$ ${TMP}/adir$$ ; vi `cat ${TMP}/adir$$`'

alias cd='source mycd dummy'
alias vi='source myvi'
alias make='pwd >> ${TMP}/${uid}_makedirs ; \make NODEPENDS:=1 NOTEST:=1'
alias aliases='pwd >> ${TMP}/${uid}_aliasdirs ; source aliases'

#=============================================================================
#
#  Sundry rubbish
#
#=============================================================================

# Set the umask - no one else to read or write
umask 077

# Enable file completion
declare filec

# Make a # be a comment
alias '#'=true

# Make less the same as more
alias more=less
alias ?="source hist_search"
alias ll='ls -alt'

# Directories for easy navigation
#source directories
alias markdir="source markdir.src"

# Just in case you give these vi commands in a shell
alias :q="echo :q is not a command"
alias :wq="echo :wq is not a command"
alias :w="echo :w is not a command"

# Get rid of ~ files
alias clean='rm *~ .*~ ~*'

#===========================================================================
# Make sure things are made
#===========================================================================
\cd bin
make -k >& /dev/null
\cd ..

