#############################################################################
## Generic makefile for gnumake
# Author: awf, Feb 97
#
#     PLEASE CONSULT ME ABOUT CHANGES TO THIS FILE.
#     It's not that I'm proprietorial about it, it's just that things
#     do tend to get duplicated or done wrong.  It's best therefore that
#     they go through one channel.
#
#     awf@robots.ox.ac.uk
#
# Notes:
#
# * Most makefiles assume sourcefile can be inferred from object file, but 
#   often this leads to great complexity.  In this, you generally supply the
#   sources that will do something, rather than the objects.
# 
# * This makefile should be included after all variables have been set,
#   i.e. generally at the end of the makefile.  This is to make it somewhat
#   easier to debug, by using := instead of = for the variables.
#   Any extra makefile targets should generally be put after this is included,
#   so that they have access to computed variables such as LDPATH etc.
#
# * It is intended that variables in UPPERCASE are "external" symbols,
#   lowercase names are internal to this file.
#
# * By criminy!  Gmake includes the spaces at the end of variable values
#   dir = fred   # sets dir to "fred   "
#
# * ":=" variables are expanded once
#   "="  variables are more like macros -- any $.. references are evaluated as late as possible.
# 
# * I would strongly recommmend font-lock-mode on this file.
#
# * A good way to debug make is to
#   make |& egrep '(Must remake)|^[a-zA-Z]'
#
#            ####################
#             INCLUDED MAKEFILES
#            ####################
#  * Please keep this section up to date *
#  
#  top.mk  # 2 lines, defines $(configdir)
#    generic.mk
#      identify-os.mk    # No user-servicable parts inside, just sets $(OS)
#      config-$(OS)-$(COMPILER).mk # ./configured from config.mk.in
#      	 $(OS)-$(COMPILER).mk  # Things that can't be ./configured about this OS
#      site.mk		 # *SITE* preferences.
#      user.mk           # Even more local preferences
#      build-$(BUILD).mk # Sets code-generation flags for different build types.
#      makevars.mk	 # defines variables users makefiles may use


########################################################################
#  If users makefiles want to set these variables, the setting must occur
#  before including top.mk.  Note that assignemnts
#  must use = or += rather than := if the value depends on a variable
#  defined by params.mk
#	USES
#	IULIBS
#
#	COMPILER
#	USE_STDC++
#	FREEVERSION
#	LIBDEST
#	IULIBHOME

#######################################################################
#  These are set by config, but can be overridden by values set in user's
#  makefiles before including params.mk
#	C++
#	FRESCOINCS
#	FRESCOLIBS
#	NO_BOOL
#	IVINSTALLED_DIR
#	ACE	
#	OMNIORB
#	OPENGL
#	IU_GLUT
#	XGL
#	TCL
#	MSQL
#	MYSQL
#	VTKHOME



#############################################################################
## Set generic flags.  All os-dependent configuration should be done here,
## so there should be no "ifeq ($(OS))" anywhere else in the makefiles.
## The first tasks are to set the OS and COMPILER variables, so that
## the appropriate config file can be found.

# 1a. Find target scripts directory
sys_or_iu_macro = $(firstword $(wildcard $(IUPACKAGEHOME)/$(file) $(SYS_IUPACKAGES)/$(file)))

sys_or_iu_Scripts := $(foreach file,Scripts,$(sys_or_iu_macro))

# 2. Set OS/compiler/site-dependent variables.
#    These are determined by configure, creating a config-$(OS)-$(COMPILER).mk
#    file from config.mk.in, and include the defaults just below, plus:
#      programs/command rules:
#      	LEX, RANLIB, LORDER, C++, CC, MAKEDEPEND,
#      	MAKE_SHLIB, MAKE_ARLIB
#      	
#      directories:
#      	CXX_HDRDIR, X11_INCDIR, X11_LIBDIR
#      	
#      flags:
#      	DEFINES, OS_DEFINES, pic, wall, ccflags,
#      	rpath, link-static, link-dynamic, CCOUTFLAG

# Initial defaults for most systems, to be overwritten by
# config-$(OS)-$(COMPILER).mk
TOUCH_OLD := touch
MKDIR_P := mkdir -p

# How to find the current directory
TJWD = $(IUPACKAGEHOME)/Scripts/Shell/tjwd

SEARCH := grep
RM := rm -f 
STRIP := strip
LN := ln -f -s
EMPTY_COMMAND := perl -e "0;";

# Compiler flags
debug := -g
optimize := -O2
profile := -pg
lib_m := -lm

# Config: EXESUFFIX
# Set to ".exe" for windows, nothing for unix.
ifndef EXESUFFIX
EXESUFFIX :=
endif

# Config: MAKE_EXE
# The command used to generate executables.  On most platforms this
# is $(C++)
MAKE_EXE = $(C++) 

# Config: MAKE_EXE
# The command used to generate shared libraries.  On most platforms this
# is $(C++) -shared
MAKE_SHLIB := $(C++) -shared

# =======================================
# Linking stuff

# How to make an archive
MAKE_ARLIB = rm -f $@ && ar qcv $@ 

# These should be in compiler specific files
# Config: LIB_EXT
# The file extension for shared libraries, e.g. dll, so, sl.
LIB_EXT := so

# Config: AR_EXT
# The file extension for archive libraries, "lib" for windows,
# "a" for unix.
AR_EXT := a

# Objects are listed like this
OBJ_EXT :=o

# to do "-o OBJECT" with microsoft is "/FoOBJECT"  file.C
# the flag is /Fo and file.o must touch it!
# Use OUTPUT_OPTION as that has precedent in SunOS make.
OUTPUT_OPTION = -o $@
# Gets even more fun, the output option for executables is different than obj files!
# even more fun, to build a shared library we use link which uses -out
# to build a program we use cl.exe which uses another option
LINK_OUT_OPTION = -o $@
LINKER_OUT_OPTION = -o $@

USE_LIB_VERSION := 0

# =======================================

# Generally want to use standard C++ library
ifndef USE_STDC++
USE_STDC++ := 1
endif

# Include the platform specific rules
include $(configdir)/config-$(OS)-$(COMPILER).mk

# for even make -k to fail if the system has not been configured yet
# It would be nice if it could print out a message, but it will send
# them to this line in this file, hello unconfigured folks!  WAH
ifeq "" "$(wildcard $(configdir)/config-$(OS)-$(COMPILER).mk)"
xall::
	your system has not been configured for $(OS)-$(COMPILER)
	run configure from $SYS_IUPACKAGES before make can be run
endif

#############################################################################
## Include makevars.mk which contains variables that may be used
## in defining variables such as USES that params.mk looks at

# include $(configdir)/makevars.mk


#########################
## Now all configurable parameters have been set.  No config-specifics
## beyond this point.  It still remains to determine the BUILD and OBJDIR,
## which are more preference variables than config variables.
#########################

# 3. Set BUILD using the same technique as was used for COMPILER.
#    Default build at most sites is shared, so set that here.
#    To override the default, reset BUILD in site.mk

# Option: BUILD
# Define what type of code to produce.  This causes the inclusion of
# config/build-$(BUILD).mk which sets flags such as optimization,
# debugging, profiling and whether to produce shared or static libraries.
# Common values are "shared", "noshared", "profiled".
BUILD := noshared

# 3. A "well known" build type -- i.e. one that is mentioned in the BUILDTYPES
#     variable may be made just using make build.
BUILDTYPES := noshared profiled shared

# 4. Set OBJDIR variable.

# Variable: OBJDIR
# A slight misnomer, OBJDIR is set to a string that describes the type of
# executables that the build will produce, e.g. "solaris" or
# "irix6-CC-n32.profiled".  The full pathnames are in $(RELOBJDIR) and
# $(ABSOBJDIR).

OBJDIR := $(OS)-$(COMPILER)

# Default OBJDIR is OBJDIR.build unless build is shared.
# This is a bit horrid, but good for old target compatibility.
ifneq "$(BUILD)" "shared"
  # OBJDIR := $(OBJDIR).$(BUILD)       -- Lets not do this
endif

# 4a. Set directory for installed executables
#	Use environment values for this path so programs outside of
#	make can find it.

# 5. Set user options

# Option: LINK_INTO_TMP
# The IRIX linker is incredibly slow over NFS, so setting this option causes
# linking to be directed into /tmp, speeding things up by a factor between 1.5
# and 3.  For solaris on a fast local disk, this slows the link down by about
# 20%.  However it is on by default because it also means that remaking a shared
# library while the application is running does not cause it to crash.

# LINK_INTO_TMP :=

# Option: NOTEST
# If defined, causes all tests (and other nonessential stuff) not to be built.
# This saves mucho disk space and is faster, but is an incredibly bad idea :)
# [Insert tales of much saved bacon due to Tests here ].
ifneq "$(BUILD)" "shared"
  NOTEST := echo
endif

#############################################################################
# Include site-specific definitions.
# The site.mk file overrides any of the previous defaults:
#  BUILD, OBJDIR, COMPILER
# It can even override config parameters although that would probably be
# silly.  Second, it sets variables such as extra compiler flags, peculiar
# opengl locations, etc.

#############################################################################

### include build definition
include $(configdir)/build-$(BUILD).mk

### include site-specific defines.
#include $(configdir)/site.mk

### include any auto generated defines
#-include $(IUPACKAGEHOME)/config/config-auto.mk

### include user specific defines if they are there 
# The user defines re-override site.mk definitions if supplied.
#-include $(IUPACKAGEHOME)/etc/user.mk


############################################################
# check for configure defined stuff here
# Set all HAS_ flags, and package includes and libs

ifeq (1,$(FREEVERSION))
DEFINES += -DFREEVERSION
endif

# check to see if OPENGL still has the default value given in config.mk.in
ifneq "$(OPENGL)" ""
HAS_OPENGL	:= 1
OPENGLHOMES	:= $(dir $(OPENGL)/)
# awf fixme 3 vars, similar names OPENGL_?LIBS?
OPENGLLIB	:= $(OPENGLLIBS)
OPENGLINCS	:= $(OPENGLHOMES)include
endif

# we can have the libs but not need an include on windows!
ifeq (1,$(HAS_OPENGL))
OPENGLDEF	:= -DOPENGLRENDER
ifndef OPENGL_LIBS
ifeq ($(OS),$(filter $(OS),win32 x64))
# OPENGL_LIBS := -lGlu32 -lGlaux -lOpengl32 -- obsolete.  RIH Apr 2011
OPENGL_LIBS := -lGlu32 -lOpengl32
else
ifeq (true,$(MESA))
OPENGL_LIBS     := -lMesaGLU -lMesaGL
else
OPENGL_LIBS     := -lGLU -lGL
endif
endif
endif
endif

ifneq "$(FRESCOINCS)"  ""
HAS_FRESCO := 1
FRESCO_LIBS :=  -lfigures -lwidgets -lbase -ldefs -locs -llayouts -lfdisplay -ljpeg -lpng -lz # -lpthread
ifneq ($(HAS_X11),1)
FRESCO_LIBS += -lGDI32 -lUSER32 -lwsock32
endif
endif

# Get pwd relative to IUPACKAGEHOME. (We assume the user knows the best
# filesystem name to use, accounting for automounters etc, and has used
# it in IUPACKAGEHOME)
PKGWD := $(shell $(TJWD))
PKGWD.. := $(patsubst %$/,%,$(dir $(PKGWD)))
PWD := $(IUPACKAGEHOME)$/$(PKGWD)
PWD.. := $(IUPACKAGEHOME)$/$(PKGWD..)

# Place where compiled object files go
RELOBJDIR := $(OBJDIR)
ABSOBJDIR := $(PWD)$/$(OBJDIR)

# Place where libraries go
tj_libdir := lib.$(OBJDIR)
LIBDEST := $(IUPACKAGEHOME)/lib.$(OBJDIR)
SYS_LIBDEST := $(SYS_IUPACKAGES)/lib.$(OBJDIR)

# Macro to find the libraries
sys_or_iu_macro_lib = $(firstword $(wildcard $(IUPACKAGEHOME)/$(file) $(SYS_IUPACKAGES)/$(file)))

#check to see if the system has NITFImage or not
sys_or_iu_NITF := $(foreach file,Image/NITF/NITFImage.h,$(sys_or_iu_macro))
ifneq (,$(sys_or_iu_NITF))
NITF := -lNITF -lImageProcessing -lIUCameras -lSpatialBasics -lGeometry
USES += Photogrammetry SpatialObjects
DEFINES += -DHAS_NITF
endif

# Handy make vars
null :=
spc := $(null) $(null)

