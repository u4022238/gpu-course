# Place for OPENGL 
HAS_OPENGL := 1
X11_INCDIR := /usr/X11R6/include

LIB_EXT := dll
AR_EXT := a
LIB_PREFIX := lib
MKDIR_P := mkdir -p

VCPROJSUFFIX := vcproj

######################################################################
# Acutally YES, -MD or -MDd are needed for Fresco 
# because Fresco is compiled using -MD or -MDd
# where MD is Multi-threaded DLL and MDd is Multi-threaded Debug DLL.
# Commented by Jae-Hak Kim, 25 Jan, 2006

debug = -DMULTITHREAD_DLL -DDEBUG 

######################################################################

optimize :=
optimize_for_speed := -MD -Ox -Ob2 -DNDEBUG
# dwh - optimization and debug flags to be used within studio stuff
profile := 

# How to make a library
MAKE_ARLIB = $(RM) $@ && ar qcv $@ 

# Linking Libraries
LINK_POSTFIX :=
LINK_LIB_FLAG := -l
LINK_PREFIX := -l
LIB_PATH := -L
LDFLAGS +=  $(ENTRY) -debug -pthread -lpthread
LOCAL_LIBS += -lz 

ifeq ($(strip $(CUDA_COMPILE)),1)
   LDFLAGS := $(ENTRY)
endif

# C++/CC options for standard c++ and gcc library
#C++ := g++ -static-stdc++
C++ := g++ -std=c++11
CC  := gcc -TC -fPIC -static-libgcc -std=c++11
CUDA := nvcc 
cudaflags = -arch=$(MY_CUDA_ARCH) -std=c++11 -DCUDA_COMPILE -DCUDA_DEVICE=__device__ -DCUDA_GLOBAL=__global__ -DCUDA="__host__ __device__"

DEFINES += -Dlinux -DGNU_CHRONO
DEFINES += -DNOREPOS -DDYNAMIC_GLOBALS -DWORDS_LITTLEENDIAN=1 -D_USE_MATH_DEFINES

# The following is for c++2011
ifeq ($(strip $(GEN_DCL)),1)
ccflags :=  
else
# The standard pre-2011
ccflags := 
endif


IUE_PERL := perl

STRIP := :
RM := rm -f
CP := cp
MAKE_SHLIB = $(C++)  -LDd 

MAKE_EXE := $(C++) 
ifeq ($(strip $(CUDA_COMPILE)),1)
   MAKE_EXE := nvcc -arch=$(MY_CUDA_ARCH) -std=c++11
endif

SEARCH := egrep


RM := rm -f

/ := /
OUTPUT_OPTION = -o $@
LINK_OUT_OPTION = -o $@

OBJ_EXT := o
EXESUFFIX :=

INCDIRS += $(IUPACKAGEHOME)/win32bin $(SYS_IUPACKAGES)/win32bin

CPP_MAKEDEPEND = 1
MAKEDEPEND = $(IUE_PERL) -S $(sys_or_iu_Scripts)/Perl/depend.pl -compiler $(C++) -extra_opt  $(RELOBJDIR) -E 

lib_m :=
MAKE = make

no_implicit_templates :=
implicit_templates :=

### Local Variables: ###
### mode:font-lock ###
### End: ###
