#Uncomment the below to use a local copy of GNU Make 3.80 version.
#MAKE=/home/user/bin/make 
#############################################################################
## Generic makefile for gnumake
# Module: Default targets
# Author: awf, Feb 97
# Notes:
#     PLEASE CONSULT ME ABOUT CHANGES TO THIS FILE.
#     It's not that I'm proprietorial about it, it's just that things
#     do tend to get duplicated or done wrong.  It's best therefore that
#     they go through one channel.
#
#     awf@robots.ox.ac.uk
#

# Place this target first, as a "forward declaration" of the default target.
all: xall


# Note that xall is a double-colon target.
# [can someone clearly explain double-colon here?]
# [perhaps via a simple example?]
xall::

# cancel implicit rules:
% : %.o
% : %.c
% : %.cc
% : %.C
% : %.p
% : %.f
% : %.F
% : %.r
% : %.s
% : %.S
% : %.mod
% : %.sh

# passthru targets to set compiler.
native: native.shared
native.%:
ifeq ($(OS),irix6)
	$(MAKE) BUILD=$(@:native.%=%) COMPILER=CC-n32
else
	$(MAKE) BUILD=$(@:native.%=%) COMPILER=CC CC=cc
endif

photon: photon.shared
photon.%:
	$(MAKE) BUILD=$(@:photon.%=%) COMPILER=KCC


# 3a. Passthru targets to set build.

# 3b. A "well known" build type -- i.e. one that is mentioned in the buildtypes
#     variable may be made just using make <build> or make all.<build>
$(BUILDTYPES) : % :
	$(MAKE) BUILD=$@

$(BUILDTYPES:%=all.%) : all.% :
	$(MAKE) BUILD=$(@:all.%=%)

# Hmm, had to dump this as it was trying to make all from all.$(OBJ_EXT):
#     Any build may be specified using make all.build
all.%:
	 $(MAKE) BUILD=$(@:all.%=%) --no-print-directory

# Targets to ensure that $(RELOBJDIR) is made
dirstamp := $(RELOBJDIR)$/stamp

$(dirstamp):
	[ -d $(RELOBJDIR) ] || $(MKDIR_P) $(RELOBJDIR)
	$(TOUCH_OLD) $@

ifeq ($(LIBRARY),)
iu_library_table_update:

else
xall::
	@echo ""
	@echo "-- Updating iu_library_table file - $(LIBRARY)"
	@echo "   Directory = `tjwd`"

iu_library_table_update:
	$(sys_or_iu_Scripts)/Shell/addlib.sh $(LIBRARY) `tjwd`
endif

ifneq ($(RELOBJDIR),$(LIBDEST))
# Target to ensure that $(LIBDEST) is made
$(LIBDEST)$/stamp:
	@ echo ""
	@ echo Destination for libraries is 
	@echo "   " $(LIBDEST)
	@ echo ""
	@ [ -d $(LIBDEST) ] || $(MKDIR_P) $(LIBDEST)
	@ $(TOUCH_OLD) $@

endif

#############################################################################
#############################################################################
### Set include and link paths

#############################################################################
# Set derived variables, assuming using target
# Variable:
#
# This section sets the compiler flags from the userdefined variables
#  USES -- A list of package names in specific-to-general order.
#  SOURCES -- List of source files to compile to objects.
#  OBJECTS -- List of additional objects whose compilation is handled in the
#             makefile.  If you find you're using this variable  to do something
#             generic, such as compile e.g. a YACC or pascal file, please consider
#             adding the rules to this rules.mk.  Mail targetjr@robots.ox.ac.uk if
#             you wonder how generic your rule will be.
#
# Compiler flags:
#  CCFLAGS -- Special code-generation flags.
#  INCDIRS -- Complete include-file search path, no -I flags.
#  DEFINES -- List of -D, -U flags for C preprocessor.
#
# Linking flags:
#  LIBDIRS -- Complete linker directory path, no -L flags.
#  LDLIBS -- Complete list of libraries to be loaded
#    LOCAL_LIBS -- First, depend on TargetJr
#    IULIBS     -- Second, portions of TargetJr selected.
#    IU3DLIBS   -- Used within IULIBS to select 3D rendering libraries (based
#                  on USE_RENDERING, OPENGLRENDER variables).
#    STDLIBS    -- Third, based on values of variables USE_X11, USE_SOCKETS etc.
#
#
# To modify behavior from the make command line, define the following variables
# with var='value':
#  CMDCFLAGS	-- adds flags to C compilation commands
#  CMDCCFLAGS	-- adds flags to C++ compilation commands
#  CMDLDFLAGS	-- adds flags to linker commands
#
#  CMDCFLAGS1, CMDCCFLAGS1, CMDLDFLAGS1 -- like above variables, but adds
#		   flags to the beginning of the generated list of flags
#		   rather than to the end.
#

# Add sections in specific-to-general order

#############################################################################
## 1. Defaults for LDLIBS, CCFLAGS.

# Variable: LDLIBS
# The list of -llibraries to link.  If not set, defaults to
#  -lTestLib $(LOCAL_LIBS) $(IULIBS) $(STDLIBS)

# This file is included after the makefile, so we override
# with the default only if a variable is not set.
ifdef MAKE_SHARED
#  LINK_TESTLIB = -lTestLib
   USE_DEFAULT_TESTLIB=1	# add TestLib to LIBDIRS later
endif 

# Switch to ifndef so LDLIBS = $(VAR) is seen as NON empty
ifndef LDLIBS
   LDLIBS = $(LINK_TESTLIB) $(LOCAL_LIBS) $(IULIBS) $(STDLIBS) $(MATLABLIBS) $(LOCAL_LIBS)
endif

#############################################################################
## 2. Build STDLIBS and LIBDIRS.

# Select: STDLIBS
# The list of libraries generated by the USES variable and USE_* flags.

## 2.1 Add TargetJr packages, from the USES variable.
# Use INCDIRS and LIBDIRS without -I or -L in case some systems need them specified
# differently.  This also has the advantage that dirs can be added below, such as
# X11_INCDIR, and systems which have it in /usr/include don't need to worry about
# the empty -I on the command line that a simple -I$(X11_INCDIR) would leave.

# REMEMBER:  Order is significant in this section -- add USES checks in the order
# that they would finally be linked: specific to general.


# * Target packages from the USES variable.
# For each package in USES, pathsearch for it on $(IUPACKAGEHOME):$(SYS_IUPACKAGES)
# Warning don't use $/ in this line, it will not work for win32, on the bright side 
# -I paths for the compiler on win32 seem to work with mixed / and \ !

sys_or_iu_GeneralUtility := $(foreach file,GeneralUtility,$(sys_or_iu_macro))
sys_or_iu_Scripts := $(foreach file,Scripts,$(sys_or_iu_macro))
sys_or_iu_lib := $(foreach file,$(tj_libdir),$(sys_or_iu_macro_lib))

# Variable: IU3DLIBS
# Set to the appropriate TargetJr libraries when USE_RENDERING is
# on.  Use it in your LDLIBS if you're linking the 3D stuff.
ifeq ($(strip $(USE_RENDERING)),1)
# * If use_rendering is on, then 
# we always want GenericView3D 
  IU3DLIBS += -lGenericView3D
  ifeq ($(HAS_OPENGL),1)
     IU3DLIBS += -lOpenGLView3D -lGenericView3D
     USE_OPENGL := 1
  endif
  ifeq ($(HAS_XGL),1)
     IU3DLIBS += -lXglView3D -lGenericView3D
     USE_XGL := 1
  endif
  ifeq ($(HAS_X11),1)
     USE_X11 := 1
  endif
endif

#=====================================================
# This is where the iu_library_table gets updated from
#=====================================================
ifneq ($(strip $(LIBRARY)),)
addlib_output := $(shell $(sys_or_iu_Scripts)/Shell/addlib.sh $(LIBRARY) `tjwd` Point1)
endif

#==============================================================================
### Automatic USES and IULIBS generation stuff
ifeq ($(strip $(GEN_IULIBS)),1)

ifeq ($(OS),$(filter $(OS),win32 x64))
RHLIBDEPENDENCIES_TABLE_DIRS += -t$(shell echo $(IUPACKAGEHOME) | sed -e "s@//\([A-Za-z]\)@\1:@g")
RHLIBDEPENDENCIES_TABLE_DIRS += -t$(shell echo $(SYS_IUPACKAGES) | sed -e "s@//\([A-Za-z]\)@\1:@g") 
else
RHLIBDEPENDENCIES_TABLE_DIRS += -t$(IUPACKAGEHOME) -t$(SYS_IUPACKAGES)
endif

RHLIBDEPENDENCIES := $(sys_or_iu_Scripts)/CC/$(RELOBJDIR)/makedependencies$(EXESUFIX) -i -a $(LIBDEPENDENCIES_OPTIONS) $(RHLIBDEPENDENCIES_TABLE_DIRS)

rh_ilibs  := $(patsubst -l%,%, $(IULIBS))
rh_info   := $(shell echo $(rh_ilibs) | $(RHLIBDEPENDENCIES) )
rh_uses   := $(patsubst -l%,, $(rh_info))
rh_ldlibs := $(patsubst -u%,, $(rh_info))

USES += $(patsubst -u%,%, $(rh_uses))
#   @echo USES = $(USES)   -- Causes "missing separator" error

ifeq ($(BUILD),shared)
IULIBS := $(rh_ldlibs)
else
# This was tripled -- Removed by RIH Sept 4, 2017
IULIBS := $(rh_ldlibs) $(rh_ldlibs)
endif

IU_LibDependencies : makefile 
	@echo ""
	@echo "-- Updating IU_LibDependencies"
	@echo $(rh_ilibs) > IU_LibDependencies
else
IULIBS := $(patsubst -l-l%,-l%, $(foreach lib, $(IULIBS), -l$(lib)))
IU_LibDependencies : makefile 
	@echo ""
	@echo "-- Updating IU_LibDependencies"
	@echo $(patsubst -l%,%, $(IULIBS)) > IU_LibDependencies
endif
# end of GEN_IULIBS

#==============================================================================

#### hack in Numerics for COOL
ifeq (,$(findstring Numerics,$(USES)))
  ifneq (,$(findstring COOL,$(USES)))
xall::
	@echo $(PWD)"/makefile:1: warning: You now need to use Numerics with COOL"
    USES += Numerics
  endif
endif

# test for missing packages and warn the user
SYS_OR_IUUSES := $(foreach file,$(USES),$(sys_or_iu_macro))

uses_count := $(words $(USES))
comp_uses_count := $(words $(SYS_OR_IUUSES))
ifneq ($(uses_count), $(comp_uses_count))
xall::
	@echo '***Warning***: in '$(PWD)/makefile
	@echo "   not all packages in USES found"
	@echo "   Used packages = \""$(USES)"\""
	@echo "   Found packages = \""$(subst $(IUPACKAGEHOME)$/,,$(SYS_OR_IUUSES))"\""
	@echo "   Check iu_library_table:  Case in directory names MUST BE RIGHT!!"
	
endif

INCDIRS += $(SYS_OR_IUUSES)

-include $(configdir)/site-uses.mk

## 2.2 Add to LIBDIRS and STDLIBS, based on USE_* variables.

# * COOL
ifneq "" "$(findstring COOL,$(USES))"
  STDLIBS  += -lTJCOOL
endif

# * Numerics
ifneq "" "$(findstring Numerics,$(USES))"
  #STDLIBS  += -liue-math -lnetlib
endif

ifneq "" "$(findstring COOL,$(USES))$(findstring Numerics,$(USES))"
ifeq "" "$(findstring -DNOREPOS,$(OS_DEFINES))"
  DEFINES += # -DNOREPOS 
endif
endif

# Places where libraries may be
LIBDIRS += $(LIBDEST) $(SYS_LIBDEST)

# * Standard C++ includes
# Set USE_STDC++ to 0 in order not to include "Smallg++"
ifeq ($(strip $(USE_STDC++)),1)
  SYS_OR_IU_SMALLG := $(firstword $(wildcard $(IUPACKAGEHOME)/Smallg++ $(SYS_IUPACKAGES)/Smallg++))
endif

## COOL backward compat
ifneq (,$(findstring -lTJCOOL,$(LDLIBS)))
  ifeq (,$(findstring iue-math,$(LDLIBS)))
xall::
	@echo $(PWD)"/makefile:1: warning: You now need to use -liue-math -lnetlib with -lTJCOOL"
#STDLIBS += -liue-math -lnetlib
  endif
endif

# Select: USE_STDC++
# Set to 0 in order not to link "Smallg++", the TargetJr ANSI C++ library.
# Add stdc++ -- only needed for iostream (cout,cerr,operator<<())
#  -- should disappear
ifeq ($(strip $(USE_STDC++)),1)
  STDLIBS += -lSmallg++
  STDLIBS += $(ns-link-static) $(AC_STDCXX_LIB) $(ns-link-dynamic)
endif

############# 3rd-party libraries start here
## "3rd-party" means non-target libraries that one compiles and installs independently. 

# Fresco library
# Select: USE_FRESCO
# Set to 1 in order to add Fresco headers and libraries to the compile.
# If HAS_FRESCO has not been set, adds -UHAS_FRESCO to DEFINES
ifeq ($(strip $(USE_FRESCO)),1)
 INCDIRS += $(FRESCOHOME) $(FRESCOHOME)/Build
 ifdef HAS_FRESCO
  INCDIRS += $(FRESCOINCS) $(subst $/lib,,$(FRESCOLIBS))
  # Fresco only uses the config.h on unix
  ifneq ($(OS),$(filter $(OS),win32 x64 linux))
     DEFINES += -DHAVE_CONFIG_H
  endif
  LIBDIRS += $(FRESCOLIBS)
  DIR_Parmesan = $(foreach file,Parmesan,$(sys_or_iu_macro_lib))
  STDLIBS += $(FRESCO_LIBS)
  ifeq (1,$(strip $(HAS_X11)))
    IUDXLIBS += -lDXKitTj -lDXKit 
    USE_X11 := 1
  endif
 else
  MINI_SO_TESTS =
  PROGRAM =
  SOURCES =
  MINI_SO_SOURCES =
  MINI_PROG_SOURCES =
  LIBRARY =

xall::
	@echo Your system is not configured for Fresco, can not build here.
	@echo Re-Run configure from the IUPACKAGEHOME or SYS_IUPACKAGES with
	@echo --fresco=/path/to/fresco
	@echo all build targets have been removed.
  # Comment on nonexistence of fresco
  DEFINES += -UHAS_FRESCO
 endif
endif

# if the user tries to linke in -lImageClasses, then add -lJPEG

# make sure they do not have it already
ifeq "" "$(findstring -lJPEG,$(IULIBS))"
# only do this if they have -lImageClasses
  ifneq "" "$(findstring -lImageClasses,$(IULIBS))"
# make sure the user is not getting -ljpeg from USE_FRESCO
    ifeq "" "$(findstring -ljpeg,$(STDLIBS))"
      STDLIBS += -lJPEG
    endif
  endif
endif

# tcl library
# Select: USE_TCL
ifeq  ($(strip $(USE_TCL)),1)
 ifeq ($(strip $(HAS_TCL)),1)
  INCDIRS += $(TCLINCS)
  C_INCDIRS += $(TCLINCS)
  LIBDIRS += $(TCLLIB)
  DEFINES += $(TCLDEFINES)
  STDLIBS += $(TCLLIBS)
 else
  # Useful as a comment anyway..
  DEFINES += -UTCL_INSTALLED
 endif
endif


# msql library
# Select: USE_MSQL
ifeq  ($(strip $(USE_MSQL)),1)
  INCDIRS += $(MSQLINCS)
  LIBDIRS += $(MSQLLIB)
  DEFINES += $(MSQLDEF)
  STDLIBS += $(MSQLLIBS)
  USE_SOCKETS := 1
endif

############# 3rd-party C libraries

# mysql library
# Select: USE_MYSQL
ifeq  ($(strip $(USE_MYSQL)),1)
  INCDIRS += $(MYSQLINCS)
  LIBDIRS += $(MYSQLLIB)
  DEFINES += $(MYSQLDEF)
  STDLIBS += $(MYSQLLIBS)
  USE_SOCKETS := 1
endif

# VTK
# Select: USE_VTK
# Set to 1 to include the VTK headers and libraries
ifeq ($(strip $(USE_VTK)),1)
   INCDIRS += $(VTK_INC_DIR)
   LIBDIRS += $(VTK_LIB_DIR)
   DEFINES += $(VTK_DEFINES)
   STDLIBS += $(VTK_LIBS)
   ifeq ($(HAS_X11),1)
      USE_X11 := 1
   endif
endif

# GLUT (Open GL) libraries
# Select: USE_GLUT
ifeq ($(strip $(USE_GLUT)),1)
  INCDIRS += $(IU_GLUTINCS)
  LIBDIRS += $(IU_GLUTLIB)
  DEFINES += $(IU_GLUTDEF)
  STDLIBS += $(IU_GLUT_LIBS) 
endif

# Open GL libraries
# Select: USE_OPENGL
ifeq ($(strip $(USE_OPENGL)),1)
  INCDIRS += $(OPENGLINCS)
  LIBDIRS += $(OPENGLLIB)
  DEFINES += $(OPENGLDEF)
  ifndef MAKE_SHARED
##### pcp - i found this in rules.mk on the O2 build
##### looked useful so I checked it in
### Hmmm.  On SGI with native GL, should use shared libs...  How do we decide?
### What we really mean is that libraries supplied by the OS, such as libc and libX11
### should be dynamic, byt 3rd party such as Mesa should be static.  This can be achieved
### by having static Mesa in a different dir, or always having static mesa... hmm.
    STDLIBS += $(OPENGL_LIBS)
  else
    STDLIBS += $(OPENGL_LIBS) 
  endif # pcp - this endif was commented out 
endif

# XGL libraries
# Select: USE_XGL
ifeq ($(strip $(USE_XGL)),1)
  INCDIRS += $(XGLINCS)
  LIBDIRS += $(XGLLIB)
  ifeq ($(words $(wildcard $(XGL)/lib/libxgl.a)),0)
    STDLIBS += $(XGL_LIBS)
  else
    STDLIBS += $(link-static) $(XGL_LIBS) $(link-dynamic)
  endif
  DEFINES += $(XGLDEF)
endif

# SGI OpenGL Software Development Kit
ifeq ($(strip $(USE_SGI_OPENGL)),1)
  ifeq ($(OS),$(filter $(OS),win32 x64))
# Select: USE_SGI_OPENGL
    INCDIRS += $(SGI_OPENGLINCS)
    LIBDIRS += $(SGI_OPENGLLIBS)
    STDLIBS += -lglu -lopengl
  else
    NOT_WIN_CLEAR_BUILD := 1
  endif
endif # win32

# Microsoft WinG Software Development Kit
ifeq ($(strip $(USE_WING)),1)
  ifeq ($(OS),$(filter $(OS),win32 x64))
  # Wing libraries
  # Select: USE_WING
    INCDIRS += $(WINGINCS)
    LIBDIRS += $(WINGLIBS)
    STDLIBS += -lWing32
  else
    NOT_WIN_CLEAR_BUILD := 1
  endif
endif # win32

# Microsoft Windows API
ifeq ($(strip $(USE_WINAPI)),1)
 ifeq ($(OS),$(filter $(OS),win32 x64))
 # Select: USE_WINAPI
   #INCDIRS += $(WINAPIINCS)
   LIBDIRS += $(WINAPILIBS)
   STDLIBS += Gdi32.lib User32.lib Comdlg32.lib Comctl32.lib Ws2_32.lib Advapi32.lib version.lib
 #  STDLIBS += Gdi32.lib User32.lib Comdlg32.lib Comctl32.lib Ws2_32.lib Advapi32.lib kernel32.lib
#   STDLIBS += Gdi32.lib User32.lib Comdlg32.lib Comctl32.lib Ws2_32.lib Advapi32.lib kernel32.lib version.lib
 else
   NOT_WIN_CLEAR_BUILD := 1
 endif
endif # win32

# Microsoft Foundation Class API
# Select: USE_MFC
ifeq ($(strip $(USE_MFC)),1)
  ifeq ($(OS),$(filter $(OS),win32 x64))
    INCDIRS += $(MFCINCS)
    LIBDIRS += $(MFCLIBS)
  else
    NOT_WIN_CLEAR_BUILD := 1
  endif
endif # win32

# Microsoft Foundation Class API
# Select: USE_OLE
ifeq ($(strip $(USE_OLE)),1)
  ifeq ($(OS),$(filter $(OS),win32 x64))
    #INCDIRS += $(OLEINCS)
    LIBDIRS += $(OLELIBS)
    STDLIBS += Ole32.lib Oleaut32.lib Uuid.lib
  else
    NOT_WIN_CLEAR_BUILD := 1
  endif
endif # win32

# Microsoft ATL API
# Select: USE_ATL
ifeq ($(strip $(USE_ATL)),1)
  ifeq ($(OS),$(filter $(OS),win32 x64))
    INCDIRS += $(ATLINCS)
    LIBDIRS += $(ATLLIBS)
  else
    NOT_WIN_CLEAR_BUILD := 1
  endif
endif # win32


# clear out all the build if windows stuff is built on a non windows enviroment
ifdef NOT_WIN_CLEAR_BUILD
  SOURCES =
  PROGRAM =
  LIBRARY =
xall::
	@echo "Warning, SOURCE PROGRAM and LIBRARY CLEARED"
	@echo "MICROSOFT DEPENDENT CODE CAN NOT BE BUILT ON UNIX"
endif


# ACE
# Select: USE_ACE
# Set to 1 to include the ACE headers and libACE
ifeq ($(strip $(USE_ACE)),1)
   INCDIRS += $(ACE_INC_DIR)
   LIBDIRS += $(ACELIB)
   DEFINES += $(ACEDEF) $(ACE_CXXFLAGS)
   STDLIBS += $(ACELIBS)
endif

# OMNIORB
# Select: USE_OMNIORB
# Set to 1 to include the OMNIORB headers and the omniorb libraries
ifeq ($(strip $(USE_OMNIORB)),1)
   INCDIRS += $(OMNI_INC_DIR)
   LIBDIRS += $(OMNI_LIB_DIR)
   DEFINES += -DWITH_OMNIORB $(OMNI_DFLAGS) $(OMNI_CXXFLAGS)
   STDLIBS += $(OMNI_LIBS)
endif


# Switch off "-Bdynamic" or "-Bstatic" for the remaining libs:
STDLIBS += $(no-link-dynamic)

# Select: USE_X11
# Set to 1 to use X11 libraries
INCDIRS += /usr/X11R6/include
C_INCDIRS += /usr/X11R6/include
ifeq ($(strip $(USE_X11)),1)
 ifeq ($(strip $(HAS_X11)),1)
  INCDIRS += $(X11_INCDIR)
  C_INCDIRS += $(X11_INCDIR)
  LIBDIRS += $(X11_LIBDIR)
  STDLIBS += -lXext -lX11
  USE_SOCKETS := 1
ifdef USING_GCC_COMPILER
  OS_DEFINES += -isystem $(X11_INCDIR)
endif
 endif
endif

# QT
# Select: USE_QT
ifeq ($(strip $(USE_QT)),1)
 DEFINES += -DHAS_QT
 ifeq ($(OS),$(filter $(OS),win32 x64))
  DEFINES += -DQT_DLL -DQT_THREAD_SUPPORT

  INCDIRS += $(QT_INC_DIR)
  LIBDIRS += $(QT_LIB_DIR)

  QT_MOC = $(QT_MOC_DIR)/moc
  QT_LIBS := qt-mt230nc.lib

  STDLIBS += $(QT_LIBS)
 endif

# rule to make moc files
moc_%.cc: %.h
	$(QT_MOC) $(@:moc_%.cc=%.h) -o $@

moc_%.cpp: %.h
	$(QT_MOC) $(@:moc_%.cpp=%.h) -o $@

realclean::
	-$(RM) moc*.cc moc*.cpp

clean::
	-$(RM) moc*.cc moc*.cpp
endif


# Finally include the windows default LIBDIRS path
LIBDIRS += $(VCC_LIBPATH)
LIBDIRS += $(VCCSDK_LIBPATH)
LIBDIRS += $(LOCAL_LIBDIRS)

# Select: USE_SOCKETS
ifeq ($(strip $(USE_SOCKETS)),1)
  STDLIBS += $(lib_sockets)
endif

# Dynamic load library.
STDLIBS += $(lib_dl)

# Math library
STDLIBS += $(lib_m) $(lib_c)

#############################################################################
#############################################################################

### It becomes more generic from here....

## Assemble the final flags.

# The tj_* flags are those specific to target as a library,
# Imagine tj_CPPFLAGS to be equivalent to -I/usr/X11/include, although
# it's much longer.
#
# Then the non-prefixed versions are made from the prefixed ones and any others
# that are needed, just as a standard compilation which uses many libraries does.

## CPPFLAGS included after defines and before includes so both can be overridden:
## defines by -U, includes by prefixing the correct one.

# Config: CVFLAGS
# Compiler Verbosity flags, may be overridden by specifying in site.mk
ifndef CVFLAGS
 CVFLAGS := $(wall)
endif

# Config: CGFLAGS
# Code Generation Flags; passed to cc1,cc1plus,ld.
# Set in build-$(BUILD).mk

SG_INCDIRS = $(INCDIRS) $(SYS_OR_IU_SMALLG) $(VCCSDK_INCLUDE_PATH)
include_flags = $(SG_INCDIRS:%=-I%) $(VCC_INCLUDE_FLAGS)
ifeq ($(OS),$(filter $(OS),win32 x64))
include_flags := $(shell echo $(include_flags) |sed -e "s@//\([A-Za-z]\)@\1:@g")
endif
tj_CPPFLAGS = $(include_flags)

tj_CGFLAGS = $(CMDCFLAGS1) $(CGFLAGS) $(CVFLAGS) $(ccflags) $(CMDCFLAGS)
tj_CCFLAGS = $(CMDCCFLAGS1) $(tj_CGFLAGS) $(APP_CCFLAGS) $(CPPFLAGS) $(CMDCCFLAGS)

# For CUDA -- added by RIH Mar 13, 2018.  Not sure what all these flags are
#tj_CUDAGFLAGS = $(CGFLAGS) $(CVFLAGS) $(cudaflags) 
tj_CUDAFLAGS = $(cudaflags) $(CPPFLAGS) 

# jb@aai -- removed CGFLAGS from LDFLAGS because the $(pic) was
# getting into the link, which was causing problems in certain cases
# (alphatech).  Also, code isn't really being generated at link time
# anyway.  Note that the CGFLAGS still appears when creating shared
# libraries (see rule further below for creating shared libs, which
# puts tj_CGFLAGS on the command line)

# tj_LDFLAGS = $(CGFLAGS) $(LDFLAGS)
tj_LDFLAGS = $(CMDLDFLAGS1) $(LDFLAGS) $(CMDLDFLAGS)

####
# Assemble the final versions
# EHsc for .NET -- should be elsewhere
CPPFLAGS += $(OS_DEFINES) $(DEFINES) $(tj_CPPFLAGS)
ifeq ($(COMPILER),VC90)
CPPFLAGS += -EHsc
endif
CFLAGS   = $(C_INCDIRS:%=-I%) $(CGFLAGS) $(APP_CFLAGS) $(CPPFLAGS)
CCFLAGS  = $(CGFLAGS) $(APP_CCFLAGS) $(CPPFLAGS)

# Internal: LDPATH
# Made from LIBDIRS, holds appropriate -L and -R or -rpath flags.
LDPATH := $(LIBDIRS:%=$(LIB_PATH)%) $(VCC_LIB_FLAGS)
ifneq ($(RPATHLIBDIRS),)
LDPATH += $(rpath)
endif

# convert unix slash to win32 slash
#ifeq ($(OS),$(filter $(OS),win32 x64))
#win32_LDPATH = $(shell echo $(LDPATH) |sed -e "s@//\([A-Za-z]\)@\1:@g")
#LDPATH := $(subst /,\\,$(win32_LDPATH))
#endif
# Temporarily commented Mar 8th 2008 RIH

# Internal: LOADLIBES
# LOADLIBES is used by the default GNU make rules for linking executables.
LOADLIBES := $(LDPATH)


# "Harden" LDLIBS.  This means that any $(...) references in LDLIBS are
# expanded at this point, and future changes to the referred-to variables
# have no effect.  Specifically, any changes to IULIBS, STDLIBS, etc after 
# the inclusion of rules.mk will be ignored.

# Also, for now, double the library path to make sure we get it all
# Undone by RIH -- 4 Sept 2017
#LDLIBS := $(LDLIBS) 

# Backward-compatibilty hacks on LDLIBS
ifneq (,$(findstring -lCOOL,$(LDLIBS)))
xall::
	@echo $(PWD)"/makefile:1: warning: -lCOOL should be changed to -lTJCOOL"
endif
LDLIBS := $(subst -lCOOL,-lTJCOOL,$(LDLIBS))

# Convert LDLIBS to -l form.
LDLIBS := $(patsubst -l%,$(LINK_PREFIX)%$(LINK_POSTFIX),$(LDLIBS))

#############################################################################

# basenames etc.
srcbase := $(basename $(SOURCES))

## Some hairy macros to get the current source file from the
# dependency list.  The problem is that when we have an object file,
# we can't infer its suffix, and we don't want to do it by wildcarding
# as it's not uncommon to have a file hanging around with the same
# basename but a different suffix.  (Two ways that happens are (a)
# changing the suffix of a file and not removing the old one
# e.g. "promoting" a file from C to C++ and (b) autogenerated .c
# files).  Therefore we want to get back to the original source file
# that the makefile mentions.

# Really we want the %.C on the right hand side of the rule, but there appears
# no way to get at it cleanly.  Instead, we assume that the %.C appears first in
# the dependency list *before* any headers or suchlike, and use GNU make's
# special variable $^, which contains all the dependencies, in the order they
# were specified.

source_for_object = $(firstword $^)

# Note that "source_for_object = $(filter $(baseat).%,$(allsources))" fails for
# files with dots because base.instances.C and base.C will both be extracted for
# file base.C Oh for regexp matching...

#############################################################################
# Targets
#
# Defines the standard targets based on whether variables are set:
#  SUBDIRS: List of subdirs to recurse into after other making.
#  LIBRARY, VERSION: Make library with name LIBRARY.
#                    Shared library is version VERSION.

# First combine sources by type.  For example, MINI_SO_TESTS are MINI_SO_SOURCES

ifndef NOTEST

ifdef MINI_SO_TESTS
MINI_SO_SOURCES += $(MINI_SO_TESTS)
MINI_PROG_SOURCES += TestDriver.C
pre_deps += TestDriver.C 
# Note: TestDriver.C is an autogenerated file, its rule is below.
endif

endif

# Template instantiations should reside in files with the extension .C or .cc
# in subdirectory Templates.  They are automatically added to SOURCES.

allsources_tmpl := $(wildcard Templates/*.C) $(wildcard Templates/*.cc)
TEMPLATE_SOURCES += $(allsources_tmpl)

################ Build objects from $(SOURCES)

# Combine all sources into one variable so that all objects are made from it,
# even though the objects might be used in various nefarious ways
allsources := $(SOURCES) $(MINI_PROG_SOURCES) $(MINI_SO_SOURCES)

## Separate allsources based on suffix
allsources_cu := $(filter %.cu,$(allsources))
allsources_C := $(filter %.C,$(allsources))
allsources_cxx := $(filter %.cxx,$(allsources))
allsources_cpp := $(filter %.cpp,$(allsources))
allsources_c++ := $(filter %.c++,$(allsources))
allsources_cc := $(filter %.cc,$(allsources))
allsources_yy := $(filter %.yy,$(allsources))
allsources_ll := $(filter %.ll,$(allsources))
allsources_c := $(filter %.c,$(allsources))
allsources_y := $(filter %.y,$(allsources))
allsources_l := $(filter %.l,$(allsources))
allsources_S := $(filter %.S,$(allsources))
allsources_s := $(filter %.s,$(allsources))

allsources_rc := $(filter %.rc,$(RESOURCE_SOURCES))

# Add to all .cc files those generated from yacc, lex
#allsources_cc += $(allsources_yy:%.yy=%.cc)
#allsources_cc += $(allsources_ll:%.ll=%.cc)
#allsources_c += $(allsources_y:%.y=%.c)
#allsources_c += $(allsources_l:%.l=%.c)

## Build objects from suffixed sources
allsources_obj_cu := $(allsources_cu:%.cu=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_C := $(allsources_C:%.C=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_cxx := $(allsources_cxx:%.cxx=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_cpp := $(allsources_cpp:%.cpp=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_c++ := $(allsources_c++:%.c++=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_cc := $(allsources_cc:%.cc=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_c := $(allsources_c:%.c=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_S := $(allsources_S:%.S=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_s := $(allsources_s:%.s=$(RELOBJDIR)$/%.$(OBJ_EXT))

allsources_res_rc := $(allsources_rc:%.rc=$(RELOBJDIR)$/%.$(RES_EXT))

allsources_cc_y := $(allsources_y:%.y=%.c++)
allsources_obj_y := $(allsources_y:%.y=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_cc_l := $(allsources_l:%.l=%.c++)
allsources_obj_l := $(allsources_l:%.l=$(RELOBJDIR)$/%.$(OBJ_EXT))

allsources_tmpl_C := $(filter %.C,$(allsources_tmpl))
allsources_obj_tmpl_C := $(allsources_tmpl_C:%.C=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_tmpl_cc := $(filter %.cc,$(allsources_tmpl))
allsources_obj_tmpl_cc := $(allsources_tmpl_cc:%.cc=$(RELOBJDIR)$/%.$(OBJ_EXT))
allsources_obj_tmpl := $(allsources_obj_tmpl_C) $(allsources_obj_tmpl_cc) 

## Recombine just the objects from SOURCES -- these will be used to go into
## the "main" target in a directory.
SOURCES_res += $(patsubst %,$(RELOBJDIR)$/%.$(RES_EXT),$(basename $(RESOURCE_SOURCES)))
SOURCES_obj += $(patsubst %,$(RELOBJDIR)$/%.$(OBJ_EXT),$(basename $(SOURCES)))
SOURCES_obj += $(patsubst %,$(RELOBJDIR)$/%.$(OBJ_EXT),$(basename $(TEMPLATE_SOURCES)))

# $(baseat)  Expands to the basename of the current target, with .$(OBJ_EXT) stripped
baseat = $(@:$(RELOBJDIR)$/%.$(OBJ_EXT)=%)

extraflags_for_object = $(FLAGS_$(source_for_object))

# Option: CLEAN
# Set in site.mk, or on make line, to indicate that object files should be
# removed after successfully building the library or executables.
CLEAN_FILES := $(RELOBJDIR)$/*.$(OBJ_EXT).i $(RELOBJDIR)$/Templates$/*.$(OBJ_EXT).i\
               $(RELOBJDIR)$/ii_files$/* $(RELOBJDIR)$/Templates$/ii_files$/*
ifdef CLEAN
CLEAN_FILES += $(SOURCES_obj) $(SOURCES:%=$(RELOBJDIR)$/%.d) \
               $(RELOBJDIR)$/Templates$/*.d $(RELOBJDIR)$/vc50.pdb \
               $(RELOBJDIR)$/*.def $(RELOBJDIR)$/ex_*.exe $(LIBDEST)$/$(LIBRARY).pdb \
               $(LIBDEST)$/$(LIBRARY).ilk $(LIBDEST)$/$(LIBRARY).exp
endif

# check if REQUIRES_IMPLICIT_TEMPLATES is set
# if so, use implicit templates
ifeq (1,$(strip $(REQUIRES_IMPLICIT_TEMPLATES)))
no_implicit_templates := ${implicit_templates}
endif


# Config: PRAGMA_INSTANTIATE_TEMPLATES
# Set by configure, or in $(OS)-$(COMPILER).mk to indicate that the compiler
# uses #pragma instantiate Class<T> rather than template class Class<T> to
# instantiate templates.
# Now obsolete, iue++ does the work


define COMPILE.cuda
	$(MKDIR_P) $(@D)
	$(CUDA) $(source_for_object) $(no_implicit_templates) $(tj_CUDAFLAGS) $(extraflags_for_object) --device-c $(OUTPUT_OPTION)
endef

define COMPILE.c++
	$(MKDIR_P) $(@D)
	$(C++) $(source_for_object) $(no_implicit_templates) $(tj_CCFLAGS) $(extraflags_for_object) -c $(OUTPUT_OPTION)
endef

define COMPILE.tmpl
	$(MKDIR_P) $(@D)
	$(C++) $(force_c++) $(DOS_FULL_SRC_PATH)$(source_for_object) $(implicit_templates_in_subdir) \
	  -DINSTANTIATE_TEMPLATES $(tj_CCFLAGS) $(extraflags_for_object) -c $(OUTPUT_OPTION)
endef

# Sunpro CC does not understand the .S suffix
# COMPILE.S
ifndef OS_COMPILE.S
COMPILE.S = $(CC) $(source_for_object) $(extraflags_for_object) $(CFLAGS) -c $(OUTPUT_OPTION)
else
COMPILE.S = $(OS_COMPILE.S)
endif

# COMPILE.s
ifndef OS_COMPILE.s
COMPILE.s = $(COMPILE.S)
else
COMPILE.s = $(OS_COMPILE.s)
endif

## Note: extraflags_for_object should be after defaults

define COMPILE.c
	@$(MKDIR_P) $(@D)
	$(CC) $(DOS_FULL_SRC_PATH)$(source_for_object) $(CFLAGS) $(extraflags_for_object) -c $(OUTPUT_OPTION)
endef
COMPILE.y = bison -d -t -o $@ $(YACCFLAGS) $(source_for_object) && mv $@.h $(subst .c++,.h,$@)
COMPILE.l = $(LEX) -t $(LEXFLAGS) $(source_for_object) > $@

nullobj := $(RELOBJDIR)$/null.$(OBJ_EXT)

#ifdef noisy_make
DASH_LINE = echo "------------------------------------------------------------"
VERBOSE_COMPILE = @echo "" ; echo "-- Compiling $<"
CDLINE = echo ""; $(DASH_LINE) ; echo "-- Compiling directory $(@D)" ; echo ""
#else
#VERBOSE_COMPILE = @true
#CDLINE = @true
#endif

## Now make dependencies for source files
# yacc (bison) and lex (flex) added by Peter Vanroose - 24 Oct 1997
$(allsources_obj_cu) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cu
	$(VERBOSE_COMPILE)
	$(COMPILE.cuda)
$(allsources_obj_C) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.C
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_obj_cc) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cc
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_obj_cxx) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cxx
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_obj_cpp) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cpp
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_obj_c++) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.c++
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_obj_c) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.c
	$(VERBOSE_COMPILE)
	$(COMPILE.c)
$(allsources_obj_tmpl_C) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.C
	$(VERBOSE_COMPILE)
	$(COMPILE.tmpl)
$(allsources_obj_tmpl_cc) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cc
	$(VERBOSE_COMPILE)
	$(COMPILE.tmpl)
$(allsources_obj_y) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.c++
	$(VERBOSE_COMPILE)
	$(COMPILE.c++)
$(allsources_cc_y) : %.c++ : %.y
	$(VERBOSE_COMPILE)
	$(COMPILE.y)
$(allsources_cc_l) : %.c++ : %.l
	$(VERBOSE_COMPILE)
	$(COMPILE.l)
$(allsources_obj_l) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.c++
	$(COMPILE.c++)

$(allsources_obj_S) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.S
	$(VERBOSE_COMPILE)
	$(COMPILE.S)
$(allsources_obj_s) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.s
	$(VERBOSE_COMPILE)
	$(COMPILE.s)


#$(allsources_yy:%.yy=%.cc) : %.cc : %.yy

#  $(allsources_yy:%.yy=%.cc) $(allsources_yy:%.yy=%.h) : $(allsources_yy)
# 
# $(allsources_yy:%.yy=%.cc) $(allsources_yy:%.yy=%.h) : %.cc %.h : %.yy
# 	  bison $(firstword $^) --defines --debug -o $@
# 	  mv $@.h $(@:.cc=.h)
# 
$(allsources_l:%.l=%.cc) : %.cc : %.l
	  flex -p -o$@ $(firstword $^)

$(allsources_ll:%.ll=%.cc) : %.cc : %.ll
	  flex -p -o$@ $(firstword $^)

$(allsources_ll:%.ll=$(RELOBJDIR)$/%.$(OBJ_EXT)) : $(RELOBJDIR)$/%.$(OBJ_EXT) : %.cc
	$(COMPILE.c++)

# An error on this line of the form
#     target `fex_parser.yy' doesn't match the target pattern
# means it's really on the line above

cpp-%: FORCE
	$(C++) $* $(tj_CCFLAGS) $(FLAGS_$*) -E

#=============================================================================
################ Hartley _dcl.h files 
# These are the rules for generating _dcl.h files from .c and .cc files
# currently those are the two extensions used in Richards Carmen code.
# I don't expect, or want this to become used in the rest of targetjr,
# so it should not be that limiting.

ifeq ($(strip $(GEN_DCL)),1)

# Set CVARDIR to Utilities if it is otherwise not set
ifeq ($(CVARDIR),)
CVARDIR := Utilities
endif

# keep _dcl.h files upto date
dot_dcl_from_c_files := $(patsubst %.c,.%_dcl.h,$(allsources_c))
dot_dcl_from_cc_files := $(patsubst %.cc,.%_dcl.h,$(allsources_cc))
xall:: $(dot_dcl_from_c_files) $(dot_dcl_from_cc_files)

# make .file_dcl.h depend on .c files  
# each time a .c file is changed the .file_dcl.h is created
# if it is different than file_dcl.h, .file_dcl.h is copied to 
# file_dcl.h, this is done by the gen_decl.pl script
ifneq ($(strip $(dot_dcl_from_c_files)),)
$(dot_dcl_from_c_files) : .%_dcl.h: %.c
	@perl -S $(sys_or_iu_Scripts)/Perl/gen_decl.pl -X $(CVARDIR) -c -N $(source_for_object) -o $@ $(source_for_object) 
endif

ifneq ($(strip $(dot_dcl_from_cc_files)),)
$(dot_dcl_from_cc_files) : .%_dcl.h: %.cc
	@perl -S $(sys_or_iu_Scripts)/Perl/gen_decl.pl -X $(CVARDIR) -N $(source_for_object) -o $@ $(source_for_object) 
endif

# generate _decl.h files if they are not there
# if gen_decl is given -create_only option it will first check
# to see if the file is there and exit if it exists, otherwise it will create
# the file.
pre_deps_c  = $(patsubst %.c,%_dcl.h,$(allsources_c))
pre_deps_cc = $(patsubst %.cc,%_dcl.h,$(allsources_cc))
xall:: $(pre_deps_c) $(pre_deps_cc)

ifneq ($(strip $(pre_deps_c)),)
$(pre_deps_c) : %_dcl.h : %.c
	@perl -S $(sys_or_iu_Scripts)/Perl/gen_decl.pl -create_only -X $(CVARDIR) -c -N $(source_for_object) -o $@ $(source_for_object)
endif

ifneq ($(strip $(pre_deps_cc)),)
$(pre_deps_cc) : %_dcl.h : %.cc
	@perl -S $(sys_or_iu_Scripts)/Perl/gen_decl.pl -create_only -X $(CVARDIR) -N $(source_for_object) -o $@ $(source_for_object)
endif

pre_deps = $(pre_deps_c) $(pre_deps_cc)

endif

#############################################################################
################ DEPENDENCIES ###############################################

# Internal: allsources_deps
# The list of .d files in which dependencies live.
# The .d files for x.cc are $(RELOBJDIR)$/x.cc.d
# This allows us to easily determine the src file for a .d file
ifndef NODEPENDS
allsources_deps := $(allsources:%=$(RELOBJDIR)$/%.d) $(allsources_tmpl:%=$(RELOBJDIR)$/%.d)
endif

# Now-simple macro to infer source file name from .d name.
source_for_@d = $(@:$(RELOBJDIR)$/%.d=%)

# including $(dirstamp) ensures the dir is made.
ifneq "" "$(strip $(allsources_deps))"
-include $(dirstamp) $(allsources_deps)
endif

# Option: NODEPENDS
# Set to 1 to inhibit dependency checking.  If you have never been bitten by
# a non up-to-date dependency before, do not use this flag.
ifndef NODEPENDS

ifeq ($(strip $(QUICK)),1)
# In quick mode, don't remake deps
$(allsources_deps) : $(dirstamp)
	touch $@
else

# Variable: pre_deps
# Additional dependencies for the deps files.  This varible would
# normally be empty causing no trouble, however if you generate _decl.h files
# from .c files this needs to be done before the depends are run, so the var is set

# Must be careful with .d files, as we never want to leave an empty one behind.  If we do, it
# won't get remade.  So make deps to a temp file and move it only if the command has succeeded.
# Notes:
#- Piping through perl would hide the exit code of MAKEDEPEND, so do that afterwards.
#- Depends can legitimately fail when files are removed.
#- Added dependency on .cc so depends are made when .d is not there, and .cc
#  is autogenerated.

# Config: CPP_MAKEDEPEND
# Set to 1 if using the win32bin/depends.pl script

ifdef CPP_MAKEDEPEND

$(allsources_deps) : $(RELOBJDIR)/%.d : IU_LibDependencies $(dirstamp) $(pre_deps) %
	@test -f $(source_for_@d) || echo "Source file $(source_for_@d) not found"
	@$(MKDIR_P) $(@D)
	@echo "-- Making dependencies for $(source_for_@d)"
ifdef SHOW_DEPENDCMD
	$(MAKEDEPEND) $(force_language_macro) $(source_for_@d) $(CPPFLAGS)  -out $@
else
	@$(MAKEDEPEND) $(force_language_macro) $(source_for_@d) $(CPPFLAGS)  -out $@
endif

else


obj_for_@d = $(@:$(suffix $(@:.d=)).d=.$(OBJ_EXT))

$(allsources_deps) : $(RELOBJDIR)/%.d : IU_LibDependencies $(dirstamp) $(pre_deps) %
#	@echo 'Depending: $(MAKEDEPEND) $(source_for_@d) $(CPPFLAGS)'
	@test -f $(source_for_@d) || echo "Source file $(source_for_@d) not found"
	@$(MKDIR_P) $(@D)
	@echo "--- Making dependencies for $(source_for_@d)"
	@$(RM) $@
ifdef IUE_PLUSPLUS
	@$(C++) -V $(source_for_@d) $(CPPFLAGS) -M $@ -o $(obj_for_@d) # -c
else
	$(MAKEDEPEND) $(source_for_@d) $(CPPFLAGS) > $@~
	@perl $(sys_or_iu_Scripts)/Perl/tj-massage-depends.pl -OBJFILE=$(obj_for_@d) -DEPFILE=$@  < $@~ > $@
	@$(RM) $@~
endif

endif # CPP_MAKEDEPEND

endif # QUICK

endif # NODEPENDS


#############################################################################
################ LIBRARY ####################################################
# at one point LIBRARY was LIB, but this conflicted with Microsoft!

ifeq "$(origin LIB)" "file"
xall::
	@echo "		************************************************"
	@echo "		*                                              *"
	@echo "		*              Warning:                        *"
	@echo "		*   Change LIB to LIBRARY in your makefile!    *"
	@echo "		*                                              *"
	@echo "		************************************************"

LIBRARY = $(LIB)
endif

# Variable: LIBRARY
# The name of a library to be made from SOURCES.
ifneq ($(strip $(LIBRARY)),)

# Config: MAKE_SHARED
# Should be defined in build-$(BUILD).mk if BUILD implies
# shared libraries and dynamically linked executables.

ifndef MAKE_SHARED
###############################
# Archive/Static library target
libfile := $(LIBDEST)$/$(LIB_PREFIX)$(strip $(LIBRARY)).$(AR_EXT)

xall:: $(libfile)

# Config: MAKE_ARLIB MAKE_ARLIB_CMD
# Set MAKE_ARLIB if what? FIXME
# Otherwise use MAKE_ARLIB_CMD

ifeq (,$(MAKE_ARLIB))
$(libfile):  IU_LibDependencies $(LIBDEST)$/stamp $(SOURCES_obj)
	@echo ""
	@echo "-- Creating Static Library $@"
	@$(RM) $@
	$(MAKE_ARLIB_CMD) $@ $(SOURCES_obj)
	-$(RM) $(CLEAN_FILES)

else
$(libfile): IU_LibDependencies $(LIBDEST)$/stamp $(SOURCES_obj)
	@echo ""
	@echo "-- Creating Static Library $@"
	$(MAKE_ARLIB) $(SOURCES_obj)
	-$(RM) $(CLEAN_FILES)

endif # MAKE_ARLIB
else # MAKE_SHARED

###########################
# Shared library/DLL target
# some os's (freebsd ) require a version, so default to 1.0
ifeq (,$(VERSION))
VERSION = 1.0
endif

shlibnameversion := lib$(LIBRARY).$(LIB_EXT).$(VERSION)
shlibname := $(LIB_PREFIX)$(LIBRARY).$(LIB_EXT)
shlibfile := $(LIBDEST)$/$(shlibname)
shlibversionfile := $(LIBDEST)$/$(shlibnameversion)
set_soname := $(soname_opt)

ifeq ($(strip $(USE_LIB_VERSION)),1)
xall::  $(shlibversionfile)
endif # USE_LIB_VERSION

xall:: $(shlibfile) fred

##--------------------------------------------------------------------------
## Win32 needs to make the library.{def,exp} files.
ifeq ($(COMPILER),VC90)
DEFINES += -DBUILDING_$(LIBRARY)_DLL
deffile := $(RELOBJDIR)$/$(LIBRARY).def
xall:: $(deffile)
$(deffile): $(LIBDEST)$/stamp $(SOURCES_obj)
ifdef SHOW_MODULEDEF_CMD
	@echo ""
	@echo "-- [Win32] Creating Module Definition File for Shared Library $(LIBRARY)"
	perl -S makedef.pl $@ $(shlibname) $(RELOBJDIR) $(SOURCES_obj) 
	@weedbadrefs $@
	@echo "Finished creating definition file"
else
	@echo ""
	@echo "-- [Win32] Creating Module Definition File for Shared Library $(LIBRARY)"
	@perl -S makedef.pl $@ $(shlibname) $(RELOBJDIR) $(SOURCES_obj)
	@weedbadrefs $@
	@echo "Finished creating definition file"
	@date
endif

#  -- This is to get some directories to compile in .NET
#     For some reason it does not like the -def option and causes
#     undefined symbols.

ifeq ($(strip $(DONT_GENERATE_DEF_FILE)),1)
opt_deffile = 
#opt_deffile = -def:$(deffile)
else
opt_deffile = 
#opt_deffile = -def:$(deffile)
endif

# This is to handle the dlls can't mutually depend on each other
# you have to use lib for a first pass, then use link to build the lib
expfile := $(LIBDEST)$/$(LIBRARY).exp
dos_expfile := $(shell echo $(expfile) |sed -e "s@//\([A-Za-z]\)@\1:@g")
dos_libfile := $(dos_expfile:.exp=.lib)
xall:: $(expfile)
$(expfile): $(deffile)
	@echo ""
	@echo "-- [Win32] Creating Export File and Import Library for $(LIBRARY)"
	lib $(VCC_LIBPATH:%=-LIBPATH:%) $(VCC_LIB_FLAGS) -nologo -machine:IX86 $(opt_deffile) -out:$(dos_libfile) $(SOURCES_obj)
	@echo "Finished creating Export File and Import Library"
	@date
endif # VC90

##----------------------------------------------------------------------------
# Linking with gcc

ifeq ($(COMPILER),gcc)

libfile := $(LIBDEST)$/$(LIBRARY).lib
dos_libfile := $(shell echo $(libfile) |sed -e "s@//\([A-Za-z]\)@\1:@g")
xall:: $(libfile)
$(libfile): $(LIBDEST)$/stamp $(SOURCES_obj)
	@echo ""
	@echo "--  Creating archive $(LIBRARY)"
	ar rcs $(dos_libfile) $(VCC_LIBPATH:%=-LIBPATH:%) $(VCC_LIB_FLAGS) $(SOURCES_obj)
	@echo "Finished creating archive"
	@date
endif # gcc

##----------------------------------------------------------------------------

safelink:
	$(MAKE) LINK_INTO_TMP=1

# See params.mk, link shared lib into /tmp and then mv to final
# destination.
ifeq "" "$(strip $(LINK_INTO_TMP))"
  shlib_op = $(LINK_OUT_OPTION)
  shlib_mv = @$(EMPTY_COMMAND)
else
  ifeq ($(OS),$(filter $(OS),win32 x64))
    shlib_tmpname = tmplink.$(subst /,_,$(OBJDIR)).$(@F)
  else
    shlib_tmpname = /tmp/tmplink.$(subst /,_,$(OBJDIR)).$(@F)
  endif
  shlib_op = $(foreach @,$(shlib_tmpname),$(OUTPUT_OPTION))
  # sometimes this business gets funny dates onto the libfile
  # (SGI->/tmp, mv /tmp to Solaris NFS2 partition, date is 1hr ahead)
  # The touch is required to fix it.
  shlib_mv = mv $(shlib_tmpname) $@ && touch $@
endif # LINK_INTO_TMP

#--------------------------------------------------------------------
# The MAKE_SHLIB line was commented back in my RIH on March 8, 2008
# so that Matlab dlls will be made properly.  This may break ordinary builds.
#--------------------------------------------------------------------
# Commented back out on Aug 15, 2008.  Use Visual Studio project files
# to compile Matlab dlls -- RIH.  It didn't work anyway.
#--------------------------------------------------------------------

$(shlibfile): IU_LibDependencies $(LIBDEST)$/stamp $(SOURCES_obj) $(expfile) $(deffile) 
#	@echo ""
#	@echo "-- Linking Shared Library $(LIBRARY)"
#	@echo "-- Making $@"
#	$(RM) so_locations
#	$(MAKE_SHLIB) $(SOURCES_obj) $(shlib_op) $(set_soname) $(tj_CGFLAGS) $(LDFLAGS) $(LINK_SHARED_LIB_FLAGS) $(sh_extra_libs) $(opt_deffile)
#	$(shlib_mv)
#	-$(RM) $(CLEAN_FILES)
	@echo ""
	@echo "-- Updating iu_library_table file - $(LIBRARY)"
	$(sys_or_iu_Scripts)/Shell/addlib.sh $(LIBRARY) `tjwd`

# Config: USE_LIB_VERSION
# This is for OS's like sunos that need a version number on the 
# shared library for things to work.  The USE_LIB_VERSION flag
# is set in the os-compiler file in the config directory.
# This is wrapped with an ifdef MAKE_SHARED so that it is only done
# with the shared version
ifeq ($(strip $(USE_LIB_VERSION)),1)
$(shlibversionfile):  $(shlibfile)
	$(RM) $(shlibversionfile)
	$(LN) $(shlibfile) $(shlibversionfile)
endif

endif # MAKE_SHARED

endif # LIBRARY
################ END LIBRARY ################################################
#############################################################################

#############################################################################
################ STATIC_LIBRARY #############################################

# Variable: STATIC_LIBRARY
# The name of a static library to be made from SOURCES.
ifneq ($(strip $(STATIC_LIBRARY)),)

libfile := $(LIBDEST)$/$(LIB_PREFIX)$(STATIC_LIBRARY).$(AR_EXT)

xall:: $(libfile)

# Config: MAKE_ARLIB MAKE_ARLIB_CMD
# Set MAKE_ARLIB if what? FIXME
# Otherwise use MAKE_ARLIB_CMD
ifeq (,$(MAKE_ARLIB))
$(libfile): IU_LibDependencies $(LIBDEST)$/stamp $(SOURCES_obj)
	@echo ""
	@echo "-- Creating a Static Library $@"
	@$(RM) $@
	$(STRIP) $@
	$(MAKE_ARLIB_CMD) $@ $(SOURCES_obj)
	-$(RM) $(CLEAN_FILES)

else
$(libfile): IU_LibDependencies $(LIBDEST)$/stamp $(SOURCES_obj)
	@echo ""
	@echo "-- Creating Static Library $@"
	$(MAKE_ARLIB) $(SOURCES_obj)
	$(STRIP) $@
	-$(RM) $(CLEAN_FILES)

endif # MAKE_ARLIB
endif # STATIC_LIBRARY
################ END STATIC_LIBRARY #########################################
#############################################################################


#############################################################################
################################# PROGRAM  ##################################

# Internal: LIB_DEPENDENCIES
# The full pathnames of all static libraries being linked, generated from
# LIBDIRS and LDLIBS.  For example, an LDLIBS of
#  -lmylib -lTJCOOL -lm
# might produce:
#  /homes/me/lib/libmylib.a /usr/target/COOL/lib.CPU/libTJCOOL.a /usr/lib/libm.a
# This list can then be used as a dependency of any executable.
ifneq ($(OS),$(filter $(OS),win32 x64))
LIB_DEPENDENCIES = $(foreach lib,$(LDLIBS:-l%=%),$(firstword $(wildcard $(LIBDIRS:%=%$/lib$(lib).$(AR_EXT))))) $(VCC_LIB_FLAGS)
else
LIB_DEPENDENCIES = $(foreach lib,$(LDLIBS:-l%=%), $(wildcard $(LIBDIRS:%=%$/$(lib))))
endif


# Variable: PROGRAM
# A single executable program made from $(SOURCES).
ifneq ($(strip $(PROGRAM)),)


progfile := $(RELOBJDIR)$/$(PROGRAM)

xall:: $(progfile)

ifdef MAKE_SHARED
## Assume no lib deps for shared builds
statlibs :=
else
statlibs := $(LIB_DEPENDENCIES)
endif

ifneq "" "$(strip $(LINK_INTO_TMP))"
 ifeq ($(OS),$(filter $(OS),win32 x64))
   link_tmpname = tmplink.$(@F).$(subst /,_,$(OBJDIR))
 else
   link_tmpname = /tmp/tmplink.$(@F).$(subst /,_,$(OBJDIR))
 endif
 link_op = $(foreach @,$(link_tmpname),$(LINK_OUT_OPTION))
 link_mv = mv $(link_tmpname) $@
else
 link_op = $(LINK_OUT_OPTION)
 link_mv = @$(EMPTY_COMMAND)
endif # LINK_INTO_TMP

##  To use purify, see the definition of MAKE_EXE
$(progfile): $(SOURCES_obj) $(SOURCES_res) $(OBJECTS) $(statlibs)
	@echo ""
	@echo "-- Linking Program $(PROGRAM)"
	$(MAKE_EXE) $(link_op) $(SOURCES_obj) $(SOURCES_res) $(OBJECTS)  $(BUGFIX_OBJECTS)  $(tj_LDFLAGS)  $(LDPATH)  $(LDLIBS) $(LDOPTS)
	$(link_mv)
	-$(RM) $(CLEAN_FILES)

endif # PROGRAM
############################### END PROGRAM  ################################
#############################################################################

################# 
# Variable: MATLAB_MEX_SOURCES
ifneq ($(strip $(MATLAB_MEX_SOURCES)),)

MATLAB_EXES += $(patsubst %.C,%,$(basename $(MATLAB_MEX_SOURCES)))

# Commented out for now - we make the dlls from project files
# RIH - August 15, 2008
#

$(MATLAB_EXES):   makefile
	@echo ""
	@echo -- Compile mexFunction $@
#	$(MAKE) MEXBUILD=1 SOURCES=$@.C LIBRARY=$@ $@.dll
#	mv $@.exp $@.lib $@.pdb stamp $(RELOBJDIR)

xall:: $(MATLAB_EXES)

endif # MATLAB_MEX_SOURCES

################# 
# Variable: MINI_PROG_SOURCES
# Single source files to be compiled and linked against LDLIBS.
ifneq ($(strip $(MINI_PROG_SOURCES)),)

# Internal: MINI_PROG_EXES
# The list of executables generated from MINI_PROG_SOURCES
MINI_PROG_EXES += $(patsubst %,$(RELOBJDIR)$/%$(EXESUFFIX),$(basename $(MINI_PROG_SOURCES)))

xall:: $(MINI_PROG_EXES)

ifdef USE_TMPLCLOSURE

IUESCANLINKFORINSTANCESCMD = perl -S -x _iuescanlinkforinstances
# Add the parameters  -v -T=progs for debugging output for iueinstantiateclosure
IUEINSTANTIATECLOSURECMD = perl -S -w -x iueinstantiateclosure 

define compile-tmpl-insts
	if [ -d Templates -a \! -z "$COMPILING_TEMPLATES" ]; then \
	  if [ \! -z "`ls Templates`" ]; then \
	    echo ""; \
	    echo "-- Compiling Template Instances"; \
	    $(MAKE) COMPILING_TEMPLATES=1; \
	  fi; \
	fi
endef

#
# Make sure that exes depend on the template libraries
# and also link in individual Templates object files (because this is for USE_TMPLCLOSURE).
# also make sure that exes depend on the template libraries

$(MINI_PROG_EXES): %$(EXESUFFIX): %.$(OBJ_EXT) $(allsources_obj_tmpl) $(LIB_DEPENDENCIES) $(libfile) makefile
	@if [ -z "$COMPILING_TEMPLATES" ]; then $(compile-tmpl-insts); fi
	@echo ""
	@echo "-- Linking Mini Program $@"
	@linkout=/tmp/linkout$$$$; \
	instances=/tmp/instances$$$$; \
	lstatfile=/tmp/linkstatus$$$$; \
	if [ -f $$lstatfile ]; then echo rm $$lstatfile; rm $$lstatfile; fi; \
	echo "--   Undefined symbols may indicate missing template instances."; \
	echo "--   Template instantiator will try to resolve them."; \
	echo $(MAKE_EXE) $(LINK_OUT_OPTION) $(firstword $^) \
	$(OBJECTS_$(basename $(@F))) $(allsources_obj_tmpl) \
	$(BUGFIX_OBJECTS) \
	$(tj_LDFLAGS) $(LDPATH) $(LDLIBS) \| tee $$linkout; \
	($(MAKE_EXE) $(LINK_OUT_OPTION) $(firstword $^) \
	$(OBJECTS_$(basename $(@F))) $(allsources_obj_tmpl) \
	$(BUGFIX_OBJECTS) \
	$(tj_LDFLAGS) $(LDPATH) $(LDLIBS); lstat=$$?; if [ $$lstat != 0 ]; then echo $$lstat > $$lstatfile; fi) 2>&1 | tee $$linkout; \
	estat=0; \
	if [ -f $$lstatfile ]; then \
	    echo " "; \
	    echo "-- UNDEFINED SYMBOLS MAY INDICATE MISSING TEMPLATE INSTANCES."; \
	    echo "   TRYING TO RESOLVE THEM..."; \
	    echo "-- SCANNING LINKER OUTPUT FOR TEMPLATE INSTANCES"; \
	    echo " "; \
	    echo $(IUESCANLINKFORINSTANCESCMD) -c $(firstword $(C++)) \< $$linkout \> $$instances; \
	    $(IUESCANLINKFORINSTANCESCMD) -c $(firstword $(C++)) < $$linkout > $$instances; \
	    estat=$$?; \
	    if [ $$estat != 0 ]; then \
	      echo "$(IUEINSTANTIATECLOSURECMD)" -f $$instances -L Templates; \
	      $(IUEINSTANTIATECLOSURECMD) -f $$instances -L Templates; \
	      estat=$$?; \
	    fi; \
	    if [ $$estat = 2 ]; then \
		echo " "; \
		echo "-- RERUNNING MAKE TO BRING IN NEW TEMPLATE INSTANCES"; \
		echo " "; \
		$(compile-tmpl-insts); \
		estat=$$?; \
	    else \
		echo " "; \
		echo "-- NO TEMPLATE INSTANCES CREATED.  LINK PROBABLY FAILED FOR OTHER REASONS."; \
		echo " "; \
		exit 1; \
	    fi; \
	fi; \
	$(RM) $$linkout $$instances $$lstatfile; \
	exit $$estat;

else
# make sure that exes depend on the template libraries
# but do *not* link in individual object files that happen to be here.
$(MINI_PROG_EXES): %$(EXESUFFIX): %.$(OBJ_EXT) $(LIB_DEPENDENCIES) $(libfile) makefile
	@echo ""
	@echo -- Linking program $@ 
	$(MAKE_EXE) $(LINK_OUT_OPTION)\
	$(tj_CGFLAGS) $(firstword $^) \
	$(OBJECTS_$(basename $(@F))) \
	$(BUGFIX_OBJECTS) \
	$(tj_LDFLAGS) \
	$(LDPATH) \
	$(LDLIBS) \
	$(LINKING_PROGRAM_LIBS)

endif # USE_TMPLCLOSURE

endif # MINI_PROG_SOURCES

#==============================================================================

### Generation of project files

VCPROJDIR := VCCProjects-$(COMPILER)

MOREDEFINES += define(MACRO_MatlabRoot,$(MATLAB_ROOT))

ProjectMacros   := $(sys_or_iu_Scripts)/Shell/ProjectTemplateMacros.m4
ProjectTemplate_lib := $(sys_or_iu_Scripts)/Shell/ProjectTemplate-LIB-$(COMPILER).m4
ProjectTemplate_dll := $(sys_or_iu_Scripts)/Shell/ProjectTemplate-DLL-$(COMPILER).m4

TSOURCES := $(wildcard Templates/*.C) $(wildcard Templates/*.cc)

pfSOURCES := $(foreach lib, $(SOURCES) $(TSOURCES), sourcefile($(lib)))

pfUSES := $(foreach inc, $(SG_INCDIRS), includedir($(inc)))
#strippedINCLUDES := $(patsubst -I%,%, $(include_flags))
#pfUSES := $(foreach dir, $(strippedINCLUDES), includedir($(dir)))

strippedIULIBS := $(patsubst -l%,%, $(IULIBS))
strippedLDLIBS := $(patsubst -l%,%, $(LDLIBS))
strippedLDLIBS := $(patsubst %.lib,%, $(strippedLDLIBS))
pfIULIBS := $(foreach lib, $(strippedIULIBS), libdir($(lib)))
pfLDLIBS := $(foreach lib, $(strippedLDLIBS), libdir($(lib)))

strippedDEFINES := $(patsubst -D%,%, $(DEFINES))
pfDEFINES := $(foreach lib, $(strippedDEFINES), adddefine($(lib)))

# ----------------------------------------------------
# If using Visual Studio, then make project files
ifeq ($(COMPILER),$(filter $(COMPILER), VC90 VC100 VC140 VC150))

projfile_lib := $(VCPROJDIR)/$(LIBRARY)-LIB.$(VCPROJSUFFIX)
$(projfile_lib) : makefile 
	@echo ""
	@echo "-- Updating LIB Project File"
	@$(MKDIR_P) $(VCPROJDIR)
	@echo "$(MOREDEFINES) $(pfDEFINES) $(pfUSES) $(pfIULIBS) $(pfSOURCES) projname($(LIBRARY)) frescohome($(FRESCOHOME)) iupackagehome($(IUPACKAGEHOME))" | m4 $(ProjectMacros) - $(ProjectTemplate_lib) > $(projfile_lib)

#projfile_dll := $(VCPROJDIR)/$(LIBRARY)-DLL.$(VCPROJSUFFIX)
#$(projfile_dll) : makefile 
#	@echo ""
#	@echo "-- Updating DLL Project File"
#	@echo "$(MOREDEFINES) $(pfDEFINES) $(pfUSES) $(pfIULIBS) $(pfSOURCES) projname($(LIBRARY)) frescohome($(FRESCOHOME)) iupackagehome($(IUPACKAGEHOME))" | m4 $(ProjectMacros) - $(ProjectTemplate_dll) > $(projfile_dll)

endif
# ----------------------------------------------------

# Make if LIBRARY is defined
ifeq ($(LIBRARY),)
xall::
else
# xall:: $(projfile_lib) $(projfile_dll)
xall:: $(projfile_lib)
endif

# Produce the proj files if LIBRARY is defined
ifeq ($(LIBRARY),)
vccfiles::
else
vccfiles:: $(projfile_lib)
endif

allvccfiles::
ifdef SUBDIRS
	@$(MAKE) NODEPENDS=1 subdirs-allvccfiles
else
	@$(MAKE) vccfiles
endif


#################
# Variable: MINI_SO_TESTS
# Mini shared libraries for TestDrivering.
# Setting this also causes TestDriver.C to be copied from
# the config directory and built.

# The shared libs will be built by MINI_SO_SOURCES, this bit adds the rules for
# running them.

ifneq ($(strip $(MINI_SO_TESTS)),)
xall:: tests

ifdef NOTEST
tests:
	@echo "Not running MINI_SO_TESTS"
else

TESTDRIVER_BASENAME := TestDriver$(EXESUFFIX)
TESTDRIVER := $(RELOBJDIR)/$(TESTDRIVER_BASENAME)

TESTDRIVER_SRC := $(configdir)/TestDriver.C

TestDriver.C: $(TESTDRIVER_SRC)
	$(RM) $@ && cp $^ $@

test_targets := $(MINI_SO_TESTS:%.C=$(RELOBJDIR)/%.out)

tests: $(test_targets)

$(RELOBJDIR)/%.out: $(TESTDRIVER) $(RELOBJDIR)/$(LIB_PREFIX)%.$(LIB_EXT) FORCE
	@echo Running test $(LIB_PREFIX)
	cd $(RELOBJDIR) && ./$(TESTDRIVER_BASENAME) $(@F:%.out=%)
	@tail -2 $@

runtest-%: $(RELOBJDIR)/$(@:runtest-%=%).out

endif # NOTEST
endif # MINI_SO_TESTS

#################
# Variable: MINI_SO_SOURCES
# Mini shared libraries for tjrunning, made only when MAKE_SHARED is set
ifneq ($(strip $(MINI_SO_SOURCES)),)
ifdef MAKE_SHARED
MINI_SO_EXES := $(patsubst %,$(RELOBJDIR)$/$(LIB_PREFIX)%.$(LIB_EXT),$(basename $(MINI_SO_SOURCES)))

xall:: $(MINI_SO_EXES)

ifeq ($(OS),$(filter $(OS),win32 x64))
ifdef EXPORT_MAIN
WINSTUFF2 = -link -export:main $(LDLIBS) $(LDPATH)
else
WINSTUFF = -link -export:$(basename $@) $(LDLIBS) $(LDPATH)
WINSTUFF2 = $(subst $(RELOBJDIR)$/,,$(WINSTUFF))
endif
endif

$(MINI_SO_EXES): $(RELOBJDIR)$/$(LIB_PREFIX)%$(LIB_EXT): $(RELOBJDIR)$/%$(OBJ_EXT) $(allsources_obj_tmpl)
	$(MAKE_SHLIB) $(LINKER_OUT_OPTION) $(tj_CGFLAGS) $(firstword $^) $(allsources_obj_tmpl) $(OBJECTS_$(basename $@)) $(WINSTUFF2)

endif # NOSHARED
endif # MINI_SO_SOURCES

#############################################################################
############################# SUBDIRECTORIES  ###############################

#################
# Variable: SUBDIRS
# Set to a list of subdirectories into which make should recurse.
# Note that missing directories are silently skipped (this is a feature).
ifneq ($(strip $(SUBDIRS)),)

actual_subdirs = $(wildcard $(SUBDIRS))

xall:: subdirs

subdirs:
	$(MAKE) BUILD=$(BUILD) $(actual_subdirs:%=%/recurse-subdirs) subdir_target=

subdirs-%:
	@ $(MAKE) BUILD=$(BUILD) $(actual_subdirs:%=%/recurse-subdirs) subdir_target=$(@:subdirs-%=%)

# NOTE: tj-buildreport.pl uses the string "Running make in " to make its report,
# so do not remove this output, if you change the string, also make the change
# in Scripts/Perl/tj-buildreport.pl

$(actual_subdirs:%=%/recurse-subdirs): FORCE
	@if test -f $(@D)/makefile ; then ( \
	    cd $(@D) && \
	    echo ""  &&	\
	    echo "--------------------------------------------------------" && \
	    echo "-- Running make in `pwd`" && \
	    $(MAKE) --no-print BUILD=$(BUILD) $(subdir_target)\
	) ; else ( \
            echo "** No makefile in `pwd`/$(@D)" \
        ) ;  fi

else
subdirs:
	@echo No SUBDIRS set in makefile

subdirs-%:
	@$(EMPTY_COMMAND)
endif



#############################################################################
##############################  DOCUMENTATION  ##############################

################# MANPAGES
# Internal: MANDIR
# Destination for autogenerated manual pages.
MANDIR := $(IUPACKAGEHOME)$/man$/man3
MANSUF := 3
GENMAN := gendoc -t
gendocloc = $(foreach file,Scripts/Perl/gendoc,$(sys_or_iu_macro))

massage_windex := 'if (/^Cool(\w+)\s+\w+\s+\((\w+)\)/) { print "rm -f Cool$$1 && $(LN) $$1.$$2 Cool$$1.$$2 \n"; }'

# Variable: MANPAGE_SOURCES
# If set, the definitive list of source files from which manual pages are to
# be generated.  If not, it is built from SOURCES by adding .h to the basenames.

ifndef MANPAGE_SOURCES
 # use wildcard function to filter out nonexistent .h files
 MANPAGE_SOURCES := $(wildcard $(addsuffix .h,$(srcbase)))
endif

make-windex:
#	catman-do $(MANDIR)/..

# subdir_target defined means we are being called by a higher make.
# This means that windex is made just once, at the top level.
ifndef subdir_target
manpage_need_windex := make-windex
endif

ifeq "" "$(strip $(MANPAGE_SOURCES))"
# Just go into subdirs.
manpages : subdirs-manpages  $(manpage_need_windex)
else
# Make manpages here, then go into subdirs

# compute actual manpage filenames
manpage_sources_base = $(basename $(MANPAGE_SOURCES))

MANPAGES := $(manpage_sources_base:%=$(MANDIR)$/%.$(MANSUF))

ifeq ($(OS),$(filter $(OS),win32 x64))
mandepend = perl -S tj-mandepend -I.. \
	"$(MANDIR)" "$(MANPREFIX)" "$(MANSUF)" "$(MANPAGE_SOURCES)" > $(RELOBJDIR)/mandeps
else
mandepend = perl $(sys_or_iu_Scripts)/Perl/tj-mandepend -I.. \
	"$(MANDIR)" "$(MANPREFIX)" "$(MANSUF)" "$(MANPAGE_SOURCES)" > $(RELOBJDIR)/mandeps
endif

$(RELOBJDIR)/mandeps:
	[ -d $(RELOBJDIR) ] || $(MKDIR_P) $(RELOBJDIR)
	@$(mandepend)

-include $(RELOBJDIR)/mandeps

manpages: $(MANPAGES) subdirs-manpages $(manpage_need_windex)

manpage_source_from_dest = \
	$(wildcard $(@:$(MANDIR)$/%.$(MANSUF)=%.h)  $(@:$(MANDIR)$/%.$(MANSUF)=%)  )

$(MANPAGES) : $(MANDIR)$/%.$(MANSUF) : $(gendocloc)
	@$(MKDIR_P) $(@D)
	$(GENMAN) -I.. $(manpage_source_from_dest) > $@

# We don't want to generate these dependencies every time as it will
# drive people scatty

mandepend: FORCE
	$(mandepend)

endif # MANPAGE_SOURCES


################# HTML
# Internal: HTMLDIR
# Subdirectory into which to place HTML indices.
HTMLDIR := HTML
GENHTML := $(IUPACKAGEHOME)$/Scripts$/Perl$/gendoc -h

# use wildcard function to filter out nonexistent .h files
HTMLHEADERS := $(wildcard $(MANPAGE_SOURCES))
HTMLPAGES := $(HTMLHEADERS:%.h=$(HTMLDIR)$/%.html)

htmlpages: htmlfiles

# Variable: HTMLINDEX
# Define this variable if you have a handwritten html index
# page.  If unset, one will be generated automatically
# from MANPAGE_SOURCES.
ifndef HTMLINDEX
HTMLINDEX := $(HTMLDIR)$/classes.html
htmlindex: FORCE
	@$(MKDIR_P) $(HTMLDIR)
	tj-make-htmlindex "$(MANPAGE_SOURCES)" "$(SUBDIRS)" > $(HTMLINDEX)
else
htmlindex:
endif # HTMLINDEX

htmlfiles: $(HTMLPAGES) subdirs-htmlfiles htmlindex

# Add this rule so peeps can do make HTML/Fred.html
$(HTMLDIR)$/%.html: %.h
	@$(MKDIR_P) $(HTMLDIR)
	gendoc -I.. -h -colors 'BGCOLOR="#FFFFFF" TEXT="#000000" LINK="#FF0000" ALINK="#0000FF"' $(@:$(HTMLDIR)$/%.html=%.h) > $@


################## MISC

# Handy FORCE target.
FORCE:

.PHONY: FORCE all xall makefiles Makefiles depend

# Handy target to print a variable's value e.g.
# make echovar-SOURCES
echovar-%:
	@echo $($(@:echovar-%=%))

# Need seperate showvar because echovar can then not be used in scripts...
showvar-%:
	@echo $(@:showvar-%=%) = \"$($(@:showvar-%=%))\"

originvar-%:
	@echo $($*)
	@echo origin: $(origin $*)

makefiles:
	for i in $(SUBDIRS) ; do isnake $$i$/Imakefile > $$i$/makefile ; done

makefiles.gnu:
	for i in $(SUBDIRS) ; do isnake $$i$/Imakefile > $$i$/makefile.gnu ; done

# Makefiles targets gone for good.  Hurrah.
Makefiles: subdirs-Makefiles
	@echo "Using gmake: no need to make makefiles"

Makefiles.%: subdirs-Makefiles.%
	@echo "Using gmake: no need to make makefiles"

# Brutal depend target -- may end up remaking the lot twice.
depend:
	-@$(RM) $(RELOBJDIR)$/*.d
	$(MAKE) FORCE

depend.%:
	$(MAKE) BUILD=$(@:depend.%=%) depend

realclean::
	-$(RM) IU_LibDependencies
	-$(RM) -r $(RELOBJDIR)
ifdef SUBDIRS
	-$(MAKE) NODEPENDS=1 subdirs-realclean
	-$(RM) -r $(RELOBJDIR)
endif

clean::
	-$(RM) $(RELOBJDIR)$/*.d $(CLEAN_FILES) $(RELOBJDIR)$/*.$(OBJ_EXT)
	-$(RM) $(RELOBJDIR)$/Templates/*.d $(RELOBJDIR)$/Templates/*.$(OBJ_EXT)
ifdef RES_EXT
	-$(RM) $(RELOBJDIR)$/*.$(RES_EXT)
endif
ifdef SUBDIRS
	$(MAKE) NODEPENDS=1 subdirs-clean
endif

clean_obj_only::  subdirs-clean_obj_only
	-$(RM) $(RELOBJDIR)$/*.$(OBJ_EXT)
	-$(RM) $(RELOBJDIR)$/Templates/*.$(OBJ_EXT)
ifdef RES_EXT
	-$(RM) $(RELOBJDIR)$/*.$(RES_EXT)
endif

clean_dep_only::  subdirs-clean_dep_only
	-$(RM) $(RELOBJDIR)$/*.d
	-$(RM) $(RELOBJDIR)$/Templates/*.d

clean_prog:: subdirs-clean_prog

ifneq ($(strip $(PROGRAM)),)
clean_prog::
	$(RM) $(progfile)
endif
ifneq ($(strip $(MINI_SO_EXES)),)
clean_prog::
	$(RM) $(MINI_SO_EXES)
endif

ifneq ($(strip $(MINI_PROG_EXES)),)
clean_prog::
	$(RM) $(MINI_PROG_EXES)
endif

#======================================================================
#  Find-base clean targets (faster but allows no control by submakes)
#======================================================================
ifeq ($(OS),$(filter $(OS),win32 x64))
# on the PC, rm is under cygwin, not /bin
RM_CMD = rm
else
RM_CMD = /bin/rm
endif
#----------------------------------------------------------------------
# get rid of everything generated for this architecture.
#----------------------------------------------------------------------
fastfullclean:
	find . -name $(notdir $(OBJDIR)) -type d -prune -print | xargs $(RM_CMD) -rf
	find . -name $(notdir $(LIBDEST)) -type d -prune -print | xargs $(RM_CMD) -rf


#----------------------------------------------------------------------
# get rid of everything generated for this architecture and template instances
#----------------------------------------------------------------------
fastfullcleant:
	find . -name $(notdir $(OBJDIR)) -type d -prune -print | xargs $(RM_CMD) -rf
	find . -name $(notdir $(LIBDEST)) -type d -prune -print | xargs $(RM_CMD) -rf
	find . -name Templates -prune -print | xargs $(RM_CMD) -rf

#----------------------------------------------------------------------
# keep libraries and executables
#----------------------------------------------------------------------
fastclean:
	@dirs=`find . \( -name $(notdir $(OBJDIR)) -o -name $(notdir $(LIBDEST)) \) -prune -print`; \
	for d in $$dirs; do \
	  echo cleaning $$d; \
	  rm -rf $$d/*.o $$d/core $$d/*.d; \
	done

#----------------------------------------------------------------------
# keep libraries, executables, and depends
#----------------------------------------------------------------------
fastclean-keepdepends:
	@dirs=`find . \( -name $(notdir $(OBJDIR)) -o -name lib.$(OBJDIR) \) -prune -print`; \
	for d in $$dirs; do \
	  echo cleaning $$d; \
	  rm -rf $$d/*.o $$d/core; \
	done


help:
	@echo "Available targets are :"
	@echo "   '<blank>'          : Same as make all"
	@echo "   all                : Same as make all.shared"
	@echo "   all.shared         : Recompile as necessary -- dynamic libraries gcc/g++"
	@echo "   all.noshared       : Recompile as necessary -- static libraries gcc/g++"
	@echo "   native.shared      : Recompile as necessary -- dynamic libraries native"
	@echo "   native.noshared    : Recompile as necessary -- static libraries native"
	@echo "   clean              : Remove all binaries and dependencies"
	@echo "   clean_obj_only     : Removes all .o files "
	@echo "   echovar-VAR        : Print value of VAR in the makefile "


# include this 
-include $(configdir)/rules-$(OS)-$(COMPILER).mk

#--------------------------------------------------------------
#  Some stuff for getting linking information - not guaranteed on all platforms.
#  Written by R. Hartley (hartley@crd.ge.com).
#--------------------------------------------------------------
RHLINKINFO = $(IUPACKAGEHOME)/RHUtilities/interpreter/linkinfo
link.out :
	@echo ""
	@echo Link report ...
	$(RHLINKINFO)/linksummary $(link_op) $(SOURCES_obj) $(SOURCES_res) $(OBJECTS)  $(BUGFIX_OBJECTS)  $(tj_LDFLAGS)  $(LDPATH)  $(LDLIBS) $(LDOPTS) > link.out

rawlink.out :
	@echo ""
	@echo Link report ...
	$(RHLINKINFO)/rawlinksummary $(link_op) $(SOURCES_obj) $(SOURCES_res) $(OBJECTS)  $(BUGFIX_OBJECTS)  $(tj_LDFLAGS)  $(LDPATH)  $(LDLIBS) $(LDOPTS) > rawlink.out

rawestlink.out :
	@echo ""
	@echo Link report ...
	$(RHLINKINFO)/rawestlinksummary $(link_op) $(SOURCES_obj) $(SOURCES_res) $(OBJECTS)  $(BUGFIX_OBJECTS)  $(tj_LDFLAGS)  $(LDPATH)  $(LDLIBS) $(LDOPTS) > rawestlink.out


### Local Variables: ###
### mode:font-lock ###
### End: ###

