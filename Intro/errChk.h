
#ifndef GPU_ERR_CHK_H
#define GPU_ERR_CHK_H

// Error checking stuff
#define gpuErrChk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
#define gpuErrChk_Device(ans) { gpuDeviceAssert((ans), __FILE__, __LINE__); }
#define malloc_chk(size) mallocAssert((size), __FILE__, __LINE__)

inline __host__   void gpuAssert(
      cudaError_t code, const char *file, int line, bool abort=true)
   {
   if (code != cudaSuccess) 
      {
      printf("gpuAssert: %s %s %d\n", 
            cudaGetErrorString(code), file, line);
      fflush(stdout);

      if (abort) exit(code);
      }
   }

inline void *mallocAssert(
      size_t size, const char *file, int line)
   {
   // Check that ptr is not NULL
   void *ptr = malloc(size);
   if (ptr == NULL)
      {
      printf("malloc requested %ld bytes: returned NULL: %s %d\n", 
            size, file, line);
      fflush(stdout);

      exit (EXIT_FAILURE);
      }

   // If success, return ptr
   return ptr;
   }

inline __device__   void gpuDeviceAssert(
      cudaError_t code, const char *file, int line, bool abort=true)
   {
   if (code != cudaSuccess) 
      {
      printf("gpuDeviceAssert: %s %s %d\n", 
            cudaGetErrorString(code), file, line);
      // if (abort) return;
      }
   }

// Define some cuda calls with checking
#define cudaMalloc_chk(addr, nbytes) gpuErrChk(cudaMalloc((addr), (nbytes)))

#define cudaMallocManaged_chk(addr, nbytes) gpuErrChk(cudaMallocManaged((addr), (nbytes)))

#define cudaMemcpy_chk(dest, src, nbytes, type) \
      gpuErrChk(cudaMemcpy((dest), (src), (nbytes), (type)))

#define cudaSetDevice_chk(device) gpuErrChk(cudaSetDevice (device))

#define cudaGetDevice_chk(device) gpuErrChk(cudaGetDevice (device))

#define cudaGetDeviceCount_chk(device) gpuErrChk(cudaGetDeviceCount (device))

#define cudaDeviceSynchronize_chk() gpuErrChk(cudaDeviceSynchronize ())

#define cudaPeekAtLastError_chk() gpuErrChk(cudaPeekAtLastError ())

#endif

/*------------------------------------ Not yet done
cudaDeviceProp
cudaFree
cudaGetDeviceProperties
cudaGetErrorString
cudaOccupancyMaxActiveBlocksPerMultiprocessor
cudaOccupancyMaxPotentialBlockSize
--------------------------------------*/ 
