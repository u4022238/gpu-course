#include <stdio.h>
#include "./errChk.h"

CUDA void say_it ()
   {
#  ifdef __CUDA_ARCH__
      printf ("Hello from device!!\n");
#  else
      printf ("Hello from host!!\n");
#  endif
   }

__global__ void say_hello (void)
   {
   say_it ();
   }

int main (void)
   {
   printf ("From device:\n");
   say_hello<<<3,1>>> ();

   // Wait for the threads to terminate
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   printf ("From host:\n");
   say_it ();
   return 0;
   }

   
