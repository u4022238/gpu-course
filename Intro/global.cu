#include <stdio.h>
#include "./errChk.h"

// Illustrates the use of global (local) memory

__device__ int ginc;

__device__ void increment (int *a)
   {
   // Adds ginc to each value
   a[ginc] = ginc;
   printf ("In thread %d: ginc = %d, a = %d\n", threadIdx.x, ginc, a[ginc]);
   }

__device__ void say_hello ()
   {
   printf ("Hello from thread %d\n", threadIdx.x);
   }

__device__ void say_goodbye ()
   {
   printf ("Goodbye from thread %d\n", threadIdx.x);
   }

__global__ void add (int *a, int size)
   {
   int incr = threadIdx.x;
   ginc = incr;
   printf ("In thread %d: ginc = %d\n", threadIdx.x, ginc);
   // say_hello();
   increment (a);
   // say_goodbye();
   }

int main (void)
   {
   const int N=10;
   int a[N];
   int size = N * sizeof(int);

   // Initialize a
   for (int i=0; i<N; i++) a[i] = 147;

   // Allocate memory on the device
   int *g_a;
   cudaMalloc_chk((void **) &g_a, size);
   cudaMemcpy_chk(g_a, a, size, cudaMemcpyHostToDevice);

   // Launch the kernel in different threads on the GPU
   add<<<1,N>>> (g_a, N);
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   // Read back once threads are finished
   cudaMemcpy_chk(a, g_a, size, cudaMemcpyDeviceToHost);

   // Print some results
   for (int i=0; i<N; i++)
      printf ("%3d : %3d\n", i, a[i]);

   // Finish
   return 0;
   }

