#include <stdio.h>
#include "./errChk.h"

__global__ void mykernel (void)
   {
   printf("Go jump\n");
   return;
   }

int main (void)
   {
   mykernel<<<1,7>>> ();
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();
   printf ("Hello World!\n");
   return 0;
   }

   
