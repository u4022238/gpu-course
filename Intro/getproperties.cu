/*
    struct cudaDeviceProp {
        char name[256];
        size_t totalGlobalMem;
        size_t sharedMemPerBlock;
        int regsPerBlock;
        int warpSize;
        size_t memPitch;
        int maxThreadsPerBlock;
        int maxThreadsDim[3];
        int maxGridSize[3];
        int clockRate;
        size_t totalConstMem;
        int major;
        int minor;
        size_t textureAlignment;
        size_t texturePitchAlignment;
        int deviceOverlap;
        int multiProcessorCount;
        int kernelExecTimeoutEnabled;
        int integrated;
        int canMapHostMemory;
        int computeMode;
        int maxTexture1D;
        int maxTexture1DMipmap;
        int maxTexture1DLinear;
        int maxTexture2D[2];
        int maxTexture2DMipmap[2];
        int maxTexture2DLinear[3];
        int maxTexture2DGather[2];
        int maxTexture3D[3];
        int maxTexture3DAlt[3];
        int maxTextureCubemap;
        int maxTexture1DLayered[2];
        int maxTexture2DLayered[3];
        int maxTextureCubemapLayered[2];
        int maxSurface1D;
        int maxSurface2D[2];
        int maxSurface3D[3];
        int maxSurface1DLayered[2];
        int maxSurface2DLayered[3];
        int maxSurfaceCubemap;
        int maxSurfaceCubemapLayered[2];
        size_t surfaceAlignment;
        int concurrentKernels;
        int ECCEnabled;
        int pciBusID;
        int pciDeviceID;
        int pciDomainID;
        int tccDriver;
        int asyncEngineCount;
        int unifiedAddressing;
        int memoryClockRate;
        int memoryBusWidth;
        int l2CacheSize;
        int maxThreadsPerMultiProcessor;
        int streamPrioritiesSupported;
        int globalL1CacheSupported;
        int localL1CacheSupported;
        size_t sharedMemPerMultiprocessor;
        int regsPerMultiprocessor;
        int managedMemSupported;       // Not recognized for some strange reason
        int isMultiGpuBoard;
        int multiGpuBoardGroupID;
        int singleToDoublePrecisionPerfRatio;
        int pageableMemoryAccess;
        int concurrentManagedAccess;
    }
*/

#include "stdio.h"
#include "./errChk.h"

int main (void)
   {
   // Get the device properties

   // First of all, determine whether there are any devices.
   int ndevices;
   cudaGetDeviceCount (&ndevices);

   // Print out the number of devices
   printf ("Number of devices found:  %d\n", ndevices);

   for (int device = 0; device < ndevices; device++)
      {
      printf ("\n-------------------------\n");
      printf ("       Device %d\n", device);
      printf ("-------------------------\n");

      // cudaGetDevice (&device);
      // cudaSetDevice (device);       // Sets the device to be used

      cudaDeviceProp prop;
      cudaGetDeviceProperties (&prop, device);

      // Print them out
      printf (" name = \"%s\"\n", prop.name);
      printf (" totalGlobalMem = %lu\n", prop.totalGlobalMem);
      printf (" sharedMemPerBlock = %lu\n", prop.sharedMemPerBlock);
      printf (" regsPerBlock = %d\n", prop.regsPerBlock);
      printf (" warpSize = %d\n", prop.warpSize);
      printf (" memPitch = %lu\n", prop.memPitch);
      printf (" maxThreadsPerBlock = %d\n", prop.maxThreadsPerBlock);
      printf (" maxThreadsDim[3] = %d, %d, %d\n", 
            prop.maxThreadsDim[0],
            prop.maxThreadsDim[1],
            prop.maxThreadsDim[2]);
      printf (" maxGridSize[3] = %d, %d, %d\n", 
            prop.maxGridSize[0],
            prop.maxGridSize[1],
            prop.maxGridSize[2]);
      printf (" clockRate = %d\n", prop.clockRate);
      printf (" totalConstMem = %lu\n", prop.totalConstMem);
      printf (" major = %d\n", prop.major);
      printf (" minor = %d\n", prop.minor);
      printf (" textureAlignment = %lu\n", prop.textureAlignment);
      printf (" texturePitchAlignment = %lu\n", prop.texturePitchAlignment);
      printf (" deviceOverlap = %d\n", prop.deviceOverlap);
      printf (" multiProcessorCount = %d\n", prop.multiProcessorCount);
      printf (" kernelExecTimeoutEnabled = %d\n", prop.kernelExecTimeoutEnabled);
      printf (" integrated = %d\n", prop.integrated);
      printf (" canMapHostMemory = %d\n", prop.canMapHostMemory);
      printf (" computeMode = %d\n", prop.computeMode);
      printf (" maxTexture1D = %d\n", prop.maxTexture1D);
      printf (" maxTexture1DMipmap = %d\n", prop.maxTexture1DMipmap);
      printf (" maxTexture1DLinear = %d\n", prop.maxTexture1DLinear);
      printf (" maxTexture2D[2] = %d, %d\n", 
            prop.maxTexture2D[0],
            prop.maxTexture2D[1]);
      printf (" maxTexture2DMipmap[2] = %d, %d\n", 
            prop.maxTexture2DMipmap[0],
            prop.maxTexture2DMipmap[1]);
      printf (" maxTexture2DLinear[3] = %d, %d, %d\n", 
            prop.maxTexture2DLinear[0],
            prop.maxTexture2DLinear[1],
            prop.maxTexture2DLinear[2]);
      printf (" maxTexture2DGather[2] = %d, %d\n", 
            prop.maxTexture2DGather[0],
            prop.maxTexture2DGather[1]);
      printf (" maxTexture3D[3] = %d, %d, %d\n", 
            prop.maxTexture3D[0],
            prop.maxTexture3D[1],
            prop.maxTexture3D[2]);
      printf (" maxTexture3DAlt[3] = %d, %d, %d\n", 
            prop.maxTexture3DAlt[0],
            prop.maxTexture3DAlt[1],
            prop.maxTexture3DAlt[2]);
      printf (" maxTextureCubemap = %d\n", prop.maxTextureCubemap);
      printf (" maxTexture1DLayered[2] = %d, %d\n", 
            prop.maxTexture1DLayered[0],
            prop.maxTexture1DLayered[1]);
      printf (" maxTexture2DLayered[3] = %d, %d, %d\n", 
            prop.maxTexture2DLayered[0],
            prop.maxTexture2DLayered[1],
            prop.maxTexture2DLayered[2]);
      printf (" maxTextureCubemapLayered[2] = %d, %d\n", 
            prop.maxTextureCubemapLayered[0],
            prop.maxTextureCubemapLayered[1]);
      printf (" maxSurface1D = %d\n", prop.maxSurface1D);
      printf (" maxSurface2D[2] = %d, %d\n", 
            prop.maxSurface2D[0],
            prop.maxSurface2D[1]);
      printf (" maxSurface3D[3] = %d, %d, %d\n", 
            prop.maxSurface3D[0],
            prop.maxSurface3D[1],
            prop.maxSurface3D[2]);
      printf (" maxSurface1DLayered[2] = %d, %d\n", 
            prop.maxSurface1DLayered[0],
            prop.maxSurface1DLayered[1]);
      printf (" maxSurface2DLayered[3] = %d, %d, %d\n", 
            prop.maxSurface2DLayered[0],
            prop.maxSurface2DLayered[1],
            prop.maxSurface2DLayered[2]);
      printf (" maxSurfaceCubemap = %d\n", prop.maxSurfaceCubemap);
      printf (" maxSurfaceCubemapLayered[2] = %d, %d\n", 
            prop.maxSurfaceCubemapLayered[0],
            prop.maxSurfaceCubemapLayered[1]);
      printf (" surfaceAlignment = %lu\n", prop.surfaceAlignment);
      printf (" concurrentKernels = %d\n", prop.concurrentKernels);
      printf (" ECCEnabled = %d\n", prop.ECCEnabled);
      printf (" pciBusID = %d\n", prop.pciBusID);
      printf (" pciDeviceID = %d\n", prop.pciDeviceID);
      printf (" pciDomainID = %d\n", prop.pciDomainID);
      printf (" tccDriver = %d\n", prop.tccDriver);
      printf (" asyncEngineCount = %d\n", prop.asyncEngineCount);
      printf (" unifiedAddressing = %d\n", prop.unifiedAddressing);
      printf (" memoryClockRate = %d\n", prop.memoryClockRate);
      printf (" memoryBusWidth = %d\n", prop.memoryBusWidth);
      printf (" l2CacheSize = %d\n", prop.l2CacheSize);
      printf (" maxThreadsPerMultiProcessor = %d\n", prop.maxThreadsPerMultiProcessor);
      printf (" streamPrioritiesSupported = %d\n", prop.streamPrioritiesSupported);
      printf (" globalL1CacheSupported = %d\n", prop.globalL1CacheSupported);
      printf (" localL1CacheSupported = %d\n", prop.localL1CacheSupported);
      printf (" sharedMemPerMultiprocessor = %lu\n", prop.sharedMemPerMultiprocessor);
      printf (" regsPerMultiprocessor = %d\n", prop.regsPerMultiprocessor);
      // printf (" managedMemSupported = %d\n", prop.managedMemSupported);
      printf (" isMultiGpuBoard = %d\n", prop.isMultiGpuBoard);
      printf (" multiGpuBoardGroupID = %d\n", prop.multiGpuBoardGroupID);
      printf (" singleToDoublePrecisionPerfRatio = %d\n", prop.singleToDoublePrecisionPerfRatio);
      printf (" pageableMemoryAccess = %d\n", prop.pageableMemoryAccess);
      printf (" concurrentManagedAccess = %d\n", prop.concurrentManagedAccess);
   
      }

   return EXIT_SUCCESS;
   }
