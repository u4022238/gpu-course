#include <stdio.h>
#include "./errChk.h"

// This illustrates adding two arrays using managed memory.  This
// omits explicit migration of data from host to device memory
// It also illustrates the use of device functions.

__global__ void dot (int *a, int *b, int *c, int dim, int nthreads)
   {
   const int nvals = (dim + nthreads-1) / nthreads;

   const int start = blockIdx.x * nvals;
   int end = (blockIdx.x + 1) * nvals;
   if (end > dim) end = dim;

   // printf ("Thread %d : %d -- %d\n", blockIdx.x, start, end);

   int val = 0;
   for (int reps = 0; reps < 100 ; reps++)
      for (int i=start; i<end; i++)
         val += a[i] * b[i];

   c[blockIdx.x] = val;
   }

void random_ints (int * const a, const int N)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = rand() % 100;
   }

void const_ints (int * const a, const int N, const int val)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = val;
   }

int main (void)
   {
   const int nthreads=512;
   const int nvals = 100000;
   int *a, *b, *c;
   int valsize = nvals * sizeof(int);
   int tsize   = nthreads * sizeof(int);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, valsize);
   cudaMallocManaged_chk ((void **) &b, valsize);
   cudaMallocManaged_chk ((void **) &c, tsize);

   // Initialize a, b, c
   const_ints (a, nvals, 1);
   const_ints (b, nvals, 2);

   printf ("Starting\n"); fflush(stdout);

   // Launch the kernel in different threads on the GPU
   dot<<<nthreads,1>>> (a, b, c, nvals, nthreads);

   // Wait for the threads to complete
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   printf ("Finished\n"); fflush(stdout);

   // Now, add up the final values
   int sum = 0;
   for (int i=0; i<nthreads; i++)
      sum += c[i];
   printf ("Sum = %d\n", sum);

   // Clean up
   cudaFree (a);
   cudaFree (b);
   cudaFree (c);

   // Finish
   return 0;
   }

