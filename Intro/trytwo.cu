#include <stdio.h>
#include "./errChk.h"

// This illustrates adding two arrays using managed memory.  This
// omits explicit migration of data from host to device memory
// It also illustrates the use of device functions.

__global__ void dot (
      int *a, int *b, int *c, 
      int nvals,           // Total number of values in a and b
      int vals_per_thread, // Number of values to be done by each thread
      int start_thread     // Number of first thread 
      )
   {
   // Work out which thread -- this assumes a 1-d grid of blocks and threads
   int thread_this_device = threadIdx.x + blockIdx.x * blockDim.x;
   int threadid = start_thread + thread_this_device;

   // Work out the actual device this is working on
   int device;
   cudaGetDevice (&device);

   // Determine the region of array to be summed
   int tstart = vals_per_thread * threadid;
   int tend = tstart + vals_per_thread;
   if (tend > nvals) tend = nvals;

   // See that this is working
   printf ("dot: thread %d on device %d: summing %d to %d\n", 
         threadid, device, tstart, tend);

   int val = 0;
   for (int i=tstart; i<tend; i++)
      val += a[i] * b[i];

   // One output value per thread
   c[threadid] = val;
   }

void random_ints (int * const a, const int N)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = rand() % 100;
   }

void const_ints (int * const a, const int N, const int val)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = val;
   }

int main (void)
   {
   // Size of the problem
   const int nthreads=10;
   const int nvals = 1000;
   const int vals_per_thread = (nvals + nthreads - 1) / nthreads;

   // Values to be worked with 
   int *a, *b, *c;
   int valsize = nvals * sizeof(int);
   int tsize   = nthreads * sizeof(int);

   // Determine whether there are any devices.
   int ndevices;
   gpuErrChk(cudaGetDeviceCount (&ndevices));

   // Print out the number of devices
   printf ("Number of devices found:  %d\n", ndevices);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, valsize);
   cudaMallocManaged_chk ((void **) &b, valsize);
   cudaMallocManaged_chk ((void **) &c, tsize);

   // Initialize a, b, c
   const_ints (a, nvals, 1);
   const_ints (b, nvals, 2);

   printf ("Starting\n"); fflush(stdout);

   // Work out how many threads to give to each device
   int threads_per_device = (nthreads + ndevices - 1) / ndevices;

   for (int device = 0; device < ndevices; device++)
      {
      // Launch the kernel in different threads on the GPU
      cudaSetDevice_chk (device);

      // Launch threads
      int start_thread = threads_per_device * device;
      dot<<<threads_per_device,1>>> 
         (a, b, c, nvals, vals_per_thread, start_thread);
      cudaPeekAtLastError_chk();
      }

   // Wait for the threads to complete
   cudaDeviceSynchronize_chk();

   printf ("Finished\n"); fflush(stdout);

   // Now, add up the final values
   int sum = 0;
   for (int i=0; i<nthreads; i++)
      {
      printf ("c[%d] = %d\n", i, c[i]);
      sum += c[i];
      }
   printf ("Sum = %d\n", sum);

   // Clean up
   cudaFree (a);
   cudaFree (b);
   cudaFree (c);

   // Finish
   return 0;
   }

