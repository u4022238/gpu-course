#include <stdio.h>
#include "./errChk.h"

__global__ void hello (void)
   {
   cudaError_t code;

   int devcount;
   code = cudaGetDeviceCount (&devcount);
#if 0
   if (code == cudaSuccess)
      printf ("device: cudaGetDeviceCount succeeded: code %08x, devcount = %d\n", code, devcount);
   else
      printf ("device: cudaGetDeviceCount failed: code %08x\n", code);
#endif

   int adevice;
   code = cudaGetDevice (&adevice);
   if (code == cudaSuccess)
      printf ("device: cudaGetDevice succeeded: code %08x, adevice = %d\n", code, adevice);
   else
      printf ("device: cudaGetDevice failed: code %08x\n", code);
   printf ("Hello from device %d\n", adevice);

#if 0
   int val;
   code = cudaDeviceGetAttribute (&val, cudaDevAttrMaxThreadsPerBlock, 0);
   if (code == cudaSuccess)
      printf ("device: cudaDeviceGetAttribute succeeded: code %08x, val = %d\n", code, val);
   else
      printf ("device: cudaDeviceGetAttribute failed: code %08x\n", code);

   code = cudaDeviceGetAttribute (&val, cudaDevAttrMaxThreadsPerBlock, 1);
   if (code == cudaSuccess)
      printf ("device: cudaDeviceGetAttribute succeeded: code %08x, val = %d\n", code, val);
   else
      printf ("device: cudaDeviceGetAttribute failed: code %08x\n", code);

   // const char *astring = cudaGetErrorString (code);
   // printf ("device: cudaGetDevice returned with code \"%s\"\n", astring);
#endif
   }

int main (void)
   {
   cudaSetDevice_chk (1);
   hello<<<1,1>>> ();
   cudaError_t code = cudaPeekAtLastError ();


   printf ("host: kernel returned with code \"%s\"\n", 
      cudaGetErrorString (code));

   code = cudaDeviceSynchronize();
   printf ("host: cudaDeviceSynchronize returned with code \"%s\"\n", 
      cudaGetErrorString (code));

   printf ("Hello from host\n");
   return 0;
   }

   
