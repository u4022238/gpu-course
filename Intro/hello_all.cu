#include <stdio.h>
#include "./errChk.h"

__global__ void hello (void)
   {
   // This is here just as a bug fix.  You need a dummy call to a device function.
   int ndevices;
   cudaGetDeviceCount (&ndevices);

   int adevice;
   cudaError_t code = cudaGetDevice (&adevice);
   printf ("Hello from device %d\n", adevice);
   }

int main (void)
   {

   int ndevices;
   cudaGetDeviceCount (&ndevices);
   printf ("Number of devices: %d\n", ndevices);

   for (int device=0; device<ndevices; device++)
      {
      // Set the device
      cudaSetDevice_chk(device);

      // printf ("About to launch on device %d\n", thedevice);
      hello<<<3,1>>> ();
      cudaPeekAtLastError_chk();
      }

   cudaDeviceSynchronize_chk();

   printf ("Hello from host\n");
   return 0;
   }

   
