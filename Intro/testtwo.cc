#include <stdlib.h>
#include <stdio.h>

int main (int argc, char *argv[])
   {

   const int nrows = 5;
   const int ncols = 10;
   const int nvals = nrows * ncols;
   int (*A)[ncols] = (int (*)[ncols])malloc (nvals * sizeof(int));

   for (int i=0; i<nrows; i++)
      for (int j=0; j<ncols; j++)
         A[i][j] = i*ncols + j;

   // Write them out
   for (int i=0; i<nrows; i++)
      for (int j=0; j<ncols; j++)
         printf ("%d \n", A[i][j]);
   fflush(stdout);

   return EXIT_SUCCESS;
   }
