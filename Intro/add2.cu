#include <stdio.h>
#include "./errChk.h"

__global__ void add (int *a, int *b, int *c)
   {
   c[blockIdx.x] = a[blockIdx.x] + b[blockIdx.x];
   }

void random_ints (int * const a, const int N)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = rand() % 100;
   }

int main (void)
   {
   const int N=512;
   int *a, *b, *c;
   int *d_a, *d_b, *d_c;
   int size = N * sizeof(int);

   // Allocate memory on the device
   cudaMalloc_chk ((void **) &d_a, size);
   cudaMalloc_chk ((void **) &d_b, size);
   cudaMalloc_chk ((void **) &d_c, size);

   // Initialize a, b, c
   a = (int *)malloc (size); random_ints(a, N);
   b = (int *)malloc (size); random_ints(b, N);
   c = (int *)malloc (size); 

   // Copy inputs to device
   cudaMemcpy_chk (d_a, a, size, cudaMemcpyHostToDevice);
   cudaMemcpy_chk (d_b, b, size, cudaMemcpyHostToDevice);

   // Launch the kernel on GPU
   add<<<N,1>>> (d_a, d_b, d_c);

   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   // Copy result back
   cudaMemcpy_chk (c, d_c, size, cudaMemcpyDeviceToHost);

   // Print some results
   for (int i=0; i<20; i++)
      printf ("%3d : %2d + %2d = %3d\n", i, a[i], b[i], c[i]);

   // Clean up
   cudaFree (d_a);
   cudaFree (d_b);
   cudaFree (d_c);

   free(a); 
   free(b);
   free(c);

   // Finish
   return 0;
   }

