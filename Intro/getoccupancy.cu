#include "stdio.h"
#include "./errChk.h"

__global__ 
void MyKernel(int *array, int arrayCount) 
   { 
   // Simply squares the elements of the array
   int idx = threadIdx.x + blockIdx.x*blockDim.x; 
   if (idx < arrayCount) 
      array[idx] *= array[idx]; 
   } 

void launchMyKernel(int *array, int arrayCount) 
   { 
   int blockSize;    // The launch configurator returned block size 
   int minGridSize;  // The minimum grid size needed to achieve the 
                     // maximum occupancy for a full device launch 
   int numBlocks;     // The actual grid size needed, based on input size 

   cudaOccupancyMaxPotentialBlockSize( 
         &minGridSize, &blockSize, MyKernel, 0, 0); 

   printf ("minGridSize = %d, blockSize = %d\n", minGridSize, blockSize);

   // Round up according to array size 
   numBlocks = (arrayCount + blockSize - 1) / blockSize; 

   // Launch kernel
   MyKernel<<< numBlocks, blockSize >>>(array, arrayCount); 
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   // calculate theoretical occupancy
   int maxActiveBlocks;
   cudaOccupancyMaxActiveBlocksPerMultiprocessor( 
         &maxActiveBlocks, MyKernel, blockSize, 0);

   // Get device properties
   int device;
   cudaDeviceProp props;
   cudaGetDevice_chk(&device);
   cudaGetDeviceProperties(&props, device);

   // Print some device properties
   printf ("warpSize = %d, maxThreadsPerMultiProcessor = %d\n",
         props.warpSize, props.maxThreadsPerMultiProcessor);

   // Compute the occupancy
   float maxActiveThreads = maxActiveBlocks * blockSize;
   float maxActiveWarps = maxActiveThreads / props.warpSize;
   float maxWarpsPerMultiProcessor =  
      (float)(props.maxThreadsPerMultiProcessor / props.warpSize);
   float occupancy2 = maxActiveWarps / maxWarpsPerMultiProcessor;

   // The one given by the program
   float occupancy = 
      (maxActiveBlocks * blockSize / props.warpSize) 
      / (float)(props.maxThreadsPerMultiProcessor / props.warpSize);

   printf("Launched blocks of size %d. Theoretical occupancy: %f\n", 
      blockSize, occupancy);
   printf("Launched blocks of size %d. Theoretical occupancy: %f\n", 
      blockSize, occupancy2);
   }

int main (void)
   {
   // Allocate an array of values
   const int arraySize = 100000;
   int *val;
   cudaMallocManaged_chk ((void **)&val, arraySize * sizeof(int));

   // Fill up some values -- pseudo primes
   const int prime = 65537;
   const int offset = 10374;
   const int base = 23864;
   for (int i=0; i<arraySize; i++)
      val[i] = (i * base + offset) % prime;

   // Launch a kernel
   launchMyKernel (val, arraySize);

   return EXIT_SUCCESS;
   }
