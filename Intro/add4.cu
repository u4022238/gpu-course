#include <stdio.h>
#include "./errChk.h"

// This illustrates adding two arrays using managed memory.  This
// omits explicit migration of data from host to device memory
// It also illustrates the use of device functions.

__device__ double do_add (int x, int y)
   {
   return ((double)x + (double) y);
   }

__global__ void add (int *a, int *b, int *c)
   {
   // c[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
   c[threadIdx.x] = (int)floor(do_add (a[threadIdx.x], b[threadIdx.x]));
   printf ("In thread %d\n", threadIdx.x);
   }

void random_ints (int * const a, const int N)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = rand() % 100;
   }

int main (void)
   {
   const int N=512;
   int *a, *b, *c;
   int size = N * sizeof(int);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, size);
   cudaMallocManaged_chk ((void **) &b, size);
   cudaMallocManaged_chk ((void **) &c, size);

   // Initialize a, b, c
   random_ints(a, N);
   random_ints(b, N);
   for (int i=0; i<N; i++) c[i] = i;

   // Launch the kernel in different threads on the GPU
   add<<<1,N>>> (a, b, c);

   // Wait for the threads to complete
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   // Print some results
   for (int i=0; i<20; i++)
      printf ("%3d : %2d + %2d = %3d\n", i, a[i], b[i], c[i]);

   // Clean up
   // cudaFree (a);
   // cudaFree (b);
   // cudaFree (c);

   // Finish
   return 0;
   }

