#include <stdio.h>
#include "./errChk.h"

// This illustrates the use of two-dimensional arrays.
// It uses Managed memory, so as to avoid explicit copying of data.
// Hence, it omits explicit migration of data from host to device memory

// This is a global, dimensions of a matrix
const int nrows = 10;

class Person
   {
   int height;
   int age;
   int weight;
   };

// It also illustrates the use of device functions.
const int ncols = 10;

__global__ void average_height ( Person *persons, int npersons);
   {
   // Each thread will compute the sum of heights of a 


   }

void rand_matrix (int (*a)[ncols])
   {
   // Initialize the values
   for (int i=0; i<nrows; i++)
      for (int j=0; j<ncols; j++)
         a[i][j] = rand() % 100;
   }

void print_matrix (int (*A)[ncols])
   {
   // Print out the product
   for (int i=0; i<nrows; i++)
      {
      for (int j=0; j<ncols; j++)
         printf ("%5d ", A[i][j]);
      printf ("\n");
      }
   }

class 

int main (void)
   {
   const int nvals = nrows * ncols;
   int (*a)[ncols]; 
   int (*b)[ncols];
   int (*c)[ncols];
   int datasize =  nvals * sizeof(int);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, datasize);
   cudaMallocManaged_chk ((void **) &b, datasize);
   cudaMallocManaged_chk ((void **) &c, datasize);

   // Initialize a, b
   rand_matrix (a);
   rand_matrix (b);

   printf ("Starting\n"); fflush(stdout);

   // Launch the kernel in different threads on the GPU
   // One thread is launched for each row of the array
   matmult<<<nvals,1>>> (a, b, c);

   // Wait for the threads to complete
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   printf ("Finished\n"); fflush(stdout);

   // Print out the results
   printf ("Matrix A\n");
   print_matrix (a);

   printf ("Matrix B\n");
   print_matrix (b);

   printf ("Matrix C\n");
   print_matrix (c);

   // Clean up
   cudaFree (a);
   cudaFree (b);
   cudaFree (c);

   // Finish
   return 0;
   }

