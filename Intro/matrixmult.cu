#include <stdio.h>
#include "./errChk.h"

// This illustrates the use of two-dimensional arrays.
// It uses Managed memory, so as to avoid explicit copying of data.
// Hence, it omits explicit migration of data from host to device memory
// It also illustrates the use of device functions.

// This is a global, dimensions of a matrix
const int nrows = 10;
const int ncols = 10;

__global__ void matmult ( int (*a)[ncols], int (*b)[ncols], int (*c)[ncols])
   {
   // Each thread will compute a single entry of the product
   // and store it in the appropriate entry of c.

   // Work out which entry to process, based on blockId
   // It is assumed that there are as many threads as there are entries
   const int entry = blockIdx.x;
   const int therow = entry /ncols;
   const int thecol = entry % ncols;

   // Take the matrix product
   int val = 0;
   for (int j=0; j<ncols; j++)
      val += a[therow][j] * b[j][thecol];

   printf ("Thread %d processing row %d, column %d: val = %d\n", 
         entry, therow, thecol, val);

   // Store in c
   c[therow][thecol] = val;
   }

void rand_matrix (int (*a)[ncols])
   {
   // Initialize the values
   for (int i=0; i<nrows; i++)
      for (int j=0; j<ncols; j++)
         a[i][j] = rand() % 100;
   }

void print_matrix (int (*A)[ncols])
   {
   // Print out the product
   for (int i=0; i<nrows; i++)
      {
      for (int j=0; j<ncols; j++)
         printf ("%5d ", A[i][j]);
      printf ("\n");
      }
   }

int main (void)
   {
   const int nvals = nrows * ncols;
   int (*a)[ncols]; 
   int (*b)[ncols];
   int (*c)[ncols];
   int datasize =  nvals * sizeof(int);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, datasize);
   cudaMallocManaged_chk ((void **) &b, datasize);
   cudaMallocManaged_chk ((void **) &c, datasize);

   // Initialize a, b
   rand_matrix (a);
   rand_matrix (b);

   printf ("Starting\n"); fflush(stdout);

   // Launch the kernel in different threads on the GPU
   // One thread is launched for each row of the array
   matmult<<<nvals,1>>> (a, b, c);

   // Wait for the threads to complete
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   printf ("Finished\n"); fflush(stdout);

   // Print out the results
   printf ("Matrix A\n");
   print_matrix (a);

   printf ("Matrix B\n");
   print_matrix (b);

   printf ("Matrix C\n");
   print_matrix (c);

   // Clean up
   cudaFree (a);
   cudaFree (b);
   cudaFree (c);

   // Finish
   return 0;
   }

