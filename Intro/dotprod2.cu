#include <stdio.h>
#include "./errChk.h"

// This illustrates adding two arrays using memory copying. 

__global__ void dot (int *a, int *b, int *c, int dim, int nthreads)
   {
   //printf ("Thread %d \n", blockIdx.x);

   const int nvals = (dim + nthreads-1) / nthreads;

   const int start = blockIdx.x * nvals;
   int end = (blockIdx.x + 1) * nvals;
   if (end > dim) end = dim;

   //printf ("Thread %d : %d -- %d\n", blockIdx.x, start, end);

   int val = 0;
   for (int reps = 0; reps < 100 ; reps++)
      for (int i=start; i<end; i++)
         val += a[i] * b[i];

   //printf ("Thread %d : %d -- %d: val = %d\n", blockIdx.x, start, end, val);

   c[blockIdx.x] = val;
   }

void random_ints (int * const a, const int N)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = rand() % 100;
   }

void const_ints (int * const a, const int N, const int val)
   {
   // Initialize the values
   for (int i=0; i<N; i++)
      a[i] = val;
   }

int main (void)
   {
   const int nthreads=512;
   const int nvals = 10000;
   int *a, *b, *c;
   int *d_a, *d_b, *d_c;
   int valsize = nvals * sizeof(int);
   int tsize   = nthreads * sizeof(int);

   // Allocate local
   a = (int *) malloc (valsize);
   b = (int *) malloc (valsize);
   c = (int *) malloc (tsize);

   // Allocate device memory
   cudaMalloc_chk ((void **) &d_a, valsize);
   cudaMalloc_chk ((void **) &d_b, valsize);
   cudaMalloc_chk ((void **) &d_c, tsize);

   // Initialize a, b, c
   const_ints (a, nvals, 1);
   const_ints (b, nvals, 2);

   printf ("Starting\n"); fflush(stdout);

   // Copy inputs to device
   cudaMemcpy_chk (d_a, a, valsize, cudaMemcpyHostToDevice);
   cudaMemcpy_chk (d_b, b, valsize, cudaMemcpyHostToDevice);

   // Launch the kernel in different threads on the GPU
   dot<<<nthreads,1>>> (d_a, d_b, d_c, nvals, nthreads);

   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   // Copy result back
   cudaMemcpy_chk (c, d_c, tsize, cudaMemcpyDeviceToHost);

   printf ("Finished\n"); fflush(stdout);

   // Now, add up the final values
   int sum = 0;
   for (int i=0; i<nthreads; i++)
      sum += c[i];
   printf ("Sum = %d\n", sum);

   // Clean up
   free (a);
   free (b);
   free (c);

   cudaFree (d_a);
   cudaFree (d_b);
   cudaFree (d_c);

   // Finish
   return 0;
   }

