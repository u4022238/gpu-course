#include <stdio.h>
#include "./errChk.h"

__global__ void hello (void)
   {
   cudaError_t code;

   // Should be unnecessary, but without this, the following
   // call to cudaGetDevice fails with an unknown error.
   int devcount;
   code = cudaGetDeviceCount (&devcount);

   // Now, get the device
   int adevice;
   code = cudaGetDevice (&adevice);
   if (code == cudaSuccess)
      printf ("device: cudaGetDevice succeeded: code %08x, adevice = %d\n", code, adevice);
   else
      printf ("device: cudaGetDevice failed: code %08x\n", code);

   // Identify the device
   printf ("Hello from device %d\n", adevice);
   }

int main (void)
   {
   cudaSetDevice_chk (0);
   hello<<<1,1>>> ();

   cudaError_t code;
   code = cudaPeekAtLastError ();
   printf ("host: kernel returned with code \"%s\"\n", 
      cudaGetErrorString (code));

   cudaSetDevice_chk (1);
   hello<<<1,1>>> ();

   code = cudaPeekAtLastError ();
   printf ("host: kernel returned with code \"%s\"\n", 
      cudaGetErrorString (code));

   code = cudaDeviceSynchronize();
   printf ("Hello from host\n");
   return 0;
   }

   
