#include <stdio.h>

__global__ void add (int *a, int *b, int *c)
   {
   *c = *a + *b;
   }

int main (void)
   {
   int a = 2;
   int b = 7;
   int c;
   int *d_a, *d_b, *d_c;
   int size = sizeof(int);

   // Allocate memory on the device
   cudaMalloc ((void **) &d_a, size);
   cudaMalloc ((void **) &d_b, size);
   cudaMalloc ((void **) &d_c, size);

   // Copy inputs to device
   cudaMemcpy (d_a, &a, size, cudaMemcpyHostToDevice);
   cudaMemcpy (d_b, &b, size, cudaMemcpyHostToDevice);

   // Launch the kernel on GPU
   add<<<1,1>>> (d_a, d_b, d_c);

   // Copy result back
   cudaMemcpy (&c, d_c, size, cudaMemcpyDeviceToHost);

   // Clean up
   cudaFree (d_a);
   cudaFree (d_b);
   cudaFree (d_c);

   // Print out the result
   printf ("%d + %d = %d\n", a, b, c);

   // Finish
   return 0;
   }

