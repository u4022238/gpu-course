#include <stdio.h>
#include "./errChk.h"

// This illustrates the use of two-dimensional arrays.
// It uses Managed memory, so as to avoid explicit copying of data.
// Hence, it omits explicit migration of data from host to device memory
// It also illustrates the use of device functions.

// This is a global, number of columns in an array
const int ncols = 10;

__global__ void dot ( int (*a)[ncols], int (*b)[ncols], int *c)
   {
   // Each thread will take the dot product of one row of a and b
   // and store them in the appropriate entry of c.

   // Work out which row to process, based on blockId
   const int therow = blockIdx.x;

   // Take the dot product of the row of a and b
   int val = 0;
   for (int j=0; j<ncols; j++)
      val += a[therow][j] * b[therow][j];

   printf ("Thread %d processing row %d: sum = %d\n", therow, therow, val);

   // Try something
   int *x; 
   cudaMalloc((void **) &x, 10);
   for (int i=0; i<10; i++)
      x[i] = i;

   // Store in c
   c[therow] = val;
   }

void const_ints (int (*a)[ncols], const int nrows, const int val)
   {
   // Initialize the values
   for (int i=0; i<nrows; i++)
      for (int j=0; j<nrows; j++)
         a[i][j] = val*i;
   }

int main (void)
   {
   const int nrows = 20;
   const int nvals = nrows * ncols;
   int (*a)[ncols]; 
   int (*b)[ncols];
   int *c;
   int datasize =  nvals * sizeof(int);

   // Allocate memory on the device
   cudaMallocManaged_chk ((void **) &a, datasize);
   cudaMallocManaged_chk ((void **) &b, datasize);
   cudaMallocManaged_chk ((void **) &c, nrows * sizeof(int));

   // Initialize a, b
   const_ints (a, nrows, 2);
   const_ints (b, nrows, 3);

   printf ("Starting\n"); fflush(stdout);

   // Launch the kernel in different threads on the GPU
   // One thread is launched for each row of the array
   dot<<<nrows,1>>> (a, b, c);

   // Wait for the threads to complete
   cudaPeekAtLastError_chk();
   cudaDeviceSynchronize_chk();

   printf ("Finished\n"); fflush(stdout);

   // Now, add up the final values
   int sum = 0;
   for (int i=0; i<nrows; i++)
      sum += c[i];
   printf ("Sum = %d\n", sum);

   // Clean up
   cudaFree (a);
   cudaFree (b);
   cudaFree (c);

   // Finish
   return 0;
   }

