#include <stdio.h>
#include "./errChk.h"

__global__ void hello (void)
   {
   int adevice;
   cudaError_t code = cudaGetDevice (&adevice);
   if (code == cudaSuccess)
      printf ("device: cudaGetDevice succeeded: code %08x\n", code);
   else
      printf ("device: cudaGetDevice failed: code %08x\n", code);

   printf ("Hello from device %d\n", adevice);

   // const char *astring = cudaGetErrorString (code);
   // printf ("device: cudaGetDevice returned with code \"%s\"\n", astring);
   }

int main (void)
   {
   int ndevices;
   cudaGetDeviceCount (&ndevices);
   printf ("Number of devices: %d\n", ndevices);

   // Set the device and check that it works
   cudaSetDevice_chk(0);
   int thedevice;
   cudaError_t code = cudaGetDevice (&thedevice);
   printf ("host: cudaGetDevice returned with code %08x = \"%s\"\n", 
      code, cudaGetErrorString (code));

   printf ("About to launch on device %d\n", thedevice);
   hello<<<1,1>>> ();
   cudaError_t code2 = cudaPeekAtLastError ();

   printf ("host: kernel returned with code \"%s\"\n", 
      cudaGetErrorString (code2));

   cudaDeviceSynchronize_chk();
   printf ("Hello from host\n");
   return 0;
   }

   
